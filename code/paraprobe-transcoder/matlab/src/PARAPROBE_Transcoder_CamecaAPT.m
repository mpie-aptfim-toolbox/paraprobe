% paraprobe-transcoder
% script to read Cameca/AMETEK *.APT file from APSuite6/IVAS4
% Markus K\"uhbach, m.kuehbach@mpie.de, 2020/03/19

clearvars;

%specify the location of the *.apt input file
fn = 'Z:/GITHUB/MPIE_APTFIM_TOOLBOX/paraprobe/code/84d83abc-537d-4795-854e-b65b53d5c0c9.apt';
%fn = 'Z:/GITHUB/MPIE_APTFIM_TOOLBOX/paraprobe/code/481f2262-b0d5-4dfb-91b7-fb31b57cd1b0.apt';


%create a class object containing all variables of the *.apt file
%apt = PARAPROBE_Transcoder(fn);
apt = PARAPROBE_Transcoder2(fn); 

% exemplarily call a method of the internal classes inside the file object
% here to show what the individual section of the file contain
apt.header.print();


% exemplar show mass spec or cumulative mass spec
histogram(apt.mq, 1000);
%plot(apt.pulse,apt.Multiplicity);
ecdf(apt.mq)