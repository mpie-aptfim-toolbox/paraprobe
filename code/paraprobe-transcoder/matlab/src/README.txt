% paraprobe-transcoder, m.kuehbach@mpie.de, Markus K\"uhbach, 2020/03/25

% Front end users should only use
% PARAPROBE_Transcoder_CamecaAPT.m shows how to import *.APT files

% Developers can modify the rest of the *.m files.
% These are the Matlab classes for the dirty backend work which the front end
% script calls.

% THIS IS A FIRST DEVELOPMENT VERSION !!
% SO FOR NOW PLEASE REPORT CHANGES OR BUGS TO m.kuehbach@mpie.de
% instead of implementing or fixing them yourself, this 
% way we can feed the bugfixes back into the community
% developers should contact me to edit on the GitLab directly!