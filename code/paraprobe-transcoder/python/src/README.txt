% paraprobe-transcoder, m.kuehbach@mpie.de, Markus K\"uhbach, 2020/03/25

% Front end users have to care only about the last three lines showing
% how to load a *.APT file by Python use PARAPROBE_Transcoder_CamecaAPT.py

% Developers can modify the upper parts, the class implementation of the reader.

% THIS IS A FIRST DEVELOPMENT VERSION !!
% SO FOR NOW PLEASE REPORT CHANGES OR BUGS TO m.kuehbach@mpie.de
% instead of implementing or fixing them yourself, this 
% way we can feed the bugfixes back into the community
% developers should contact me to edit on the GitLab directly!