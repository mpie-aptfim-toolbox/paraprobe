//##MK::GPLV3

#ifndef __PARAPROBE_TRANSCODER_XDMF_H__
#define __PARAPROBE_TRANSCODER_XDMF_H__

#include "PARAPROBE_TranscoderHDF5.h"

class transcoder_xdmf : public xdmfHdl
{
	//tool-specific sub-class of a XDMF inheriting all methods of the base class h5Hdl
	//coordinating instance handling all (sequential) writing to HDF5 file wrapping HDF5 low-level C library calls

public:
	transcoder_xdmf();
	~transcoder_xdmf();

	int create_volrecon_file( const string xmlfn, const size_t nions, const string h5ref );

//private:
};

#endif
