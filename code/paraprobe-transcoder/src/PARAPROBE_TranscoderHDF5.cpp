/*
	Copyright Max-Planck-Institut f\"ur Eisenforschung, GmbH, D\"sseldorf
	Data structure, code design, parallel implementation:
	Markus K\"uhbach, 2017-2018

	Third-party contributions:
	Andrew Breen - sequential Matlab code snippets for reconstruction and EPOS
	Markus G\"otz et al. - HPDBScan
	Kartik Kukreja - path compressed union/find
	Lester Hedges - AABBTree

	PARAPROBE --- is an MPI/OpenMP/SIMD-parallelized tool for efficient scalable
	processing of Atom Probe Tomography data targeting back-end processing.

	This file is part of PARAPROBE.

	PARAPROBE is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

 	PARAPROBE is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with paraprobe.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_TranscoderHDF5.h"

//https://www.learncpp.com/cpp-tutorial/114-constructors-and-initialization-of-derived-classes/


transcoder_h5::transcoder_h5()
{
}


transcoder_h5::~transcoder_h5()
{
}


int transcoder_h5::create_paraprobe_apth5( const string h5fn )
{
	h5resultsfn = h5fn;
	fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );

	//generate a IFES APTTC conformant APTH5 HDF5 file
	//##MK::catch errors

	//domain specific HDF5 keywords and data fields
	groupid = H5Gcreate2(fileid, APTH5_ACQ_00, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "APTH5_ACQ_00 " << status << "\n";

	//metadata specifying the environment in which the tool was used
	groupid = H5Gcreate2(fileid, APTH5_ACQ_01, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "APTH5_ACQ_01 " << status << "\n";

	//metadata specifying details and pieces of information about the tool
	groupid = H5Gcreate2(fileid, APTH5_ACQ_02, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "APTH5_ACQ_02 " << status << "\n";

	//measured quantities during the course of this experiment with this specific tool for this specific sample
	groupid = H5Gcreate2(fileid, APTH5_ACQ_03, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "APTH5_ACQ_03 " << status << "\n";

	status = H5Fclose(fileid);
cout << "Closing APTH5 file" << "\n";

	return WRAPPED_HDF5_SUCCESS;
}


int transcoder_h5::create_paraprobe_h5( const string h5fn )
{
	//taken from paraprobe-synthetic create_synthetic
	h5resultsfn = h5fn;
	fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );

	//generate a PARAPROBE APTH5 HDF5 file
	//##MK::catch errors

	//domain specific HDF5 keywords and data fields
	groupid = H5Gcreate2(fileid, PARAPROBE_SYNTH_VOLRECON, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "PARAPROBE_SYNTH_VOLRECON " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_SYNTH_VOLRECON_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "PARAPROBE_SYNTH_VOLRECON_META " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_SYNTH_VOLRECON_META_HRDWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "PARAPROBE_SYNTH_VOLRECON_META_HRDWR " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_SYNTH_VOLRECON_META_SFTWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "PARAPROBE_SYNTH_VOLRECON_META_SFTWR " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_SYNTH_VOLRECON_RES, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "PARAPROBE_SYNTH_VOLRECON_RES " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_TRANSCODE_DETSPACE, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "PARAPROBE_TRANSCODE_DETSPACE " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_TRANSCODE_DETSPACE_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "PARAPROBE_TRANSCODE_DETSPACE_META " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_TRANSCODE_DETSPACE_META_HRDWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "PARAPROBE_TRANSCODE_DETSPACE_META_HRDWR " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_TRANSCODE_DETSPACE_META_SFTWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "PARAPROBE_TRANSCODE_DETSPACE_META_SFTWR " << status << "\n";


	groupid = H5Gcreate2(fileid, PARAPROBE_TRANSCODE_DETSPACE_RES, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "PARAPROBE_TRANSCODE_DETSPACE_RES " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_TRANSCODE_VOLTCURVES, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "PARAPROBE_TRANSCODE_VOLTCURVES " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_TRANSCODE_VOLTCURVES_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "PARAPROBE_TRANSCODE_VOLTCURVES_META " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_TRANSCODE_VOLTCURVES_META_HRDWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "PARAPROBE_TRANSCODE_VOLTCURVES_META_HRDWR " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_TRANSCODE_VOLTCURVES_META_SFTWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "PARAPROBE_TRANSCODE_VOLTCURVES_META_SFTWR " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_TRANSCODE_VOLTCURVES_RES, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "PARAPROBE_TRANSCODE_VOLTCURVES_RES " << status << "\n";

	status = H5Fclose(fileid);
cout << "Closing APTH5 file" << "\n";

	return WRAPPED_HDF5_SUCCESS;
}
