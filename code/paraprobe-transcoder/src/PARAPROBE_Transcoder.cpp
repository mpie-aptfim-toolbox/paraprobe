//##MK::CODESPLIT

#include "PARAPROBE_TranscoderHdl.h"


//define what is passed upon execution
#define SIMID									1
#define CONTROLFILE								2

#ifndef GITSHA									//otherwise we could not compile without adding -DGITSHA="d123" a dummy GitSha
	#define GITSHA								0
#endif
//stringification of strings passed from cmake at compile time
//https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html
#define xstr(s)									str(s)
#define str(s) 									#s


void hello( void )
{
	cout << "paraprobe-transcoder" << "\n";
	cout << xstr(GITSHA) << "\n";
	cout << "Collecting " << CiteMeTranscoder::cite() << " publications for the tool to cite:" << "\n";
	for( auto it = CiteMeTranscoder::Citations.begin(); it != CiteMeTranscoder::Citations.end(); it++ ) {
		cout << it->author << "\n";
		cout << it->title << "\n";
		cout << it->journal << ", " << it->year << ", " << it->vol << ", pp" << it->pages << "\n";
		cout << it->doi << "\n";
	}

	cout << "Checking system-specific sizeof datatypes" << "\n";
	cout << "sizeof(APTFileHeaderIO) " << sizeof(APTFileHeaderIO) << "\n";
	cout << "sizeof(APTFileHeaderExtraIO) " << sizeof(APTFileHeaderExtraIO) << "\n";
	cout << "sizeof(APTSectionHeaderIO) " << sizeof(APTSectionHeaderIO) << "\n";
	cout << "sizeof(APTSectionHeaderExtraIO) " << sizeof(APTSectionHeaderExtraIO) << "\n";

	cout << "sizeof(cameca_filetime) " << sizeof(cameca_filetime) << "\n";
	cout << "sizeof(cameca_int64) " << sizeof(cameca_int64) << "\n";
	cout << "sizeof(int64_t) " << sizeof(int64_t) << "\n";
	cout << "sizeof(uint64_t) " << sizeof(uint64_t) << "\n";
	cout << "sizeof(unsigned long) " << sizeof(unsigned long) << "\n";
	cout << "sizeof(long) " << sizeof(long) << "\n";
	cout << "sizeof(int) " << sizeof(int) << "\n";
	cout << "sizeof(unsigned short) " << sizeof(unsigned short) << "\n";
	cout << "\n";
}


bool init(  int pargc, char** pargv )
{
	ConfigShared::SimID = stoul( pargv[SIMID] );
	try {
		ConfigTranscoder::readXML(pargv[CONTROLFILE]);
	}
	catch (exception& e) {
		cerr << "\n" << "Unable to parse control file! Details:" << "\n" << e.what() << "\n"; return false;
	}
	if ( ConfigTranscoder::checkUserInput() == false ) {
		cerr << "\n" << "Control file settings failed the validity check!" << "\n"; return false;
	}
	else {
		cout << "\n" << "Input is valid under SimulationID = " << "SimID." <<  ConfigShared::SimID << "\n";
	}
	cout << endl;
	return true;
}


void transcode_fileformats()
{
	//allocate sequential transcoder instance
	transcoderHdl* transcoder = NULL;
	try {
		transcoder = new transcoderHdl;
	}
	catch (bad_alloc &exc) {
		cerr << "Unable to allocate a sequential transcoderHdl instance" << "\n";
		return;
	}

	//load source file and prepare APTH5 HDF5 file group structure
	if ( transcoder->load_source_file() == true ) {

		if ( transcoder->init_target_file() == true ) { //transform the specific formats

			if ( transcoder->write_environment_and_settings() == true ) {
				cout << "Rank " << MASTER << " environment and settings written!" << "\n";
			}
			else {
				cerr << "Rank " << MASTER << " writing environment and settings failed!" << "\n";
				delete transcoder; transcoder = NULL; return;
			}

			switch (ConfigTranscoder::TranscodingMode)
			{
				case E_TRANSCODE_POS_TO_APTH5:
					transcoder->transcode_pos_to_paraprobe_apth5(); break;
					//transcoder->transcode_pos_to_apth5(); break;
				case E_TRANSCODE_EPOS_TO_APTH5:
					transcoder->transcode_epos_to_paraprobe_apth5(); break;
					//transcoder->transcode_epos_to_apth5(); break;
				case E_TRANSCODE_CAMECA_APT_TO_APTH5:
					transcoder->transcode_cameca_apt_to_paraprobe_apth5(); break;
					//transcoder->transcode_aptv2_to_apth5(); break;

				default:
					break;
			}
		}
		else {
			cerr << "Initializing target APTH5 failed" << "\n";
		}
	}
	else {
		cerr << "Loading source failed" << "\n";
	}

	transcoder->transcoder_tictoc.spit_profiling( "Transcoder", ConfigShared::SimID, MASTER );

	//release resources
	delete transcoder; transcoder = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();

	hello();
//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	//helloworld( argc, argv );

	if ( init( argc, argv ) == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	//##MK::n/a

//EXECUTE SPECIFIC TASKS
	transcode_fileformats();

//DESTROY MPI
	//##MK::n/a

	double toc = omp_get_wtime();
	cout << "paraprobe-transcoder took " << (toc-tic) << " seconds wall-clock time in total" << endl;

	return 0;
}
