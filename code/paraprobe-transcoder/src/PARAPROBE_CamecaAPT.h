//##MK::GPLV3

#ifndef __PARAPROBE_TRANSCODER_CAMECAAPT_H__
#define __PARAPROBE_TRANSCODER_CAMECAAPT_H__

//shared headers
#include "../../paraprobe-utils/src/metadata/PARAPROBE_UtilsMetadataDefsH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_SyntheticMetadataDefsH5.h"
#include "../../paraprobe-utils/src/PARAPROBE_APTH5.h"
#include "../../paraprobe-utils/src/PARAPROBE_XDMF.h"

//tool-specific headers
#include "PARAPROBE_TranscoderCiteMe.h"

//field REL_TYPE describes the relation of the records in this section 
//to the master sequence number of the main position section
enum CAMECA_APT_REL_TYPE
{
    //an enum implicitly encodes unsigned integers starting from 0,1,2,3, ...
	REL_UNKNOWN,
    ONE_TO_ONE,							// One-to-one ion to record mapping.  No index needed. 
    INDEXED,							// Sparse data.  Every record has an 64bit ion index as its first element
    UNRELATED,							// Records are not related to ion indices at all.
    ONE_TO_MANY							// Each record relates to a number of indices.
										// First element is # of indices, then a list, then the record itself.
};

//field RECORD_TYPE describes the way records are written in the section
enum CAMECA_APT_RECORD_TYPE
{
    RT_UNKNOWN,
    FIXED_SIZE,							// Each record has a fixed length. Random access is just record * size + header.
    VARIABLE_SIZE,						// Variable length records. Each record starts 64b length. No random access.
    VARIABLE_INDEXED					// Variable length records, but with an index table at the beginning of
										// the section that gives access points.  Semi-random access. (TDB)
};

//field RECORD_DATATYPE, along with iDataTypeSize, defines the type of data stored in the section
enum CAMECA_APT_RECORD_DATATYPE
{
    DT_UNKNOWN,
    INT,								// Signed integer, iDataTypeSize = { 8, 16, 32, 64 }
    UINT,								// Unsigned integer, iDataTypeSize = arbitrary (can bit-pack within records)
    FLOAT,								// Floating point, IEEE754, iDataTypeSize = { 32, 64 }
    CHARSTRING,							// Character string.  iDataTypeSize = { 8, 16 }.
										//   iRecordSize of 0 is null terminated; iRecordSize > 0 is fixed length
    OTHER
};

//The following strings are section types. They represent data that was previously stored in both .POS and .EPOS formats.
//Typically, it shouldt be necessary to have exact types except for specific IVAS required Position and Mass
// known section type strings
#define CAMECA_APT_POSITION_SECTION			L"Position"
#define CAMECA_APT_MASS_SECTION				L"Mass"
#define CAMECA_APT_IONTYPE_SECTION			L"Ion Type"
#define CAMECA_APT_TOF_SECTION				L"Time of Flight"
#define CAMECA_APT_VOLTAGE_SECTION			L"Voltage"
#define CAMECA_APT_PULSEV_SECTION			L"Pulse Voltage"
#define CAMECA_APT_DETECTORXY_SECTION		L"Detector Coordinates"
#define CAMECA_APT_PULSEDELTA_SECTION		L"Pulse Delta"
#define CAMECA_APT_HITTYPE_SECTION			L"Hit Type"   // "Hit register"
#define CAMECA_APT_PHASE_SECTION			L"Phase"


#define CAMECA_APT_CSIGNATURE_LEN				4
#define CAMECA_APT_WCSECTIONTYP_LEN				32
#define CAMECA_APT_WCDATAUNITTYP_LEN			16
#define CAMECA_APT_KWNSECT_POSITION_EXTRALEN	6


typedef uint64_t cameca_filetime;
typedef uint64_t cameca_int64;

struct APTFileHeaderIO
{
    char	cSignature[CAMECA_APT_CSIGNATURE_LEN];					// "APT\0"
    int		iHeaderSize;					// Header size, bytes (540)
    int		iHeaderVersion;					// Version # (2)
    
	//APSuite is a Windows executable! so in Windows the wchar_t type encodes UTF-16 2-byte wide chars
	//in Linux and OS X this is 4 bytes UTF-32 (gcc/g++ and XCode) though!
	//wchar_t	wcFilename[256];			// Original filename, UTF16, null terminated.
	unsigned short wcFilename[256];			//C++ unsigned short 2B in Unix

	//##MK::working on MAWS30, a Ubuntu 18.04.3 LTS, x86, little-endian system adding the additional two cameca_int64 will
	//bring us a 544 B struct which cannot be used to parse ion counts correctly this is likely because
	//addding the 8B entry adds additional padding, we could have expected this when we see the design of this
	//struct one should better start with the largest and not the smallest data items and do not mix!


	//FILETIME	ftCreationTime;				// Original file creation time
//	cameca_filetime	ftCreationTime;					// ##MK::currently not analyzed
    //int64_t	llIonCount;						// Number of ions represented by file
	//when going from Windows to Unix see here
	//https://stackoverflow.com/questions/13604137/definition-of-int64-t
//	cameca_int64 llIonCount;

	//static arrays cSignature and wcFilename in C++ will be zero initialized by default
	APTFileHeaderIO();
};

ostream& operator<<(ostream& in, APTFileHeaderIO const & val);


struct APTFileHeaderExtraIO
{
	cameca_int64 ftCreationTime;
	cameca_int64 llIonCount;
	APTFileHeaderExtraIO() : ftCreationTime(0), llIonCount(0) {}
};

ostream& operator<<(ostream& in, APTFileHeaderExtraIO const & val);

//we separate IO and actual APT*Header structs because the current way how Cameca lays out its structs
//causes alignment inconsistencies, for instance Cameca states that an APTFileHeader should have 540 B
//but if we for Unix, x86, Little-endian put the two cameca_int64 from APTFileHeaderExtraIO into the APTFileHeaderIO
//the resulting C++ struct has 544 B because of alignment issues and reading fails

struct APTSectionHeaderIO
{
    char 			cSignature[CAMECA_APT_CSIGNATURE_LEN];						// Section signature, "SEC\0"
    int				iHeaderSize;						// Size of the section header, bytes (148)
    int				iHeaderVersion;						// Version # of this section header (2)

	//wchar_t		wcSectionType[CAMECA_APT_WCSECTIONTYP_LEN];				// String representation of the section type.  UTF16
	unsigned short 	wcSectionType[CAMECA_APT_WCSECTIONTYP_LEN];				//##MK::see comments for APTFileHeader
    int				iSectionVersion;					// Version of this section data
    CAMECA_APT_REL_TYPE	eRelationshipType;			// How the records relate to ion #

    CAMECA_APT_RECORD_TYPE	eRecordType;			// Type of record (fixed, variable, etc.)
    CAMECA_APT_RECORD_DATATYPE eRecordDataType;		// The data type of the records (int, float, etc.)
    int				iDataTypeSize;						// The size, in bits, of the data type (eg 8 = 1 byte)
    int				iRecordSize;						// Size of the record (bytes). This must be a multiple
	//   of iDataTypeSize and 8, or 0 for variable length
    //wchar_t			wcDataUnit[CAMECA_APT_WCDATAUNITTYP_LEN];						// UTF16 string representing the unit of the data (eg "nm")
	unsigned short 	wcDataUnit[CAMECA_APT_WCDATAUNITTYP_LEN];
    //int64			llRecordCount;						// Number of records following this header.
//    cameca_int64	llRecordCount;
	//   DO NOT USE for seeking to next section, use llByteCount
    //int64			llByteCount;						// Number of bytes following the header.  This may be
 //   cameca_int64	llByteCount;
	//   > llRecordCount * iRecordSize to allow for padding
	
	APTSectionHeaderIO();
};

ostream& operator<<(ostream& in, APTSectionHeaderIO const & val);


struct APTSectionHeaderExtraIO
{
	//int64			llRecordCount;						// Number of records following this header.
	cameca_int64	llRecordCount;
	//   DO NOT USE for seeking to next section, use llByteCount
    //int64			llByteCount;						// Number of bytes following the header.  This may be
    cameca_int64	llByteCount;
	//   > llRecordCount * iRecordSize to allow for padding

	APTSectionHeaderExtraIO() : llRecordCount(0), llByteCount(0) {}
};

ostream& operator<<(ostream& in, APTSectionHeaderExtraIO const & val);


//duplication to have all properties conveniently organized in the code
struct APTFileHeader
{
    char	cSignature[CAMECA_APT_CSIGNATURE_LEN];
    int		iHeaderSize;
    int		iHeaderVersion;
    unsigned short wcFilename[256];
	cameca_int64 ftCreationTime;
	cameca_int64 llIonCount;
    APTFileHeader();
    APTFileHeader( APTFileHeaderIO const & head, APTFileHeaderExtraIO const & ifo );
};

ostream& operator<<(ostream& in, APTFileHeader const & val);


struct APTSectionHeader
{
    char 			cSignature[CAMECA_APT_CSIGNATURE_LEN];
    int				iHeaderSize;
    int				iHeaderVersion;
	unsigned short 	wcSectionType[CAMECA_APT_WCSECTIONTYP_LEN];
    int				iSectionVersion;
    CAMECA_APT_REL_TYPE	eRelationshipType;
    CAMECA_APT_RECORD_TYPE	eRecordType;
    CAMECA_APT_RECORD_DATATYPE eRecordDataType;
    int				iDataTypeSize;
    int				iRecordSize;
	unsigned short 	wcDataUnit[CAMECA_APT_WCDATAUNITTYP_LEN];
	cameca_int64	llRecordCount;
	cameca_int64	llByteCount;

	APTSectionHeader();
	APTSectionHeader( APTSectionHeaderIO const & head, APTSectionHeaderExtraIO const & ifo );
};

ostream& operator<<(ostream& in, APTSectionHeader const & val);



struct offsifo
{
	size_t efirst;										//first element
	size_t elast;										//how many elements
	size_t etarget;										//proposed number of data elements, not Bytes, to read per block
	size_t ecurrent;									//actual number of data elements, not Bytes, to read per block
	size_t count;
	size_t status;
	offsifo() : efirst(0), elast(0), etarget(0), ecurrent(0), count(0), status(0) {}
};

ostream& operator<<(ostream& in, offsifo const & val);


struct cameca_apt_section
{
	unsigned short wcSectionType[CAMECA_APT_WCSECTIONTYP_LEN];
	cameca_apt_section();
};

ostream& operator<<(ostream& in, cameca_apt_section const & val);



class cameca_apt
{
	//implements functionality to read and ## write AMETEK Cameca's APT file format

public:
	cameca_apt();
	~cameca_apt();
	
	bool read_cameca_apt_file_header();
	int closestNumber(int n, int m);
	/*
	bool read_cameca_apt_mass_header();
	bool read_cameca_apt_mass_data();
	bool read_cameca_apt_xyz_header();
	bool read_cameca_apt_xyz_data();
	*/
	unsigned int read_section_header_autodetect();
	template <class T> bool read_section_data_fixed_onetoone( unsigned int const & key );
	/*
	bool read_section_data_fixed_onetoone_float( unsigned int const & key );
	bool read_section_data_fixed_onetoone_uint16( unsigned int const & key );
	bool read_section_data_fixed_onetoone_double( unsigned int const & key );
	*/


	//bool read_aptv2( const string fn );
	bool read_aptv2_autodetect( const string fn );
	/*
	bool read_aptv2( const string fn );
	bool crack_aptv2( const string fn );
	*/

	APTFileHeader header;
	//APSuite6/IVAS4 *.APT v2 format possible headers 2020/03/22
	//map<pair<unsigned int, APTSectionHeader>> sections;
	map<unsigned int, APTSectionHeader> idtfyd_sections;

	/*
	APTSectionHeader mass_sect;
	APTSectionHeader xyz_sect;

	//##MK::2020/03/05 normal output after reconstruction with APSuite 6
	vector<apt_real> mq;								//mass-to-charge calibrated 1. section
	vector<p3d> xyz;									//reconstructed x,y,z 2. section
	*/
	
	aabb3d tipbox;										//axis-align box about the xyz coordinates

	vector<apt_real> tof;
	vector<apt_real> pulse;
	vector<apt_real> freq;
	vector<apt_real> tElapsed;
	vector<apt_real> erate;
	vector<unsigned short> tstage;
	vector<apt_real> TargetErate;
	vector<apt_real> TargetFlux;
	vector<short> pulseDelta;
	vector<apt_real> Pres;
	vector<apt_real> VAnodeMon;
	vector<apt_real> Temp;
	vector<apt_real> AmbTemp;
	vector<unsigned short> FractureGuard;
	vector<apt_real> Vref;
	vector<apt_real> Noise;
	vector<apt_real> Uniformity;
	vector<int> xstage;
	vector<int> ystage;
	vector<int> zstage;
	vector<int64_t> z; //##MK::really long ?
	vector<apt_real> tofc;
	vector<apt_real> Mass;
	vector<apt_real> tofb;
	vector<apt_real> xs;
	vector<apt_real> ys;
	vector<apt_real> zs;
	vector<apt_real> rTip;
	vector<apt_real> zApex;
	vector<apt_real> zSphereCorr;
	vector<apt_real> XDet_mm;
	vector<apt_real> YDet_mm;
	vector<int> Multiplicity;
	vector<apt_real> Vap;
	vector<apt_real> DetectorCoordinates;
	vector<apt_real> Position;

private:
	string filename;
	size_t filesize;
	FILE* file;
	offsifo offs;
	map<unsigned int, cameca_apt_section> known_sections;
};

#endif
