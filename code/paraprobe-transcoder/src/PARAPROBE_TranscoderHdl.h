//##MK::GPLV3

#ifndef __PARAPROBE_TRANSCODERHDL_H__
#define __PARAPROBE_TRANSCODERHDL_H__

#include "PARAPROBE_TranscoderXDMF.h"

class transcoderHdl
{
	//process-level class which implements the MPI worker instance which
	//transcode different file formats used in APT/FIM to a specific HDF5 file

public:
	transcoderHdl();
	~transcoderHdl();

	bool load_source_file();
	bool load_pos_seq( const string posfn );
	bool load_epos_seq( const string eposfn );
	bool load_cameca_apt_seq( const string aptfn );

	bool init_target_file();
	bool write_environment_and_settings();

	void transcode_pos_to_apth5();
	void transcode_pos_to_paraprobe_apth5();
	void transcode_epos_to_apth5();
	void transcode_epos_to_paraprobe_apth5(); //temporary solution until we have a commonly accepted H5 based exchange file format
	//void transcode_aptv2_to_apth5();
	void transcode_cameca_apt_to_paraprobe_apth5();


	//void set_mpidatatypes( void );
	//inline int get_rank( void ) { return myRank; }
	//inline int get_nranks( void ) { return nRanks; }
	//void set_rank( const int rr ) { myRank = rr; }
	//void set_nranks( const int nnrr ) { nRanks = nnrr; }

	cameca_apt aptv2reader;

	transcoder_h5 debugh5Hdl;
	transcoder_xdmf debugxdmfHdl;

	vector<vector<pos>*> rawdata_pos;
	vector<vector<epos>*> rawdata_epos;
	unsigned long nevt;

	profiler transcoder_tictoc;

private:
	//MPI related
	//int myRank;											//my MPI ID in the MPI_COMM_WORLD
	//int nRanks;
};


#endif

