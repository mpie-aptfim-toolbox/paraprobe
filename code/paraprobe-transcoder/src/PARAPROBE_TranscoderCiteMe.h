//##MK::GPLV3


#ifndef __PARAPROBE_TRANSCODER_CITEME_H__
#define __PARAPROBE_TRANSCODER_CITEME_H__

#include "CONFIG_Transcoder.h"


class CiteMeTranscoder
{
public:
	
	static vector<bibtex> Citations;
	
	static size_t cite();
};

#endif

