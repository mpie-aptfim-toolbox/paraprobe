//##MK::GPLV3

#include "PARAPROBE_CamecaAPT.h"

APTFileHeaderIO::APTFileHeaderIO()
{
	for( int i = 0; i < 4; i++ ) {
		this->cSignature[i] = 0x00;
	}
	this->iHeaderSize = 0; //540;
	this->iHeaderVersion = 0; //2;
	for( int i = 0; i < 256; i++ ) {
		this->wcFilename[i] = 0;
	}
}

ostream& operator<<(ostream& in, APTFileHeaderIO const & val)
{
	for( int i = 0; i < 4; i++ ) {
		//in << "cSignature[" << i << "] __" << int(val.cSignature[i]) << "__" << "\n";
		stringstream ss;
		ss << std::hex << int(val.cSignature[i]); // int decimal_value
		string res ( ss.str() );
		in << "cSignature[" << i << "] __" << res << "__" << "\n";
	}
	in << "iHeaderSize " << val.iHeaderSize << "\n";
	in << "iHeaderVersion " << val.iHeaderVersion << "\n";
	for( int i = 0; i < 256; i++ ) {
		//in << "wcFilename[" << i << "] __" << int(val.wcFilename[i]) << "__" << "\n";
		stringstream ss;
		ss << std::hex << int(val.wcFilename[i]);
		string res ( ss.str() );
		in << "wcFilename[" << i << "] __" << res << "__" << "\n";
	}
	return in;
}


ostream& operator<<(ostream& in, APTFileHeaderExtraIO const & val)
{
	in << "ftCreationTime " << val.ftCreationTime << "\n";
	in << "llIonCount " << val.llIonCount << "\n";
	return in;
}


APTSectionHeaderIO::APTSectionHeaderIO()
{
	for( int i = 0; i < 4; i++ ) {
		this->cSignature[i] = 0x00;
	}
	this->iHeaderSize = 0; //148;
	this->iHeaderVersion = 0; //2;
	for( int i = 0; i < 32; i++ ) {
		this->wcSectionType[i] = 0;
	}
	this->iSectionVersion = 0;
	this->eRelationshipType = REL_UNKNOWN;
	this->eRecordType = RT_UNKNOWN;
	this->eRecordDataType = DT_UNKNOWN;
	this->iDataTypeSize = 0;
	this->iRecordSize = 0;
	for( int i = 0; i < 16; i++ ) {
		this->wcDataUnit[i] = 0;
	}
}

ostream& operator<<(ostream& in, APTSectionHeaderIO const & val)
{
	for( int i = 0; i < 4; i++ ) {
		//in << "cSignature[" << i << "] __" << int(val.cSignature[i]) << "__" << "\n";
		stringstream ss;
		ss << std::hex << int(val.cSignature[i]); // int decimal_value
		string res ( ss.str() );
		in << "cSignature[" << i << "] __" << res << "__" << "\n";
	}
	in << "iHeaderSize " << val.iHeaderSize << "\n";
	in << "iHeaderVersion " << val.iHeaderVersion << "\n";
	for( int i = 0; i < 32; i++ ) {
		//in << "wcSectionType[" << i << "] __" << int(val.wcSectionType[i]) << "__" << "\n";
		stringstream ss;
		ss << std::hex << int(val.wcSectionType[i]); // int decimal_value
		string res ( ss.str() );
		in << "wcSectionType[" << i << "] __" << res << "__" << "\n";
	}
	in << "iSectionVersion " << val.iSectionVersion << "\n";
	in << "eRelationshipType " << val.eRelationshipType << "\n";
	in << "eRecordType " << val.eRecordType << "\n";
	in << "eRecordDataType " << val.eRecordDataType << "\n";
	in << "iDataTypeSize " << val.iDataTypeSize << "\n";
	in << "iRecordSize " << val.iRecordSize << "\n";
	for( int i = 0; i < 16; i++ ) {
		//in << "wcDataUnit[" << i << "] __" << int(val.wcDataUnit[i]) << "__" << "\n";
		stringstream ss;
		ss << std::hex << int(val.wcDataUnit[i]); // int decimal_value
		string res ( ss.str() );
		in << "wcDataUnit[" << i << "] __" << res << "__" << "\n";
	}
	return in;
}

ostream& operator<<(ostream& in, APTSectionHeaderExtraIO const & val)
{
	in << "llRecordCount " << val.llRecordCount << "\n";
	in << "llByteCount " << val.llByteCount << "\n";
	return in;
}


//convenience couts
APTFileHeader::APTFileHeader()
{
	for( int i = 0; i < 4; i++ ) {
		this->cSignature[i] = 0x00;
	}
	this->iHeaderSize = 0; //540;
	this->iHeaderVersion = 0; //2;
	for( int i = 0; i < 256; i++ ) {
		this->wcFilename[i] = 0;
	}
	this->ftCreationTime = 0;
	this->llIonCount = 0;
}


APTFileHeader::APTFileHeader( APTFileHeaderIO const & head, APTFileHeaderExtraIO const & ifo )
{
	for( int i = 0; i < 4; i++ ) {
		this->cSignature[i] = head.cSignature[i];
	}
	this->iHeaderSize = head.iHeaderSize;
	this->iHeaderVersion = head.iHeaderVersion;
	for( int i = 0; i < 256; i++ ) {
		this->wcFilename[i] = head.wcFilename[i];
	}
	this->ftCreationTime = ifo.ftCreationTime;
	this->llIonCount = ifo.llIonCount;
}

ostream& operator<<(ostream& in, APTFileHeader const & val)
{
	for( int i = 0; i < 4; i++ ) {
		//in << "cSignature[" << i << "] __" << int(val.cSignature[i]) << "__" << "\n";
		stringstream ss;
		ss << std::hex << int(val.cSignature[i]);
		string res ( ss.str() );
		in << "cSignature[" << i << "] __" << res << "__" << "\n";
	}
	in << "iHeaderSize " << val.iHeaderSize << "\n";
	in << "iHeaderVersion " << val.iHeaderVersion << "\n";
	for( int i = 0; i < 256; i++ ) {
		//in << "wcFilename[" << i << "] __" << int(val.wcFilename[i]) << "__" << "\n";
		stringstream ss;
		ss << std::hex << int(val.wcFilename[i]);
		string res ( ss.str() );
		in << "wcFilename[" << i << "] __" << res << "__" << "\n";
	}
	in << "ftCreationTime " << val.ftCreationTime << "\n";
	in << "llIonCount " << val.llIonCount << "\n";
	return in;
}


APTSectionHeader::APTSectionHeader()
{
	for( int i = 0; i < 4; i++ ) {
		this->cSignature[i] = 0x00;
	}
	this->iHeaderSize = 0; //148;
	this->iHeaderVersion = 0; //2;
	for( int i = 0; i < 32; i++ ) {
		this->wcSectionType[i] = 0;
	}
	this->iSectionVersion = 0;
	this->eRelationshipType = REL_UNKNOWN;
	this->eRecordType = RT_UNKNOWN;
	this->eRecordDataType = DT_UNKNOWN;
	this->iDataTypeSize = 0;
	this->iRecordSize = 0;
	for( int i = 0; i < 16; i++ ) {
		this->wcDataUnit[i] = 0;
	}
	this->llRecordCount = 0;
	this->llByteCount = 0;
}


APTSectionHeader::APTSectionHeader( APTSectionHeaderIO const & head, APTSectionHeaderExtraIO const & ifo )
{
	for( int i = 0; i < 4; i++ ) {
		this->cSignature[i] = head.cSignature[i];
	}
	this->iHeaderSize = head.iHeaderSize;
	this->iHeaderVersion = head.iHeaderVersion;
	for( int i = 0; i < 32; i++ ) {
		this->wcSectionType[i] = head.wcSectionType[i];
	}
	this->iSectionVersion = head.iSectionVersion;
	this->eRelationshipType = head.eRelationshipType;
	this->eRecordType = head.eRecordType;
	this->eRecordDataType = head.eRecordDataType;
	this->iDataTypeSize = head.iDataTypeSize;
	this->iRecordSize = head.iRecordSize;
	for( int i = 0; i < 16; i++ ) {
		this->wcDataUnit[i] = head.wcDataUnit[i];
	}
	this->llRecordCount = ifo.llRecordCount;
	this->llByteCount = ifo.llByteCount;
}

ostream& operator<<(ostream& in, APTSectionHeader const & val)
{
	for( int i = 0; i < 4; i++ ) {
		//in << "cSignature[" << i << "] __" << int(val.cSignature[i]) << "__" << "\n";
		stringstream ss;
		ss << std::hex << int(val.cSignature[i]); // int decimal_value
		string res ( ss.str() );
		in << "cSignature[" << i << "] __" << res << "__" << "\n";
	}
	in << "iHeaderSize " << val.iHeaderSize << "\n";
	in << "iHeaderVersion " << val.iHeaderVersion << "\n";
	for( int i = 0; i < 32; i++ ) {
		//in << "wcSectionType[" << i << "] __" << int(val.wcSectionType[i]) << "__" << "\n";
		stringstream ss;
		ss << std::hex << int(val.wcSectionType[i]); // int decimal_value
		string res ( ss.str() );
		in << "wcSectionType[" << i << "] __" << res << "__" << "\n";
	}
	in << "iSectionVersion " << val.iSectionVersion << "\n";
	in << "eRelationshipType " << val.eRelationshipType << "\n";
	in << "eRecordType " << val.eRecordType << "\n";
	in << "eRecordDataType " << val.eRecordDataType << "\n";
	in << "iDataTypeSize " << val.iDataTypeSize << "\n";
	in << "iRecordSize " << val.iRecordSize << "\n";
	for( int i = 0; i < 16; i++ ) {
		//in << "wcDataUnit[" << i << "] __" << int(val.wcDataUnit[i]) << "__" << "\n";
		stringstream ss;
		ss << std::hex << int(val.wcDataUnit[i]); // int decimal_value
		string res ( ss.str() );
		in << "wcDataUnit[" << i << "] __" << res << "__" << "\n";
	}
	in << "llRecordCount " << val.llRecordCount << "\n";
	in << "llByteCount " << val.llByteCount << "\n";
	return in;
}


ostream& operator<<(ostream& in, offsifo const & val)
{
	in << "efirst " << val.efirst << "\n";
	in << "elast " << val.elast << "\n";
	in << "etarget " << val.etarget << "\n";
	in << "ecurrent " << val.ecurrent << "\n";
	in << "count " << val.count << "\n";
	in << "status " << val.status << "\n";
	return in;
}


cameca_apt_section::cameca_apt_section()
{
	for( int i = 0; i < CAMECA_APT_WCSECTIONTYP_LEN; i++ ) {
		this->wcSectionType[i] = 0x00;
	}
}

ostream& operator<<(ostream& in, cameca_apt_section const & val)
{
	for( int i = 0; i < CAMECA_APT_WCSECTIONTYP_LEN; i++ ) {
		//in << "wcDataUnit[" << i << "] __" << int(val.wcDataUnit[i]) << "__" << "\n";
		stringstream ss;
		ss << std::hex << int(val.wcSectionType[i]); // int decimal_value
		string res ( ss.str() );
		in << "wcSectionType[" << i << "] __" << res << "__" << "\n";
	}
	return in;
}

#define CAMECA_APT_KWNSECT_FAILURE					UINT32MX
#define CAMECA_APT_KWNSECT_UNKNOWN					0
#define CAMECA_APT_KWNSECT_TOF						1
#define CAMECA_APT_KWNSECT_PULSE					2
#define CAMECA_APT_KWNSECT_FREQ						3
#define CAMECA_APT_KWNSECT_TELAPSED					4
#define CAMECA_APT_KWNSECT_ERATE					5
#define CAMECA_APT_KWNSECT_TSTAGE					6
#define CAMECA_APT_KWNSECT_TARGETERATE				7
#define CAMECA_APT_KWNSECT_TARGETFLUX				8
#define CAMECA_APT_KWNSECT_PULSEDELTA				9
#define CAMECA_APT_KWNSECT_PRES						10
#define CAMECA_APT_KWNSECT_VANODEMON				11
#define CAMECA_APT_KWNSECT_TEMP						12
#define CAMECA_APT_KWNSECT_AMBTEMP					13
#define CAMECA_APT_KWNSECT_FRACTUREGRD				14
#define CAMECA_APT_KWNSECT_VREF						15
#define CAMECA_APT_KWNSECT_NOISE					16
#define CAMECA_APT_KWNSECT_UNIFRMITY				17
#define CAMECA_APT_KWNSECT_XSTAGE					18
#define CAMECA_APT_KWNSECT_YSTAGE					19
#define CAMECA_APT_KWNSECT_ZSTAGE					20
#define CAMECA_APT_KWNSECT_Z						21
#define CAMECA_APT_KWNSECT_TOFC						22
#define CAMECA_APT_KWNSECT_MASS						23
#define CAMECA_APT_KWNSECT_TOFB						24
#define CAMECA_APT_KWNSECT_XS						25
#define CAMECA_APT_KWNSECT_YS						26
#define CAMECA_APT_KWNSECT_ZS						27
#define CAMECA_APT_KWNSECT_RTIP						28
#define CAMECA_APT_KWNSECT_ZAPEX					29
#define CAMECA_APT_KWNSECT_ZSPHCORR					30
#define CAMECA_APT_KWNSECT_XDET						31
#define CAMECA_APT_KWNSECT_YDET						32
#define CAMECA_APT_KWNSECT_MULT						33
#define CAMECA_APT_KWNSECT_VAP						34
#define CAMECA_APT_KWNSECT_DETCORR					35
#define CAMECA_APT_KWNSECT_POSITION					36

//#define VERBOSE_KEYWORDS

cameca_apt::cameca_apt()
{
	//header = APTFileHeaderIO();
	//header_ifo = APTFileHeaderExtraIO();

	header = APTFileHeader();
	/*
	mass_sect = APTSectionHeader();
	xyz_sect = APTSectionHeader();
	*/

	tipbox = aabb3d();

	filename = "";
	filesize = 0; //in byte
	file = NULL;
	offs = offsifo();

	vector<unsigned short> hexx;
	string keyword = "";
	//tof
	keyword = "tof";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_TOF, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_TOF].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_TOF] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//pulse
	keyword = "pulse";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_PULSE, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j))) ); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_PULSE].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_PULSE] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//freq
	keyword = "freq";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_FREQ, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_FREQ].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_FREQ] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//tElapsed
	keyword = "tElapsed";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_TELAPSED, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_TELAPSED].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_TELAPSED] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//erate
	keyword = "erate";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_ERATE, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_ERATE].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_ERATE] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//tstage
	keyword = "tstage";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_TSTAGE, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_TSTAGE].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_TSTAGE] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//TargetErate
	keyword = "TargetErate";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_TARGETERATE, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_TARGETERATE].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_TARGETERATE] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//TargetFlux
	keyword = "TargetFlux";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_TARGETFLUX, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_TARGETFLUX].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_TARGETFLUX] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//pulseDelta
	keyword = "pulseDelta";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_PULSEDELTA, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_PULSEDELTA].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_PULSEDELTA] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//Pres
	keyword = "Pres";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_PRES, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_PRES].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_PRES] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//VAnodeMon
	keyword = "VAnodeMon";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_VANODEMON, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_VANODEMON].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_VANODEMON] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//Temp
	keyword = "Temp";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_TEMP, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_TEMP].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_TEMP] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//AmbTemp
	keyword = "AmbTemp";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_AMBTEMP, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_AMBTEMP].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_AMBTEMP] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//FractureGuard
	keyword = "FractureGuard";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_FRACTUREGRD, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_FRACTUREGRD].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_FRACTUREGRD] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//Vref
	keyword = "Vref";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_VREF, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_VREF].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_VREF] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//Noise
	keyword = "Noise";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_NOISE, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_NOISE].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_NOISE] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//Uniformity
	keyword = "Uniformity";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_UNIFRMITY, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_UNIFRMITY].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_UNIFRMITY] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//xstage
	keyword = "xstage";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_XSTAGE, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_XSTAGE].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_XSTAGE] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//ystage
	keyword = "ystage";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_YSTAGE, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_YSTAGE].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_YSTAGE] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//zstage
	keyword = "zstage";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_ZSTAGE, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_ZSTAGE].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_ZSTAGE] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//z
	keyword = "z";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_Z, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_Z].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_Z] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//tofc
	keyword = "tofc";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_TOFC, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_TOFC].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_TOFC] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//Mass
	keyword = "Mass";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_MASS, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_MASS].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_MASS] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//tofb
	keyword = "tofb";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_TOFB, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_TOFB].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_TOFB] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//xs
	keyword = "xs";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_XS, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_XS].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_XS] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//ys
	keyword = "ys";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_YS, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_YS].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_YS] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//zs
	keyword = "zs";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_ZS, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_ZS].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_ZS] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//rTip
	keyword = "rTip";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_RTIP, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_RTIP].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_RTIP] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//zApex
	keyword = "zApex";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_ZAPEX, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_ZAPEX].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_ZAPEX] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//zSphereCorr
	keyword = "zSphereCorr";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_ZSPHCORR, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_ZSPHCORR].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_ZSPHCORR] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//XDet_mm
	keyword = "XDet_mm";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_XDET, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_XDET].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_XDET] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//YDet_mm
	keyword = "YDet_mm";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_YDET, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_YDET].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_YDET] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//Multiplicity
	keyword = "Multiplicity";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_MULT, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_MULT].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_MULT] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//Vap
	keyword = "Vap";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_VAP, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_VAP].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_VAP] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//Detector Coordinates
	keyword = "Detector Coordinates";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_DETCORR, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_DETCORR].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_DETCORR] << "\n"; //hexx.clear();
#endif
	hexx.clear();
	//Position
	keyword = "Position";
	known_sections.insert( pair<unsigned int, cameca_apt_section>( CAMECA_APT_KWNSECT_POSITION, cameca_apt_section() ) );
	for( size_t j = 0; j < keyword.length(); j++ ) { hexx.push_back( static_cast<unsigned short>(int(keyword.at(j)))); } //transform ASCII character to integer key
	for( size_t i = 0; i < hexx.size(); i++ ) { known_sections[CAMECA_APT_KWNSECT_POSITION].wcSectionType[i] = hexx.at(i); }
#ifdef VERBOSE_KEYWORDS
	cout << keyword << "\n" << known_sections[CAMECA_APT_KWNSECT_POSITION] << "\n"; //hexx.clear();
#endif
	hexx.clear();
}


cameca_apt::~cameca_apt()
{
	if ( file != NULL ) {
		fclose(file);
		file = NULL;
	}
	offs = offsifo();
}


#define FISHING		4096

/*
bool cameca_apt::read_aptv2( const string fn )
{
	size_t count = 0; // map binary representations one-on-one into C++ structs specific for the format
	size_t status = 0;
	FILE* file = fopen( fn.c_str(), "rb" );
	if ( file == NULL ) {
		cerr << "Opening APT file " << fn << " failed!" << "\n";
		return false;
	}

cout << "Attempt reading APTFileHeader sizeof(APTFileHeader) " << sizeof(APTFileHeader) << "\n";
	header = APTFileHeader();
	count = 1; // map binary representation of the header one-on-one into the C++ struct
	status = 0;
	status = fread( &header, sizeof(APTFileHeader), count, file ); //file pointer is advanced implicitly/automatically
	if ( status != count ) {
		cerr << "APTFileHeader reading failed!" << "\n";
		fclose(file);
		return false;
	}
cout << header << "\n";

	//fish the two uint64_t separately for alignment reasons
cout << "Attempt reading APTFileHeaderExtra sizeof(APTFileHeaderExtra) " << sizeof(APTFileHeaderExtra) << "\n";
	header_ifo = APTFileHeaderExtra();
	count = 1;
	status = 0;
	status = fread( &header_ifo, sizeof(APTFileHeaderExtra), 1, file);
	if ( status != count ) {
		cerr << "APTFileHeaderExtra reading failed!" << "\n";
		fclose(file);
		return false;
	}
cout << header_ifo << "\n";

	//read first section
cout << "Attempt reading APTSectionHeader sizeof(APTSectionHeader) " << sizeof(APTSectionHeader) << "\n";
	APTSectionHeader tmp = APTSectionHeader();
	count = 1;
	status = 0;
	status = fread( &tmp, sizeof(APTSectionHeader), count, file );
	if ( status != count ) {
		cerr << "APTSectionHeader reading failed!" << "\n";
		fclose(file);
		return false;
	}
cout << tmp << "\n";

cout << "Attempt reading APTSectionHeaderExtra sizeof(APTSectionHeaderExtra) " << sizeof(APTSectionHeaderExtra) << "\n";
	APTSectionHeaderExtra tmp_ifo = APTSectionHeaderExtra();
	count = 1;
	status = 0;
	status = fread( &tmp_ifo, sizeof(APTSectionHeaderExtra), count, file );
	if ( status != count ) {
		cerr << "APTSectionHeaderExtra reading failed!" << "\n";
		fclose(file);
		return false;
	}
cout << tmp_ifo << "\n";

	sections.push_back( APTSectionHeader( tmp ) );
	sections_ifo.push_back( APTSectionHeaderExtra( tmp_ifo ) );

cout << "------------------------------------->" << "\n";
cout << sections.back() << "\n";
cout << "------------------------------------->" << "\n";
cout << sections_ifo.back() << "\n";

	//##MK::as of 2020/03/05 1. header is the mass to charge what follows thereafter?
	//MK::read successively blocks of C structs and store in blocks the rawdata
	//benefit is that such later rawdata can be processes blockwise and deleted successively reducing the total memory footprint of the application for reconstruction by not more than though factor 2
	size_t efirst = 0;
	size_t elast = static_cast<size_t>(header_ifo.llIonCount);
	size_t etarget = MPIIO_READ_CACHE / sizeof(apt_real);
	size_t ecurrent = 0;
	apt_real* rbuf = NULL;
	for ( size_t e = efirst; e < elast; 	) { //read block-by-block, number of elements, not total bytes
		//update actual read cache length
		ecurrent = ((e + etarget) < elast) ? etarget : elast - e;
		//allocate buffer if none existent, reallocate if of different size
		if ( rbuf != NULL ) {
			if ( ecurrent != etarget ) {
				//reallocate buffer of size ecurrent
				delete [] rbuf; rbuf = NULL;
				try { rbuf = new apt_real[ecurrent]; }
				catch (bad_alloc &ioexc) {
					cerr << "Unable to reallocate rbuf for apt_real in cameca_apt_seq read!" << "\n";
					fclose(file); return false;
				}
			}
			//(re)utilize allocated buffer
		}
		else {
			//least likely case first time allocation
			try { rbuf = new apt_real[ecurrent]; }
			catch (bad_alloc &ioexc) {
				cerr << "Unable to allocate rbuf for apt_real in cameca_apt_seq read!" << "\n";
				fclose(file); return false;
			}
		}
		//by now buffer allocated or already out of subroutine
		status = 0;
		status = fread( rbuf, sizeof(apt_real), ecurrent, file ); //file pointer is advanced implicitly/automatically
		if ( status == ecurrent ) { //read success
			//dump data//allocate new buffer to store results
			for( size_t j = 0; j < ecurrent; j++ ) {
				mq.push_back( rbuf[j] );
			}
			e = e + ecurrent;
cout << "e/ecurrent/mq.size()\t\t" << e << "\t\t" << ecurrent << "\t\t" << mq.size() << "\t\t" << mq.back() << "\n";
		} //block read of ecurrent successful
		else {
			cerr << "I/O failed on " << fn << " at element position " << e << "\n";
			delete [] rbuf; fclose(file); return false;
		}
	} //next block
	delete [] rbuf; rbuf = NULL;
cout << "mq.size() " << mq.size() << "\t\t" << mq.back() << "\n";

	//read second section
	//catch possible extra header for data extent
	//parse xyz
	//########################################
	//########################################
	//########################################


	//fish the next bytes
	unsigned char fishuc[FISHING]; //zero-initialized by default
	cout << "Start fishing unsigned char's " << "\n";
	//for( int ii = 0; ii < FISHING; ii++ ) {
	//	cout << "fish[" << ii << "] __" << int(fish[ii]) << "__" << "\n";
	//}
	count = 1;
	status = 0;
	status = fread( fishuc, sizeof(unsigned char)*FISHING, count, file );
	if ( status != count ) {
		cerr << "APTSectionHeaderExtra reading failed!" << "\n";
		fclose(file);
		return false;
	}

	for( int ii = 0; ii < FISHING; ii++ ) {
		//cout << "fish[" << ii << "] __" << int(fishuc[ii]) << "__" << "\n";
		stringstream ss;
		ss << std::hex << int(fishuc[ii]); // int decimal_value
		string res ( ss.str() );
		cout << "fish[" << ii << "] __" << res << "__" << "\n";
	}

	//cout << "APTFileHeader reading was successful!" << "\n";
	//cout << tmp << "\n";

	fclose(file);
	return true;
}
*/


bool cameca_apt::read_cameca_apt_file_header()
{
	//implement platform checks because currently only Unix, x86, little-endian is supported
	if ( sizeof(cameca_filetime) != 8 || sizeof(cameca_int64) != 8 ) { //sizeof(APTFileHeaderIO) != 540 || sizeof(APTFileHeaderExtraIO) != 2*8 ||
		cerr << "The current implementation of the *.APT reader does not support this hardware/operating system configuration!" << "\n";
		cerr << "Inspect the datatypes, alignments, and endianness for your specific system!" << "\n";
		return false;
	}

	//get total size of the dataset to check for possible inconsistence
	/*unsigned long sz[1] = {0};
	struct stat oscall;
	if ( stat( filename.c_str() , &oscall ) != -1 ) { //MK::take care of case when filesize exceeds range of unsigned long...
		sz[0] = static_cast<unsigned long>(oscall.st_size);
		filesize = static_cast<size_t>(sz[0]);
cout << "The *.APT file " << filename << " has a total size of " << filesize << " Bytes" << "\n";
	}*/

	offs.status = 0;
	offs.status = fseek( file, 0, SEEK_END );   //##MK::not portable
	if ( offs.status == 0 ) {
		long int sz = ftell( file );
		rewind(file);
		filesize = sz;
cout << "The *.APT file " << filename << " has a total size of " << filesize << "/" << sz << " Bytes" << "\n";
	}
	else {
		cerr << "*.APT file " << filename << " cannot be accessed!" << "\n"; return false;
	}


	long int implicit_fp_pos = ftell( file );
cout << "Implicit file pointer for fread stands at " << implicit_fp_pos << " Bytes from the beginning of the file" << "\n";
	if ( static_cast<size_t>(implicit_fp_pos) != 0 ) {
		cout << "We are not starting at the beginning of the file!" << "\n"; return false;
	}

cout << "Attempt reading APTFileHeader sizeof(APTFileHeaderIO) " << sizeof(APTFileHeaderIO) << "\n";
	APTFileHeaderIO tmp_header = APTFileHeaderIO();
	offs.count = 1; // map binary representation of the header one-on-one into the C++ struct
	offs.status = 0;
	offs.status = fread( &tmp_header, sizeof(APTFileHeaderIO), offs.count, file ); //file pointer is advanced implicitly/automatically
	if ( offs.status != offs.count ) {
		cerr << "APTFileHeaderIO reading failed!" << "\n";
		return false;
	}
//cout << tmp_header << "\n";

	//consistence checks
	//is the file a valid APT file?
	if ( tmp_header.cSignature[0] != 0x41 || tmp_header.cSignature[1] != 0x50 || tmp_header.cSignature[2] != 0x54 || tmp_header.cSignature[3] != 0x00 ) {
		cerr << "The cSignature of tmp_header indicates the file is not a valid *.APT file!" << "\n";
		return false;
	}
	if ( tmp_header.iHeaderVersion != 2 ) {
		cerr << "The iHeaderVersion of tmp_header indicates the file has a different *.APT version!" << "\n";
		return false;
	}

	//fish the additional two uint64_t separately for alignment reasons detailed in the declaration of the APTFileHeaderIO structs
cout << "Attempt reading APTFileHeaderExtra sizeof(APTFileHeaderExtraIO) " << sizeof(APTFileHeaderExtraIO) << "\n";
	APTFileHeaderExtraIO tmp_ifo = APTFileHeaderExtraIO();
	offs.count = 1;
	offs.status = 0;
	offs.status = fread( &tmp_ifo, sizeof(APTFileHeaderExtraIO), offs.count, file);
	if ( offs.status != offs.count ) {
		cerr << "APTFileHeaderExtraIO reading failed!" << "\n";
		return false;
	}
//cout << tmp_ifo << "\n";

	//consistence checks
	if ( tmp_ifo.llIonCount == 0 ) {
		cerr << "The llIonCount of tmp_ifo indicates that the file does not contain ions!" << "\n";
		return false;
	}

	//translate infos
	header = APTFileHeader( tmp_header, tmp_ifo );
	return true;
}


//https://www.geeksforgeeks.org/find-number-closest-n-divisible-m/
int cameca_apt::closestNumber(int n, int m)
{
    // find the quotient
    int q = n / m;

    // 1st possible closest number
    int n1 = m * q;

    // 2nd possible closest number
    int n2 = (n * m) > 0 ? (m * (q + 1)) : (m * (q - 1));

    // if true, then n1 is the required closest number
    if (abs(n - n1) < abs(n - n2))
        return n1;

    // else n2 is the required closest number
    return n2;
}


/*
bool cameca_apt::read_cameca_apt_mass_header()
{
	//read first section
cout << "Attempt reading APTSectionHeaderIO sizeof(APTSectionHeaderIO) " << sizeof(APTSectionHeaderIO) << "\n";
	APTSectionHeaderIO tmp_header = APTSectionHeaderIO();
	offs.count = 1;
	offs.status = 0;
	offs.status = fread( &tmp_header, sizeof(APTSectionHeaderIO), offs.count, file );
	if ( offs.status != offs.count ) {
		cerr << "APTSectionHeaderIO reading failed!" << "\n";
		return false;
	}
//cout << tmp_header << "\n";

	//consistence checks
	//is this at all a section?
	if ( tmp_header.cSignature[0] != 0x53 || tmp_header.cSignature[1] != 0x45 || tmp_header.cSignature[2] != 0x43 || tmp_header.cSignature[3] != 0x00 ) {
		cerr << "The cSignature of tmp_header indicates an invalid section in the *.APT file!" << "\n";
		return false;
	}
	if ( tmp_header.iHeaderVersion != 2 ) {
		cerr << "The iHeaderVersion tmp_header indicates the section version is unexpected!" << "\n";
		return false;
	}
	//is this the section we want, the mass section?
	if ( 	tmp_header.wcSectionType[0] != 77 || tmp_header.wcSectionType[1] != 97 ||
			tmp_header.wcSectionType[2] != 115 || tmp_header.wcSectionType[3] != 115  ) { //"Mass" 0x4d, 0x61, 0x73, 0x73
		cerr << "The wcSectionType of tmp_header indicates the section is not encoding the Mass-to-charge!" << "\n";
		return false;
	}
	if ( tmp_header.eRelationshipType != ONE_TO_ONE ) {
		cerr << "The eRelationshipType tmp_header is not ONE_TO_ONE but that is the only currently supported!" << "\n";
		return false;
	}
	if ( tmp_header.eRecordType != FIXED_SIZE ) {
		cerr << "The eRecordType tmp_header is not FIXED_SIZE but that is the only currently supported!" << "\n";
		return false;
	}
	if ( tmp_header.eRecordDataType != FLOAT ) {
		cerr << "The eRecordDataType tmp_header is not FLOAT but we have expected this for mass values!" << "\n";
		return false;
	}
	if ( tmp_header.iDataTypeSize != 32 ) {
		cerr << "The iDataTypeSize is not 32(-bit) what we would have expected for single-precision mass-to-charge values!" << "\n";
		return false;
	}
	if ( tmp_header.iRecordSize != 1*(32/8) ) {
		cerr << "The iRecordSize is not 1*4B what we would expect for single-precision mass-to-charge values!" << "\n";
		return false;
	}

cout << "Attempt reading APTSectionHeaderExtraIO sizeof(APTSectionHeaderExtraIO) " << sizeof(APTSectionHeaderExtraIO) << "\n";
	APTSectionHeaderExtraIO tmp_ifo = APTSectionHeaderExtraIO();
	offs.count = 1;
	offs.status = 0;
	offs.status = fread( &tmp_ifo, sizeof(APTSectionHeaderExtraIO), offs.count, file );
	if ( offs.status != offs.count ) {
		cerr << "APTSectionHeaderExtraIO reading failed!" << "\n";
		return false;
	}
//cout << tmp_ifo << "\n";

	//consistence checks
	if ( tmp_ifo.llRecordCount != header.llIonCount ) { //assumes FIXED_SIZE, ONE_TO_ONE, FLOAT
		cerr << "The llRecordCount does not match with llIonCount!" << "\n";
		return false;
	}
	if ( tmp_ifo.llByteCount != (header.llIonCount * tmp_header.iRecordSize) ) {
		cerr << "The llByteCount does not match with llIonCount*iRecordSize!" << "\n";
		return false;
	}

	mass_sect = APTSectionHeader( tmp_header, tmp_ifo );
	return true;
}


bool cameca_apt::read_cameca_apt_mass_data()
{
	size_t faulty = 0;
	//MK::read successively blocks of C instead of all at once,
	//benefit is that such later rawdata can be processes blockwise and deleted successively reducing
	//the total memory footprint of the application for reconstruction by not more than though factor 2
	size_t efirst = 0;
	size_t elast = static_cast<size_t>(header.llIonCount);
	size_t etarget = MPIIO_READ_CACHE / (sizeof(apt_real)*1);
	size_t ecurrent = 0;
	size_t status = 0;
	apt_real* rbuf = NULL;
	size_t eold = 0;
	for ( size_t e = efirst; e < elast; 	) { //read block-by-block, number of elements, not total bytes
		//update actual read cache length
//cout << "e/eold/ecurrent/elast\t\t" << e << "\t\t" << eold << "\t\t" << ecurrent << "\t\t" << elast << "\n";
		if ( (e + etarget) < elast) {
			eold = ecurrent;
			ecurrent = etarget;
		}
		else {
			eold = ecurrent;
			ecurrent = elast - e;
			if ( ecurrent < 1 ) { //we are done because nobody there
				break;
			}
			if ( ecurrent > elast ) {
				cerr << "Failure, ecurrent wrap-around!" << "\n";
				delete [] rbuf; rbuf = NULL; return false;
			}
		}
		//allocate a buffer if none existent, reallocate if required of different size
		if ( rbuf != NULL ) {
			if ( ecurrent != eold ) { //reallocate buffer of size ecurrent
				delete [] rbuf; rbuf = NULL;
				try {
					rbuf = new apt_real[ecurrent];
				}
				catch (bad_alloc &ioexc) {
					cerr << "Unable to reallocate rbuf in read_cameca_apt_mass_data!" << "\n";
					return false;
				}
			}
			//nothing to do just (re)utilize the allocated buffer
		}
		else {
			//least likely case first time allocation
			try {
				rbuf = new apt_real[ecurrent];
			}
			catch (bad_alloc &ioexc) {
				cerr << "Unable to allocate rbuf in read_cameca_apt_mass_data!" << "\n";
				return false;
			}
		}
		//by now buffer allocated or already out of subroutine
		status = 0;
		status = fread( rbuf, sizeof(apt_real), ecurrent, file ); //file pointer is advanced implicitly/automatically
//cout << "status " << status << "\n";
		if ( status == ecurrent ) { //read success
			//dump data//allocate new buffer to store results
			for( size_t j = 0; j < ecurrent; j++ ) {
				mq.push_back( rbuf[j] );
				if ( rbuf[j] > 1.0e-5 ) {
					continue;
				}
				else {
					cerr << setprecision(32) << rbuf[j] << "\n";
 					faulty++;
				}
			}
//cout << "e/ecurrent/mq.size()\t\t" << e << "\t\t" << ecurrent << "\t\t" << mq.size() << "\t\t" << mq.back() << "\n";
		} //block read of ecurrent successful
		else {
			cerr << "I/O failed in read_cameca_apt_mass_data at element position " << e << "\n";
			delete [] rbuf; rbuf = NULL; return false;
		}
		e = e + ecurrent;
	} //next block
	delete [] rbuf; rbuf = NULL;
cout << "mq.size() " << mq.size() << "\t\t" << mq.back() << "\n";
cerr << "Number of faulty ions zero numerically zero m/q " << faulty << "\n";
	return true;
}


bool cameca_apt::read_cameca_apt_xyz_header()
{
	//read second section
cout << "Attempt reading APTSectionHeaderIO sizeof(APTSectionHeaderIO) " << sizeof(APTSectionHeaderIO) << "\n";
	APTSectionHeaderIO tmp_header = APTSectionHeaderIO();
	offs.count = 1;
	offs.status = 0;
	offs.status = fread( &tmp_header, sizeof(APTSectionHeaderIO), offs.count, file );
	if ( offs.status != offs.count ) {
		cerr << "APTSectionHeaderIO reading failed!" << "\n";
		return false;
	}
//cout << tmp_header << "\n";

	//consistence checks
	//is this at all a section?
	if ( tmp_header.cSignature[0] != 0x53 || tmp_header.cSignature[1] != 0x45 || tmp_header.cSignature[2] != 0x43 || tmp_header.cSignature[3] != 0x00 ) {
		cerr << "The cSignature of tmp_header indicates an invalid section in the *.APT file!" << "\n";
		return false;
	}
	if ( tmp_header.iHeaderVersion != 2 ) {
		cerr << "The iHeaderVersion tmp_header indicates the section version is unexpected!" << "\n";
		return false;
	}
	//is this the section we want, the position section?
	if ( 	tmp_header.wcSectionType[0] != 80 || tmp_header.wcSectionType[1] != 111 ||
			tmp_header.wcSectionType[2] != 115 || tmp_header.wcSectionType[3] != 105 ||
			tmp_header.wcSectionType[4] != 116 || tmp_header.wcSectionType[5] != 105 ||
			tmp_header.wcSectionType[6] != 111 || tmp_header.wcSectionType[7] != 110  ) { //"Position" 0x50, 0x6f,...
		cerr << "The wcSectionType of tmp_header indicates the section is not encoding the Position coordinate values!" << "\n";
		return false;
	}
	if ( tmp_header.eRelationshipType != ONE_TO_ONE ) {
		cerr << "The eRelationshipType tmp_header is not ONE_TO_ONE but that is the only currently supported!" << "\n";
		return false;
	}
	if ( tmp_header.eRecordType != FIXED_SIZE ) {
		cerr << "The eRecordType tmp_header is not FIXED_SIZE but that is the only currently supported!" << "\n";
		return false;
	}
	if ( tmp_header.eRecordDataType != FLOAT ) {
		cerr << "The eRecordDataType tmp_header is not FLOAT but we have expected this for mass values!" << "\n";
		return false;
	}
	if ( tmp_header.iDataTypeSize != 32 ) {
		cerr << "The iDataTypeSize is not 32(-bit) what we would have expected for single-precision position coordinate values!" << "\n";
		return false;
	}
	if ( tmp_header.iRecordSize != 3*(32/8) ) {
		cerr << "The iRecordSize is not 3*4B what we would expect for single-precision position coordinate values!" << "\n";
		return false;
	}

cout << "Attempt reading APTSectionHeaderExtraIO sizeof(APTSectionHeaderExtraIO) " << sizeof(APTSectionHeaderExtraIO) << "\n";
	APTSectionHeaderExtraIO tmp_ifo = APTSectionHeaderExtraIO();
	offs.count = 1;
	offs.status = 0;
	offs.status = fread( &tmp_ifo, sizeof(APTSectionHeaderExtraIO), offs.count, file );
	if ( offs.status != offs.count ) {
		cerr << "APTSectionHeaderExtraIO reading failed!" << "\n";
		return false;
	}
//cout << tmp_ifo << "\n";

	//consistence checks
	if ( tmp_ifo.llRecordCount != header.llIonCount ) { //assumes FIXED_SIZE, ONE_TO_ONE, FLOAT
		cerr << "The llRecordCount does not match with llIonCount!" << "\n";
		return false;
	}
	if ( tmp_ifo.llByteCount != (header.llIonCount * tmp_header.iRecordSize) ) {
		cerr << "The llByteCount does not match with llIonCount*iRecordSize!" << "\n";
		return false;
	}

	xyz_sect = APTSectionHeader( tmp_header, tmp_ifo );
	return true;
}


bool cameca_apt::read_cameca_apt_xyz_data()
{
	//the Position coordinates exclusively, at least currently APT v2, have an additionally preceeding header
	//this header encodes Xmin,Xmax,Ymin,Ymax,Zmin,Zmax of the points
	//MK::read this header first!
	size_t status = 0;

	apt_real extraheader[6];
	for( int i = 0; i < 6; i++ ) {
		extraheader[i] = 0.0;
	}
	status = fread( extraheader, sizeof(apt_real), 6, file ); //file pointer is advanced implicitly/automatically
	if ( status == 6 ) { //read success
		tipbox.xmi = extraheader[0];
		tipbox.xmx = extraheader[1];
		tipbox.ymi = extraheader[2];
		tipbox.ymx = extraheader[3];
		tipbox.zmi = extraheader[4];
		tipbox.zmx = extraheader[5];
		tipbox.scale();
	}
	else {
		cerr << "I/O failed in for reading extra header preceeding the Position coordinate values!" << "\n";
		return false;
	}
//cout << tipbox << "\n";

	size_t efirst = 0;
	size_t elast = static_cast<size_t>(header.llIonCount*3); //three elements/values per ion
	int sz = static_cast<int>(sizeof(apt_real)); //##MK::size_of is unsigned and < INT32MX in this case
	int nn = static_cast<int>(MPIIO_READ_CACHE) / sz;
	int mm = 3;
	int etrg = closestNumber( nn, mm ); //we de-interleave the elements into p3d so we need to have blocks integer divisible by 3 in this special for xyz
	size_t etarget = static_cast<size_t>(etrg); //MPIIO_READ_CACHE / sizeof(apt_real);
	if ( (etarget % static_cast<size_t>(3)) != 0 ) {
		cerr << "etarget is not integer divisible by 3!" << "\n";
		return false;
	}
	size_t ecurrent = 0;
	status = 0;
	apt_real* rbuf = NULL;
	size_t eold = 0;
	for ( size_t e = efirst; e < elast; 	) { //read block-by-block, number of elements, not total bytes
		//update actual read cache length
		if ( (e + etarget) < elast) {
			eold = ecurrent;
			ecurrent = etarget;
		}
		else {
			//##MK::divisible by 3
			eold = ecurrent;
			ecurrent = elast - e;
			if ( (ecurrent % static_cast<size_t>(3)) != 0 ) {
				cerr << "Remainder ecurrent is not integer divisible by 3!" << "\n";
				delete [] rbuf; rbuf = NULL; return false;
			}
			if ( ecurrent < 1 ) { //we are done because nobody there
				break;
			}
			if ( ecurrent > elast ) {
				cerr << "Failure, ecurrent wrap-around!" << "\n";
				delete [] rbuf; rbuf = NULL; return false;
			}
		}
cout << "e/eold/ecurrent/elast\t\t" << e << "\t\t" << eold << "\t\t" << ecurrent << "\t\t" << elast << "\n";
		//allocate a buffer if none existent, reallocate if required of different size
		if ( rbuf != NULL ) {
			if ( ecurrent != eold ) { //reallocate buffer of size ecurrent
				delete [] rbuf; rbuf = NULL;
				try {
					rbuf = new apt_real[ecurrent];
				}
				catch (bad_alloc &ioexc) {
					cerr << "Unable to reallocate rbuf in read_cameca_apt_xyz_data!" << "\n";
					return false;
				}
			}
			//nothing to do just (re)utilize the allocated buffer
		}
		else {
			//least likely case first time allocation
			try {
				rbuf = new apt_real[ecurrent];
			}
			catch (bad_alloc &ioexc) {
				cerr << "Unable to allocate rbuf in read_cameca_apt_xyz_data!" << "\n";
				return false;
			}
		}
		//by now buffer allocated or already out of subroutine
		status = 0;
		size_t here = ftell(file);
cout << "ftell " << here << "\n";
		status = fread( rbuf, sizeof(apt_real), ecurrent, file ); //file pointer is advanced implicitly/automatically
cout << "status " << status << "\n";
		if ( status == ecurrent ) { //read success
			//dump data//allocate new buffer to store results
			for( size_t j = 0; j < ecurrent;    ) {
				xyz.push_back( p3d( rbuf[j+0], rbuf[j+1], rbuf[j+2] ) );
				j += 3;
			}
//cout << "e/ecurrent/xyz.size()\t\t" << e << "\t\t" << ecurrent << "\t\t" << xyz.size() << "\t\t" << xyz.back() << "\n";
		} //block read of ecurrent successful
		else {
			cerr << "I/O failed in read_cameca_apt_xyz_data at element position " << e << "\n";
			delete [] rbuf; rbuf = NULL; return false;
		}
		e = e + ecurrent;
cout << "e\t\t" << e << "\n";
	} //next block
	delete [] rbuf; rbuf = NULL;
//cout << "xyz.size() " << xyz.size() << "\t\t" << xyz.back() << "\n";
	return true;
}
*/

unsigned int cameca_apt::read_section_header_autodetect()
{
	//read first section
cout << "Attempt reading APTSectionHeaderIO sizeof(APTSectionHeaderIO) " << sizeof(APTSectionHeaderIO) << "\n";
	//have we already reached the end of the file?
	long int implicit_fp_pos = ftell( file );
cout << "Implicit file pointer for fread stands at " << implicit_fp_pos << " Bytes from the beginning of the file" << "\n";
	if ( static_cast<size_t>(implicit_fp_pos) >= (filesize-1) ) {
		cout << "We have reached the end of the *.APT file!" << "\n";
		return CAMECA_APT_KWNSECT_UNKNOWN;
	}

	APTSectionHeaderIO tmp_header = APTSectionHeaderIO();
	offs.count = 1;
	offs.status = 0;
	offs.status = fread( &tmp_header, sizeof(APTSectionHeaderIO), offs.count, file );
	if ( offs.status != offs.count ) {
		cerr  << "APTSectionHeaderIO reading failed!" << "\n"; return CAMECA_APT_KWNSECT_FAILURE;
	}
//cout << tmp_header << "\n";

	//consistence checks
	if ( tmp_header.cSignature[0] != 0x53 || tmp_header.cSignature[1] != 0x45 || tmp_header.cSignature[2] != 0x43 || tmp_header.cSignature[3] != 0x00 ) {
		cerr << "The cSignature of tmp_header indicates an invalid section in the *.APT file!" << "\n";
		return CAMECA_APT_KWNSECT_FAILURE;
	}
	if ( tmp_header.iHeaderVersion != 2 ) {
		cerr << "The iHeaderVersion tmp_header indicates the section version is unexpected!" << "\n";
		return CAMECA_APT_KWNSECT_FAILURE;
	}
	//which section is this?
	unsigned int keyword = CAMECA_APT_KWNSECT_UNKNOWN;
	for( map<unsigned int, cameca_apt_section>::iterator it = known_sections.begin(); it != known_sections.end(); it++ ) {
		bool this_section = true;
		for( int i = 0; i < CAMECA_APT_WCSECTIONTYP_LEN; i++ ) {
			if ( tmp_header.wcSectionType[i] == it->second.wcSectionType[i] ) {
				continue;
			}
			else {
				this_section = false;
				break; //it cannot be known_section it
			}
		}
		if ( this_section == true ) { //we found it
			keyword = it->first;
			//this is the trick, we have identified which section it is
		}
	}
	if ( keyword == CAMECA_APT_KWNSECT_UNKNOWN ) {
		cerr << "The wcSectionType is encoding an unknown section!" << "\n";
		for( int j = 0; j < CAMECA_APT_WCSECTIONTYP_LEN; j++ ) {
			stringstream ss;
			ss << std::hex << int(tmp_header.wcSectionType[j]);
			string res ( ss.str() );
			cerr << "\t\ttmp_header.wcSectionType[" << j << "] __" << res << "__" << "\n";
		}
		return CAMECA_APT_KWNSECT_UNKNOWN;
	}
	//has the section not yet been read?
	map<unsigned int, APTSectionHeader>::iterator cand = idtfyd_sections.find( keyword );
	if ( cand != idtfyd_sections.end() ) {
		cerr << "We hit a section that seems to be a duplicate or has already been read!" << "\n";
		return CAMECA_APT_KWNSECT_FAILURE;
	}
	if ( tmp_header.eRelationshipType != ONE_TO_ONE ) {
		cerr << "The eRelationshipType tmp_header is not ONE_TO_ONE but that is the only currently supported!" << "\n";
		return CAMECA_APT_KWNSECT_FAILURE;
	}
	if ( tmp_header.eRecordType != FIXED_SIZE ) {
		cerr << "The eRecordType tmp_header is not FIXED_SIZE but that is the only currently supported!" << "\n";
		return CAMECA_APT_KWNSECT_FAILURE;
	}
	//so far all eRelationshipType == ONE_TO_ONE and eRecordType == FIXED_SIZE

cout << "Attempt reading APTSectionHeaderExtraIO sizeof(APTSectionHeaderExtraIO) " << sizeof(APTSectionHeaderExtraIO) << "\n";
	APTSectionHeaderExtraIO tmp_ifo = APTSectionHeaderExtraIO();
	offs.count = 1;
	offs.status = 0;
	offs.status = fread( &tmp_ifo, sizeof(APTSectionHeaderExtraIO), offs.count, file );
	if ( offs.status != offs.count ) {
		cerr << "APTSectionHeaderExtraIO reading failed!" << "\n";
		return CAMECA_APT_KWNSECT_FAILURE;
	}
//cout << tmp_ifo << "\n";

	//consistence checks
	if ( tmp_ifo.llRecordCount != header.llIonCount ) { //assumes FIXED_SIZE, ONE_TO_ONE
		cerr << "The llRecordCount does not match with llIonCount!" << "\n";
		return CAMECA_APT_KWNSECT_FAILURE;
	}
	if ( tmp_ifo.llByteCount != (header.llIonCount * tmp_header.iRecordSize) ) { //##MK::the *.APT format specification details that ByteCount > llIonCount*iRecordSize because of padding this is really poor
		cerr << "The llByteCount does not match with llIonCount*iRecordSize!" << "\n";
		//##MK::currently we do not allow for padding!
		return CAMECA_APT_KWNSECT_FAILURE;
	}

cout << "Identified APTSection " << keyword << "\n";
	idtfyd_sections.insert( pair<unsigned int, APTSectionHeader>( keyword, APTSectionHeader( tmp_header, tmp_ifo ) ) );
	return keyword;
}


template <class T> bool cameca_apt::read_section_data_fixed_onetoone( unsigned int const & key )
{
	//special handling of header for CAMECA_APT_KWNSECT_POSITION
	if ( key == CAMECA_APT_KWNSECT_POSITION ) {
		size_t status = 0;
		apt_real extraheader[CAMECA_APT_KWNSECT_POSITION_EXTRALEN];
		for( int i = 0; i < CAMECA_APT_KWNSECT_POSITION_EXTRALEN; i++ ) {
			extraheader[i] = 0.0;
		}
		status = fread( extraheader, sizeof(apt_real), CAMECA_APT_KWNSECT_POSITION_EXTRALEN, file ); //file pointer is advanced implicitly/automatically
		if ( status == CAMECA_APT_KWNSECT_POSITION_EXTRALEN ) { //read success
			tipbox.xmi = extraheader[0];	tipbox.xmx = extraheader[1];
			tipbox.ymi = extraheader[2];	tipbox.ymx = extraheader[3];
			tipbox.zmi = extraheader[4];	tipbox.zmx = extraheader[5];
			tipbox.scale();
		}
		else {
			cerr << "I/O failed for CAMECA_APT_KWNSECT_POSITION while reading preceeding extra header!" << "\n"; return false;
		}
cout << "Tipbox " << "\n" << tipbox << "\n";
	}


	size_t faulty = 0; //MK::read successively blocks of C instead of all at once,
	//benefit is that such later rawdata can be processes blockwise and deleted successively reducing
	//the total memory footprint of the application for reconstruction by not more than though factor 2

	int mm = idtfyd_sections[key].iRecordSize / (idtfyd_sections[key].iDataTypeSize/8);
	size_t efirst = 0;
	size_t elast = static_cast<size_t>(header.llIonCount*mm);
	//size_t ecnt = idtfyd_sections[key].iRecordSize / (idtfyd_sections[key].iDataTypeSize/8);

	//prepare etarget element I/O read cache per read
	int sz = static_cast<int>(sizeof(T)); //##MK::size_of T is unsigned and < INT32MX in this case
	int nn = static_cast<int>(MPIIO_READ_CACHE) / sz;
	int etrg = closestNumber( nn, mm ); //we read interleaved data with tuples of mm elements per ion, so the blocks need to be integer divisible by mm
	size_t etarget = static_cast<size_t>(etrg); //MPIIO_READ_CACHE / sizeof(apt_real);
	if ( (etarget % static_cast<size_t>(mm)) != 0 ) {
		cerr << "etrg " << etrg << " is not integer divisible by mm = " << mm << " !" << "\n"; return false;
	}
	else {
		cout << "etrg " << etrg << " is integer divisible by mm = " << mm << "\n";
	}

//cout << "ecnt " << ecnt << " sizeof(T) " << sizeof(T) << "\n";
//	size_t etarget = MPIIO_READ_CACHE / (sizeof(T)*ecnt); //ifo.iRecordSize/(ifo.iDataTypeSize/8));

	size_t ecurrent = 0;
	size_t status = 0;
	T* rbuf = NULL;
	size_t eold = 0;
	for ( size_t e = efirst; e < elast; 	) { //read block-by-block, number of elements, not total bytes
		//update actual read cache length
//cout << "e/eold/ecurrent/elast\t\t" << e << "\t\t" << eold << "\t\t" << ecurrent << "\t\t" << elast << "\n";
		if ( (e + etarget) < elast) {
			eold = ecurrent;
			ecurrent = etarget;
		}
		else {
			eold = ecurrent;
			ecurrent = elast - e;
			if ( (ecurrent % static_cast<size_t>(mm)) != 0 ) {
				cerr << "Remainder ecurrent is not integer divisible by mm = " << mm  << " !" << "\n";
				delete [] rbuf; rbuf = NULL; return false;
			}
			if ( ecurrent < 1 ) { //we are done because nobody there
				break;
			}
			if ( ecurrent > elast ) {
				cerr << "Failure, ecurrent wrap-around in read_section_data_fixed_onetoone!" << "\n";
				delete [] rbuf; rbuf = NULL; return false;
			}
		}
		//allocate a buffer if none existent, reallocate if required of different size
		if ( rbuf != NULL ) {
			if ( ecurrent != eold ) { //reallocate buffer of size ecurrent
				delete [] rbuf; rbuf = NULL;
				try {
					rbuf = new T[ecurrent];
				}
				catch (bad_alloc &ioexc) {
					cerr << "Unable to reallocate rbuf in read_section_data_fixed_onetoone!" << "\n"; return false;
				}
			}
			//nothing to do just (re)utilize the allocated buffer
		}
		else {
			//least likely case first time allocation
			try {
				rbuf = new T[ecurrent];
			}
			catch (bad_alloc &ioexc) {
				cerr << "Unable to allocate rbuf in read_section_data_fixed_onetoone!" << "\n"; return false;
			}
		}
		//by now buffer allocated or already out of subroutine
		status = 0;
		status = fread( rbuf, sizeof(T), ecurrent, file ); //file pointer is advanced implicitly/automatically
//cout << "status " << status << "\n";
		if ( status == ecurrent ) { //read success
			//dump data//allocate new buffer to store results
			switch( key )
			{
				case CAMECA_APT_KWNSECT_TOF:
					for( size_t j = 0; j < ecurrent; j++ ) { tof.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_PULSE:
					for( size_t j = 0; j < ecurrent; j++ ) { pulse.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_FREQ:
					for( size_t j = 0; j < ecurrent; j++ ) { freq.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_TELAPSED:
					for( size_t j = 0; j < ecurrent; j++ ) { tElapsed.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_ERATE:
					for( size_t j = 0; j < ecurrent; j++ ) { erate.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_TSTAGE:
					for( size_t j = 0; j < ecurrent; j++ ) { tstage.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_TARGETERATE:
					for( size_t j = 0; j < ecurrent; j++ ) { TargetErate.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_TARGETFLUX:
					for( size_t j = 0; j < ecurrent; j++ ) { TargetFlux.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_PULSEDELTA:
					for( size_t j = 0; j < ecurrent; j++ ) { pulseDelta.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_PRES:
					for( size_t j = 0; j < ecurrent; j++ ) { Pres.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_VANODEMON:
					for( size_t j = 0; j < ecurrent; j++ ) { VAnodeMon.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_TEMP:
					for( size_t j = 0; j < ecurrent; j++ ) { Temp.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_AMBTEMP:
					for( size_t j = 0; j < ecurrent; j++ ) { AmbTemp.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_FRACTUREGRD:
					for( size_t j = 0; j < ecurrent; j++ ) { FractureGuard.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_VREF:
					for( size_t j = 0; j < ecurrent; j++ ) { Vref.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_NOISE:
					for( size_t j = 0; j < ecurrent; j++ ) { Noise.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_UNIFRMITY:
					for( size_t j = 0; j < ecurrent; j++ ) { Uniformity.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_XSTAGE:
					for( size_t j = 0; j < ecurrent; j++ ) { xstage.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_YSTAGE:
					for( size_t j = 0; j < ecurrent; j++ ) { ystage.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_ZSTAGE:
					for( size_t j = 0; j < ecurrent; j++ ) { zstage.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_Z:
					for( size_t j = 0; j < ecurrent; j++ ) { z.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_TOFC:
					for( size_t j = 0; j < ecurrent; j++ ) { tofc.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_MASS:
					for( size_t j = 0; j < ecurrent; j++ ) { Mass.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_TOFB:
					for( size_t j = 0; j < ecurrent; j++ ) { tofb.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_XS:
					for( size_t j = 0; j < ecurrent; j++ ) { xs.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_YS:
					for( size_t j = 0; j < ecurrent; j++ ) { ys.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_ZS:
					for( size_t j = 0; j < ecurrent; j++ ) { zs.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_RTIP:
					for( size_t j = 0; j < ecurrent; j++ ) { rTip.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_ZAPEX:
					for( size_t j = 0; j < ecurrent; j++ ) { zApex.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_ZSPHCORR:
					for( size_t j = 0; j < ecurrent; j++ ) { zSphereCorr.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_XDET:
					for( size_t j = 0; j < ecurrent; j++ ) { XDet_mm.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_YDET:
					for( size_t j = 0; j < ecurrent; j++ ) { YDet_mm.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_MULT:
					for( size_t j = 0; j < ecurrent; j++ ) { Multiplicity.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_VAP:
					for( size_t j = 0; j < ecurrent; j++ ) { Vap.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_DETCORR:
					for( size_t j = 0; j < ecurrent; j++ ) { DetectorCoordinates.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_POSITION:
					for( size_t j = 0; j < ecurrent; j++ ) { Position.push_back( rbuf[j] ); } break;
				default:
					break;
			}
//cout << "e/ecurrent/mq.size()\t\t" << e << "\t\t" << ecurrent << "\t\t" << mq.size() << "\t\t" << mq.back() << "\n";
		} //block read of ecurrent successful
		else {
			cerr << "I/O failed in read_section_data_fixed_onetoone_float at element position " << e << "\n";
			delete [] rbuf; rbuf = NULL; return false;
		}
		e = e + ecurrent;
	} //next block
	delete [] rbuf; rbuf = NULL;

	switch( key )
	{
		case CAMECA_APT_KWNSECT_TOF:
			cout << "tof.size() " << tof.size() << "\t\t" << tof.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_PULSE:
			cout << "pulse.size() " << pulse.size() << "\t\t" << pulse.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_FREQ:
			cout << "freq.size() " << freq.size() << "\t\t" << freq.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_TELAPSED:
			cout << "tElapsed.size() " << tElapsed.size() << "\t\t" << tElapsed.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_ERATE:
			cout << "erate.size() " << erate.size() << "\t\t" << erate.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_TSTAGE:
			cout << "tstage.size() " << tstage.size() << "\t\t" << tstage.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_TARGETERATE:
			cout << "TargetErate.size() " << TargetErate.size() << "\t\t" << TargetErate.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_TARGETFLUX:
			cout << "TargetFlux.size() " << TargetFlux.size() << "\t\t" << TargetFlux.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_PULSEDELTA:
			cout << "pulseDelta.size() " << pulseDelta.size() << "\t\t" << pulseDelta.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_PRES:
			cout << "Pres.size() " << Pres.size() << "\t\t" << Pres.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_VANODEMON:
			cout << "VAnodeMon.size() " << VAnodeMon.size() << "\t\t" << VAnodeMon.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_TEMP:
			cout << "Temp.size() " << Temp.size() << "\t\t" << Temp.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_AMBTEMP:
			cout << "AmbTemp.size() " << AmbTemp.size() << "\t\t" << AmbTemp.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_FRACTUREGRD:
			cout << "FractureGuard.size() " << FractureGuard.size() << "\t\t" << FractureGuard.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_VREF:
			cout << "Vref.size() " << Vref.size() << "\t\t" << Vref.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_NOISE:
			cout << "Noise.size() " << Noise.size() << "\t\t" << Noise.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_UNIFRMITY:
			cout << "Uniformity.size() " << Uniformity.size() << "\t\t" << Uniformity.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_XSTAGE:
			cout << "xstage.size() " << xstage.size() << "\t\t" << xstage.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_YSTAGE:
			cout << "ystage.size() " << ystage.size() << "\t\t" << ystage.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_ZSTAGE:
			cout << "zstage.size() " << zstage.size() << "\t\t" << zstage.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_Z:
			cout << "z.size() " << z.size() << "\t\t" << z.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_TOFC:
			cout << "tofc.size() " << tofc.size() << "\t\t" << tofc.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_MASS:
			cout << "Mass.size() " << Mass.size() << "\t\t" << Mass.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_TOFB:
			cout << "tofb.size() " << tofb.size() << "\t\t" << tofb.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_XS:
			cout << "xs.size() " << xs.size() << "\t\t" << xs.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_YS:
			cout << "ys.size() " << ys.size() << "\t\t" << ys.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_ZS:
			cout << "zs.size() " << zs.size() << "\t\t" << zs.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_RTIP:
			cout << "rTip.size() " << rTip.size() << "\t\t" << rTip.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_ZAPEX:
			cout << "zApex.size() " << zApex.size() << "\t\t" << zApex.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_ZSPHCORR:
			cout << "zSphereCorr.size() " << zSphereCorr.size() << "\t\t" << zSphereCorr.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_XDET:
			cout << "XDet_mm.size() " << XDet_mm.size() << "\t\t" << XDet_mm.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_YDET:
			cout << "YDet_mm.size() " << YDet_mm.size() << "\t\t" << YDet_mm.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_MULT:
			cout << "Multiplicity.size() " << Multiplicity.size() << "\t\t" << Multiplicity.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_VAP:
			cout << "Vap.size() " << Vap.size() << "\t\t" << Vap.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_DETCORR:
			cout << "DetectorCoordinates.size() " << DetectorCoordinates.size() << "\t\t" << DetectorCoordinates.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_POSITION:
			cout << "Position.size() " << Position.size() << "\t\t" << Position.back() << "\n"; break;
		default:
			break;
	}
	return true;
}

/*
bool cameca_apt::read_section_data_fixed_onetoone_float( unsigned int const & key )
{
	//#####special handling of header for positions!


	size_t faulty = 0; //MK::read successively blocks of C instead of all at once,
	//benefit is that such later rawdata can be processes blockwise and deleted successively reducing
	//the total memory footprint of the application for reconstruction by not more than though factor 2
	size_t efirst = 0;
	size_t elast = static_cast<size_t>(header.llIonCount);
	size_t ecnt = idtfyd_sections[key].iRecordSize / (idtfyd_sections[key].iDataTypeSize/8);
cout << "ecnt " << ecnt << "\n";
	size_t etarget = MPIIO_READ_CACHE / (sizeof(apt_real)*ecnt); //ifo.iRecordSize/(ifo.iDataTypeSize/8));

	size_t ecurrent = 0;
	size_t status = 0;
	apt_real* rbuf = NULL;
	size_t eold = 0;
	for ( size_t e = efirst; e < elast; 	) { //read block-by-block, number of elements, not total bytes
		//update actual read cache length
//cout << "e/eold/ecurrent/elast\t\t" << e << "\t\t" << eold << "\t\t" << ecurrent << "\t\t" << elast << "\n";
		if ( (e + etarget) < elast) {
			eold = ecurrent;
			ecurrent = etarget;
		}
		else {
			eold = ecurrent;
			ecurrent = elast - e;
			if ( ecurrent < 1 ) { //we are done because nobody there
				break;
			}
			if ( ecurrent > elast ) {
				cerr << "Failure, ecurrent wrap-around in read_section_data_fixed_onetoone_float!" << "\n";
				delete [] rbuf; rbuf = NULL; return false;
			}
		}
		//allocate a buffer if none existent, reallocate if required of different size
		if ( rbuf != NULL ) {
			if ( ecurrent != eold ) { //reallocate buffer of size ecurrent
				delete [] rbuf; rbuf = NULL;
				try {
					rbuf = new apt_real[ecurrent];
				}
				catch (bad_alloc &ioexc) {
					cerr << "Unable to reallocate rbuf in read_section_data_fixed_onetoone_float!" << "\n"; return false;
				}
			}
			//nothing to do just (re)utilize the allocated buffer
		}
		else {
			//least likely case first time allocation
			try {
				rbuf = new apt_real[ecurrent];
			}
			catch (bad_alloc &ioexc) {
				cerr << "Unable to allocate rbuf in read_section_data_fixed_onetoone_float!" << "\n"; return false;
			}
		}
		//by now buffer allocated or already out of subroutine
		status = 0;
		status = fread( rbuf, sizeof(apt_real), ecurrent, file ); //file pointer is advanced implicitly/automatically
//cout << "status " << status << "\n";
		if ( status == ecurrent ) { //read success
			//dump data//allocate new buffer to store results
			switch( key )
			{
				case CAMECA_APT_KWNSECT_TOF:
					for( size_t j = 0; j < ecurrent; j++ ) { tof.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_PULSE:
					for( size_t j = 0; j < ecurrent; j++ ) { pulse.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_FREQ:
					for( size_t j = 0; j < ecurrent; j++ ) { freq.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_TELAPSED:
					for( size_t j = 0; j < ecurrent; j++ ) { tElapsed.push_back( rbuf[j] ); } break;
				case CAMECA_APT_KWNSECT_ERATE:
					for( size_t j = 0; j < ecurrent; j++ ) { erate.push_back( rbuf[j] ); } break;
				default:
					break;
			}
//cout << "e/ecurrent/mq.size()\t\t" << e << "\t\t" << ecurrent << "\t\t" << mq.size() << "\t\t" << mq.back() << "\n";
		} //block read of ecurrent successful
		else {
			cerr << "I/O failed in read_section_data_fixed_onetoone_float at element position " << e << "\n";
			delete [] rbuf; rbuf = NULL; return false;
		}
		e = e + ecurrent;
	} //next block
	delete [] rbuf; rbuf = NULL;

	switch( key )
	{
		case CAMECA_APT_KWNSECT_TOF:
			cout << "tof.size() " << tof.size() << "\t\t" << tof.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_PULSE:
			cout << "pulse.size() " << pulse.size() << "\t\t" << pulse.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_FREQ:
			cout << "freq.size() " << freq.size() << "\t\t" << freq.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_TELAPSED:
			cout << "tElapsed.size() " << tElapsed.size() << "\t\t" << tElapsed.back() << "\n"; break;
		case CAMECA_APT_KWNSECT_ERATE:
			cout << "erate.size() " << erate.size() << "\t\t" << erate.back() << "\n"; break;
		default:
			break;
	}
	return true;
}


bool cameca_apt::read_section_data_fixed_onetoone_uint16(  unsigned int const & key )
{
	//####################
	return true;
}


bool cameca_apt::read_section_data_fixed_onetoone_double(  unsigned int const & key )
{
	//####################
	return true;
}
*/



bool cameca_apt::read_aptv2_autodetect( const string fn )
{
	offs = offsifo();
	offs.count = 0; // map binary representations one-on-one into C++ structs specific for the format
	offs.status = 0;
	filename = fn;
	file = fopen( filename.c_str(), "rb" );
	if ( file == NULL ) {
		cerr << "Opening APT file " << fn << " failed!" << "\n"; return false;
	}
	else {
		cout << "Opening *.APT file on " << file << "\n";
	}

	if ( read_cameca_apt_file_header() == false ) {
		cerr << "Reading APSuite6/IVAS4 *.APT file header failed!" << "\n"; return false;
	}
cout << header << "\n";

	//as of 2020/03/05 an APSuite6/IVAS4 *.APT file has an adaptive arrangement of the sections
	//within APSuite6/IVAS4 these sections are referred to as APT branches
	for( auto it = known_sections.begin(); it != known_sections.end(); it++ ) {
		//the idea of this adaptive function is as follows: obviously an *.APT can contain at most all possible branches
		//but the *.APT format does not guarantee that these are stored in a particular order
		//so we probe which of the known sections we find, if we find a section we read its data

		//the trick is that we analyze for each section that we get from the file whether it is the desired
		unsigned int sectionid = read_section_header_autodetect();
		switch( sectionid )
		{
			case CAMECA_APT_KWNSECT_FAILURE:
				cerr << "Autodetect the " << it->first << " APT header failed!" << "\n"; return false;
			case CAMECA_APT_KWNSECT_TOF:
				cout << "Autodetected section CAMECA_APT_KWNSECT_TOF" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_TOF ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_TOF data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_PULSE:
				cout << "Autodetected section CAMECA_APT_KWNSECT_PULSE" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_PULSE ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_PULSE data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_FREQ:
				cout << "Autodetected section CAMECA_APT_KWNSECT_FREQ" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_FREQ ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_FREQ data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_TELAPSED:
				cout << "Autodetected section CAMECA_APT_KWNSECT_TELAPSED" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_TELAPSED ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_TELAPSED data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_ERATE:
				cout << "Autodetected section CAMECA_APT_KWNSECT_ERATE" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_ERATE ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_ERATE data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_TSTAGE:
				cout << "Autodetected section CAMECA_APT_KWNSECT_TSTAGE" << "\n";
				if ( read_section_data_fixed_onetoone<unsigned short>( CAMECA_APT_KWNSECT_TSTAGE ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_TSTAGE data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_TARGETERATE:
				cout << "Autodetected section CAMECA_APT_KWNSECT_TARGETERATE" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_TARGETERATE ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_TARGETERATE data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_TARGETFLUX:
				cout << "Autodetected section CAMECA_APT_KWNSECT_TARGETFLUX" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_TARGETFLUX ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_TARGETFLUX data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_PULSEDELTA:
				cout << "Autodetected section CAMECA_APT_KWNSECT_PULSEDELTA" << "\n";
				if ( read_section_data_fixed_onetoone<short>( CAMECA_APT_KWNSECT_PULSEDELTA ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_PULSEDELTA data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_PRES:
				cout << "Autodetected section CAMECA_APT_KWNSECT_PRES" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_PRES ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_PRES data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_VANODEMON:
				cout << "Autodetected section CAMECA_APT_KWNSECT_VANODEMON" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_VANODEMON ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_VANODEMON data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_TEMP:
				cout << "Autodetected section CAMECA_APT_KWNSECT_TEMP" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_TEMP ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_TEMP data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_AMBTEMP:
				cout << "Autodetected section CAMECA_APT_KWNSECT_AMBTEMP" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_AMBTEMP ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_AMBTEMP data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_FRACTUREGRD:
				cout << "Autodetected section CAMECA_APT_KWNSECT_FRACTUREGRD" << "\n";
				if ( read_section_data_fixed_onetoone<unsigned short>( CAMECA_APT_KWNSECT_FRACTUREGRD ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_FRACTUREGRD data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_VREF:
				cout << "Autodetected section CAMECA_APT_KWNSECT_VREF" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_VREF ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_VREF data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_NOISE:
				cout << "Autodetected section CAMECA_APT_KWNSECT_NOISE" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_NOISE ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_NOISE data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_UNIFRMITY:
				cout << "Autodetected section CAMECA_APT_KWNSECT_UNIFRMITY" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_UNIFRMITY ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_UNIFRMITY data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_XSTAGE:
				cout << "Autodetected section CAMECA_APT_KWNSECT_XSTAGE" << "\n";
				if ( read_section_data_fixed_onetoone<int>( CAMECA_APT_KWNSECT_XSTAGE ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_XSTAGE data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_YSTAGE:
				cout << "Autodetected section CAMECA_APT_KWNSECT_YSTAGE" << "\n";
				if ( read_section_data_fixed_onetoone<int>( CAMECA_APT_KWNSECT_YSTAGE ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_YSTAGE data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_ZSTAGE:
				cout << "Autodetected section CAMECA_APT_KWNSECT_ZSTAGE" << "\n";
				if ( read_section_data_fixed_onetoone<int>( CAMECA_APT_KWNSECT_ZSTAGE ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_ZSTAGE data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_Z:
				cout << "Autodetected section CAMECA_APT_KWNSECT_Z" << "\n";
				if ( read_section_data_fixed_onetoone<int64_t>( CAMECA_APT_KWNSECT_Z ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_Z data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_TOFC:
				cout << "Autodetected section CAMECA_APT_KWNSECT_TOFC" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_TOFC ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_TOFC data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_MASS:
				cout << "Autodetected section CAMECA_APT_KWNSECT_MASS" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_MASS ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_MASS data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_TOFB:
				cout << "Autodetected section CAMECA_APT_KWNSECT_TOFB" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_TOFB ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_TOFB data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_XS:
				cout << "Autodetected section CAMECA_APT_KWNSECT_XS" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_XS ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_XS data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_YS:
				cout << "Autodetected section CAMECA_APT_KWNSECT_YS" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_YS ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_YS data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_ZS:
				cout << "Autodetected section CAMECA_APT_KWNSECT_ZS" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_ZS ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_ZS data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_RTIP:
				cout << "Autodetected section CAMECA_APT_KWNSECT_RTIP" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_RTIP ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_RTIP data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_ZAPEX:
				cout << "Autodetected section CAMECA_APT_KWNSECT_ZAPEX" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_ZAPEX ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_ZAPEX data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_ZSPHCORR:
				cout << "Autodetected section CAMECA_APT_KWNSECT_ZSPHCORR" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_ZSPHCORR ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_ZSPHCORR data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_XDET:
				cout << "Autodetected section CAMECA_APT_KWNSECT_XDET" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_XDET ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_XDET data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_YDET:
				cout << "Autodetected section CAMECA_APT_KWNSECT_YDET" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_YDET ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_YDET data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_MULT:
				cout << "Autodetected section CAMECA_APT_KWNSECT_MULT" << "\n";
				if ( read_section_data_fixed_onetoone<int>( CAMECA_APT_KWNSECT_MULT ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_MULT data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_VAP:
				cout << "Autodetected section CAMECA_APT_KWNSECT_VAP" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_VAP ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_VAP data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_DETCORR:
				cout << "Autodetected section CAMECA_APT_KWNSECT_DETCORR" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_DETCORR ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_DETCORR data failed!" << "\n"; return false;
				} else { break; }
			case CAMECA_APT_KWNSECT_POSITION:
				cout << "Autodetected section CAMECA_APT_KWNSECT_POSITION" << "\n";
				if ( read_section_data_fixed_onetoone<apt_real>( CAMECA_APT_KWNSECT_POSITION ) == false ) {
					cerr << "Reading CAMECA_APT_KWNSECT_POSITION data failed!" << "\n"; return false;
				} else { break; }
			default: //CAMECA_APT_KWNSECT_UNKNOWN
				cout << "Stop reading sections!" << "\n";
				return true;
		}
	}

	return true;
}


/*
bool cameca_apt::read_aptv2( const string fn )
{
	offs = offsifo();
	offs.count = 0; // map binary representations one-on-one into C++ structs specific for the format
	offs.status = 0;
	filename = fn;
	file = fopen( filename.c_str(), "rb" );
	if ( file == NULL ) {
		cerr << "Opening APT file " << fn << " failed!" << "\n";
		return false;
	}

	if ( read_cameca_apt_file_header() == false ) {
		cerr << "Reading APSuite6/IVAS4 *.APT file header failed!" << "\n";
		fclose(file); file = NULL; return false;
	}
cout << header << "\n";

	//in what follows to order of executing the read commands matters
	//MK::the code has two layers in this function we detail the sections and data
	//in the lower layer, i.e. the functions called we have the actual I/O calls

	//as of 2020/03/05 an APSuite6/IVAS4 *.APT file has as the first section the "Mass to charge"
	if ( read_cameca_apt_mass_header() == false ) {  //##MK::make a generic mass reader
		cerr << "Reading APSuite6/IVAS4 *.APT section header with for Mass failed!" << "\n";
		fclose(file); file = NULL; return false;
	}
cout << mass_sect << "\n";

	if ( read_cameca_apt_mass_data() == false ) {
		cerr << "Reading APSuite6/IVAS4 *.APT mass-to-charge failed!" << "\n";
		fclose(file); file = NULL; return false;
	}
cout << "mq.size() " << mq.size() << "\n";

	//as of 2020/03/05 an APSuite6/IVAS4 *.APT file has as the second section the "Positions"
	//MK::currently this section only has an extra section telling the extend
	if ( read_cameca_apt_xyz_header() == false ) {
		cerr << "Reading APSuite6/IVAS4 *.APT section header with for Position failed!" << "\n";
		fclose(file); file = NULL; return false;
	}
cout << xyz_sect << "\n";

	if ( read_cameca_apt_xyz_data() == false ) {
		cerr << "Reading APSuite6/IVAS4 *.APT extend and xyz failed!" << "\n";
		fclose(file); file = NULL; return false;
	}

cout << "tipbox " << tipbox << "\n";
cout << "xyz.size() " << xyz.size() << "\n";


	//fish the next bytes
	unsigned char fishuc[FISHING]; //zero-initialized by default
	cout << "Start fishing unsigned char's " << "\n";
	//for( int ii = 0; ii < FISHING; ii++ ) {
	//	cout << "fish[" << ii << "] __" << int(fish[ii]) << "__" << "\n";
	//}
	offs.count = 1;
	offs.status = 0;
	offs.status = fread( fishuc, sizeof(unsigned char)*FISHING, offs.count, file );
	if ( offs.status != offs.count ) {
		cerr << "APTSectionHeaderExtra reading failed!" << "\n"; return false;
	}

	for( int ii = 0; ii < FISHING; ii++ ) {
		//cout << "fish[" << ii << "] __" << int(fishuc[ii]) << "__" << "\n";
		stringstream ss;
		ss << std::hex << int(fishuc[ii]); // int decimal_value generating hex values
		string res ( ss.str() );
		cout << "fish[" << ii << "] __" << res << "__" << "\n";
	}

	return true;
}

bool cameca_apt::crack_aptv2( const string fn )
{
	offs = offsifo();
	offs.count = 0; // map binary representations one-on-one into C++ structs specific for the format
	offs.status = 0;
	filename = fn;
	file = fopen( filename.c_str(), "rb" );
	if ( file == NULL ) {
		cerr << "Opening APT file " << fn << " failed!" << "\n";
		return false;
	}

	//skip
	long int ni = 7907772;
	//filehead,tof,pulse
	//long int off = 540 + 148 + 4*ni + 148 + 4*ni;
	long int off = ConfigTranscoder::DebugSkip;
	offs.status = fseek ( file , off , SEEK_SET );
	if ( offs.status != 0 ) {
		cerr << "fseek failed!" << "\n"; return false;
	}

	//fish the next bytes
	unsigned char fishuc[FISHING]; //zero-initialized by default
	cout << "Start fishing unsigned char's " << "\n";
	//for( int ii = 0; ii < FISHING; ii++ ) {
	//	cout << "fish[" << ii << "] __" << int(fish[ii]) << "__" << "\n";
	//}
	offs.count = 1;
	offs.status = 0;
	offs.status = fread( fishuc, sizeof(unsigned char)*FISHING, offs.count, file );
	if ( offs.status != offs.count ) {
		cerr << "APTSectionHeaderExtra reading failed!" << "\n"; return false;
	}

	for( int ii = 0; ii < FISHING; ii++ ) {
		//cout << "fish[" << ii << "] __" << int(fishuc[ii]) << "__" << "\n";
		stringstream ss;
		ss << std::hex << int(fishuc[ii]); // int decimal_value generating hex values
		string res ( ss.str() );
		cout << "fish[" << ii << "] __" << res << "__" << "\n";
	}

	return true;
}
*/


/*
//endi swop
struct be2le {
	unsigned int k;
};

struct swop {
	unsigned int i;

	inline unsigned int bswap_uint_2_uint( unsigned int ui )
	{
		unsigned int retVal = 0; //keep it deterministic
		char *pVal = (char*) &ui;
		char* pRetVal = (char*) &retVal;
		for (int i=0; i<4; ++i) {
			pRetVal[4-1-i] = pVal[i];
		}
		return retVal;
	}

	swop() : i(0) {}
	swop(const struct be2le in)
	{ //upon storing the detector event within the PARAPROBE data structure we pass the bigendian values and change endianness on the fly
		i = bswap_uint_2_uint(in.k);
	}
	swop( const unsigned int _i ) : i(_i) {}
};
*/


//big to little uint32 ?
/*
struct be2le* rbuf = NULL;
try {
	rbuf = new struct be2le[3];
}
catch (bad_alloc &ioexc) {
	cerr << "Unable to allocate rbuf for swop structs in cameca_apt_seq read" << "\n";
	fclose(file);
	return APTFileHeader();
}
size_t test = 0;
test = fread( rbuf, sizeof(be2le), 3, file );
cout << "Swop" << "\n";
for( int ii = 0; ii < 3; ii++) {
	swop j = swop( rbuf[ii] );
	cout << ii << " __" << j.i << "__" << "UINT32MX - j.i __" << (static_cast<unsigned int>(UINT32MX) - j.i) << "__" << "\n";
}
*/

/*
//int64_t
int fish[3];
size_t test = 0;
test = fread( fish, sizeof(int), 3, file);
for( int ii = 0; ii < 3; ii++ ){
	cout << "fish[" << ii << "] __" << fish[ii] << "__" << "\n";
}

//uint64_t
uint64_t fish[2];
size_t test = 0;
test = fread( fish, sizeof(uint64_t), 2, file);
for( int ii = 0; ii < 2; ii++ ){
	cout << "fish[" << ii << "] __" << fish[ii] << "__" << "\n";
}

unsigned char skip[4];
status = fread( skip, sizeof(unsigned char)*4, 1, file );

unsigned int nions = 0;
status = fread( &nions, sizeof(unsigned int), 1, file );
cout << "nions " << nions << "\n";
*/
