//##MK::GPLV3

#include "CONFIG_Transcoder.h"


E_TRANSCODER_MODE ConfigTranscoder::TranscodingMode = E_TRANSCODE_NOTHING;
string ConfigTranscoder::Inputfile = "";

//magic
size_t ConfigTranscoder::DebugSkip = 0;


bool ConfigTranscoder::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.Input.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigTranscoder")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;

	//WHAT TYPE OF ANALYSIS TO DO
	mode = read_xml_attribute_uint32( rootNode, "TranscodingMode" );
	switch (mode)
	{
		case E_TRANSCODE_POS_TO_APTH5:
			TranscodingMode = E_TRANSCODE_POS_TO_APTH5; break;
		case E_TRANSCODE_EPOS_TO_APTH5:
			TranscodingMode = E_TRANSCODE_EPOS_TO_APTH5; break;
		case E_TRANSCODE_CAMECA_APT_TO_APTH5:
			TranscodingMode = E_TRANSCODE_CAMECA_APT_TO_APTH5; break;
		default:
			TranscodingMode = E_TRANSCODE_NOTHING;
	}
	Inputfile = read_xml_attribute_string( rootNode, "Inputfile" );

	//DebugSkip = read_xml_attribute_uint32( rootNode, "DebugSkip" );

	return true;
}


bool ConfigTranscoder::checkUserInput()
{
	cout << "ConfigTranscoder::" << "\n";
	//##MK::check if Inputfile ending is POS or EPOS
	//##MK::check that Outputfile ending is APTH5

	if ( TranscodingMode == E_TRANSCODE_POS_TO_APTH5 ) {
		if ( Inputfile.substr(Inputfile.length()-4) != ".pos" ) {
			cerr << "Attempting to transcode a POS file to APTH5 without delivering a *.pos Inputfile!" << "\n"; return false;
		}
		cout << "Transcode a POS file to APTH5" << "\n";
	}
	if ( TranscodingMode == E_TRANSCODE_EPOS_TO_APTH5 ) {
		if ( Inputfile.substr(Inputfile.length()-5) != ".epos" ) {
			cerr << "Attempting to transcode a EPOS file to APTH5 without delivering an *.epos Inputfile!" << "\n"; return false;
		}
		cout << "Transcode an EPOS file to APTH5" << "\n";
	}
	if ( TranscodingMode == E_TRANSCODE_CAMECA_APT_TO_APTH5 ) {
		if ( Inputfile.substr(Inputfile.length()-4) != ".apt" ) {
			cerr << "Attempting to transcode a APT file to APTH5 without delivering an *.apt Inputfile!" << "\n"; return false;
		}
		cout << "Transcode a APT file to APTH5" << "\n";
	}

	cout << "TranscodingMode " << TranscodingMode << "\n";
	cout << "Input read from " << Inputfile << "\n";
	//cout << "Output written to " << Outputfile << "\n";

	cout << "\n";
	return true;
}


void ConfigTranscoder::reportSettings( vector<pparm> & res)
{
	res.push_back( pparm( "TranscodingMode", sizet2str(TranscodingMode), "", "which internal processing, i.e. transcoding between which in- and output format" ) );
	res.push_back( pparm( "Inputfile", Inputfile, "", "name of the file to become transcoded" ) );
}

