//##MK::GPLV3

#ifndef __PARAPROBE_TRANSCODERHDF_H__
#define __PARAPROBE_TRANSCODERHDF_H__

/*
//shared headers
#include "../../paraprobe-utils/src/metadata/PARAPROBE_UtilsMetadataDefsH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_SyntheticMetadataDefsH5.h"
#include "../../paraprobe-utils/src/PARAPROBE_APTH5.h"
//#include "../../paraprobe-utils/src/PARAPROBE_HDF5.h"
#include "../../paraprobe-utils/src/PARAPROBE_XDMF.h"

//tool-specific headers
//#include "CONFIG_Transcoder.h"
#include "PARAPROBE_TranscoderCiteMe.h"
*/

#include "PARAPROBE_CamecaAPT.h"

class transcoder_h5 : public h5Hdl
{
	//tool-specific sub-class of a HDF5 inheriting all methods of the base class h5Hdl
	//coordinating instance handling all (sequential) writing to HDF5 file wrapping HDF5 low-level C library calls

public:
	transcoder_h5();
	~transcoder_h5();

	int create_paraprobe_apth5( const string h5fn );
	int create_paraprobe_h5( const string h5fn );

//private:
};

#endif
