cmake_minimum_required(VERSION 3.10)

################################################################################################################################
##USER INTERACTION##############################################################################################################
##in this section modifications and local paths need to be modified by the user#################################################
################################################################################################################################
#choose which target system to use
set(USE_MPI30 ON)
set(USE_TALOS OFF)

#update GitSHA of the paraprobe repository prior commit, i.e. pre-processing upon compilation pulling last GitSHA
#we cannot update the current GitSHA as this would create a circular dependency, namely once we have commit, i.e. know the file
#set(MYGITSHA "")


#tell the top directory where this local PARAPROBE version is stored
if(USE_MPI30)
	set(MYPROJECTPATH "/home/m.kuehbach/GITHUB/MPIE_APTFIM_TOOLBOX/paraprobe/code/")
	#set(MYPROJECTPATH "/home/m.kuehbach/Paper18/code/")
endif()
if(USE_TALOS)
	#set(MYPROJECTPATH "/talos/u/mkuehbac/MPIE_APTFIM_TOOLBOX/paraprobe/code/")
	#set(MYPROJECTPATH "/talos/scratch/mkuehbac/Paper18/MPIE_APTFIM_TOOLBOX/paraprobe/code/")
	set(MYPROJECTPATH "/talos/scratch/mkuehbac/Paper14/code")
endif()

#define where to find the utility tools
set(MYUTILSPATH "${MYPROJECTPATH}/paraprobe-utils/build/CMakeFiles/paraprobe-utils.dir/src/")


#### #define where to find the utility tools
#### set(MYUTILSPATH "${MYPROJECTPATH}/paraprobe-utils/")

#where are key external libraries, for paraprobe only HDF5
if(USE_MPI30)
	#set(MYHDFPATH "/home/m.kuehbach/APT3DOIM/src/thirdparty/HDF5/CMake-hdf5-1.10.2/build/HDF5-1.10.2-Linux/HDF_Group/HDF5/1.10.2")
	#set(MYHDFPATH "/home/m.kuehbach/Maintain/paraprobe/src/thirdparty/HDF5/CMake-hdf5-1.10.2/build/HDF5-1.10.2-Linux/HDF_Group/HDF5/1.10.2")
	set(MYHDFPATH "/home/m.kuehbach/ParaprobeTraining/paraprobe/code/thirdparty/HDF5/CMake-hdf5-1.10.6/HDF5-1.10.6-Linux/HDF_Group/HDF5/1.10.6")
	#set(MYHDFPATH "/home/m.kuehbach/GITHUB/MPIE_APTFIM_TOOLBOX/paraprobe/code/thirdparty/HDF5/CMake-hdf5-1.10.7/HDF5-1.10.7-Linux/HDF_Group/HDF5/1.10.7")
endif()
if(USE_TALOS)
	set(MYHDFPATH "/talos/u/mkuehbac/HDF5/CMake-hdf5-1.10.2/build/HDF5-1.10.2-Linux/HDF_Group/HDF5/1.10.2")
endif()



