//##MK::GPLV3

#ifndef __PARAPROBE_TESSELLATOR_HDL_H__
#define __PARAPROBE_TESSELLATOR_HDL_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_TessellatorAcquisor.h"


class tessellatorHdl
{
	//process-level class which implements the worker instance which executes multithreaded computations
	//of Voronoi cells and writes result to a specifically-formatted HDF5 file

public:
	tessellatorHdl();
	~tessellatorHdl();

	bool read_periodictable();
/*
	bool read_iontype_combinations( string fn );
*/

	bool read_reconxyz_from_apth5();
	bool read_ranging_from_apth5();
	bool read_itypes_from_apth5();
	bool read_dist2hull_from_apth5();

	bool broadcast_reconxyz();
	bool broadcast_ranging();
	bool broadcast_distances();

	void itype_sensitive_spatial_decomposition();
//	void distribute_matpoints_on_processes_roundrobin();

	unsigned int execute_local_workpackage(); 			//multi-threaded tessellating of round robin assigned cells

	bool init_target_file();
	bool write_environment_and_settings();
	bool write_cell_ids_to_apth5();
	bool write_cell_volume_to_apth5();					//communicated to master
	bool write_cell_neighbors_to_apth5();
//	bool write_results_to_apth5();
/*
	bool write_shortlist_matpoints_withresults_to_apth5();
*/

	int get_myrank();
	int get_nranks();
	void set_myrank( const int rr );
	void set_nranks( const int nn );
	void init_mpidatatypes();

	rangeTable rng;
//	itypeCombiHdl itsk;
//	vector<PhaseCandidate> cands;
	unsigned char maximum_iontype_uc;

	decompositor sp;
//	volsampler mp;
	vector<int> mp2rk;								//mapping of material points => to ranks
	vector<int> mp2me;

	/*
	//vector<MPI_MatPoint_Info> mpifo;				//individual results
	vector<MPI_Fourier_ROI> mproi;					//only significant for MASTER to organize results writing
	vector<MPI_Fourier_HKLValue> mpres;				//only significant for MASTER to organize results writing
	*/
	
	h5Hdl inputReconH5Hdl;
	h5Hdl inputDistH5Hdl;
	tessellator_h5 debugh5Hdl;
	tessellator_xdmf debugxdmf;
	
	vector<p3dm1> reconstruction;					//the reconstructed and ranged ion point cloud
	vector<p3d> xyz;								//reconstructed ion positions
	vector<unsigned char> ityp_org;					//iontypes ranged, original
	vector<apt_real> dist2hull;						//distance field
	
	vector<tess*> regions;							//one region per OpenMP thread

	profiler tess_tictoc;

private:
	//MPI related
	int myrank;										//my MPI ID in the MPI_COMM_WORLD
	int nranks;

	//MPI_Datatype MPI_Ion_Type;
	MPI_Datatype MPI_Ranger_DictKeyVal_Type;
	MPI_Datatype MPI_Synth_XYZ_Type;
	MPI_Datatype MPI_Ranger_Iontype_Type;
	MPI_Datatype MPI_Ranger_MQIval_Type;
	MPI_Datatype MPI_VoroCell_Info_Type;
};


#endif

