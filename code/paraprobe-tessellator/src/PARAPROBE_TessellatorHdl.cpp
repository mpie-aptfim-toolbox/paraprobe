//##MK::GPLV3

#include "PARAPROBE_TessellatorHdl.h"


tessellatorHdl::tessellatorHdl()
{
	myrank = MASTER;
	nranks = 1;
	maximum_iontype_uc = 0x00;
}


tessellatorHdl::~tessellatorHdl()
{
	for( size_t i = 0; i < regions.size(); i++ ) {
		if ( regions.at(i) != NULL ) {
			delete regions.at(i);
			regions.at(i) = NULL;
		}
	}

	maximum_iontype_uc = 0x00;
}


bool tessellatorHdl::read_periodictable()
{
	//all read PeriodicTable
	/*
	if ( rng.nuclides.load_isotope_library( ConfigTessellator::InputfilePSE ) == false ) {
		cerr << "Rank " << get_myrank() << " loading of PeriodicTableOfElements failed!" << "\n"; return false;
	}
	*/
	if ( rng.nuclides.load_isotope_library() == false ) {
		cerr << "Rank " << get_myrank() << " loading of Config_SHARED PeriodicTableOfElements failed!" << "\n"; return false;
	}
	cout << "Rank " << get_myrank() << " loaded rng.nuclides.isotopes.size() " << rng.nuclides.isotopes.size() << " isotopes" << "\n";
	return true;
}


bool tessellatorHdl::read_reconxyz_from_apth5()
{
	double tic = MPI_Wtime();

	//load xyz positions
	if ( inputReconH5Hdl.read_xyz_from_apth5( ConfigTessellator::InputfileReconstruction, xyz ) == true ) {
		cout << ConfigTessellator::InputfileReconstruction << " read positions successfully" << "\n";
	}
	else {
		cerr << ConfigTessellator::InputfileReconstruction << " read positions failed!" << "\n"; return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	tess_tictoc.prof_elpsdtime_and_mem( "ReadReconXYZFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

    return true;
}


bool tessellatorHdl::read_ranging_from_apth5()
{
	double tic = MPI_Wtime();

	//load ranging information
	if ( inputReconH5Hdl.read_iontypes_from_apth5( ConfigTessellator::InputfileReconstruction, rng.iontypes ) == true ) {
		cout << ConfigTessellator::InputfileReconstruction << " read ranging information successfully" << "\n";

		//create iontype_dictionary
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
			size_t id = static_cast<size_t>(it->id);
			size_t hashval = rng.hash_molecular_ion( it->strct.Z1, it->strct.Z2, it->strct.Z3 );
	cout << id << "\t\t" << hashval << "\t\t" << (int) it->strct.Z1 << ";" << (int) it->strct.Z2 << ";" << (int) it->strct.Z3 << "\n";
			if ( id != 0 && hashval != 0 ) { //skip adding existent UNKNOWN_IONTYPE
				auto jt = rng.iontypes_dict.find( hashval );
				if ( jt == rng.iontypes_dict.end() ) { //really a new iontype for which not yet mqival were defined
	cout << "--->Adding new type " << id << "\t\t" << hashval << "\n";
					//add iontype in dictionary
					rng.iontypes_dict.insert( pair<size_t,size_t>( hashval, id ) );
				}
				else {
	cerr << "--->Detected an iontype where the triplet Z1, Z2, Z3 is a duplicate, which should not happen as iontypes have to be mutually exclusive!" << "\n";
					return false;
					//cout << "--->Adding to an existent type " << id << "\t\t" << hashval << "\t\t" << jt->second << "\n";
					//match to an already existent type
					//rng.iontypes_dict.insert( pair<size_t,size_t>( hashval, id ) ); //jt->second ) );
					//##MK::this fix makes it de facto a charge state specific 8;0,0;0;1 and 8;0;0;0;0;0;0;1 and 8;0;0;0;0;0;0;2 map to different types
					//return false; //##MK::switched of for FAU APT School
				}
			}
			else {
	cout << "--->Me skipping the UNKNOWN_IONTYPE" << "\n";
			}
		}

		//##MK::BEGIN DEBUG, make sure we use have correct ranging data
		cout << "--->Debugging ranging data..." << "\n";
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
			size_t id = static_cast<size_t>(it->id);
			size_t hashval = rng.hash_molecular_ion( it->strct.Z1, it->strct.Z2, it->strct.Z3 );
			cout << id << "\t\t" << hashval << "\t\t" << (int) it->strct.Z1 << ";" << (int) it->strct.Z2 << ";" << (int) it->strct.Z3 << "\n";
			for( auto jt = it->rng.begin(); jt != it->rng.end(); jt++ ) {
				cout << "\t\t\t" << jt->lo << "\t\t" << jt->hi << "\n";
			}
		}
		cout << "--->Debugging ion dictionary..."  << "\n";
		for( auto kt = rng.iontypes_dict.begin(); kt != rng.iontypes_dict.end(); kt++ ) {
			cout << kt->first << "\t\t" << kt->second << "\n";
		}
		//##MK::END DEBUG
	}
	else {
		cerr << ConfigTessellator::InputfileReconstruction << " read ranging information failed!" << "\n"; return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	tess_tictoc.prof_elpsdtime_and_mem( "ReadRangingFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

    return true;
}


bool tessellatorHdl::read_itypes_from_apth5()
{
	double tic = MPI_Wtime();

	//load atom/ion types per xyz position
	if ( inputReconH5Hdl.read_ityp_from_apth5( ConfigTessellator::InputfileReconstruction, ityp_org ) == true ) {
		cout << ConfigTessellator::InputfileReconstruction << " read itypes successfully" << "\n";
	}
	else {
		cerr << ConfigTessellator::InputfileReconstruction << " read itypes failed!" << "\n"; return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	tess_tictoc.prof_elpsdtime_and_mem( "ReadAtomTypesFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

    return true;
}


bool tessellatorHdl::read_dist2hull_from_apth5()
{
	double tic = MPI_Wtime();

	if ( this->inputDistH5Hdl.read_dist_from_apth5( ConfigTessellator::InputfileHullAndDistances, dist2hull ) == true ) {
		cout << ConfigTessellator::InputfileHullAndDistances << " read distances successfully" << "\n";
	}
	else {
		cerr << ConfigTessellator::InputfileHullAndDistances << " read distances failed!" << "\n"; return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	tess_tictoc.prof_elpsdtime_and_mem( "ReadDistancesFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

    return true;
}

//##MK::try to move these generic master to all broadcast functionalities into an own class


bool tessellatorHdl::broadcast_reconxyz()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
		return true;
	}

	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( xyz.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " ion reconstructions!" << "\n"; localhealth = 0;
		}
		if ( xyz.size() == 0 ) {
			cerr << "Ion positions is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the MASTER's xyz size and state!" << "\n"; return false;
	}

	//communicate size of the master's dataset
	int nevt = 0;
	if ( get_myrank() == MASTER ) {
		nevt = static_cast<int>(xyz.size());
	}
	MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	MPI_Synth_XYZ* buf = NULL;
	try {
		buf = new MPI_Synth_XYZ[nevt];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for MPI_Synth_XYZ buffer!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL;
		return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < xyz.size(); i++ ) {
			buf[i] = MPI_Synth_XYZ( xyz.at(i) );
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, nevt, MPI_Synth_XYZ_Type, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			xyz.reserve ( static_cast<size_t>(nevt) );
			for( int i = 0; i < nevt; i++ ) {
				xyz.push_back( p3d( buf[i].x, buf[i].y, buf[i].z ) );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout to instantiate xyz!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";	return false;
	}

//cout << "Rank " << get_myrank() << " listened to MASTER reconstruction.size() " << reconstruction.size() << "\n";
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	tess_tictoc.prof_elpsdtime_and_mem( "BroadcastReconXYZ", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


bool tessellatorHdl::broadcast_ranging()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
		return true;
	}

	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( rng.iontypes.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " iontypesinfo!" << "\n"; localhealth = 0;
		}
		if ( rng.iontypes.size() == 0 ) {
			cerr << "MASTER's rng.iontypes is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
		if ( ityp_org.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " ityp_org per ion!" << "\n"; localhealth = 0;
		}
		if ( ityp_org.size() == 0 ) {
			cerr << "MASTER's ityp_org is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the MASTER's ranging size and state!" << "\n"; return false;
	}

	//communicate metadata first
	int nrng_ivl_evt_dict[4] = {0, 0, 0, 0};
	if ( get_myrank() == MASTER ) {
		nrng_ivl_evt_dict[0] = static_cast<int>(rng.iontypes.size());
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
			nrng_ivl_evt_dict[1] += static_cast<int>(it->rng.size());
		}
		nrng_ivl_evt_dict[2] = static_cast<int>(ityp_org.size());
		nrng_ivl_evt_dict[3] = static_cast<int>(rng.iontypes_dict.size());
	}
	MPI_Bcast( nrng_ivl_evt_dict, 4, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	MPI_Ranger_Iontype* buf1 = NULL;
	MPI_Ranger_MQIval* buf2 = NULL;
	unsigned char* buf3 = NULL;
	MPI_Ranger_DictKeyVal* buf4 = NULL;
	try {
		buf1 = new MPI_Ranger_Iontype[nrng_ivl_evt_dict[0]];
		buf2 = new MPI_Ranger_MQIval[nrng_ivl_evt_dict[1]];
		buf3 = new unsigned char[nrng_ivl_evt_dict[2]];
		buf4 = new MPI_Ranger_DictKeyVal[nrng_ivl_evt_dict[3]];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for buf1, buf2, buf3, and buf4!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf1; buf1 = NULL;
		delete [] buf2; buf2 = NULL;
		delete [] buf3; buf3 = NULL;
		delete [] buf4; buf4 = NULL;
		return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		size_t iv = 0;
		size_t id = 0;
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++, id++ ) {
			unsigned char typid = static_cast<unsigned char>(it->id);
			buf1[id] = MPI_Ranger_Iontype( typid, it->strct.Z1, it->strct.N1, it->strct.Z2, it->strct.N2,
												it->strct.Z3, it->strct.N3, it->strct.sign, it->strct.charge );
			for( auto jt = it->rng.begin(); jt != it->rng.end(); jt++, iv++ ) {
				buf2[iv] = MPI_Ranger_MQIval( jt->lo, jt->hi, typid );
			}
		}
		for( size_t i = 0; i < ityp_org.size(); i++ ) {
			buf3[i] = ityp_org.at(i);
		}
		size_t ii = 0;
		for( auto kvt = rng.iontypes_dict.begin(); kvt != rng.iontypes_dict.end(); kvt++, ii++ ) {
			buf4[ii] = MPI_Ranger_DictKeyVal( static_cast<unsigned long long>(kvt->first), static_cast<unsigned long long>(kvt->second) );
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf1, nrng_ivl_evt_dict[0], MPI_Ranger_Iontype_Type, MASTER, MPI_COMM_WORLD );
	MPI_Bcast( buf2, nrng_ivl_evt_dict[1], MPI_Ranger_MQIval_Type, MASTER, MPI_COMM_WORLD);
	MPI_Bcast( buf3, nrng_ivl_evt_dict[2], MPI_UNSIGNED_CHAR, MASTER, MPI_COMM_WORLD );
	MPI_Bcast( buf4, nrng_ivl_evt_dict[3], MPI_Ranger_DictKeyVal_Type, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			rng.iontypes.reserve( static_cast<size_t>(nrng_ivl_evt_dict[0]) );
			for( int i = 0; i < nrng_ivl_evt_dict[0]; i++ ) {
				rng.iontypes.push_back( rangedIontype() );
				rng.iontypes.back().id = static_cast<unsigned int>( buf1[i].id );
				rng.iontypes.back().strct = evapion3( buf1[i].Z1, buf1[i].N1, buf1[i].Z2, buf1[i].N2, buf1[i].Z3, buf1[i].N3, buf1[i].sign, buf1[i].charge );
				for( int j = 0; j < nrng_ivl_evt_dict[1]; j++ ) { //was [2], ##MK:avoid O(N^2) search, but should not be a problem because N << 256
					if ( static_cast<unsigned int>(buf2[j].id) != rng.iontypes.back().id ) {
						continue;
					}
					else {
						rng.iontypes.back().rng.push_back( mqival( buf2[j].low, buf2[j].high ) );
					}
				}
			}
			ityp_org.reserve( static_cast<size_t>(nrng_ivl_evt_dict[2]) );
			for( int k = 0; k < nrng_ivl_evt_dict[2]; k++ ) {
				ityp_org.push_back( buf3[k] );
			}

			for ( int kv = 0; kv < nrng_ivl_evt_dict[3]; kv++ ) {
				rng.iontypes_dict.insert( pair<size_t,size_t>(
						static_cast<size_t>(buf4[kv].key), static_cast<size_t>(buf4[kv].val) ) );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout to instantiate rng.iontypes and ityp_org!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf1; buf1 = NULL;
	delete [] buf2; buf2 = NULL;
	delete [] buf3; buf3 = NULL;
	delete [] buf4; buf4 = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";	return false;
	}

//cout << "Rank " << get_myrank() << " listened to MASTER reconstruction.size() " << reconstruction.size() << "\n";
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	tess_tictoc.prof_elpsdtime_and_mem( "BroadcastRanging", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


bool tessellatorHdl::broadcast_distances()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
		return true;
	}

	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( dist2hull.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " ion dist2hull info!" << "\n"; localhealth = 0;
		}
		if ( dist2hull.size() == 0 ) {
			cerr << "MASTER's dist2hull is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the MASTER's dist2hull size and state!" << "\n"; return false;
	}

	//communicate size of the master's dataset
	int nevt = 0;
	if ( get_myrank() == MASTER ) {
		nevt = static_cast<int>(dist2hull.size());
	}
	MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	float* buf = NULL;
	try {
		buf = new float[nevt];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for float buffer!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL;
		return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < dist2hull.size(); i++ ) {
			buf[i] = dist2hull.at(i);
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, nevt, MPI_FLOAT, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			dist2hull.reserve ( static_cast<size_t>(nevt) );
			for( int i = 0; i < nevt; i++ ) {
				dist2hull.push_back( buf[i] );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout to instantiate dist2hull!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";	return false;
	}

//cout << "Rank " << get_myrank() << " listened to MASTER reconstruction.size() " << reconstruction.size() << "\n";
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	tess_tictoc.prof_elpsdtime_and_mem( "BroadcastDist2Hull", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


void tessellatorHdl::itype_sensitive_spatial_decomposition()
{
	double tic = MPI_Wtime();

	sp.tip_aabb_get_extrema( xyz );

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	tess_tictoc.prof_elpsdtime_and_mem( "SpecimenAABB3DGetExtrema", APT_XX, APT_IS_SEQ, mm, tic, toc);

	tic = MPI_Wtime();

	//get maximum iontype
	unsigned int maximum_ityp = 0;
	for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
		if ( it->id < static_cast<unsigned char>(UCHARMX) ) {
			if ( it->id > maximum_ityp ) {
				maximum_ityp = it->id;
			}
		}
		else {
			cerr << "Detected an invalid iontype during itype_sensitive_spatial_decomposition!" << "\n"; return;
		}
	}

	maximum_iontype_uc = static_cast<unsigned char>(maximum_ityp); //MK::not the number of disjoint types but the maximum ityp value!
cout << "Rank " << get_myrank() << " maximum itype identified is " << (int) maximum_iontype_uc << "\n";

	sp.loadpartitioning_arraywithhalo_per_ityp( xyz, dist2hull, ityp_org,
			maximum_iontype_uc, ConfigTessellator::GuardZoneFactor );

	//first point were ityp_org and ityp_rnd are no longer necessary and could in principle be deleted

	toc = MPI_Wtime();
	mm = tess_tictoc.get_memoryconsumption();
	tess_tictoc.prof_elpsdtime_and_mem( "AABB3DLoadPartitioning", APT_XX, APT_IS_PAR, mm, tic, toc);
}


/*
void tessellatorHdl::distribute_matpoints_on_processes_roundrobin()
{
	//all processes have the same matpoints and agree on the same global round-robin work partitioning
	//##MK::error handling
	if ( mp.matpoints.size() >= static_cast<size_t>(INT32MX-1) ) {
		cerr << "More material sampling points were defined than current implementation can handle!" << "\n"; return;
	}
	if ( mp.matpoints.size() == 0 ) {
		cerr << "No material points to process!" << "\n"; return;
	}

	double tic = MPI_Wtime();

	mp2rk.reserve( mp.matpoints.size() );
	//globally orchestrated round-robin partitioning
	//MK::block partitioning is expected to result in strong load-imbalance because the implicit order on the ion positions
	//in xyz maps the specimen geometry, whether positions earlier or to the end of the array xyz are likely closer to
	//the specimen boundary and therefore processes taking such block on average have fewer ions per region and therefore
	//will finish earlier, even though the sequence of ion hits on the detector xy plane i.e. the xy plane is somewhat random
	//the order in z is not!
	int nmp = static_cast<int>(mp.matpoints.size());
	int nr = get_nranks();
	int mr = get_myrank();
	for( int mpid = 0; mpid < nmp; mpid++ ) {
		int thisrank = mpid % nr;
		mp2rk.push_back( thisrank );
		if ( thisrank != mr ) {
			continue;
		}
		else {
			mp2me.push_back( mpid ); //global mat point IDs!
//cout << "Rank " << thisrank << " takes care of material point/ROI " << mpid << "\n";
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = araullo_tictoc.get_memoryconsumption();
	araullo_tictoc.prof_elpsdtime_and_mem( "DistributeMatPointsOnMPIProcesses", APT_XX, APT_IS_PAR, mm, tic, toc);

	cout << "Rank " << get_myrank() << " will process " << mp2me.size() << " of a total of " << mp2rk.size() << " sampling points" << "\n";
}
*/


unsigned int tessellatorHdl::execute_local_workpackage()
{
	double tic = MPI_Wtime();
cout << "Rank " << get_myrank() << " CPU MPI/OpenMP-hybrid tessellating... " << "\n";

	unsigned int faultycells = 0;
	#pragma omp parallel shared(faultycells)
	{
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();

		#pragma omp master
		{
			regions = vector<tess*>( nt, NULL );
		}
		//necessary as otherwise write back conflict on workers from threads
		#pragma omp barrier

		//setup thread-local worker and use thread-local memory
		tess* thrhdl = NULL;
		try {
			thrhdl = new tess;
		}
		catch (bad_alloc &mecroak) {
			#pragma omp critical
			{
				cerr << "Rank " << get_myrank() << " thread " << mt << " unable to allocate an tess instance!" << "\n";
			}
		}

		//now workers backreference holders can safely be modified by current thread
		//MK::pointer will point to thread-local memory
		#pragma omp critical
		{
			regions.at(mt) = thrhdl;
		}

		if ( thrhdl->configure( get_myrank(), get_nranks(), sp.tip, sp.db_guarded.at(mt) ) == true ) {

			//MK::does not make any correction attempt for cells at boundary, just detect characterize cells
			thrhdl->tessellate( sp.db_guarded.at(mt) );
		}

		#pragma omp critical
		{
			faultycells += thrhdl->mystats.CellsInsideIll;
		}
	} //end of parallel region

	double toc = MPI_Wtime();
	memsnapshot mm = tess_tictoc.get_memoryconsumption();
	tess_tictoc.prof_elpsdtime_and_mem( "HybridTessellating", APT_XX, APT_IS_PAR, mm, tic, toc);

cout << "Rank " << get_myrank() << " hybrid tessellating took " << (toc-tic) << " seconds" << "\n";
	return faultycells;
}


bool tessellatorHdl::init_target_file()
{
	//##MK::generate group hierarchy of an open source IFES APTTC APTH5 HDF5 file
	double tic = MPI_Wtime();

	string h5fn_out = "PARAPROBE.Tessellator.Results.SimID." + to_string(ConfigShared::SimID) + ".Stats.h5";

	debugh5Hdl.h5resultsfn = h5fn_out;

	//only master instantiates the file
	if ( get_myrank() == MASTER ) {
cout << "Rank " << MASTER << " initializing target file " << h5fn_out << "\n";

		if ( debugh5Hdl.create_tessellator_apth5( h5fn_out ) == WRAPPED_HDF5_SUCCESS ) {
			double toc = MPI_Wtime();
			memsnapshot mm = memsnapshot();
			tess_tictoc.prof_elpsdtime_and_mem( "InitStatsResultsH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
			return true;
		}
		else {
			return false;
		}
	}
	else { //slaves just fill the names
		double toc = MPI_Wtime();
		memsnapshot mm = memsnapshot();
		tess_tictoc.prof_elpsdtime_and_mem( "InitStatsResultsH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
		return true;
	}
}


bool tessellatorHdl::write_environment_and_settings()
{
	vector<pparm> hardware = tess_tictoc.report_machine();
	string prfx = PARAPROBE_TESS_META_HRDWR; //PARAPROBE_UTILS_HRDWR_META_KEYS;
	for( auto it = hardware.begin(); it != hardware.end(); it++ ) {
		cout << it->keyword << "__" << it->value << "__" << it->unit << "__" << it->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *it ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing hardware setting " << it->keyword << " failed!" << "\n";
			return false;
		}
	}
	cout << "Rank " << MASTER << " hardware settings written to H5" << "\n";

	vector<pparm> software;
	ConfigTessellator::reportSettings( software );
	prfx = PARAPROBE_TESS_META_SFTWR; //PARAPROBE_UTILS_SFTWR_META_KEYS;
	for( auto jt = software.begin(); jt != software.end(); jt++ ) {
		cout << jt->keyword << "__" << jt->value << "__" << jt->unit << "__" << jt->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *jt ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing software tool setting " << jt->keyword << " failed!" << "\n";
			return false;
		}
	}
	cout << "Rank " << MASTER << " software settings written to H5" << "\n";

	/*
	//add LatticeIontypeCombinations using the same prfx
	for( auto kt = itsk.iifo.begin(); kt != itsk.iifo.end(); kt++ ) {
		cout << kt->keyword << "__" << kt->value << "__" << kt->unit << "__" << kt->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *kt ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing software tool setting " << kt->keyword << " failed!" << "\n";
			return false;
		}
	}
	*/
	return true;
}

/*
bool tessellatorHdl::write_materialpoints_to_apth5()
{
	double tic = MPI_Wtime();

	if ( workers.size() == 0 ) {
		cerr << "Rank " << get_myrank() << " workers is empty!" << "\n"; return false;
	}
	if ( workers.at(0) == NULL ) {
		cerr << "Rank " << get_myrank() << " MASTER worker does not exist!" << "\n"; return false;
	}

	string fwslash = "/";

	size_t nso2 = ConfigAraullo::NumberOfSO2Directions;
	if ( directions.size() != nso2 ) {
		cerr << "Rank " << get_myrank() << " directions.size() is unexpectedly different!" << "\n"; return false;
	}

	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";

	//elevation, azimuth
	dsnm = PARAPROBE_ARAULLO_META_DIR_ELAZ;
	vector<float> f32 = vector<float>( nso2 * PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX, 0.f );
	for( size_t ea = 0; ea < nso2; ea++ ) {
		f32[ea*PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX+0] = directions[ea].elevation;
		f32[ea*PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX+1] = directions[ea].azimuth;
	}
	ifo = h5iometa( dsnm, f32.size()/PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX, PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create DIR_ELAZ metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, f32.size()/PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX, 0, PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX,
			f32.size()/PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX, PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store DIR_ELAZ metadata! " << status << "\n"; return false;
	}

	//x,y,z
	f32 = vector<float>( nso2 * PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX, 0.f );
	dsnm = PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ; //direction vectors for visualization
	for( size_t ea = 0; ea < nso2; ea++ ) {
		//new:geographic definition active rotation about azimuth counter-clockwise with az = 0 beginning at x axis, right handed CS
		//new:elevation from xy plane upwards positive
		float e = directions[ea].elevation;
		float a = directions[ea].azimuth;
		f32[ea*PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX+0] = cos(a) * cos(e);
		f32[ea*PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX+1] = sin(a) * cos(e);
		f32[ea*PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX+2] = sin(e);
	}
	ifo = h5iometa( dsnm, f32.size()/PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX, PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create DIR_XYZ metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, f32.size()/PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX, 0, PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX,
			f32.size()/PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX, PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store DIR_XYZ metadata! " << status << "\n"; return false;
	}
	f32 = vector<float>();

	//XDMF topo
	vector<unsigned int> u32 = vector<unsigned int>( 3*directions.size(), 1 ); //MK::we need to store a triplet of XDMF topology type count and vertex ID
	dsnm = PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO;
	for ( size_t i = 0; i < directions.size(); i++ ) {  //XDMF type key 1, how many 1 because vertices
		u32.at(i*3+2) = i;
	}
	ifo = h5iometa( dsnm, u32.size()/PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO_NCMAX, PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create DIR_TOPO metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, u32.size()/PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO_NCMAX, 0, PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO_NCMAX,
			u32.size()/PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO_NCMAX, PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store DIR_TOPO metadata! " << status << "\n"; return false;
	}
	u32 = vector<unsigned int>();

	//material point grid xyz
	f32 = vector<float>( PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX*mp.matpoints.size(), 0.f ); //MK::we need to store a triplet of XDMF topology type count and vertex ID
	dsnm = PARAPROBE_ARAULLO_META_MP_XYZ;
	for ( size_t i = 0; i < mp.matpoints.size(); i++ ) {
		f32.at(i*PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX+0) = mp.matpoints[i].x;
		f32.at(i*PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX+1) = mp.matpoints[i].y;
		f32.at(i*PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX+2) = mp.matpoints[i].z;
	}
	ifo = h5iometa( dsnm, f32.size()/PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX, PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create MP_XYZ metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, f32.size()/PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX, 0, PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX,
			f32.size()/PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX, PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX );
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store MP_XYZ metadata! " << status << "\n"; return false;
	}
	f32 = vector<float>( PARAPROBE_ARAULLO_META_MP_R_NCMAX*mp.matpoints.size(), 0.f ); //MK::we need to store a triplet of XDMF topology type count and vertex ID
	dsnm = PARAPROBE_ARAULLO_META_MP_R;
	for ( size_t i = 0; i < mp.matpoints.size(); i++ ) {
		f32.at(i*PARAPROBE_ARAULLO_META_MP_R_NCMAX+0) = mp.matpoints[i].R;
	}
	ifo = h5iometa( dsnm, f32.size()/PARAPROBE_ARAULLO_META_MP_R_NCMAX, PARAPROBE_ARAULLO_META_MP_R_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create MP_R metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, f32.size()/PARAPROBE_ARAULLO_META_MP_R_NCMAX, 0, PARAPROBE_ARAULLO_META_MP_R_NCMAX,
			f32.size()/PARAPROBE_ARAULLO_META_MP_R_NCMAX, PARAPROBE_ARAULLO_META_MP_R_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store MP_R metadata! " << status << "\n"; return false;
	}
	f32 = vector<float>();

	u32 = vector<unsigned int>( PARAPROBE_ARAULLO_META_MP_ID_NCMAX*mp.matpoints.size(), 0 );
	dsnm = PARAPROBE_ARAULLO_META_MP_ID;
	for ( size_t i = 0; i < mp.matpoints.size(); i++ ) {
		u32.at(i*PARAPROBE_ARAULLO_META_MP_ID_NCMAX+0) = mp.matpoints[i].mpid;
	}
	ifo = h5iometa( dsnm, u32.size()/PARAPROBE_ARAULLO_META_MP_ID_NCMAX, PARAPROBE_ARAULLO_META_MP_ID_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create MP_ID metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, u32.size()/PARAPROBE_ARAULLO_META_MP_ID_NCMAX, 0, PARAPROBE_ARAULLO_META_MP_ID_NCMAX,
			u32.size()/PARAPROBE_ARAULLO_META_MP_ID_NCMAX, PARAPROBE_ARAULLO_META_MP_ID_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store MP_ID metadata! " << status << "\n"; return false;
	}

	//XDMF topo
	u32 = vector<unsigned int>( 3*mp.matpoints.size(), 1 ); //MK::we need to store a triplet of XDMF topology type count and vertex ID
	dsnm = PARAPROBE_ARAULLO_META_MP_TOPO;
	for ( size_t i = 0; i < mp.matpoints.size(); i++ ) {
		u32.at(3*i+2) = i;
	}
	ifo = h5iometa( dsnm, u32.size()/PARAPROBE_ARAULLO_META_MP_TOPO_NCMAX, PARAPROBE_ARAULLO_META_MP_TOPO_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create MP_TOPO metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, u32.size()/PARAPROBE_ARAULLO_META_MP_TOPO_NCMAX, 0, PARAPROBE_ARAULLO_META_MP_TOPO_NCMAX,
			u32.size()/PARAPROBE_ARAULLO_META_MP_TOPO_NCMAX, PARAPROBE_ARAULLO_META_MP_TOPO_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store MP_TOPO metadata! " << status << "\n"; return false;
	}
	u32 = vector<unsigned int>();

	//phases
	u32.push_back( itsk.iphcands.size() );
	dsnm = PARAPROBE_ARAULLO_META_PHCAND_N;
	ifo = h5iometa( dsnm, u32.size()/PARAPROBE_ARAULLO_META_PHCAND_N_NCMAX, PARAPROBE_ARAULLO_META_PHCAND_N_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " PARAPROBE_ARAULLO_META_PHCAND_N create failed! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, u32.size()/PARAPROBE_ARAULLO_META_PHCAND_N_NCMAX, 0, PARAPROBE_ARAULLO_META_PHCAND_N_NCMAX,
				u32.size()/PARAPROBE_ARAULLO_META_PHCAND_N_NCMAX, PARAPROBE_ARAULLO_META_PHCAND_N_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " PARAPROBE_ARAULLO_META_PHCAND_N write failed! " << status << "\n"; return false;
	}
	u32 = vector<unsigned int>();

	for( auto mtsk = itsk.iphcands.begin(); mtsk != itsk.iphcands.end(); mtsk++ ) {
		string grpnm = PARAPROBE_ARAULLO_META_PHCAND + fwslash + "PH" + to_string(mtsk->candid);
		status = debugh5Hdl.create_group( grpnm );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "Rank " << get_myrank() << " grpnm " << grpnm << " create failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " grpnm " << grpnm << " create " << status << "\n";

		vector<unsigned char> u8_id;
		vector<unsigned char> u8_wh;
		for( int tg = 0; tg < TARGET_LIST_SIZE; tg++ ) {
			unsigned char current_ityp = mtsk->targets[tg];
			if ( current_ityp != 0xFF ) {
				u8_id.push_back( current_ityp );
				evapion3 tmp = rng.iontypes.at(static_cast<int>(current_ityp)).strct;
				u8_wh.push_back( tmp.Z1 );
				u8_wh.push_back( tmp.N1 );
				u8_wh.push_back( tmp.Z2 );
				u8_wh.push_back( tmp.N2 );
				u8_wh.push_back( tmp.Z3 );
				u8_wh.push_back( tmp.N3 );
				u8_wh.push_back( tmp.sign );
				u8_wh.push_back( tmp.charge );
			}
		}
		vector<float> f32_rsp;
		f32_rsp.push_back( mtsk->realspace );
		f32_rsp.push_back( mtsk->realspacemin );
		f32_rsp.push_back( mtsk->realspacemax );

		dsnm = grpnm + fwslash + PARAPROBE_ARAULLO_META_PHCAND_ID;
		ifo = h5iometa( dsnm, u8_id.size()/PARAPROBE_ARAULLO_META_PHCAND_ID_NCMAX, PARAPROBE_ARAULLO_META_PHCAND_ID_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " create failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " create " << status << "\n";
		offs = h5offsets( 0, u8_id.size()/PARAPROBE_ARAULLO_META_PHCAND_ID_NCMAX, 0, PARAPROBE_ARAULLO_META_PHCAND_ID_NCMAX,
						u8_id.size()/PARAPROBE_ARAULLO_META_PHCAND_ID_NCMAX, PARAPROBE_ARAULLO_META_PHCAND_ID_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, u8_id );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " write failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " write " << status << "\n";
		u8_id = vector<unsigned char>();

		dsnm = grpnm + fwslash + PARAPROBE_ARAULLO_META_PHCAND_WHAT;
		ifo = h5iometa( dsnm, u8_wh.size()/PARAPROBE_ARAULLO_META_PHCAND_WHAT_NCMAX, PARAPROBE_ARAULLO_META_PHCAND_WHAT_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " create failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " create " << status << "\n";
		offs = h5offsets( 0, u8_wh.size()/PARAPROBE_ARAULLO_META_PHCAND_WHAT_NCMAX, 0, PARAPROBE_ARAULLO_META_PHCAND_WHAT_NCMAX,
						u8_wh.size()/PARAPROBE_ARAULLO_META_PHCAND_WHAT_NCMAX, PARAPROBE_ARAULLO_META_PHCAND_WHAT_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, u8_wh );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " write failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " write " << status << "\n";
		u8_wh = vector<unsigned char>();

		dsnm = grpnm + fwslash + PARAPROBE_ARAULLO_META_PHCAND_RSP;
		ifo = h5iometa( dsnm, f32_rsp.size()/PARAPROBE_ARAULLO_META_PHCAND_RSP_NCMAX, PARAPROBE_ARAULLO_META_PHCAND_RSP_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " create failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " create " << status << "\n";
		offs = h5offsets( 0, f32_rsp.size()/PARAPROBE_ARAULLO_META_PHCAND_RSP_NCMAX, 0, PARAPROBE_ARAULLO_META_PHCAND_RSP_NCMAX,
						f32_rsp.size()/PARAPROBE_ARAULLO_META_PHCAND_RSP_NCMAX, PARAPROBE_ARAULLO_META_PHCAND_RSP_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32_rsp );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " write failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " write " << status << "\n";
		f32_rsp = vector<float>();

		//instantiate groups for phase-specific results were instantiated at init_targetfile
	}

	string xdmffn = "PARAPROBE.Araullo.Results.SimID." + to_string(ConfigShared::SimID) + ".MatPoints.xdmf";
	debugxdmf.create_materialpoint_file( xdmffn, mp.matpoints.size(), debugh5Hdl.h5resultsfn );

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "WriteMatPointsResultsH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}
*/


bool tessellatorHdl::write_cell_ids_to_apth5()
{
	return true;
}


bool tessellatorHdl::write_cell_volume_to_apth5()
{
	double tic = MPI_Wtime();

	int localhealth = 1;
	int globalhealth = 0;

	//allocate results buffer for HDF5
	vector<double> f64;
	if ( get_myrank() == MASTER ) {
		try {
			f64 = vector<double>( xyz.size(), 0.0 ); //default value for all eroded cells
		}
		catch(bad_alloc &croak) {
			cerr << "write_cell_volume_to_apth5 on MASTER failed during allocation of f64!" << "\n"; localhealth = 0;
		}
	}
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " detected an error during writing in write_cell_volume_to_apth5!" << "\n"; return false;
	}

	//now we have f64 allocated an initialized on the master
	MPI_VoroCellInfo* snd = NULL;
	MPI_VoroCellInfo* rcv = NULL;
	size_t mylen = 0;

	if ( get_myrank() == MASTER ) {
		//MASTER fills in his own results
		for( size_t mt = 0; mt < regions.size(); mt++ ) {
			if ( regions.at(mt) != NULL ) {
				for( auto it = regions.at(mt)->io_cellifo.begin(); it != regions.at(mt)->io_cellifo.end(); it++ ) {
					//MK::mind that the order of the global IonID like in xyz is NOT RETAINED during the processing of the cells
					//therefore we need to reorganize the results as otherwise xyz implicit order maps to different cell volumina!
					unsigned int thisone = it->IonID;
					f64.at(thisone) = it->volume;
				}
			}
		}
	}
	else { //==SLAVES meanwhile packs their results ready for sending them to the master
		mylen = 0;
		for( size_t mt = 0; mt < regions.size(); mt++ ) {
			if ( regions.at(mt) != NULL ) {
				mylen += regions.at(mt)->io_cellifo.size();
			}
		}
		if ( mylen > 0 ) { //if the slave has sth to contribute pack it
			try {
				snd = new MPI_VoroCellInfo[mylen];
				size_t i = 0;
				for( size_t mt = 0; mt < regions.size(); mt++ ) {
					if ( regions.at(mt) != NULL ) {
						size_t nj = regions.at(mt)->io_cellifo.size();
						for( size_t j = 0; j < nj; j++, i++ ) {
							snd[i].V = regions.at(mt)->io_cellifo.at(j).volume;
							snd[i].IonID = regions.at(mt)->io_cellifo.at(j).IonID;
						}
					}
				}
			}
			catch (bad_alloc &mecroak) {
				cerr << "write_cell_volume_to_apth5 on rank " << get_myrank() << " failed during allocation of snd!" << "\n"; localhealth = 0;
			}
		}
		else {
			cout << "write_cell_volume_to_apth5 on rank " << get_myrank() << " has no results and thus does not participate" << "\n";
		}
	}
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " detected an error during preparation of snd arrays in write_cell_volume_to_apth5!" << "\n"; return false;
	}

	//slaves need to communicate (even 0) how many results they have such that the MASTER can allocate the results buffer
	//##MK::up to here above steps are necessary, thereafter one may resort in the future to a faster MPI_Gatherv on the snd buffers instead of a trivial for loop with barriers
	unsigned int* rk2n = NULL; //only significant for MASTER
	if ( get_myrank() == MASTER ) {
		rk2n = new unsigned int[get_nranks()];
		//##MK::error management
	}

	unsigned int sndcnt = static_cast<unsigned int>(mylen);
	MPI_Gather(&sndcnt, 1, MPI_UNSIGNED, rk2n, 1, MPI_UNSIGNED, MASTER, MPI_COMM_WORLD );
	//visit the slaves and pull their results
	for( int rk = MASTER+1; rk < get_nranks(); rk++ ) { //+1 because we already have the results of the master
		if ( get_myrank() == MASTER ) {
			if ( rk2n[rk] > 0 ) {
				MPI_VoroCellInfo* rcv = NULL;
				rcv = new MPI_VoroCellInfo[rk2n[rk]]; //(); should do it as well...
				for( unsigned int ii = 0; ii < rk2n[rk]; ii++ ) { rcv[ii] = MPI_VoroCellInfo(); } //hyperphobic

				MPI_Recv( rcv, rk2n[rk], MPI_VoroCell_Info_Type, rk, rk, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
				for( unsigned int ii = 0; ii < rk2n[rk]; ii++ ) {
					unsigned int thisone = rcv[ii].IonID; //##MK::also error checks could be implemented here
					f64.at(thisone) = rcv[ii].V;
				}

				delete [] rcv; rcv = NULL;
			}
		}
		else {
			if ( get_myrank() == rk ) { //slave whos turn it is sends the data
				if ( sndcnt > 0 ) {
					MPI_Send( snd, sndcnt, MPI_VoroCell_Info_Type, MASTER, rk, MPI_COMM_WORLD );

					delete [] snd; snd = NULL;
				}
			}
		}
		MPI_Barrier( MPI_COMM_WORLD );
	} //pull results from next rank

	delete [] rk2n; rk2n = NULL;

	//then query results from neighbors for this phase
	if ( get_myrank() == MASTER ) {
		cout << "Rank " << get_myrank() << " writes cell volumina to H5 file!" << "\n";
		//volume
		int status = 0;
		h5iometa ifo = h5iometa();
		h5offsets offs = h5offsets();
		string dsnm = PARAPROBE_TESS_RES_VOL;
		//in-place writing of f64
		ifo = h5iometa( dsnm, f64.size()/PARAPROBE_TESS_RES_VOL_NCMAX, PARAPROBE_TESS_RES_VOL_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f64le( ifo );
		if ( status == WRAPPED_HDF5_SUCCESS ) {
			offs = h5offsets( 0, f64.size()/PARAPROBE_TESS_RES_VOL_NCMAX, 0, PARAPROBE_TESS_RES_VOL_NCMAX,
						f64.size()/PARAPROBE_TESS_RES_VOL_NCMAX, PARAPROBE_TESS_RES_VOL_NCMAX );
			status = debugh5Hdl.write_contiguous_matrix_f64le_hyperslab( ifo, offs, f64 );
			if ( status == WRAPPED_HDF5_SUCCESS ) {
	//cout << "Rank " << get_myrank() << " wrote SpecificPeak " << dsnm << " status " << status << "\n";
				f64 = vector<double>();
			}
			else {
				cerr << "Rank " << get_myrank() << " failed to write PARAPROBE_TESS_RES_VOL " << status << "\n"; return false;
			}
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to create PARAPROBE_TESS_RES_VOL " << status << "\n"; return false;
		}

		//cell erosion distance
		f64.push_back( ConfigTessellator::CellErosionDistance );
		dsnm = PARAPROBE_TESS_META_DERO;
		ifo = h5iometa( dsnm, f64.size()/PARAPROBE_TESS_META_DERO_NCMAX, PARAPROBE_TESS_META_DERO_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f64le( ifo );
		if ( status == WRAPPED_HDF5_SUCCESS ) {
			offs = h5offsets( 0, f64.size()/PARAPROBE_TESS_META_DERO_NCMAX, 0, PARAPROBE_TESS_META_DERO_NCMAX,
						f64.size()/PARAPROBE_TESS_META_DERO_NCMAX, PARAPROBE_TESS_META_DERO_NCMAX );
			status = debugh5Hdl.write_contiguous_matrix_f64le_hyperslab( ifo, offs, f64 );
			if ( status == WRAPPED_HDF5_SUCCESS ) {
	//cout << "Rank " << get_myrank() << " wrote SpecificPeak " << dsnm << " status " << status << "\n";
				f64 = vector<double>();
			}
			else {
				cerr << "Rank " << get_myrank() << " failed to write PARAPROBE_TESS_META_DERO " << status << "\n"; return false;
			}
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to create PARAPROBE_TESS_META_DERO " << status << "\n"; return false;
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	tess_tictoc.prof_elpsdtime_and_mem( "WriteCellVolumeH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool tessellatorHdl::write_cell_neighbors_to_apth5()
{
	return true;
}

/*
bool tessellatorHdl::write_results_to_apth5()
{
	double tic = MPI_Wtime();

	if ( workers.size() == 0 ) {
		cerr << "Rank " << get_myrank() << " workers is empty!" << "\n"; return true;
	}
	if ( workers.at(0) == NULL ) {
		cerr << "Rank " << get_myrank() << " MASTER worker does not exist!" << "\n"; return true;
	}

	string fwslash = "/";
	size_t nso2 = ConfigAraullo::NumberOfSO2Directions;
	size_t nbins = workers.at(0)->cfg.NumberOfBins;

	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";
	size_t roffset = 0;

	//##MK::DEBUG simple OpenMP profiling per material point
	string tictoc_fn = "PARAPROBE.Araullo.SimID." + to_string(ConfigShared::SimID) + ".Rank." + to_string(get_myrank()) + ".DetailedProfiling.csv";
	ofstream csvlog;
	csvlog.open(tictoc_fn.c_str(), ofstream::out | ofstream::trunc);
	if (csvlog.is_open() == true) {
		//header
		csvlog << "MPID;PHCANDID;IonsInROI;ThreadID;Project;FFT;FindPks\n";
		csvlog << ";;;;s;s;s\n";
		csvlog << "MPID;PHCANDID;IonsInROI;ThreadID;Project;FFT;FindPks\n";
	}
	else {
		cerr << "Unable to write process-local profiling files" << "\n";
	}
	//##MK::DEBUG

	for( size_t thr = 0; thr < workers.size(); thr++ ) {
		acquisor* thrhdl = workers.at(thr);
		if ( thrhdl != NULL ) {
			for( auto it = thrhdl->res.begin(); it != thrhdl->res.end(); it++ ) {

				int mpid = it->mpid;
				int phcid = it->phcandid;
				araullo_res* thisone = it->dat;

//cout << "Rank " << get_myrank() << " mpid/phcid/thisone " << mpid << ";" << phcid << ";" << thisone << "\n";

				//to separate or not the results of different images or use aggregated hyperslabs?
				//++ separation is much more intuitive to read and check for implementation errors, in particular for non experts
				//++ allows for explicit visualization of single material point results
				//-- is less performant because more datasets to create, in particular when doing so using PHDF5
				//##MK::here I opted to use the separated approach
				if ( thisone != NULL ) {

					if ( ConfigAraullo::IOSpecificPeaks == true ) {
//cout << "Rank " << get_myrank() << " IOSpecificPeaks mpid/phcid/thisone " << mpid << ";" << phcid << ";" << thisone << "\t\t" << thisone->SpecificPeaksTbl.size() << "\n";

						dsnm = PARAPROBE_ARAULLO_RES_SPECPEAK + fwslash + "PH" + to_string(phcid) + fwslash + to_string(mpid);
						//in-place writing of thisone->SpecificPeaksTbl
						ifo = h5iometa( dsnm, thisone->SpecificPeaksTbl.size()/PARAPROBE_ARAULLO_RES_SPECPEAK_NCMAX, PARAPROBE_ARAULLO_RES_SPECPEAK_NCMAX );
						status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
						if ( status == WRAPPED_HDF5_SUCCESS ) {
							offs = h5offsets( 0, thisone->SpecificPeaksTbl.size()/PARAPROBE_ARAULLO_RES_SPECPEAK_NCMAX,
									0, PARAPROBE_ARAULLO_RES_SPECPEAK_NCMAX,
										thisone->SpecificPeaksTbl.size()/PARAPROBE_ARAULLO_RES_SPECPEAK_NCMAX, PARAPROBE_ARAULLO_RES_SPECPEAK_NCMAX);
							status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, thisone->SpecificPeaksTbl );
							if ( status == WRAPPED_HDF5_SUCCESS ) {
//cout << "Rank " << get_myrank() << " wrote SpecificPeak " << dsnm << " status " << status << "\n";
								thisone->SpecificPeaksTbl = vector<float>();
							}
							else {
								cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to store " << dsnm << " status " << status << "\n"; return false;
							}
						}
						else {
							cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to create " << dsnm << " status " << status << "\n"; return false;
						}
					}
					if ( ConfigAraullo::IOStrongPeaks == true ) {
//cout << "Rank " << get_myrank() << " IOStrongPeaks mpid/phcid/thisone " << mpid << ";" << phcid << ";" << thisone << "\t\t" << thisone->ThreePeaksTbl.size() << "\n";

						dsnm = PARAPROBE_ARAULLO_RES_THREEPEAK + fwslash + "PH" + to_string(phcid) + fwslash + to_string(mpid);
						//in-place writing of thisone->ThreePeaksTbl
						ifo = h5iometa( dsnm, thisone->ThreePeaksTbl.size()/PARAPROBE_ARAULLO_RES_THREEPEAK_NCMAX, PARAPROBE_ARAULLO_RES_THREEPEAK_NCMAX);
						status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
						if ( status == WRAPPED_HDF5_SUCCESS ) {
							offs = h5offsets( 0, thisone->ThreePeaksTbl.size()/PARAPROBE_ARAULLO_RES_THREEPEAK_NCMAX,
										0, PARAPROBE_ARAULLO_RES_THREEPEAK_NCMAX,
										thisone->ThreePeaksTbl.size()/PARAPROBE_ARAULLO_RES_THREEPEAK_NCMAX, PARAPROBE_ARAULLO_RES_THREEPEAK_NCMAX);
							status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, thisone->ThreePeaksTbl );
							if ( status == WRAPPED_HDF5_SUCCESS ) {
//cout << "Rank " << get_myrank() << " wrote ThreePeaksTbl " << dsnm << " status " << status << "\n";
								thisone->ThreePeaksTbl = vector<float>();
							}
							else {
								cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to store " << dsnm << " status " << status << "\n"; return false;
							}
						}
						else {
							cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to create " << dsnm << " status " << status << "\n"; return false;
						}
					}
					if ( ConfigAraullo::IOHistoCnts == true ) {
//cout << "Rank " << get_myrank() << " IOHistoCnts mpid/phcid/thisone " << mpid << ";" << phcid << ";" << thisone << "\t\t" << thisone->SDMHistoTblU32.size() <<  "\n";

						dsnm = PARAPROBE_ARAULLO_RES_HISTO_CNTS + fwslash + "PH" + to_string(phcid) + fwslash + to_string(mpid);
						//only one PhaseCandidate per result so use SDMHistoTblU32 in-place , histogram count u32
						ifo = h5iometa( dsnm, thisone->SDMHistoTblU32.size()/nbins, nbins);
						status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
						if ( status == WRAPPED_HDF5_SUCCESS ) {
							offs = h5offsets( 0, thisone->SDMHistoTblU32.size()/nbins, 0, nbins,
									thisone->SDMHistoTblU32.size()/nbins, nbins);
							status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, thisone->SDMHistoTblU32 );
							if ( status == WRAPPED_HDF5_SUCCESS ) {
//cout << "Rank " << get_myrank() << " wrote SDMU32 " << dsnm << " status " << status << "\n";
								thisone->SDMHistoTblU32 = vector<unsigned int>();
							}
							else {
								cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to store " << dsnm << " status " << status << "\n"; return false;
							}
						}
						else {
							cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to create " << dsnm << " status " << status << "\n"; return false;
						}
					}
					if ( ConfigAraullo::IOHistoFFTMagn == true ) {
//cout << "Rank " << get_myrank() << " IOHistoFFTMagn mpid/phcid/thisone " << mpid << ";" << phcid << ";" << thisone << "\t\t" << thisone->SDMHistoTblF32.size() << "\n";

						dsnm = PARAPROBE_ARAULLO_RES_HISTO_MAGN + fwslash + "PH" + to_string(phcid) + fwslash + to_string(mpid);
						//only one PhaseCandidate per result so use SDMHistoTblU32 in-place , histogram count f32 containing the fft on the first [0, nbins/2
						ifo = h5iometa( dsnm, thisone->SDMHistoTblF32.size()/nbins, nbins);
						status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
						if ( status == WRAPPED_HDF5_SUCCESS ) {
							offs = h5offsets( 0, thisone->SDMHistoTblF32.size()/nbins, 0, nbins,
							thisone->SDMHistoTblF32.size()/nbins, nbins);
							status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, thisone->SDMHistoTblF32 );
							if ( status == WRAPPED_HDF5_SUCCESS ) {
//cout << "Rank " << get_myrank() << " wrote SDMF32 " << dsnm << " status " << status << "\n";
								thisone->SDMHistoTblF32 = vector<float>();
							}
							else {
								cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to store " << dsnm << " status " << status << "\n"; return false;
							}
						}
						else {
							cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to create " << dsnm << " status " << status << "\n"; return false;
						}
					}

					if ( csvlog.is_open() == true ) {
						csvlog << mpid << ";" << phcid << ";" << thisone->tictoc[TICTOC_IONSINROI] << ";" <<
								thisone->tictoc[TICTOC_THREADID] << ";" << thisone->tictoc[TICTOC_PROJECT] << ";" <<
									thisone->tictoc[TICTOC_FFT] << ";" << thisone->tictoc[TICTOC_FINDPKS] << "\n";
					}
					//MK::it->dat remains != NULL to indicate we have results for this one
				} //done writing all desired and populated fields for a specific material point and phase
				//no else because if no results then there were no results achieved for the material point maybe because there were no or not sufficient ions to support the analysis
			} //next result from this thread
		}
	} //next thread of this rank

	if ( csvlog.is_open() == true ) {
		csvlog.flush();
		csvlog.close();
	}

	//strict I/O policy, all matpoints with their results need to end up in file, otherwise we already went out with a false
	double toc = MPI_Wtime();
	memsnapshot mm = araullo_tictoc.get_memoryconsumption();
	araullo_tictoc.prof_elpsdtime_and_mem( "WriteResultsH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;

	//collect XDMF results
	//##MK::see paraprobe-araullo write_results_to_apth5 on how to collect xdmf results
}
*/

int tessellatorHdl::get_myrank()
{
	return this->myrank;
}


int tessellatorHdl::get_nranks()
{
	return this->nranks;
}


void tessellatorHdl::set_myrank( const int rr )
{
	this->myrank = rr;
}


void tessellatorHdl::set_nranks( const int nn )
{
	this->nranks = nn;
}


void tessellatorHdl::init_mpidatatypes()
{
	MPI_Type_contiguous(2, MPI_UNSIGNED_LONG_LONG, &MPI_Ranger_DictKeyVal_Type);
	MPI_Type_commit(&MPI_Ranger_DictKeyVal_Type);

	MPI_Type_contiguous(3, MPI_FLOAT, &MPI_Synth_XYZ_Type);
	MPI_Type_commit(&MPI_Synth_XYZ_Type);

	MPI_Type_contiguous(9, MPI_UNSIGNED_CHAR, &MPI_Ranger_Iontype_Type);
	MPI_Type_commit(&MPI_Ranger_Iontype_Type);

	int elementCounts[2] = {2, 4};
	MPI_Aint displacements[2] = {0, 2 * MPIIO_OFFSET_INCR_F32};
	MPI_Datatype oldTypes[2] = {MPI_FLOAT, MPI_UNSIGNED_CHAR};
	MPI_Type_create_struct(2, elementCounts, displacements, oldTypes, &MPI_Ranger_MQIval_Type);
	MPI_Type_commit(&MPI_Ranger_MQIval_Type);

	int elementCounts2[2] = {1, 2};
	MPI_Aint displacements2[2] = {0, 1 * MPIIO_OFFSET_INCR_F64};
	MPI_Datatype oldTypes2[2] = {MPI_DOUBLE, MPI_UNSIGNED};
	MPI_Type_create_struct(2, elementCounts2, displacements2, oldTypes2, &MPI_VoroCell_Info_Type);
	MPI_Type_commit(&MPI_VoroCell_Info_Type);


	/*int elementCounts[2] = {3, 1};
	MPI_Aint displacements[2] = {0, 3 * MPIIO_OFFSET_INCR_F32};
	MPI_Datatype oldTypes[2] = {MPI_FLOAT, MPI_UNSIGNED};
	MPI_Type_create_struct(2, elementCounts, displacements, oldTypes, &MPI_Ion_Type);
	MPI_Type_commit(&MPI_Ion_Type);*/
}
