//##MK::GPLV3

#ifndef __PARAPROBE_TESSELLATOR_ACQUISOR_H__
#define __PARAPROBE_TESSELLATOR_ACQUISOR_H__

#include "PARAPROBE_TessellatorStructs.h"
//#include "PARAPROBE_TessellatorPhaseCandHdl.h"

//#define SINGLEGPUWORKLOAD					10
//#define NORESULTS_FOR_THIS_MATERIALPOINT	INT32MX
//#define WAIT_UNTIL_GLOBAL_EXTREMUM_FOUND	INT32MX


class tess
{
	//implementing a thread-local object keeping the heavy- and metadata of a thread-local tessellation
public:
	tess();
	~tess();

	bool configure( const int r, const int nr, aabb3d const & globalbox, threadmemory_guarded* const data );
/*
	void set_zbounds( const p6d64 these );
	void set_zbounds( zlim const & these );
	void set_halosize_p3d64( p3d64 const & these );
	void set_halosize_p3d( p3d const & these );
	void set_haloinfo();
	void pull_ionportion_inplace( vector<p3dm3> const & in, aabb3d const & globalbox);
	void pull_ionportion( vector<p3dm3> const & in, aabb3d const & globalbox );
*/
	void tessellate( threadmemory_guarded* const data );

/*
	//maps a thread-local Voronoi cell ID to a global ion ID
	vector<unsigned int> localID2worldID;
	vector<cellstats> cellmeta;
	vector<wallstats> wallmeta;
	vector<string> pvecmeta;
	vector<aabb3d> cellaabb;
	buildstats mystats;

	//heavy topology and geometry data of scientific interest
	vector<voro_io_info> io_info;
	vector<unsigned int> io_ion2cell;
	vector<float> io_cellposition;
	vector<float> io_vol;
	vector<unsigned int> io_nfaces; //##MK::could be made unsigned char...
	vector<unsigned char> io_wall;
	vector<unsigned char> io_ion2type;

#ifndef VALIDZONE_IONS_ONLY
	vector<short> io_halo;
#endif
	vector<size_t> io_topo;
	vector<float> io_geom;			//implicit 1d contiguous triplets of values define one 3d point coordinate

	cell_write_meta myprecisiondemand;
*/
/*
	bool i_store_tess_metainfo;
	bool i_store_tess_cellpos;
	bool i_store_tess_topogeom;
	tessHdl* owner;
	Tree* surfacehull;
	vector<tri3d>* trianglehull;
	//vector<unsigned int>* ion2pp3idx;
	vector<unsigned int>* surfdistance_ids;
	vector<apt_xyz>* surfdistance_vals;
	vector<apt_xyz>* ion2pp3surf;
*/

	vector<wallstats> io_cellwall;
	vector<cellstats> io_cellifo;
	vector<cellprof> io_cellprof;
	vector<int> io_cellnbors;
	buildstats mystats;

private:
	unsigned int roundrobin_rankid;			//modulo remainder ID which portions to compute known global process context
	unsigned int roundrobin_nranks;
	aabb3d globalworld;						//externally detailed bounding box typically entire APT tip
	aabb3d localhalo;						//thread memory region bounding box with halo
	aabb3d localinside;						//thread memory region bounding box without halo
	map<int,cellifo> myions_ifo;			//which ions does this rank have to process?
/*
	//void collect_cell_heavydata( unsigned int jd, cell_write_meta const & info, vector<int> const & nbors,
	//		vector<int> const & fvert, vector<double> const & verts, const double vol );
	cell_write_meta collect_cell_heavydata2( const short cellstatus, const unsigned int jd, vector<int> const & nbors,
			vector<int> const & fvert, vector<double> const & verts );
*/
	p3i identify_blockpartitioning( const size_t p_total, const int p_perblock_target, aabb3d const & roi );
	wallstats identify_cell_wallcontact( vector<int> const & nbors );
	void identify_cell_neighbors( vector<int> const & nbors );
/*
	string identify_cell_pvector( vector<int> const & nbors, vector<int> const & fvert );
	int truncate_cell_wallcontact( const p3d vcenter, voronoicell_neighbor & vcell, aabb3d & vcbox );
	int truncate_cell_wallcontact2( const p3d pion, voronoicell_neighbor & vcell, aabb3d & vcbox );

	aabb3d identify_cell_aabb3d( vector<double> const & verts );
	cell_write_meta identify_cell_storage_demands( vector<int> const & nbors,
			vector<int> const & fvert, vector<double> const & verts);
	void identify_minset_of_coordtriplets( vector<int> const & nbors,
			vector<int> const & fvert, vector<double> const & verts,
			vector<int> & fvert_reindexed, vector<p3d> & verts_triplets );

	vector<p3dm3> ionportion;
	aabb3d mywindow;
	zlim zbounds;
	p3d halosize;
	wallcharacter haloinfo;
	map<string,int> mypvectors;
*/
};

#endif
