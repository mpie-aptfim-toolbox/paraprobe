//##MK::GPLV3

#include "CONFIG_Tessellator.h"

//string ConfigTessellator::InputfilePSE = "";
string ConfigTessellator::InputfileReconstruction = "";
string ConfigTessellator::InputfileHullAndDistances = "";
SPATIAL_SPLITTING_METHOD ConfigTessellator::SpatialSplittingMethod = EQUAL_COUNTS;
apt_real ConfigTessellator::CellErosionDistance = 0.0;

bool ConfigTessellator::IOCellVolume = true;
bool ConfigTessellator::IOCellNeighbors = false;
bool ConfigTessellator::IOCellShape = false;
bool ConfigTessellator::IOCellProfiling = false;

apt_real ConfigTessellator::GuardZoneFactor = 5.0;
int ConfigTessellator::IonsPerBlock = 5;


bool ConfigTessellator::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.Tessellator.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigTessellator")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;

	//WHAT TYPE OF ANALYSIS TO DO
	//InputfilePSE = read_xml_attribute_string( rootNode, "InputfilePSE" );
	InputfileReconstruction = read_xml_attribute_string( rootNode, "InputfileReconstruction" );
	InputfileHullAndDistances = read_xml_attribute_string( rootNode, "InputfileDistances" );
	
	mode = read_xml_attribute_uint32( rootNode, "SpatialSplittingMethodAnalysisMode" );
	switch (mode)
	{
		case EQUAL_WEIGHTS:
			SpatialSplittingMethod = EQUAL_WEIGHTS; break;
		default:
			SpatialSplittingMethod = EQUAL_COUNTS;
	}
	CellErosionDistance = read_xml_attribute_float( rootNode, "CellErosionDistance" );
	
	IOCellVolume = read_xml_attribute_bool( rootNode, "IOCellVolume" );
	IOCellNeighbors = read_xml_attribute_bool( rootNode, "IOCellNeighbors" );
	IOCellShape = read_xml_attribute_bool( rootNode, "IOCellShape" );
	IOCellProfiling = read_xml_attribute_bool( rootNode, "IOCellProfiling" );
	
	return true;
}


bool ConfigTessellator::checkUserInput()
{
	cout << "ConfigTessellator::" << "\n";
	switch(SpatialSplittingMethod)
	{
		case EQUAL_WEIGHTS:
			cout << "Distributing ions on threads based on accumulated distance weight!" << "\n";
		case EQUAL_COUNTS:
			cout << "Distributing ions on threads based on accumulated ion counts!" << "\n";
	}
	if ( CellErosionDistance < EPSILON ) {
		cerr << "CellErosionDistance must be positive and finite!" << "\n"; return false;
	}
	cout << "CellErosionDistance " << CellErosionDistance << "\n";
	
	cout << "IOCellVolume " << IOCellVolume << "\n";
	cout << "IOCellNeighbors " << IOCellNeighbors << "\n";
	cout << "IOCellShape " << IOCellShape << "\n";
	cout << "IOCellProfiling " << IOCellProfiling << "\n";
	
	//cout << "InputfilePeriodicTableOfElements read from " << InputfilePSE << "\n";
	cout << "InputfileReconstruction read from " << InputfileReconstruction << "\n";
	cout << "InputfileDistances read from " << InputfileHullAndDistances << "\n";
	
	cout << "GuardZoneFactor " << GuardZoneFactor << "\n";
	cout << "IonsPerBlock " << IonsPerBlock << "\n";
	cout << "\n";
	return true;
}


void ConfigTessellator::reportSettings( vector<pparm> & res)
{
	//res.push_back( pparm( "InputfilePSE", InputfilePSE, "", "" ) );
	res.push_back( pparm( "InputfileReconstruction", InputfileReconstruction, "", "pre-computed reconstruction space x,y,z, and atom type" ) );
	res.push_back( pparm( "InputfileDistances", InputfileHullAndDistances, "", "pre-computed distances of ions to edge of dataset" ) );
	res.push_back( pparm( "SpatialSplittingMethod", sizet2str(SpatialSplittingMethod), "", "which workload distributing of cells to computing cores" ) );
	res.push_back( pparm( "CellErosionDistance", real2str(CellErosionDistance), "nm", "threshold below which cells close to the edge of the dataset are not computed and their volume set to 0.0" ) );
	res.push_back( pparm( "IOCellVolume", sizet2str(IOCellVolume), "", "whether or not to report the volume of each cell" ) );
	res.push_back( pparm( "IOCellNeighbors", sizet2str(IOCellNeighbors), "", "currently not implemented" ) ); //"whether or not to report the neighbor of each cell" ) );
	res.push_back( pparm( "IOCellShape", sizet2str(IOCellShape), "", "currently not implemented" ) ); //"whether or not to report the shape of each cell" ) );
	res.push_back( pparm( "IOCellProfiling", sizet2str(IOCellProfiling), "", "whether or not to report how long the computations took" ) );
	res.push_back( pparm( "GuardZoneFactor", real2str(GuardZoneFactor), "", "scaling of the halo to each thread region along +-z" ) );
	res.push_back( pparm( "IonsPerBlock", int2str(IonsPerBlock), "", "how many points/ions/atoms to have in each bin of the spatial binning within Voro++" ) );
}
