//##MK::GPLV3

#include "PARAPROBE_TessellatorAcquisor.h"


tess::tess()
{
	mystats = buildstats();

	roundrobin_rankid = MASTER;
	roundrobin_nranks = SINGLEPROCESS;
	globalworld = aabb3d();
	localhalo = aabb3d();
	localinside = aabb3d();
/*
	i_store_tess_metainfo = false;
	i_store_tess_cellpos = false;
	i_store_tess_topogeom = false;

	owner = NULL;
	surfacehull = NULL;
	trianglehull = NULL;
	surfdistance_ids = NULL;
	surfdistance_vals = NULL;
	//ion2pp3surf = NULL;

	mywindow = aabb3d();
	zbounds = zlim();
	halosize = p3d();
	haloinfo = wallcharacter();

	mystats = buildstats();
	myprecisiondemand = cell_write_meta();
*/
}


tess::~tess()
{
	//do not delete owner, surfacehull, trianglehull, ion2pp3idx, ion2pp3surf these are only a backreferences
}


p3i tess::identify_blockpartitioning( const size_t p_total, const int p_perblock_target, aabb3d const & roi )
{
	p3i out = p3i( 1, 1, 1);
	if ( roi.xsz > EPSILON ) {
		apt_real yrel = roi.ysz / roi.xsz;
		apt_real zrel = roi.zsz / roi.xsz;
		apt_real ix = pow(
				(static_cast<apt_real>(p_total) / static_cast<apt_real>(p_perblock_target)) / (yrel * zrel),
				(1.f/3.f) );

		out.x = ( static_cast<int>(floor(ix)) > 0 ) ? static_cast<int>(floor(ix)) : 1;
		apt_real iy = yrel * ix;
		out.y = ( static_cast<int>(floor(iy)) > 0 ) ? static_cast<int>(floor(iy)) : 1;
		apt_real iz = zrel * ix;
		out.z = ( static_cast<int>(floor(iz)) > 0 ) ? static_cast<int>(floor(iz)) : 1;
	}
	return out;
}


wallstats tess::identify_cell_wallcontact( vector<int> const & nbors )
{
	//does cell make boundary contact or not ? (indicated by negative neighbor
	//numeral info of neighbor details which domain wall cuts the cell into a potential incorrect geometry
	wallstats out = wallstats();
	for( auto it = nbors.begin(); it != nbors.end(); ++it ) {
		if ( *it >= 0 ) {
			continue;
		}
		else { //a cell that has boundary contact
			int thisone = *it; //abs(*it); //-1 to 1, -2 to 2, and so forth, Voro++ domain wall neighbors are -1,-2,-3,-4,-5,-6
			switch (thisone)
			{
				case -1:
					out.xmi_touch = true;	out.any_touch = true;	out.halo_insufficient = true;
					break;
				case -2:
					out.xmx_touch = true;	out.any_touch = true;	out.halo_insufficient = true;
					break;
				case -3:
					out.ymi_touch = true;	out.any_touch = true;	out.halo_insufficient = true;
					break;
				case -4:
					out.ymx_touch = true;	out.any_touch = true;	out.halo_insufficient = true;
					break;
				//##MK::we use a partitioning into threadlocal z domains of a global nonperiodic tess
				//##MK::hence check additionally whether haloregion was sufficient for
				//truncating the cell only through the halo regions and not the domain walls
				//##MK::this consistency check identifies whether cells close to the
				//threadlocal domain boundaries have correct geometry
				case -5:
					out.zmi_touch = true;	out.any_touch = true;	out.halo_insufficient = true;
					/*if ( haloinfo.zmi_ishalo == true && out.halo_insufficient == false ) {
						out.halo_insufficient = true;
					}*/
					break;
				case -6:
					out.zmx_touch = true;	out.any_touch = true;	out.halo_insufficient = true;
					/*if ( haloinfo.zmx_ishalo == true && out.halo_insufficient == false ) {
						out.halo_insufficient = true;
					}*/
					break;
				default:
					break;
			}
		}
	} //all neighbors have to be tested to know for sure with which boundaries we make contact
	return out;
}


void tess::identify_cell_neighbors( vector<int> const & nbors )
{
	for( auto it = nbors.begin(); it != nbors.end(); ++it ) {
		io_cellnbors.push_back( *it );
	}
}


bool tess::configure( const int r,  const int nr, aabb3d const & globalbox, threadmemory_guarded* const data )
{
	roundrobin_rankid = r;
	roundrobin_nranks = nr;
	if ( data->ionpp1_org.size() == data->ionifo_org.size() ) {
		globalworld = globalbox;
		for( size_t ityp = 0; ityp < data->ionpp1_org.size(); ityp++ ) {
			if ( data->ionpp1_org.at(ityp) != NULL && data->ionifo_org.at(ityp) != NULL ) {
				vector<p3d> const * pts = data->ionpp1_org.at(ityp);
				vector<p3difo> const * ifo = data->ionifo_org.at(ityp);
				size_t ni = pts->size();
				for( size_t i = 0; i < ni; i++ ) {
					if ( ifo->at(i).pad1 == 0xFF ) { //in the validzone
						//##MK::update the valid zone
						localinside.possibly_enlarge_me( pts->at(i) );
					}
					else { //in the halo zone
						//##MK::update the halo zone
						localhalo.possibly_enlarge_me( pts->at(i) );
					}
				}
			}
			else {
				#pragma omp critical
				{
					cerr << "Rank " << roundrobin_rankid << " thread " << omp_get_thread_num() << " one of ionpp1_org.at(" << ityp << ") and ionifo_org.at(" << ityp << ") is NULL!" << "\n";
				}
				return false;
			}
		}

		//assure that the localhalo encapsulates at least localinside //relevant for top and bottom regions and single-threaded
		localhalo.possibly_enlarge_me( localinside );

		globalworld.scale();
		localhalo.scale();
		localinside.scale();
		return true;
	}
	//else {
	#pragma omp critical
	{
		cerr << "Rank " << roundrobin_rankid << " thread " << omp_get_thread_num() << " ionpp1_org.size() != ionifo_org.size()!" << "\n";
	}
	return false;
}


void tess::tessellate( threadmemory_guarded* const data )
{
	//5.0 as target value based on empirical result by Rycroft, http://math.lbl.gov/voro++/examples/timing_test/
	size_t ionportion = 0;
	for( size_t ityp = 0; ityp < data->ionpp1_org.size(); ityp++ ) {
		if ( data->ionpp1_org.at(ityp) != NULL ) {
			ionportion += data->ionpp1_org.at(ityp)->size();
		}
	}

	if ( ionportion >= static_cast<size_t>(INT32MX-1) ) {
		#pragma omp critical
		{
			cerr << "Rank " << roundrobin_rankid << " thread " << omp_get_thread_num() << " local ionportion exceeds INT32MX-1 which this implementation does not support!" << "\n";
		}
		return;
	}

	if ( roundrobin_rankid > static_cast<unsigned int>(UINT16MX-1) || static_cast<unsigned int>(omp_get_num_threads()) >= static_cast<unsigned int>(UINT16MX-1) ) {
		#pragma omp critical
		{
			cerr << "Rank " << roundrobin_rankid << " thread " << omp_get_thread_num() << " current implementation supports at most UINT16MX-1 processes and threads respectively for now!" << "\n"; //mainly because of cellprof datatype...
		}
		return;
	}


	//blockpartitioning is the essential trick in Voro++ to reduce spatial querying costs
	//too few blocks too many points to test against
	//too many blocks too much memory overhead and cache trashing (L1,L2 and not to forget "page" TLB cache)
	p3i blocks = identify_blockpartitioning( ionportion, ConfigTessellator::IonsPerBlock, localhalo );
	bool periodicbox = false;

	container con(	localhalo.xmi - EPSILON, localhalo.xmx + EPSILON,
					localhalo.ymi - EPSILON, localhalo.ymx + EPSILON,
					localhalo.zmi - EPSILON, localhalo.zmx + EPSILON,
					blocks.x, blocks.y, blocks.z,
					periodicbox, periodicbox, periodicbox, ConfigTessellator::IonsPerBlock );

	buildstats stats = buildstats( blocks.x, blocks.y, blocks.z, ionportion, con.total_particles() );
	int i = 0; //temporary ion ID to map ion position in the container to the implicit order of the above linear walk through along ionpp1 and ionifo data arrays
	//MK::need to start i at zero only then
	//Voro++ .pid() getter will return us this ion ID
	//MK:: DO NOT JUMBLE THIS LOCAL ION ID UP WITH THE GLOBAL ID OF AN ION !
	//MK::only ionifo_org knows this global ion ID !
	for( size_t ityp = 0; ityp < data->ionpp1_org.size(); ityp++ ) {
		if ( data->ionpp1_org.at(ityp) != NULL ) {
			size_t nj = data->ionpp1_org.at(ityp)->size();
			for( size_t j = 0; j < nj; j++ ) {
				con.put( i,
						data->ionpp1_org.at(ityp)->at(j).x,
						data->ionpp1_org.at(ityp)->at(j).y,
						data->ionpp1_org.at(ityp)->at(j).z );

				//MK::work distributing, all process use the same number of threads and load the same global dataset
				//they also use the same spatial partitioning, thus each process only needs to compute each -nth cell
				//all have to be put in the container because they define neighboring cells but not all have to be processed
				//so the tessellation machines of region after region within each region all processes distribute the computation
				//and the storage of the cells!
				p3difo ifo = data->ionifo_org.at(ityp)->at(j);
				if ( ( ifo.IonID % roundrobin_nranks) == roundrobin_rankid ) {
					if ( ifo.pad1 == 0xFF ) {
						myions_ifo.insert( pair<int,cellifo>(i, cellifo(ifo.dist, ifo.IonID, false)) ); //no ghost, real ion
					}
					else {
						myions_ifo.insert( pair<int,cellifo>(i, cellifo(ifo.dist, ifo.IonID, true)) ); //a ghost
					}
				}
				//we need to accumulate local ionID
				i++;
			}
		}
	}

	/*
	#pragma omp critical
	{
		cout << "Rank " << roundrobin_rankid << " Thread " << omp_get_thread_num() << " blocks.xyz " << blocks.x << ";" << blocks.y << ";" << blocks.z << " ionportion " << ionportion << " con.total() " << con.total_particles() << "\n";
	}
	*/

	//temporaries per cell, will be reinitialized during looping over cells

	apt_real derosion = ConfigTessellator::CellErosionDistance;
	unsigned short rr = static_cast<unsigned short>(roundrobin_rankid);
	unsigned short thr = static_cast<unsigned short>(omp_get_thread_num());

	c_loop_all cl(con);
	voronoicell_neighbor c;
	if (cl.start()) { //this is an incremental computing of the tessellation which holds at no point the entire tessellation
		do {
			//Voro++ is not order conserving inasmuch as it not guaranteed that the iteration over cl.start() to end() machines
			//off the cells to ions i in ascending order we should not assume an order
			int clpid = cl.pid();
			map<int,cellifo>::iterator here = myions_ifo.find( clpid );

			if ( here != myions_ifo.end() ) { //an ion for which this rank would at all tessellate
				if ( here->second.ghost == false ) { //an ion not a ghost ion
					mystats.CellsInsideAll++;
					if ( here->second.d >= derosion ) { //an ion sufficiently far enough from the dataset edge
						mystats.CellsInsideNotEroded++;
						if ( con.compute_cell(c, cl) == true ) { //an ion for which the tessellation was successful
							mystats.CellsInsideConstructible++;

							//gather boundary contact information about the cell
							double vol = c.volume(); //MK::cell volume
							vector<int> first_neigh; //MK::number of first order neighbors including negative values in case of domain contact
							c.neighbors(first_neigh);

							vector<int> first_f_vert; //MK::vertices and facets
							c.face_vertices(first_f_vert);
							vector<double> first_v;
							double xx, yy, zz; //##MK::barycenter??
							cl.pos( xx, yy, zz );
							c.vertices(xx, yy, zz, first_v);

							//characterize the cell
							//ConfigTessellator::IOCellWallContact == true
							wallstats health = identify_cell_wallcontact( first_neigh );

							if ( health.any_touch == false ) { //a healthy cell
								mystats.CellsInsideHealthy++;

								io_cellwall.push_back( health );
								int nf = 0;
								int nv = 0; //##MK::not analyzed

								if ( ConfigTessellator::IOCellNeighbors == true ) {
									mystats.NumberOfFacetsProcessed += first_neigh.size();
									nf = ( first_neigh.size() < static_cast<size_t>(INT32MX) ) ? static_cast<int>(first_neigh.size()) : INT32MX;
									identify_cell_neighbors( first_neigh );
								}

								if ( ConfigTessellator::IOCellVolume == true ) {
									io_cellifo.push_back( cellstats(vol, here->second.IonID, nf, nv) );
								}

								if ( ConfigTessellator::IOCellShape == true ) {
									//##MK::compute the cell shape
									//#####################
								}

								if ( ConfigTessellator::IOCellProfiling == true ) {
									cellprof prof = cellprof( rr, thr );
									io_cellprof.push_back( prof );
								}
							} //done with a healthy cell
							else {
								mystats.CellsInsideIll++;
							}
						} //a valid constructed cell
					} //considered and within distance
				} //not a ghost ion
				else {
					mystats.CellsHalo++;
				}
			} //an ion my need to needed to take care of
		} while (cl.inc());
	}

	mystats = stats;
	#pragma omp critical
	{
		cout << "Rank " << rr << " thread " << thr << " blocksxyz " << mystats.BlocksX << ";" << mystats.BlocksY << ";" << mystats.BlocksZ << ";" << " ion/con " << mystats.IonsPerRegion << ";" << mystats.IonsInContainer << "\n";
		cout << "Rank " << rr << " thread " << thr << " io_cellwall/io_cellifo/io_cellprof.size() " << io_cellwall.size() << ";" << io_cellifo.size() << ";" << io_cellprof.size() << "\n";
		cout << "Rank " << rr << " thread " << thr << " ghost/insideall/healthy/ill.size() " << mystats.CellsHalo << ";" << mystats.CellsInsideAll << ";" << mystats.CellsInsideHealthy << ";" << mystats.CellsInsideIll << "\n";
	}
}
