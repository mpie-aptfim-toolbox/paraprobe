//##MK::GPLV3

#ifndef __PARAPROBE_TESSELLATOR_XDMF_H__
#define __PARAPROBE_TESSELLATOR_XDMF_H__

#include "PARAPROBE_TessellatorHDF5.h"

class tessellator_xdmf : public xdmfHdl
{
	//tool-specific sub-class of a XDMF inheriting all methods of the base class xdmfHdl
	//coordinating instance handling all (sequential) writing to XDMF text file to supplement visualization of HDF5 content

public:
	tessellator_xdmf();
	~tessellator_xdmf();

/*
	int create_materialpoint_file( const string xmlfn, const size_t nmp, const string h5ref );
	int create_phaseresults_file( const string xmlfn, const size_t ndir, const string h5ref,
			const unsigned int phcandid, vector<int> const & mpids );
*/

//private:
};

#endif
