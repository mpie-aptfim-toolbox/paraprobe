//##MK::GPLV3

#include "PARAPROBE_TessellatorHdl.h"


//define what is passed upon execution
#define SIMID									1
#define CONTROLFILE								2

//##MK::for querying system maximum physical memory see
//https://stackoverflow.com/questions/2513505/how-to-get-available-memory-c-g

#ifndef GITSHA									//otherwise we could not compile without adding -DGITSHA="d123" a dummy GitSha
	#define GITSHA								0
#endif
//stringification of strings passed from cmake at compile time
//https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html
#define xstr(s)									str(s)
#define str(s) 									#s


void hello( void )
{
	cout << "paraprobe-araullo" << "\n";
	cout << xstr(GITSHA) << "\n";
	cout << "Collecting " << CiteMeTessellator::cite() << " publications for the tool to cite:" << "\n";
	for( auto it = CiteMeTessellator::Citations.begin(); it != CiteMeTessellator::Citations.end(); it++ ) {
		cout << it->author << "\n";
		cout << it->title << "\n";
		cout << it->journal << ", " << it->year << ", " << it->vol << ", pp" << it->pages << "\n";
		cout << it->doi << "\n";
	}
}



bool init(  int pargc, char** pargv )
{
	ConfigShared::SimID = stoul( pargv[SIMID] );
	try {
		ConfigTessellator::readXML(pargv[CONTROLFILE]);
	}
	catch (exception& e) {
		cerr << "\n" << "Unable to parse control file! Details:" << "\n" << e.what() << "\n"; return false;
	}
	if ( ConfigTessellator::checkUserInput() == false ) {
		cerr << "\n" << "Control file settings failed the validity check!" << "\n"; return false;
	}
	else {
		cout << "\n" << "\t\tInput is valid under SimulationID = " << "SimID." <<  ConfigShared::SimID << "\n";
	}
	cout << endl;
	return true;
}


void hybrid_tessellating_of_reconstruction( const int r, const int nr, char** pargv )
{
	//allocate process-level instance of a tessellatorHdl. The instance handles all process-internal
	//processing, the instances synchronize across and communicate with each other during execution
	tessellatorHdl* tes = NULL;

	double ttic = MPI_Wtime();

	int localhealth = 1;
	int globalhealth = nr;
	try {
		tes = new tessellatorHdl;
		tes->set_myrank(r);
		tes->set_nranks(nr);
		tes->init_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Unable to allocate an tessellatorHdl class object instance" << "\n"; localhealth = 0;
	}

	if ( tes->read_periodictable() == true ) {
		cout << "Rank " << r << " reads PSE successfully rng.nuclides.isotopes.size() " << tes->rng.nuclides.isotopes.size() << "\n";
	}
	else {
		cerr << "Rank " << r << " loading of PeriodicTableOfElements failed!" << "\n"; localhealth = 0;
	}

	//##MK::here is the place to inject potential black lists to exclude certain ions

	//do we have all processes on board?
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete tes; tes = NULL; return;
	}
	
	double ttoc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	tes->tess_tictoc.prof_elpsdtime_and_mem( "ReadXMLParameter", APT_XX, APT_IS_SEQ, mm, ttic, ttoc );
	ttic = MPI_Wtime();

	//we have all on board, so read the reconstructed ion coordinates and distances
	//MK::we could have all processes loading from the same file, or have only MASTER do so and broadcast, we opt for the latter strategy
	//##MK::should not be a problem because initialization is orders of magnitude cheaper than actual calculations...
	if ( tes->get_myrank() == MASTER ) {

		if ( tes->read_reconxyz_from_apth5() == true ) {
			cout << "MASTER read successfully all ion coordinates" << "\n";
		}
		else {
			cerr << "MASTER was unable to read ion coordinates !" << "\n"; localhealth = 0;
		}
		if ( tes->read_ranging_from_apth5() == true ) {
			cout << "MASTER read successfully ranging information" << "\n";
		}
		else {
			cerr << "MASTER was unable to read ranging information !" << "\n"; localhealth = 0;
		}
		if ( tes->read_itypes_from_apth5() == true ) {
			cout << "MASTER read successfully iontypes for the xyz coordinates" << "\n";
		}
		else {
			cerr << "MASTER was unable to read iontypes !" << "\n"; localhealth = 0;
		}

		if ( tes->read_dist2hull_from_apth5() == true ) {
			cout << "MASTER read successfully all distances" << "\n";
		}
		else {
			cerr << "WARNING: MASTER was unable to read precomputed distances for all ions so F32MX values are used!" << "\n";
			cerr << "WARNING: This will bias the results at the edge of the dataset" << "\n";
			try {
				tes->dist2hull = vector<apt_real>( tes->xyz.size(), F32MX );
			}
			catch (bad_alloc &mecroak) {
				cerr << "MASTER allocation of dist2hull failed!"; localhealth = 0;
			}
		}

		//##MK::here is the chance to inject precomputed voxelization of the reconstructed domain
	}
	//else {} //slaves processes wait
	//ttoc = MPI_Wtime();
	//mm = tes->tess_tictoc.get_memoryconsumption();
	//tes->tess_tictoc.prof_elpsdtime_and_mem( "ReadMaterialsDataAPTH5", APT_XX, APT_IS_SEQ, mm, ttic, ttoc );
	//ttic = MPI_Wtime();
	//necessary? second order issue wrt to performance for as few as 80 processes like on TALOS...?
	MPI_Barrier( MPI_COMM_WORLD );
	
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that all data have arrived!" << "\n";
		delete tes; tes = NULL; return;
	}
	
	if ( tes->broadcast_reconxyz() == true ) {
		cout << "Rank " << r << " has synchronized reconstruction" << "\n";
	}
	else {
		cerr << "Rank " << r << " failed to synchronize reconstruction" << "\n"; localhealth = 0;
	}
	if ( tes->broadcast_ranging() == true ) {
		cout << "Rank " << r << " has synchronized ranging" << "\n";
	}
	else {
		cerr << "Rank " << r << " failed to synchronize ranging" << "\n"; localhealth = 0;
	}
	if ( tes->broadcast_distances() == true ) { //GB/s bidirectional bandwidth one 4B float per ion, so at most 8 GB ~ a few seconds...
	 	cout << "Rank " << r << " has synchronized distances" << "\n";
	}
	else {
	 	cerr << "Rank " << r << " failed to synchronized distances" << "\n"; localhealth = 0;
	}

	if ( tes->dist2hull.size() != tes->xyz.size() ) {
		cerr << "Rank " << r << " detects we have not a distance value for each ion!" << "\n"; localhealth = 0;
	}
	//wait until all have the necessary data

	MPI_Barrier( MPI_COMM_WORLD );
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes have synchronized the reconstruction, ranging data, and distances!" << "\n";
		delete tes; tes = NULL; return;
	}

	//##MK:check paraprobe-araullo to see how to inject phase candidates
	
	//now processes can proceed and process massively parallel and independently
	//MK::each process computes the same grid and sets the same deterministic a priori known work partitioning for now
	tes->itype_sensitive_spatial_decomposition();

	//##MK::check paraprobe-araullo to see how to distribute

	unsigned int localpoor = tes->execute_local_workpackage(); //0 if all cells were healthy, > 0 when errors or unhealthy cells occurred!

	unsigned int globalpoor = 0;
	MPI_Allreduce(&localpoor, &globalpoor, 1, MPI_UNSIGNED, MPI_SUM, MPI_COMM_WORLD);
	if ( localpoor > 0 ) {
		cerr << "Rank " << r << " found " << localpoor << " cells within target region but incorrect shape!" << "\n";
	}
	if ( globalpoor > 0 ) {
		//report at least timing
		cerr << "Rank " << r << " stopping because some processes found cells with incorrect shape!" << "\n";
		tes->tess_tictoc.spit_profiling( "Tessellator", ConfigShared::SimID, tes->get_myrank() );
		delete tes; tes = NULL; return;
	}

	if ( r == MASTER ) {
		if ( tes->init_target_file() == true ) {
			cout << "Rank " << r << " successfully initialized APTH5 results file" << "\n";
		}
		else {
			cerr << "Rank " << r << " unable to initialize results APTH5 file" << "\n"; localhealth = 0;
		}
		if ( tes->write_environment_and_settings() == true ) {
			cout << "Rank " << MASTER << " environment and settings written!" << "\n";
		}
		else {
			cerr << "Rank " << MASTER << " writing environment and settings failed!" << "\n"; localhealth = 0;
		}
	}
	MPI_Barrier(MPI_COMM_WORLD);

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes have synchronized after generation of the target file!" << "\n";
		delete tes; tes = NULL; return;
	}

	//report volume and some profiling statistics
	if ( tes->write_cell_volume_to_apth5() == true ) {
		cout << "Processes communicated cell volumina and wrote results to HDF5 file!" << "\n";
		cerr << "Cells which were inconstructible, within the eroded distance from the dataset edge were reported to have " << static_cast<double>(0.0) << " volume!" << "\n";
		cerr << "Cells which made boundary contact despite the guard zone were reported to have " << static_cast<double>(0) << " volume!" << "\n";
	}
	else {
		cerr << "Rank " << r << " has recognized that not all results were collectable!" << "\n";
		delete tes; tes = NULL; return;
	}
	
	//##MK::for heavy data see paraprobe-araullo and old paraprobe implementation on www.github.com/mkuehbach/PARAPROBE

	tes->tess_tictoc.spit_profiling( "Tessellator", ConfigShared::SimID, tes->get_myrank() );
	//release resources
	delete tes; tes = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();

	hello();
//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	//helloworld( argc, argv );

	if ( init( argc, argv ) == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks!" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else { 
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}
	cout << r << "-th MPI process initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";
	
//EXECUTE SPECIFIC TASK
	hybrid_tessellating_of_reconstruction( r, nr, argv );
	
//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	
	double toc = omp_get_wtime();
	cout << "paraprobe-tessellator took " << (toc-tic) << " seconds wall-clock time in total" << endl;

	return 0;
}
