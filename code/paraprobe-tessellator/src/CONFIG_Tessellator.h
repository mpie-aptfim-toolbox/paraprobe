//##MK::GPLV3

#ifndef __PARAPROBE_CONFIG_TESSELLATOR_H__
#define __PARAPROBE_CONFIG_TESSELLATOR_H__

//#include "../../paraprobe-utils/src/CONFIG_Shared.h"
#include "../../paraprobe-utils/src/PARAPROBE_VolumeSampler.h"

enum SPATIAL_SPLITTING_METHOD {
	EQUAL_COUNTS,
	EQUAL_WEIGHTS
};


class ConfigTessellator
{
public:
	
	//static string InputfilePSE;
	static string InputfileReconstruction;
	static string InputfileHullAndDistances;
	static SPATIAL_SPLITTING_METHOD SpatialSplittingMethod;	
	static apt_real CellErosionDistance;
	
	static bool IOCellVolume;
	static bool IOCellNeighbors;
	static bool IOCellShape;
	static bool IOCellProfiling;

	static apt_real GuardZoneFactor;
	static int IonsPerBlock;
		
	static bool readXML( string filename = "" );
	static bool checkUserInput();
	static void reportSettings( vector<pparm> & res );
};

#endif
