//##MK::GPLV3

#include "PARAPROBE_TessellatorStructs.h"

/*
ostream& operator<<(ostream& in, cellimplicitkeys const & val)
{
	in << "NCoordinatesVals/NTopologyVals\t\t" << val.ncoordinatevalues << ";" << val.ntopologyvalues << endl;
	return in;
}


ostream& operator<<(ostream& in, cell_write_meta const & val)
{
	in << "Precision/NCellsElementsTopoGeom\t\t" << val.prec << ";" << val.n_cells << ";" << val.n_topo << ";" << val.n_geom << endl;
	return in;
}
*/

ostream& operator<<(ostream& in, buildstats const & val)
{
	in << "CellsHalo " << val.CellsHalo << "\n";
	in << "CellsInsideAll " << val.CellsInsideAll << "\n";
	in << "CellsInsideNotEroded " << val.CellsInsideNotEroded << "\n";
	in << "CellsInsideConstructible " << val.CellsInsideConstructible << "\n";
	in << "CellsInsideHealthy " << val.CellsInsideHealthy << "\n";
	in << "CellsInsideIll " << val.CellsInsideIll << "\n";
	in << "NumberOfFacetsProcessed " << val.NumberOfFacetsProcessed << "\n";
	in << "NumberOfVerticesProcessed " << val.NumberOfVertsProcessed << "\n";
	in << "Voro++ spatial binning " << val.BlocksX << " x " << val.BlocksY << " x " << val.BlocksZ << "\n";
	in << "IonsInRegion/IonsInVoro++Container " << val.IonsPerRegion << ";" << val.IonsInContainer << "\n";
	return in;
}


ostream& operator << (ostream& in, wallstats const & val) {
	in << val.xmi_touch << ";" << val.xmx_touch << "--" << val.ymi_touch << ";" << val.ymx_touch << "--" << val.zmi_touch << ";" << val.zmx_touch << "--" << val.any_touch << "--" << val.halo_insufficient << "\n";
	return in;
}


ostream& operator << (ostream& in, cellifo const & val) {
	in << val.d << ";" << val.IonID << ";" << val.ghost << "\n";
	return in;
}



ostream& operator << (ostream& in, cellstats const & val) {
	in << val.volume << ";" << val.IonID << ";" << val.nfaces << "\n"; //val.x << ";" << val.y << ";" << val.z << ";" <<
	return in;
}


ostream& operator << (ostream& in, cellprof const & val) {
	in << val.rank << ";" << val.threadid << "\n";
	return in;
}



/*
ostream& operator << (ostream& in, elaz const & val) {
	in << val.elevation << ";" << val.azimuth << "\n";
	return in;
}


ostream& operator << (ostream& in, iontype_ival const & val) {
	in << "[ " << val.incl_left << ", " << val.excl_right << ") m = " << val.m << "\n";
	return in;
}


ostream& operator << (ostream& in, binning_info const & val) {
	in << "ROIRadius " << val.R << "\n";
	in << "Histogram dR " << val.dR << "\n";
	in << "Histogram mult " << val.binner << "\n";
	in << "Histogram/2 mult " << val.binner2 << "\n";
	in << "Histogram RdR " << val.RdR << "\n";
	in << "Histogram pad " << val.Rpadding << "\n";
	in << "Histogram nbins " << val.NumberOfBins << "\n";
	in << "FFT nbins half " << val.NumberOfBinsHalf << "\n";
	return in;
}


ostream& operator << (ostream& in, threepeaks const & val) {
	in << val.max1.first << ";" << val.max1.second << "\n";
	in << val.max2.first << ";" << val.max2.second << "\n";
	in << val.max3.first << ";" << val.max3.second << "\n";
	return in;
}
*/
