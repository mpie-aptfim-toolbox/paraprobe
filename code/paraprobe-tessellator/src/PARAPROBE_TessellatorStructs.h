//##MK::GPLV3

#ifndef __PARAPROBE_TESSELLATOR_STRUCTS_H__
#define __PARAPROBE_TESSELLATOR_STRUCTS_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_TessellatorVoroXXInterface.h"


/*

//user defined testing parameter
#define GUARDZONE_ION			2
#define VALIDZONE_ION			1
#define DO_NOT_DEREFERENCE		UINT32MX
//##MK::#define LDRVEX					25.0		//nm
//##MK::#define LDRVEY					25.0
//##MK::#define LDRVEZ					250.0


//MK::given the specific case of Voronoi tessellations to atoms and provided the following holds:
//+no Voronoi cell has more than 255 unique coordinate values of its vertices
//+no Voronoi cell needs more than 255 individual values to encode its face topology
//we can use the unsigned char datatype instead of int to store metadata and topological pieces of info
//thereby compressing the dataset inplace by a factor of 4x
#define IMPLICIT_STORING_UCHAR			1
#define IMPLICIT_STORING_UINT32			2
#define IMPLICIT_STORING_UINT64			3

#define TESS_BOUNDARYCORR_UNNECESSARY	+2
#define TESS_BOUNDARYCORR_SUCCESS		+1
#define TESS_BOUNDARYCORR_NOTRIANGLES	-1
#define TESS_BOUNDARYCORR_NOCUTPLANE	-2
#define TESS_BOUNDARYCORR_CELLDELETED	-3
#define TESS_BOUNDARYCORR_FAILED		-4

struct cellimplicitkeys
{
	unsigned char ntopologyvalues;			//how many individual topological pieces of information
	unsigned char ncoordinatevalues;		//how many individual coordinate values
	//##MK::this has 6B trailing padding but for now lets accept it, to HDF5 will write only array of unsigned char pairs!
};

ostream& operator<<(ostream& in, cellimplicitkeys const & val);

struct cell_write_meta
{
	//precision demand
	size_t prec;			//which precision is necessary to store topology info of the cell
	size_t n_cells;			//how many cells we have?
	size_t n_topo;			//how many elements with given precision for topology required
	size_t n_geom;			//how many elements with given precision for geometry required
	cell_write_meta() : prec(0), n_cells(0), n_topo(0), n_geom(0) {}
	cell_write_meta(const size_t _pr, const size_t _nc, const size_t _nt, const size_t _ng) :
		prec(_pr), n_cells(_nc), n_topo(_nt), n_geom(_ng) {}
};

ostream& operator<<(ostream& in, cell_write_meta const & val);
*/

struct buildstats
{
	size_t CellsHalo;
	size_t CellsInsideAll;
	size_t CellsInsideNotEroded;
	size_t CellsInsideConstructible;
	size_t CellsInsideHealthy;
	size_t CellsInsideIll;
	size_t NumberOfFacetsProcessed;
	size_t NumberOfVertsProcessed;
	size_t BlocksX;
	size_t BlocksY;
	size_t BlocksZ;
	size_t IonsPerRegion;
	size_t IonsInContainer;

	buildstats() : CellsHalo(0), CellsInsideAll(0), CellsInsideNotEroded(0), CellsInsideConstructible(0), CellsInsideHealthy(0),
			CellsInsideIll(0), NumberOfFacetsProcessed(0), NumberOfVertsProcessed(0),
			BlocksX(0), BlocksY(0), BlocksZ(0), IonsPerRegion(0), IonsInContainer(0) {}
	buildstats( const int _blx, const int _bly, const int _blz, const size_t _ireg, const int _icon ) :
			CellsHalo(0), CellsInsideAll(0), CellsInsideNotEroded(0), CellsInsideConstructible(0), CellsInsideHealthy(0), CellsInsideIll(0),
					NumberOfFacetsProcessed(0), NumberOfVertsProcessed(0),
					BlocksX(_blx), BlocksY(_bly), BlocksZ(_blz), IonsPerRegion(_ireg), IonsInContainer(_icon) {}
	buildstats(const size_t _nchal, const size_t _ncinall, const size_t _ncinnero,
			const size_t _ncinconst, const size_t _ncingood, const size_t _ncinill, const size_t _nfprc, const size_t _nvprc ) :
					CellsHalo(_nchal), CellsInsideAll(_ncinall), CellsInsideNotEroded(_ncinnero),
					CellsInsideConstructible(_ncinconst), CellsInsideHealthy(_ncingood), CellsInsideIll(_ncinill),
					NumberOfFacetsProcessed(_nfprc), NumberOfVertsProcessed(_nvprc),
					BlocksX(0), BlocksY(0), BlocksZ(0), IonsPerRegion(0), IonsInContainer(0) {}
};

ostream& operator<<(ostream& in, buildstats const & val);



struct wallstats
{
	//captures logical information which threadlocal domain walls cut the cell
	bool xmi_touch;
	bool xmx_touch;
	bool ymi_touch;
	bool ymx_touch;
	bool zmi_touch;
	bool zmx_touch;
	bool any_touch;
	bool halo_insufficient;		//if true indicates that halo was insufficient and at least
	//if halo_insufficient == true re-tessellation with larger halo guard width is required
	wallstats() : 	xmi_touch(false), xmx_touch(false),
					ymi_touch(false), ymx_touch(false),
					zmi_touch(false), zmx_touch(false),
					any_touch(false), halo_insufficient(false) {}
	wallstats(const bool _xmi, const bool _xmx, const bool _ymi, const bool _ymx,
			const bool _zmi, const bool _zmx, const bool _any, const bool _haloproblem ) :
				xmi_touch(_xmi), xmx_touch(_xmx),
				ymi_touch(_ymi), ymx_touch(_ymx),
				zmi_touch(_zmi), zmx_touch(_zmx),
				any_touch(_any), halo_insufficient(_haloproblem) {}
};

ostream& operator << (ostream& in, wallstats const & val);


/*
struct wallcharacter
{
	//carries information whether the threadlocal domain for which the tessellation is
	//computed is logically connected to neighboring domains
	//_ishalo
	//-->if true means there is a neighbor domain wall is a halo boundary
	//-->else false means there is no neighbor domain, wall marks end of global nonperiodic dataset
	bool xmi_ishalo;
	bool xmx_ishalo;
	bool ymi_ishalo;
	bool ymx_ishalo;
	bool zmi_ishalo;
	bool zmx_ishalo;
	bool pad1;
	bool pad2;
	wallcharacter() : 	xmi_ishalo(false), xmx_ishalo(false),
						ymi_ishalo(false), ymx_ishalo(false),
						zmi_ishalo(false), zmx_ishalo(false),
						pad1(false), pad2(false) {}
	wallcharacter(const bool _xmi, const bool _xmx,
			const bool _ymi, const bool _ymx,
			const bool _zmi, const bool _zmx ) :
				xmi_ishalo(_xmi), xmx_ishalo(_xmx),
				ymi_ishalo(_ymi), ymx_ishalo(_ymx),
				zmi_ishalo(_zmi), zmx_ishalo(_zmx),
				pad1(false), pad2(false) {}
};
*/

struct cellifo
{
	apt_real d;
	unsigned int IonID;
	bool ghost;
	bool pad1;
	bool pad2;
	bool pad3;
	cellifo() : d(F32MX), IonID(UINT32MX), ghost(false), pad1(false), pad2(false), pad3(false) {}
	cellifo( const apt_real _d, const unsigned int _ionid, const unsigned int _ghost ) :
		d(_d), IonID(_ionid), ghost(_ghost), pad1(false), pad2(false), pad3(false) {}
};

ostream& operator << (ostream& in, cellifo const & val);


struct cellstats
{
	/*double x;
	double y;
	double z;*/
	double volume;
	unsigned int IonID;
	int nfaces;
	int nverts;
	cellstats() :  volume(0.0), IonID(UINT32MX), nfaces(0), nverts(0) {} //x(0.0), y(0.0), z(0.0),
	cellstats( const double _v, const unsigned int _ionid, const int _nf, const int _nv) :
			volume(_v), IonID(_ionid), nfaces(_nf), nverts(_nv) {} //const double _x, const double _y, const double _z,	x(_x), y(_y), z(_z),
};

ostream& operator << (ostream& in, cellstats const & val);


struct cellprof
{
	unsigned short rank;
	unsigned short threadid;
	cellprof() : rank(MASTER), threadid(MASTER) {}
	cellprof( const unsigned short _r, const unsigned short _thr ) : rank(_r), threadid(_thr) {}
};

ostream& operator << (ostream& in, cellprof const & val);


#endif
