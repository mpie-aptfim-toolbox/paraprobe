//##MK::GPLV3

#ifndef __PARAPROBE_TESSELLATOR_CITEME_H__
#define __PARAPROBE_TESSELLATOR_CITEME_H__

#include "CONFIG_Tessellator.h"


class CiteMeTessellator
{
public:
	
	static vector<bibtex> Citations;
	
	static size_t cite();
};

#endif

