//##MK::GPLV3

#ifndef __PARAPROBE_DBSCAN_HDL_H__
#define __PARAPROBE_DBSCAN_HDL_H__

//here is the level where we loop in all tool-specific utils
//#include "PARAPROBE_DBScanAcquisor.h"

#include "PARAPROBE_DBScanTaskHdl.h"


class dbscanHdl //: public ioutilsHdl
{
	//process-level class which implements the worker instance which executes DBScan analyses
	//tasks are distributed round robin across processes, individual DBScans performed using OpenMP

public:
	dbscanHdl();
	~dbscanHdl();

	bool read_periodictable();
	bool read_iontype_combinations( string fn );
	bool read_reconxyz_from_apth5();
	bool read_ranging_from_apth5();
	bool read_itypes_from_apth5();

	bool read_dist2hull_from_apth5();
	
	bool broadcast_reconxyz();
	bool broadcast_ranging();
	bool broadcast_distances();

	bool define_analysis_tasks();
	//void randomize_iontype_labels();
	//void itype_sensitive_spatial_decomposition();
	//bool prepare_processlocal_resultsbuffer();

	void execute_local_workpackage();

	//void distribute_matpoints_on_processes();

	bool init_target_file();
	bool write_environment_and_settings();
	bool write_metadata_for_all_analysis_tasks();
	bool write_data_for_visualizing_cluster();
	bool write_count_distributions_cluster();
	bool communicate_analysis_tasks_results();
	bool write_xdmf_metadata();
	bool write_local_results();
	//bool collect_results_on_masterprocess();
	//bool write_materialpoints_to_apth5();

	int get_myrank();
	int get_nranks();
	void set_myrank( const int rr );
	void set_nranks( const int nn );
	void init_mpidatatypes();

	rangeTable rng;
	itypeCombiHdl itsk;
	vector<DBScanTask> results;
	vector<MPI_DBScan_Task> xdmfifo;
	unsigned char maximum_iontype_uc;

/*
	decompositor sp;
	volsampler mp;
	//vector<int> mp2rk;						//mapping of material points => to ranks,
	//vector<int> mp2me;
	//vector<p3dm1> reconstruction;
*/

	vector<p3d> xyz;							//the reconstructed and ranged ion point cloud
	vector<unsigned char> ityp_org;				//iontypes ranged, original
	vector<apt_real> dist2hull;

	h5Hdl inputReconH5Hdl;
	h5Hdl inputDistH5Hdl;
	dbscan_h5 debugh5Hdl;
	dbscan_xdmf debugxdmf;
	vector<DBScanXDMF> xdmfattr;
	
	//vector<acquisor*> workers;

	profiler dbscan_tictoc;

private:
	//MPI related
	int myrank;								//my MPI ID in the MPI_COMM_WORLD
	int nranks;

	MPI_Datatype MPI_Ion_Type;
	MPI_Datatype MPI_Ranger_DictKeyVal_Type;
	MPI_Datatype MPI_Synth_XYZ_Type;
	MPI_Datatype MPI_Ranger_Iontype_Type;
	MPI_Datatype MPI_Ranger_MQIval_Type;
	MPI_Datatype MPI_DBScan_Task_Type;
};


#endif

