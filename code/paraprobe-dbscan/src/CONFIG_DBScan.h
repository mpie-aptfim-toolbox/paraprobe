//##MK::GPLV3

#ifndef __PARAPROBE_CONFIG_DBSCAN_H__
#define __PARAPROBE_CONFIG_DBSCAN_H__

//#include "../../paraprobe-utils/src/CONFIG_Shared.h"
#include "../../paraprobe-utils/src/PARAPROBE_Histogram.h"
#include "../../paraprobe-utils/src/PARAPROBE_SDM3D.h"
#include "../../paraprobe-utils/src/PARAPROBE_FFT.h"

//##MK::ostream how to

/*
pair<string,bool> parse_knn_neighbors( const string in, vector<unsigned int> & out );
*/


struct lival_real
{
	apt_real min;
	apt_real incr;
	apt_real max;
	lival_real() : min(0.f), incr(1.f), max(0.f) {}
	lival_real( const apt_real _mi, const apt_real _inc, const apt_real _mx ) :
		min(_mi), incr(_inc), max(_mx) {}
};


struct lival_u32
{
	unsigned int min;
	unsigned int incr;
	unsigned int max;
	lival_u32() : min(0.f), incr(1.f), max(0.f) {}
	lival_u32( const unsigned int _mi, const unsigned int _inc, const unsigned int _mx ) :
		min(_mi), incr(_inc), max(_mx) {}
};

enum DBSCAN_MODE {
	NOTHING,
	NORMAL,
	MAXSEP
};


class ConfigDBScan
{
public:
	
	//static string InputfilePSE;
	static string InputfileReconstruction;
	static string InputfileHullAndDistances;
	
	static DBSCAN_MODE ClusteringMethod;

	static lival_real Epsilon;
	static lival_u32 MinPtsKth;
	static lival_u32 MaxSepMinPts;
	
	//maximum-separation method
	static lival_real MaxSepEroDist;
	static apt_real DatasetEdgeThresholdDistance;

	static bool IOStoreClusterIDs;
	static bool IOStoreClusters;
		
	//internals
	static size_t MaxSizeCachedResPerNode;

	static bool readXML( string filename = "" );
	static bool checkUserInput();
	static void reportSettings( vector<pparm> & res );
};

#endif
