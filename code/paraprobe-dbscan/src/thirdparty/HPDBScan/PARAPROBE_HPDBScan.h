//##MK::GPLV3

/*
	Markus G\"otz et al. - HPDBScan
	https://doi.org/10.1145/2834892.2834894
	http://swmath.org/software/30118
*/


#ifndef __PARAPROBE_HPDBSCAN_H__
#define __PARAPROBE_HPDBSCAN_H__

//#include "../../paraprobe-utils/src/PARAPROBE_Histogram.h"
//#include "../../paraprobe-utils/src/PARAPROBE_SDM3D.h"
#include "../../../../paraprobe-utils/src/PARAPROBE_Datatypes.h"

//#define HPDBSCAN_VERBOSE

struct pntcnt
{
	unsigned int id;
	unsigned int cnts;
	pntcnt() : id(0), cnts(0) {}
	pntcnt( const unsigned int _id, const unsigned int _cnts ) : id(_id), cnts(_cnts) {}
};

inline bool SortForAscCnt(const pntcnt & a, const pntcnt & b)
{
	return a.cnts < b.cnts;
}


struct particle
{
	p3d64 barycenter;
	unsigned int nions;
	unsigned int id;
	bool boundarycontact;
	bool pad1;
	bool pad2;
	bool pad3;
	particle() : barycenter(p3d()), nions(0), id(UINT32MX), boundarycontact(false), pad1(false), pad2(false), pad3(false) {}
	particle( const p3d64 _bc, const unsigned int _nions, const unsigned int _id, const bool _bnd ) : barycenter(_bc), nions(_nions),
			id(_id), boundarycontact(_bnd), pad1(false), pad2(false), pad3(false) {}
};

ostream& operator << (ostream& in, particle const & val);


inline bool SortParticleForAscCnt(const particle & a, const particle & b)
{
	return a.nions < b.nions;
}

/*
#include "PARAPROBE_TAPSimHdl.h"
*/

#ifndef HPDBSCAN_UTIL_H
#define HPDBSCAN_UTIL_H

/*
#include <set>
#include <numeric>
#include <cstdlib>
#include <unistd.h>
#include <unordered_set>
*/

#define THREE_DIMENSIONS	3

//typedef p3dm1	HPDBPoint;


/**
 * Atomic Operations
 */
template <typename T>
void atomicMin(T* address, T value)
{
    T previous = __sync_fetch_and_add(address, 0);

    while (previous > value) {
        if  (__sync_bool_compare_and_swap(address, previous, value))
        {
            break;
        }
        else
        {
            previous = __sync_fetch_and_add(address, 0);
        }
    }
}

/*
 * Output
 */
template <typename T>
std::ostream& operator<<(std::ostream& os, const std::set<T>& set)
{
    os << "{";
    std::copy(set.begin(), set.end(), std::ostream_iterator<T>(os, ", "));
    os << "}";
    return os;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec)
{
    os << "[";
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<T>(os, ", "));
    os << "]";
    return os;
}

template <typename T, typename U>
std::ostream& operator<<(std::ostream& os, const std::pair<T, U>& pair)
{
    os << "[" << pair.first << " : " << pair.second << "]";
    return os;
}

template <typename T, typename U>
std::ostream& operator<<(std::ostream& os, const std::map<T, U>& map)
{
    for (auto& pair : map)
    {
        os << pair << std::endl;
    }
    return os;
}

#endif // UTIL_H

////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef HPDBSCAN_POINTS_H
#define HPDBSCAN_POINTS_H


#define REORDER true

const ssize_t NOT_VISITED = std::numeric_limits<ssize_t>::max();
const ssize_t NOISE       = std::numeric_limits<ssize_t>::max() - 1;


struct HPDBPoint
{
	apt_real x;
	apt_real y;
	apt_real z;
	ssize_t m;			//encodes the signed (global ion ID + 1), the sign is negative if the ion is within threshold to the dataset edge, if positive it is inside
	HPDBPoint() : x(0.0), y(0.0), z(0.0), m(0) {}
	HPDBPoint( const apt_real _x, const apt_real _y, const apt_real _z, const ssize_t _m ) : x(_x), y(_y), z(_z), m(_m) {}
};


std::ostream& operator<<(std::ostream& os, const HPDBPoint& pp3 );


struct HPDBPointRes
{
	ssize_t m1;			//cluster ID, if negative core point, if positive eps-reachable point, if 0 noise point
	apt_real x;
	apt_real y;
	apt_real z;
	unsigned int m2;	//global ion id

	HPDBPointRes() : m1(NOISE), x(0.0), y(0.0), z(0.0), m2(0) {}
	HPDBPointRes( const ssize_t _m1, const apt_real _x, const apt_real _y, const apt_real _z, const unsigned int _m2 ) :
		m1(_m1), x(_x), y(_y), z(_z), m2(_m2) {}
};


class Pointz
{
    size_t m_size;

    std::vector<size_t>  m_cells;
    std::vector<ssize_t> m_clusters;
    std::vector<HPDBPoint>   m_points;
    std::vector<size_t>  m_reorder;

public:
    Pointz(size_t size);

    /**
     *  Iteration
     */
    inline std::vector<HPDBPoint>::iterator begin()
    {
        return this->m_points.begin();
    }

    inline std::vector<HPDBPoint>::const_iterator begin() const
    {
        return this->m_points.begin();
    }

    inline std::vector<HPDBPoint>::iterator end()
    {
        return this->m_points.end();
    }

    inline std::vector<HPDBPoint>::const_iterator end() const
    {
        return this->m_points.end();
    }

    /**
     *  Access
     */
    inline const size_t cell(size_t index) const
    {
        return this->m_cells[index];
    }

    inline const ssize_t cluster(const size_t index) const
    {
        return std::abs(this->m_clusters[index]);
    }

    inline bool corePoint(const size_t index) const
    {
        return this->m_clusters[index] < 0;
    }

    inline HPDBPoint& operator[](size_t index)
    {
        return this->m_points[index];
    }

    inline const size_t size() const
    {
        return this->m_size;
    }

    /**
     * Modifiers
     */
    inline void cell(size_t index, size_t number)
    {
        this->m_cells[index] = number;
    }

    inline void cluster(const size_t index, ssize_t value, bool core)
    {
        atomicMin(&this->m_clusters[index], core ? -value : value);
    }

    inline void overrideCluster(const size_t index, ssize_t value, bool core)
    {
        this->m_clusters[index] = core ? -value : value;
    }

    inline void overrideCluster(const size_t index, ssize_t value)
    {
    	this->m_clusters[index] = value;
    }

    /**
     * Operations
     */
    void sortByCell(size_t maxDigits);
    void reorder(size_t maxDigits);
};

/**
 * Output
 */
std::ostream& operator<<(std::ostream& os, const Pointz& points);

#endif // POINTS_H



////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef HPDBSCAN_SPACE_H
#define HPDBSCAN_SPACE_H


typedef std::map< size_t, size_t >                    CellCounter;
typedef std::map< size_t, std::pair<size_t, size_t> > CellIndex;

class Space
{
    CellIndex           m_cellIndex;

    std::vector<size_t> m_cells;
    HPDBPoint				m_maximum;
    HPDBPoint				m_minimum;

    Pointz&             m_points;
    size_t              m_total;

    /**
     * Initialization
     */
    void computeCells(float epsilon);
    bool probeDimensions(float epsilon, aabb3d & dims );
    void computeDimensions(float epsilon, aabb3d & dims );


public:
    Space(Pointz& points, float epsilon);

    /**
     * Access
     */
    inline const CellIndex& cellIndex() const
    {
        return this->m_cellIndex;
    }

    inline const std::vector<size_t>& cells() const
    {
        return this->m_cells;
    }

    inline size_t cells(size_t dimension) const
    {
        return this->m_cells[dimension];
    }

    inline const HPDBPoint& max() const
    {
        return this->m_maximum;
    }

    /*inline float max(size_t dimension) const
    {
        return this->m_maximum[dimension];

    }*/

    inline const HPDBPoint& min() const
    {
        return this->m_minimum;
    }

    /*inline float min(size_t dimension) const
    {
        return this->m_minimum[dimension];
    }*/

    inline size_t total() const
    {
        return this->m_total;
    }

    /**
     * Operations
     */
    std::vector<size_t> getNeighbors(const size_t cellId) const;
    size_t regionQuery(const size_t pointIndex, const size_t cell, const std::vector<size_t>& neighborPoints, const float EPS2, std::vector<size_t>& minPointsArea) const;
};

/**
 * Output
 */
std::ostream& operator<<(std::ostream& os, const Space& space);

#endif // SPACE_H



#ifndef HPDBSCAN_RULES_H
#define	HPDBSCAN_RULES_H


class Rules
{
    std::map<ssize_t, ssize_t> m_rules;

public:
    Rules();

    const std::map<ssize_t, ssize_t>::const_iterator begin() const
    {
        return this->m_rules.begin();
    }

    const std::map<ssize_t, ssize_t>::const_iterator end() const
    {
        return this->m_rules.end();
    }

    ssize_t rule(const size_t index) const;
    bool update(const ssize_t first, const ssize_t second);
};

void merge(Rules& omp_out, Rules& omp_in);
#pragma omp declare reduction(merge: Rules: merge(omp_out, omp_in)) initializer(omp_priv(omp_orig))

#endif	// RULES_H



///////////////////////////////////////////////////////////////////////////////
#ifndef HPDBSCAN_HPDBSCAN_H
#define HPDBSCAN_HPDBSCAN_H

struct dbscanres
{
	//##MK::quick and dirty
	size_t nCore;
	size_t nClustered;
	size_t nNoise;
	size_t nClusterFound;

	size_t Nmin;
	apt_real Dmax;

	dbscanres() : nCore(0), nClustered(0), nNoise(0),
			nClusterFound(0), Nmin(0), Dmax(0.f) {}
	dbscanres(const size_t _nCore, const size_t _nClu, const size_t _nNoi,
			const size_t _nTot, const size_t _Nm, const apt_real _Dm) :
		nCore(_nCore), nClustered(_nClu), nNoise(_nNoi),
		nClusterFound(_nTot), Nmin(_Nm), Dmax(_Dm) {}
};

class HPDBSCAN
{
public:
    HPDBSCAN( const std::vector<HPDBPoint>& ions );
    /*
    HPDBSCAN(const std::string& filename);
    */
    HPDBSCAN( float* data, size_t size );
    void scan(float epsilon, size_t minPoints);
    void readout_cluster( vector<HPDBPointRes> & pnts );
    void characterize_cluster( const unsigned int minpts_to_be_significant, vector<particle> & prec );
    void count_cluster( vector<pntcnt> & tbl );
    /*
    unsigned int flag_as_clustered( const unsigned int typid, const unsigned int maxtypid );
    void writeFile(const std::string& outpuFilePath);
    */
    ~HPDBSCAN();

    Pointz m_points;

    /*
     * Internal Operations
     */
    void applyRules(const Rules& rules);
    Rules localDBSCAN(const Space &space, float epsilon, size_t minPoints);
    Pointz readIons(const std::vector<HPDBPoint> & ions );
    /*
    Pointz readFile(const std::string& filename);
    dbscanres summarize1( TypeSpecDescrStat const & tskdetails, bool const * interior, sqb & vgrd,
    		vector<p3dm1> & ioncloud, const unsigned int maxtypeid, const unsigned int tid, const unsigned int rid );*/
    /*
    dbscanres summarize2( TypeSpecDescrStat const & tskdetails, bool const * interior, sqb & vgrd,
        		vector<p3dm1> & ioncloud, const unsigned int maxtypeid, const unsigned int tid, const unsigned int rid,
				h5Hdl & iohdl );
	*/

//public:

};

#endif // HPDBSCAN_H

/*
struct HPDBParms
{
    std::string file;
    std::string out;
    //size_t      threads;
    size_t      minPoints;
    float       epsilon;

    HPDBParms() :
        file("data.csv"),
        out("out.csv"),
        minPoints(10), epsilon(0.01) {}
    	//threads(omp_get_max_threads()),
    HPDBParms(const float _eps, const size_t _minp) :
    	file(""), out(""), minPoints(_minp), epsilon(_eps)  {} //threads(omp_get_max_threads()),
};
*/

/*
struct cl3d
{
	cl3d() : id(0), n(0) {}
	cl3d(const size_t _id, const long _n) : id(_id), n(_n) {}

	void set_boundary_precipitate()
	{
		this->n *= -1;
	}
	bool has_boundary_contact() const
	{
		if ( this->n > -1 )
			return false;
		else
			return true;
	}
	p3d center_of_gravity() const
	{

		if ( this->members.size() > 0 ) {
			p3d cgrv = p3d();
			size_t n = 0;
			for( auto it = this->members.begin(); it != this->members.end(); ++it, ++n ) {
				cgrv.x += it->x;
				cgrv.y += it->y;
				cgrv.z += it->z;
			}
			cgrv.x /= static_cast<apt_real>(n);
			cgrv.y /= static_cast<apt_real>(n);
			cgrv.z /= static_cast<apt_real>(n);
			return cgrv;
		}
		else
			return p3d();
	}

	vector<p3dm1> members;
	size_t id;
	long n;
};


class clusterHdl
{
	//instance organizing thread-local processing of cluster
public:
	clusterHdl();
	~clusterHdl();

	void boundary_contact_analysis( bool const * inside, sqb & vgrd );
	void report_size_distr_csv( const string whichtarget, const string againstwhich,
			const unsigned int tid, const unsigned int runid );
	void report_size_distr2( const string whichtarget, const string againstwhich,
			const unsigned int tid, const unsigned int runid, const bool excludeboundary );
	void report_size_distr_vtk( const string whichtarget, const string againstwhich,
			const unsigned int tid, const unsigned int runid );
	void report_size_distr_hdf5( const string whichtarget, const string againstwhich,
			const unsigned int tid, const unsigned int runid, h5Hdl & h5io );

    inline const size_t population( const bool excludeboundary ) const
    {
        if ( excludeboundary == false ) {
        	return this->precipitates.size();
        }
        else {
        	size_t pop = 0;
        	for( auto it = this->precipitates.begin(); it != this->precipitates.end(); ++it ) {
        		if ( it->has_boundary_contact() == false )
        			++pop;
        	}
        	return pop;
        }
    }

	vector<cl3d> precipitates;
};
*/

#endif
