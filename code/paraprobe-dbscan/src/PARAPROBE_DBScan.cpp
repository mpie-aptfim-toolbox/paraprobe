//##MK::GPLV3

#include "PARAPROBE_DBScanHdl.h"


//define what is passed upon execution
#define SIMID									1
#define CONTROLFILE								2

//##MK::for querying system maximum physical memory see
//https://stackoverflow.com/questions/2513505/how-to-get-available-memory-c-g

#ifndef GITSHA									//otherwise we could not compile without adding -DGITSHA="d123" a dummy GitSha
	#define GITSHA								0
#endif
//stringification of strings passed from cmake at compile time
//https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html
#define xstr(s)									str(s)
#define str(s) 									#s


void hello( void )
{
	cout << "paraprobe-dbscan" << "\n";
	cout << xstr(GITSHA) << "\n";
	cout << "Collecting " << CiteMeDBScan::cite() << " publications for the tool to cite:" << "\n";
	for( auto it = CiteMeDBScan::Citations.begin(); it != CiteMeDBScan::Citations.end(); it++ ) {
		cout << it->author << "\n";
		cout << it->title << "\n";
		cout << it->journal << ", " << it->year << ", " << it->vol << ", pp" << it->pages << "\n";
		cout << it->doi << "\n";
	}
}


bool init(  int pargc, char** pargv )
{
	ConfigShared::SimID = stoul( pargv[SIMID] );
	try {
		ConfigDBScan::readXML(pargv[CONTROLFILE]);
	}
	catch (exception& e) {
		cerr << "\n" << "Unable to parse control file! Details:" << "\n" << e.what() << "\n"; return false;
	}
	if ( ConfigDBScan::checkUserInput() == false ) {
		cerr << "\n" << "Control file settings failed the validity check!" << "\n"; return false;
	}
	else {
		cout << "\n" << "\t\tInput is valid under SimulationID = " << "SimID." <<  ConfigShared::SimID << "\n";
	}
	cout << endl;
	return true;
}


void run_highthroughput_dbscan_variants_on_reconstruction( const int r, const int nr, char** pargv )
{
	//allocate process-level instance of a dbscanHdl. The instance handles all process-internal
	//processing, the instances synchronize across and communicate with each other during execution
	dbscanHdl* dbsc = NULL;

	double ttic = MPI_Wtime();

	int localhealth = 1;
	int globalhealth = nr;
	try {
		dbsc = new dbscanHdl;
		dbsc->set_myrank(r);
		dbsc->set_nranks(nr);
		dbsc->init_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Unable to allocate an dbscan class object instance" << "\n"; localhealth = 0;
	}

	if ( dbsc->read_periodictable() == true ) {
		cout << "Rank " << r << " reads PSE successfully rng.nuclides.isotopes.size() " << dbsc->rng.nuclides.isotopes.size() << "\n";
	}
	else {
		cerr << "Rank " << r << " loading of PeriodicTableOfElements failed!" << "\n"; localhealth = 0;
	}

	string xmlfn = pargv[CONTROLFILE];
	if ( dbsc->read_iontype_combinations( xmlfn ) == true ) {
		cout << "Rank " << r << " reads IontypeCombinations successfully " << "\n";
	}
	else {
		cerr << "Rank " << r << " loading of IontypeCombinations failed!" << "\n"; localhealth = 0;
	}

	//do we have all processes on board?
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete dbsc; dbsc = NULL; return;
	}
	
	double ttoc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	dbsc->dbscan_tictoc.prof_elpsdtime_and_mem( "ReadXMLParameter", APT_XX, APT_IS_PAR, mm, ttic, ttoc );
	ttic = MPI_Wtime();

	//we have all on board, now read xyz, ranging, possibly precomputed distance information for all ions
	//MK::we could have all processes loading from the same file, or have only MASTER do so and broadcast, we opt for the latter strategy
	//##MK::should not be a problem because initialization is orders of magnitude cheaper than actual calculations...
	if ( dbsc->get_myrank() == MASTER ) {
		if ( dbsc->read_reconxyz_from_apth5() == true ) {
			cout << "MASTER read successfully all ion coordinates" << "\n";
		}
		else {
			cerr << "MASTER was unable to read ion coordinates !" << "\n"; localhealth = 0;
		}
		if ( dbsc->read_ranging_from_apth5() == true ) {
			cout << "MASTER read successfully ranging information" << "\n";
		}
		else {
			cerr << "MASTER was unable to read ranging information !" << "\n"; localhealth = 0;
		}
		if ( dbsc->read_itypes_from_apth5() == true ) {
			cout << "MASTER read successfully all iontypes" << "\n";
		}
		else {
			cerr << "MASTER was unable to read iontypes !" << "\n"; localhealth = 0;
		}
		if ( dbsc->read_dist2hull_from_apth5() == true ) {
			cout << "MASTER read successfully all ion distance to triangularized dataset boundary!" << "\n";
		}
		else {
			cerr << "MASTER was unable to read ion distance values!" << "\n"; localhealth = 0;
		}
		if ( dbsc->xyz.size() != dbsc->dist2hull.size() ) {
			cerr << "MASTER found that not for every position there is at all or only one distance value!" << "\n"; localhealth = 0;
		}
	}
	//else {} //slaves processes wait
	ttoc = MPI_Wtime();
	mm = dbsc->dbscan_tictoc.get_memoryconsumption();
	dbsc->dbscan_tictoc.prof_elpsdtime_and_mem( "ReadMaterialsDataAPTH5", APT_XX, APT_IS_SEQ, mm, ttic, ttoc );
	ttic = MPI_Wtime();
	//necessary? second order issue wrt to performance for as few as 80 processes like on TALOS...?
	MPI_Barrier( MPI_COMM_WORLD );

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that all data have arrived!" << "\n";
		delete dbsc; dbsc = NULL; return;
	}

	if ( dbsc->broadcast_reconxyz() == true ) {
		cout << "Rank " << r << " has synchronized reconstruction" << "\n";
	}
	else {
		cerr << "Rank " << r << " failed to synchronize reconstruction" << "\n"; localhealth = 0;
	}
	if ( dbsc->broadcast_ranging() == true ) {
		cout << "Rank " << r << " has synchronized ranging" << "\n";
	}
	else {
		cerr << "Rank " << r << " failed to synchronize ranging" << "\n"; localhealth = 0;
	}
	if ( dbsc->broadcast_distances() == true ) { //GB/s bidirectional bandwidth one 4B float per ion, so at most 8 GB ~ a few seconds...
	 	cout << "Rank " << r << " has synchronized distances" << "\n";
	}
	else {
	 	cerr << "Rank " << r << " failed to synchronized distances" << "\n"; localhealth = 0;
	}
	ttoc = MPI_Wtime();
	mm = dbsc->dbscan_tictoc.get_memoryconsumption();
	dbsc->dbscan_tictoc.prof_elpsdtime_and_mem( "BroadcastMaterialsData", APT_XX, APT_IS_PAR, mm, ttic, ttoc );

	MPI_Barrier( MPI_COMM_WORLD );

	if ( dbsc->define_analysis_tasks() == true ) {
		cout << "Rank " << r << " has defined the analysis tasks successfully" << "\n";
	}
	else {
		cout << "Rank " << r << " detected inconsistencies during defining analysis tasks!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes have synchronized the ion coordinates, ranging and possible distances!" << "\n";
		delete dbsc; dbsc = NULL; return;
	}

	//we might need to dump intermediate results
	if ( dbsc->get_myrank() == MASTER ) {
		if ( dbsc->init_target_file() == true ) {
			cout << "Rank MASTER successfully initialized APTH5 results file" << "\n";
		}
		else {
			cerr << "Rank MASTER unable to initialize results APTH5 file" << "\n"; localhealth = 0;
		}
		if ( dbsc->write_environment_and_settings() == true ) {
			cout << "Rank " << MASTER << " environment and settings written!" << "\n";
		}
		else {
			cerr << "Rank " << MASTER << " writing environment and settings failed!" << "\n"; localhealth = 0;
		}
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes have synchronized after generation of the target file!" << "\n";
		delete dbsc; dbsc = NULL; return;
	}

	MPI_Barrier( MPI_COMM_WORLD ); //assure that file is ready and we can write to it

	//now processes can proceed and process massively parallel and independently the analysis, individually using OpenMP multithreading

	dbsc->execute_local_workpackage();

cout << "Rank " << r << " has completed its workpackage" << "\n";
	MPI_Barrier(MPI_COMM_WORLD);

/*
	if ( dbsc->collect_results_on_masterprocess() == true ) {
		if ( dbsc->get_myrank() == MASTER ) {
			cout << "Rank MASTER successfully collected description information on the material points" << "\n";
		}
	}
	else {
		cerr << "Rank " << r << " unable to provide descriptive information on material points to the MASTER!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes participated in collection of descriptive information on material points!" << "\n";
		delete dbsc; dbsc = NULL; return;
	}

	if ( dbsc->get_myrank() == MASTER ) {
		if ( dbsc->write_materialpoints_to_apth5() == true ) { //does not require MPI communication all results already on the master
			cout << "Rank MASTER successfully wrote all materialpoints to an APTH5 file" << "\n";
		}
		else {
			cerr << "Rank MASTER unable to write all materialpoints to the APTH5 file" << "\n"; localhealth = 0;
		}
	}
	ttic = MPI_Wtime();
*/

	//only the processes know how many cluster were found for the tasks
	//however, in order to create a single XDMF file which contains metadata for visualizing all analysis tasks
	//we need the number of clusters, we could parse them out from the HDF5 file after writing or, like here, communicate to the master
	if ( ConfigDBScan::IOStoreClusterIDs == true ) {
		if ( dbsc->communicate_analysis_tasks_results() == true ) {
			if ( dbsc->get_myrank() == MASTER ) {
				cout << "Rank MASTER successfully collected analysis tasks results" << "\n";
			}
		}
		else {
			cerr << "Rank " << r << " detected errors while participating in communicating analysis tasks results!" << "\n"; localhealth = 0;
		}

		if ( dbsc->get_myrank() == MASTER ) {
			if ( dbsc->write_xdmf_metadata() == true ) {
				cout << "Rank MASTER wrote the XDMF metadata successfully" << "\n";
			}
			else {
				cerr << "Rank MASTER detected errors while trying to write the XDMF metadata !" << "\n"; localhealth = 0;
			}
		}
		globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != nr ) {
			cerr << "Rank " << r << " has recognized that not all processes participated in collection of communicating analysis tasks results!" << "\n";
			delete dbsc; dbsc = NULL; return;
		}
	}

	for( int rk = MASTER; rk < nr; rk++ ) {
		if ( dbsc->get_myrank() == rk ) {
			if ( dbsc->write_local_results() == true ) {
				cout << "Rank " << rk << " successfully wrote all its results to H5 file" << "\n";
			}
			else {
				cerr << "Rank " << rk << " unable to write all materialpoints to the APTH5 file" << "\n"; localhealth = 0;
			}
		}
		//hyperphobic
		MPI_Barrier( MPI_COMM_WORLD );
	}

/*
	ttoc = MPI_Wtime();
	mm = dbsc->dbscan_tictoc.get_memoryconsumption();
	dbsc->dbscan_tictoc.prof_elpsdtime_and_mem( "WritingH5ResultsFile", APT_XX, APT_IS_SEQ, mm, ttic, ttoc);
*/
	dbsc->dbscan_tictoc.spit_profiling( "DBScan", ConfigShared::SimID, dbsc->get_myrank() );

	//release resources
	delete dbsc; dbsc = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();

	hello();
//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	//helloworld( argc, argv );

	if ( init( argc, argv ) == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks!" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else { 
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}
	cout << r << "-th MPI process initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";
	
//EXECUTE SPECIFIC TASK
	run_highthroughput_dbscan_variants_on_reconstruction( r, nr, argv );
	
//DESTROY MPI
	//##MK::output profiling results

	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	cout << "Rank " << r << " left MPI_COMM_WORLD deconstructed" << "\n";

	double toc = omp_get_wtime();
	cout << "paraprobe-dbscan took " << (toc-tic) << " seconds wall-clock time in total" << endl;
	return 0;
}


/*
	unsigned int a = 0;
	unsigned int b = 1;
	unsigned int c = 2;
	unsigned int d = 3;
	unsigned int nrr = 2;
	unsigned int amod = a % nrr;
	unsigned int bmod = b % nrr;
	unsigned int cmod = c % nrr;
	unsigned int dmod = d % nrr;
	cout << amod << "\n";
	cout << bmod << "\n";
	cout << cmod << "\n";
	cout << dmod << "\n";
	return 0;
*/

