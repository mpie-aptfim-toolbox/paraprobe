//##MK::GPLV3

#ifndef __PARAPROBE_DBSCAN_XDMF_H__
#define __PARAPROBE_DBSCAN_XDMF_H__

#include "PARAPROBE_DBScanHDF5.h"

struct DBScanXDMF
{
	unsigned int nelements;
	unsigned int ntopo;
	unsigned int nxyz;
	string tasknm;
	string topo_dsnm;
	string xyz_dsnm;
	DBScanXDMF() : nelements(0), ntopo(0), nxyz(0), tasknm(""), topo_dsnm(""), xyz_dsnm("") {}
	DBScanXDMF( const unsigned int _nelem, const unsigned int _ntopo, const unsigned int _nxyz,
			const string _tsknm, const string _topodsnm, const string _xyzdsnm ) :
		nelements(_nelem), ntopo(_ntopo), nxyz(_nxyz), tasknm(_tsknm), topo_dsnm(_topodsnm), xyz_dsnm(_xyzdsnm) {}
};


class dbscan_xdmf : public xdmfHdl
{
	//tool-specific sub-class of a XDMF inheriting all methods of the base class xdmfHdl
	//coordinating instance handling all (sequential) writing to XDMF text file to supplement visualization of HDF5 content

public:
	dbscan_xdmf();
	~dbscan_xdmf();

	int create_dbscan_file( const string xmlfn, vector<DBScanXDMF> const & in, const string h5ref );

//private:
};

#endif
