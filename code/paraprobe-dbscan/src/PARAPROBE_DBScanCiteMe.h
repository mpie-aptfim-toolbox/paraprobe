//##MK::GPLV3


#ifndef __PARAPROBE_DBSCAN_CITEME_H__
#define __PARAPROBE_DBSCAN_CITEME_H__

#include "CONFIG_DBScan.h"


class CiteMeDBScan
{
public:
	
	static vector<bibtex> Citations;
	
	static size_t cite();
};

#endif

