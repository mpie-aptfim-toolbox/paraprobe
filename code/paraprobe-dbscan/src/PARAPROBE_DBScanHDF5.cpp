//##MK::GPLV3

#include "PARAPROBE_DBScanHDF5.h"
//https://www.learncpp.com/cpp-tutorial/114-constructors-and-initialization-of-derived-classes/


dbscan_h5::dbscan_h5()
{
}


dbscan_h5::~dbscan_h5()
{
}


int dbscan_h5::create_dbscan_apth5( const string h5fn )
{
	h5resultsfn = h5fn;
	fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );
	if ( fileid < 0 ) {
		cerr << "Create dbscan apth5 file creation failed! " << fileid << "\n"; return WRAPPED_HDF5_FAILED;
	}

	//domain specific HDF5 keywords and data fields
	//if ( ConfigDBScan::ClusteringMethod == NORMAL ) {
		groupid = H5Gcreate2(fileid, PARAPROBE_DBSC, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_DBSC failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_DBSC failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}

		groupid = H5Gcreate2(fileid, PARAPROBE_DBSC_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_DBSC_META failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_DBSC_META failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}

		groupid = H5Gcreate2(fileid, PARAPROBE_DBSC_META_HRDWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_DBSC_META_HRDWR failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_DBSC_META_HRDWR failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}

		groupid = H5Gcreate2(fileid, PARAPROBE_DBSC_META_SFTWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_DBSC_META_SFTWR failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_DBSC_META_SFTWR failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}

		groupid = H5Gcreate2(fileid, PARAPROBE_DBSC_RES, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_DBSC_RES failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_DBSC_RES failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}
	//}
	/*
	else if ( ConfigDBScan::ClusteringMethod == MAXSEP ) {
		groupid = H5Gcreate2(fileid, PARAPROBE_MSMA, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_MSMA failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_MSMA failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}

		groupid = H5Gcreate2(fileid, PARAPROBE_MSMA_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_MSMA_META failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_MSMA_META failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}
		groupid = H5Gcreate2(fileid, PARAPROBE_MSMA_RES, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_MSMA_RES failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_MSMA_RES failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}
	}
	*/
	
	//add environment and tool specific settings
/*
	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_HRDWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_HRDWR failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_HRDWR failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_HRDWR_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_HRDWR_META failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_HRDWR_META failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_HRDWR_META_KEYS, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_HRDWR_META_KEYS failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_HRDWR_META_KEYS failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_SFTWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_SFTWR failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARARPROBE_UTILS_SFTWR failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_SFTWR_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_SFTWR_META failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_SFTWR_META failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_SFTWR_META_KEYS, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_SFTWR_META_KEYS failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_SFTWR_META_KEYS failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}
*/

	//close file
	status = H5Fclose(fileid);
	if ( status < 0 ) {
		cerr << "Close file " << h5resultsfn << " failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	return WRAPPED_HDF5_SUCCESS;
}
