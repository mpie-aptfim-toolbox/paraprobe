//##MK::GPLV3

#include "PARAPROBE_DBScanTaskHdl.h"

ostream& operator << (ostream& in, TypeCombi const & val) {
	in << "__" << val.targets << "\n"; //"__\t\t__" << val.nbors << "__" << "\n";
	return in;
}



itypeCombiHdl::itypeCombiHdl()
{
}


itypeCombiHdl::~itypeCombiHdl()
{
}


bool itypeCombiHdl::load_iontype_combinations( string xmlfn )
{
	//cout << "Importing itypeCombinations..." << "\n";
	ifstream file( xmlfn );
	if ( file.fail() ) {
		cerr << "Unable to locate input file " << xmlfn << "\n"; return false;
	}
	else {
		cout << "Opening input file " << xmlfn << "\n";
	}

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigDBScan")) {
		cerr << "Undefined parameters file!" << "\n"; return false;
	}

	xml_node<>* comb_node = rootNode->first_node("IontypeCombinations");
	int i = 0;
	for (xml_node<> * entry_node = comb_node->first_node("entry"); entry_node; entry_node = entry_node->next_sibling() ) {
		string trg = entry_node->first_attribute("targets")->value();
		//string nbr = entry_node->first_attribute("neighbors")->value();

		if ( trg != "" ) { // && nbr != "" ) {
			this->icombis.push_back( TypeCombi( trg ) ); //, nbr) );
cout << this->icombis.back() << "\n";

			string ky = "IontypeCombi" + to_string(i); i++;
			string vl = "Targets;" + trg; //+ ";Neighbors;" + nbr;
			this->iifo.push_back( pparm( ky, vl, "", "" ) );
		}
		else {
			cout << "WARNING attempt to pass an inacceptable IontypeCombination entry both targets and neighbors are empty!" << "\n"; return false;
		}
	}

	return true;
}
