//##MK::GPLV3


#ifndef __PARAPROBE_DBSCAN_TASKHANDLING_H__
#define __PARAPROBE_DBSCAN_TASKHANDLING_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_DBScanStructs.h"

//here, inject third-party HPDBScan implementation
//#include "../../paraprobe-dbscan/src/thirdparty/HPDBSCAN/PARAPROBE_HPDBScan.h"
#include "thirdparty/HPDBScan/PARAPROBE_HPDBScan.h"

struct TypeCombi
{
	string targets;
	//string nbors;
	TypeCombi() : targets("") {} //, nbors("") {}
	TypeCombi( const string _trg ) : targets(_trg) {} // const string _nbr ) : targets(_trg), nbors(_nbr) {}
};

ostream& operator << (ostream& in, TypeCombi const & val);


struct Evapion3Combi
{
	vector<evapion3> targets;
	//vector<evapion3> nbors;
	Evapion3Combi() : targets(vector<evapion3>()) {} //, nbors(vector<evapion3>()) {}
};


struct UC8Combi
{
	vector<unsigned char> targets;
	//vector<unsigned char> nbors;
	UC8Combi() : targets(vector<unsigned char>()) {} //, nbors(vector<unsigned char>()) {}
};

#define ORIGINAL_IONTYPES		true

/*
#define RANDOMIZED_IONTYPES		false
#define WANT_RDF				true
#define WANT_KNN				true
#define	WANT_SDM				true
#define WANT_NONE				false
*/

inline bool SortXDMFIfoForTaskIDAsc(const MPI_DBScan_Task & a, const MPI_DBScan_Task & b)
{
	return a.taskid < b.taskid;
}


struct DBScanTaskIfo
{
	double tprep;
	double tdbsc;
	double tpost;
	size_t nions;
	//unsigned int pad;
	DBScanTaskIfo() : tprep(0.0), tdbsc(0.0), tpost(0.0), nions(0) {} //, pad(0) {}
};


struct DBScanTask
{
	//helper structure to implement information channeling and wiring across process/thread/task-loop hierarchy
	//8+4+4+one page
	vector<HPDBPointRes> lbl_res;
	vector<particle> prec_res;
	//size_t pos_firstnbor;
	apt_real eps;
	unsigned int kth; //kth nearest neighbor to be in eps environment for DBScan
	unsigned int minpts; //to consider cluster a significant one
	unsigned int taskid;
	unsigned int rank;
	DBScanTaskIfo ifo;
	//unsigned int method;

	//##MK::maybe determine a maximum admissible size to pack this object even stronger in memory to improve cacheline utilization
	vector<unsigned char> targ_nbors;		//first all target iontypes on [0, pos_firstnbor), next all neighbors on [pos_firstnbor,targ_nbors.size())

	DBScanTask() : lbl_res(vector<HPDBPointRes>()), prec_res(vector<particle>()), eps(0.0), kth(1), minpts(0), taskid(UINT32MX), rank(UINT32MX), ifo(DBScanTaskIfo()) {} //pos_firstnbor(0),
	DBScanTask( const apt_real _eps, const unsigned int _kth, const unsigned int _minpts,
			const unsigned int _tskid, const unsigned int _rk ) : lbl_res(vector<HPDBPointRes>()), prec_res(vector<particle>()),
			eps(_eps), kth(_kth), minpts(_minpts), taskid(_tskid), rank(_rk), ifo(DBScanTaskIfo()) {} //pos_firstnbor(0),
};


class itypeCombiHdl
{
	//class which translates human-readable single/molecular ion type combination strings into the internal itype unsigned char
	//format with which internally all ions are analyzed
public:
	itypeCombiHdl();
	~itypeCombiHdl();

	bool load_iontype_combinations( string xmlfn );
	//bool prepare_iontype_combinations( rangeTable & ranger, const size_t max_memory_per_node );

	vector<TypeCombi> icombis;
	vector<UC8Combi> combinations;
	vector<DBScanTask> itasks;

	vector<pparm> iifo;
};



#endif

