//##MK::GPLV3

#include "PARAPROBE_DBScanHdl.h"


dbscanHdl::dbscanHdl()
{
	myrank = MASTER;
	nranks = 1;
	maximum_iontype_uc = 0;
}


dbscanHdl::~dbscanHdl()
{
    xyz = vector<p3d>();
    ityp_org = vector<unsigned char>();
    dist2hull = vector<apt_real>();
}


bool dbscanHdl::read_periodictable()
{
	//all read PeriodicTable
	/*
	if ( rng.nuclides.load_isotope_library( ConfigDBScan::InputfilePSE ) == false ) {
		cerr << "Rank " << get_myrank() << " loading of PeriodicTableOfElements failed!" << "\n"; return false;
	}
	*/
	if ( rng.nuclides.load_isotope_library() == false ) {
		cerr << "Rank " << get_myrank() << " loading of Config_SHARED PeriodicTableOfElements failed!" << "\n"; return false;
	}
	cout << "Rank " << get_myrank() << " loaded rng.nuclides.isotopes.size() " << rng.nuclides.isotopes.size() << " isotopes" << "\n";
	return true;
}


bool dbscanHdl::read_iontype_combinations( string fn )
{
	//all read IontypeCombinations
	if ( itsk.load_iontype_combinations( fn ) == false ) {
		cerr << "Rank " << get_myrank() << " loading of IontypeCombinations failed!" << "\n"; return false;
	}
	cout << "Rank " << get_myrank() << " itsk.icombis.size() " << itsk.icombis.size() << " icombis " << "\n";
	return true;
}


bool dbscanHdl::read_reconxyz_from_apth5()
{
	double tic = MPI_Wtime();

	//load xyz positions
	if ( inputReconH5Hdl.read_xyz_from_apth5( ConfigDBScan::InputfileReconstruction, xyz ) == true ) {
		cout << ConfigDBScan::InputfileReconstruction << " read positions successfully" << "\n";
	}
	else {
		cerr << ConfigDBScan::InputfileReconstruction << " read positions failed!" << "\n"; return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	dbscan_tictoc.prof_elpsdtime_and_mem( "ReadReconstructedXYZFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

    return true;
}


bool dbscanHdl::read_ranging_from_apth5()
{
	double tic = MPI_Wtime();

	//load ranging information
	if ( inputReconH5Hdl.read_iontypes_from_apth5( ConfigDBScan::InputfileReconstruction, rng.iontypes ) == true ) {
		cout << ConfigDBScan::InputfileReconstruction << " read ranging information successfully" << "\n";

		//create iontype_dictionary
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
			size_t id = static_cast<size_t>(it->id);
			size_t hashval = rng.hash_molecular_ion( it->strct.Z1, it->strct.Z2, it->strct.Z3 );
	cout << id << "\t\t" << hashval << "\t\t" << (int) it->strct.Z1 << ";" << (int) it->strct.Z2 << ";" << (int) it->strct.Z3 << "\n";
			if ( id != 0 && hashval != 0 ) { //skip adding existent UNKNOWN_IONTYPE
				auto jt = rng.iontypes_dict.find( hashval );
				if ( jt == rng.iontypes_dict.end() ) { //really a new iontype for which not yet mqival were defined
	cout << "--->Adding new type " << id << "\t\t" << hashval << "\n";
					//add iontype in dictionary
					rng.iontypes_dict.insert( pair<size_t,size_t>( hashval, id ) );
				}
				else {
	cerr << "--->Detected an iontype where the triplet Z1, Z2, Z3 is a duplicate, which should not happen as iontypes have to be mutually exclusive!" << "\n";
					return false;
	//cout << "--->Adding to an existent type " << id << "\t\t" << hashval << "\t\t" << jt->second << "\n";
					//match to an already existent type
					//rng.iontypes_dict.insert( pair<size_t,size_t>( hashval, id ) ); //jt->second ) );
					//##MK::this fix makes it de facto a charge state specific 8;0,0;0;1 and 8;0;0;0;0;0;0;1 and 8;0;0;0;0;0;0;2 map to different types
					//return false; //##MK::switched of for FAU APT School
				}
			}
			else {
	cout << "--->Me skipping the UNKNOWN_IONTYPE" << "\n";
			}
		}

		//##MK::BEGIN DEBUG, make sure we use have correct ranging data
		cout << "--->Debugging ranging data..." << "\n";
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
			size_t id = static_cast<size_t>(it->id);
			size_t hashval = rng.hash_molecular_ion( it->strct.Z1, it->strct.Z2, it->strct.Z3 );
			cout << id << "\t\t" << hashval << "\t\t" << (int) it->strct.Z1 << ";" << (int) it->strct.Z2 << ";" << (int) it->strct.Z3 << "\n";
			for( auto jt = it->rng.begin(); jt != it->rng.end(); jt++ ) {
				cout << "\t\t\t" << jt->lo << "\t\t" << jt->hi << "\n";
			}
		}
		cout << "--->Debugging ion dictionary..."  << "\n";
		for( auto kt = rng.iontypes_dict.begin(); kt != rng.iontypes_dict.end(); kt++ ) {
			cout << kt->first << "\t\t" << kt->second << "\n";
		}
		//##MK::END DEBUG
	}
	else {
		cerr << ConfigDBScan::InputfileReconstruction << " read ranging information failed!" << "\n"; return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	dbscan_tictoc.prof_elpsdtime_and_mem( "ReadRangingFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

    return true;
}


bool dbscanHdl::read_itypes_from_apth5()
{
	double tic = MPI_Wtime();

	//load itype per xyz position
	if ( inputReconH5Hdl.read_ityp_from_apth5( ConfigDBScan::InputfileReconstruction, ityp_org ) == true ) {
		cout << ConfigDBScan::InputfileReconstruction << " read itypes successfully" << "\n";
	}
	else {
		cerr << ConfigDBScan::InputfileReconstruction << " read itypes failed!" << "\n"; return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	dbscan_tictoc.prof_elpsdtime_and_mem( "ReadIontypesXYZFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

    return true;
}


bool dbscanHdl::read_dist2hull_from_apth5()
{
	//double tic = MPI_Wtime();

	if ( inputDistH5Hdl.read_dist_from_apth5( ConfigDBScan::InputfileHullAndDistances, dist2hull ) == true ) {
		cout << ConfigDBScan::InputfileReconstruction << " read distances successfully" << "\n";
	}
	else {
		cerr << ConfigDBScan::InputfileReconstruction << " read distances failed!" << "\n"; return false;
	}

	//double toc = MPI_Wtime();
	//memsnapshot mm = memsnapshot();
	//spatstat_tictoc.prof_elpsdtime_and_mem( "ReadDistancesToTriangleHullFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

    return true;
}


bool dbscanHdl::broadcast_reconxyz()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
		return true;
	}

	//double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( xyz.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " ion reconstructions!" << "\n"; localhealth = 0;
		}
		if ( xyz.size() == 0 ) {
			cerr << "Ion positions is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the MASTER's xyz size and state!" << "\n"; return false;
	}

	//communicate size of the master's dataset
	int nevt = 0;
	if ( get_myrank() == MASTER ) {
		nevt = static_cast<int>(xyz.size());
	}
	MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	MPI_Synth_XYZ* buf = NULL;
	try {
		buf = new MPI_Synth_XYZ[nevt];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for MPI_Synth_XYZ buffer!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL;
		return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < xyz.size(); i++ ) {
			buf[i] = MPI_Synth_XYZ( xyz.at(i) );
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, nevt, MPI_Synth_XYZ_Type, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			xyz.reserve ( static_cast<size_t>(nevt) );
			for( int i = 0; i < nevt; i++ ) {
				xyz.push_back( p3d( buf[i].x, buf[i].y, buf[i].z ) );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout to instantiate xyz!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";	return false;
	}

//cout << "Rank " << get_myrank() << " listened to MASTER reconstruction.size() " << reconstruction.size() << "\n";

	//double toc = MPI_Wtime();
	//memsnapshot mm = memsnapshot();
	//spatstat_tictoc.prof_elpsdtime_and_mem( "BroadcastReconXYZ", APT_XX, APT_IS_PAR, mm, tic, toc);
	return true;
}


bool dbscanHdl::broadcast_ranging()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
		return true;
	}

	//double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( rng.iontypes.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " iontypesinfo!" << "\n"; localhealth = 0;
		}
		if ( rng.iontypes.size() == 0 ) {
			cerr << "MASTER's rng.iontypes is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
		if ( ityp_org.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " ityp_org per ion!" << "\n"; localhealth = 0;
		}
		if ( ityp_org.size() == 0 ) {
			cerr << "MASTER's ityp_org is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the MASTER's ranging size and state!" << "\n"; return false;
	}

	//communicate metadata first
	int nrng_ivl_evt_dict[4] = {0, 0, 0, 0};
	if ( get_myrank() == MASTER ) {
		nrng_ivl_evt_dict[0] = static_cast<int>(rng.iontypes.size());
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
			nrng_ivl_evt_dict[1] += static_cast<int>(it->rng.size());
		}
		nrng_ivl_evt_dict[2] = static_cast<int>(ityp_org.size());
		nrng_ivl_evt_dict[3] = static_cast<int>(rng.iontypes_dict.size());
	}
	MPI_Bcast( nrng_ivl_evt_dict, 4, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	MPI_Ranger_Iontype* buf1 = NULL;
	MPI_Ranger_MQIval* buf2 = NULL;
	unsigned char* buf3 = NULL;
	MPI_Ranger_DictKeyVal* buf4 = NULL;
	try {
		buf1 = new MPI_Ranger_Iontype[nrng_ivl_evt_dict[0]];
		buf2 = new MPI_Ranger_MQIval[nrng_ivl_evt_dict[1]];
		buf3 = new unsigned char[nrng_ivl_evt_dict[2]];
		buf4 = new MPI_Ranger_DictKeyVal[nrng_ivl_evt_dict[3]];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for buf1, buf2, buf3, and buf4!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf1; buf1 = NULL;
		delete [] buf2; buf2 = NULL;
		delete [] buf3; buf3 = NULL;
		delete [] buf4; buf4 = NULL;
		return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		size_t iv = 0;
		size_t id = 0;
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++, id++ ) {
			unsigned char typid = static_cast<unsigned char>(it->id);
			buf1[id] = MPI_Ranger_Iontype( typid, it->strct.Z1, it->strct.N1, it->strct.Z2, it->strct.N2,
												it->strct.Z3, it->strct.N3, it->strct.sign, it->strct.charge );
			for( auto jt = it->rng.begin(); jt != it->rng.end(); jt++, iv++ ) {
				buf2[iv] = MPI_Ranger_MQIval( jt->lo, jt->hi, typid );
			}
		}
		for( size_t i = 0; i < ityp_org.size(); i++ ) {
			buf3[i] = ityp_org.at(i);
		}
		size_t ii = 0;
		for( auto kvt = rng.iontypes_dict.begin(); kvt != rng.iontypes_dict.end(); kvt++, ii++ ) {
			buf4[ii] = MPI_Ranger_DictKeyVal( static_cast<unsigned long long>(kvt->first), static_cast<unsigned long long>(kvt->second) );
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf1, nrng_ivl_evt_dict[0], MPI_Ranger_Iontype_Type, MASTER, MPI_COMM_WORLD );
	MPI_Bcast( buf2, nrng_ivl_evt_dict[1], MPI_Ranger_MQIval_Type, MASTER, MPI_COMM_WORLD);
	MPI_Bcast( buf3, nrng_ivl_evt_dict[2], MPI_UNSIGNED_CHAR, MASTER, MPI_COMM_WORLD );
	MPI_Bcast( buf4, nrng_ivl_evt_dict[3], MPI_Ranger_DictKeyVal_Type, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			rng.iontypes.reserve( static_cast<size_t>(nrng_ivl_evt_dict[0]) );
			for( int i = 0; i < nrng_ivl_evt_dict[0]; i++ ) {
				rng.iontypes.push_back( rangedIontype() );
				rng.iontypes.back().id = static_cast<unsigned int>( buf1[i].id );
				rng.iontypes.back().strct = evapion3( buf1[i].Z1, buf1[i].N1, buf1[i].Z2, buf1[i].N2, buf1[i].Z3, buf1[i].N3, buf1[i].sign, buf1[i].charge );
				for( int j = 0; j < nrng_ivl_evt_dict[1]; j++ ) { //was [2], ##MK:avoid O(N^2) search, but should not be a problem because N << 256
					if ( static_cast<unsigned int>(buf2[j].id) != rng.iontypes.back().id ) {
						continue;
					}
					else {
						rng.iontypes.back().rng.push_back( mqival( buf2[j].low, buf2[j].high ) );
					}
				}
			}
			ityp_org.reserve( static_cast<size_t>(nrng_ivl_evt_dict[2]) );
			for( int k = 0; k < nrng_ivl_evt_dict[2]; k++ ) {
				ityp_org.push_back( buf3[k] );
			}

			for ( int kv = 0; kv < nrng_ivl_evt_dict[3]; kv++ ) {
				rng.iontypes_dict.insert( pair<size_t,size_t>(
						static_cast<size_t>(buf4[kv].key), static_cast<size_t>(buf4[kv].val) ) );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout to instantiate rng.iontypes and ityp_org!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf1; buf1 = NULL;
	delete [] buf2; buf2 = NULL;
	delete [] buf3; buf3 = NULL;
	delete [] buf4; buf4 = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";	return false;
	}

//cout << "Rank " << get_myrank() << " listened to MASTER reconstruction.size() " << reconstruction.size() << "\n";

	//double toc = MPI_Wtime();
	//memsnapshot mm = memsnapshot();
	//spatstat_tictoc.prof_elpsdtime_and_mem( "BroadcastRangingData", APT_XX, APT_IS_PAR, mm, tic, toc);
	return true;
}


bool dbscanHdl::broadcast_distances()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
		return true;
	}

	//double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( dist2hull.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " ion dist2hull info!" << "\n"; localhealth = 0;
		}
		if ( dist2hull.size() == 0 ) {
			cerr << "MASTER's dist2hull is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the MASTER's dist2hull size and state!" << "\n"; return false;
	}

	//communicate size of the master's dataset
	int nevt = 0;
	if ( get_myrank() == MASTER ) {
		nevt = static_cast<int>(dist2hull.size());
	}
	MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	float* buf = NULL;
	try {
		buf = new float[nevt];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for float buffer!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL;
		return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < dist2hull.size(); i++ ) {
			buf[i] = dist2hull.at(i);
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, nevt, MPI_FLOAT, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			dist2hull.reserve ( static_cast<size_t>(nevt) );
			for( int i = 0; i < nevt; i++ ) {
				dist2hull.push_back( buf[i] );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout to instantiate dist2hull!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";	return false;
	}

//cout << "Rank " << get_myrank() << " listened to MASTER reconstruction.size() " << reconstruction.size() << "\n";

	//double toc = MPI_Wtime();
	//memsnapshot mm = memsnapshot();
	//spatstat_tictoc.prof_elpsdtime_and_mem( "BroadcastDist2Hull", APT_XX, APT_IS_PAR, mm, tic, toc);
	return true;
}


bool dbscanHdl::define_analysis_tasks()
{
	//all create the internal spatial statistics and two-point statistics (SDM) task list,
	//i.e. which iontype combinations are probed
	double tic = MPI_Wtime();

	unsigned int maximum_ityp = 0;
	for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
		if ( it->id < static_cast<unsigned char>(UCHARMX) ) {
			if ( it->id > maximum_ityp ) {
				maximum_ityp = it->id;
			}
		}
		else {
			cerr << "Detected an invalid iontype during itype_sensitive_spatial_decomposition!" << "\n"; return false;
		}
	}

	maximum_iontype_uc = static_cast<unsigned char>(maximum_ityp);

	size_t max_memory_per_node = dbscan_tictoc.get_memory_max_on_node();
	size_t max_memory_for_buf = 0.1 * max_memory_per_node;

	cout << "Rank " << get_myrank() << " max_memory_per_node " << max_memory_per_node << " B" << "\n";
	cout << "Rank " << get_myrank() << " max_memory_for_buf " << max_memory_for_buf << " B" << "\n";

	//##MK::so far we buffer results for each analysis

	//firstly, we transform the human-readable icombis targ/nbor strings pairs into evapion3 objects
	vector<Evapion3Combi> tmp;
	for( auto it = itsk.icombis.begin(); it != itsk.icombis.end(); it++ ) {
		tmp.push_back( Evapion3Combi() );

		rng.add_evapion3( it->targets, tmp.back().targets );
		//rng.add_evapion3( it->nbors, tmp.back().nbors );

		//##BEGIN DEBUG
		cout << "Rank " << get_myrank() << " targets __" << it->targets << "__ ---->" << "\n";
		for( auto jt = tmp.back().targets.begin(); jt != tmp.back().targets.end(); jt++ ) {
			cout << (int) jt->Z1 << ";" << (int) jt->Z2 << ";" << (int) jt->Z3  << "\n";
		}
		/*
		cout << "Rank " << get_myrank() << " nbors __" << it->nbors << "__ ---->" << "\n";
		for( auto jt = tmp.back().nbors.begin(); jt != tmp.back().nbors.end(); jt++ ) {
			cout << (int) jt->Z1 << ";" << (int) jt->Z2 << ";" << (int) jt->Z3  << "\n";
		}
		*/
		//##END DEBUG
	}

	//secondly, we have to understand that these user-combinations may not at all be present in the ranging data, i.e.
	//are possibly not all evapion3 types that we identifiable in the analyzed dataset as ranging data,
	//sure, in most cases user will make correct input but in every case we need to make sure
	map<size_t,size_t>::iterator does_not_exist = rng.iontypes_dict.end();
	for( auto jt = tmp.begin(); jt != tmp.end(); jt++ ) {
		UC8Combi valid;
		for( auto tg = jt->targets.begin(); tg != jt->targets.end(); tg++ ) {
			size_t hashval = rng.hash_molecular_ion( tg->Z1, tg->Z2, tg->Z3 );
			//MK::it is important to understand here that the user is not enforced to write the molecular ions
			//within the XML input file in order, because above preprocessing assures that combinations such as AlH: or H:Al will be sorted
			//into a unique representation namely Al, H, UNOCCUPIED as it holds also for the ranging
			//Consequently, internal ion type handling is handling commutativity to please the user
			map<size_t,size_t>::iterator vt = rng.iontypes_dict.find( hashval );
			if ( vt != does_not_exist ) { //kt is an ion type which exists within the ranging data
cout << " Adding an identified ion from the ranging process as target\t\t" << vt->first << "\t\t" << vt->second << "\n";
				valid.targets.push_back( vt->second );
			}
		}
		/*
		for( auto nb = jt->nbors.begin(); nb != jt->nbors.end(); nb++ ) {
			size_t hashval = rng.hash_molecular_ion( nb->Z1, nb->Z2, nb->Z3 );
			//MK::it is important to understand here that the user is not enforced to write the molecular ions
			//within the XML input file in order, because above preprocessing assures that combinations such as AlH: or H:Al will be sorted
			//into a unique representation namely Al, H, UNOCCUPIED as it holds also for the ranging
			//Consequently, internal ion type handling is handling commutativity to please the user
			map<size_t,size_t>::iterator vt = rng.iontypes_dict.find( hashval );
			if ( vt != does_not_exist ) { //kt is an ion type which exists within the ranging data
cout << " Adding an identified ion from the ranging process as neighb\t\t" << vt->first << "\t\t" << vt->second << "\n";
				valid.nbors.push_back( vt->second );
			}
		}
		*/

		itsk.combinations.push_back( UC8Combi() );
		for( auto tgs = valid.targets.begin(); tgs != valid.targets.end(); tgs++ ) { //explicit deep copy
			itsk.combinations.back().targets.push_back( *tgs );
		}
		/*
		for( auto nbs = valid.nbors.begin(); nbs != valid.nbors.end(); nbs++ ) {
			itsk.combinations.back().nbors.push_back( *nbs );
		}
		*/

		//1.) check if any evapion3 matches to any known ranged
		//2.) if so create a new UC8Combi object and
		//3.) translate evapion3 to known unsigned char iontype values from the ConfigSpatstat::InputfileReconstruction "/Ranging..."
		//MK::the trick here is to end up that eventually the actual analysis of iontypes will use exclusively
		//unsigned char keys. e.g. lets assume we have Al-Al and Al was ranged from the input as unsigned char == 0x01 then
		//the pairing Al-Al becomes UC8Combi.targets = 0x01 and UC8Combi.nbors = 0x01
		//targets/nbors == 0x00 means always all ions
		//we do not allow for negations of types as this can always be reformulated
		//5.) we store eventually in this->
		//this->combinations.push_back( UC8Combi() );
		//combinations.back().targets.push_back()
		//combinations.back().nbors.push_back();
	}
	//get rid of temporaries
	tmp = vector<Evapion3Combi>();


	//thirdly, and now having the IontypeCombinations we plan the actual tasks
	unsigned int tskid = 0; //scientific analysis tasks start at 0
	unsigned int nranks = static_cast<unsigned int>(get_nranks());
	unsigned int myrank = static_cast<unsigned int>(get_myrank());
	unsigned int thisrank = static_cast<unsigned int>(MASTER);
	size_t max_local_memory_expected = 0; //in bytes

	if ( itsk.combinations.size() < 1 ) {
		cerr << "Combinations were specified but not parsable after having evaluated InputfileReconstruction ranging data and settings!" << "\n";
		return false;
	}

cout << "About to prepare DBScan tasks..." << "\n";
	for( auto it = itsk.combinations.begin(); it != itsk.combinations.end(); it++ ) { //for ORIGINAL ion labels

		//assuming ClusteringMethod == NORMAL
		lival_real eps_ival = ConfigDBScan::Epsilon;
		lival_u32 mpts_ival = ConfigDBScan::MinPtsKth;
		if ( ConfigDBScan::ClusteringMethod == MAXSEP ) {
			eps_ival = ConfigDBScan::Epsilon;
			mpts_ival = ConfigDBScan::MaxSepMinPts;
		}

		for( apt_real epsilon = eps_ival.min; epsilon <= eps_ival.max; epsilon += eps_ival.incr ) { //epsilon serving as dmax in case of ClusteringMethod == MAXSEP
			for( unsigned int mpts = mpts_ival.min; mpts <= mpts_ival.max; mpts += mpts_ival.incr ) { //for MAXSEP high-throughput over number of significant cluster
				//which MPI process will compute this?
				//##MK::all process store that there are these tasks
				thisrank = tskid % nranks; //round-robin distributing
				//if ( tskid % nranks == thisrank ) { //I do it
					//cout << "Rank " << thisrank << " processes task " << tskid << "\n";

				//which DBScan parameters?
				if ( ConfigDBScan::ClusteringMethod == NORMAL ) {
					itsk.itasks.push_back( DBScanTask( epsilon, mpts, 1, tskid, thisrank ) ); //DBScan a cluster neds to have at least one point
				}
				else { //ClusteringMethod == MAXSEP
					itsk.itasks.push_back( DBScanTask( epsilon, 1, mpts, tskid, thisrank ) ); //maximum separation method with kth := 1, high-throughput
				}

				//which ions as targets?
				for( auto tgs = it->targets.begin(); tgs != it->targets.end(); tgs++ ) {
					itsk.itasks.back().targ_nbors.push_back( *tgs );
				}
				/*
				itsk.itasks.back().pos_firstnbor = itsk.itasks.back().targ_nbors.size(); //first all target iontypes on [0, pos_firstnbor), next all neighbors on [pos_firstnbor,targ_nbors.size())
				for( auto nbs = it->nbors.begin(); nbs != it->nbors.end(); nbs++ ) {
					itsk.itasks.back().targ_nbors.push_back( *nbs );
				}
				*/

				if ( thisrank == myrank ) { //if I am responsible for the task
					size_t mem_per_task = 0;
					if ( ConfigDBScan::IOStoreClusterIDs == true ) {
						mem_per_task += static_cast<size_t>(xyz.size())*sizeof(long);
					}
					max_local_memory_expected += mem_per_task;
				}
				//in every case, increment taskid
				tskid++;
			} //next minPts setting
		} //next epsilon value
	} //next ion type combination


	//final check would all these buffers fit in memory?
	if ( max_local_memory_expected >= max_memory_per_node ) {
		cerr << "With " << max_local_memory_expected << " B memory required you need to use more nodes with the current implementation!" << "\n"; return false;
	}
	else {
		cout << "Rank expected memory demands for buffering intermediate results to be " << max_local_memory_expected << " B" << "\n";
	}

	//##MK::BEGIN DEBUG
	for( int rk = MASTER; rk < get_nranks(); rk++ ) {
		if ( rk == get_myrank() ) {
		cout << "Rank " << rk << " prepared all tasks now verifying successful deep copies of values..." << "\n";
			for( auto ic = itsk.icombis.begin(); ic != itsk.icombis.end(); ic++ ) {
				cout << ic->targets << "\n"; //"\t\t" << ic->nbors << "\n";
			}
			for( auto jt = itsk.combinations.begin(); jt != itsk.combinations.end(); jt++ ) {
				cout << "Rank " << rk << " itsk.combiations " << jt - itsk.combinations.begin() << "\n";
				for( auto tg = jt->targets.begin(); tg != jt->targets.end(); tg++ ) {
					cout << "Rank " << rk << " tg = " << (int) *tg << "\n";
				}
				/*
				for( auto nb = jt->nbors.begin(); nb != jt->nbors.end(); nb++ ) {
					cout << "Rank " << rk << " nb = " << (int) *nb << "\n";
				}
				*/
			}
			for( auto tsk = itsk.itasks.begin(); tsk != itsk.itasks.end(); tsk++ ) {
				cout << "Rank " << rk << " isk.itask " << tsk->taskid << " rank " << tsk->rank << "\n";
				cout << "epsilon = " << tsk->eps << "\n";
				cout << "kth = " << tsk->kth << "\n";
				cout << "minPts = " << tsk->minpts << "\n";
				//cout << "pos_firstnbor = " << tsk->pos_firstnbor << "\n";
				for( size_t uc = 0; uc < tsk->targ_nbors.size(); uc++ ) { //tsk->pos_firstnbor
					cout << "Target\t\t" << (int) tsk->targ_nbors.at(uc) << "\n";
				}
				/*
				for( size_t uc = tsk->pos_firstnbor; uc < tsk->targ_nbors.size(); uc++ ) {
					cout << "Neighb\t\t" << (int) tsk->targ_nbors.at(uc) << "\n";
				}
				*/
			}
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
	//##MK::END DEBUG

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	dbscan_tictoc.prof_elpsdtime_and_mem( "DefineAnalysisTasks", APT_XX, APT_IS_PAR, mm, tic, toc );

	return true;
}

/*
void dbscanHdl::randomize_iontype_labels()
{
	double tic = MPI_Wtime();

	//https://meetingcpp.com/blog/items/stdrandom_shuffle-is-deprecated.html
	mt19937 urng;
	urng.seed( ConfigSpatstat::PRNGWorldSeed );
	urng.discard( ConfigSpatstat::PRNGWarmup );

	try {
		ityp_rnd = ityp_org;
	}
	catch(bad_alloc &mecroak) {
		cerr << "Allocation of ityp_rnd failed!" << "\n";
	}

	//randomize order on array via uniform Mersenne twister PRNG shuffling, maintaining total number of ions of specific type
	//##MK::evaluate throws
	shuffle( ityp_rnd.begin(), ityp_rnd.end(), urng);

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	spatstat_tictoc.prof_elpsdtime_and_mem( "RandomizeIontypeLabels", APT_XX, APT_IS_SEQ, mm, tic, toc );

}


void dbscanHdl::itype_sensitive_spatial_decomposition()
{
	double tic = MPI_Wtime();

	sp.tip_aabb_get_extrema( xyz );

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	spatstat_tictoc.prof_elpsdtime_and_mem( "SpecimenAABB3DGetExtrema", APT_XX, APT_IS_SEQ, mm, tic, toc);

	tic = MPI_Wtime();

	//get maximum iontype
	unsigned int maximum_ityp = 0;
	for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
		if ( it->id < static_cast<unsigned char>(UCHARMX) ) {
			if ( it->id > maximum_ityp ) {
				maximum_ityp = it->id;
			}
		}
		else {
			cerr << "Detected an invalid iontype during itype_sensitive_spatial_decomposition!" << "\n"; return;
		}
	}

	maximum_iontype_uc = static_cast<unsigned char>(maximum_ityp);
cout << "Rank " << get_myrank() << " maximum itype identified is " << (int) maximum_iontype_uc << "\n";


	sp.loadpartitioning_subkdtree_per_ityp( xyz, dist2hull, ityp_org, ityp_rnd, maximum_iontype_uc );

	//first point were ityp_org and ityp_rnd are no longer necessary and could in principle be deleted

	toc = MPI_Wtime();
	mm = spatstat_tictoc.get_memoryconsumption();
	spatstat_tictoc.prof_elpsdtime_and_mem( "AABB3DLoadPartitioning", APT_XX, APT_IS_PAR, mm, tic, toc);
}


bool dbscanHdl::prepare_processlocal_resultsbuffer()
{
	//build thread-local results buffer
	double tic = MPI_Wtime();

	for( auto tsk = itsk.itasks.begin(); tsk != itsk.itasks.end(); tsk++ ) {
		results.push_back( SpatstatTask() );
		results.back().taskid = tsk->taskid;
		results.back().WhichLabel = tsk->WhichLabel;
		results.back().IsRDF = tsk->IsRDF;
		results.back().IsKNN = tsk->IsKNN;
		results.back().IsSDM = tsk->IsSDM;
		if ( (tsk->IsRDF == true || tsk->IsKNN == true) && tsk->IsSDM == false ) {
			linear_ival rr = linear_ival( tsk->R.min, tsk->R.incr, tsk->R.max );
			try {
				results.back().hst1d_res = new histogram( rr );
			}
			catch (bad_alloc &croak) {
				cerr << "results.back().hst1d_res allocation error at task " << tsk->taskid << "\n"; return false;
			}
		}
		if ( (tsk->IsRDF == false && tsk->IsKNN == false) && tsk->IsSDM == true ) {
			try {
				results.back().sdm3d_res = new sdm3d( tsk->R.max, tsk->R.incr, 201 );
			}
			catch (bad_alloc &croak) {
				cerr << "results.back().sdm3d_res allocation error at task " << tsk->taskid << "\n"; return false;
			}
		}
		results.back().kthorder = tsk->kthorder;
		results.back().pos_firstnbor = tsk->pos_firstnbor;
		results.back().targ_nbors = tsk->targ_nbors;
		results.back().R = tsk->R;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = spatstat_tictoc.get_memoryconsumption();
	spatstat_tictoc.prof_elpsdtime_and_mem( "PrepareProcessResultsBuffer", APT_XX, APT_IS_SEQ, mm, tic, toc );

	cout << "Rank " << get_myrank() << " has allocated process-local results buffer !" << "\n";
	return true;
}
*/


void dbscanHdl::execute_local_workpackage()
{
	//rank works through its local analysis tasks
	double tic = MPI_Wtime();
	double mytic = 0.0;
	double mytoc = 0.0;
	cout << "Rank " << get_myrank() << " participating in MPI/OMP-parallelized DBScan clustering..." << "\n";
	unsigned int myrank = static_cast<unsigned int>(get_myrank());

	for( auto tsk = itsk.itasks.begin(); tsk != itsk.itasks.end(); tsk++ ) {
		if ( tsk->rank != myrank ) { //skip tasks that I am not taking care of, for many tasks and rank this is the typical case
			continue;
		}

		mytic = MPI_Wtime();
		//apply masks and extract target ions
		vector<HPDBPoint> filtered_ions;
		vector<unsigned char> target_itypes = tsk->targ_nbors;
		unsigned int nxyz = static_cast<unsigned int>(xyz.size());
		for( unsigned int i = 0; i < nxyz; i++ ) {
			unsigned char ityp = ityp_org.at(i);
			for( auto it = target_itypes.begin(); it != target_itypes.end(); ++it ) {
				if ( ityp != *it ) { //assume filtered ions are seldom
					continue;//sk->targ_nbors.begin(); it != tsk->targ_nbors.end())
				}
				else {
					p3d p = xyz.at(i);
					apt_real dist2edge = dist2hull.at(i);
					ssize_t mark = i + 1; //+1 to be able to sign also the 0-th ion ssize_t is 2^64 vs unsigned int is 2^32
					if ( dist2edge >= ConfigDBScan::DatasetEdgeThresholdDistance ) { //most likely an ion inside
						filtered_ions.push_back( HPDBPoint( p.x, p.y, p.z, +mark ) );
					}
					else {
						filtered_ions.push_back( HPDBPoint( p.x, p.y, p.z, -mark ) ); //mark with a negative label
					}
					break; //because there is no need to test other ion types
				}
			}
		}
		tsk->ifo.nions = filtered_ions.size();

		mytoc = MPI_Wtime();
		tsk->ifo.tprep = mytoc - mytic;

		//process DBScan using multithreaded HPDBScan and do clustering analysis
		mytic = MPI_Wtime();

//cout << "\t\tHPDBSCAN parallel scanning with epsilon " << tsk->eps << " and minPoints " << tsk->minpts << "\n";
		HPDBSCAN scanner( filtered_ions );
		scanner.scan( tsk->eps, tsk->kth ); //this implements a DBScan, not the maximum separation method!

		mytoc = MPI_Wtime();
		tsk->ifo.tdbsc = mytoc - mytic;
//cout << "\t\tHPDBScan took " << (mytoc-mytic) << " seconds" << "\n";

		//process summary
		mytic = MPI_Wtime();

		if ( ConfigDBScan::IOStoreClusterIDs == true ) {
			scanner.readout_cluster( tsk->lbl_res );
		}

		if ( ConfigDBScan::IOStoreClusters == true ) {
			scanner.characterize_cluster( tsk->minpts, tsk->prec_res );
			/*
			//##MK::BEGIN DEBUG
			for( auto it = tsk->prec_res.begin(); it != tsk->prec_res.end(); it++ ) {
				cout << "\t\t" << it->id << "\t\t" << it->nions << "\t\t" << it->boundarycontact << "\t\t" << it->barycenter.x << ";" << it->barycenter.y << ";" << it->barycenter.z << "\n";
			}
			//##MK::END DEBUG
			*/
		}

		mytoc = MPI_Wtime();
		tsk->ifo.tpost = mytoc - mytic;

	} //process next task

	double toc = MPI_Wtime();
	memsnapshot mm = dbscan_tictoc.get_memoryconsumption();
	dbscan_tictoc.prof_elpsdtime_and_mem( "ExecuteLocalWorkpackage", APT_XX, APT_IS_PAR, mm, tic, toc );

	cout << "Rank " << get_myrank() << " local DBScan took " << (toc-tic) << " seconds!" << "\n";
}


bool dbscanHdl::init_target_file()
{
	double tic = MPI_Wtime();

	//##MK::generate group hierarchy of an open source IFES APTTC APTH5 HDF5 file
	string h5fn_out = "PARAPROBE.DBScan.Results.SimID." + to_string(ConfigShared::SimID) + ".h5";
cout << "Initializing target file " << h5fn_out << "\n";

	if ( debugh5Hdl.create_dbscan_apth5( h5fn_out ) != WRAPPED_HDF5_SUCCESS ) {
		return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	dbscan_tictoc.prof_elpsdtime_and_mem( "InitTargetFile", APT_XX, APT_IS_SEQ, mm, tic, toc );

	return true;
}


bool dbscanHdl::write_environment_and_settings()
{
	vector<pparm> hardware = dbscan_tictoc.report_machine();
	string prfx = PARAPROBE_DBSC_META_HRDWR; //PARAPROBE_UTILS_HRDWR_META_KEYS;
	for( auto it = hardware.begin(); it != hardware.end(); it++ ) {
		cout << it->keyword << "__" << it->value << "__" << it->unit << "__" << it->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *it ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing hardware setting " << it->keyword << " failed!" << "\n";
			return false;
		}
	}
	cout << "Rank " << MASTER << " hardware settings written to H5" << "\n";

	vector<pparm> software;
	ConfigDBScan::reportSettings( software );
	prfx = PARAPROBE_DBSC_META_SFTWR; //PARAPROBE_UTILS_SFTWR_META_KEYS;
	for( auto jt = software.begin(); jt != software.end(); jt++ ) {
		cout << jt->keyword << "__" << jt->value << "__" << jt->unit << "__" << jt->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *jt ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing software tool setting " << jt->keyword << " failed!" << "\n";
			return false;
		}
	}
	cout << "Rank " << MASTER << " software settings written to H5" << "\n";

	//add IontypeCombinations using the same prfx
	for( auto kt = itsk.iifo.begin(); kt != itsk.iifo.end(); kt++ ) {
		cout << kt->keyword << "__" << kt->value << "__" << kt->unit << "__" << kt->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *kt ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing software tool setting " << kt->keyword << " failed!" << "\n";
			return false;
		}
	}

	return true;
}


bool dbscanHdl::write_metadata_for_all_analysis_tasks()
{
	double tic = MPI_Wtime();

	string fwslash = "/";
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";
	string grnm = "";
	unsigned int myrank = static_cast<unsigned int>(get_myrank());

	string prefix = PARAPROBE_DBSC_META;
	for( auto tsk = itsk.itasks.begin(); tsk != itsk.itasks.end(); tsk++ ) {
		if ( tsk->rank != myrank ) {
			continue;
		}
		//create group for task case
		grnm = prefix + fwslash + "Task" + to_string(tsk->taskid);
		status = debugh5Hdl.create_group( grnm );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " failed to create group " << grnm << "\n"; return false;
		}

		//ion2edge distance threshold
		vector<apt_real> f32;
		f32.push_back( ConfigDBScan::DatasetEdgeThresholdDistance);
		dsnm = grnm + fwslash + PARAPROBE_DBSC_META_THRS;
		ifo = h5iometa( dsnm, f32.size()/PARAPROBE_DBSC_META_THRS_NCMAX, PARAPROBE_DBSC_META_THRS_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status >= 0 ) {
			offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_DBSC_META_THRS_NCMAX, ifo.nr, PARAPROBE_DBSC_META_THRS_NCMAX);
			status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
			if ( status < 0 ) {
				cerr << "Rank " << get_myrank() << " failed to write " << dsnm << "\n"; return false;
			}
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to create " << dsnm << "\n"; return false;
		}
		f32 = vector<float>();

		//epsilon
		f32.push_back( tsk->eps );
		dsnm = grnm + fwslash + PARAPROBE_DBSC_META_EPS;
		ifo = h5iometa( dsnm, f32.size()/PARAPROBE_DBSC_META_EPS_NCMAX, PARAPROBE_DBSC_META_EPS_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status >= 0 ) {
			offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_DBSC_META_EPS_NCMAX, ifo.nr, PARAPROBE_DBSC_META_EPS_NCMAX);
			status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
			if ( status < 0 ) {
				cerr << "Rank " << get_myrank() << " failed to write " << dsnm << "\n"; return false;
			}
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to create " << dsnm << "\n"; return false;
		}
		f32 = vector<float>();

		//kth nearest
		vector<unsigned int> u32;
		u32.push_back( tsk->kth );
		dsnm = grnm + fwslash + PARAPROBE_DBSC_META_KTH;
		ifo = h5iometa( dsnm, u32.size()/PARAPROBE_DBSC_META_KTH_NCMAX, PARAPROBE_DBSC_META_KTH_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status >= 0 ) {
			offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_DBSC_META_KTH_NCMAX, ifo.nr, PARAPROBE_DBSC_META_KTH_NCMAX);
			status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
			if ( status < 0 ) {
				cerr << "Rank " << get_myrank() << " failed to write " << dsnm << "\n"; return false;
			}
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to create " << dsnm << "\n"; return false;
		}
		u32 = vector<unsigned int>();

		//minpts
		u32.push_back( tsk->minpts );
		dsnm = grnm + fwslash + PARAPROBE_DBSC_META_MINPTS;
		ifo = h5iometa( dsnm, u32.size()/PARAPROBE_DBSC_META_MINPTS_NCMAX, PARAPROBE_DBSC_META_MINPTS_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status >= 0 ) {
			offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_DBSC_META_MINPTS_NCMAX, ifo.nr, PARAPROBE_DBSC_META_MINPTS_NCMAX);
			status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
			if ( status < 0 ) {
				cerr << "Rank " << get_myrank() << " failed to write " << dsnm << "\n"; return false;
			}
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to create " << dsnm << "\n"; return false;
		}
		u32 = vector<unsigned int>();

		//iontype targets
		vector<unsigned char> uc8_tg;
		for( auto it = tsk->targ_nbors.begin(); it != tsk->targ_nbors.end(); it++ ) {
			uc8_tg.push_back( *it );
			size_t itypid = static_cast<size_t>(*it);
			evapion3 const & jt = rng.iontypes.at(itypid).strct;
			uc8_tg.push_back( jt.Z1 ); uc8_tg.push_back( jt.N1 );
			uc8_tg.push_back( jt.Z2 ); uc8_tg.push_back( jt.N2 );
			uc8_tg.push_back( jt.Z3 ); uc8_tg.push_back( jt.N3 );
			uc8_tg.push_back( jt.sign); uc8_tg.push_back( jt.charge );
		}
		dsnm = grnm + fwslash + PARAPROBE_DBSC_META_TYPID_TG;
		ifo = h5iometa( dsnm, uc8_tg.size()/PARAPROBE_DBSC_META_TYPID_TG_NCMAX, PARAPROBE_DBSC_META_TYPID_TG_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
		if ( status >= 0 ) {
			offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_DBSC_META_TYPID_TG_NCMAX, ifo.nr, PARAPROBE_DBSC_META_TYPID_TG_NCMAX);
			status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_tg );
			if ( status < 0 ) {
				cerr << "Rank " << get_myrank() << " failed to write " << dsnm << "\n"; return false;
			}
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to create " << dsnm << "\n"; return false;
		}
		uc8_tg = vector<unsigned char>();

		//number of ions
		vector<size_t> u64;
		u64.push_back( tsk->ifo.nions );
		dsnm = grnm + fwslash + PARAPROBE_DBSC_META_NIONS;
		ifo = h5iometa( dsnm, u64.size()/PARAPROBE_DBSC_META_NIONS_NCMAX, PARAPROBE_DBSC_META_NIONS_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u64le( ifo );
		if ( status >= 0 ) {
			offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_DBSC_META_NIONS_NCMAX, ifo.nr, PARAPROBE_DBSC_META_NIONS_NCMAX);
			status = debugh5Hdl.write_contiguous_matrix_u64le_hyperslab( ifo, offs, u64 );
			if ( status < 0 ) {
				cerr << "Rank " << get_myrank() << " failed to write " << dsnm << "\n"; return false;
			}
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to create " << dsnm << "\n"; return false;
		}
		u64 = vector<size_t>();

		//profiling data
		vector<double> f64;
		f64.push_back( tsk->ifo.tprep );
		f64.push_back( tsk->ifo.tdbsc );
		f64.push_back( tsk->ifo.tpost );
		dsnm = grnm + fwslash + PARAPROBE_DBSC_META_PROF;
		ifo = h5iometa( dsnm, f64.size()/PARAPROBE_DBSC_META_PROF_NCMAX, PARAPROBE_DBSC_META_PROF_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f64le( ifo );
		if ( status >= 0 ) {
			offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_DBSC_META_PROF_NCMAX, ifo.nr, PARAPROBE_DBSC_META_PROF_NCMAX);
			status = debugh5Hdl.write_contiguous_matrix_f64le_hyperslab( ifo, offs, f64 );
			if ( status < 0 ) {
				cerr << "Rank " << get_myrank() << " failed to write " << dsnm << "\n"; return false;
			}
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to create " << dsnm << "\n"; return false;
		}
		f64 = vector<double>();

		//mpi rank
		u32.push_back( tsk->rank );
		dsnm = grnm + fwslash + PARAPROBE_DBSC_META_RANK;
		ifo = h5iometa( dsnm, u32.size()/PARAPROBE_DBSC_META_RANK_NCMAX, PARAPROBE_DBSC_META_RANK_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status >= 0 ) {
			offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_DBSC_META_RANK_NCMAX, ifo.nr, PARAPROBE_DBSC_META_RANK_NCMAX);
			status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
			if ( status < 0 ) {
				cerr << "Rank " << get_myrank() << " failed to write " << dsnm << "\n"; return false;
			}
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to create " << dsnm << "\n"; return false;
		}
		u32 = vector<unsigned int>();

	} //next task

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	dbscan_tictoc.prof_elpsdtime_and_mem( "WriteMetadataRank" + to_string(get_myrank()), APT_XX, APT_IS_SEQ, mm, tic, toc );
	return true;
}


bool dbscanHdl::write_data_for_visualizing_cluster()
{
	double tic = MPI_Wtime();

	string fwslash = "/";
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";
	string grnm = "";
	unsigned int myrank = static_cast<unsigned int>(get_myrank());

	string prefix = PARAPROBE_DBSC_RES;
	for( auto tsk = itsk.itasks.begin(); tsk != itsk.itasks.end(); tsk++ ) {
		if ( tsk->rank != myrank ) {
			continue;
		}
		//create group for task case
		grnm = prefix + fwslash + "Task" + to_string(tsk->taskid);
		status = debugh5Hdl.create_group( grnm );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " failed to create group " << grnm << "\n"; return false;
		}

		//clusterid
		vector<long> i64;
		for( auto it = tsk->lbl_res.begin(); it != tsk->lbl_res.end(); it++ ) {
			i64.push_back( static_cast<long>(it->m1) );
		}
		dsnm = grnm + fwslash + PARAPROBE_DBSC_RES_LBL;
		ifo = h5iometa( dsnm, i64.size()/PARAPROBE_DBSC_RES_LBL_NCMAX, PARAPROBE_DBSC_RES_LBL_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_i64le( ifo );
		if ( status >= 0 ) {
			offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_DBSC_RES_LBL_NCMAX, ifo.nr, PARAPROBE_DBSC_RES_LBL_NCMAX);
			status = debugh5Hdl.write_contiguous_matrix_i64le_hyperslab( ifo, offs, i64 );
			if ( status < 0 ) {
				cerr << "Rank " << get_myrank() << " failed to write " << dsnm << "\n"; return false;
			}
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to create " << dsnm << "\n"; return false;
		}
		i64 = vector<long>();

		//xyz
		vector<float> f32;
		for( auto it = tsk->lbl_res.begin(); it != tsk->lbl_res.end(); it++ ) {
			f32.push_back( it->x );
			f32.push_back( it->y );
			f32.push_back( it->z );
		}
		dsnm = grnm + fwslash + PARAPROBE_DBSC_RES_XYZ;
		ifo = h5iometa( dsnm, f32.size()/PARAPROBE_DBSC_RES_XYZ_NCMAX, PARAPROBE_DBSC_RES_XYZ_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status >= 0 ) {
			offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_DBSC_RES_XYZ_NCMAX, ifo.nr, PARAPROBE_DBSC_RES_XYZ_NCMAX);
			status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
			if ( status < 0 ) {
				cerr << "Rank " << get_myrank() << " failed to write " << dsnm << "\n"; return false;
			}
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to create " << dsnm << "\n"; return false;
		}
		f32 = vector<float>();

		//XDMF topology
		if ( tsk->lbl_res.size() < static_cast<size_t>(UINT32MX-1) ) {
			unsigned int nii = static_cast<unsigned int>(tsk->lbl_res.size());
			vector<unsigned int> u32 = vector<unsigned int>( 3*static_cast<size_t>(nii), 1 ); //XDMF type key for point
			for( unsigned int ii = 0; ii < nii; ii++ ) {
				u32.at(3*ii+2) = ii;
			}
			dsnm = grnm + fwslash + PARAPROBE_DBSC_RES_TOPO;
			ifo = h5iometa( dsnm, u32.size()/PARAPROBE_DBSC_RES_TOPO_NCMAX, PARAPROBE_DBSC_RES_TOPO_NCMAX );
			status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
			if ( status >= 0 ) {
				offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_DBSC_RES_TOPO_NCMAX, ifo.nr, PARAPROBE_DBSC_RES_TOPO_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
				if ( status < 0 ) {
					cerr << "Rank " << get_myrank() << " failed to write " << dsnm << "\n"; return false;
				}
			}
			else {
				cerr << "Rank " << get_myrank() << " failed to create " << dsnm << "\n"; return false;
			}
			u32 = vector<unsigned int>();
		}
		else {
			cerr << "There are more ions than supported by unsigned int of current implementation!" << "\n"; return false;
		}

	} //next task

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	dbscan_tictoc.prof_elpsdtime_and_mem( "WriteClusterVisDataRank" + to_string(get_myrank()), APT_XX, APT_IS_SEQ, mm, tic, toc );
	return true;
}


bool dbscanHdl::write_count_distributions_cluster()
{
	double tic = MPI_Wtime();

	string fwslash = "/";
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";
	string grnm = "";
	unsigned int myrank = static_cast<unsigned int>(get_myrank());

	string prefix = PARAPROBE_DBSC_RES;
	for( auto tsk = itsk.itasks.begin(); tsk != itsk.itasks.end(); tsk++ ) {
		if ( tsk->rank != myrank ) {
			continue;
		}

		//process the size distributions
		//create group for task case
		grnm = prefix + fwslash + "Task" + to_string(tsk->taskid);
		status = debugh5Hdl.create_group( grnm );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " failed to create group " << grnm << "\n"; return false;
		}

		//empirical cumulative distribution without edge correction
		if ( tsk->prec_res.size() > 0 ) {
			//particle count not number of ion count weighted !
			vector<double> f64;
			size_t n_bnd_no = tsk->prec_res.size();
			double cumsum_bnd_no = static_cast<double>(n_bnd_no);
			for ( auto it = tsk->prec_res.begin(); it != tsk->prec_res.end(); it++ ) {
				f64.push_back( static_cast<double>(it->nions) ); //number of ions versus
				f64.push_back( static_cast<double>(it - tsk->prec_res.begin() + 1) / cumsum_bnd_no ); //cdf, +1 upper interval boundary
			}
			dsnm = grnm + fwslash + PARAPROBE_DBSC_RES_CDFRAW;
			ifo = h5iometa( dsnm, f64.size()/PARAPROBE_DBSC_RES_CDFRAW_NCMAX, PARAPROBE_DBSC_RES_CDFRAW_NCMAX );
			status = debugh5Hdl.create_contiguous_matrix_f64le( ifo );
			if ( status >= 0 ) {
				offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_DBSC_RES_CDFRAW_NCMAX, ifo.nr, PARAPROBE_DBSC_RES_CDFRAW_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_f64le_hyperslab( ifo, offs, f64 );
				if ( status < 0 ) {
					cerr << "Rank " << get_myrank() << " failed to write " << dsnm << "\n"; return false;
				}
			}
			else {
				cerr << "Rank " << get_myrank() << " failed to create " << dsnm << "\n"; return false;
			}
			f64 = vector<double>();

			//boundary corrected
			size_t n_bnd_yes = 0;
			for ( auto it = tsk->prec_res.begin(); it != tsk->prec_res.end(); it++ ) {
				if ( it->boundarycontact == false ) {
					n_bnd_yes++;
				}
			}
			if ( n_bnd_yes > 0 ) {
				double cumsum_bnd_yes = static_cast<double>(n_bnd_yes);
				size_t ii = 0;
				for ( auto it = tsk->prec_res.begin(); it != tsk->prec_res.end(); it++ ) {
					if ( it->boundarycontact == false ) {
						f64.push_back( static_cast<double>(it->nions) ); //number of ions versus
						f64.push_back( static_cast<double>((ii + 1) / cumsum_bnd_yes ) ); //cdf
						ii++;
					}
				}
				dsnm = grnm + fwslash + PARAPROBE_DBSC_RES_CDFCLN;
				ifo = h5iometa( dsnm, f64.size()/PARAPROBE_DBSC_RES_CDFCLN_NCMAX, PARAPROBE_DBSC_RES_CDFCLN_NCMAX );
				status = debugh5Hdl.create_contiguous_matrix_f64le( ifo );
				if ( status >= 0 ) {
					offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_DBSC_RES_CDFCLN_NCMAX, ifo.nr, PARAPROBE_DBSC_RES_CDFCLN_NCMAX);
					status = debugh5Hdl.write_contiguous_matrix_f64le_hyperslab( ifo, offs, f64 );
					if ( status < 0 ) {
						cerr << "Rank " << get_myrank() << " failed to write " << dsnm << "\n"; return false;
					}
				}
				else {
					cerr << "Rank " << get_myrank() << " failed to create " << dsnm << "\n"; return false;
				}
				f64 = vector<double>();
			} //done with boundary corrected ECDF
		} //done with ECDFs
	} //next task

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	dbscan_tictoc.prof_elpsdtime_and_mem( "WriteSizeDistrosRank" + to_string(get_myrank()), APT_XX, APT_IS_SEQ, mm, tic, toc );
	return true;
}


bool dbscanHdl::communicate_analysis_tasks_results()
{
	//process generate buffer to store their results
	if ( get_nranks() < 2 ) {
		//single process, master has all information already just needs to collect it
		xdmfifo = vector<MPI_DBScan_Task>();
		for( auto tsk = itsk.itasks.begin(); tsk != itsk.itasks.end(); tsk++ ) {
			if ( tsk->lbl_res.size() <= static_cast<size_t>(UINT32MX-1) && tsk->prec_res.size() <= static_cast<size_t>(UINT32MX-1) ) { //results can be matched on unsigned int
				xdmfifo.push_back( MPI_DBScan_Task( tsk->eps, tsk->kth, tsk->minpts, tsk->taskid, tsk->rank,
						static_cast<unsigned int>(tsk->lbl_res.size()), static_cast<unsigned int>(tsk->prec_res.size())  ) );
			}
			else {
				cerr << "Rank " << get_myrank() << " detected that the results for task " << tsk->taskid << " do not match on datatype unsigned int !" << "\n"; return false;
			}
		}

		sort( xdmfifo.begin(), xdmfifo.end(), SortXDMFIfoForTaskIDAsc );

		cout << "MASTER is skipping results collection step because he has already all solutions!" << "\n";
		return true;
	}
	else {
		int localhealth = 1;
		int globalhealth = 0;
		unsigned int myrank = static_cast<unsigned int>(get_myrank());

		//multiple processes, each process except MASTER defines a send buffer first
		vector<MPI_DBScan_Task> sbuf;
		for( auto tsk = itsk.itasks.begin(); tsk != itsk.itasks.end(); tsk++ ) { //fill the send buffer, ##MK::this is strictly speaking not necessary for master
			if ( tsk->rank != myrank ) {
				continue;
			}
			else { //myrank
				if ( tsk->lbl_res.size() <= static_cast<size_t>(UINT32MX-1) && tsk->prec_res.size() <= static_cast<size_t>(UINT32MX-1) ) { //results can be matched on unsigned int
					sbuf.push_back( MPI_DBScan_Task( tsk->eps, tsk->kth, tsk->minpts, tsk->taskid, tsk->rank,
							static_cast<unsigned int>(tsk->lbl_res.size()), static_cast<unsigned int>(tsk->prec_res.size())  ) );
				}
				else {
					cerr << "Rank " << get_myrank() << " detected that the results for task " << tsk->taskid << " do not match on datatype unsigned int !" << "\n"; localhealth = 0; break;
				}
			}
		} //next task

		//check that sbuf length remains in int32 range
		if ( sbuf.size() >= static_cast<size_t>(INT32MX-1) ) {
			cerr << "Rank " << get_myrank() << " detected that it took care of more than UINT32MX results !" << "\n"; localhealth = 0;
		}

		//check if all processes have filled their send buffers with valid metadata
		globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != get_nranks() ) {
			cerr << "Rank " << get_myrank() << " has recognized that not all processes participated in collection of communicating analysis tasks results!" << "\n";
			return false;
		}

		//identify how many results from which processes and communicate through to all processes
		int* TasksPerRank = new int[get_nranks()];
		for( int rk = 0; rk < get_nranks(); rk++ ) { TasksPerRank[rk] = 0; }
		int nsbuf = static_cast<int>(sbuf.size());

		MPI_Allreduce( &nsbuf, TasksPerRank, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD );

		xdmfifo = vector<MPI_DBScan_Task>();
		for ( int rk = MASTER; rk < get_nranks(); rk++ ) {
			if ( get_myrank() != MASTER ) { //if I am a slave, I send data to master...
				if ( rk == get_myrank() ) { //... when it is my turn
					MPI_DBScan_Task* snd = new MPI_DBScan_Task[TasksPerRank[rk]];
					for( size_t i = 0; i < sbuf.size(); i++ ) {
						snd[i] = sbuf.at(i);
					}
					sbuf = vector<MPI_DBScan_Task>();

					MPI_Send( snd, TasksPerRank[rk], MPI_DBScan_Task_Type, MASTER, rk, MPI_COMM_WORLD );
					delete [] snd; snd = NULL;
				}
			}
			else { //if I am get_myrank() == MASTER the MASTER, I collect
				if ( rk == MASTER ) { //opps, my own data I dump them directly
					xdmfifo.insert( xdmfifo.end(), sbuf.begin(), sbuf.end() );
				}
				else { //rk != MASTER, slaves data I receive data and dump thereafter
					MPI_DBScan_Task* rcv = new MPI_DBScan_Task[TasksPerRank[rk]];
					for( int i = 0; i < TasksPerRank[rk]; i++ ) {
						rcv[i] = MPI_DBScan_Task();
					}

					MPI_Recv( rcv, TasksPerRank[rk], MPI_DBScan_Task_Type, rk, rk, MPI_COMM_WORLD, MPI_STATUS_IGNORE );

					for( int i = 0; i < TasksPerRank[rk]; i++ ) { //dump them!
						xdmfifo.push_back( rcv[i] );
					}
					delete [] rcv; rcv = NULL;
				}
			}
		}

		delete [] TasksPerRank; TasksPerRank = NULL;

		//sort xdmfifo
		sort( xdmfifo.begin(), xdmfifo.end(), SortXDMFIfoForTaskIDAsc );
	}

	return true;
}


bool dbscanHdl::write_xdmf_metadata()
{
	string fwslash = "/";
	vector<DBScanXDMF> xdmfattr;
	for( auto it = xdmfifo.begin(); it != xdmfifo.end(); it++ ) {
		xdmfattr.push_back( DBScanXDMF() );
		xdmfattr.back().nelements = it->lbl_res_n; //as many as there are ions/points
		xdmfattr.back().ntopo = 3*it->lbl_res_n;
		xdmfattr.back().nxyz = 1*it->lbl_res_n;
		xdmfattr.back().tasknm = "Task" + to_string(it->taskid);
		xdmfattr.back().topo_dsnm = PARAPROBE_DBSC_RES + fwslash + "Task" + to_string(it->taskid) + fwslash + PARAPROBE_DBSC_RES_TOPO;
		xdmfattr.back().xyz_dsnm = PARAPROBE_DBSC_RES + fwslash + "Task" + to_string(it->taskid) + fwslash + PARAPROBE_DBSC_RES_XYZ;
	}

	string xmlfn = "PARAPROBE.DBScan.Results.SimID." + to_string(ConfigShared::SimID) + ".h5.xdmf";
	debugxdmf.create_dbscan_file( xmlfn, xdmfattr, debugh5Hdl.h5resultsfn );

	return true;
}


bool dbscanHdl::write_local_results()
{
	if ( write_metadata_for_all_analysis_tasks() == false ) {
		return false;
	}

	if ( ConfigDBScan::IOStoreClusterIDs == true ) {

		if ( write_data_for_visualizing_cluster() == false ) {
			return false;
		}
	}

	if ( ConfigDBScan::IOStoreClusters == true ) {

		if ( write_count_distributions_cluster() == false ) {
			return false;
		}
	}

	return true;
}


/*
bool dbscanHdl::collect_results_on_masterprocess()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER is skipping results collection step because he has already all solutions!" << "\n";
		return true;
	}

	double tic = MPI_Wtime();

	auto mastertsk = results.begin();
	//for( auto tsk = itsk.itasks.begin(); tsk != itsk.itasks.end(); tsk++, mastertsk++ ) {
	for( auto tsk = results.begin(); tsk != results.end(); tsk++, mastertsk++ ) {
		//every process has this task and partial results accumulated in process-local histograms 1d and 3d of the same size
		int localhealth = 1;
		int globalhealth = 0;

		//assure that every process has the same view on a task as the MASTER
		MPI_Spatstat_Task ifo = MPI_Spatstat_Task();
		if ( get_myrank() == MASTER ) {
			ifo.rmin = mastertsk->R.min;
			ifo.rincr = mastertsk->R.incr;
			ifo.rmax = mastertsk->R.max;
			ifo.hst1d_cnt = ( mastertsk->hst1d_res != NULL ) ? static_cast<unsigned int>(mastertsk->hst1d_res->cnts.size()) : 0;
			ifo.sdm3d_cnt = ( mastertsk->sdm3d_res != NULL ) ? mastertsk->sdm3d_res->get_nxyz() : 0;
			ifo.targ_nbors_cnt = static_cast<unsigned int>(mastertsk->targ_nbors.size());
			ifo.pos_firstnbor = static_cast<unsigned int>(mastertsk->pos_firstnbor);
			ifo.kthorder = mastertsk->kthorder;
			ifo.taskid = mastertsk->taskid;
			ifo.WhichLabel = mastertsk->WhichLabel;
			ifo.IsRDF = mastertsk->IsRDF;
			ifo.IsKNN = mastertsk->IsKNN;
			ifo.IsSDM = mastertsk->IsSDM;
		}

		MPI_Bcast( &ifo, 1, MPI_Spatstat_Task_Type, MASTER, MPI_COMM_WORLD );

		if ( get_myrank() != MASTER ) {
			if ( fabs(ifo.rmin - tsk->R.min) > EPSILON || fabs(ifo.rincr - tsk->R.incr) > EPSILON || fabs(ifo.rmax - tsk->R.max) > EPSILON ) {
				cerr << "Rank " << get_myrank() << " detected inconsistency of own tsk->R  with that of the MASTER!" << "\n"; localhealth = 0;
			}
			if ( ifo.hst1d_cnt > 0 ) {
				if ( tsk->hst1d_res != NULL ) {
					if ( tsk->hst1d_res->cnts.size() != ifo.hst1d_cnt ) {
						cerr << "Rank " << get_myrank() << " detected inconsistent size of tsk->hst1d_res with that of the MASTER!" << "\n"; localhealth = 0;
					}
				}
				else {
					cerr << "Rank " << get_myrank() << " detected that it has not allocated hst1d_res but the MASTER!" << "\n"; localhealth = 0;
				}
			}
			else { //== 0
				if ( tsk->hst1d_res != NULL ) {
					cerr << "Rank " << get_myrank() << " detected MASTER has a hst1d_res == NULL but for me it is != NULL!" << "\n"; localhealth = 0;
				}
			}
			if ( ifo.sdm3d_cnt > 0 ) {
				if ( tsk->sdm3d_res != NULL ) {
					if ( tsk->sdm3d_res->get_nxyz() != ifo.sdm3d_cnt ) {
						cerr << "Rank " << get_myrank() << " detected inconsistent size of tsk->sdm3d_res with that of the MASTER!" << "\n"; localhealth = 0;
					}
				}
				else {
					cerr << "Rank " << get_myrank() << " detected that it has not allocated sdm3d_res but the MASTER!" << "\n"; localhealth = 0;
				}
			}
			else { //==0
				if ( tsk->sdm3d_res != NULL ) {
					cerr << "Rank " << get_myrank() << " detected MASTER has a sdm3d_res == NULL but for me it is != NULL!" << "\n"; localhealth = 0;
				}
			}
			if ( ifo.targ_nbors_cnt != static_cast<unsigned int>(tsk->targ_nbors.size()) || ifo.pos_firstnbor != static_cast<unsigned int>(tsk->pos_firstnbor) ||
					ifo.kthorder != tsk->kthorder || ifo.taskid != tsk->taskid ) {
				cerr << "Rank " << get_myrank() << " detected that targ_nbors, pos_firstnbor, kthorder, taskid is inconsistent to MASTER!" << "\n"; localhealth = 0;
			}
			if ( ifo.WhichLabel != tsk->WhichLabel || ifo.IsRDF != tsk->IsRDF || ifo.IsKNN != tsk->IsKNN || ifo.IsSDM != tsk->IsSDM ) {
				cerr << "Rank " << get_myrank() << " detected that WhichLabel, IsRDF, IsKNN, IsSDM  is inconsistent to MASTER!" << "\n"; localhealth = 0;
			}
		}

		//if there is an error all go out and not just only one process
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != get_nranks() ) {
			cerr << "Rank " << get_myrank() << " has recognized that not all processes have same sized buffers for task " << ifo.taskid << " !" << "\n"; return false;
		}

		if ( ( ifo.IsRDF == true || ifo.IsKNN == true ) && ifo.IsSDM == false ) {
cout << "Rank " << get_myrank() << " prepare RDF/KNN ifo.taskid " << ifo.taskid << "\n";
			localhealth = 1;
			globalhealth = 0;

			double* snd = NULL;
			double* rcv = NULL;
			size_t nb = 0;
			if ( ifo.hst1d_cnt > 0 ) {
				nb = ifo.hst1d_cnt + 2; //+2 for lowest and highest dump!
				snd = new double[nb];
				if ( snd != NULL ) {
					for( unsigned int b = 0; b < nb-2; b++ ) {
						snd[b] = tsk->hst1d_res->cnts.at(b);
					}
					snd[nb-2] = tsk->hst1d_res->cnts_lowest;
					snd[nb-1] = tsk->hst1d_res->cnts_highest;
				}
				else {
					cerr << "Rank " << get_myrank() << " was unable hst1d_res to allocate snd buffer for task " << ifo.taskid << " !" << "\n"; localhealth = 0;
				}
				//different to snd, buffer rcv is significant only at root/MASTER

				if ( get_myrank() == MASTER ) {
					rcv = new double[nb];
					if ( rcv != NULL ) {
						for( unsigned int b = 0; b < nb; b++ ) {
							rcv[b] = 0.0;
						}
					}
					else {
						cerr << "Rank " << MASTER << " was unable hst1d_res to allocate snd buffer for task " << ifo.taskid << " !" << "\n"; localhealth = 0;
					}
				}

				MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
				if ( globalhealth != get_nranks() ) {
					cerr << "Rank " << get_myrank() << " has recognized that not all processes have same sized buffers for task " << ifo.taskid << " !" << "\n"; return false;
				}

cout << "Rank " << get_myrank() << " reduce RDF/KNN ifo.taskid " << ifo.taskid << "\n";
				//pull bin values and low/highest from all people
				MPI_Reduce( snd, rcv, static_cast<int>(nb), MPI_DOUBLE, MPI_SUM, MASTER, MPI_COMM_WORLD );

				delete [] snd; snd = NULL;
				if ( get_myrank() == MASTER ) {
					for( unsigned int b = 0; b < nb-2; b++ ) {
						mastertsk->hst1d_res->cnts.at(b) = rcv[b];
					}
					mastertsk->hst1d_res->cnts_lowest = rcv[nb-2];
					mastertsk->hst1d_res->cnts_highest = rcv[nb-1];
					delete [] rcv; rcv = NULL;
				}
			}

			//##MK::for debugging but performance killer
cout << "Rank " << get_myrank() << " rcv/snd buffer synced " << ifo.taskid << "\n";

			MPI_Barrier( MPI_COMM_WORLD );
			continue;
		}

		if ( ( ifo.IsRDF == false && ifo.IsKNN == false ) && ifo.IsSDM == true ) {
cout << "Rank " << get_myrank() << " prepare SDM ifo.taskid " << ifo.taskid << "\n";
			localhealth = 1;
			globalhealth = 0;

			unsigned int* snd = NULL;
			unsigned int* rcv = NULL;
			size_t nb = 0;
			if ( ifo.sdm3d_cnt > 0 ) {
				nb = ifo.sdm3d_cnt;
				snd = new unsigned int[nb];
				if ( snd != NULL ) {
					for( unsigned int b = 0; b < nb; b++ ) {
						snd[b] = tsk->sdm3d_res->cnts.at(b);
					}
				}
				else {
					cerr << "Rank " << get_myrank() << " was unable sdm3d_res to allocate snd buffer for task " << ifo.taskid << " !" << "\n"; localhealth = 0;
				}
				//different to snd, buffer rcv is significant only at root/MASTER
				if ( get_myrank() == MASTER ) {
					rcv = new unsigned int[nb];
					if ( rcv != NULL ) {
						for( unsigned int b = 0; b < nb; b++ ) {
							rcv[b] = 0;
						}
					}
					else {
						cerr << "Rank " << MASTER << " was unable sdm3d_res to allocate snd buffer for task " << ifo.taskid << " !" << "\n"; localhealth = 0;
					}
				}

				MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
				if ( globalhealth != get_nranks() ) {
					cerr << "Rank " << get_myrank() << " has recognized that not all processes have same sized buffers for task " << ifo.taskid << " !" << "\n"; return false;
				}

cout << "Rank " << get_myrank() << " reduce SDM ifo.taskid " << ifo.taskid << "\n";
				//pull bin values and low/highest from all people
				MPI_Reduce( snd, rcv, static_cast<int>(nb), MPI_UNSIGNED, MPI_SUM, MASTER, MPI_COMM_WORLD );

				delete [] snd; snd = NULL;
				if ( get_myrank() == MASTER ) {
					for( unsigned int b = 0; b < nb; b++ ) {
						mastertsk->sdm3d_res->cnts.at(b) = rcv[b];
					}
					delete [] rcv; rcv = NULL;
				}
			}

			//##MK::for debugging but performance killer
cout << "Rank " << get_myrank() << " rcv/snd buffer synced " << ifo.taskid << "\n";

			MPI_Barrier( MPI_COMM_WORLD );
			continue;
		}
	} //next task


	double toc = MPI_Wtime();
	memsnapshot mm = spatstat_tictoc.get_memoryconsumption();
	spatstat_tictoc.prof_elpsdtime_and_mem( "CollectResultsOnMasterProcess", APT_XX, APT_IS_PAR, mm, tic, toc);
	cout << "Rank " << get_myrank() << " participated successfully in results collection stage took " << (toc-tic) << " seconds" << "\n";

    return true;
}


bool dbscanHdl::write_materialpoints_to_apth5()
{
	if ( get_myrank() != MASTER ) {
		cout << "Non-MASTER processes do not participate in results I/O!" << "\n"; return true;
	}

	double tic = MPI_Wtime();

	string fwslash = "/";
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";
	string grnm = "";

	vector<unsigned char> uc8_tg;
	vector<unsigned char> uc8_nb;
	bool SDM_SupportGrid_Required = false;
	vector<SpatstatSDMXDMF> xdmfattr;

	for( auto tsk = results.begin(); tsk != results.end(); tsk++ ) {
cout << "taskid/IsRDF/IsKNN/IsSDM = " << tsk->taskid << ";" << tsk->IsRDF << ";" << tsk->IsKNN << ";"<< tsk->IsSDM << "\n";

		//iontypes
		uc8_tg = vector<unsigned char>();
		uc8_nb = vector<unsigned char>();
		for( auto it = tsk->targ_nbors.begin(); it != (tsk->targ_nbors.begin() + tsk->pos_firstnbor); it++ ) {
			uc8_tg.push_back( *it );
			size_t itypid = static_cast<size_t>(*it);
			evapion3 const & jt = rng.iontypes.at(itypid).strct;
			uc8_tg.push_back( jt.Z1 ); uc8_tg.push_back( jt.N1 );
			uc8_tg.push_back( jt.Z2 ); uc8_tg.push_back( jt.N2 );
			uc8_tg.push_back( jt.Z3 ); uc8_tg.push_back( jt.N3 );
			uc8_tg.push_back( jt.sign); uc8_tg.push_back( jt.charge );
		}
		for( auto it = (tsk->targ_nbors.begin() + tsk->pos_firstnbor); it != tsk->targ_nbors.end(); it++ ) {
			uc8_nb.push_back( *it );
			size_t itypid = static_cast<size_t>(*it);
			evapion3 const & jt = rng.iontypes.at(itypid).strct;
			uc8_nb.push_back( jt.Z1 ); uc8_nb.push_back( jt.N1 );
			uc8_nb.push_back( jt.Z2 ); uc8_nb.push_back( jt.N2 );
			uc8_nb.push_back( jt.Z3 ); uc8_nb.push_back( jt.N3 );
			uc8_nb.push_back( jt.sign); uc8_nb.push_back( jt.charge );
		}

		if ( tsk->IsRDF == true && tsk->IsKNN == false && tsk->IsSDM == false ) {
			if ( tsk->hst1d_res != NULL ) {
cout << "taskid/hst1d_res " << tsk->taskid << ";" << tsk->hst1d_res << "\n";
				//results
				grnm = PARAPROBE_SPST_RDF_RES + fwslash + "Task" + to_string(tsk->taskid);
				status = debugh5Hdl.create_group( grnm );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create group " << grnm << "\n"; return false;
				}
				dsnm = grnm + fwslash + PARAPROBE_SPST_RDF_RES_CNTS;
				unsigned int nb = tsk->hst1d_res->cnts.size();
				ifo = h5iometa( dsnm, nb+2, 2 ); //+2 for lower and upper, radius;value
				vector<double> f64 = vector<double>( ifo.nr*ifo.nc, 0.0 );

				//###additional columns are necessary to normalize...
				f64.at(0*2+0) = tsk->hst1d_res->bounds.min; //cnts_lowest dump
				f64.at(0*2+1) = tsk->hst1d_res->cnts_lowest;
				for( unsigned int b = 0; b < nb; b++ ) {
					f64.at(1*2+b*2+0) = tsk->hst1d_res->right(b);
					f64.at(1*2+b*2+1) = tsk->hst1d_res->cnts.at(b);
				}
				f64.at(1*2+nb*2+0*2+0) = F64MX;
				f64.at(1*2+nb*2+0*2+1) = tsk->hst1d_res->cnts_highest;

				status = debugh5Hdl.create_contiguous_matrix_f64le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, nb+2, 0, PARAPROBE_SPST_RDF_RES_CNTS_NCMAX, nb+2, PARAPROBE_SPST_RDF_RES_CNTS_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_f64le_hyperslab( ifo, offs, f64 );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				f64 = vector<double>();

				//metadata
				grnm = PARAPROBE_SPST_RDF_META + fwslash + "Task" + to_string(tsk->taskid);
				status = debugh5Hdl.create_group( grnm );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create group " << grnm << "\n"; return false;
				}

				//iontypes used
				dsnm = grnm + fwslash + PARAPROBE_SPST_RDF_META_TYPID_TG;
				ifo = h5iometa( dsnm, tsk->pos_firstnbor, PARAPROBE_SPST_RDF_META_TYPID_TG_NCMAX );
				status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_SPST_RDF_META_TYPID_TG_NCMAX, ifo.nr, PARAPROBE_SPST_RDF_META_TYPID_TG_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_tg );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}

				dsnm = grnm + fwslash + PARAPROBE_SPST_RDF_META_TYPID_NB;
				ifo = h5iometa( dsnm, tsk->targ_nbors.size()-tsk->pos_firstnbor, PARAPROBE_SPST_RDF_META_TYPID_NB_NCMAX );
				status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_SPST_RDF_META_TYPID_NB_NCMAX, ifo.nr, PARAPROBE_SPST_RDF_META_TYPID_NB_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_nb );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}

				//randomization info
				vector<unsigned char> uc8_rnd;
				if (tsk->WhichLabel == RANDOMIZED_IONTYPES ) {
					uc8_rnd.push_back( 0x01 );
					dsnm = grnm + fwslash + "RandomizedLabels";
				}
				else { //ORIGINAL_IONTYPES
					uc8_rnd.push_back( 0x00 );
					dsnm = grnm + fwslash + "OriginalLabels";
				}
				ifo = h5iometa( dsnm, 1, 1 );
				status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, 1, 0, 1, 1, 1);
				status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_rnd );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				uc8_rnd = vector<unsigned char>();

				delete tsk->hst1d_res;
				tsk->hst1d_res = NULL;
			}
			continue;
		}
		if ( tsk->IsRDF == false && tsk->IsKNN == true && tsk->IsSDM == false ) {
			if ( tsk->hst1d_res != NULL ) {
cout << "taskid/hst1d_res " << tsk->taskid << ";" << tsk->hst1d_res << "\n";
				//results
				grnm = PARAPROBE_SPST_KNN_RES + fwslash + "Task" + to_string(tsk->taskid);
				status = debugh5Hdl.create_group( grnm );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create group " << grnm << "\n"; return false;
				}
				dsnm = grnm + fwslash + PARAPROBE_SPST_KNN_RES_CNTS;
				unsigned int nb = tsk->hst1d_res->cnts.size();
				ifo = h5iometa( dsnm, nb+2, 2 ); //+2 for lower and upper, radius;value
				vector<double> f64 = vector<double>( ifo.nr*ifo.nc, 0.0 );

				f64.at(0*2+0) = tsk->hst1d_res->bounds.min; //cnts_lowest dump
				f64.at(0*2+1) = tsk->hst1d_res->cnts_lowest;
				for( unsigned int b = 0; b < nb; b++ ) {
					f64.at(1*2+b*2+0) = tsk->hst1d_res->right(b);
					f64.at(1*2+b*2+1) = tsk->hst1d_res->cnts.at(b);
				}
				f64.at(1*2+nb*2+0*2+0) = F64MX;
				f64.at(1*2+nb*2+0*2+1) = tsk->hst1d_res->cnts_highest;

				status = debugh5Hdl.create_contiguous_matrix_f64le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, nb+2, 0, PARAPROBE_SPST_KNN_RES_CNTS_NCMAX, nb+2, PARAPROBE_SPST_KNN_RES_CNTS_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_f64le_hyperslab( ifo, offs, f64 );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				f64 = vector<double>();

				//metadata
				grnm = PARAPROBE_SPST_KNN_META + fwslash + "Task" + to_string(tsk->taskid);
				status = debugh5Hdl.create_group( grnm );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create group " << grnm << "\n"; return false;
				}

				//iontypes used
				dsnm = grnm + fwslash + PARAPROBE_SPST_KNN_META_TYPID_TG;
				ifo = h5iometa( dsnm, tsk->pos_firstnbor, PARAPROBE_SPST_KNN_META_TYPID_TG_NCMAX );
				status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_SPST_KNN_META_TYPID_TG_NCMAX, ifo.nr, PARAPROBE_SPST_KNN_META_TYPID_TG_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_tg );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				dsnm = grnm + fwslash + PARAPROBE_SPST_KNN_META_TYPID_NB;
				ifo = h5iometa( dsnm, tsk->targ_nbors.size()-tsk->pos_firstnbor, PARAPROBE_SPST_KNN_META_TYPID_NB_NCMAX );
				status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_SPST_KNN_META_TYPID_NB_NCMAX, ifo.nr, PARAPROBE_SPST_KNN_META_TYPID_NB_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_nb );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}

				//randomization info
				vector<unsigned char> uc8_rnd;
				if (tsk->WhichLabel == RANDOMIZED_IONTYPES ) {
					uc8_rnd.push_back( 0x01 );
					dsnm = grnm + fwslash + "RandomizedLabels";
				}
				else { //ORIGINAL_IONTYPES
					uc8_rnd.push_back( 0x00 );
					dsnm = grnm + fwslash + "OriginalLabels";
				}
				ifo = h5iometa( dsnm, 1, 1 );
				status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, 1, 0, 1, 1, 1);
				status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_rnd );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				uc8_rnd = vector<unsigned char>();

				//kth order info
				vector<unsigned int> u32;
				u32.push_back( tsk->kthorder );
				dsnm = grnm + fwslash + "kthNearestNeighbor";
				ifo = h5iometa( dsnm, 1, 1);
				status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, 1, 0, 1, 1, 1);
				status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				u32 = vector<unsigned int>();

				delete tsk->hst1d_res;
				tsk->hst1d_res = NULL;
			}
			continue;
		}
		if ( tsk->IsRDF == false && tsk->IsKNN == false && tsk->IsSDM == true ) {
			if ( tsk->sdm3d_res != NULL ) {
				SDM_SupportGrid_Required = true;
cout << "taskid/sdm3d_res " << tsk->taskid << ";" << tsk->sdm3d_res << "\n";
				//results
				grnm = PARAPROBE_SPST_SDM_RES + fwslash + "Task" + to_string(tsk->taskid);
				status = debugh5Hdl.create_group( grnm );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create group " << grnm << "\n"; return false;
				}
				dsnm = grnm + fwslash + PARAPROBE_SPST_SDM_RES_CNTS;
				unsigned int nb = tsk->sdm3d_res->cnts.size();
				ifo = h5iometa( dsnm, nb, 1 );
				vector<unsigned int> u32 = vector<unsigned int>( nb, 0 );

				u32 = tsk->sdm3d_res->cnts; //deep copy

				status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, nb, 0, PARAPROBE_SPST_SDM_RES_CNTS_NCMAX, nb, PARAPROBE_SPST_SDM_RES_CNTS_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				u32 = vector<unsigned int>();

				//metadata
				grnm = PARAPROBE_SPST_SDM_META + fwslash + "Task" + to_string(tsk->taskid);
				status = debugh5Hdl.create_group( grnm );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create group " << grnm << "\n"; return false;
				}

				//iontypes
				dsnm = grnm + fwslash + PARAPROBE_SPST_SDM_META_TYPID_TG;
				ifo = h5iometa( dsnm, tsk->pos_firstnbor, PARAPROBE_SPST_SDM_META_TYPID_TG_NCMAX );
				status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_SPST_SDM_META_TYPID_TG_NCMAX, ifo.nr, PARAPROBE_SPST_SDM_META_TYPID_TG_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_tg );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				uc8_tg = vector<unsigned char>();
				dsnm = grnm + fwslash + PARAPROBE_SPST_SDM_META_TYPID_NB;
				ifo = h5iometa( dsnm, tsk->targ_nbors.size()-tsk->pos_firstnbor, PARAPROBE_SPST_SDM_META_TYPID_NB_NCMAX );
				status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_SPST_SDM_META_TYPID_NB_NCMAX, ifo.nr, PARAPROBE_SPST_SDM_META_TYPID_NB_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_nb );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				uc8_nb = vector<unsigned char>();

				//randomization info
				vector<unsigned char> uc8_rnd;
				if (tsk->WhichLabel == RANDOMIZED_IONTYPES ) {
					uc8_rnd.push_back( 0x01 );
					dsnm = grnm + fwslash + "RandomizedLabels";
				}
				else { //ORIGINAL_IONTYPES
					uc8_rnd.push_back( 0x00 );
					dsnm = grnm + fwslash + "OriginalLabels";
				}
				ifo = h5iometa( dsnm, 1, 1 );
				status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, 1, 0, 1, 1, 1);
				status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_rnd );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				uc8_rnd = vector<unsigned char>();

				//kth order info
				u32.push_back( tsk->kthorder );
				dsnm = grnm + fwslash + "kthNearestNeighbor";
				ifo = h5iometa( dsnm, 1, 1);
				status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, 1, 0, 1, 1, 1);
				status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				u32 = vector<unsigned int>();

				xdmfattr.push_back( SpatstatSDMXDMF() );
				xdmfattr.back().nelements = tsk->sdm3d_res->get_nxyz();
				xdmfattr.back().ntopo = 3*tsk->sdm3d_res->get_nxyz();
				xdmfattr.back().nxyz = 1*tsk->sdm3d_res->get_nxyz();
				xdmfattr.back().nm = ( tsk->WhichLabel == RANDOMIZED_IONTYPES ) ?
						"Task" + to_string(tsk->taskid) + "Rnd" : "Task" + to_string(tsk->taskid) + "Org";
				xdmfattr.back().dsnm = PARAPROBE_SPST_SDM_RES + fwslash + "Task" + to_string(tsk->taskid) +
						fwslash + PARAPROBE_SPST_SDM_RES_CNTS;

				delete tsk->sdm3d_res;
				tsk->sdm3d_res = NULL;
			}
			continue;
		}
	}

	//if there are SDM tasks we also store a point grid with which to visualize the SDM directly in Paraview
	if ( SDM_SupportGrid_Required == true ) {
		//##MK::currently we use the same tsk->R radii settings for all SDM tasks! otherwise make this per task!
		vector<float> f32;
		sdm3d* sdm = NULL;
		sdm = new sdm3d( ConfigSpatstat::ROIRadiiSDM.max, ConfigSpatstat::ROIRadiiSDM.incr, 201 );
		size_t nb = 0;
		if ( sdm != NULL ) {
			nb = sdm->get_nxyz();
			f32.reserve( nb * PARAPROBE_SPST_SDM_META_XYZ_NCMAX );
			unsigned int nx = sdm->get_nx();
			for( unsigned int z = 0; z < nx; z++ ) {
				for( unsigned int y = 0; y < nx; y++ ) {
					for( unsigned int x = 0; x < nx; x++ ) {
						p3d here = sdm->get_bincenter( x, y, z );
						f32.push_back( here.x );
						f32.push_back( here.y );
						f32.push_back( here.z );
					}
				}
			}

			dsnm = PARAPROBE_SPST_SDM_META_XYZ;
			ifo = h5iometa( dsnm, nb, PARAPROBE_SPST_SDM_META_XYZ_NCMAX );
			status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
			if ( status < 0 ) {
				cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
			}
			offs = h5offsets( 0, nb, 0, PARAPROBE_SPST_SDM_META_XYZ_NCMAX, nb, PARAPROBE_SPST_SDM_META_XYZ_NCMAX);
			status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
			if ( status < 0 ) {
				cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
			}
			f32 = vector<float>();

			delete sdm; sdm = NULL;

			vector<unsigned int> u32 = vector<unsigned int>( 3*nb, 1 ); //XDMF geom primitive and topo type key and vertex ID
			for( size_t b = 0; b < nb; b++ ) {
				u32.at(3*b+2) = static_cast<unsigned int>(b);
			}
			dsnm = PARAPROBE_SPST_SDM_META_TOPO;
			ifo = h5iometa( dsnm, 3*nb, PARAPROBE_SPST_SDM_META_TOPO_NCMAX );
			status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
			if ( status < 0 ) {
				cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
			}
			offs = h5offsets( 0, 3*nb, 0, PARAPROBE_SPST_SDM_META_TOPO_NCMAX, 3*nb, PARAPROBE_SPST_SDM_META_TOPO_NCMAX);
			status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
			if ( status < 0 ) {
				cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
			}
			u32 = vector<unsigned int>();
		}
		else {
			cerr << "Rank " << MASTER << " failed to generate SDM to write PARAPROBE_SPST_SDM_META_XYZ!" << "\n"; return false;
		}
	}

	string xmlfn = "PARAPROBE.Spatstat.Results.SimID." + to_string(ConfigShared::SimID) + ".h5.SDM.xdmf";
	debugxdmf.create_spatstat_file( xmlfn, xdmfattr, debugh5Hdl.h5resultsfn );

	double toc = MPI_Wtime();
	memsnapshot mm = spatstat_tictoc.get_memoryconsumption();
	spatstat_tictoc.prof_elpsdtime_and_mem( "WriteResultsToHDF5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	cout << "Rank " << get_myrank() << " Writing of material infos into H5 file took " << (toc-tic) << " seconds" << "\n";
	return true;
}
*/

int dbscanHdl::get_myrank()
{
	return this->myrank;
}


int dbscanHdl::get_nranks()
{
	return this->nranks;
}


void dbscanHdl::set_myrank( const int rr )
{
	this->myrank = rr;
}


void dbscanHdl::set_nranks( const int nn )
{
	this->nranks = nn;
}


void dbscanHdl::init_mpidatatypes()
{
	MPI_Type_contiguous(2, MPI_UNSIGNED_LONG_LONG, &MPI_Ranger_DictKeyVal_Type);
	MPI_Type_commit(&MPI_Ranger_DictKeyVal_Type);

	MPI_Type_contiguous(3, MPI_FLOAT, &MPI_Synth_XYZ_Type);
	MPI_Type_commit(&MPI_Synth_XYZ_Type);

	MPI_Type_contiguous(9, MPI_UNSIGNED_CHAR, &MPI_Ranger_Iontype_Type);
	MPI_Type_commit(&MPI_Ranger_Iontype_Type);

	int elementCounts[2] = {2, 4};
	MPI_Aint displacements[2] = {0, 2 * MPIIO_OFFSET_INCR_F32};
	MPI_Datatype oldTypes[2] = {MPI_FLOAT, MPI_UNSIGNED_CHAR};
	MPI_Type_create_struct(2, elementCounts, displacements, oldTypes, &MPI_Ranger_MQIval_Type);
	MPI_Type_commit(&MPI_Ranger_MQIval_Type);

	int elementCounts2[2] = {1, 6};
	MPI_Aint displacements2[2] = {0, 6 * MPIIO_OFFSET_INCR_F32 };
	MPI_Datatype oldTypes2[2] = {MPI_FLOAT, MPI_UNSIGNED};
	MPI_Type_create_struct(2, elementCounts2, displacements2, oldTypes2, &MPI_DBScan_Task_Type);
	MPI_Type_commit(&MPI_DBScan_Task_Type);
}
