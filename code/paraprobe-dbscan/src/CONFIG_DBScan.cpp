//##MK::GPLV3

#include "CONFIG_DBScan.h"

//string ConfigDBScan::InputfilePSE = "";
string ConfigDBScan::InputfileReconstruction = "";
string ConfigDBScan::InputfileHullAndDistances = "";
DBSCAN_MODE ConfigDBScan::ClusteringMethod = NOTHING;

lival_real ConfigDBScan::Epsilon = lival_real();
lival_u32 ConfigDBScan::MinPtsKth = lival_u32();
lival_u32 ConfigDBScan::MaxSepMinPts = lival_u32();
lival_real ConfigDBScan::MaxSepEroDist = lival_real();
apt_real ConfigDBScan::DatasetEdgeThresholdDistance = 0.0;

bool ConfigDBScan::IOStoreClusterIDs = false;
bool ConfigDBScan::IOStoreClusters = false;

size_t ConfigDBScan::MaxSizeCachedResPerNode = GIGABYTE2BYTE(static_cast<size_t>(16)); //useful value for talos but should be auto-parsed!
//MK::global seeds for pseudorandom number generation (PRNG) need to be the same across all processes!
//##MK::otherwise processes do different label randomization

/*
pair<string,bool> parse_knn_neighbors( const string in, vector<unsigned int> & out )
{
	//parse and chop k-nearest neighbor ID string into values if possible
	pair<string,bool> status = pair<string,bool>( "", true );
	
	stringstream parsethis;
	parsethis << in;
	string datapiece;
	
	size_t nsemicolon = count( in.begin(), in.end(), ';' );
	//https://stackoverflow.com/questions/8888748/how-to-check-if-given-c-string-or-char-contains-only-digits
	if ( in.size() < 1 ) {
		string mess = "No values were passed to parse_knn_neighbors!";
		return pair<string,bool>( mess, false );
	}
	
	for( size_t i = 0; i < nsemicolon + 1; i++ ) { //parse at least the single value
		getline( parsethis, datapiece, ';');
		unsigned long val = stoul(datapiece);
		if ( val < UINT32MX ) {
			out.push_back( static_cast<unsigned int>(val) );
		}
		else {
			string mess = to_string(val) + " is a very large value this seems unrealistic!";
			return pair<string,bool>( mess, false );
		}
	}
	return pair<string,bool>( "", true );
}
*/


bool ConfigDBScan::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.DBScan.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigDBScan")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;
  
	//InputfilePSE = read_xml_attribute_string( rootNode, "InputfilePSE" );
	InputfileReconstruction = read_xml_attribute_string( rootNode, "InputfileReconstruction" );
	InputfileHullAndDistances = read_xml_attribute_string( rootNode, "InputfileHullAndDistances" );

	//WHAT TYPE OF ANALYSIS TO DO
	mode = read_xml_attribute_uint32( rootNode, "ClusteringMethod" );
	switch (mode)
	{
		case NORMAL:
			ClusteringMethod = NORMAL; break;
		case MAXSEP:
			ClusteringMethod = MAXSEP; break;
		default:
			ClusteringMethod = NOTHING;
	}
	
	Epsilon = lival_real(	read_xml_attribute_float( rootNode, "DBScanEpsilonMin" ),
							read_xml_attribute_float( rootNode, "DBScanEpsilonIncr" ),
							read_xml_attribute_float( rootNode, "DBScanEpsilonMax"  )  );
							
    MinPtsKth = lival_u32( 	read_xml_attribute_uint32( rootNode, "DBScanMinPtsMin" ),
							read_xml_attribute_uint32( rootNode, "DBScanMinPtsIncr" ),
							read_xml_attribute_uint32( rootNode, "DBScanMinPtsMax" )   );

	if ( ClusteringMethod == MAXSEP ) {
		MaxSepMinPts = lival_u32(		read_xml_attribute_uint32( rootNode, "MaxSepNumberOfIonsMin" ),
										read_xml_attribute_uint32( rootNode, "MaxSepNumberOfIonsIncr" ),
										read_xml_attribute_uint32( rootNode, "MaxSepNumberOfIonsMax" )     );
		/*
		MaxSepEroDist = lival_real( 	read_xml_attribute_uint32( rootNode, "MaxSepErosionDistanceMin" ),
										read_xml_attribute_uint32( rootNode, "MaxSepErosionDistanceIncr" ),
										read_xml_attribute_uint32( rootNode, "MaxSepErosionDistanceMax" )   );
		*/
	}

    DatasetEdgeThresholdDistance = read_xml_attribute_float( rootNode, "DatasetEdgeThresholdDistance" );

    IOStoreClusterIDs = read_xml_attribute_bool( rootNode, "IOStoreClusterIDs" );
    IOStoreClusters = read_xml_attribute_bool( rootNode, "IOStoreClusters" );

	//MaxSizeCachedResPerNode = GIGABYTE2BYTE(read_xml_attribute_uint32( rootNode, "MaxMemGigabytePerNode"));
	return true;
}


bool ConfigDBScan::checkUserInput()
{
	cout << "ConfigDBScan::" << "\n";
	switch(ClusteringMethod)
	{
		case NORMAL:
			cout << "Doing classical DBScan algorithm" << "\n"; break;
		case MAXSEP:
			cout << "Performing a maximum separation method" << "\n";
			cerr << "Maximum separation method currently without ion erosion!" << "\n";
			break;
		default:
			cerr << "No analysis tasks chosen!" << "\n"; return false;
	}
	
	if ( Epsilon.min < 0.0 ) {
		cerr << "DBScanEpsilonMin must be at least 0.0!" << "\n"; return false;
	}
	if ( Epsilon.incr < EPSILON ) {
		cerr << "DBScanEpsilonIncr must be positive and finite!" << "\n"; return false;
	}
	if ( Epsilon.max < Epsilon.min ) {
		cerr << "DBScanEpsilonMax must be at least as large as DBScanEpsilonMin!" << "\n"; return false;
	}
	cout << "DBScanEpsilonMin " << Epsilon.min << "\n";
	cout << "DBScanEpsilonIncr " << Epsilon.incr << "\n";
	cout << "DBScanEpsilonMax " << Epsilon.max << "\n";
	
	if ( MinPtsKth.min < 1 ) {
		cerr << "DBScanMinPtsMin needs to be at least 1 !" << "\n"; return false;
	}
	if ( MinPtsKth.incr < 1 ) {
		cerr << "DBScanMinPtsIncr must be at least 1 !" << "\n"; return false;
	}
	if ( MinPtsKth.max < MinPtsKth.min ) {
		cerr << "DBScanMinPtsMax must be at least as large as DBScanMinPtsMin!" << "\n"; return false;
	}
	cout << "DBScanMinPtsMin " << MinPtsKth.min << "\n";
	cout << "DBScanMinPtsIncr " << MinPtsKth.incr << "\n";
	cout << "DBScanMinPtsMax " << MinPtsKth.max << "\n";
	
	if ( ClusteringMethod == MAXSEP ) {
		if ( MaxSepMinPts.min < 1 ) {
			cerr << "MaxSepNumberOfIonsMin needs to be at least 1 !" << "\n"; return false;
		}
		if ( MaxSepMinPts.incr < 1 ) {
			cerr << "MaxSepNumberOfIonsIncr must be at least 1 !" << "\n"; return false;
		}
		if ( MaxSepMinPts.max < MaxSepMinPts.min ) {
			cerr << "MaxSepNumberOfIonsMax must be at least as large as MaxSepNumberOfIonsMin!" << "\n"; return false;
		}
		cout << "MaxSepNumberOfIonsMin " << MaxSepMinPts.min << "\n";
		cout << "MaxSepNumberOfIonsIncr " << MaxSepMinPts.incr << "\n";
		cout << "MaxSepNumberOfIonsMax " << MaxSepMinPts.max << "\n";

		/*
		if ( MaxSepEroDist.min < 0.0 ) {
			cerr << "MaxSepErosionDistanceMin must be at least 0.0!" << "\n"; return false;
		}
		if ( MaxSepEroDist.incr < EPSILON ) {
			cerr << "MaxSepErosionDistanceIncr must be at least positive and finite!" << "\n"; return false;
		}
		if ( MaxSepEroDist.max < MaxSepEroDist.min ) {
			cerr << "MaxSepErosionDistanceMax must be at least as large as MaxSepErosionDistanceMin!" << "\n"; return false;
		}
		cout << "MaxSepErosionDistanceMin " << MaxSepEroDist.min << "\n";
		cout << "MaxSepErosionDistanceIncr " << MaxSepEroDist.incr << "\n";
		cout << "MaxSepErosionDistanceMax " << MaxSepEroDist.max << "\n";
		*/
	}

	cout << "DatasetEdgeThresholdDistance " << DatasetEdgeThresholdDistance << "\n";
	
	cout << "IOStoreClusterIDs " << IOStoreClusterIDs << "\n";
	cout << "IOStoreClusters " << IOStoreClusters << "\n";

    //cout << "InputfilePSE read from " << InputfilePSE << "\n";
    cout << "InputReconstruction read from " << InputfileReconstruction << "\n";
    cout << "InputHullsAndDistances read from " << InputfileHullAndDistances << "\n";

    //cout << "MaxSizeCachedResPerNode " << MaxSizeCachedResPerNode << "\n";

	cout << "\n";
	return true;
}


void ConfigDBScan::reportSettings( vector<pparm> & res )
{
	//res.push_back( pparm( "InputfilePSE", InputfilePSE, "", "Periodic table of elements to map human-readable element labels into numeric keys" ) );
	res.push_back( pparm( "InputfileReconstruction", InputfileReconstruction, "", "HDF5 file with x,y,z ion positions (reconstructed) and iontype labels" ) );
	res.push_back( pparm( "InputfileHullAndDistances", InputfileHullAndDistances, "", "HDF5 file with distance of each ion to a triangulated dataset edge" ) );
	res.push_back( pparm( "ClusteringMethod", sizet2str(static_cast<size_t>(ClusteringMethod)), "", "Clustering method used" ) );

	res.push_back( pparm( "DBScanEpsilonMin", real2str(Epsilon.min), "nm", "Lower bound of the linear interval of epsilon/dmax values" ) );
	res.push_back( pparm( "DBScanEpsilonIncr", real2str(Epsilon.incr), "nm", "Increment of epsilon/dmax values" ) );
	res.push_back( pparm( "DBScanEpsilonMax", real2str(Epsilon.max), "nm", "Upper bound of the linear interval of epsilon/dmax values" ) );
	
	res.push_back( pparm( "DBScanMinPtsMin", uint2str(MinPtsKth.min), "", "Lower bound of the linear interval of kth nearest neighbor to define a core point (for DBScan)" ) );
	res.push_back( pparm( "DBScanMinPtsIncr", uint2str(MinPtsKth.incr), "", "Increment of kth nearest neighbor defining core points" ) );
	res.push_back( pparm( "DBScanMinPtsMax", uint2str(MinPtsKth.max), "", "Upper bound of the linear interval of kth nearest neighbor defining a core point" ) );
	
	if ( ClusteringMethod == MAXSEP ) {

		res.push_back( pparm( "MaxSepNumberOfIonsMin", uint2str(MaxSepMinPts.min), "", "Lower bound of number of ions at which to consider the cluster a significant" ) );
		res.push_back( pparm( "MaxSepNumberOfIonsIncr", uint2str(MaxSepMinPts.incr), "", "Increment of number of ions at which to consider the cluster a significant" ) );
		res.push_back( pparm( "MaxSepNumberOfIonsMax", uint2str(MaxSepMinPts.max), "", "Upper bound of number of ions at which to consider the cluster a significant" ) );

		/*
		res.push_back( pparm( "MaxSepErosionDistanceMin", real2str(MaxSepEroDist.min), "nm", "not implemented" ) );
		res.push_back( pparm( "MaxSepErosionDistanceIncr", real2str(MaxSepEroDist.incr), "nm", "not implemented" ) );
		res.push_back( pparm( "MaxSepErosionDistanceMax", real2str(MaxSepEroDist.max), "nm", "not implemented" ) );
		*/
	}
	res.push_back( pparm( "DatasetEdgeThresholdDistance", real2str(DatasetEdgeThresholdDistance), "nm", "Threshold distance below which ions are considered as being too close to the dataset edge and thus a cluster is likely truncated" ) );
	
	res.push_back( pparm( "IOStoreClusterIDs", sizet2str(static_cast<size_t>(IOStoreClusterIDs)), "", "" ) );
	res.push_back( pparm( "IOStoreClusters", sizet2str(static_cast<size_t>(IOStoreClusters)), "", "" ) );

	res.push_back( pparm( "MaxSizeCachedResPerNode", sizet2str(MaxSizeCachedResPerNode), "", "" ) );
}


