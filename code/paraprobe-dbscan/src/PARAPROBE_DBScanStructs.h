//##MK::GPLV3

#ifndef __PARAPROBE_DBSCAN_STRUCTS_H__
#define __PARAPROBE_DBSCAN_STRUCTS_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_DBScanXDMF.h"

//##MK::moves this to the ranger in utilities
#define UNKNOWN_IONTYPE		0

struct ityp
{
	unsigned char m_org;	//the physically-based assigned label
	unsigned char m_rnd;	//the pseudo-randomized uncorrelated label
	ityp() : m_org(UNKNOWN_IONTYPE), m_rnd(UNKNOWN_IONTYPE) {}
	ityp( const unsigned char _morg, const unsigned char _mrnd ) : m_org(_morg), m_rnd(_mrnd) {}
};

ostream& operator << (ostream& in, ityp const & val);

/*
struct ival
{
	size_t incl_left;
	size_t excl_right;
	ival() : incl_left(0), excl_right(0) {}
	ival( const size_t _left, const size_t _right ) :
		incl_left(_left), excl_right(_right) {}
};

ostream& operator << (ostream& in, ival const & val);
*/


#endif
