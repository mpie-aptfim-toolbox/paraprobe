cmake_minimum_required(VERSION 3.10)

################################################################################################################################
##DEVELOPER SECTION#############################################################################################################
##in this section software developers need to make changes when debugging#######################################################
################################################################################################################################ 
#please name your project accordingly
set(MYPROJECTNAME "paraprobe_dbscan")

#pull general information about external libraries and paths related to paraprobe
message([STATUS] "MYGITSHA: ${MYGITSHA}")
string(REGEX REPLACE "-" "_" MYCLEANGITSHA ${MYGITSHA}) #'-' is an invalid character in a C preprocessor macro
message([STATUS] "MYCLEANGITSHA: ${MYCLEANGITSHA}")
#add_definitions( -DGITSHA=${MYGITSHA} )
add_definitions( -DGITSHA=${MYCLEANGITSHA} )


include("../PARAPROBE.ExternalLibraries.cmake")
if(USE_MPI30)
	message([STATUS] "We are working on MAWS30")
endif()
if(USE_TALOS)
	message([STATUS] "We are compiling on TALOS")
endif()
message([STATUS] "MYPROJECTPATH: ${MYPROJECTPATH}")
message([STATUS] "MYUTILSPATH: ${MYUTILSPATH}")
message([STATUS] "MYHDF5PATH: ${MYHDFPATH}")

#compose a tool-specific path
set(MYTOOLPATH "${MYPROJECTPATH}/paraprobe-dbscan/")
message([STATUS] "MYTOOLPATH: ${MYTOOLPATH}")


#identify which compiler to use
message([STATUS] "MYCCC_COMPILER: __${CMAKE_C_COMPILER}__")
message([STATUS] "MYCXX_COMPILER: __${CMAKE_CXX_COMPILER}__")
if(${CMAKE_C_COMPILER} STREQUAL "icc" AND ${CMAKE_CXX_COMPILER} STREQUAL "icpc")
	message([STATUS] "We compile with Intel")
	set(EMPLOY_ITLCOMPILER ON)
endif()
if(${CMAKE_C_COMPILER} STREQUAL "gcc" AND ${CMAKE_CXX_COMPILER} STREQUAL "g++")
	message([STATUS] "We compile with GNU")
	set(EMPLOY_GNUCOMPILER ON)
endif()
if(${CMAKE_C_COMPILER} EQUAL "pgcc" AND ${CMAKE_CXX_COMPILER} EQUAL "pg++")
	message([STATUS] "We compile with PGI/LLVM")
	set(EMPLOY_PGICOMPILER ON)
endif()
#define which library dependencies exist
set(EMPLOY_MYHDF5 ON)

#define which parallelization layers are used
set(EMPLOY_PARALLELISM_MPI ON)
set(EMPLOY_PARALLELISM_OMP ON)
set(EMPLOY_PARALLELISM_OPENACC OFF)

#choose optimization level
##-O0 nothing, debug purposes, -O1 moderate optimization, -O2 -O3 for production level up to aggressive architecture specific non-portable optimization
if(EMPLOY_ITLCOMPILER)
	set(MYOPTLEVEL "-O3 -march=skylake -g") #"-O0")
endif()
if(EMPLOY_GNUCOMPILER)
	set(MYOPTLEVEL "-O3 -march=native")
endif()
if(EMPLOY_PGICOMPILER)
	set(MYOPTLEVEL "-O2 -pg -tp=haswell-64") # -O0 -acc -ta=tesla -Minfo=accel,mp")
endif()


################################################################################################################################
##END OF INTERACTION FOR NON PRO USERS##########################################################################################
##here advanced users might want/need to make modifications if they use non default places for thirdparty libraries#############
################################################################################################################################ 
#HDF5 local installation for advanced I/O, collecting metadata and bundle analysis results together
if(EMPLOY_MYHDF5)
	message([STATUS] "We use the HDF5 library for advanced I/O")
	include_directories("${MYHDFPATH}/include")
	link_directories("${MYHDFPATH}/lib")
	##link against static libraries
	set(MYHDFLINKFLAGS "-L${MYHDFPATH}/lib/ ${MYHDFPATH}/lib/libhdf5_hl.a ${MYHDFPATH}/lib/libhdf5.a ${MYHDFPATH}/lib/libz.a ${MYHDFPATH}/lib/libszip.a -ldl")
endif()
#############################################################################################################################
##AUTOMATIC SECTION##########################################################################################################
##normally no further changes should be required below unless for development################################################
#############################################################################################################################
#user input sanity checks

#automatically assign project name and compiler flags
project(${MYPROJECTNAME})
set(CMAKE_BUILD_DIR "build")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${MYOPTLEVEL}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${MYOPTLEVEL}")

#setting up compiler-specifics
if(EMPLOY_ITLCOMPILER AND EMPLOY_GNUCOMPILER AND EMPLOY_PGICOMPILER)
	message([FATAL_ERROR] "We have to use a compiler!")
endif()
if(EMPLOY_ITLCOMPILER)
	message([STATUS] "We compile with Intel")
	set(MYVERBOSE "-qopt-report=5") #-phase=vec") #https://software.intel.com/en-us/node/522949/ "-qopt-report-phase=vec")
	add_definitions("${MYOPTLEVEL} ${MYVERBOSE}") # ${MYMKLCOMP}")
	#omp
	#if(EMPLOY_PARALLELISM_OMP)
	set(MYOMP "-qopenmp -lpthread")
	add_definitions(${MYOMP})
	#elseif()
	#	set(MYOMP "")
	#endif()
	#c2011 support
	add_definitions("-std=c++0x")
	#further definitions
	add_definitions("-w3") #-Wall -Warray-bounds -Wchar-subscripts -Wcomment -Wenum-compare -Wformat -Wuninitialized -Wmaybe-uninitialized -Wmain 
			#-Wnarrowing -Wnonnull -Wparentheses -Wpointer-sign -Wreorder -Wreturn-type -Wsign-compare -Wsequence-point -Wtrigraphs 
			#-Wunused-function -Wunused-but-set-variable -Wunused-variable -Wwrite-strings -Wint-to-pointer-cast")
endif()
if(EMPLOY_GNUCOMPILER)
	message([STATUS] "We compile with GCC")
	set(MYVERBOSE "-fopt-info")
	add_definitions("${MYOPTLEVEL} ${MYVERBOSE}") #${MYSIMDFLAGS}")
	#omp
	#if(EMPLOY_PARALLELISM_OMP)
	set(MYOMP "-fopenmp -lpthread")
	add_definitions(${MYOMP})
	#elseif()
	#	set(MYOMP "")
	#endif()
	#c2011 support
	add_definitions("-std=c++11")
	#further definitions
	add_definitions("-Wall -Warray-bounds -Wchar-subscripts -Wcomment -Wenum-compare -Wformat 
		-Wuninitialized -Wmaybe-uninitialized -Wmain -Wnonnull -Wparentheses -Wreorder -Wreturn-type -Wsign-compare -Wsequence-point 
		-Wtrigraphs -Wunused-function -Wunused-but-set-variable -Wunused-variable") #-Wnarrowing
endif()
if(EMPLOY_PGICOMPILER)
	message([STATUS] "We compile with PGI")
	set(MYOMP "-mp -ldl")
	add_definitions("${MYOPTLEVEL} ${MYOMP}")
	set(MYOPENACCLINK "-acc -ta=nvidia") #host
	add_definitions("-std=c++11")
	#additional defs and large file support
endif()

message([STATUS] "Projectname is ${MYPROJECTNAME}")
message([STATUS] "We utilize optimization level ${MYOPTLEVEL}")

#parallelization - MPI process-level
#query location of MPI library
if(EMPLOY_PARALLELISM_MPI)
	find_package(MPI REQUIRED)
	include_directories(${MPI_INCLUDE_PATH})
endif()

#specific paths of dependencies for this tool
set(MYTOOLSRCPATH "${MYTOOLPATH}/src/")

#list firstly the precompiled shared aka utils, secondly the tool-specific components, lastly the tool-specific main
add_executable(${MYPROJECTNAME}
	${MYUTILSPATH}/PARAPROBE_Verbose.cpp.o
	${MYUTILSPATH}/PARAPROBE_Profiling.cpp.o
	${MYUTILSPATH}/PARAPROBE_EPOSEndianness.cpp.o
	${MYUTILSPATH}/PARAPROBE_Datatypes.cpp.o
	${MYUTILSPATH}/PARAPROBE_OriMath.cpp.o
	${MYUTILSPATH}/PARAPROBE_Math.cpp.o
	${MYUTILSPATH}/PARAPROBE_PeriodicTable.cpp.o
	${MYUTILSPATH}/PARAPROBE_Composition.cpp.o	
	${MYUTILSPATH}/PARAPROBE_Histogram.cpp.o
	${MYUTILSPATH}/PARAPROBE_SDM3D.cpp.o
	${MYUTILSPATH}/PARAPROBE_AABBTree.cpp.o
	${MYUTILSPATH}/PARAPROBE_SpaceBucketing.cpp.o
	${MYUTILSPATH}/PARAPROBE_KDTree.cpp.o
	${MYUTILSPATH}/CONFIG_Shared.cpp.o
	${MYUTILSPATH}/PARAPROBE_HDF5.cpp.o
	${MYUTILSPATH}/PARAPROBE_XDMF.cpp.o
	${MYUTILSPATH}/PARAPROBE_Threadmemory.cpp.o
	${MYUTILSPATH}/PARAPROBE_Decompositor.cpp.o
	${MYUTILSPATH}/PARAPROBE_VolumeSampler.cpp.o
	${MYUTILSPATH}/PARAPROBE_CPUGPUWorkloadStructs.cpp.o
	${MYUTILSPATH}/PARAPROBE_FFT.cpp.o
	
	${MYTOOLSRCPATH}/CONFIG_DBScan.cpp
	${MYTOOLSRCPATH}/PARAPROBE_DBScanCiteMe.cpp
	${MYTOOLSRCPATH}/PARAPROBE_DBScanHDF5.cpp
	${MYTOOLSRCPATH}/PARAPROBE_DBScanXDMF.cpp
	${MYTOOLSRCPATH}/PARAPROBE_DBScanStructs.cpp
	${MYTOOLSRCPATH}/PARAPROBE_DBScanTaskHdl.cpp
	${MYTOOLSRCPATH}/thirdparty/HPDBScan/PARAPROBE_HPDBScan.cpp
	#${MYTOOLSRCPATH}/PARAPROBE_DBScanAcquisor.cpp
	${MYTOOLSRCPATH}/PARAPROBE_DBScanHdl.cpp
	
	${MYTOOLSRCPATH}/PARAPROBE_DBScan.cpp
)

#linking process, the target link libraries command is specific for each tool of the toolbox
target_link_libraries(${MYPROJECTNAME} ${MYOMP} ${MPI_LIBRARIES} ${MYHDFLINKFLAGS} ) #${MYOPENACCLINK})

#MPI compilation settings
if(MPI_COMPILE_FLAGS)
	set_target_properties(${MYPROJECTNAME} PROPERTIES COMPILE_FLAGS "${MPI_COMPILE_FLAGS}")
endif()

if(MPI_LINK_FLAGS)
	set_target_properties(${MYPROJECTNAME} PROPERTIES LINK_FLAGS "${MPI_LINK_FLAGS}")
endif()
