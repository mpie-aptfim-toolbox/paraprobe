//##MK::GPLV3


#ifndef __PARAPROBE_RECONSTRUCT_CITEME_H__
#define __PARAPROBE_RECONSTRUCT_CITEME_H__

#include "CONFIG_Reconstruct.h"


class CiteMeReconstruct
{
public:
	
	static vector<bibtex> Citations;
	
	static size_t cite();
};

#endif

