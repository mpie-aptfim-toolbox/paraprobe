//##MK::GPLV3

#include "PARAPROBE_ReconstructHdl.h"


//define what is passed upon execution
#define SIMID									1
#define CONTROLFILE								2

//##MK::for querying system maximum physical memory see
//https://stackoverflow.com/questions/2513505/how-to-get-available-memory-c-g

#ifndef GITSHA									//otherwise we could not compile without adding -DGITSHA="d123" a dummy GitSha
	#define GITSHA								0
#endif
//stringification of strings passed from cmake at compile time
//https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html
#define xstr(s)									str(s)
#define str(s) 									#s


void hello( void )
{
	cout << "paraprobe-reconstruct" << "\n";
	cout << xstr(GITSHA) << "\n";
	cout << "Collecting " << CiteMeReconstruct::cite() << " publications for the tool to cite:" << "\n";
	for( auto it = CiteMeReconstruct::Citations.begin(); it != CiteMeReconstruct::Citations.end(); it++ ) {
		cout << it->author << "\n";
		cout << it->title << "\n";
		cout << it->journal << ", " << it->year << ", " << it->vol << ", pp" << it->pages << "\n";
		cout << it->doi << "\n";
	}
}



bool init(  int pargc, char** pargv )
{
	ConfigShared::SimID = stoul( pargv[SIMID] );
	try {
		ConfigReconstruct::readXML(pargv[CONTROLFILE]);
	}
	catch (exception& e) {
		cerr << "\n" << "Unable to parse control file! Details:" << "\n" << e.what() << "\n"; return false;
	}
	if ( ConfigReconstruct::checkUserInput() == false ) {
		cerr << "\n" << "Control file settings failed the validity check!" << "\n"; return false;
	}
	else {
		cout << "\n" << "\t\tInput is valid under SimulationID = " << "SimID." <<  ConfigShared::SimID << "\n";
	}
	cout << endl;
	return true;
}


void generate_detector_hitmaps( const int r, const int nr, char** pargv )
{
	//allocate process-level instance of a detspcHdl. The instance handles all process-internal
	//processing, the instances synchronize across and communicate with each other during execution
	detspcHdl* dsp = NULL;

	double ttic = MPI_Wtime();

	int localhealth = 1;
	int globalhealth = nr;
	try {
		dsp = new detspcHdl;
		dsp->set_myrank(r);
		dsp->set_nranks(nr);
		dsp->init_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Unable to allocate an detspcHdl class object instance" << "\n"; localhealth = 0;
	}

	/*
	if ( dsp->read_periodictable() == true ) {
		cout << "Rank " << r << " reads PSE successfully rng.nuclides.isotopes.size() " << sdm->rng.nuclides.isotopes.size() << "\n";
	}
	else {
		cerr << "Rank " << r << " loading of PeriodicTableOfElements failed!" << "\n"; localhealth = 0;
	}
	string xmlfn = pargv[CONTROLFILE];
	if ( sdm->read_iontype_combinations( xmlfn ) == true ) {
		cout << "Rank " << r << " reads IontypeCombinations successfully " << "\n";
	}
	else {
		cerr << "Rank " << r << " loading of IontypeCombinations failed!" << "\n"; localhealth = 0;
	}
	*/

	//do we have all processes on board?
	//single MPI process right now
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete dsp; dsp = NULL; return;
	}

	double ttoc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	dsp->detspc_tictoc.prof_elpsdtime_and_mem( "ReadXMLParameter", APT_XX, APT_IS_SEQ, mm, ttic, ttoc );
	ttic = MPI_Wtime();

	//we have all on board, read reconstruction and triangle hull
	//MK::we could have all processes loading from the same file, or have only MASTER do so and broadcast, we opt for the latter strategy
	//##MK::should not be a problem because initialization is orders of magnitude cheaper than actual calculations...
	if ( dsp->get_myrank() == MASTER ) {

		if ( dsp->read_detectorxy_ranging_from_apth5() == true ) {
			cout << "MASTER read successfully all detector hit coordinates and ranging information" << "\n";
		}
		else {
			cerr << "MASTER was unable to read detector hit coordinates and ranging information!" << "\n"; localhealth = 0;
		}
	}

	//necessary? second order issue wrt to performance for as few as 80 processes like on TALOS...?
	MPI_Barrier( MPI_COMM_WORLD );

	//##MK::see paraprobe-araullo AraulloHdl for intermediate communication

	//now processes can proceed and process massively parallel and independently

	dsp->define_detector_binning();

	dsp->execute_local_workpackage();

	cout << "Rank " << r << " has completed its workpackage" << "\n";

	MPI_Barrier(MPI_COMM_WORLD);

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that MASTER was not successful in writing material points to the APTH5 file" << "\n";
		delete dsp; dsp = NULL; return;
	}

	ttic = MPI_Wtime();

	if ( dsp->init_target_file() == true ) {
		cout << "Rank " << r << " successfully initialized APTH5 results file" << "\n";
	}
	else {
		cerr << "Rank " << r << " unable to initialize results APTH5 file" << "\n"; localhealth = 0;
	}
	if ( r == MASTER ) {
		if ( dsp->write_environment_and_settings() == true ) {
			cout << "Rank " << MASTER << " environment and settings written!" << "\n";
		}
		else {
			cerr << "Rank " << MASTER << " writing environment and settings failed!" << "\n"; localhealth = 0;
		}

		//##MK::check paraprobe-araullo AraulloHdl possibly more complicated communication pattern here if nrank > 1

		if ( dsp->write_hitmaps_to_apth5() == true ) { //does not require MPI communication all results already on the master
			cout << "Rank " << MASTER << " successfully wrote all hitmaps to an APTH5 file" << "\n";
		}
		else {
			cerr << "Rank " << MASTER << " unable to write all hitmaps to an APTH5 file" << "\n"; localhealth = 0;
		}
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that MASTER was not successful in writing material points to the APTH5 file" << "\n";
		delete dsp; dsp = NULL; return;
	}

	ttoc = MPI_Wtime();
	mm = dsp->detspc_tictoc.get_memoryconsumption();
	dsp->detspc_tictoc.prof_elpsdtime_and_mem( "WritingH5ResultsFile", APT_XX, APT_IS_SEQ, mm, ttic, ttoc);

	//##MK::check paraprobe-araullo AraulloHdl for more complicated communication pattern following here

	dsp->detspc_tictoc.spit_profiling( "Reconstruct", ConfigShared::SimID, dsp->get_myrank() );

	//release resources
	delete dsp; dsp = NULL;
}


void generate_reconstructions( const int r, const int nr, char** pargv )
{
	//allocate process-level instance of a detspcHdl. The instance handles all process-internal
	//processing, the instances synchronize across and communicate with each other during execution
	reconHdl* rec = NULL;

	double ttic = MPI_Wtime();

	int localhealth = 1;
	int globalhealth = nr;
	try {
		rec = new reconHdl;
		rec->set_myrank(r);
		rec->set_nranks(nr);
		rec->init_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Unable to allocate an reconHdl class object instance" << "\n"; localhealth = 0;
	}

	/*
	if ( rec->read_periodictable() == true ) {
		cout << "Rank " << r << " reads PSE successfully rng.nuclides.isotopes.size() " << sdm->rng.nuclides.isotopes.size() << "\n";
	}
	else {
		cerr << "Rank " << r << " loading of PeriodicTableOfElements failed!" << "\n"; localhealth = 0;
	}
	string xmlfn = pargv[CONTROLFILE];
	if ( sdm->read_iontype_combinations( xmlfn ) == true ) {
		cout << "Rank " << r << " reads IontypeCombinations successfully " << "\n";
	}
	else {
		cerr << "Rank " << r << " loading of IontypeCombinations failed!" << "\n"; localhealth = 0;
	}
	*/

	//do we have all processes on board?

	//single MPI process right now
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete rec; rec = NULL; return;
	}

	double ttoc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	rec->reconstruct_tictoc.prof_elpsdtime_and_mem( "ReadXMLParameter", APT_XX, APT_IS_SEQ, mm, ttic, ttoc );
	ttic = MPI_Wtime();

	//we have all on board, read reconstruction and triangle hull
	//MK::we could have all processes loading from the same file, or have only MASTER do so and broadcast, we opt for the latter strategy
	//##MK::should not be a problem because initialization is orders of magnitude cheaper than actual calculations...
	if ( rec->get_myrank() == MASTER ) {

		if ( rec->read_detectorxy_ranging_from_apth5() == true ) {
			cout << "MASTER read successfully all detector hit coordinates and ranging information" << "\n";
		}
		else {
			cerr << "MASTER was unable to read detector hit coordinates and ranging information!" << "\n"; localhealth = 0;
		}

		if ( rec->read_voltagecurve_from_apth5() == true ) {
			cout << "MASTER read successfully Vdc and Vpu data" << "\n";
		}
		else {
			cerr << "MASTER was unable to read Vdc and Vpu data!" << "\n"; localhealth = 0;
		}
	}

	//necessary? second order issue wrt to performance for as few as 80 processes like on TALOS...?
	MPI_Barrier( MPI_COMM_WORLD );
	
	//single MPI process right now
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete rec; rec = NULL; return;
	}

	//##MK::see paraprobe-araullo AraulloHdl for intermediate communication

	//now processes can proceed and process massively parallel and independently

	rec->define_reconstructions();

	rec->execute_local_workpackage();

	cout << "Rank " << r << " has completed its workpackage" << "\n";

	/*
	MPI_Barrier(MPI_COMM_WORLD);
	*/

	ttic = MPI_Wtime();

	if ( rec->init_target_file() == true ) {
		cout << "Rank " << r << " successfully initialized APTH5 results file" << "\n";
	}
	else {
		cerr << "Rank " << r << " unable to initialize results APTH5 file" << "\n"; localhealth = 0;
	}
	if ( r == MASTER ) {
		if ( rec->write_environment_and_settings() == true ) {
			cout << "Rank " << MASTER << " environment and settings written!" << "\n";
		}
		else {
			cerr << "Rank " << MASTER << " writing environment and settings failed!" << "\n"; localhealth = 0;
		}

		//##MK::check paraprobe-araullo AraulloHdl possibly more complicated communication pattern here if nrank > 1

		if ( rec->write_reconxyz_to_apth5() == true ) { //does not require MPI communication all results already on the master
			cout << "Rank " << MASTER << " successfully wrote all reconstructions to an APTH5 file" << "\n";
		}
		else {
			cerr << "Rank " << MASTER << " unable to write all reconstructions to an APTH5 file" << "\n"; localhealth = 0;
		}
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that MASTER was not successful in writing material points to the APTH5 file" << "\n";
		delete rec; rec = NULL; return;
	}

	ttoc = MPI_Wtime();
	mm = rec->reconstruct_tictoc.get_memoryconsumption();
	rec->reconstruct_tictoc.prof_elpsdtime_and_mem( "WritingH5ResultsFile", APT_XX, APT_IS_SEQ, mm, ttic, ttoc);

	//##MK::check paraprobe-araullo AraulloHdl for more complicated communication pattern following here

	rec->reconstruct_tictoc.spit_profiling( "Reconstruct", ConfigShared::SimID, rec->get_myrank() );

	//release resources
	delete rec; rec = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();

	hello();
//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	//helloworld( argc, argv );

	if ( init( argc, argv ) == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks!" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else { 
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}
	cout << r << "-th MPI process initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";
	
//EXECUTE SPECIFIC TASK
	if ( ConfigReconstruct::ReferenceSpace == DETECTOR_SPACE ) {
		generate_detector_hitmaps( r, nr, argv );
	}
	
	if ( ConfigReconstruct::ReferenceSpace == RECONSTRUCTION_SPACE ) {
		generate_reconstructions( r, nr, argv );
	}
	
//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	
	double toc = omp_get_wtime();
	cout << "paraprobe-reconstruct took " << (toc-tic) << " seconds wall-clock time in total" << endl;

	return 0;
}
