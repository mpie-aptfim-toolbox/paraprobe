//##MK::GPLV3

#include "PARAPROBE_ReconstructHDF5.h"
//https://www.learncpp.com/cpp-tutorial/114-constructors-and-initialization-of-derived-classes/


reconstruct_h5::reconstruct_h5()
{
}


reconstruct_h5::~reconstruct_h5()
{
}


int reconstruct_h5::create_reconstruct_apth5( const string h5fn )
{
	h5resultsfn = h5fn;
	fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );
	if ( fileid < 0 ) {
		cerr << "Create reconstruct apth5 file creation failed! " << fileid << "\n"; return WRAPPED_HDF5_FAILED;
	}

	//domain specific HDF5 keywords and data fields
	if ( ConfigReconstruct::ReferenceSpace == DETECTOR_SPACE ) {
		//########################################
		/*
		groupid = H5Gcreate2(fileid, PARAPROBE_ARAULLO, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_ARAULLO failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_ARAULLO failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}

		groupid = H5Gcreate2(fileid, PARAPROBE_ARAULLO_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_ARAULLO_META failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_ARAULLO_META failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}
		groupid = H5Gcreate2(fileid, PARAPROBE_ARAULLO_META_MP, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_ARAULLO_META_MP failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_ARAULLO_META_MP failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}

		groupid = H5Gcreate2(fileid, PARAPROBE_ARAULLO_META_PHCAND, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_ARAULLO_META_PHCAND failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_ARAULLO_META_PHCAND failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}

		groupid = H5Gcreate2(fileid, PARAPROBE_ARAULLO_META_DIR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_ARAULLO_META_DIR failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_ARAULLO_META_DIR failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}

		groupid = H5Gcreate2(fileid, PARAPROBE_ARAULLO_RES, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_ARAULLO_RES failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_ARAULLO_RES failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}
		*/
	}
	
	if ( ConfigReconstruct::ReferenceSpace == RECONSTRUCTION_SPACE ) {
		//######################################
		cerr << "Not yet implemented!" << "\n"; return WRAPPED_HDF5_FAILED;
	}

	//add environment and tool specific settings
	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_HRDWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_HRDWR failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_HRDWR failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_HRDWR_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_HRDWR_META failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_HRDWR_META failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_HRDWR_META_KEYS, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_HRDWR_META_KEYS failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_HRDWR_META_KEYS failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_SFTWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_SFTWR failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARARPROBE_UTILS_SFTWR failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_SFTWR_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_SFTWR_META failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_SFTWR_META failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_SFTWR_META_KEYS, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_SFTWR_META_KEYS failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_SFTWR_META_KEYS failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	//close file
	status = H5Fclose(fileid);
	if ( status < 0 ) {
		cerr << "Close file " << h5resultsfn << " failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	return WRAPPED_HDF5_SUCCESS;
}
