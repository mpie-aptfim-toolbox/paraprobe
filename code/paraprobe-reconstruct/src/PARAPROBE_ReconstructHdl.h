//##MK::GPLV3

#ifndef __PARAPROBE_ARAULLO_HDL_H__
#define __PARAPROBE_ARAULLO_HDL_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_ReconstructBasEtAl.h"


class detspcHdl
{
	//process-level class which implements the worker instance which executes spatial distribution maps according to the method
	//of Vicente Araullo-Peters et al. using OpenMP and OpenACC i.e. CPU and GPUs simultaneously
	//specimens result are written to a specifically-formatted HDF5 file

public:
	detspcHdl();
	~detspcHdl();

	//bool read_periodictable();
	//bool read_iontype_combinations( string fn );

	bool read_detectorxy_ranging_from_apth5();
	//bool broadcast_detectorxy();

	//bool define_phase_candidates();
	//void spatial_decomposition();
	//void itype_sensitive_spatial_decomposition();
	//void define_matpoint_volume_grid();
	//void distribute_matpoints_on_processes_roundrobin();
	void define_detector_binning();
	void execute_local_workpackage(); 	//multi-threaded CPU on the non-master + 1x GPU

	bool init_target_file();
	bool write_environment_and_settings();
	bool write_hitmaps_to_apth5();

	int get_myrank();
	int get_nranks();
	void set_myrank( const int rr );
	void set_nranks( const int nn );
	void init_mpidatatypes();

	//rangeTable rng;
	//itypeCombiHdl itsk;
	//vector<PhaseCandidate> cands;
	//unsigned char maximum_iontype_uc;

	h5Hdl inputAPTH5Hdl;
	reconstruct_h5 debugh5Hdl;
	reconstruct_xdmf debugxdmf;
	
	vector<p2dm1> detxy;

	vector<hitmap> hitmaps;

	profiler detspc_tictoc;

private:
	//MPI related
	int myrank;								//my MPI ID in the MPI_COMM_WORLD
	int nranks;

	//MPI_Datatypes
	/*
	MPI_Datatype MPI_Ranger_DictKeyVal_Type;
	MPI_Datatype MPI_Synth_XYZ_Type;
	MPI_Datatype MPI_Ranger_Iontype_Type;
	MPI_Datatype MPI_Ranger_MQIval_Type;
	*/
};



class reconHdl
{
public:
	reconHdl();
	~reconHdl();

	//bool read_periodictable();
	//bool read_iontype_combinations( string fn );

	bool read_detectorxy_ranging_from_apth5();
	bool read_voltagecurve_from_apth5();

	bool broadcast_detectorxy();
	bool broadcast_ranging();
	bool broadcast_voltages();
	
	//bool define_phase_candidates();

	//void spatial_decomposition();
	//void itype_sensitive_spatial_decomposition();
	//void define_matpoint_volume_grid();
	//void distribute_matpoints_on_processes_roundrobin();
	void define_reconstructions();
	void distribute_reconstructions_on_processes_roundrobin();

	void execute_local_workpackage();

	bool init_target_file();
	bool write_environment_and_settings();
	bool write_reconxyz_to_apth5();

	int get_myrank();
	int get_nranks();
	void set_myrank( const int rr );
	void set_nranks( const int nn );
	void init_mpidatatypes();

	//rangeTable rng;
	//itypeCombiHdl itsk;
	//vector<PhaseCandidate> cands;
	//unsigned char maximum_iontype_uc;

	//decompositor sp;
	//volsampler mp;
	vector<int> recon2rk;								//mapping of material points => to ranks
	vector<int> recon2me;
	
	h5Hdl inputAPTH5Hdl;
	reconstruct_h5 debugh5Hdl;
	reconstruct_xdmf debugxdmf;

	vector<p2dm1> detxy;								//detector hit data
	vector<voltm1> voltage;								//interleaved Vdc
	//vector<rhit> detxy_vdcpu;

	vector<recon> reconstructions;
	
	profiler reconstruct_tictoc;

private:
	//MPI related
	int myrank;								//my MPI ID in the MPI_COMM_WORLD
	int nranks;

	//MPI_Datatypes
	//MPI_Datatype MPI_Ranger_DictKeyVal_Type;
	//MPI_Datatype MPI_Synth_XYZ_Type;
	//MPI_Datatype MPI_Ranger_Iontype_Type;
	//MPI_Datatype MPI_Ranger_MQIval_Type;
};


#endif

