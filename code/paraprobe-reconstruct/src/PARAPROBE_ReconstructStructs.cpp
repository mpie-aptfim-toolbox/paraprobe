//##MK::GPLV3

#include "PARAPROBE_ReconstructStructs.h"

unsigned int sqb2d::where( p2dm1 const & p )
{
	//2d implicit x+y*nx
	if ( 	p.x >= this->rectangle.xmi &&
			p.x <= this->rectangle.xmx &&
			p.y >= this->rectangle.ymi &&
			p.y <= this->rectangle.ymx  ) {
		apt_real ix = floor((p.x - this->rectangle.xmi) / (this->rectangle.xmx - this->rectangle.xmi) * static_cast<apt_real>(this->nx));
		apt_real iy = floor((p.y - this->rectangle.ymi) / (this->rectangle.ymx - this->rectangle.ymi) * static_cast<apt_real>(this->ny));
		unsigned int bx = static_cast<unsigned int>(ix);
		unsigned int by = static_cast<unsigned int>(iy);
		if ( bx < this->nx && by < this->ny ) {
			return bx + this->nx * by;
		}
		else {
			return UINT32MX;
		}
	}
	return UINT32MX;
}


p2d sqb2d::where( const unsigned int bxy )
{
	//2d implicit x+y*nx
	unsigned int by = bxy / this->nx;
	unsigned int bx = bxy - by*this->nx;
	if ( bx < this->nx && by < this->ny ) {
		apt_real px = this->rectangle.xmi + this->width*(0.5 + static_cast<apt_real>(bx));
		apt_real py = this->rectangle.ymi + this->width*(0.5 + static_cast<apt_real>(by));
		return p2d( px, py );
	}
	return p2d(F32MX, F32MX);
}


ostream& operator<<(ostream& in, sqb2d const & val)
{
	in << "BinningNXY/width = " << val.nx << ";" << val.ny << ";" << "\t\t" << val.nxy << "\t\t" << val.width << "\n";
	in << "BinningBoundingRectangle = " << val.rectangle << "\n";
	return in;
}


ostream& operator<<(ostream& in, ion_sequence const & val)
{
	in << "Ionsequence [ " << val.first << " : " << val.skip << " : " <<  val.last << " )" << "\n";
	return in;
}


hitmap::hitmap()
{
	mcp_grid = sqb2d();
	ion_seq = ion_sequence();
}


hitmap::hitmap( ion_sequence const & isq, sqb2d const & mcp )
{
	mcp_grid = mcp;
	ion_seq = isq;
}


hitmap::~hitmap()
{
}


ostream& operator<<(ostream& in, hitmap const & val)
{
	in << "Hitmap" << "\n";
	in << "ion_sequence " << val.ion_seq << "\n";
	in << "mcp_grid " << val.mcp_grid << "\n";
	unsigned int sum = 0;
	for( auto it = val.cnts.begin(); it != val.cnts.end(); it++ ) {
		sum += *it;
	}
	in << "Sum(cnts) " << sum << " ions" << "\n";
	return in;
}


ostream& operator<<(ostream& in, bas_protocol_parms const & val)
{
	in << "FlightLength " << val.FlightLength << "\n";
	in << "AtomicDensity " << val.AtomicDensity << "\n";
	in << "EvaporationField " << val.EvaporationField << "\n";
	in << "DetectionEfficiency " << val.DetectionEfficiency << "\n";
	in << "FieldFactor " << val.FieldFactor << "\n";
	in << "ImageCompressionFactor " << val.ImageCompressionFactor << "\n";
	return in;
}


recon::recon()
{
	parms = bas_protocol_parms();
}


recon::recon( bas_protocol_parms const & in )
{
	parms = in;
}


recon::~recon()
{
}


ostream& operator<<(ostream& in, recon const & val)
{
	in << "Reconstruction according to Bas et al." << "\n";
	in << val.parms << "\n";
	in << "xyz.size() " << val.xyz.size() << "\n";
	return in;
}


void recon::apply_gault_breen_protocol( vector<p2dm1> const & dxy, vector<voltm1> const & vdcpu )
{
	//using Bas et al. protocol to reconstruct a proxy of the specimen from the detector x/y hits and voltage curve data
cout << "Starting Gault / Breen protocol..." << "\n";
cout << parms << "\n";

	double tic = MPI_Wtime();

	if ( dxy.size() != vdcpu.size() ) {
		cerr << "We have not for every ion detector x/y hit coordinates, Vdc and Vpu, unable to perform the reconstruction!" << "\n"; return;
	}

	size_t ni = dxy.size();

	//allocate temporary memory, ##MK::should also become leaner
	vector<apt_real> ang; //angle argument cart2sph
	vector<apt_real> thetaP; //compressed angle
	vector<apt_real> theta; //launch angle
	vector<apt_real> Vsum; //VDC+VPU
	vector<apt_real> rcurr; //current radius
	try {
		ang.reserve( ni );
		thetaP.reserve( ni );
		theta.reserve( ni );
		Vsum.reserve( ni );
		rcurr.reserve( ni );
	}
	catch (bad_alloc&reconexc) {
		cerr << "apply_gault_breen_protocol unable to allocate ang, thetaP, and theta array!" << "\n"; return;
	}

	//Cartesian detector coordinates into polar coordinates
	//in-place computation of effective detector area, prep for radius evolution from voltage curve (in nm)
	apt_real maxrad = 0.f;
	apt_real L = parms.FlightLength;
	apt_real kfFevap = parms.FieldFactor * parms.EvaporationField;
	for( size_t i = 0; i < ni; i++ ) {

		ang.push_back( atan2( dxy[i].y, dxy[i].x ) );

		//compute the compressed angle thetaP, rad is current radius
		apt_real rad = sqrt( SQR(dxy[i].x) + SQR(dxy[i].y) );
		thetaP.push_back( atan( rad / L ) );

		Vsum.push_back( vdcpu[i].Vdc + vdcpu[i].Vpu );
//cout << ang[e] << "\t\t" << rad << "\t\t" << thetaP[e] << "\t\t" << vdcpu[e] << endl;
		if ( rad < maxrad )
			continue;
		else
			maxrad = rad; //>=

		//current radius
		rcurr.push_back( Vsum[i] / kfFevap );
	}

	//radius evolution from voltage curve (in nm) in-place
	apt_real m = parms.ImageCompressionFactor - 1.f;
	for( size_t i = 0; i < ni; i++ ) {
		theta.push_back( thetaP[i] + asin(m * sin(thetaP[i])) );
	}

	apt_real Adet = SQR(maxrad) * MYPI; //this is the strategy used by A. Breen
	apt_real omegaFactor = (1.0 / parms.AtomicDensity) * SQR(parms.FlightLength) * SQR(parms.FieldFactor) * SQR(parms.EvaporationField) /
									(parms.DetectionEfficiency * Adet * SQR(parms.ImageCompressionFactor));

	apt_real cumsumdz = 0.f;
	for( size_t i = 0; i < ni; i++ ) {

		apt_real zP = rcurr[i] * (1.f - cos(theta[i]));
		apt_real d = rcurr[i] * sin(theta[i]);

		cumsumdz = cumsumdz + (omegaFactor / SQR(Vsum[i])); //##MK::cumsumdz + dz[e]; //##MK::dz[e] = omegaFactor / SQR(vdcpu[e]);

		apt_real zrecon = cumsumdz + zP;
		apt_real xrecon = d * cos(ang[i]);
		apt_real yrecon = d * sin(ang[i]);

		xyz.push_back( p3dm1( xrecon, yrecon, zrecon, i ) );
	}

	double toc = MPI_Wtime();
cout << "Completed reconstruction xyz.size() " << xyz.size() << " in " << (toc-tic) << " seconds" << "\n";
}
