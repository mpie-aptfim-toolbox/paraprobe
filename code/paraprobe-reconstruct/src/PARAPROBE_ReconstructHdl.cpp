//##MK::GPLV3

#include "PARAPROBE_ReconstructHdl.h"


detspcHdl::detspcHdl()
{
	myrank = MASTER;
	nranks = SINGLEPROCESS;
}


detspcHdl::~detspcHdl()
{
}


bool detspcHdl::read_detectorxy_ranging_from_apth5()
{
	double tic = MPI_Wtime();

	//load detector xy positions
	if ( inputAPTH5Hdl.read_detxy_from_apth5( ConfigReconstruct::InputfileTranscoder, detxy ) == true ) {
		cout << ConfigReconstruct::InputfileTranscoder << " read detector positions successfully" << "\n";
	}
	else {
		cerr << ConfigReconstruct::InputfileTranscoder << " read detector positions failed!" << "\n"; return false;
	}

	//load ranging information
	/*
	if ( inputReconH5Hdl.read_iontypes_from_apth5( ConfigAraullo::InputfileReconstruction, rng.iontypes ) == true ) {
		cout << ConfigAraullo::InputfileReconstruction << " read ranging information successfully" << "\n";
	}
	else {
		cerr << ConfigAraullo::InputfileReconstruction << " read ranging information failed!" << "\n"; return false;
	}

	//create iontype_dictionary
	for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
		size_t id = static_cast<size_t>(it->id);
		size_t hashval = rng.hash_molecular_ion( it->strct.Z1, it->strct.Z2, it->strct.Z3 );
cout << id << "\t\t" << hashval << "\t\t" << (int) it->strct.Z1 << ";" << (int) it->strct.Z2 << ";" << (int) it->strct.Z3 << "\n";
		if ( id != 0 && hashval != 0 ) { //skip adding existent UNKNOWN_IONTYPE
			auto jt = rng.iontypes_dict.find( hashval );
			if ( jt == rng.iontypes_dict.end() ) { //really a new iontype for which not yet mqival were defined
cout << "--->Adding new type " << id << "\t\t" << hashval << "\n";
				//add iontype in dictionary
				rng.iontypes_dict.insert( pair<size_t,size_t>( hashval, id ) );
			}
			else {
cerr << "--->Detected replicated iontype which should not happen as iontypes have to be mutually exclusive!" << "\n"; return false;
			}
		}
		else {
cout << "--->Me skipping the UNKNOWN_IONTYPE" << "\n";
		}
	}

	//load actual itypes per xyz position
	if ( inputReconH5Hdl.read_ityp_from_apth5( ConfigAraullo::InputfileReconstruction, ityp_org ) == true ) {
		cout << ConfigAraullo::InputfileReconstruction << " read itypes successfully" << "\n";
	}
	else {
		cerr << ConfigAraullo::InputfileReconstruction << " read itypes failed!" << "\n"; return false;
	}
	*/

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	detspc_tictoc.prof_elpsdtime_and_mem( "ReadDetectorXYFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


/*
bool detspcHdl::broadcast_detectorxy()
{
	return true;
}
*/


void detspcHdl::define_detector_binning()
{
	//compute maximum dimensions of the detector hit map data
	aabb2d rectangle = aabb2d();
	for( auto it = detxy.begin(); it != detxy.end(); it++ ) {
		rectangle.possibly_enlarge_me( *it );
	}
	rectangle.scale();
cout << "Maximum detector space position identified " << rectangle << "\n";

	//define all desired binnings
	size_t ions_per_map = static_cast<size_t>(0.5*(ConfigReconstruct::IonsPerHitMap.min + ConfigReconstruct::IonsPerHitMap.max));
cout << "Ions per hitmap " << ions_per_map << "\n";

	apt_real mcp_resolution = max( rectangle.xsz, rectangle.ysz ) / ConfigReconstruct::DetectorGridBinWidthX;
	size_t nmcp = ceil( mcp_resolution + 1.0 );
	sqb2d mastermcp = sqb2d( nmcp, nmcp, ConfigReconstruct::DetectorGridBinWidthX, rectangle );

	hitmaps = vector<hitmap>();
	for( size_t ions_processed = 0; ions_processed < detxy.size();      ) {
		ion_sequence iseq = ion_sequence();
		iseq.first = ions_processed;
		iseq.skip = 1;
		iseq.last = ((iseq.first + ions_per_map) < detxy.size()) ? iseq.first + ions_per_map : detxy.size() - iseq.first;

		hitmaps.push_back( hitmap( iseq, mastermcp ) );
cout << "ions_processed " << ions_processed << "\n" << hitmaps.back() << "\n";

		ions_processed = ions_processed + (iseq.last - iseq.first);
	}
}


void detspcHdl::execute_local_workpackage()
{
	for( auto it = hitmaps.begin(); it != hitmaps.end(); it++ ) {
		size_t ion_drain = 0;
		for( size_t j = it->ion_seq.first; j < it->ion_seq.last; j += it->ion_seq.skip ) { //O(n)
			p2dm1 hit = detxy.at(j);
			unsigned int here = it->mcp_grid.where( hit );
			if ( here != UINT32MX ) {
				it->cnts[here]++;
			}
			else {
				ion_drain++;
			}
		}

		//##MK::one could employ a delocalization here

		if ( ion_drain < 1 ) {
			cout << "Generated hitmap " << it - hitmaps.begin() << " ions drained " << ion_drain << "\n";
		}
		else {
			cerr << "Generated hitmap " << it - hitmaps.begin() << " ions drained " << ion_drain << "\n";
		}
	}
}


bool detspcHdl::init_target_file()
{
	return true;
}


bool detspcHdl::write_environment_and_settings()
{
	return true;
}
	

bool detspcHdl::write_hitmaps_to_apth5()
{
	return true;
}


int detspcHdl::get_myrank()
{
	return this->myrank;
}


int detspcHdl::get_nranks()
{
	return this->nranks;
}


void detspcHdl::set_myrank( const int rr )
{
	this->myrank = rr;
}


void detspcHdl::set_nranks( const int nn )
{
	this->nranks = nn;
}


void detspcHdl::init_mpidatatypes()
{
	/*
	MPI_Type_contiguous(2, MPI_UNSIGNED_LONG_LONG, &MPI_Ranger_DictKeyVal_Type);
	MPI_Type_commit(&MPI_Ranger_DictKeyVal_Type);

	int elementCounts[2] = {3, 1};
	MPI_Aint displacements[2] = {0, 3 * MPIIO_OFFSET_INCR_F32};
	MPI_Datatype oldTypes[2] = {MPI_FLOAT, MPI_UNSIGNED};
	MPI_Type_create_struct(2, elementCounts, displacements, oldTypes, &MPI_Ion_Type);
	MPI_Type_commit(&MPI_Ion_Type);
	*/
}


reconHdl::reconHdl()
{
	myrank = MASTER;
	nranks = SINGLEPROCESS;
}


reconHdl::~reconHdl()
{

}


bool reconHdl::read_detectorxy_ranging_from_apth5()
{
	double tic = MPI_Wtime();

	//load detector xy positions
	if ( inputAPTH5Hdl.read_detxy_from_apth5( ConfigReconstruct::InputfileTranscoder, detxy ) == true ) {
		cout << ConfigReconstruct::InputfileTranscoder << " read detector positions successfully" << "\n";
	}
	else {
		cerr << ConfigReconstruct::InputfileTranscoder << " read detector positions failed!" << "\n"; return false;
	}

	//load ranging information
	/*
	if ( inputReconH5Hdl.read_iontypes_from_apth5( ConfigAraullo::InputfileReconstruction, rng.iontypes ) == true ) {
		cout << ConfigAraullo::InputfileReconstruction << " read ranging information successfully" << "\n";
	}
	else {
		cerr << ConfigAraullo::InputfileReconstruction << " read ranging information failed!" << "\n"; return false;
	}

	//create iontype_dictionary
	for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
		size_t id = static_cast<size_t>(it->id);
		size_t hashval = rng.hash_molecular_ion( it->strct.Z1, it->strct.Z2, it->strct.Z3 );
cout << id << "\t\t" << hashval << "\t\t" << (int) it->strct.Z1 << ";" << (int) it->strct.Z2 << ";" << (int) it->strct.Z3 << "\n";
		if ( id != 0 && hashval != 0 ) { //skip adding existent UNKNOWN_IONTYPE
			auto jt = rng.iontypes_dict.find( hashval );
			if ( jt == rng.iontypes_dict.end() ) { //really a new iontype for which not yet mqival were defined
cout << "--->Adding new type " << id << "\t\t" << hashval << "\n";
				//add iontype in dictionary
				rng.iontypes_dict.insert( pair<size_t,size_t>( hashval, id ) );
			}
			else {
cerr << "--->Detected replicated iontype which should not happen as iontypes have to be mutually exclusive!" << "\n"; return false;
			}
		}
		else {
cout << "--->Me skipping the UNKNOWN_IONTYPE" << "\n";
		}
	}

	//load actual itypes per xyz position
	if ( inputReconH5Hdl.read_ityp_from_apth5( ConfigAraullo::InputfileReconstruction, ityp_org ) == true ) {
		cout << ConfigAraullo::InputfileReconstruction << " read itypes successfully" << "\n";
	}
	else {
		cerr << ConfigAraullo::InputfileReconstruction << " read itypes failed!" << "\n"; return false;
	}
	*/

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	reconstruct_tictoc.prof_elpsdtime_and_mem( "ReadDetectorXYFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool reconHdl::read_voltagecurve_from_apth5()
{
	double tic = MPI_Wtime();

	//load dc and pulse voltage data
	if ( inputAPTH5Hdl.read_voltages_from_apth5( ConfigReconstruct::InputfileTranscoder, voltage ) == true ) {
		cout << ConfigReconstruct::InputfileTranscoder << " read voltage curve data successfully" << "\n";
	}
	else {
		cerr << ConfigReconstruct::InputfileTranscoder << " read voltage curve data failed!" << "\n"; return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	reconstruct_tictoc.prof_elpsdtime_and_mem( "ReadDetectorXYFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool reconHdl::broadcast_detectorxy()
{
	return true;
}


bool reconHdl::broadcast_ranging()
{
	return true;
}


bool reconHdl::broadcast_voltages()
{
	return true;
}


void reconHdl::define_reconstructions()
{
	//currently only one reconstruction but looping of recon parameter is straightforward

	bas_protocol_parms settings = bas_protocol_parms(
										0.5*(ConfigReconstruct::FlightLength.min + ConfigReconstruct::FlightLength.max),
										0.5*(ConfigReconstruct::AtomicDensity.min + ConfigReconstruct::AtomicDensity.max),
										0.5*(ConfigReconstruct::EvaporationField.min + ConfigReconstruct::EvaporationField.max),
										0.5*(ConfigReconstruct::DetectionEfficiency.min + ConfigReconstruct::DetectionEfficiency.max),
										0.5*(ConfigReconstruct::FieldFactor.min + ConfigReconstruct::FieldFactor.max),
										0.5*(ConfigReconstruct::ImageCompressionFactor.min + ConfigReconstruct::ImageCompressionFactor.max)
																														);
cout << "Defining a reconstruction using Bas et al. protocol " << settings << "\n";

	reconstructions.push_back( recon(settings) );
}


void reconHdl::distribute_reconstructions_on_processes_roundrobin()
{
}


void reconHdl::execute_local_workpackage()
{
	double tic = MPI_Wtime();

	for( auto it = reconstructions.begin(); it != reconstructions.end(); it++ ) {

		it->apply_gault_breen_protocol( detxy, voltage );

	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	reconstruct_tictoc.prof_elpsdtime_and_mem( "BuildReconstructions", APT_XX, APT_IS_SEQ, mm, tic, toc);
}


bool reconHdl::init_target_file()
{
	return true;
}


bool reconHdl::write_environment_and_settings()
{
	return true;
}


bool reconHdl::write_reconxyz_to_apth5()
{
	return true;
}


int reconHdl::get_myrank()
{
	return this->myrank;
}


int reconHdl::get_nranks()
{
	return this->nranks;
}


void reconHdl::set_myrank( const int rr )
{
	this->myrank = rr;
}


void reconHdl::set_nranks( const int nn )
{
	this->nranks = nn;
}


void reconHdl::init_mpidatatypes()
{
	/*
	MPI_Type_contiguous(2, MPI_UNSIGNED_LONG_LONG, &MPI_Ranger_DictKeyVal_Type);
	MPI_Type_commit(&MPI_Ranger_DictKeyVal_Type);

	int elementCounts[2] = {3, 1};
	MPI_Aint displacements[2] = {0, 3 * MPIIO_OFFSET_INCR_F32};
	MPI_Datatype oldTypes[2] = {MPI_FLOAT, MPI_UNSIGNED};
	MPI_Type_create_struct(2, elementCounts, displacements, oldTypes, &MPI_Ion_Type);
	MPI_Type_commit(&MPI_Ion_Type);
	*/
}
