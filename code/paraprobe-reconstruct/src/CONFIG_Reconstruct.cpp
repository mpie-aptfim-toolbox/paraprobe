//##MK::GPLV3

#include "CONFIG_Reconstruct.h"

//string ConfigReconstruct::InputfilePSE = "";
string ConfigReconstruct::InputfileTranscoder = "";

WHICH_SPACE ConfigReconstruct::ReferenceSpace = RECONSTRUCTION_SPACE;
RECONSTRUCTION_METHOD ConfigReconstruct::ReconMethod = BAS_ETAL;	

lival ConfigReconstruct::FlightLength = lival();
lival ConfigReconstruct::AtomicDensity = lival();
lival ConfigReconstruct::EvaporationField = lival();
lival ConfigReconstruct::DetectionEfficiency = lival();
lival ConfigReconstruct::FieldFactor = lival();
lival ConfigReconstruct::ImageCompressionFactor = lival();
	
lival ConfigReconstruct::IonsPerHitMap = lival();
apt_real ConfigReconstruct::DetectorGridBinWidthX = 0.0;
apt_real ConfigReconstruct::DetectorGridBinWidthY = 0.0;


bool ConfigReconstruct::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.Reconstruct.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigReconstruct")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;

	//WHAT TYPE OF ANALYSIS TO DO
	mode = read_xml_attribute_uint32( rootNode, "WhichSpace" );
	switch (mode) {
		case DETECTOR_SPACE:
			ReferenceSpace = DETECTOR_SPACE; break;
		default:
			ReferenceSpace = RECONSTRUCTION_SPACE;
	}
	
	//InputfilePSE = read_xml_attribute_string( rootNode, "InputfilePeriodicTableOfElements" );
	InputfileTranscoder = read_xml_attribute_string( rootNode, "InputfileTranscoder" );
	
	/* ##MK::only one at the moment
	mode = read_xml_attribute_uint32( rootNode, "ReconstructionAlgorithm" );
	switch (mode) {
		default:
			ReconMethod = BAS_ETAL;
	}
	*/
	
	FlightLength = lival( 	read_xml_attribute_float( rootNode, "FlightLengthMin"  ),
							read_xml_attribute_float( rootNode, "FlightLengthIncr"  ),
							read_xml_attribute_float( rootNode, "FlightLengthMax"  )  );
	AtomicDensity = lival( 	read_xml_attribute_float( rootNode, "AtomicDensityMin"  ),
							read_xml_attribute_float( rootNode, "AtomicDensityIncr"  ),
							read_xml_attribute_float( rootNode, "AtomicDensityMax"  )  );
	EvaporationField = lival( 	read_xml_attribute_float( rootNode, "EvaporationFieldMin"  ),
							read_xml_attribute_float( rootNode, "EvaporationFieldIncr"  ),
							read_xml_attribute_float( rootNode, "EvaporationFieldMax"  )  );
	DetectionEfficiency = lival( 	read_xml_attribute_float( rootNode, "DetectionEfficiencyMin"  ),
							read_xml_attribute_float( rootNode, "DetectionEfficiencyIncr"  ),
							read_xml_attribute_float( rootNode, "DetectionEfficiencyMax"  )  );
	FieldFactor = lival( 	read_xml_attribute_float( rootNode, "FieldFactorMin"  ),
							read_xml_attribute_float( rootNode, "FieldFactorIncr"  ),
							read_xml_attribute_float( rootNode, "FieldFactorMax"  )  );
	ImageCompressionFactor = lival( 	read_xml_attribute_float( rootNode, "ImageCompressionFactorMin"  ),
							read_xml_attribute_float( rootNode, "ImageCompressionFactorIncr"  ),
							read_xml_attribute_float( rootNode, "ImageCompressionFactorMax"  )  );
	IonsPerHitMap = lival( 	read_xml_attribute_float( rootNode, "IonsPerHitMapMin"  ),
							read_xml_attribute_float( rootNode, "IonsPerHitMapIncr"  ),
							read_xml_attribute_float( rootNode, "IonsPerHitMapMax"  )  );
							
	DetectorGridBinWidthX = read_xml_attribute_float( rootNode, "DetectorGridBinWidthX" );
	DetectorGridBinWidthY = read_xml_attribute_float( rootNode, "DetectorGridBinWidthY" );

							
	return true;
}


bool ConfigReconstruct::checkUserInput()
{
	cout << "ConfigReconstruct::" << "\n";
	switch (ReferenceSpace)
	{
		case DETECTOR_SPACE:
			cout << "Working in detector space" << "\n"; break;
		default: //RECONSTRUCTION_SPACE:
			cout << "Working in reconstruction space" << "\n";
	}
	if ( ReferenceSpace == RECONSTRUCTION_SPACE ) {
		//##MK::add citations
		cout << "ReconstructionAlgorithm is modified Bas et  al." << "\n";
	}
	
	//cout << "InputfilePeriodicTableOfElements read from " << InputfilePSE << "\n";
	cout << "InputfileTranscoder read from " << InputfileTranscoder << "\n";
	if ( FlightLength.min < EPSILON || FlightLength.min > FlightLength.max ) {
		cerr << "FlightLengthMin needs to be positive, finite, and not larger than corresponding Max!" << "\n"; return false;
	}
	cout << "FlightLengthMin " << FlightLength.min << "\n";
	if ( FlightLength.incr < EPSILON ) {
		cerr << "FlightLengthIncr needs to be positive!" << "\n"; return false;
	}
	cout << "FlightLengthIncr " << FlightLength.incr << "\n";
	if ( FlightLength.max < EPSILON ) {
		cerr << "FlightLengthMax needs to be positive!" << "\n"; return false;
	}
	cout << "FlightLengthMax " << FlightLength.max << "\n";
	
	if ( AtomicDensity.min < EPSILON || AtomicDensity.min > AtomicDensity.max ) {
		cerr << "AtomicDensityMin needs to be positive, finite, and not larger than corresponding Max!" << "\n"; return false;
	}
	cout << "AtomicDensityMin " << AtomicDensity.min << "\n";
	if ( AtomicDensity.incr < EPSILON ) {
		cerr << "AtomicDensityIncr needs to be positive!" << "\n"; return false;
	}
	cout << "AtomicDensityIncr " << AtomicDensity.incr << "\n";
	if ( AtomicDensity.max < EPSILON ) {
		cerr << "AtomicDensityMax needs to be positive!" << "\n"; return false;
	}
	cout << "AtomicDensityMax " << AtomicDensity.max << "\n";

	if ( EvaporationField.min < EPSILON || EvaporationField.min > EvaporationField.max ) {
		cerr << "EvaporationFieldMin needs to be positive, finite, and not larger than corresponding Max!" << "\n"; return false;
	}
	cout << "EvaporationFieldMin " << EvaporationField.min << "\n";
	if ( EvaporationField.incr < EPSILON ) {
		cerr << "EvaporationFieldIncr needs to be positive!" << "\n"; return false;
	}
	cout << "EvaporationFieldIncr " << EvaporationField.incr << "\n";
	if ( EvaporationField.max < EPSILON ) {
		cerr << "EvaporationFieldMax needs to be positive!" << "\n"; return false;
	}
	cout << "EvaporationFieldMax " << EvaporationField.max << "\n";
	
	if ( DetectionEfficiency.min < EPSILON || DetectionEfficiency.min > DetectionEfficiency.max ) {
		cerr << "DetectionEfficiencyMin needs to be positive, finite, and not larger than corresponding Max!" << "\n"; return false;
	}
	cout << "DetectionEfficiencyMin " << DetectionEfficiency.min << "\n";
	if ( DetectionEfficiency.incr < EPSILON ) {
		cerr << "DetectionEfficiencyIncr needs to be positive!" << "\n"; return false;
	}
	cout << "DetectionEfficiencyIncr " << DetectionEfficiency.incr << "\n";
	if ( DetectionEfficiency.max < EPSILON ) {
		cerr << "DetectionEfficiencyMax needs to be positive!" << "\n"; return false;
	}
	cout << "DetectionEfficiencyMax " << DetectionEfficiency.max << "\n";

	if ( FieldFactor.min < EPSILON || FieldFactor.min > FieldFactor.max ) {
		cerr << "FieldFactorMin needs to be positive, finite, and not larger than corresponding Max!" << "\n"; return false;
	}
	cout << "FieldFactorMin " << FieldFactor.min << "\n";
	if ( FieldFactor.incr < EPSILON ) {
		cerr << "FieldFactorIncr needs to be positive!" << "\n"; return false;
	}
	cout << "FieldFactorIncr " << FieldFactor.incr << "\n";
	if ( FieldFactor.max < EPSILON ) {
		cerr << "FieldFactorMax needs to be positive!" << "\n"; return false;
	}
	cout << "FieldFactorMax " << FieldFactor.max << "\n";
	
	if ( ImageCompressionFactor.min < EPSILON || ImageCompressionFactor.min > ImageCompressionFactor.max ) {
		cerr << "ImageCompressionFactorMin needs to be positive, finite, and not larger than corresponding Max!" << "\n"; return false;
	}
	cout << "ImageCompressionFactorMin " << ImageCompressionFactor.min << "\n";
	if ( ImageCompressionFactor.incr < EPSILON ) {
		cerr << "ImageCompressionFactorIncr needs to be positive!" << "\n"; return false;
	}
	cout << "ImageCompressionFactorIncr " << ImageCompressionFactor.incr << "\n";
	if ( ImageCompressionFactor.max < EPSILON ) {
		cerr << "ImageCompressionFactorMax needs to be positive!" << "\n"; return false;
	}
	cout << "ImageCompressionFactorMax " << ImageCompressionFactor.max << "\n";

	if ( IonsPerHitMap.min < EPSILON || IonsPerHitMap.min > IonsPerHitMap.max ) {
		cerr << "IonsPerHitMapMin needs to be positive, finite, and not larger than corresponding Max!" << "\n"; return false;
	}
	cout << "IonsPerHitMapMin " << IonsPerHitMap.min << "\n";
	if ( IonsPerHitMap.incr < EPSILON ) {
		cerr << "IonsPerHitMapIncr needs to be positive!" << "\n"; return false;
	}
	cout << "IonsPerHitMapIncr " << IonsPerHitMap.incr << "\n";
	if ( IonsPerHitMap.max < EPSILON ) {
		cerr << "IonsPerHitMapMax needs to be positive!" << "\n"; return false;
	}
	cout << "IonsPerHitMapMax " << IonsPerHitMap.max << "\n";
	
	if ( DetectorGridBinWidthX < EPSILON || DetectorGridBinWidthY < EPSILON) {
		cerr << "DetectorGridBinWidthX and Y need both to be positive!" <<  "\n"; return false;
	}
	cout << "DetectorGridBinWidthX " << DetectorGridBinWidthX << "\n";
	cout << "DetectorGridBinWidthY " << DetectorGridBinWidthY << "\n";
	
	cout << "\n";
	return true;
}


void ConfigReconstruct::reportSettings( vector<pparm> & res )
{
	//res.push_back( pparm( "InputfilePSE", InputfilePSE, "", "" ) );
	res.push_back( pparm( "InputfileTranscoder", InputfileTranscoder, "", "were to read measured detector hit and time-of-flight data from" ) );
	
	res.push_back( pparm( "WhichSpace", uint2str(ReferenceSpace), "", "in which reference space to work" ) );
	res.push_back( pparm( "ReconstructionAlgorithm", uint2str(ReconMethod), "", "which reconstruction protocol" ) );

	res.push_back( pparm( "FlightLengthMin", real2str(FlightLength.min), "cm", "for flight length, minimum value of linear interval" ) );
	res.push_back( pparm( "FlightLengthIncr", real2str(FlightLength.incr), "cm", "for flight length, increment of linear interval" ) );
	res.push_back( pparm( "FlightLengthMax", real2str(FlightLength.max), "cm", "for flight length, maximum value of linear interval" ) );
	res.push_back( pparm( "AtomicDensityMin", real2str(AtomicDensity.min), "atoms/nm^3", "for assumption of a global atomic density, minimum value of linear interval" ) );
	res.push_back( pparm( "AtomicDensityIncr", real2str(AtomicDensity.incr), "atoms/nm^3", "for assumption of a global atomic density, increment of linear interval" ) );
	res.push_back( pparm( "AtomicDensityMax", real2str(AtomicDensity.max), "atoms/nm^3", "for assumption of a global atomic density, maximum value of linear interval" ) );
	res.push_back( pparm( "EvaporationFieldMin", real2str(EvaporationField.min), "V/nm", "for assumption of an evaporation field strength, minimum value of linear interval" ) );
	res.push_back( pparm( "EvaporationFieldIncr", real2str(EvaporationField.incr), "V/nm", "for assumption of an evaporation field strength, increment of linear interval" ) );
	res.push_back( pparm( "EvaporationFieldMax", real2str(EvaporationField.max), "V/nm", "for assumption of an evaporation field strength, maximum value of linear interval" ) );
	res.push_back( pparm( "DetectionEfficiencyMin", real2str(DetectionEfficiency.min), "1.0", "for assumption of a detection efficiency, minimum value of linear interval" ) );
	res.push_back( pparm( "DetectionEfficiencyIncr", real2str(DetectionEfficiency.incr), "1.0", "for assumption of a detection efficiency, increment of linear interval" ) );
	res.push_back( pparm( "DetectionEfficiencyMax", real2str(DetectionEfficiency.max), "1.0", "for assumption of a detection efficiency, maximum value of linear interval" ) );
	res.push_back( pparm( "FieldFactorMin", real2str(FieldFactor.min), "1.0", "empirical field factor, minimum value of linear interval" ) ); //has a unit?
	res.push_back( pparm( "FieldFactorIncr", real2str(FieldFactor.incr), "1.0", "empirical field factor, increment of linear interval" ) );
	res.push_back( pparm( "FieldFactorMax", real2str(FieldFactor.max), "1.0", "empirical field factor, maximum value of linear interval" ) );

	res.push_back( pparm( "ImageCompressionFactorMin", real2str(ImageCompressionFactor.min), "1.0", "image compression factor, minimum value of linear interval" ) );
	res.push_back( pparm( "ImageCompressionFactorIncr", real2str(ImageCompressionFactor.incr), "1.0", "image compression factor, increment of linear interval" ) );
	res.push_back( pparm( "ImageCompressionFactorMax", real2str(ImageCompressionFactor.max), "1.0", "image compression factor, maximum value of linear interval" ) );
	
	res.push_back( pparm( "IonsPerHitMapMin", real2str(IonsPerHitMap.min), "", "for detector hit map, minimum number of ions to take (of linear interval)" ) );
	res.push_back( pparm( "IonsPerHitMapIncr", real2str(IonsPerHitMap.incr), "", "for detector hit map, increment number of ions to take (of linear interval)" ) );
	res.push_back( pparm( "IonsPerHitMapMax", real2str(IonsPerHitMap.max), "", "for detector hit map, maximum number of ions to take (of linear interval)" ) );
	res.push_back( pparm( "DetectorGridBinWidthX", real2str(DetectorGridBinWidthX), "cm", "for gridding detector hit map, bin width along x" ) );
	res.push_back( pparm( "DetectorGridBinWidthY", real2str(DetectorGridBinWidthY), "cm", "for gridding detector hit map, bin width along y" ) );
}
