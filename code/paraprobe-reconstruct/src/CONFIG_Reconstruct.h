//##MK::GPLV3

#ifndef __PARAPROBE_CONFIG_RECONSTRUCT_H__
#define __PARAPROBE_CONFIG_RECONSTRUCT_H__


#include "../../paraprobe-utils/src/PARAPROBE_Histogram.h"
#include "../../paraprobe-utils/src/PARAPROBE_FFT.h"


enum WHICH_SPACE {
	RECONSTRUCTION_SPACE,
	DETECTOR_SPACE
};


enum RECONSTRUCTION_METHOD {
	BAS_ETAL
};

/*
struct ival
{
	apt_real min;
	apt_real incr;
	apt_real max;
	ival() : min(0.0), incr(0.0), max(0.0) {}
	ival(const apt_real _mi, const apt_real _incr, const apt_real _mx ) :
		min(_mi), incr(_incr), max(_mx) {}
};
*/


class ConfigReconstruct
{
public:
	
	//static string InputfilePSE;
	static string InputfileTranscoder;
		
	static WHICH_SPACE ReferenceSpace;
	static RECONSTRUCTION_METHOD ReconMethod;

	static lival FlightLength;
	static lival AtomicDensity;
	static lival EvaporationField;
	static lival DetectionEfficiency;
	static lival FieldFactor;
	static lival ImageCompressionFactor;
	
	static lival IonsPerHitMap;
	static apt_real DetectorGridBinWidthX;
	static apt_real DetectorGridBinWidthY;

	static bool readXML( string filename = "" );
	static bool checkUserInput();
	static void reportSettings( vector<pparm> & res );
};

#endif
