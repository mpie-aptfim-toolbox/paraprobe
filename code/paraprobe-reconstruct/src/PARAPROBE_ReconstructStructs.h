//##MK::GPLV3

#ifndef __PARAPROBE_RECONSTRUCT_STRUCTS_H__
#define __PARAPROBE_RECONSTRUCT_STRUCTS_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_ReconstructHdlGPU.h"

struct sqb2d
{
	//MK::add reject if binning is too fine thereby exceeding UINT32
	unsigned int nx;
	unsigned int ny;
	unsigned int nxy;

	apt_xyz width;
	aabb2d rectangle;

	sqb2d() : nx(1), ny(1), nxy(1), width(F32MX), rectangle(aabb2d()) {}
	sqb2d( const unsigned int _nx, const unsigned int _ny, const apt_xyz _width, const aabb2d _rect) :
		nx(_nx), ny(_ny), nxy(_nx*_ny), width(_width), rectangle(_rect) {}

	unsigned int where( p2dm1 const & p );
	p2d where( const unsigned int bxy );
};

ostream& operator<<(ostream& in, sqb2d const & val);


struct ion_sequence
{
	size_t first;
	size_t skip;
	size_t last; 				//one past end
	ion_sequence() : first(0), skip(1), last(0) {}
	ion_sequence( const size_t _f, const size_t _l ) :
		first(_f), skip(1), last(_l) {}
};

ostream& operator<<(ostream& in, ion_sequence const & val);


class hitmap
{
public:
	hitmap();
	hitmap( ion_sequence const & isq, sqb2d const & mcp );
	~hitmap();

	ion_sequence ion_seq;
	sqb2d mcp_grid;
	vector<unsigned int> cnts;
};

ostream& operator<<(ostream& in, hitmap const & val);



struct bas_protocol_parms
{
	apt_real FlightLength;
	apt_real AtomicDensity;
	apt_real EvaporationField;
	apt_real DetectionEfficiency;
	apt_real FieldFactor;
	apt_real ImageCompressionFactor;
	bas_protocol_parms() : FlightLength(0.f), AtomicDensity(0.f), EvaporationField(0.f),
			DetectionEfficiency(0.f), FieldFactor(0.f), ImageCompressionFactor(0.f) {}
	bas_protocol_parms( const apt_real _flen, const apt_real _atdens, const apt_real _evpfld,
			const apt_real _deteff, const apt_real _kf, const apt_real _icf ) : FlightLength(_flen),
					AtomicDensity(_atdens), EvaporationField(_evpfld), DetectionEfficiency(_deteff),
					FieldFactor(_kf), ImageCompressionFactor(_icf) {}
};

ostream& operator<<(ostream& in, bas_protocol_parms const & val);


class recon
{
public:
	recon();
	recon( bas_protocol_parms const & in );
	~recon();

	void apply_gault_breen_protocol( vector<p2dm1> const & dxy, vector<voltm1> const & vdcpu );

	bas_protocol_parms parms;
	vector<p3dm1> xyz;
};


ostream& operator<<(ostream& in, recon const & val);


#endif
