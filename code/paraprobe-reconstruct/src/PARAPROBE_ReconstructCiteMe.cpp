//##MK::GPLV3

#include "PARAPROBE_ReconstructCiteMe.h"

vector<bibtex> CiteMeReconstruct::Citations = vector<bibtex>();

size_t CiteMeReconstruct::cite()
{
	//add additional citations
	Citations.push_back( bibtex( "Article",
								"M. K\"uhbach and P. Bajaj and A. Breen and E. A. J\"agle and B. Gault",
								"On Strong Scaling Open Source Tools for Mining Atom Probe Tomography Data",
								"Microscopy and Microanalysis",
								"2019",
								"Volume 25, Supplement S2",
								"298-299",
								"https://doi.org/10.1017/S1431927619002228" ) );

	Citations.push_back( bibtex( "Article",
								"M. K\"uhbach and P. Bajaj and A. Breen and E. A. J\"agle and B. Gault",
								"On Strong Scaling Open Source Tools for Mining Atom Probe Tomography Data",
								"Ultramicroscopy",
								"2020",
								"",
								"",
								"" ) );

	Citations.push_back( bibtex( "Article",
								"M. K\"uhbach and M. Kasemer and A. Breen and B. Gault",
								"",
								"Journal of Applied Crystallography",
								"2020",
								"",
								"",
								"" ) );

	return Citations.size();
}
