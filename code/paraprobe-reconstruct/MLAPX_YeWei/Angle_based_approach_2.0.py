# -*- coding: utf-8 -*-
"""
Created on Sat Jan 13 12:12:30 2018

@author: User
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Jan 12 13:52:27 2018

@author: y.wei
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Jan  9 14:42:36 2018

@author: y.wei
"""

import numpy as np
import cv2
import os
from keras.models import load_model
from Pole_classification import draw_poles
from Pole_classification import pole_classification


def denoise(rawImage, ero_iter, dilate_iter):    #
    hsv = (cv2.cvtColor(rawImage, cv2.COLOR_BGR2GRAY))#np.invert
    img_new = cv2.medianBlur(hsv,9)
    blurred = cv2.GaussianBlur(img_new, (11, 11), 0)
    thresh1 = cv2.erode(blurred, None, iterations=ero_iter)
    thresh2 = cv2.dilate(thresh1, None, iterations=dilate_iter)
    return thresh2

def Blob_detector(original_img, minArea, minThres, maxThres,erosion,dilation):
    params = cv2.SimpleBlobDetector_Params()
    # Change thresholds
    params.minThreshold = minThres
    params.maxThreshold = maxThres;
     
    # Filter by Area.
    params.filterByArea = True
    params.minArea = minArea  # 2,200, 200,0.1,0.1,0.1
    ## 
    ### Filter by Circularity
    params.filterByCircularity = True
    params.minCircularity = 0.1
    ## 
    ## Filter by Convexity
    params.filterByConvexity = True
    params.minConvexity = 0.1 #0.3
    # 
    ## Filter by Inertia
    params.filterByInertia = True
    params.minInertiaRatio = 0.1 #0.3
     
    # Create a detector with the parameters
    ver = (cv2.__version__).split('.')
    if int(ver[0]) < 3 :
        detector = cv2.SimpleBlobDetector(params)
    else : 
        detector = cv2.SimpleBlobDetector_create(params)        
    keypoints = detector.detect(denoise(original_img,erosion,dilation))
    if keypoints is None:
        print('Poles is not found!')
        return []
    else:
        return keypoints   #new_image,


def process_img(original_image,minArea, minThres,maxThres, erosion, dilation):
    processed_img = cv2.cvtColor(original_image, cv2.COLOR_BGR2GRAY)
    processed_img  = cv2.medianBlur(processed_img ,5)
    processed_img = cv2.GaussianBlur(processed_img, (5,5), 0 )   
    processed_img = cv2.erode(processed_img, None, iterations=2)
    processed_img = cv2.dilate(processed_img, None, iterations=4)
    processed_img = cv2.Canny(processed_img, threshold1=50, threshold2=100)#100,200
#    plt.figure(1)
#    plt.imshow(processed_img)
    processed_img = cv2.GaussianBlur(processed_img, (5,5), 0 )
    processed_img  = cv2.medianBlur(processed_img ,3)
    
    thresholdVal=50
    minlinelengthVal=45
    maxlinegapVal=19
    
    lines = cv2.HoughLinesP(processed_img, rho=1.0, theta=np.pi/180.0,
                                    threshold=thresholdVal,
                                    minLineLength=minlinelengthVal,
                                    maxLineGap=maxlinegapVal)
#    print(lines)
    keypoints=Blob_detector(original_image, minArea, minThres, maxThres, erosion, dilation)

    coords_keypoints = [list(p.pt) for p in keypoints]
    keysize = [np.int(p.size) for p in keypoints]
    return lines, coords_keypoints, keysize
       

if __name__ == "__main__": 
    data_path=os.path.join(os.path.dirname(os.path.abspath(__file__)),'data_test')
    data_save=os.path.join(os.path.dirname(os.path.abspath(__file__)),'data_detected')
    data_dir_list = os.listdir(data_path)
    n=0  
    NN_model = load_model('Angle_based_model.h5')
    data = []
    for file in os.listdir(data_path):   
            if file.endswith("png"):
                screen = cv2.imread(data_path +'\\'+ file)#imgs[i] =np.array((Image.open(path1+ '\\' + file)))#np.invert
                print('loading image: {}'.format(file))
                minThres=1
                maxThres=200
                minArea=10
                erosion=4 #4
                dilation=7 #7
                lines, poles, poles_size = process_img(screen, minArea, minThres, maxThres, erosion, dilation)        
                comp_image = draw_poles(screen, poles[1], poles_size[1],(0,0,255))
                location = pole_classification(screen, comp_image, lines, poles, poles_size, file, data_save, NN_model)
                n+=1
                if location is not None:
                    data.append([file, location[0], location[2]])
                else:
                    print('unrecognized image: {}'.format(file))
                    
    import pickle
    with open('data.txt','wb') as fp:
          pickle.dump(data,fp)
   #this is for reading the data.txt file             
#   with open("test.txt", "rb") as fp:   # Unpickling
#           loaded_data = pickle.load(fp)