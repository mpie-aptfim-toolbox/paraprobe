# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 19:49:50 2020

@author: y.wei
"""

from test_OOP_massive import Classification
import os
import cv2
import numpy as np
from itertools import combinations

def find_angles(mypole, poles):   
    a = np.array(poles[0])
    b = np.array(mypole)
    c = np.array(poles[1])

    ba = a - b
    bc = c - b

    cosine_angle_ac = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))
    angle_ac = np.arccos(cosine_angle_ac)
    return np.degrees(angle_ac)

def draw_poles(img, pole, pole_size,colors):
    pole=[np.int(pole[0]), np.int(pole[1])]
    new_image = cv2.circle(img, (pole[0],
    pole[1]),
    radius=np.int(pole_size), 
    color=colors,
    thickness=5)
    return new_image

def find_angles_polygon(poles):
    angles = []
    for pole in poles:    
        maximum_angle=1
        for i in list(combinations(poles, 2)):
            angle =find_angles(pole, i) 
            if maximum_angle <= angle:
                maximum_angle = angle
        angles.append(maximum_angle)
    return angles  

def magnitude(p1, p2):
    vect_x = p2[0] - p1[0]
    vect_y = p2[1] - p1[1]
    return np.sqrt(vect_x**2 + vect_y**2)
      
def check_polygon_length_ratio(poles):
    ds=[]
    x=list(combinations(poles, 2))

    for xx in x:
         d1 = magnitude(xx[0], xx[1])
#         d2 = magnitude(mypole, poles[1])
         ds.append(d1)
    ratio = min(ds)/max(ds)
#    max_d =max(ds)
    return ratio, max(ds)    

def PolygonArea(poles):
#    x= np.array([mypole[0][0] ,mypole[1][0] ,mypole[2][0] ,mypole[3][0]]) 
#    y= np.array([600-mypole[0][1] ,600-mypole[1][1] ,600-mypole[2][1] ,600-mypole[3][1]])
    Cs=[]
    for pole in list(combinations(poles, 2)):   
         C=magnitude(pole[0], pole[1])
         Cs.append(C)
    total_C=sum(Cs)         
    return total_C

def pole_classification(screen, comp_image, lines, poles, poles_size, file, data_save, model):
    height,width,channels = screen.shape    
    i=0
    ii=0
    poles_high_rank =[]
    poles_size_high_rank=[]
    num_shown_poles=6
    for pole, pole_size in zip(poles, poles_size):   
        if screen[np.int(pole[1]), np.int(pole[0]),0]>20:          
            poles_high_rank.append(poles[ii])
            poles_size_high_rank.append(pole_size)
            i+=1 
        ii+=1
    #    print(len(poles_high_rank))
        if i>=num_shown_poles:
            break
    #        continue
            
        
    j=0        
    for pole, pole_size in zip(poles_high_rank, poles_size_high_rank):    
            comp_image = draw_poles(comp_image, pole, pole_size,(255,255,255))
        #            for x in range(0,len(keypoints)):
#            text='(%s)' % (j)
            location=(np.int(pole[0])-2*np.int(pole_size)+20, 
                      np.int(pole[1])- np.int(pole_size))
    #                    comp_image=cv2.putText(
    #                                        comp_image, text,location,
    #                                        cv2.FONT_HERSHEY_SIMPLEX, 
    #                                        0.5,
    #                                        (0,0,0),
    #                                       3)    
            j+=1
            
#    num_adjacent_poles=3
    angles=[]
    my_true_poles=[]
    rank_true_poles=[]
    
#    m=0 
    #high_rank = 6
    for  mypoles, rank in zip(list(combinations(poles_high_rank, 4)), list(combinations(list(range(0,num_shown_poles)), 4))):    
    #     high_rank=100 # any big value is fine   
         angles = find_angles_polygon(mypoles)
         indices = [i for i, x in enumerate(angles) if x >=115]
         
         if len(indices)<=1:
             if(max(angles)<=160 and min(angles)>=15 and sum(angles)>358):   
                current_rank=sum(rank)
                my_true_poles.append(mypoles) 
                rank_true_poles.append(current_rank)
           
    final_true_poles = []
    ratio_final_true_poles = []
    ratio_Q_factor=0.455
    size_maxratio=0.75
    for pole in my_true_poles:
        r,maxd=check_polygon_length_ratio(pole)
        size=maxd/width
    #                print('ratio:{}'.format(r))
    #                print('size:{}'.format(size))
        if r > ratio_Q_factor and  size < size_maxratio:
            final_true_poles.append(pole)  
            ratio_final_true_poles.append(r)
            print('{} bigger than ratio Q factor {} and its size {} is small than 0.8, qualified!'.format(r,ratio_Q_factor, size))
    if len(final_true_poles)==0:
        print('we have failed to find a qualified polygon!')
        
    unique_true_poles=[]
    new_Areas=[]    
    final_angles=[]
    if len(final_true_poles)>0:
        for mypole in final_true_poles:
    #            corners_sorted = sorted(mypole)
                new_Area = PolygonArea(mypole)
                final_angle = find_angles_polygon(mypole)
    #            new_Area=PolygonArea(mypole)
                new_Areas.append(new_Area)
                final_angles.append(final_angle)
                
        tempo_true_angles=[]
        tempo_true_types=[]
        tempo_true_poles=[]
        final_areas=[]
        for my_angles, mypoles in zip(final_angles, final_true_poles):
             print('angle sampling and selection:')
             new_a=Classification(my_angles, model)
             tempo_type, preds=new_a.neural_network()
             print(preds*100)
#             tempo_poles= mypoles
             print(my_angles)  
             print(sum(my_angles))
             indice_none_zero = [i for i, x in enumerate(tempo_type) if (x !=0)]
             if len(indice_none_zero)>=2:
                 tempo_true_angles.append(my_angles)
                 tempo_true_types.append(tempo_type)
                 tempo_true_poles.append(mypoles)
    
        if len(tempo_true_types)>=1:
            for mypole in tempo_true_poles:
        #            corners_sorted = sorted(mypole)
                final_area = PolygonArea(mypole)
                final_areas.append(final_area)
    
            minimum = sorted(final_areas)
            mimimum_indice = [j for j, v in enumerate(final_areas) if v == minimum[0]]
            true_types=tempo_true_types[mimimum_indice[0]]
            true_angles=tempo_true_angles[mimimum_indice[0]]
            unique_true_poles=tempo_true_poles[mimimum_indice[0]]
    
            for pole, angle, true_type in zip(unique_true_poles, true_angles, true_types):    
                    comp_image = draw_poles(comp_image, pole, 10, (0,0,0))
                    text_type='(Type:{})'.format(true_type)
                    text_angle='(angle:{})'.format(angle)#np.int()
#                    text_position = '(X:{}, Y:{})'.format(np.int(pole[0])-width/2,-(np.int(pole[1])-height/2))
                    location_type=(np.int(pole[0])-4*np.int(pole_size)-20, 
                              np.int(pole[1])+np.int(pole_size)+25)
#                    location_position=(np.int(pole[0])-np.int(pole_size)-30, 
#                                       np.int(pole[1])+np.int(pole_size)+45)
                    comp_image=cv2.putText(
                                        comp_image, text_type, location_type,
                                        cv2.FONT_HERSHEY_SIMPLEX, 
                                        1.5,
                                        (0,0,0),#(0,0,255)
                                       5) 
        else:
            true_types=[]
            true_angles=[]
            unique_true_poles=[]
            min_value = min(new_Areas)
            min_index = new_Areas.index(min_value)
            unique_poles=final_true_poles[min_index]    
            unique_angles = find_angles_polygon(unique_poles) 
            the_type='unknown'        
            for pole, angle in zip(unique_poles, unique_angles):    
                    comp_image = draw_poles(comp_image, pole, 10, (255,0,255))
                    text_angle='(angle:{})'.format(np.int(angle))
    
                    location=(np.int(pole[0])-2*np.int(pole_size), 
                              np.int(pole[1])+2*np.int(pole_size))
                    comp_image=cv2.putText(
                                        comp_image, text_angle, location,
                                        cv2.FONT_HERSHEY_SIMPLEX, 
                                        0.8,
                                        (0,0,0),
                                       3)
        cv2.imwrite(data_save +'\\'+ file, comp_image)
        return true_types, true_angles, unique_true_poles
