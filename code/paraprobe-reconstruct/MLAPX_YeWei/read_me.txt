1. Put detector hit map in the folder: data_test
2. Run the python file: Angle_based_apporach_2.0.py
3. Poles on the detector hit maps will be given labels.
Notes:
1. Be careful: For detector hit map with very distorted features, machine will yield no result or even wrong result due to high uncertainty.
2. Program will save data as a pickle file: data.txt (in the program i also showed how to load them at last lines)
3. its is still python-styled raw pixel coordinates, you must be carefule with them.