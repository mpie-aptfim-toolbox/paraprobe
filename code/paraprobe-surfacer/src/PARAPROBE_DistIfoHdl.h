//##MK::GPLV3

#ifndef __PARAPROBE_SURFACER_DISTIFOHDL_H__
#define __PARAPROBE_SURFACER_DISTIFOHDL_H__

#include "PARAPROBE_TriangleTree.h"

class distifoHdl
{
public:
	distifoHdl();
	~distifoHdl();

	void identify_vacuum_atsurface_bincenters();
	void compute_coarse_distance_tovacuum();
	bool build_hullbins_kdtree();
	void chop_hullbins_kdtree();
	
	voxlizer cca;
	vector<p3d> hullbins;
	vector<apt_real> cdistances;

	kd_tree* tree;
	vector<p3d> thistree; //the actual tree
};

#endif
