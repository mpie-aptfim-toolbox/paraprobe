//##MK::GPLV3

#include "PARAPROBE_DistIfoHdl.h"

distifoHdl::distifoHdl()
{
	tree = NULL;
}


distifoHdl::~distifoHdl()
{
	if ( tree != NULL ) {
		delete tree; tree = NULL;
	}
}


void distifoHdl::identify_vacuum_atsurface_bincenters()
{
	size_t nxyz = cca.vxlgrid.nxyz;
	for( size_t b = 0; b < nxyz; ++b ) {
		if ( cca.IsSurface[b] == false ) { //surface bins are seldom
			continue;
		}
		else {
			p3d here = cca.vxlgrid.where( b );
			if ( here.x < F32MX && here.y < F32MX && here.z < F32MX ) {
				hullbins.push_back( cca.vxlgrid.where( b ) );
			}
			else {
cerr << "Detected an invalid hullbin position b/x/y/z " << b << "\t\t" << here.x << ";" << here.y << ";" << here.z << "\n";
			}
		}
	}
}


void distifoHdl::compute_coarse_distance_tovacuum()
{
	cdistances = vector<apt_real>( cca.vxlgrid.nxyz, F32MX );
	//F32MX means we do not have coarse distance information, so we need to compute exact distances
	//conservatively using many triangles

	#pragma omp parallel
	{
		vector<pair<unsigned int,apt_real>> myres;
		//start with a very conservative search radius to find hullbins
		apt_real R = 0.5 * min( min((cca.vxlgrid.box.xmx - cca.vxlgrid.box.xmi),
				(cca.vxlgrid.box.ymx - cca.vxlgrid.box.ymi) ), (cca.vxlgrid.box.zmx - cca.vxlgrid.box.zmi) );
		apt_real RSQR = SQR(R);

		apt_real RequeryThrshld = ConfigSurfacer::RequeryingThreshold;
		unsigned int RequeryCount = 50;

		size_t nxyz = cca.vxlgrid.nxyz;
		#pragma omp for schedule(dynamic,1) nowait
		for( size_t b = 0; b < nxyz; b++ ) {
			if ( cca.IsInside[b] == true ) {
				//although this pre-processing demands to scan probably a large number of bin center positions
				//it allows to cut time in total for the subsequent distancing because
				//i) here only point-point distances have to be computed
				//ii) we compute distances between barycenter positions of a binning on a position point cloud
				//with a lower point density per unit volume (100-500 ions) / (2nm)^3
				p3d bc = cca.vxlgrid.where( b );

				//query the tree
				vector<p3d> mycand;
				tree->range_rball_append_external( bc, thistree, RSQR, mycand );

				//test the result for shortest distance with requerying
				if ( mycand.size() > 0 ) { //most likely case
					size_t ntests = 0;
					size_t np = mycand.size();
					size_t pt = 0;
					apt_real CurrentBest = RSQR;
					apt_real CurrentValue = RSQR;
					while ( pt < np ) { //head controlled, if pt == 0 and np == 0 will not execute
						p3d pc = mycand.at(pt); //##MK::in-place
						CurrentValue = SQR(pc.x-bc.x)+SQR(pc.y-bc.y)+SQR(pc.z-bc.z);
						if ( CurrentValue > CurrentBest ) { //most likely not resetting the minimum
							pt++;
							continue;
						}
						else {
							apt_real OldBest = CurrentBest;
							//update best, we found a shorter one!
							CurrentBest = CurrentValue;
							if ( OldBest > EPSILON ) { //is also a numerical stable requerying possible eventually?
								unsigned int rem = np - pt;
								if ( (CurrentValue / OldBest) > RequeryThrshld || rem < RequeryCount ) {
									//insufficient distance reduction to warrant costs of a full requery
									//or insufficient remaining triangles to scan to warrant cost of a full requery
									pt++;
									continue;
								}
								else { //substantial distance reduction achieved, it is likely useful to requery now
									apt_real Rnew = sqrt(CurrentBest) + EPSILON; //better a little bit larger R to catch case of exact overlap
									apt_real RnewSQR = SQR(Rnew);

									mycand = vector<p3d>();
									ntests += pt; //so many tests were performed before this requerying
									tree->range_rball_append_external( bc, thistree, RnewSQR, mycand );
									pt = 0;
									np = mycand.size();
									continue;
								} //back to while
							} //case stable requerying done
							else { //querying might not be stable because float sqrt(eps) is ill-posed then do not requery and instead test remaining candidates
								pt++;
								continue;
							}
						}
					} //eventually all candidates tested

					//MK::we need to add a safety distance here as we have computed distances between
					//barycenter of voxel but the ions can be also close to the edges of the voxel
					//there might be some improvement potential here but for now we are just conservative
					apt_real coarse_result = sqrt(CurrentBest) + 2.0 * sqrt(3.0) * cca.vxlgrid.width;

					myres.push_back( pair<unsigned int,apt_real>( b, coarse_result ) );
					/*#pragma omp critical
					{
							cout << "Setting distance for b/bc/CurrentBest/coarse " << b << "\t\t" << bc.x << ";" << bc.y << ";" << bc.z << "\t\t" <<
									CurrentBest << ";" << coarse_result << "\t\t" << myres.back().first << ";" << myres.back().second << "\n";
					}*/
				} //done with the candidates
			} //done with computing a distance for an interior bin
		} //next voxel

		#pragma omp critical
		{
			cout << "Thread " << omp_get_thread_num() << " about to communicate local coarse distances!" << "\n";
			for( auto it = myres.begin(); it != myres.end(); it++ ) {
				unsigned int b = it->first;
				cdistances.at(b) = it->second;
			}
			myres = vector<pair<unsigned int,apt_real>>();
		}
	} //done computing all distances
}


bool distifoHdl::build_hullbins_kdtree()
{
	//##MK:::
	//build KDtree to find fast close hullbins to a voxel center
	/*vector<p3d> hullbins_org;
	vector<unsigned int> hullbins_lbls;
	unsigned int lbl = 0;
	hullbins_org.reserve( hullbins.size() );
	hullbins_lbls.reserve( hullbins.size() );
	for( auto it = hullbins.begin(); it != hullbins.end(); it++, lbl++ ) {
		hullbins_org.push_back( *it );
		hullbins_lbls.push_back( lbl );
	}*/

	vector<size_t> permutations; //the permutations to better linearize the memory accesses when querying the tree
	tree = NULL;
	try {
		tree = new kd_tree; //this calls the default constructor
	}
	catch (bad_alloc &mecroak) {
		cerr << "Allocation failure for tree upon coarse distance approximation!" << "\n"; return false;
	}

	tree->build( hullbins, permutations, 16 );

	cout << "Rank " << MASTER << " globally rotating direction grid tree has " << tree->nodes.size() << " nodes" << "\n";

	tree->pack( permutations, hullbins, thistree );
	permutations = vector<size_t>();
	/*hullbins_org = vector<p3d>();
	hullbins_lbls = vector<unsigned int>();*/

	return true;
}


void distifoHdl::chop_hullbins_kdtree()
{
	if ( tree != NULL ) {
		delete tree; tree = NULL;
	}
	thistree = vector<p3d>();
}

