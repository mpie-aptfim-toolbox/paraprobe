//##MK::GPLV3


#ifndef __PARAPROBE_SURFACER_CITEME_H__
#define __PARAPROBE_SURFACER_CITEME_H__

#include "CONFIG_Surfacer.h"


class CiteMeSurfacer
{
public:
	
	static vector<bibtex> Citations;
	
	static size_t cite();
};

#endif

