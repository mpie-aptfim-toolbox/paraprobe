/*
	Copyright Max-Planck-Institut f\"ur Eisenforschung, GmbH, D\"sseldorf
//##MK::CODE
 *
 */

#ifndef __PARAPROBE_CONFIG_SURFACER_H__
#define __PARAPROBE_CONFIG_SURFACER_H__

#include "../../paraprobe-utils/src/CONFIG_Shared.h"

enum SURFACING_MODE {
	SURFACE_NOTHING,
	SURFACE_ALPHASHAPE					//needs the CGAL library
};


enum ALPHASHAPE_ALPHAVALUE_CHOICE {
	ASHAPE_SMALLEST_SOLID,				//which alpha to use for alpha shape
	ASHAPE_CGAL_OPTIMAL
};


enum DISTANCING_MODE {
	DISTANCING_NOTHING,
	DISTANCING_ANALYTICAL_COMPLETE,				//analytical ion to closest triangle all ions
	DISTANCING_ANALYTICAL_SKIN					//within skin to triangle hull up to rmax, rest not ranged
};


class ConfigSurfacer
{
public:
	
	static SURFACING_MODE SurfacingMode;
	static ALPHASHAPE_ALPHAVALUE_CHOICE AlphaShapeAlphaValue;
	static string InputfileReconstruction;
	//static string Outputfile;
	
	static apt_real AdvIonPruneBinWidthMin;
	static apt_real AdvIonPruneBinWidthIncr;
	static apt_real AdvIonPruneBinWidthMax;
	static int EdgeDetectionKernelWidth;
	
	static apt_real AdvDistanceBinWidthMin;
	static apt_real AdvDistanceBinWidthIncr;
	static apt_real AdvDistanceBinWidthMax;

	static DISTANCING_MODE DistancingMode;
	static apt_real DistancingRadiusMax;
	
	static apt_real RequeryingThreshold;

	static bool readXML( string filename = "" );
	static bool checkUserInput();
	static void reportSettings( vector<pparm> & res );
};

#endif
