//##MK::CODESPLIT


#ifndef __PARAPROBE_SURFACER_VOXELIZER_H__
#define __PARAPROBE_SURFACER_VOXELIZER_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_Decompositor.h"

//MK::maybe better inherit from utils/Vxlizer
class voxlizer
{
	//the class is a utility tool to interface to the binning and HK algoritm used during binning the ion position cloud and binarize
	//the actual assignment of which ions end up in which bins is required temporarily only, hence it is executed
	//and managed in the subroutine context of a callee to of this voxlizer class
public:
	voxlizer();
	~voxlizer();
	
	void define_rectangular_binning( const apt_real bwidth, const aabb3d rve );
	bool allocate_binaries();
	void initialize_bins();
	size_t rectangular_binning( const p3dm3 p );
	bool identify_vacuum_bins();
	void identify_vacuumbins_withmoore_nonvacuum_contact();
	void identify_surface_adjacent_bins();
	void identify_inside_bins(); //##MK::improve here, better do a identify_bin_state() because first vaccum, then surfac,e then inside
	
	sqb vxlgrid;
	
	bool* IsVacuum;		//a bin with no ions laying in vacuum aka outside the tip
	bool* IsSurface;	//a bin with at least one ion in the Moore neighborhood of the vacuum
	bool* IsInside;		//a bin entirely enclosed with no Moore neighbor that is IsSurface == true
};

#endif
