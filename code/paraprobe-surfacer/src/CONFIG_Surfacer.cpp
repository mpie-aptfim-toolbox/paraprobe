//##MK::CODE

#include "CONFIG_Surfacer.h"

SURFACING_MODE ConfigSurfacer::SurfacingMode = SURFACE_NOTHING;
ALPHASHAPE_ALPHAVALUE_CHOICE ConfigSurfacer::AlphaShapeAlphaValue = ASHAPE_SMALLEST_SOLID;
string ConfigSurfacer::InputfileReconstruction = "";
//string ConfigSurfacer::Outputfile = "";
apt_real ConfigSurfacer::AdvIonPruneBinWidthMin = 1.f; //1nm best practice guess useful for many cases
apt_real ConfigSurfacer::AdvIonPruneBinWidthIncr = 1.f;
apt_real ConfigSurfacer::AdvIonPruneBinWidthMax = 1.f;
int ConfigSurfacer::EdgeDetectionKernelWidth = 1;

apt_real ConfigSurfacer::AdvDistanceBinWidthMin = 2.f; //1nm best practice guess useful for many cases
apt_real ConfigSurfacer::AdvDistanceBinWidthIncr = 2.f;
apt_real ConfigSurfacer::AdvDistanceBinWidthMax = 2.f;


DISTANCING_MODE ConfigSurfacer::DistancingMode = DISTANCING_NOTHING;
apt_real ConfigSurfacer::DistancingRadiusMax = 0.f;
apt_real ConfigSurfacer::RequeryingThreshold = 0.9;


bool ConfigSurfacer::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.Input.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigSurfacer")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;

	//WHAT TYPE OF ANALYSIS TO DO
	mode = read_xml_attribute_uint32( rootNode, "SurfacingMode" );
	switch (mode)
	{
		case SURFACE_ALPHASHAPE:
			SurfacingMode = SURFACE_ALPHASHAPE; break;
		default:
			SurfacingMode = SURFACE_NOTHING;
	}
	InputfileReconstruction = read_xml_attribute_string( rootNode, "InputfileReconstruction" );
	//Outputfile = read_xml_attribute_string( rootNode, "Outputfile" );
	
	mode = read_xml_attribute_uint32( rootNode, "AlphaShapeAlphaValue" );
	switch (mode)
	{
		case ASHAPE_SMALLEST_SOLID:
			AlphaShapeAlphaValue = ASHAPE_SMALLEST_SOLID; break;
		case ASHAPE_CGAL_OPTIMAL:
			AlphaShapeAlphaValue = ASHAPE_CGAL_OPTIMAL; break;
		default:
			AlphaShapeAlphaValue = ASHAPE_SMALLEST_SOLID;
	}
	
	AdvIonPruneBinWidthMin = read_xml_attribute_float( rootNode, "AdvIonPruneBinWidthMin" );
	AdvIonPruneBinWidthIncr = read_xml_attribute_float( rootNode, "AdvIonPruneBinWidthIncr" );
	AdvIonPruneBinWidthMax = read_xml_attribute_float( rootNode, "AdvIonPruneBinWidthMin" );
	
	mode = read_xml_attribute_uint32( rootNode, "DistancingMode" );
	switch (mode)
	{
		case DISTANCING_NOTHING:
			DistancingMode = DISTANCING_NOTHING; break;
		case DISTANCING_ANALYTICAL_SKIN:
			DistancingMode = DISTANCING_ANALYTICAL_SKIN; break;
		default:
			DistancingMode = DISTANCING_ANALYTICAL_COMPLETE;

	}

	if ( DistancingMode == DISTANCING_ANALYTICAL_SKIN ) {
		DistancingRadiusMax = read_xml_attribute_float( rootNode, "DistancingRadiusMax" );
	}
	
	if ( DistancingMode == DISTANCING_ANALYTICAL_COMPLETE ) {
		AdvDistanceBinWidthMin = read_xml_attribute_float( rootNode, "AdvDistanceBinWidthMin" );
		AdvDistanceBinWidthIncr = read_xml_attribute_float( rootNode, "AdvDistanceBinWidthIncr" );
		AdvDistanceBinWidthMax = read_xml_attribute_float( rootNode, "AdvDistanceBinWidthMax" );
	}

	if ( DistancingMode != DISTANCING_NOTHING ) {
		RequeryingThreshold = read_xml_attribute_float( rootNode, "RequeryingThreshold" );
	}
	return true;
}


bool ConfigSurfacer::checkUserInput()
{
	cout << "ConfigSurfacer::" << "\n";
	switch(SurfacingMode)
	{
		case SURFACE_ALPHASHAPE:
			cout << "Building an alpha shape of the entire specimen" << "\n"; break;
		case SURFACE_NOTHING:
			cerr << "No surfacing choosen, about to quit" << "\n"; return false;
	}
	if( SurfacingMode != SURFACE_NOTHING ) {
		switch(AlphaShapeAlphaValue)
		{
			case ASHAPE_SMALLEST_SOLID:
				cout << "Building the alpha-shape with the smallest alpha value to get a solid" << "\n"; break;
			case ASHAPE_CGAL_OPTIMAL:
				cout << "Building the alpha-shape with the according to the CGAL optimal alpha value" << "\n"; break;
			default:
				cerr << "Using an unknown alpha-shape alpha value" << "\n"; return false;
		}
		if( AdvIonPruneBinWidthMin < EPSILON ) {
			cerr << "AdvIonPruneBinWidthMin must be positive and finite!" << "\n"; return false;
		}
		cout << "AdvIonPruneBinWidthMin " << AdvIonPruneBinWidthMin << "\n";
		if( AdvIonPruneBinWidthIncr < EPSILON ) {
			cerr << "AdvIonPruneBinWidthIncr must be positive and finite!" << "\n"; return false;
		}
		cout << "AdvIonPruneBinWidthIncr " << AdvIonPruneBinWidthIncr << "\n";
		if( AdvIonPruneBinWidthMin > AdvIonPruneBinWidthMax ) {
			cerr << "AdvIonPruneBinWidthMax should be larger than AdvIonPruneBinWidthMin!" << "\n"; return false;
		}
		cout << "AdvIonPruneBinWidthMax " << AdvIonPruneBinWidthMax << "\n";
		if( EdgeDetectionKernelWidth < 1 ) {
			cerr << "EdgeDetectionKernelWidth should be at least 1 !" << "\n"; return false;
		}
		cout << "EdgeDetectionKernelWidth " << EdgeDetectionKernelWidth << "\n";
	}
	
	switch(DistancingMode)
	{
		case DISTANCING_ANALYTICAL_COMPLETE:
			cout << "Computing analytical distance to closest triangle for each ion within dataset is costly" << "\n"; break;
		case DISTANCING_ANALYTICAL_SKIN:
			cout << "Computing analytical distance to closest triangle for each ion closer than DistancingRadiusMax" << "\n"; break;
		default:
			cerr << "No distance values to the hull are computed" << "\n";
	}
	
	if ( DistancingMode == DISTANCING_ANALYTICAL_SKIN ) {
		if( DistancingRadiusMax < EPSILON ) {
			cerr << "DistancingRadiusMax should be positive and finite!" << "\n"; return false;
		}
		cout << "DistancingRadiusMax " << DistancingRadiusMax << "\n";
	}
	if ( DistancingMode == DISTANCING_ANALYTICAL_COMPLETE ) {
		if( AdvDistanceBinWidthMin < EPSILON ) {
			cerr << "AdvDistanceBinWidthMin must be positive and finite!" << "\n"; return false;
		}
		cout << "AdvDistanceBinWidthMin " << AdvDistanceBinWidthMin << "\n";
		if( AdvDistanceBinWidthIncr < EPSILON ) {
			cerr << "AdvDistanceBinWidthIncr must be positive and finite!" << "\n"; return false;
		}
		cout << "AdvDistanceBinWidthIncr " << AdvDistanceBinWidthIncr << "\n";
		if( AdvDistanceBinWidthMin > AdvDistanceBinWidthMax ) {
			cerr << "AdvDistanceBinWidthMax should be larger than AdvDistanceBinWidthMin!" << "\n"; return false;
		}
		cout << "AdvDistanceBinWidthMax " << AdvDistanceBinWidthMax << "\n";
	}
	if ( DistancingMode != DISTANCING_NOTHING ) {
		if ( RequeryingThreshold > 0.9 ) { //must not be 1.0 !
			cerr << "RequeryingThreshold should be on interval [0.0, 0.9]!" << "\n"; return false;
		}
		cout << "RequeryingThreshold " << RequeryingThreshold << "\n"; //can be set to 0.0 then one will never requery
	}
	cout << "InputReconstruction read from " << InputfileReconstruction << "\n";
	//cout << "Output written to " << Outputfile << "\n";

	cout << "\n";
	return true;
}


void ConfigSurfacer::reportSettings( vector<pparm> & res )
{
	res.push_back( pparm( "SurfacingMode", sizet2str(SurfacingMode), "", "which computational geometry method to characterize the edge of the dataset" ) );
	res.push_back( pparm( "AlphaShapeAlphaValue", sizet2str(AlphaShapeAlphaValue), "", "which method to define which alpha-shape to take" ) );
	res.push_back( pparm( "InputfileReconstruction", InputfileReconstruction, "", "pre-computed reconstruction space x,y,z, and atom type" ) );

	res.push_back( pparm( "AdvIonPruneBinWidthMin", real2str(AdvIonPruneBinWidthMin), "nm", "for filtering ions prior alpha-shape computation, minimum to take for linear interval of values for spacing between grid cells along x=y=z" ) );
	res.push_back( pparm( "AdvIonPruneBinWidthIncr", real2str(AdvIonPruneBinWidthIncr), "nm", "for filtering ions prior alpha-shape computation, increment to take for linear interval of values for spacing between grid cells along x=y=z" ) );
	res.push_back( pparm( "AdvIonPruneBinWidthMax", real2str(AdvIonPruneBinWidthMax), "nm", "for filtering ions prior alpha-shape computation, maximum to take for linear interval of values for spacing between grid cells along x=y=z" ) );
	res.push_back( pparm( "EdgeDetectionKernelWidth", int2str(EdgeDetectionKernelWidth), "1", "for filtering ions, extend of Moore-type kernel in every direction to segment interior of dataset vs exterior" ) );
	res.push_back( pparm( "DistancingMode", sizet2str(DistancingMode), "", "which method and choice when distancing ions to the edge of the dataset") );
	res.push_back( pparm( "AdvDistanceBinWidthMin", real2str(AdvDistanceBinWidthMin), "nm", "for pre-processing to get estimates for coarse ion-to-edge distances, minimum to take for linear interval of values for spacing between grid cells along x=y=z" ) );
	res.push_back( pparm( "AdvDistanceBinWidthIncr", real2str(AdvDistanceBinWidthIncr), "nm", "for pre-processing to get estimates for coarse ion-to-edge distances, increment to take for linear interval of values for spacing between grid cells along x=y=z" ) );
	res.push_back( pparm( "AdvDistanceBinWidthMax", real2str(AdvDistanceBinWidthMax), "nm", "for pre-processing to get estimates for coarse ion-to-edge distances, maximum to take for linear interval of values for spacing between grid cells along x=y=z" ) );

	res.push_back( pparm( "DistancingRadiusMax", real2str(DistancingRadiusMax), "nm", "threshold below which ion-to-edge distances are computed analytically and above which they are set to this threshold value") );
	res.push_back( pparm( "RequeryingThreshold", real2str(RequeryingThreshold), "1.0", "control, iterative requerying of ions during distancing, fraction of old distance value below which the currently shortest distance has to be reduced before initiating a re-query" ) );
}
