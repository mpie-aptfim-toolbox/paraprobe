//##MK::CODE

#ifndef __PARAPROBE_SURFACER_XDMF_H__
#define __PARAPROBE_SURFACER_XDMF_H__

#include "PARAPROBE_SurfacerHDF5.h"

class surfacer_xdmf : public xdmfHdl
{
	//tool-specific sub-class of a XDMF inheriting all methods of the base class xdmfHdl
	//coordinating instance handling all (sequential) writing to XDMF text file to supplement visualization of HDF5 content

public:
	surfacer_xdmf();
	~surfacer_xdmf();

	int create_alphashape_file( const string xmlfn, const size_t topo_nelements,
			const size_t topo_dims, const size_t geom_dims, const string h5hull );
	int create_distance_file( const string xmlfn, const size_t nions, 
			const string h5recon, const string h5dist );
	
//private:
};

#endif
