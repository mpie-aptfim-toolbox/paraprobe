//##MK::GPLV3

#include "PARAPROBE_SurfacerHdl.h"


//define what is passed upon execution
#define SIMID									1
#define CONTROLFILE								2

#ifndef GITSHA									//otherwise we could not compile without adding -DGITSHA="d123" a dummy GitSha
	#define GITSHA								0
#endif
//stringification of strings passed from cmake at compile time
//https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html
#define xstr(s)									str(s)
#define str(s) 									#s


void hello( void )
{
	cout << "paraprobe-surfacer" << "\n";
	cout << xstr(GITSHA) << "\n";
	cout << "Collecting " << CiteMeSurfacer::cite() << " publications for the tool to cite:" << "\n";
	for( auto it = CiteMeSurfacer::Citations.begin(); it != CiteMeSurfacer::Citations.end(); it++ ) {
		cout << it->author << "\n";
		cout << it->title << "\n";
		cout << it->journal << ", " << it->year << ", " << it->vol << ", pp" << it->pages << "\n";
		cout << it->doi << "\n";
	}
}

bool init(  int pargc, char** pargv )
{
	ConfigShared::SimID = stoul( pargv[SIMID] );
	try {
		ConfigSurfacer::readXML(pargv[CONTROLFILE]);
	}
	catch (exception& e) {
		cerr << "\n" << "Unable to parse control file! Details:" << "\n" << e.what() << "\n"; return false;
	}
	if ( ConfigSurfacer::checkUserInput() == false ) {
		cerr << "\n" << "Control file settings failed the validity check!" << "\n"; return false;
	}
	else {
		cout << "\n" << "\t\tInput is valid under SimulationID = " << "SimID." <<  ConfigShared::SimID << "\n";
	}
	cout << endl;
	return true;
}


void trianglehull_and_iondistance( const int r, const int nr )
{
	//allocate sequential transcoder instance
	double tic = MPI_Wtime();

	surfacerHdl* surfacer = NULL;

	int localhealth = 1;
	int globalhealth = nr;
	try {
		surfacer = new surfacerHdl;
		surfacer->set_myrank(r);
		surfacer->set_nranks(nr);
		surfacer->init_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Unable to allocate a sequential surfacerHdl instance" << "\n"; return;
	}

	//do we have all processes on board?
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete surfacer; surfacer = NULL; return;
	}

	//we have all on board, read reconstruction
	//MK::we could have all processes loading from the same file, or have only MASTER do so and broadcast
	//we opt for the latter strategy
	//##MK::should not be a problem because initialization is orders of magnitude cheaper than actual calculations...
	if ( surfacer->get_myrank() == MASTER ) {
		if ( surfacer->read_reconxyz_from_apth5() == true ) {
			cout << "MASTER read successfully all ion coordinates of the specimen in reconstruction space" << "\n";
		}
		else {
			cerr << "MASTER was unable to read a specimen in reconstruction space!" << "\n"; localhealth = 0;
		}
		//else {} ##no slaves
		//##MK::strictly speaking not necessary, but second order issue for about 80 processes as on TALOS...?
		//MPI_Barrier( MPI_COMM_WORLD );
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	surfacer->surf_tictoc.prof_elpsdtime_and_mem( "ReadMaterialsData", APT_XX, APT_IS_SEQ, mm, tic, toc );
	tic = MPI_Wtime();

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that all data have arrived!" << "\n";
		delete surfacer; surfacer = NULL; return;
	}

	if ( surfacer->broadcast_reconxyz() == true ) {
		cout << "Rank " << r << " participated successfully in broadcast_reconxyz" << "\n";
	}
	else {
		cerr << "Rank " << r << " spotted an error during participation in broadcast_reconxyz" << "\n"; localhealth = 0;
	}

	//assure that now all processes have the same pieces of information
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all required data are consistent across the processes!" << "\n";
		delete surfacer; surfacer = NULL; return;
	}

	toc = MPI_Wtime();
	mm = memsnapshot();
	surfacer->surf_tictoc.prof_elpsdtime_and_mem( "BroadcastMaterialsData", APT_XX, APT_IS_SEQ, mm, tic, toc );

	//we opt here for a model were each process computes the same information running the same deterministic algorithm
	//##MK::in the future we could also distribute the computation of the alpha-shape but for now this is too involved
	//to implement compared to the time savings it brings about, pruning is so efficient that even for 2 billion point cloud
	//hull computation sequentially is less than 1h

	if ( surfacer->spatial_decomposition() == true ) {
		cout << "Rank " << r << " successfully decomposed the dataset spatially" << "\n";
	}
	else {
		cerr << "Rank " << r << " was unable to spatially decompose the dataset!" << "\n"; localhealth = 0;
	}

	if ( surfacer->prune_ions() == true ) {
		cout << "Rank " << r << " successfully pruned ions" << "\n";
	}
	else {
		cerr << "Rank " << r << " was unable to prune ions!" << "\n"; localhealth = 0;
	}

	if ( surfacer->build_triangle_hull() == true ) {
		cout << "Rank " << r << " successfully computed a triangle hull to the exterior dataset" << "\n";
	}
	else {
		cerr << "Rank " << r << " was unable to compute a triangle hull!" << "\n"; localhealth = 0;
	}

	tic = MPI_Wtime();

	//are all processes still the same and on board?
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized not all processes consistent after build_triangle_hull!" << "\n";
		delete surfacer; surfacer = NULL; return;
	}

	//check that the triangle hull is (at least in terms of number of triangles) the same across all nodes
	//broadcast size of master, all others check against master
	int ntriangles_master = 0;
	int ntriangles_myself = 0; //only significant at non root
	if ( surfacer->tr.tipsurface.size() > 0 && surfacer->tr.tipsurface.size() < static_cast<size_t>(INT32MX) ) {
		ntriangles_myself = static_cast<int>(surfacer->tr.tipsurface.size());
	}
	if ( surfacer->get_myrank() == MASTER ) {
		ntriangles_master = ntriangles_myself;
	}
	MPI_Bcast( &ntriangles_master, 1, MPI_INT, MASTER, MPI_COMM_WORLD );
	if ( ntriangles_master < 1 || ntriangles_master != ntriangles_myself ) {
		cerr << "Rank " << r << " detected an inconsistence in the triangle hulls across MPI_COMM_WORLD!" << "\n";
		delete surfacer; surfacer = NULL; return;
	}

	//sequential HDF5!, only MASTER writes results to file
	if ( surfacer->get_myrank() == MASTER ) {

		if ( surfacer->init_target_file() == true ) {
			cout << "Successfully initialized APTH5 results file" << "\n";

			if ( surfacer->write_environment_and_settings() == true ) {
				cout << "Rank " << MASTER << " environment and settings written!" << "\n";
			}
			else {
				cerr << "Rank " << MASTER << " writing environment and settings failed!" << "\n"; localhealth = 0;
			}

			if ( surfacer->write_voxelized_to_apth5() == true ) {
				cout << "Successfully wrote voxelization information to APTH5 file" << "\n";

				if ( surfacer->write_triangles_to_apth5() == true ) {
					cout << "Successfully wrote all triangles to an APTH5 file" << "\n";
				}
				else {
					cerr << "Unable to write all triangles to the APTH5 file" << "\n"; localhealth = 0;
				}
			}
			else {
				cerr << "Unable to write voxelization information to APTH5 file" << "\n"; localhealth = 0;
			}
		}
		else {
			cerr << "Unable to initialize results APTH5 file" << "\n"; localhealth = 0;
		}
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized not all processes consistent after build_triangle_hull and I/O of it!" << "\n";
		delete surfacer; surfacer = NULL; return;
	}

	//set a barrier here, strictyly speaking not necessary
	MPI_Barrier( MPI_COMM_WORLD );

	toc = MPI_Wtime();
	mm = surfacer->surf_tictoc.get_memoryconsumption();
	surfacer->surf_tictoc.prof_elpsdtime_and_mem( "WriteTriangleHullToHDF5", APT_XX, APT_IS_SEQ, mm, tic, toc );

	//compute distances to this triangle hull
	if ( ConfigSurfacer::DistancingMode != DISTANCING_NOTHING ) {

		//if ( ConfigSurfacer::DistancingMode == DISTANCING_ANALYTICAL_COMPLETE ) {

		if ( surfacer->compute_coarse_distances() == true ) {
			cout << "Rank " << r << " successfully computed coarse analytical distances!" << "\n";
		}
		else {
			cerr << "Rank " << r << " failed computing coarse analytical distances!" << "\n"; localhealth = 0;
		}

		globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != nr ) {
			cerr << "Rank " << r << " has recognized not all processes were able to compute analytical distances!" << "\n";
			delete surfacer; surfacer = NULL; return;
		}

		if ( r == MASTER ) {
			if ( surfacer->write_cdistances_to_apth5() == true ) {
				cout << "Rank " << r << " succeeded writing coarse distances" << "\n";
			}
			else {
				cerr << "Rank " << r << " failed writing coarse distances!" << "\n";
			}
		}

		//}

		if ( surfacer->compute_distances2() == true ) {
			cout << "Rank " << r << " successfully computed distances of ions to the triangle hull" << "\n";
		}
		else {
			cerr << "Rank " << r << " was unable to compute distances of ions to the triangle hull!" << "\n"; localhealth = 0;
		}

		tic = MPI_Wtime();
		MPI_Barrier( MPI_COMM_WORLD );

		if ( surfacer->communicate_distances2() == true ) {
			cout << "Rank " << r << " participated in communicating distance information" << "\n";
		}
		else {
			cerr << "Rank " << r << " failed communicating distance information!" << "\n"; localhealth = 0;
		}

		globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != nr ) {
			cerr << "Rank " << r << " has recognized not all processes consistent after build_triangle_hull and I/O of it!" << "\n";
			delete surfacer; surfacer = NULL; return;
		}

		toc = MPI_Wtime();
		mm = memsnapshot();
		surfacer->surf_tictoc.prof_elpsdtime_and_mem( "CommunicateDistances", APT_XX, APT_IS_PAR, mm, tic, toc );

		if ( surfacer->get_myrank() == MASTER ) {
			if ( surfacer->write_distances_to_apth5() == true ) {
				cout << "Successfully wrote all distances to the APTH5 file" << "\n";
			}
			else {
				cerr << "Unable to write distances to the APTH5 file" << "\n";
			}
		}
	}

	surfacer->surf_tictoc.spit_profiling( "Surfacer", ConfigShared::SimID, surfacer->get_myrank() );
	//release resources
	delete surfacer; surfacer = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();

	hello();
//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	//helloworld( argc, argv );

	if ( init( argc, argv ) == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks!" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else {
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}

//EXECUTE SPECIFIC TASK
	trianglehull_and_iondistance( r, nr );

//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	cout << "Rank " << r << " left MPI_COMM_WORLD deconstructed" << "\n";

	double toc = omp_get_wtime();
	cout << "paraprobe-surfacer took " << (toc-tic) << " seconds wall-clock time in total" << endl;

	return 0;
}
