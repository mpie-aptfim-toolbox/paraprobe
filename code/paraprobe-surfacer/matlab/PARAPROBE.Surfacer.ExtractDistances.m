% PARAPROBE.Araullo.CreateGeodesicSphere.40962.m
% Matthew Kasemer, Markus K\"uhbach, 2019
tic
    clear;
    clc;
    format long;
    
% load dat file from Matthew and translate into H5 format
    h5fn = 'Y:\GITHUB\MPIE_APTFIM_TOOLBOX\paraprobe\code\paraprobe-surfacer\run\PARAPROBE.Surfacer.SimID.0.apth5';
    dsnm = '/SurfaceRecon/Ion2Surface/Results/Distance';
    DAT = h5read(h5fn, dsnm);
    max(DAT(1,:));
    N = length(DAT(1,:));
    CDF(1,:) = sort(DAT(1,:));
    CDF(2,:) = (1:1:N)./N.*100.0;
    plot(CDF(1,:),CDF(2,:),'.');
    
 %% safe Matlab object and H5 file
    save('PARAPROBE.Araullo.CreateGeodesicSphere.40962.mat','-v7.3');
    h5fn = 'H:\BIGMAX_RELATED\MPIE-APTFIM-TOOLBOX\paraprobe\code\paraprobe-araullo\matlab\PARAPROBE.Araullo.GeodesicSphere40962.h5';
    dsnm = '/ElevationAzimuth';
    h5create(h5fn, dsnm, size(DAT));
    h5write(h5fn, dsnm, DAT);
toc
