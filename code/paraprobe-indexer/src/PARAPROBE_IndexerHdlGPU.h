//##MK::GPLV3

#ifndef __PARAPROBE_INDEXER_HDLGPU_H__
#define __PARAPROBE_INDEXER_HDLGPU_H__

#include "PARAPROBE_IndexerTicToc.h"

//MK::(de/)activate GPUs for easier development purposes, easier because will not force to have MPI+OMP+OPENACC capable compiler
//#define UTILIZE_GPUS
//##MK::currently the GPU support is switched off, it is highly developmental, not properly tested and only for the projections

#ifdef UTILIZE_GPUS

//using OpenACC
#include <openacc.h>
#include <accelmath.h>

//using CUDA
//##MK::

#endif


#endif
