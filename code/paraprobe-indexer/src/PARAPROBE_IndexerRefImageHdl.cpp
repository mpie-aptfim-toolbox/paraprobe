//##MK::GPLV3

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_IndexerRefImageHdl.h"

ostream& operator << (ostream& in, RefImageIO const & val) {
	in << "RefName " << val.refname << "\n";
	in << "ImageName " << val.imgname << "\n";
	in << "UseToIndex " << val.usetoidx << "\n";
	in << "RefOri " << val.refori << "\n";
	return in;
}

refImageCombiHdl::refImageCombiHdl()
{
}


refImageCombiHdl::~refImageCombiHdl()
{
}


bool refImageCombiHdl::load_refimage_definitions( string xmlfn )
{
	//cout << "Importing refimage combinations..." << "\n";
	ifstream file( xmlfn );
	if ( file.fail() ) {
		cerr << "Unable to locate input file " << xmlfn << "\n"; return false;
	}
	else {
		cout << "Opening input file " << xmlfn << "\n";
	}

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigIndexer")) {
		cerr << "Undefined parameters file!" << "\n"; return false;
	}

	xml_node<>* comb_node = rootNode->first_node("InputfileRefImages");
	int i = 0;
	for (xml_node<> * entry_node = comb_node->first_node("entry"); entry_node; entry_node = entry_node->next_sibling() ) {
		string rnm = entry_node->first_attribute("refname")->value();
		string dnm = entry_node->first_attribute("imagename")->value();
		string uix = entry_node->first_attribute("usetoindex")->value();
		string onm = entry_node->first_attribute("refori")->value();

		if ( rnm != "" && dnm != "" && uix != "" && onm != "" ) {
			this->icombis.push_back( RefImageIO( rnm, dnm, uix, onm ) );
cout << this->icombis.back() << "\n";

			string ky = "InputRefs" + to_string(i); i++;
			string vl = "Refname;" + rnm + ";Imagename;" + dnm + ";UseToIndex;" + uix + ";RefOri;" + onm;
			this->iifo.push_back( pparm( ky, vl, "", "" ) );
			/*
			//process combinations
			this->combinations.push_back( RefImage( rnm, dnm, onm ) );
cout << this->combinations.back().ori << "\n";
			*/

			//loading of intensity values happens later
		}
		else {
			cout << "WARNING attempt to pass an inacceptable InputfileRefImages all text field values are empty!" << "\n"; return false;
		}
	}

	return true;
}
