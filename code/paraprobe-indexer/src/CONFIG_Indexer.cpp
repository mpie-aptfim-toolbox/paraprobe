//##MK::GPLV3

#include "CONFIG_Indexer.h"

ANALYSIS_MODE ConfigIndexer::AnalysisMode = PERFORM_INDEXING;
string ConfigIndexer::InputfileAraullo = "";
string ConfigIndexer::OrientationRefImages = "0.0;0.0;0.0"; //identity Bunge-Euler
string ConfigIndexer::InputfileTestOris = "";

unsigned int ConfigIndexer::NumberOfS2Directions = 0;
unsigned int ConfigIndexer::IOUpToKthClosest = 1;
apt_real ConfigIndexer::IOSymmRedDisoriAngle = DEGREE2RADIANT(0.5);
bool ConfigIndexer::IOSolutionQuality = false;
bool ConfigIndexer::IOSolutionDisori = false;
bool ConfigIndexer::IODisoriMatrix = false;
bool ConfigIndexer::IOSymmReduction = false;
apt_real ConfigIndexer::IOSymmRedMaxDisoriAngle = DEGREE2RADIANT(0.5);
unsigned int ConfigIndexer::NumberOfSO3Candidates = 0;
unsigned int ConfigIndexer::NumberOfPhaseCandidates = 0;
unsigned int ConfigIndexer::NumberOfHighestPeaks = 0;

int ConfigIndexer::GPUsPerNode = 0;
int ConfigIndexer::GPUWorkload = 10; //##MK::almost sure not optimal but sweep spot which number is optimal is case dependent, therefore here the possibility to do systematic studies

size_t ConfigIndexer::CachePerIndexingEpoch = 0; //in GB

bool ConfigIndexer::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.Input.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigIndexer")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;

	//WHAT TYPE OF ANALYSIS TO DO
	mode = read_xml_attribute_uint32( rootNode, "AnalysisMode" );
	switch (mode)
	{
		case EVALUATE_DISORIENTATIONS:
			AnalysisMode = EVALUATE_DISORIENTATIONS; break;
		case TEST_DISORIENTATIONS:
			AnalysisMode = TEST_DISORIENTATIONS; break;
		case PERFORM_INDEXING:
			AnalysisMode = PERFORM_INDEXING; break;
		default:
			AnalysisMode = NOTHING;
	}
	InputfileAraullo = read_xml_attribute_string( rootNode, "InputfileAraullo" );
	InputfileTestOris = read_xml_attribute_string( rootNode, "InputfileTestOris" );

	//NumberOfS2Directions will be read in-place while parsing InputfileAraullo
	
	if ( AnalysisMode == PERFORM_INDEXING ) {
		OrientationRefImages = read_xml_attribute_string( rootNode, "OrientationRefImages" );
		IOUpToKthClosest = read_xml_attribute_uint32( rootNode, "IOUpToKthClosest" );
		IOSymmRedDisoriAngle = DEGREE2RADIANT(read_xml_attribute_float( rootNode, "IOSymmRedDisoriAngle" ));
		IOSolutionQuality = read_xml_attribute_bool( rootNode, "IOSolutionQuality" );
		IOSolutionDisori = read_xml_attribute_bool( rootNode, "IOSolutionDisori" );
		IODisoriMatrix = read_xml_attribute_bool( rootNode, "IODisoriMatrix" );
		IOSymmReduction = read_xml_attribute_bool( rootNode, "IOSymmReduction" );
		
		//IOSymmRedMaxDisoriAngle is a global parameter
		//NumberOfSO3Candidates will be set when parsing InputfileTestCands
		//NumberOfPhaseCandidates will be set when parsing InputfileAraullo
		NumberOfHighestPeaks = read_xml_attribute_uint32( rootNode, "IndexSoManyHighestPeaks" );
	}
	
	GPUsPerNode = read_xml_attribute_int32( rootNode, "GPUsPerComputingNode" );
	GPUWorkload = read_xml_attribute_int32( rootNode, "GPUWorkload" );
	if ( GPUWorkload < 0 || GPUWorkload > 10000 ) {
		cout << "WARNING: The GPU workload has to be at least 1, the user input was errorneous, so I resetted to 10!" << "\n";
		GPUWorkload = 10;
	}
	
	CachePerIndexingEpoch = read_xml_attribute_uint32( rootNode, "CachePerIndexingEpoch" );

	return true;
}


bool ConfigIndexer::checkUserInput()
{
	cout << "ConfigIndexer::" << "\n";
	switch(AnalysisMode)
	{
		case EVALUATE_DISORIENTATIONS:
			cout << "We evaluate the disorientation of all TestOris against one another." << "\n";
			cout << "We report for which combinations the disorientation angle is below " << IOSymmRedMaxDisoriAngle << " radiants" << "\n";
			break;
		case TEST_DISORIENTATIONS:
			cout << "We TEST_DISORIENTATIONS which is a DEBUG functionality!" << "\n";
		case PERFORM_INDEXING:
			cout << "We attempt an indexing of the APT specimen using the results from Araullo-Peters analysis." << "\n";
			break;
		default:
			cerr << "Unknown AnalysisMode chosen" << "\n";	return false;
	}
	
	if ( AnalysisMode == PERFORM_INDEXING ) {
		cout << "OrientationRefImages " << OrientationRefImages << "\n";
		if ( IOUpToKthClosest < 1 ) {
			cerr << "IOUpToKthClosest has to be a positive integer at least as large as 1!" << "\n"; return false;
		}
		cout << "IOUpToKthClosest " << IOUpToKthClosest << "\n";
		if ( IOSymmRedDisoriAngle < EPSILON ) {
			cerr << "IOSymmRedDisoriAngle has to be positive and finite!" << "\n"; return false;
		}
		if ( IOSymmRedDisoriAngle > IOSymmRedMaxDisoriAngle ) {
			cerr << "WARNING::IOSymmRedDisoriAngle was reset to IOSymmRedMaxDisoriAngle which is " << IOSymmRedMaxDisoriAngle << " radiants!" << "\n";
		}
		if ( NumberOfHighestPeaks < 1 ) {
			cerr << "NumberofHighestPeaks must be positive and at least one!" << "\n"; return false;
		}
		cout << "IOSymmRedDisoriAngle " << IOSymmRedDisoriAngle << "\n";
		cout << "IOSolutionQuality " << IOSolutionQuality << "\n";
		cout << "IOSolutionDisori " << IOSolutionDisori << "\n";
		cout << "IODisoriMatrix " << IODisoriMatrix << "\n";
		cout << "IOSymmReduction " << IOSymmReduction << "\n";
		cout << "NumberOfHighestPeaks " << NumberOfHighestPeaks << "\n";
	}
	cout << "IOSymmRedMaxDisoriAngle " << IOSymmRedMaxDisoriAngle << "\n";
	cout << "GPUsPerComputingNode " << GPUsPerNode << "\n";
	if ( GPUWorkload < 1 ) {
		cerr << "GPUWorkloadFactor needs to be integer and at least 1!" << "\n";
		cerr << "To delegate all work to CPUs instead set GPUsPerComputingNode to 0!" << "\n"; return false;
	}
	cout << "GPUWorkloadFactor " << GPUWorkload << "\n";
	
	profiler tmptictoc;
	size_t memoffered = tmptictoc.get_memory_max_on_node();
	size_t memneeded = GIGABYTE2BYTE(CachePerIndexingEpoch);
	if ( memneeded > (memoffered / 2) ) { //tictoc reports in Bytes, assume two processes per node
		//does not take into account fragmentation and how many process are running on the computing node
		cerr << "The computing node on which this process is running offers " << memoffered << " Bytes, assuming 2x MPI processes run " << memneeded << " Bytes cache needed, too much!" << "\n";
		return false;
	}
	cout << "The computing node on which this process is running offers " << memoffered << " Bytes, assuming 2x MPI processes run " << memneeded << " Bytes cache needed, fits" << "\n";
	cout << "CachePerIndexingEpoch " << CachePerIndexingEpoch << " GB" << "\n";

	cout << "InputfileAraullo read from " << InputfileAraullo << "\n";
	cout << "InputfileTestOris read from " << InputfileTestOris << "\n";
	
	cout << "\n";
	return true;
}


void ConfigIndexer::reportSettings( vector<pparm> & res )
{
	res.push_back( pparm( "AnalysisMode", sizet2str(AnalysisMode), "", "which type of analysis to do" ) );
	res.push_back( pparm( "InputfileAraullo", InputfileAraullo, "", "pre-computed signatures to be indexed" ) );
	res.push_back( pparm( "OrientationRefImages", OrientationRefImages, "", "pre-computed reference signature to be used for indexing" ) );
	res.push_back( pparm( "InputfileTestOris", InputfileTestOris, "", "pre-computed set of rotations to create rotated references" ) );

	res.push_back( pparm( "NumberOfS2Directions", uint2str(NumberOfS2Directions), "1", "how many elevation and azimuth pairs in the signatures" ) );

	res.push_back( pparm( "IOUpToKthClosest", uint2str(IOUpToKthClosest), "1", "up to which solution ID to report for each signature" ) );
	res.push_back( pparm( "IOSymmRedDisoriAngle", real2str(IOSymmRedDisoriAngle), "degree", "currently not used" ) );
	res.push_back( pparm( "IOSymmRedMaxDisoriAngle", real2str(IOSymmRedMaxDisoriAngle), "degree", "disorientation angle threshold below which to report computed disorientation" ) );

	res.push_back( pparm( "IOSolutionQuality", sizet2str(IOSolutionQuality), "", "currently not used" ) );
	res.push_back( pparm( "IOSolutionDisori", sizet2str(IOSolutionDisori), "", "whether or not to report disorientation angle between measured and ground truth" ) );
	res.push_back( pparm( "IODisoriMatrix", sizet2str(IODisoriMatrix), "", "currently  not used" ) );
	res.push_back( pparm( "IOSymmReduction", sizet2str(IOSymmReduction), "", "currently not used" ) );

	res.push_back( pparm( "NumberOfSO3Candidates", uint2str(NumberOfSO3Candidates), "1", "how many rotations to test" ) );
	res.push_back( pparm( "NumberOfPhaseCandidates", uint2str(NumberOfPhaseCandidates), "1", "how many phases to test" ) );
	res.push_back( pparm( "NumberOfHighestCandidates", uint2str(NumberOfHighestPeaks), "1", "how many of the highest intensity values to take" ) );

	res.push_back( pparm( "GPUsPerNode", uint2str(GPUsPerNode), "", "currently not used" ) );
	res.push_back( pparm( "GPUWorkload", uint2str(GPUWorkload), "", "currently not used" ) );

	res.push_back( pparm( "CachePerIndexingEpoch", sizet2str(CachePerIndexingEpoch), "GB", "how many signatures to load in an epoch before writing results back to file" ) );
}


