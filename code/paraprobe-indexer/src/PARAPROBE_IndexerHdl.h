//##MK::GPLV3

#ifndef __PARAPROBE_INDEXER_HDL_H__
#define __PARAPROBE_INDEXER_HDL_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_IndexerRefImageHdl.h"


class indexerHdl
{
	//process-level class which implements the worker instance which performs the indexing
	//using OpenMP and OpenACC i.e. CPU and GPUs
	//specimens result are written to a specific HDF5 files

public:
	indexerHdl();
	~indexerHdl();
	
	bool read_refimage_defs_from_xml( string xmlfn );
	bool read_testoris_from_h5();
	bool read_directions_from_h5();
	bool read_reference_templates_from_h5();
	bool read_materialpoint_ids();


	bool broadcast_testoris();
	bool broadcast_directions();
	bool broadcast_reference_templates();
	bool broadcast_matpoints();

	bool init_target_file();
	bool write_environment_and_settings();
	bool distribute_matpoints_on_processes();
	void directions_generate_kdtree();
	void phases_identify_peaks();
	bool directions_evaluate_rotations();

	//unsigned int plan_rawdata_import();

	void load_matpoints_araullo();
	void local_indexing_epoch();
	void write_solutions_to_h5();
	

	void evaluate_disorientations_cpu();
	void test_disorientations();

#ifdef UTILIZE_GPUS
	void evaluate_disorientations_cpugpu();
#endif


	/*
	void spatial_decomposition();
	void define_matpoint_volume_grid();

	gpu_cpu_max_workload plan_max_per_epoch( const int ngpu, const int nthr, const int nmp );
	gpu_cpu_now_workload plan_now_per_epoch( const int ngpu, const int nthr, const int nmp, const int mp, const gpu_cpu_max_workload mxl );
	//void generate_debug_roi( mt19937 & mydice, vector<dft_real> * out );
	void query_ions_within_roi( mp3d const & roicenter, vector<dft_real> * out );
	void debug_epoch_profiling( vector<epoch_log> const & in, vector<epoch_prof> const & timing );
	void execute_local_workpackage();
	void execute_local_workpackage2();
		
	bool init_target_file();
	bool write_materialpoints_to_apth5();
	bool collect_results_on_masterprocess();
	bool write_results_to_apth5();
	*/
	
	int get_myrank();
	int get_nranks();
	void set_myrank( const int rr );
	void set_nranks( const int nn );
	void init_mpidatatypes();

	/*
	decompositor sp;
	volsampler mp;
	vector<MPI_Fourier_ROI> mproi;					//only significant for MASTER to organize results writing
	vector<MPI_Fourier_HKLValue> mpres;				//only significant for MASTER to organize results writing
	*/
	
	h5Hdl inputTestOrisH5Hdl;
	h5Hdl inputAraulloH5Hdl;
	h5Hdl inputRefImgsH5Hdl;
	indexer_h5 debugh5Hdl;
	//indexer_xdmf debugxdmf;

	vector<squat> testoris;					//test rotations, interpreted first as active rotations 
											//used to rotate #### measured against reference
	
	vector<p3d> directions;					//cartesian coordinates of points on the unit sphere which specify the directions along which projections during Araullo Peters we probed
	vector<p3dm1> s2g_directions;			//the actual sorted kdtree of direction points with (geodesic grid) vertex IDs as the mark value
	kd_tree* s2g_tree;

	refImageCombiHdl phlib;
	//vector<phaseCandidate> candidates;		//the phases the same across all ranks which we use to index our araullo results against

	//temporary cache for paraprobe-araullo results that we want to use for indexing
	vector<unsigned int> my_mpid;
	map<size_t,size_t> my_mpid_faulty;
	vector<vector<float>*> my_sgnl_exp;		//vector of 2d implicit my_mpid.size() * ConfigIndexer::NumberOfS2Directions, directions changes fastest
											//one vector<float>* per phase
	vector<vector<oim_res>*> my_mpid_res;	//vector of 2d implicit my_mpid.size() * ConfigIndexer::IOUpToKthClosest in sorted in ascending order, kth closest changes fastest!
	vector<vector<squat>*> my_mpid_disori;	//like my_mpid_res in organization but holding optional disorientation quaternions

	vector<unsigned int> mpid;				//material point IDs for which there are results in inputAraulloH5Hdl
	vector<int> mp2rk;						//mapping of material points => to ranks
	//map<int,int> mp2rk;

	/*
	vector<p3d> xyz;
	vector<p3dm1> xyz_ityp;					//##MK::temporarily until proper ion type handling implemented
	vector<tri3d> triangle_hull;
	vector<apt_real> dist2hull;
	*/
	//rangeTable rng;
	/*
	vector<mp3d> myworkload;				//the material points of the process
	vector<hklval> myhklval;				//##MK::DEBUG for now the actual data of interest for science
	*/
	
	profiler indexer_tictoc;

private:
	//MPI related
	int myrank;								//my MPI ID in the MPI_COMM_WORLD
	int nranks;

	MPI_Datatype MPI_Quaternion_Type;
	MPI_Datatype MPI_ElevAzimXYZ_Type;
};


#endif

