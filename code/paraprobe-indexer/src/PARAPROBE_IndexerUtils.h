//##MK::GPLV3

#ifndef __PARAPROBE_INDEXER_UTILS_H__
#define __PARAPROBE_INDEXER_UTILS_H__

//#include "PARAPROBE_VolumeSampler.h"
#include "../../paraprobe-utils/src/PARAPROBE_VolumeSampler.h"

#include "PARAPROBE_IndexerHdlGPU.h"

struct DisoriIJ
{
	apt_real th;
	unsigned int i;
	unsigned int j;
	DisoriIJ() : th(0.f), i(UINT32MX), j(UINT32MX) {}
	DisoriIJ( const apt_real _theta, const unsigned int _i, const unsigned int _j) :
		th(_theta), i(_i), j(_j) {}
};


//implementation of the FE mesh rotating
typedef unsigned short 				oim_uint; //precision is 2^16 suffices for cases up to 1*degree of the equispacedS2Grid that demands 42000 disjoint S2 grid points
typedef float 						oim_float;
#define OIMUINTMX					numeric_limits<oim_uint>::max()
#define OIMFMX						numeric_limits<oim_float>::max()

struct oim_filter
{
	oim_float val;
	oim_uint idx;
	oim_uint padding;
	oim_filter() :
		val(OIMFMX), idx(OIMUINTMX), padding(OIMUINTMX) {}
	oim_filter( const oim_float _val, const oim_uint _idx ) :
		val(_val), idx(_idx), padding(OIMUINTMX) {}
};


struct oim_res
{
	oim_float val;
	unsigned int idx;
	oim_res() :
		val(OIMFMX), idx(UNKNOWNTYPE) {}
	oim_res(const oim_float _val, const unsigned int _idx) :
		val(_val), idx(_idx) {}
};


bool SortOIMFilterAscValue( oim_filter & first, oim_filter & second );

bool SortOIMResultAscValue( oim_res & first, oim_res & second );


class phaseCandidate
{
public:
	phaseCandidate();
	~phaseCandidate();

	unsigned int candid;
	squat refori;
	//##MK::load metadata

	vector<apt_real> intensities;
	vector<oim_filter> highest;			//n-highest intensity values with value and IDs on intensities
	vector<oim_uint> s2g_exp2ref;		//implicit two dimensional array ConfigIndexer::NumberSO3*ConfigIndexer::NumberOfHighestPeaks, peaks changes fastest
	//tells me which intensity value to take from the unrotated measured (from experiment dataset) intensity value and compare to the r*ConfigIndexer::NumberOfPeaks+highest value


	//ConfigIndexer::NumberOfS2Directions encodes in the same order as the material points! the S2 image intensity values
	//on the sphere against which we compared the measured image intensity values
};


/*
class mpResult
{
public:
	mpResult();
	~mpResult();
	
	unsigned int mpid;
	vector<unsigned int> my_mpid;
	map<size_t,size_t> my_mpid_faulty;
	vector<vector<float>*> my_sgnl_exp;		//vector of 2d implicit my_mpid.size() * ConfigIndexer::NumberOfS2Directions, directions changes fastest
											//one vector<float>* per phase
	vector<vector<oim_res>*> my_mpid_res;	//vector of 2d implicit my_mpid.size() * ConfigIndexer::IOUpToKthClosest in sorted in ascending order, kth closest changes fastest!
	vector<vector<squat>*> my_mpid_disori;	//like my_mpid_res in organization but holding optional disorientation quaternions

	vector<unsigned int> mpid;				//material point IDs for which there are results in inputAraulloH5Hdl
	vector<int> mp2rk;						//mapping of material points => to ranks
};
*/


//storing of timing data
struct tictoc
{
	double dt_indexing;
	double dt_postprocess;
	unsigned int mpid;
	unsigned int phcid;
	int threadid;
	tictoc() : dt_indexing(0.0), dt_postprocess(0.0), mpid(UINT32MX), phcid(UINT32MX), threadid(INT32MX) {}
	tictoc( const double _dtidx, const double _dtpost, const unsigned int _mpid, const unsigned int _phcid, const int _thrid ) :
		dt_indexing(_dtidx), dt_postprocess(_dtpost), mpid(_mpid), phcid(_phcid), threadid(_thrid) {}
};


#endif
