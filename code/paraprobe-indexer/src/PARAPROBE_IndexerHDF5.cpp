//##MK::GPLV3

#include "PARAPROBE_IndexerHDF5.h"
//https://www.learncpp.com/cpp-tutorial/114-constructors-and-initialization-of-derived-classes/


indexer_h5::indexer_h5()
{
}


indexer_h5::~indexer_h5()
{
}


int indexer_h5::create_indexer_apth5( const string h5fn )
{
	h5resultsfn = h5fn;
	fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );
	if ( status < 0 ) {
		cerr << h5fn << " file creation failed!" << "\n"; return WRAPPED_HDF5_FAILED;
	}
cout << h5fn << " " << status << "\n";

	//generate a PARAPROBE APTH5 HDF5 file
	//domain specific HDF5 keywords and data fields
	groupid = H5Gcreate2(fileid, PARAPROBE_IDXR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << PARAPROBE_IDXR << " group creation failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}
cout << "PARAPROBE_IDXR " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_IDXR_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << PARAPROBE_IDXR_META << " group creation failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << PARAPROBE_IDXR_META << " group creation failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}
cout << "PARAPROBE_IDXR_META " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_IDXR_META_HRDWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << PARAPROBE_IDXR_META_HRDWR << " group creation failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << PARAPROBE_IDXR_META_HRDWR << " group creation failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}
cout << "PARAPROBE_IDXR_META_HRDWR " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_IDXR_META_SFTWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << PARAPROBE_IDXR_META_SFTWR << " group creation failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << PARAPROBE_IDXR_META_SFTWR << " group creation failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}
cout << "PARAPROBE_IDXR_META_SFTWR " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_IDXR_META_ORI, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << PARAPROBE_IDXR_META_ORI << " group creation failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}
cout << "PARAPROBE_IDXR_META_ORI " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_IDXR_RES, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << PARAPROBE_IDXR_RES << " group creation failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}
cout << "PARAPROBE_IDXR_RES " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_IDXR_RES_SOL, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << PARAPROBE_IDXR_RES_SOL << " group creation failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}
cout << "PARAPROBE_IDXR_RES_SOL " << status << "\n";

	if ( ConfigIndexer::IOSolutionDisori == true ) {
		groupid = H5Gcreate2(fileid, PARAPROBE_IDXR_RES_DIS, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << PARAPROBE_IDXR_RES_DIS << " group creation failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}
cout << "PARAPROBE_IDXR_RES_DIS " << status << "\n";
	}

	//create sub-groups for all phases
	string fwslash = "/";
	string grpnm = "";
	for( unsigned int ph = 0; ph < ConfigIndexer::NumberOfPhaseCandidates; ph++ ) {
		grpnm = PARAPROBE_IDXR_RES_SOL + fwslash + "PH" + to_string(ph);
		groupid = H5Gcreate2(fileid, grpnm.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << grpnm << " group creation failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}
		cout << grpnm << " " << status << "\n";

		if ( ConfigIndexer::IOSolutionDisori == true ) {
			grpnm = PARAPROBE_IDXR_RES_DIS + fwslash + "PH" + to_string(ph);
			groupid = H5Gcreate2(fileid, grpnm.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
			if ( status < 0 ) {
				cerr << grpnm << " group creation failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
			}
			cout << grpnm << " " << status << "\n";
		}
	}

	//add environment and tool specific settings
/*
	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_HRDWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_HRDWR failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_HRDWR failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_HRDWR_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_HRDWR_META failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_HRDWR_META failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_HRDWR_META_KEYS, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_HRDWR_META_KEYS failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_HRDWR_META_KEYS failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_SFTWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_SFTWR failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARARPROBE_UTILS_SFTWR failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_SFTWR_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_SFTWR_META failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_SFTWR_META failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_SFTWR_META_KEYS, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_SFTWR_META_KEYS failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_SFTWR_META_KEYS failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}
*/

	status = H5Fclose(fileid);
cout << "Closing APTH5 file" << "\n";

	return WRAPPED_HDF5_SUCCESS;
}

