//##MK::GPLV3

#ifndef __PARAPROBE_INDEXER_REFIMAGEHDL_H__
#define __PARAPROBE_INDEXER_REFIMAGEHDL_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_IndexerUtils.h"

struct RefImageIO
{
	string refname;
	string imgname;
	string usetoidx;
	string refori;
	RefImageIO() : refname(""), imgname(""), usetoidx(""), refori("") {}
	RefImageIO( const string _refnm, const string _dsnm, const string _u2idx, const string _refg ) :
		refname(_refnm), imgname(_dsnm), usetoidx(_u2idx), refori(_refg) {}
};

ostream& operator << (ostream& in, RefImageIO const & val);


struct RefImage
{
	string refname;	//H5 file with the data
	string imgname; //dataset name
	bunge ori;
	
	RefImage() : refname(""), imgname(""), ori(bunge()) {}
	RefImage( const string _refnm, const string _dsnm, const string _refg ) :
		refname(_refnm), imgname(_dsnm), ori(bunge(_refg)) {}
};


class refImageCombiHdl
{
	//class which translates human-readable single/molecular ion type combination strings into the internal itype unsigned char
	//format with which internally all ions are analyzed
public:
	refImageCombiHdl();
	~refImageCombiHdl();

	bool load_refimage_definitions( string xmlfn );
	
	vector<RefImageIO> icombis;
	//vector<RefImage> combinations;

	vector<phaseCandidate> candidates;

	vector<pparm> iifo;
};


#endif
