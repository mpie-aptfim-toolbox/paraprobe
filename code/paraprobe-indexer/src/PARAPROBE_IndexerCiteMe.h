//##MK::GPLV3

#ifndef __PARAPROBE_INDEXER_CITEME_H__
#define __PARAPROBE_INDEXER_CITEME_H__

#include "CONFIG_Indexer.h"


class CiteMeIndexer
{
public:
	
	static vector<bibtex> Citations;
	
	static size_t cite();
};

#endif

