//##MK::GPLV3

#include "PARAPROBE_IndexerHdl.h"


indexerHdl::indexerHdl()
{
	myrank = MASTER;
	nranks = 1;
	s2g_tree = NULL;
}


indexerHdl::~indexerHdl()
{
	delete s2g_tree; s2g_tree = NULL;
	for( size_t ph = 0; ph < my_sgnl_exp.size(); ph++ ) {
		if ( my_sgnl_exp.at(ph) != NULL ) {
			delete my_sgnl_exp.at(ph);
		}
	}
	my_sgnl_exp = vector<vector<float>*>();

	for( size_t ph = 0; ph < my_mpid_res.size(); ph++ ) {
		if ( my_mpid_res.at(ph) != NULL ) {
			delete my_mpid_res.at(ph);
		}
	}
	my_mpid_res = vector<vector<oim_res>*>();

	for( size_t ph = 0; ph < my_mpid_disori.size(); ph++ ) {
		if ( my_mpid_disori.at(ph) != NULL ) {
			delete my_mpid_disori.at(ph);
		}
	}
	my_mpid_disori = vector<vector<squat>*>();
}


bool indexerHdl::read_refimage_defs_from_xml( string xmlfn )
{
	if ( phlib.load_refimage_definitions( xmlfn ) == false ) {
		cerr << "Rank " << get_myrank() << " loading of refImages form XML failed!" << "\n"; return false;
	}
	cout << "Rank " << get_myrank() << " phlib.icombis.size() " << phlib.icombis.size() << " refImages " << "\n";
	return true;
}


bool indexerHdl::read_testoris_from_h5()
{
	double tic = MPI_Wtime();

	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	int status = 0;

	if ( ConfigIndexer::InputfileTestOris.substr(ConfigIndexer::InputfileTestOris.length()-3) != ".h5" ) {
		cerr << "The InputfileTestOris has not the proper h5 file extension!" << "\n";
		return false;
	}

	//probe existence of quaternion array in the input file and get its dimensions
	inputTestOrisH5Hdl.h5resultsfn = ConfigIndexer::InputfileTestOris;
	if ( inputTestOrisH5Hdl.query_contiguous_matrix_dims( PARAPROBE_IDXR_META_ORI_QUAT, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the test rotations/orientations in InputfileTestOris" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_IDXR_META_ORI_QUAT is a " << offs.nrmax << " rows " << offs.ncmax << " matrix" << "\n";
	}

	//read the content and transfer to working array
	ifo = h5iometa( PARAPROBE_IDXR_META_ORI_QUAT, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = inputTestOrisH5Hdl.read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
cout << "PARAPROBE_IDXR_META_ORI_QUAT read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_IDXR_META_ORI_QUAT on InputfileTestOris failed" << "\n"; return false;
	}

	try {
		testoris.reserve( offs.nrows() );
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading test rotations/orientations!" << "\n";
	}
	for( size_t i = 0; i < f32.size(); i += 4 ) { //+4 because four quaternion components define a row of the matrix
		//##MK::currently we use equispaced SO3 grids from MTex 5.0.3
		//these however specify orientations with respect to a specific crystal symmetry not just rotations quaternions
		//furthermore the resulting quaternions have q0 values in the range (-1,1)
		//this is problematic wrt to conventions because q0 can be negative violating the definitions of
		//Rowenhorst, 2015, MSMSE, therefore we interpret all quaternions as passive orientation quaternions
		//and flip them if necessary into the northern hemisphere
		//if given a quaternion rotation we know that changing the sign for all! (q0,q1,q2,q3) quaternion components
		//does not change the rotation, hence we flip here the incoming data from MTex as well
		if ( f32[i+0] < 0.f ) { //flip required
			testoris.push_back( squat( -f32[i+0], -f32[i+1], -f32[i+2], -f32[i+3] ) );
		}
		else { //no flip required
			testoris.push_back( squat( +f32[i+0], f32[i+1], +f32[i+2], +f32[i+3] ) );
		}
	}
	f32 = vector<float>();

	ConfigIndexer::NumberOfSO3Candidates = testoris.size();

	//dont forget to set ConfigIndexer::NumberOfSO3Candidates once all values were broadcast across process team!

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	indexer_tictoc.prof_elpsdtime_and_mem( "ReadTestOrientationsFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool indexerHdl::read_directions_from_h5()
{
	double tic = MPI_Wtime();

	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	if ( ConfigIndexer::InputfileAraullo.substr(ConfigIndexer::InputfileAraullo.length()-3) != ".h5" ) {
		cerr << "The InputfileAraullo " << ConfigIndexer::InputfileAraullo << " has not the proper h5 file extension!" << "\n";
		return false;
	}

	//probe existence of elevation/azimuth data in the input file and get dimensions
	inputAraulloH5Hdl.h5resultsfn = ConfigIndexer::InputfileAraullo;
	if ( inputAraulloH5Hdl.query_contiguous_matrix_dims( PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ in the InputfileAraullo!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX ) {
		cerr << "PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ is either empty and/or has unexpected size!" << "\n"; return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = inputAraulloH5Hdl.read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ on InputfileAraullo failed!" << "\n"; return false;
	}
cout << "PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ read " << status << "\n";

	try {
		directions.reserve( offs.nrows() );
//MK::here point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ!" << "\n"; return false;
	}

	for( size_t i = 0; i < f32.size(); i += 3 ) {
		directions.push_back( p3d( f32.at(i+0), f32.at(i+1), f32.at(i+2) ) );
	}
	f32 = vector<float>();

	ConfigIndexer::NumberOfS2Directions = directions.size();

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	indexer_tictoc.prof_elpsdtime_and_mem( "ReadElevAzimGridXYZFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool indexerHdl::read_reference_templates_from_h5()
{
	double tic = MPI_Wtime();

	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string fwslash = "/";

	/*
	if ( ConfigIndexer::InputfileAraullo.substr(ConfigIndexer::InputfileAraullo.length()-3) != ".h5" ) {
		cerr << "The InputfileAraullo " << ConfigIndexer::InputfileAraullo << " has not the proper h5 file extension!" << "\n";
		return false;
	}
	*/

	//##MK::currently we require the following restrictions for the AraulloPeters acuqired data and the phases
	//AraulloPeters acquired:
	//need to have material point results with for all phases the same number of spherical image FE vertices
	//multiple phases are possible per material point but only material points with results for all phases ##MK for now are indexed
	//RefImages to index:
	//need to have for each phase the same number of spherical image FE vertices
	//FE vertex order needs to be the same order as used for Araullo, i.e. following the same elevation azimuth pattern !

	/*
	//probe number of PhaseCandidates
	inputAraulloH5Hdl.h5resultsfn = ConfigIndexer::InputfileAraullo;
	if ( inputAraulloH5Hdl.query_contiguous_matrix_dims( PARAPROBE_ARAULLO_META_PHCAND_N, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the PARAPROBE_ARAULLO_META_PHCAND_N in the InputfileAraullo!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_ARAULLO_META_PHCAND_N is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_ARAULLO_META_PHCAND_N_NCMAX ) {
		cerr << "PARAPROBE_ARAULLO_META_PHCAND_N is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_ARAULLO_META_PHCAND_N, offs.nrows(), offs.ncols() );
	vector<unsigned int> u32;
	status = inputAraulloH5Hdl.read_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_ARAULLO_META_PHCAND_N on InputfileAraullo failed!" << "\n"; return false;
	}
cout << "PARAPROBE_ARAULLO_META_PHCAND_N read " << status << "\n";

	unsigned int nphases = 0;
	if ( u32.size() < 1 ) {
		cerr << "Transfer of PARAPROBE_ARAULLO_META_PHCAND_N failed!" << "\n"; return false;
	}
	else {
		nphases = u32.at(0);
	}
	try {
		candidates = vector<phaseCandidate>( nphases, phaseCandidate() );
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during initializing phaseCandidates!" << "\n"; return false;
	}

	for( unsigned int i = 0; i < nphases; i++ ) {
		//##MK::PhaseNaming convention is expected as follows: PHi, PHi+1, ... all phases must be present in HDF5 file!
		string grpnm = PARAPROBE_ARAULLO_META_PHCAND + fwslash + "PH" + to_string(i);
		if ( inputAraulloH5Hdl.probe_group_existence( grpnm ) == true ) {
			cout << "Group " << grpnm << " exists!" << "\n";
		}
		else {
			cerr << "Group " << grpnm << " does not exist!" << "\n"; return false;
		}
	}
	*/

	//do the files mentioned under InputfileRefImages/refname exist
	for( auto rt = phlib.icombis.begin(); rt != phlib.icombis.end(); rt++ ) {
		//is the reference one likely a proper h5 file?
		if ( rt->refname.substr(rt->refname.length()-3) != ".h5" ) {
			cerr << rt->refname << " has not the proper h5 file extension!" << "\n";
			return false;
		}

		//do they have the datasetname fields?
		inputRefImgsH5Hdl.h5resultsfn = rt->refname;
		if ( inputRefImgsH5Hdl.probe_group_existence( rt->imgname ) == true ) {
			cout << "RefImage file " << rt->refname << " and specific dataset " << rt->imgname << " exists!" << "\n";
		}
		else {
			cerr << "RefImage file " << rt->refname << " and specific dataset " << rt->imgname << " does not exist!" << "\n";
			return false;
		}

		//do they have the same number of FE vertices?
		if ( inputRefImgsH5Hdl.query_contiguous_matrix_dims( rt->imgname, offs ) != WRAPPED_HDF5_SUCCESS ) {
			cerr << "Unable access and parse dimensions of " << rt->imgname << " in " << rt->refname << "\n";
			return false;
		}
		else {
			cout << rt->imgname << " is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
		}
		if ( offs.nrows() != ConfigIndexer::NumberOfS2Directions ) {
			cerr << rt->imgname << " has " << offs.nrows() << " FE mesh vertices but " << ConfigIndexer::NumberOfS2Directions << " expected!" << "\n";
			return false;
		}
		//##MK::we currently assume their order implicitly!

		//in above first part we queried which phases were used by paraprobe-araullo, now the user might have generated
		//the corresponding phaseCandidate image intensity value arrays with a different tool
		//this flexibility to be able to load / require to load from a second file is necessary because in general
		//executing a paraprobe-araullo run on a dataset and building a library of templates are separate tasks.
		//We just have to assure that the library created and the results parsed from the reference template H5 file match logically and content-wise!

		//##MK::open a second H5 file to read the actual intensity images

		//now load the reference intensity images and rotate them using the mapping defined by reverse_rotated_image
		/*inputRefImgsH5Hdl.h5resultsfn = ConfigIndexer::InputfileRefImages;
		for( unsigned int i = 0; i < nphases; i++ ) {
		//we need to find exactly the same named phase candidates, all of them

		//##MK::to identify them even stricter, #### add probing of configuration for IontypesID, IontypesWhat

		string grpnm = PARAPROBE_ARAULLO_META_PHCAND + fwslash + "PH" + to_string(i);
		if ( inputRefImgsH5Hdl.probe_group_existence( grpnm ) == true ) {
			cout << rt->imgname << " dataset does not exist in inputRefImgsH5Hdl!" << "\n";
		}
		else {
			cerr << "Group " << grpnm << " does not exist in inputRefImgsH5Hdl!" << "\n"; return false;
		}

		string dsnm = PARAPROBE_ARAULLO_META_PHCAND + fwslash + "PH" + to_string(i) + fwslash + PARAPROBE_ARAULLO_META_PHCAND_VAL;
		if ( inputRefImgsH5Hdl.query_contiguous_matrix_dims( dsnm, offs ) != WRAPPED_HDF5_SUCCESS ) {
			cerr << "Unable access and parse dimensions of " << dsnm << "!" << "\n"; return false;
		}
		else {
			cout << dsnm << " is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
		}

		if ( offs.nrows() < 1 || offs.nrows() != directions.size() || offs.ncols() != PARAPROBE_ARAULLO_META_PHCAND_VAL_NCMAX ) {
			cerr << dsnm << " is either empty, has other number of intensity values compared to NumberOfDirections or has unexpected size!" << "\n"; return false;
		}*/

		ifo = h5iometa( rt->imgname, offs.nrows(), offs.ncols() );
		vector<float> f32;
		status = inputRefImgsH5Hdl.read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "Reading " << rt->imgname << " on inputRefImgsH5Hdl failed!" << "\n"; return false;
		}
		cout << rt->imgname << " read " << status << "\n";

		//transfer values and initialize phaseCandidate
		try {
			phlib.candidates.push_back( phaseCandidate() );
		}
		catch (bad_alloc &croak) {
			cerr << "Allocation error during initializing phaseCandidates!" << "\n"; return false;
		}

		phaseCandidate & thisone = phlib.candidates.back(); //##MK::in the future maybe give the phaseCandidates better names, names at all instead of numbers bound to on [ 0, u32.at(0) )
		thisone.candid = rt - phlib.icombis.begin();

		//thisone.intensities is implicitly in order of directions but ref templates were generated
		//through probing a synthetic crystal in arbitrary orientation, hence we need to remap into unrotated configuration
		//i.e. transform the spherical image as if generated from an unrotated / crystal in identify orientation

		thisone.intensities = vector<apt_real>( directions.size(), 0.f );

		//MK::it is important to realize that this is the grid of directions along which the 1d SDM were measured in paraprobe-araullo
		//we consider this grid as unrotated, users typically generate refimages using this grid with paraprobe-araullo or a Matlab/Python solution
		//they do so by building a synthetic single crystal for each phase NECESSARILY OF THE SAME ORIENTATION!
		//and probe how the 1d SDM for these ideal lattices look like preliminary studies with this protocol showed that
		//if these synthetic crystals remain unrotated though, i.e. for cubic [001] parallel to z-axis of the tip/specimen
		//i.e. in identity orientation (1.0, 0.f, 0.f, 0.f) the resulting 1d SDM pattern is highly symmetric and possibly
		//shows very intense intensity banding along particular directions and generate intensity bands on the sphere
		//to cope with this one typically rotates the synthetic crystal into a off-symmetric orientation upon synthesis
		//this is done in paraprobe-synthetic the rotation applied is ##MK::currently parsed and assumed valid from rt->refori !
		//however, this renders the resulting refimage intensities at each vertex non matching with the unrotated vertices in directions
		//remember we rotate the reference images and keep the measured intensity image on the sphere fixed using the unrotated grid
		//hence applying the testoris as rotations in addition to the refimages would rotate them additionally
		//instead of from the identity orientation,
		//therefore we need to rotate the intensity images of the refimage back into the unrotated configuration i.e. rotate back by refori^-1
		//before applying the testorientations!

//##MK:explain clearer and more concisely!

		//then there is only one additional rotation required to bring the template and the measured grid into registration
		bunge refbunge = bunge( rt->refori ); //DEGREE2RADIANT(5.0), DEGREE2RADIANT(5.0), DEGREE2RADIANT(5.0) ); //##MK::here 5.0, 5.0, 5.0 because that was the tip orientation used to generate the library
cout << "OrientationRefImages Bunge " << refbunge << "\n";
		squat refquat = refbunge.eu2qu();
cout << "OrientationRefImages Quat " << refquat << "\n";

		//store this reference for future purpose e.g. to compare indexed solutions with reference
		thisone.refori = refquat;

		//squat inverse = refquat.invert();
//cout << "OrientationRefImages INV " << inverse << "\n";
		//t3x3 RR = inverse.qu2om();

		t3x3 RR = refquat.qu2om();
cout << "OrientationRefImages RR " << RR << "\n";
		real_m33 Rdet = RR.det();
cout << "det(RR) = " << Rdet << "\n";

		//build KDtree to remap directions vertex locations to closest in rotated configuration
		vector<p3d> directions_original;
		vector<unsigned int> directions_lbls;
		unsigned int lbl = 0;
		for( auto it = directions.begin(); it != directions.end(); it++, lbl++ ) {
			directions_original.push_back( *it );
			directions_lbls.push_back( lbl );
		}
		//vector<unsigned int> reverse_rotated_image = vector<unsigned int>( directions.size(), UINT32MX );
		vector<p3dm1> thistree; //the actual tree
		vector<size_t> permutations; //the permutations to better linearize the memory accesses when querying the tree
		kd_tree* tree = NULL;
		try {
			tree = new kd_tree; //this calls the default constructor
		}
		catch (bad_alloc &mecroak) {
			cerr << "Rank " << MASTER << " allocation failure for tree upon globally rotating direction grid!" << "\n";
			return false;
		}

		tree->build( directions_original, permutations, 16 );
cout << "Rank " << MASTER << " globally rotating direction grid tree has " << tree->nodes.size() << " nodes" << "\n";

		tree->pack_p3dm1( permutations, directions_original, directions_lbls, thistree );
		permutations = vector<size_t>();

		//use the tree to assign the interpolated image intensities in the mentality:
		//if i were to rotate an unrotated FE vertex by RR*x where would it end, given that the synthetic specimen was rotated like this
		for( size_t i = 0; i < directions.size(); i++ ) {
			//actively rotate unrotated reference FE mesh point into rotated configuration to find
			//which intensity value would be at the original reverse i.e unrotated position
			p3d s2o = directions.at(i);
			p3d s2r = p3d( 	RR.a11*s2o.x + RR.a12*s2o.y + RR.a13*s2o.z,
							RR.a21*s2o.x + RR.a22*s2o.y + RR.a23*s2o.z,
							RR.a31*s2o.x + RR.a32*s2o.y + RR.a33*s2o.z   );
			//find the corresponding closest matching vertex ID in the unrotated configuration of directions
			p3dm1 closest = tree->nearest_safe( s2r, thistree, F32MX, OIMUINTMX );
			if ( closest.m != static_cast<unsigned int>(OIMUINTMX) ) {
				//rotating forward directions and probe for f32 guarantes that every i gets a signal
				//in some cases this can be a duplicate owing to limited angular resolving capability or orientation/RR where the rotated vertex is numerically right in the center equally angularly distant from multiple vertices...

				thisone.intensities.at(i) = f32.at(closest.m);
			}
			else {
				cerr << "Unrecoverable error during reversing reference pattern which should not occur!" << "\n"; return false;
			}
		}
		directions_original = vector<p3d>();
		directions_lbls = vector<unsigned int>();
		thistree = vector<p3dm1>();
		delete tree; tree = NULL;

	} //load the next candidate

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	indexer_tictoc.prof_elpsdtime_and_mem( "ReadPhaseCandidatesFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool indexerHdl::read_materialpoint_ids()
{
	double tic = MPI_Wtime();

	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string fwslash = "/";

	if ( ConfigIndexer::InputfileAraullo.substr(ConfigIndexer::InputfileAraullo.length()-3) != ".h5" ) {
		cerr << "The InputfileAraullo " << ConfigIndexer::InputfileAraullo << " has not the proper h5 file extension!" << "\n";
		return false;
	}

	inputAraulloH5Hdl.h5resultsfn = ConfigIndexer::InputfileAraullo;

	//filter out for which material point SpecificPeak results exist for each of the icombis
	map<int,int> mp_withimg_forallph;
	for( auto rt = phlib.icombis.begin(); rt != phlib.icombis.end(); rt++ ) {
		string dsnm = PARAPROBE_ARAULLO_RES_MP_IOID + fwslash + rt->usetoidx;

		if ( inputAraulloH5Hdl.query_contiguous_matrix_dims( dsnm, offs ) != WRAPPED_HDF5_SUCCESS ) {
			cerr << "Unable access and parse dimensions of " << dsnm << "\n"; return false;
		}
		else {
			cout << dsnm << " is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
		}

		if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_ARAULLO_META_MP_ID_NCMAX ) {
			cerr << dsnm << " is either empty and/or has unexpected size!" << "\n"; return false;
		}

		vector<int> i32;
		//read the content and transfer to working array
		ifo = h5iometa( dsnm, offs.nrows(), offs.ncols() );
		status = inputAraulloH5Hdl.read_contiguous_matrix_i32le_hyperslab( ifo, offs, i32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_ARAULLO_META_MP_ID on InputfileAraullo failed!" << "\n"; return false;
		}
	cout << "PARAPROBE_ARAULLO_META_MP_ID read " << status << "\n";

		if ( rt == phlib.icombis.begin() ) {
			//fill the still empty list of material point ID that we want to index knowning now that image intensity exist
			for( auto it = i32.begin(); it != i32.end(); it++ ) {
				mp_withimg_forallph.insert( pair<int,int>( *it, *it ) );
			}
cout << "Cold start mp_withimg_forallph.size() " << mp_withimg_forallph.size() << "\n";
		}
		else { //mp_withimg_forallph is not empty but we desire that only mat points are index for which both phases can be tested!
			//##MK::because we want to be strict
			map<int,int> i32_query;
			for( auto it = i32.begin(); it != i32.end(); it++ ) {
				i32_query.insert( pair<int,int>( *it, *it ) );
			}
			//if there are now material point ID that are not already in mp_withimg_forallph we ignore them as only candidates with
			//results for all phases are accepted!
			for( auto jt = mp_withimg_forallph.begin(); jt != mp_withimg_forallph.end(); jt++ ) {
				if ( i32_query.find( jt->first ) == mp_withimg_forallph.end() ) {
					mp_withimg_forallph.erase( jt->first );
cout << "-->Removed " << jt->first << "\n";
				}
			}
cout << "Remaining mp_withimg_forallph.size() " << mp_withimg_forallph.size() << "\n";
		}
		i32 = vector<int>();
	}

	//accept final list of material points to index
	for( auto it = mp_withimg_forallph.begin(); it != mp_withimg_forallph.end(); it++ ) {
		mpid.push_back( static_cast<unsigned int>( it->first) );
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	indexer_tictoc.prof_elpsdtime_and_mem( "ReadNumberOfMatPointsFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool indexerHdl::broadcast_testoris()
{
	double tic = MPI_Wtime();

	//MASTER communicates testorientations to all SLAVES
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( testoris.size() >= (INT32MX-1) ) {
			cerr << "Rank " << MASTER << " current implementation does not handle the case of larger than 2 billion testoris!" << "\n"; localhealth = 0;
		}
		if ( testoris.size() == 0 ) {
			cerr << "Rank " << MASTER << " testoris on MASTER is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the size and state of the dataset!" << "\n";
		return false;
	}

	//dataset on the master is okay but do we need to broadcast at all to others, i.e. are there slave processes?
	if ( get_nranks() < 2 ) {
		cout << "Rank " << get_myrank() << " skipping broadcast_testoris because single process execution" << "\n";
		return true;
	}

	//communicate size of the master's dataset
	int nevt = 0;
	if ( get_myrank() == MASTER ) {
		nevt = static_cast<size_t>(testoris.size());
	}
	MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	MPI_Quaternion* buf = NULL;
	try {
		buf = new MPI_Quaternion[nevt];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for buf!" << "\n"; localhealth = 0;
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL; return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < testoris.size(); ++i ) {
			buf[i] = MPI_Quaternion( testoris[i].q0, testoris[i].q1, testoris[i].q2, testoris[i].q3 );
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, nevt, MPI_Quaternion_Type, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			testoris.reserve( static_cast<size_t>(nevt) );
			for( int i = 0; i < nevt; i++ ) {
				testoris.push_back( squat( buf[i].q0, buf[i].q1, buf[i].q2, buf[i].q3 ) );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all ranks were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication of testoris!" << "\n";
		return false;
	}
	else {
		cout << "Rank " << get_myrank() << " communication of testoris was successful" << "\n";
	}

	/*
	cout << "Rank " << get_myrank() << " listened to MASTER testoris.size() " << testoris.size() << "\n";
	*/

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	indexer_tictoc.prof_elpsdtime_and_mem( "BroadcastTestOris", APT_XX, APT_IS_PAR, mm, tic, toc);
	return true;
}


bool indexerHdl::broadcast_directions()
{
	double tic = MPI_Wtime();

	//MASTER communicates directions to all SLAVES
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( directions.size() >= (INT32MX-1) ) {
			cerr << "Rank " << MASTER << " current implementation does not handle the case of larger than 2 billion directions!" << "\n"; localhealth = 0;
		}
		if ( directions.size() == 0 ) {
			cerr << "Rank " << MASTER << " directions on MASTER is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the size and state of the directions dataset!" << "\n";
		return false;
	}

	//directions on the master is okay, but do we need to broadcast at all to others, i.e. are there slaves?
	if ( get_nranks() < 2 ) {
		cout << "Rank " << get_myrank() << " skipping broadcast_directions because single process execution" << "\n"; return true;
	}

	//communicate size of the master's dataset
	int ndir = 0;
	if ( get_myrank() == MASTER ) {
		ndir = static_cast<size_t>(directions.size());
	}
	MPI_Bcast( &ndir, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	MPI_ElevAzimXYZ* buf = NULL;
	try {
		buf = new MPI_ElevAzimXYZ[ndir];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for buf!" << "\n"; localhealth = 0;
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL; return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < directions.size(); i++ ) {
			buf[i] = MPI_ElevAzimXYZ( directions[i].x, directions[i].y, directions[i].z );
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, ndir, MPI_ElevAzimXYZ_Type, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			directions.reserve( static_cast<size_t>(ndir) );
			for( int i = 0; i < ndir; i++ ) {
				directions.push_back( p3d( buf[i].x, buf[i].y, buf[i].z ) );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication of directions!" << "\n"; return false;
	}
	else {
		cout << "Rank " << get_myrank() << " communication of directions was successful" << "\n";
	}

	/*
	cout << "Rank " << get_myrank() << " listened to MASTER directions.size() " << directions.size() << "\n";
	*/

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	indexer_tictoc.prof_elpsdtime_and_mem( "BroadcastTestOris", APT_XX, APT_IS_PAR, mm, tic, toc);
	return true;
}


bool indexerHdl::broadcast_reference_templates()
{
	double tic = MPI_Wtime();

	//MASTER communicates reference template solutions for all phase candidates to all SLAVES
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( phlib.candidates.size() >= (INT32MX-1) ) {
			cerr << "Rank " << MASTER << " current implementation does not handle the case of larger than 2 billion phases!" << "\n"; localhealth = 0;
		}
		if ( phlib.candidates.size() == 0 ) {
			cerr << "Rank " << MASTER << " phase candidates on MASTER is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the size and state of the phase candidates datasets!" << "\n";
		return false;
	}

	//directions on the master is okay, but do we need to broadcast at all to others, i.e. are there slaves?
	if ( get_nranks() < 2 ) {
		cout << "Rank " << get_myrank() << " skipping broadcast_reference_templates because single process execution" << "\n"; return true;
	}

	//communicate size of the master's dataset
	int nphs = 0;
	if ( get_myrank() == MASTER ) {
		nphs = static_cast<size_t>(phlib.candidates.size());
	}
	MPI_Bcast( &nphs, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	//pack all results into two implicit 2d arrays
	localhealth = 1;

	unsigned int* buf1 = NULL;
	try {
		buf1 = new unsigned int[nphs];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for buf1!" << "\n"; localhealth = 0;
	}

	float* buf2 = NULL;
	try {
		buf2 = new float[nphs*ConfigIndexer::NumberOfS2Directions];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for buf2!" << "\n"; localhealth = 0;
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf1; buf1 = NULL;
		delete [] buf2; buf2 = NULL;
		return false;
	}

	//master populates communication buffers
	if ( get_myrank() == MASTER ) {
		for( int ph = 0; ph < nphs; ph++ ) {
			//copy the IDs
			buf1[ph] = phlib.candidates.at(ph).candid;
			//copy the intensities
			size_t woffset = ph*ConfigIndexer::NumberOfS2Directions;
			vector<apt_real> & these = phlib.candidates.at(ph).intensities;
			for( size_t i = 0; i < these.size(); i++ ) {
				buf2[woffset+i] = these.at(i);
			}
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf1, nphs, MPI_UNSIGNED, MASTER, MPI_COMM_WORLD );
	MPI_Bcast( buf2, nphs*static_cast<int>(ConfigIndexer::NumberOfS2Directions), MPI_FLOAT, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			phlib.candidates = vector<phaseCandidate>( nphs, phaseCandidate() );
			for( int ph = 0; ph < nphs; ph++ ) {
				phlib.candidates.at(ph).candid = buf1[ph];
				size_t roffset = ph*ConfigIndexer::NumberOfS2Directions;
				vector<apt_real> & these = phlib.candidates.at(ph).intensities;
				these.reserve( ConfigIndexer::NumberOfS2Directions );
				for( unsigned int ii = 0; ii < ConfigIndexer::NumberOfS2Directions; ii++ ) {
					these.push_back( buf2[roffset+ii] );
				}
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf1 and buf2 readout!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf1; buf1 = NULL;
	delete [] buf2; buf2 = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication of phaseCandidates!" << "\n"; return false;
	}
	else {
		cout << "Rank " << get_myrank() << " communication of phaseCandidates was successful" << "\n";
	}

	/*
	cout << "Rank " << get_myrank() << " listened to MASTER candidates.size() " << phlib.candidates.size() << "\n";
	*/

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	indexer_tictoc.prof_elpsdtime_and_mem( "BroadcastRefTemplates", APT_XX, APT_IS_PAR, mm, tic, toc);
	return true;
}


bool indexerHdl::broadcast_matpoints()
{
	double tic = MPI_Wtime();

	//MASTER communicates directions to all SLAVES
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( mpid.size() >= (INT32MX-1) ) {
			cerr << "Rank " << MASTER << " current implementation does not handle the case of larger than 2 billion material points!" << "\n"; localhealth = 0;
		}
		if ( mpid.size() == 0 ) {
			cerr << "Rank " << MASTER << " mpid on MASTER is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the size and state of the mpid dataset!" << "\n";
		return false;
	}

	//mpid on the master is okay, but do we need to broadcast at all to others, i.e. are there slaves?
	if ( get_nranks() < 2 ) {
		cout << "Rank " << get_myrank() << " skipping broadcast_matpoints because single process execution" << "\n";
		return true;
	}

	//communicate size of the master's dataset
	int nmps = 0;
	if ( get_myrank() == MASTER ) {
		nmps = static_cast<int>(mpid.size());
	}
	MPI_Bcast( &nmps, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	unsigned int* buf = NULL;
	try {
		buf = new unsigned int[nmps];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for buf!" << "\n"; localhealth = 0;
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL; return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < mpid.size(); ++i ) {
			buf[i] = mpid[i];
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, nmps, MPI_UNSIGNED, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			mpid.reserve( static_cast<size_t>(nmps) );
			for( int i = 0; i < nmps; i++ ) {
				mpid.push_back( buf[i] );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication of matpoints!" << "\n"; return false;
	}
	else {
		cout << "Rank " << get_myrank() << " communication of matpoints was successful" << "\n";
	}

	/*
	cout << "Rank " << get_myrank() << " listened to MASTER mpid.size() " << mpid.size() << "\n";
	*/

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	indexer_tictoc.prof_elpsdtime_and_mem( "BroadcastMatPointIDs", APT_XX, APT_IS_PAR, mm, tic, toc);
	return true;
}


bool indexerHdl::init_target_file()
{
	double tic = MPI_Wtime();

	string h5fn_out = "PARAPROBE.Indexer.Results.SimID." + to_string(ConfigShared::SimID) + ".h5";
	cout << "Initializing target file " << h5fn_out << "\n";

	debugh5Hdl.h5resultsfn = h5fn_out;
	if ( debugh5Hdl.create_indexer_apth5( h5fn_out ) != WRAPPED_HDF5_SUCCESS ) {
		return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	indexer_tictoc.prof_elpsdtime_and_mem( "InitTargetFile", APT_XX, APT_IS_PAR, mm, tic, toc );

	return true;
}


bool indexerHdl::write_environment_and_settings()
{
	vector<pparm> hardware = indexer_tictoc.report_machine();
	string prfx = PARAPROBE_IDXR_META_HRDWR; //PARAPROBE_UTILS_HRDWR_META_KEYS;
	for( auto it = hardware.begin(); it != hardware.end(); it++ ) {
		cout << it->keyword << "__" << it->value << "__" << it->unit << "__" << it->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *it ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing hardware setting " << it->keyword << " failed!" << "\n";
			return false;
		}
	}
	cout << "Rank " << MASTER << " hardware settings written to H5" << "\n";

	vector<pparm> software;
	ConfigIndexer::reportSettings( software );
	prfx = PARAPROBE_IDXR_META_SFTWR; //PARAPROBE_UTILS_SFTWR_META_KEYS;
	for( auto jt = software.begin(); jt != software.end(); jt++ ) {
		cout << jt->keyword << "__" << jt->value << "__" << jt->unit << "__" << jt->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *jt ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing software tool setting " << jt->keyword << " failed!" << "\n";
			return false;
		}
	}
	cout << "Rank " << MASTER << " software settings written to H5" << "\n";

	//add InputRefs with same prfx
	for( auto kt = phlib.iifo.begin(); kt != phlib.iifo.end(); kt++ ) {
		cout << kt->keyword << "__" << kt->value << "__" << kt->unit << "__" << kt->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *kt ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing software tool setting " << kt->keyword << " failed!" << "\n";
			return false;
		}
	}

	return true;
}


bool indexerHdl::distribute_matpoints_on_processes()
{
	//collectively from all processes, defines the distribution of the indexing problem at the coarsest scale
	//MK::round-robin distribution of material points to processes should be sufficient because the workload is large
	//and approximately well-balanced because for each material point same number of rotations, number of phases, and vertices
	double tic = MPI_Wtime();

	int myrank = get_myrank();
	int nranks = get_nranks();
	//int nmymp = 0;
	vector<unsigned int> nmp2rk = vector<unsigned int>( nranks, 0 );
	for( int mp = 0; mp < mpid.size(); mp++ ) { //assign each material point to a process
		int thisrank = mp % nranks;

		//IDs in mpid are not necessarily running from [0,mpid.size()) but can be int32 integers with arbitrary order and value on [0,INT32MX)!
		//to get the actual ID unsigned int actual_mpid query so: mpid.at(mp);

		//all ranks get to know who processes which mpids and how much mps in total
		mp2rk.push_back( thisrank );
		nmp2rk.at(thisrank)++;

		//locally process keep track of what they have to do
		if ( thisrank != myrank ) {
			continue;
		}
		else {
			my_mpid.push_back( mpid[mp] ); //actual world-global id of the material point, YOU MUST NOT PASS THE VALUE mp here!
			//nmymp++;
		}
	}

	//which is now the rank with most material points; and therefore the largest cache memory requirement?
	int highestrank = MASTER; //disprove that it is the master rank
	unsigned int highestworkload = 0;
	for( int r = MASTER; r < nranks; r++ ) {
		if ( nmp2rk.at(r) < highestworkload ) { //most ranks have less than the one with the highest workload
			continue;
		}
		else { //oh a possible candidate with the highest workload
			highestrank = r;
			highestworkload = nmp2rk.at(r);
		}
	}

	//typically about 41k directions and multiple phases are mined within paraprobe-araullo so for a single material point
	//we expect the storage requirements of the spherical image intensity values to be <= 10 * 41000 * 4B = 1.6 MB/ROI
	//now if we have millions of ROIs within ConfigIndexer::InputfileAraullo the file is possibly several hundred gigabytes
	//meaning, it might not fit in the memory of a single process if the program eventually is executed with one rank only
	//so we need to load the raw data in blocks, one such block is called an epoch
	size_t StorageReqPerMatPoint = 	sizeof(apt_real) * static_cast<size_t>(ConfigIndexer::NumberOfS2Directions) *
									static_cast<size_t>(ConfigIndexer::NumberOfPhaseCandidates);

	//we store results at least UpTo the k-th closest to the best solution per mat point and phase
	size_t StorageReqPerSolution = (sizeof(unsigned int) + sizeof(apt_real)) * ConfigIndexer::IOUpToKthClosest *
									static_cast<size_t>(ConfigIndexer::NumberOfPhaseCandidates);

	size_t StorageReqHighestWorkload = (StorageReqPerMatPoint + StorageReqPerSolution) * static_cast<size_t>(highestworkload);

	if ( get_myrank() == MASTER ) {
		cout << "Highestworkload is " << highestworkload << "\n";
		cout << "StorageReqPerMatPoint " << StorageReqPerMatPoint << "\n";
		cout << "StorageReqPerSolution " << StorageReqPerSolution << "\n";
		cout << "StorageReqHighestWkld " << StorageReqHighestWorkload << "\n";
		cout << "CachePerIndexingEpoch " << GIGABYTE2BYTE(ConfigIndexer::CachePerIndexingEpoch) << "\n";
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //tictoc.get_memoryconsumption();
	indexer_tictoc.prof_elpsdtime_and_mem( "IndexingDistrMatPointsMPI", APT_XX, APT_IS_SEQ, mm, tic, toc);

	if ( StorageReqHighestWorkload < GIGABYTE2BYTE(ConfigIndexer::CachePerIndexingEpoch) ) { //taking no fragmentation into account
cout << "Rank " << myrank << " got its workpackage in total " << nmp2rk.at(myrank) << " material points in " << (toc-tic) << " seconds" << "\n";
		return true;
	}

cerr << "Rank " << myrank << " got its workpackage in total " << nmp2rk.at(myrank) << " material points BUT this is too large to handle in current implementation!" << "\n";
	return false;
}


void indexerHdl::directions_generate_kdtree()
{
	//generate a KDTree for the vertices of the (geodesic grid) of directions to speed up the process
	//of finding the closest vertex to the position of an arbitrarily rotated vertex
	double tic = MPI_Wtime();

cout << "Rank " << get_myrank() << " directions_generate_kdtree()" << "\n";

	vector<unsigned int> s2g_lbls;
	try {
		s2g_lbls = vector<unsigned int>( ConfigIndexer::NumberOfS2Directions, UINT32MX );
	}
	catch (bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation failure for permutations, s2g_lbls, or s2g_directions during direction kdtree init!" << "\n"; return;
	}

	//fill in initial labels, just the incoming ids of the S2 FE mesh points, i.e. at the locations of directions on the unit sphere
	for( unsigned int lbl = 0; lbl < ConfigIndexer::NumberOfS2Directions; lbl++ ) {
		s2g_lbls.at(lbl) = lbl;
	}

	//initialize the thread-local tree
	if ( s2g_tree != NULL ) {
		cerr << "Rank " << get_myrank() << " a s2g_tree was already allocated!" << "\n"; return;
	}

	try {
		s2g_tree = new kd_tree; //this calls the default constructor
	}
	catch (bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation failure for s2g_tree!" << "\n"; return;
	}

	//this really constructs the nodes of the tree, with 16 points per leaf at most
	//temporary to hold the permutations of the S2 fe mesh points to align leaf pbjects in contiguous memory
	vector<size_t> permutations;
	s2g_tree->build( directions, permutations, 16 );

	s2g_tree->pack_p3dm1( permutations, directions, s2g_lbls, s2g_directions );

	/*
	//##MK::BEGIN DEBUG
	for( auto it = s2g_directions.begin(); it != s2g_directions.end(); it++ ) {
		cout << it->x << ";" << it->y << ";" << it->z << "\t\t" << it->m << "\n";
	}
	//##MK::END DEBUG
	*/

cout << "Rank " << get_myrank() << " process-local s2g_tree has " << s2g_tree->nodes.size() << " nodes in total consuming at least " << s2g_tree->get_treememory_consumption() << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	indexer_tictoc.prof_elpsdtime_and_mem( "IndexingBuildDirectionKDTree", APT_XX, APT_IS_PAR, mm, tic, toc);
}


void indexerHdl::phases_identify_peaks()
{
	double tic = MPI_Wtime();

	//##MK::error handling
	//filter specific (geodesic grid) vertices, those which contribute the highest absolute deviations if the S2 images
	//are not in well registration, these are the maxima, specifically identify the
	//nhighest intensity values work here with the unrotated reference template per phase
	if ( ConfigIndexer::NumberOfS2Directions >= OIMUINTMX ) {
		cerr << "Rank " << get_myrank() << " there are more directions than the implementation can handle currently (" << OIMUINTMX << ")" << "\n";
		return;
	}

	if ( ConfigIndexer::NumberOfHighestPeaks > ConfigIndexer::NumberOfS2Directions ) {
		cerr << "Rank " << get_myrank() << " desiring more peaks than there are intensity values (" << ConfigIndexer::NumberOfS2Directions << ")" << "\n";
		return;
	}

	//##MK::same (geodesic grid) for all phases
	oim_uint ni = static_cast<oim_uint>(ConfigIndexer::NumberOfS2Directions);

	for( unsigned int ph = 0; ph < ConfigIndexer::NumberOfPhaseCandidates; ph++ ) {
		//MK::mind that reference template has NFE mesh points but we use a reduced/filtered set
		//namely the ConfigIndexer::NumberOfHighestPeaks highest only
		//pull the unrotated template signal_m values for this phase into a copy
		vector<apt_real> & these = phlib.candidates.at(ph).intensities;
		vector<oim_filter> tmp;
		tmp.reserve( ni );

		for( oim_uint i = 0; i < ni; i++ ) {
			oim_float vl = these.at(static_cast<size_t>(i));
			oim_uint ii = i;
			tmp.push_back( oim_filter( vl, ii ) );
		}

		try {
			phlib.candidates.at(ph).highest = vector<oim_filter>( ConfigIndexer::NumberOfHighestPeaks, oim_filter( 0.0, OIMUINTMX ) );
		}
		catch (bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during candidates.at(" << ph << ")" << "\n"; return;
		}

		//sort the list of the filtered intensity/mesh point ID pair for this phase in ascending order
		sort( tmp.begin(), tmp.end(), SortOIMFilterAscValue );

		//filter out those vertices with the nth highest intensity values on the templates
		for( size_t highest = 0; highest < static_cast<size_t>(ConfigIndexer::NumberOfHighestPeaks); highest++ ) {
			size_t thisone = tmp.size() - 1 - highest; //one past the last, go to the last, minus which?
			phlib.candidates.at(ph).highest.at(highest) = tmp.at(thisone); //absolute highest, 2nd highest, 3rd, ...

			if ( myrank == MASTER )
				cout << "Rank " << get_myrank() << " ph/highest/val/idx\t\t" << ph << ";" << highest << ";" << tmp.at(thisone).val << ";" << tmp.at(thisone).idx << "\n";
		}
		//tmp = vector<oim_filter>();
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	indexer_tictoc.prof_elpsdtime_and_mem( "PhasesFilterHighesestPeaks", APT_XX, APT_IS_PAR, mm, tic, toc);
}


bool indexerHdl::directions_evaluate_rotations()
{
	double tic = MPI_Wtime();

	//thread-local and safe processing of the FE mesh point remapping for
	//thread-local subset of candidate orientations specified in so3g_mine

	//we take the intensity images of the reference templates rotate them into the testorientations and find
	//at which unit sphere position vertices the rotated vertices come closest
	//with these set of we build a table each row specifies which image intensity values to read from the measured of the matpoints
	//and compare with the ConfigIndexer::NumberOfHighest values
	size_t nso3 = static_cast<size_t>(ConfigIndexer::NumberOfSO3Candidates);
	size_t nhighest = static_cast<size_t>(ConfigIndexer::NumberOfHighestPeaks);

	//allocate first all necessary buffers
	for( unsigned int ph = 0; ph < ConfigIndexer::NumberOfPhaseCandidates; ph++ ) {
		try {
			phlib.candidates.at(ph).s2g_exp2ref = vector<oim_uint>( nso3*nhighest, OIMUINTMX );
		}
		catch (bad_alloc &mecroak) {
			cerr << "Rank" << get_myrank() << " failed allocating s2g_exp2ref for phase " << ph << "\n"; return false;
		}
	}
	//##MK::probe memory consumption here because the match table can become large 650k testoris * e.g. 1k highest
	memsnapshot mm = indexer_tictoc.get_memoryconsumption();

	//now build the tables for each phase separately because these have different peak values!
	for ( unsigned int ph = 0; ph < ConfigIndexer::NumberOfPhaseCandidates; ph++ ) {
		bool allhealthy = true;

		#pragma omp parallel shared(allhealthy,nso3,nhighest)
		{
			bool mehealthy = true;
			//vector<oim_uint> & there = candidates.at(ph).s2g_exp2ref;
			vector<oim_filter> & the_high_ones = phlib.candidates.at(ph).highest;
			//size_t my_woffs = 0;

			vector<oim_uint> myres; //work distribution of testoris
			vector<size_t> myrot;

			#pragma omp for schedule(dynamic,1) nowait
			for( size_t r = 0; r < testoris.size(); r++ ) { //only the outer for loop is parallelized
				//threads write to disjoint memory positions, possibly false sharing
				//my_woffs = r*nhighest;

				//use the orientation quaternion as an actively operating rotation (matrix)
				squat qq = testoris.at(r);
				t3x3 R = qq.qu2om(); //##MK::conventions

				/*
				//use the inverse of the matrix actively
				squat qqinv = qq.invert();
				t3x3 Rinv = qqinv.qu2om();
				t3x3 R = Rinv;
				*/

				myrot.push_back( r );

				for( size_t highest = 0; highest < nhighest; highest++ ) {

					//##MK::can be accelerated by generating reduced set of directions in order of highest, will save cache misses below
					size_t j = static_cast<size_t>(the_high_ones.at(highest).idx);

					//actively rotate the unrotated reference mesh point
					p3d s2r = p3d( 	R.a11*directions[j].x + R.a12*directions[j].y + R.a13*directions[j].z,
									R.a21*directions[j].x + R.a22*directions[j].y + R.a23*directions[j].z,
									R.a31*directions[j].x + R.a32*directions[j].y + R.a33*directions[j].z   );

					//find the closest match using the global s2g_tree of the process
					p3dm1 closest = s2g_tree->nearest_safe( s2r, s2g_directions, F32MX, OIMUINTMX );

			//cout << "i/j/s2r.xyz/id " << i << ";" << j << "\t\t" << s2r.x << ";" << s2r.y << ";" << s2r.z << "\t\t" << closest.m << endl;

					//compute deviation between actively rotated mesh point with closest representative of unrotated configuration
					/*//##MK::debugging
					p3d n1 = p3d( s2r.x, s2r.y, s2r.z );
					p3d n2 = p3d( closest.x, closest.y, closest.z );
					float great_circle_misori = atan(vnorm(cross(n1,n2))/dot(n1,n2));
					*/
					//float euclidean_distance = sqrt(SQR(n1.x-n2.x)+SQR(n1.y-n2.y)+SQR(n1.z-n2.z));
					//cout << "n1.xyz/n2.xyz/distance gcrc/eucl " << n1.x << ";" << n1.y << ";" << n1.z << "\t\t" << n2.x << ";" << n2.y << ";" << n2.z << "\t\t" << RADIANT2DEGREE(great_circle_misori) << ";" << euclidean_distance << endl;
					//cout << "i/highest/j/s2r.xyz/id/deg " << i << ";" << j << ";" << highest << "\t\t" << s2r.x << ";" << s2r.y << ";" << s2r.z << "\t\t" << closest.m << "\t\t" << RADIANT2DEGREE(great_circle_misori) << endl;

					if  ( closest.m != static_cast<unsigned int>(OIMUINTMX) ) { //should be always encountered
						myres.push_back( static_cast<oim_uint>( closest.m ) );
//cout << "--->myres/clm/cast\t\t" << myres.back() << "\t\t" << closest.m << "\t\t" << static_cast<oim_uint>(closest.m) << "\n";
						//there.at(my_woffs+highest) = static_cast<oim_uint>(closest.m);
						/*//##MK::debugging
						s2g_e2m_greatcircle[woffset+highest] = great_circle_misori;
						*/
						//cout << s2g_e2m_mapping[woffset+lowest] << "\t\t" << s2g_e2m_greatcircle[woffset+lowest] << endl;
					}
					else { //however we are better hyperphobic to avoid any misindexing
						mehealthy = false;
					}
				}

/*if ( r % 10000 != 0 )
	continue;
else
	cout << r << "\n";*/
			} //for all test orientations I work on...

			#pragma omp critical
			{
				if ( mehealthy == false ) {
					cerr << "Rank " << get_myrank() << " thread " << omp_get_thread_num() << " I failed in contributing to rotations table!" << "\n";
					allhealthy = false;
				}
				if ( myres.size() != (nhighest * myrot.size()) ) {
					cerr << "Rank " << get_myrank() << " thread " << omp_get_thread_num() << " I encountered an inconsistency in myres, myrot!" << "\n";
					allhealthy = false;
				}
			}

			//once all threads have established their part of the mapping
			#pragma omp barrier

			//we can write back the results into the buffer
			//##MK::impractical because requires twice the testoris.size()*nhighest as buffer
			#pragma omp critical
			{
cout << "Rank " << get_myrank() << " thread " << omp_get_thread_num() << " processed myrot.size() " << myrot.size() << " rotations and collects now" << "\n";

				for( size_t ii = 0; ii < myrot.size(); ii++ ) {
					size_t rr = myrot.at(ii);
					size_t roffset = ii*nhighest;
					size_t woffset = rr*nhighest;
					vector<oim_uint> & here = phlib.candidates.at(ph).s2g_exp2ref;
					for ( size_t highest = 0; highest < nhighest; highest++ ) {
						here.at(woffset+highest) = myres.at(roffset+highest);
//cout << "rr/high/idx/myres\t\t" << rr << "\t\t" << highest << "\t\t" << here.at(woffset+highest) << "\t\t" << myres.at(roffset+highest) << "\n";
					}
				}
				myres = vector<oim_uint>();
				myrot = vector<size_t>();
			}
		} //end of parallel region for phase ph

		if ( allhealthy == true ) {
			cout << "Rank " << get_myrank() << " rotations table for phase " << ph << " successfully build" << "\n";
		}
		else {
			cerr << "Rank " << get_myrank() << " rotations table for phase " << ph << " encountered errors!" << "\n";
			return false;
		}
	} //for each phase

	double toc = MPI_Wtime();
	indexer_tictoc.prof_elpsdtime_and_mem( "BuildRotationTablesAllPhases", APT_XX, APT_IS_PAR, mm, tic, toc );
	return true;
}


/*
unsigned int indexerHdl::plan_rawdata_import()
{
	//typically about 41k directions and multiple phases are mined within paraprobe-araullo so for a single material point
	//we expect the storage requirements of the spherical image intensity values to be <= 10 * 41000 * 4B = 1.6 MB/ROI
	//now if we have millions of ROIs within ConfigIndexer::InputfileAraullo the file is possibly several hundred gigabytes
	//meaning, it might not fit in the memory of a single process if the program eventually is executed with one rank only
	//so we need to load the raw data in blocks, one such block is called an epoch

	size_t StorageReqPerMatPoint = 	sizeof(apt_real) * static_cast<size_t>(ConfigIndexer::NumberOfS2Directions) *
									static_cast<size_t>(ConfigIndexer::NumberOfPhaseCandidates);
	size_t StorageReqMyWorkpackage = 0;
	int myrank = get_myrank();
	for( auto it = mp2rk.begin(); it != mp2rk.end(); it++ ) {
		if ( *it != myrank )
			continue;
		else
			StorageReqMyWorkpackage++;
	}

	size_t StorageReqTotal = StorageReqMyWorkpackage * StorageReqPerMatPoint;
	size_t nepochs = StorageReqTotal / ConfigIndexer::CachePerIndexingEpoch;

	if ( nepochs < static_cast<size_t>(UINT32MX) ) {
		if ( nepochs < 1 ) { //everything fits in cache of one epoch, there is at least always one indexing epoch
			return 1;
		}
		else { //split up in n indexing epochs
			return static_cast<unsigned int>(nepochs);
		}
	}

	cerr << "Rank " << get_myrank() << " planned more indexing epochs than current implementation can handle!" << "\n";
	return UINT32MX;
}
*/

void indexerHdl::load_matpoints_araullo()
{
	//##MK::currently assuming all results fit in cache
	double tic = MPI_Wtime();

	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string fwslash = "/";
	inputAraulloH5Hdl.h5resultsfn = ConfigIndexer::InputfileAraullo;
	unsigned int nph = ConfigIndexer::NumberOfPhaseCandidates;
	size_t ndir = static_cast<size_t>(ConfigIndexer::NumberOfS2Directions);

	//allocate a sufficiently large and contiguous portion of memory where to store the paraprobe-araullo results of each my material points
	//##MK::enforcing a very strict policy here: every of my material points needs to exist in the H5 file, for each phase to be indexed,
	//##MK::and all these results need to have the same number of directions otherwise we do not index!
	//##MK::because not finding a solution to a material point for which a signal was measured is something different than not having even
	//measured data for the material point
	my_sgnl_exp = vector<vector<float>*>( nph, NULL );
	bool healthy = true;
	for ( unsigned int ph = 0; ph < nph; ph++ ) {
		vector<float>* ph_sgnl_exp = NULL;
		try {
			ph_sgnl_exp = new vector<float>( my_mpid.size() * ndir, 0.0 );
			//link pointer to large buffer into dictionary-type array
			my_sgnl_exp.at(ph) = ph_sgnl_exp;
		}
		catch (bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " failed to allocate ph_sngl_exp for phase " << ph << "!" << "\n";
			healthy = false;
		}
	}

	if ( healthy == false ) {
		for( size_t ph = 0; ph < my_sgnl_exp.size(); ph++ ) {
			my_mpid = vector<unsigned int>();
			if ( my_sgnl_exp.at(ph) != NULL ) {
				delete my_sgnl_exp.at(ph);
			}
			my_sgnl_exp = vector<vector<float>*>();
		}
	}

	for( size_t mmp = 0; mmp < my_mpid.size(); mmp++ ) {
		unsigned int thismpid = my_mpid.at(mmp); //get actual material point ID "name"

		//probe if datasets for specific material points and phase exist... skip points that dont
		bool mphealthy = true;
		unsigned int ph = 0;
		while ( ph < nph && mphealthy == true ) {
			string dsnm = PARAPROBE_ARAULLO_RES_SPECPEAK + fwslash + phlib.icombis.at(ph).usetoidx + fwslash + to_string(thismpid);

			//probe existence of elevation/azimuth data in the input file and get dimensions
			if ( inputAraulloH5Hdl.query_contiguous_matrix_dims( dsnm, offs ) == WRAPPED_HDF5_SUCCESS ) {
				if ( offs.nrows() == ndir && offs.ncols() == PARAPROBE_ARAULLO_RES_SPECPEAK_NCMAX ) {
					//read the content and transfer to working array
					//##MK::improve to read block-wise
					ifo = h5iometa( dsnm, offs.nrows(), offs.ncols() );
					vector<float> f32;
					status = inputAraulloH5Hdl.read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
					if ( status == WRAPPED_HDF5_SUCCESS ) {
						vector<float> & writehere = *(my_sgnl_exp.at(ph));
						size_t writeoffset = mmp * ndir;
						for( size_t j = 0; j < f32.size(); j++ ) { //##MK::my_sgnl_exp have already been test to not be NULL pointer...!
							writehere.at(writeoffset+j) = f32[j];
						}
						//ph++;
					}
					else {
						//dataset is unreadable, keep allocated place in my_sgnl_exp
						my_mpid_faulty.insert( pair<size_t,size_t>( mmp, mmp ) );
						cerr << "Rank " << get_myrank() << " reading of " << dsnm << " failed! mmp black-listed " << mmp << "\n";
						//mphealthy = false;
					}
				}
				else {
					//dataset seems corrupted, so keep allocated place in my_sgnl_exp untouched ##MK
					my_mpid_faulty.insert( pair<size_t,size_t>( mmp, mmp ) );
					cerr << "Rank " << get_myrank() << " dimensions of " << dsnm << " do not meet expectations! mmp black-listed " << mmp << "\n";
					//mphealthy = false;
				}
			}
			else {
				//dataset dsnm does not exist, so keep allocated place in my_sgnl_exp untouched ##MK
				my_mpid_faulty.insert( pair<size_t,size_t>( mmp, mmp ) );
				cerr << "Rank " << get_myrank() << " unable access and parse dimensions of " << dsnm << "  mmp blacklisted " << mmp << " will skip material point " << thismpid << "\n";
				//mphealthy = false;
			}
			ph++;
		}

		if ( mphealthy == true ) { //paraprobe-araullo results for all phases alright so continue with next of my material points
			continue;
		}
		else { //##MK::we have violated against our strict rules set above and terminate
cerr << "Rank " << get_myrank() << " about to exit after having not been successful in reading all results!" << "\n";
			my_mpid = vector<unsigned int>();
			for( size_t ph = 0; ph < my_sgnl_exp.size(); ph++ ) {
				if ( my_sgnl_exp.at(ph) != NULL ) {
					delete my_sgnl_exp.at(ph);
				}
			}
			vector<vector<float>*> my_sgnl_exp = vector<vector<float>*>();
			return;
		}
	}

	/*
	//##MK::BEGIN DEBUG
cout << "Begin check intensity values" << "\n";
	for( size_t i = 0; i < my_sgnl_exp.size(); i++ ) {
		if ( my_sgnl_exp.at(i) != NULL ) {
			for( auto it = my_sgnl_exp.at(i)->begin(); it != my_sgnl_exp.at(i)->end(); it++ ) {
				cout << "-->i/it/val\t\t" << i << "\t\t" << it - my_sgnl_exp.at(i)->begin() << "\t\t" << *it << "\n";
			}
		}
	}
cout << "End check intensity values" << "\n";
	//##MK::END DEBUG
 	*/

	double toc = MPI_Wtime();
	memsnapshot mm = indexer_tictoc.get_memoryconsumption();
	indexer_tictoc.prof_elpsdtime_and_mem( "LoadParaprobeAraulloResultsFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
}


void indexerHdl::local_indexing_epoch()
{
	double tic = MPI_Wtime();

	//###MK::set a maximum buffer limit here

	//are there at all results to index?
	if( my_mpid.size() < 1 ) {
		return;
	}

	size_t nph = static_cast<size_t>(ConfigIndexer::NumberOfPhaseCandidates);
	size_t nsol = static_cast<size_t>(ConfigIndexer::IOUpToKthClosest);
	unsigned int ndir = ConfigIndexer::NumberOfS2Directions;
	unsigned int nso3 = ConfigIndexer::NumberOfSO3Candidates;
	size_t nhighest = static_cast<size_t>(ConfigIndexer::NumberOfHighestPeaks);

	//allocate result buffers
	my_mpid_res = vector<vector<oim_res>*>( nph, NULL );
	my_mpid_disori = vector<vector<squat>*>( nph, NULL );
	bool healthy = true;
	for ( size_t ph = 0; ph < nph; ph++ ) {
		vector<oim_res>* ph_mpid_res = NULL;
		try {
			ph_mpid_res = new vector<oim_res>( my_mpid.size() * nsol, oim_res() );
			//link pointer to large buffer into dictionary-type array
			my_mpid_res.at(ph) = ph_mpid_res;
		}
		catch (bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " failed to allocate ph_mpid_res for phase " << ph << "!" << "\n";
			healthy = false;
		}
		if ( ConfigIndexer::IOSolutionDisori == true ) {
			vector<squat>* ph_mpid_dis = NULL;
			try {
				ph_mpid_dis = new vector<squat>( my_mpid.size() * nsol, squat() );
				//link pointer to large buffer into dictionary-type array
				my_mpid_disori.at(ph) = ph_mpid_dis;
			}
			catch (bad_alloc &mecroak) {
				cerr << "Rank " << get_myrank() << " failed to allocate ph_mpid_dis for phase " << ph << "!" << "\n";
				healthy = false;
			}
		}
	}


	if ( healthy == false ) {
cerr << "Rank " << get_myrank() << " stopped indexing because unable to create solution buffers!" << "\n";
		for( size_t ph = 0; ph < my_mpid_res.size(); ph++ ) {
			if ( my_mpid_res.at(ph) != NULL ) {
				delete my_mpid_res.at(ph);
			}
			my_mpid_res = vector<vector<oim_res>*>();
		}
		for( size_t ph = 0; ph < my_mpid_disori.size(); ph++ ) {
			if ( my_mpid_disori.at(ph) != NULL ) {
				delete my_mpid_disori.at(ph);
			}
			my_mpid_disori = vector<vector<squat>*>();
		}
		cerr << "Rank " << get_myrank() << " failed in setting up result buffers!" << "\n";
		return;
	}


	//##MK::DEBUG simple OpenMP profiling per material point
	string tictoc_fn = "PARAPROBE.Indexer.SimID." + to_string(ConfigShared::SimID) + ".Rank." + to_string(get_myrank()) + ".DetailedProfiling.csv";
	ofstream csvlog;
	csvlog.open(tictoc_fn.c_str(), ofstream::out | ofstream::trunc);
	if (csvlog.is_open() == true) {
		//header
		csvlog << "MPID;PHCANDID;ThreadID;Indexing;Postprocessing\n";
		csvlog << ";;;s;s\n";
		csvlog << "MPID;PHCANDID;ThreadID;Indexing;Postprocessing\n";
	}
	else {
		cerr << "Rank " << get_myrank() << " unable to write process-local profiling files" << "\n";
		return;
	}
	//##MK::DEBUG

	//all paraprobe-araullo results loaded and indexing results buffers ready to rumble so start indexing
	for( size_t ph = 0; ph < nph; ph++ ) {

		#pragma omp parallel shared(nph,nsol,ndir,nso3,nhighest)
		{
			vector<float> const & sgnl_e = *(my_sgnl_exp.at(ph));
			vector<oim_uint> const & sgnl_idx = phlib.candidates.at(ph).s2g_exp2ref;
			squat qref = phlib.candidates.at(ph).refori;
			bool iodisori2ref = ConfigIndexer::IOSolutionDisori;

			vector<oim_res> mt_mmp_res;
			vector<squat> mt_mmp_dis;
			vector<size_t> mt_mmp_ids;
			vector<tictoc> mt_tictoc;
			int mt = omp_get_thread_num();

			//generate thread-local close copy of
			#pragma omp for schedule(dynamic,1) nowait
			for( size_t mmp = 0; mmp < my_mpid.size(); mmp++ ) { //threads cooperatively machine of material points
				//individual thread read disjoint portions of array, array chunks read are large
				//threads write to disjoint section on array

				/*
				#pragma omp critical
				{
					cout << "Rank " << get_myrank() << " thread " << omp_get_thread_num() << " attempts indexing of " << mmp << " of " << my_mpid.size() << "\n";
				}
				*/

				//is my_mpid[mmp] black-listed because no results from paraprobe-araullo are available?
				map<size_t,size_t>::iterator healthy = my_mpid_faulty.find( mmp );
				if ( healthy == my_mpid_faulty.end() ) { //most-likely no blacklisted

					double mmtic = omp_get_wtime();

					vector<oim_res> tmp; //hold temporarily all solutions for material point mmp to find the k-th closest
					vector<oim_filter> const & tmpl_intensities = phlib.candidates.at(ph).highest;
					for( unsigned int rot = 0; rot < nso3; rot++ ) { //perform template matching for all test candidates
						oim_float curr_solution_f32 = 0.f;
						size_t roffset_idx = static_cast<size_t>(rot) * nhighest;
						size_t roffset_val = mmp*ndir;

						for( size_t highest = 0; highest < nhighest; highest++ ) {
							oim_float msrr_tmpl = tmpl_intensities.at(highest).val;

							size_t compare2thisone = static_cast<size_t>(sgnl_idx.at(roffset_idx+highest));

							oim_float msrr_exp = sgnl_e.at(roffset_val + compare2thisone);

							curr_solution_f32 += SQR(msrr_tmpl - msrr_exp);
	//cout << "rot/high/curr/msrr_tmpl/msrr_exp\t\t" << rot << "\t\t" << highest << "\t\t" << curr_solution_f32 << "\t\t" << msrr_tmpl << "\t\t" << msrr_exp << "\n";
						}

						tmp.push_back( oim_res( curr_solution_f32, rot ) );
					} //next test orientation

					//all orientations tested, now sort to get the kth closest
					sort( tmp.begin(), tmp.end(), SortOIMResultAscValue );

					//##MK::perform symmetric reduction!
					//#####################################
					//#####################################

					//write back kth closest values
					//size_t woffset = mmp*nsol;
					mt_mmp_ids.push_back( mmp );

					for( size_t sol = 0; sol < nsol; sol++ ) {
						mt_mmp_res.push_back( tmp.at(sol) );
					}

					double ddtic = 0.0;
					double ddtoc = 0.0;
					if ( iodisori2ref == true ) {
						ddtic = omp_get_wtime();
						for( size_t sol = 0; sol < nsol; sol++ ) {
							unsigned int idx = tmp.at(sol).idx;
							squat qsol = testoris.at(idx);
							pair<bool,squat> thisdisori = disoriquaternion_cubic_cubic( qref, qsol );
							if ( thisdisori.first == true ) {
								mt_mmp_dis.push_back( thisdisori.second );
							}
							else {
								mt_mmp_dis.push_back( squat( F32MX, F32MX, F32MX, F32MX ) ); //##MK::no solution found so give failure quaternion
							}
						}
						ddtoc = omp_get_wtime();
					}

					//release resources
					tmp = vector<oim_res>();

					double mmtoc = omp_get_wtime();
					unsigned int thismpid = my_mpid.at(mmp); //actual material point ID "name"
					mt_tictoc.push_back( tictoc( mmtoc-mmtic, ddtoc-ddtic, thismpid, static_cast<unsigned int>(ph), mt ) );
					/*
					#pragma omp critical
					{
						cout << "Rank " << get_myrank() << " thread " << omp_get_thread_num() << " indexing of mp " << mmp << " took " << (mmtoc-mmtic) << "\n";
					}
					*/
				}
				//else { //mmp black-listed, skip and proceed with next one
				//	continue;
				//}
			} //thread grabs the next matpoint from the loop

			//thread has completed local workpackage
			#pragma omp critical
			{
cout << "Rank " << get_myrank() << " thread " << omp_get_thread_num() << " has completed indexing" << "\n";
				for( size_t ii = 0; ii < mt_mmp_ids.size(); ii++ ) {
					size_t woffset = mt_mmp_ids.at(ii)*nsol;
					size_t roffset = ii*nsol;
					for( size_t sol = 0; sol < nsol; sol++ ) {
						my_mpid_res.at(ph)->at(woffset+sol) = mt_mmp_res.at(roffset+sol);
					}
					if ( iodisori2ref == false ) {
						continue;
					}
					else {
						for( size_t sol = 0; sol < nsol; sol++ ) {
							my_mpid_disori.at(ph)->at(woffset+sol) = mt_mmp_dis.at(roffset+sol);
						}
					}
				}
				mt_mmp_ids = vector<size_t>();
				mt_mmp_res = vector<oim_res>();
				mt_mmp_dis = vector<squat>();
				for( auto ii = mt_tictoc.begin(); ii != mt_tictoc.end(); ii++ ) {
					csvlog << ii->mpid << ";" << ii->phcid << ";" << ii->threadid << ";" << ii->dt_indexing << ";" << ii->dt_postprocess << "\n";
				}
				mt_tictoc = vector<tictoc>();
			} //thread as written back his results
		} //end of parallel region
	} //continue with next phase

	csvlog.flush();
	csvlog.close();

	double toc = MPI_Wtime();
	memsnapshot mm = indexer_tictoc.get_memoryconsumption();
	indexer_tictoc.prof_elpsdtime_and_mem( "IndexingParaprobeAraulloResults", APT_XX, APT_IS_PAR, mm, tic, toc);
cout << "Rank " << get_myrank() << " has completed indexing took " << (toc-tic) << " seconds " <<
		(toc-tic) / static_cast<double>(my_mpid.size()) << " s/matpoint"<< "\n";
}


void indexerHdl::write_solutions_to_h5()
{
	//##MK::currently assuming all results fit in cache
	double tic = MPI_Wtime();

	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string fwslash = "/";
	debugh5Hdl.h5resultsfn = "PARAPROBE.Indexer.Results.SimID." + to_string(ConfigShared::SimID) + ".h5";

	if ( my_mpid_res.size() < 1 ) {
		cerr << "Rank " << get_myrank() << " has no results to report!" << "\n";
		return;
	}

	size_t nsol = static_cast<size_t>(ConfigIndexer::IOUpToKthClosest);
	//f32 has the same size for every material point and phase so allocate only once and overwrite later
	vector<float> f32 = vector<float>( PARAPROBE_IDXR_RES_SOL_NCMAX*nsol, 0.f );
	for( unsigned int ph = 0; ph < ConfigIndexer::NumberOfPhaseCandidates; ph++ ) {
		if ( my_mpid_res.at(ph) != NULL ) {
			vector<oim_res> const & these = *(my_mpid_res.at(ph));
			vector<squat> const & those = *(my_mpid_disori.at(ph));
			string prefix_sol = PARAPROBE_IDXR_RES_SOL + fwslash + "PH" + to_string(ph) + fwslash; //phlib.icombis.at(ph).usetoidx instead of to_string(ph), what is two usetoidx with the same name?
			string prefix_dis = PARAPROBE_IDXR_RES_DIS + fwslash + "PH" + to_string(ph) + fwslash;
			string dsnm = "";

			for( size_t mmp = 0; mmp < my_mpid.size(); mmp++ ) {
				unsigned int thismpid = my_mpid.at(mmp); //actual material point ID "name"

				//black-listed?
				map<size_t,size_t>::iterator healthy = my_mpid_faulty.find( mmp );
				if ( healthy == my_mpid_faulty.end() ) { //most likely not in blacklist

					dsnm = prefix_sol + to_string(thismpid);

					size_t roffset = mmp*nsol;
					//hyperphobic reset results to zero
					f32 = vector<float>( PARAPROBE_IDXR_RES_SOL_NCMAX*nsol, 0.f);
					for( size_t sol = 0; sol < nsol; sol++ ) { //which of the test orientations and which template match value?
						f32[PARAPROBE_IDXR_RES_SOL_NCMAX*sol+0] = static_cast<float>(these.at(roffset+sol).idx);
						f32[PARAPROBE_IDXR_RES_SOL_NCMAX*sol+1] = static_cast<float>(these.at(roffset+sol).val);
					}

					ifo = h5iometa( dsnm, nsol, PARAPROBE_IDXR_RES_SOL_NCMAX );
					status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
					if ( status == WRAPPED_HDF5_SUCCESS ) {
						offs = h5offsets( 0, f32.size()/PARAPROBE_IDXR_RES_SOL_NCMAX, 0, PARAPROBE_IDXR_RES_SOL_NCMAX,
								f32.size()/PARAPROBE_IDXR_RES_SOL_NCMAX, PARAPROBE_IDXR_RES_SOL_NCMAX );
						status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
						if ( status == WRAPPED_HDF5_SUCCESS ) { //was successful, next material point
							f32 = vector<float>(); //continue;
						}
						else {
							cerr << "Rank " << get_myrank() << " writing " << dsnm << " failed! " << status << "\n";
						}
					}
					else {
						cerr << "Rank " << get_myrank() << " creating " << dsnm << " failed! " << status << "\n";
					}

					if( ConfigIndexer::IOSolutionDisori == true ) {
						dsnm = prefix_dis + to_string(thismpid);
						f32 = vector<float>( PARAPROBE_IDXR_RES_DIS_NCMAX*nsol, F32MX );
						for( size_t sol = 0; sol < nsol; sol++ ) {
							f32[PARAPROBE_IDXR_RES_DIS_NCMAX*sol+0] = static_cast<float>(those.at(roffset+sol).q0);
							f32[PARAPROBE_IDXR_RES_DIS_NCMAX*sol+1] = static_cast<float>(those.at(roffset+sol).q1);
							f32[PARAPROBE_IDXR_RES_DIS_NCMAX*sol+2] = static_cast<float>(those.at(roffset+sol).q2);
							f32[PARAPROBE_IDXR_RES_DIS_NCMAX*sol+3] = static_cast<float>(those.at(roffset+sol).q3);
						}
						ifo = h5iometa( dsnm, nsol, PARAPROBE_IDXR_RES_DIS_NCMAX );
						status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
						if ( status == WRAPPED_HDF5_SUCCESS ) {
							offs = h5offsets( 0, f32.size()/PARAPROBE_IDXR_RES_DIS_NCMAX, 0, PARAPROBE_IDXR_RES_DIS_NCMAX,
									f32.size()/PARAPROBE_IDXR_RES_DIS_NCMAX, PARAPROBE_IDXR_RES_DIS_NCMAX );
							status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
							if ( status == WRAPPED_HDF5_SUCCESS ) { //was successful, next material point
								f32 = vector<float>(); //continue;
							}
							else {
								cerr << "Rank " << get_myrank() << " writing " << dsnm << " failed! " << status << "\n";
							}
						}
						else {
							cerr << "Rank " << get_myrank() << " creating " << dsnm << " failed! " << status << "\n";
						}
					}
				}
				//else blacklisted gets skipped
			} //next mmp
		}
		else {
			cerr << "Rank " << get_myrank() << " unexpected error while reading out my_mpid_res.at( " << ph << ") !" << "\n";
			return;
		}
	} //next ph

	double toc = MPI_Wtime();
	memsnapshot mm = indexer_tictoc.get_memoryconsumption();
	indexer_tictoc.prof_elpsdtime_and_mem( "WriteIndexingResultsToH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
cout << "Rank " << get_myrank() << " wrote indexing results to H5 file in " << (toc-tic) << " seconds" << "\n";
}


void indexerHdl::evaluate_disorientations_cpu()
{
	double tic = MPI_Wtime();

	//build upper triangle matrix only once
	vector<DisoriIJ> disori_matrix;
	vector<pair<size_t,size_t>> disori_errors;

	#pragma omp parallel
	{
		int mt = omp_get_thread_num();

		vector<DisoriIJ> myres; //thread-local buffer to defer write back of results to the end and thus spent more time in parallel
		size_t ncand = testoris.size();
		apt_real thrshld = ConfigIndexer::IOSymmRedMaxDisoriAngle; // only <= allowed
		vector<pair<size_t,size_t>> myerr;

		#pragma omp for schedule(dynamic,1) nowait
		for( size_t i = 0; i < ncand; i++ ) { //only the outer loop is parallelized
			//threads process entire rows of the upper triangle matrix
			double mytic = omp_get_wtime();
			squat ip = testoris.at(i);
			for( size_t j = i+1; j < ncand; j++ ) {
				squat iq = testoris.at(j);

				pair<bool,squat> ij = disoriquaternion_cubic_cubic( ip, iq );

				if ( ij.first == true ) { //most likely case
					//collect only disorientation quaternions with angle <= thrshld
					apt_real theta = 2.f*acos(ij.second.q0);
					if ( theta > thrshld ) { //most likely case disorientation of two arbitrary orientations is higher than a low disorientation angle threshold
						continue;
					}
					else {
						//##MK::DEBUG
						/*#pragma omp critical
						{
							cout << "Found new one i/j " << i << "\t\t" << j << "\n";
						}*/
						//##MK::END DEBUG
						myres.push_back( DisoriIJ(i, j, theta) );
					}
				}
				else {
					myerr.push_back( pair<size_t,size_t>(i,j) );
				}
			} //next j

			double mytoc = omp_get_wtime();
			#pragma omp critical
			{
				cout << "Rank " << get_myrank() << " thread " << omp_get_thread_num() << " i = " << i << " in " << (mytoc-mytic) <<
						" seconds " << (ncand-(i+1)) << " cases, myerr.size() " << myerr.size() << " myres.size() " << myres.size() << "\n";
			}
		} //next i

		#pragma omp critical
		{
			//concentrate all potential reallocations and feedback the cooperatively achieved results
			disori_matrix.reserve( disori_matrix.size() + myres.size() );
			for( auto it = myres.begin(); it != myres.end(); ++it ) { //fill in thread-local results to master
				disori_matrix.push_back( *it );
			}
			myres = vector<DisoriIJ>();

			//also collect all possible errors for debugging purposes
			disori_errors.reserve( disori_errors.size() + myerr.size() );
			for( auto it = myerr.begin(); it != myerr.end(); ++it ) { //fill in thread-local results to master
				disori_errors.push_back( *it );
			}
			myerr = vector<pair<size_t,size_t>>();
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	indexer_tictoc.prof_elpsdtime_and_mem( "BuildDisorientationLookUpTableCPU", APT_XX, APT_IS_PAR, mm, tic, toc );

cout << "Indexer multi-threaded disori_matrix.size() " << disori_matrix.size() << " took " << (toc-tic) << " seconds " <<
			static_cast<double>(disori_matrix.size()) / (toc-tic) << " Disoris per second" << "\n";

	//now reporting the errors
	//MK::BEGIN DEBUG
cout << "Begin report which combinations failed!" << "\n";
	for( auto it = disori_errors.begin(); it != disori_errors.end(); it++ ) {
		cout << it->first << "\t\t" << it->second << "\n";
	}
cout << "End report which combinations failed!" << "\n";
	//MK::END DEBUG

	//MK::BEGIN DEBUG
cout << "Begin report which close situations!" << "\n";
	for( auto it = disori_matrix.begin(); it != disori_matrix.end(); it++ ) {
		cout << it->i << "\t" << it->j << "\t\t" << RADIANT2DEGREE(it->th) << "\n";
	}
cout << "End report which close situations!" << "\n";
//MK::END DEBUG

}

#ifdef UTILIZE_GPUS
/* ##DEVELOPMENTAL
void indexerHdl::evaluate_disorientations_cpugpu()
{
	double tic = MPI_Wtime();

	//build upper triangle matrix only once
	vector<DisoriIJ> disori_matrix;
	vector<pair<size_t,size_t>> disori_errors;

	#pragma omp parallel
	{
		int mt = omp_get_thread_num();

		vector<DisoriIJ> myres; //thread-local buffer to defer write back of results to the end and thus spent more time in parallel
		size_t ncand = testoris.size();
		apt_real thrshld = ConfigIndexer::IOSymmRedMaxDisoriAngle; // only <= allowed
		vector<pair<size_t,size_t>> myerr;

		vector<squat> O;
		cubic_cubic_operators( O );

		#pragma omp for schedule(dynamic,1) nowait
		for( size_t i = 0; i < ncand; i++ ) { //only the outer loop is parallelized
			//threads process entire rows of the upper triangle matrix
			double mytic = omp_get_wtime();
			squat ip = testoris.at(i);

			if ( mt != MASTER ) { //non master threads compute classically
				//##########
			}
			else { //== MASTER will use the GPU
				#pragma acc data copy(Og[0:24*4]) //bring operators on the GPU wk_pq1[0:nq4],wk_qp1[0:nq4],wk_Oi_pq1[0:nq4],wk_Oi_qp1[0:nq4],wk_Oi_pq1_Oj[0:nq4],wk_Oi_qp1_Oj[0:nq4])
				{
					int blocksize = 32;
					int np = 1*4;
					int nq = blocksize*4;

					int j = i+1;
					float* ipg = NULL;
					float* ip1g = NULL;
					float* iqg = NULL;
					float* iq1g = NULL;

					ipg = new float[np];
					ip1g = new float[np];
					iqg = new float[nq];
					iq1g = new float[nq];
					//populate

					while( (j + blocksize) < ncand ) { //work in blocks on GPU

						#pragma acc data copy(ipg,ip1g,iqg,iq1g)
						{
							#pragma acc parallel loop copyin() present(ipq,ip1g,iqg,iq1g)
							for( int kk = 0; kk < blocksize; kk++ ) {
								//loop nest
							}
						}

						//next block
						j += blocksize;
					}
					for(      ; j < ncand; j++ ) { //remainder on CPU
						//######
					}


					for( size_t j = i+1; j < ncand; j++ ) { //process in blocks as many blocks as threads
						squat iq = testoris.at(j);


						ipg = new float[4];
						iqg = new float[4];
						ip1g = new float[4];
						iq1g = new float[4];
						//fill values in



						float* iq_gpu = NULL; //the row of iq's to analyze using GPU threads on host to be sent to device mem
				iq_gpu = new float[4*nqnow];

				float* ifz_gpu = NULL; //the resulting disorientation quaternion in the fundamental zone to retrieve from device and analyze on host
				ifz_gpu = new float[4*nqnow];
				float* isy_gpu = NULL;
				isy_gpu = new float[4*24];
				if ( ip_gpu != NULL && iq_gpu != NULL && ifz_gpu != NULL && isy_gpu != NULL ) {
					ip_gpu[0] = ip.q0;
					ip_gpu[1] = ip.q1;
					ip_gpu[2] = ip.q2;
					ip_gpu[3] = ip.q3;

					int k = 0;
					for( int j = i+1; j < ncand; j++, k += 4 ) {
						squat iq = testoris.at(j);
						iq_gpu[k+0] = iq.q0;
						iq_gpu[k+1] = iq.q1;
						iq_gpu[k+2] = iq.q2;
						iq_gpu[k+3] = iq.q3;
						//init solution to not a valid quaternion to spot computation errors
						ifz_gpu[k+0] = 0.f;
						ifz_gpu[k+1] = 0.f;
						ifz_gpu[k+2] = 0.f;
						ifz_gpu[k+3] = 0.f;
					}
					//disoriquaternion_cubic_cubic_gpu( ip, iq );
					for( int jj = 0; jj < 24; jj++ ) { //pull the cubic_cubic symmetry quaternions
						isy_gpu[4*jj+0] = O.at(jj).q0;
						isy_gpu[4*jj+1] = O.at(jj).q1;
						isy_gpu[4*jj+2] = O.at(jj).q2;
						isy_gpu[4*jj+3] = O.at(jj).q3;
					}

					float* wk_pq1 = NULL;
					wk_pq1 = new float[4*nqnow];
					float* wk_qp1 = NULL;
					wk_qp1 = new float[4*nqnow];

					float* wk_Oi_pq1 = NULL;
					wk_Oi_pq1 = new float[4*nqnow];
					float* wk_Oi_qp1 = NULL;
					wk_Oi_qp1 = new float[4*nqnow];

					float* wk_Oi_pq1_Oj = NULL;
					wk_Oi_pq1_Oj = new float[4*nqnow];
					float* wk_Oi_qp1_Oj = NULL;
					wk_Oi_qp1_Oj = new float[4*nqnow];

					int nq4 = 4*nqnow;

					if ( wk_pq1 != NULL && wk_qp1 != NULL &&
							wk_Oi_pq1 != NULL && wk_Oi_qp1 != NULL &&
								wk_Oi_pq1_Oj != NULL && wk_Oi_qp1_Oj != NULL ) {
						#pragma acc data copy(wk_pq1[0:nq4],wk_qp1[0:nq4],wk_Oi_pq1[0:nq4],wk_Oi_qp1[0:nq4],wk_Oi_pq1_Oj[0:nq4],wk_Oi_qp1_Oj[0:nq4])
						{
							//compute
							//squat pq1 = pp.multiply_quaternion( q1 ); //pq^-1
							//squat qp1 = qq.multiply_quaternion( p1 ); //qp^-1 because bicrystal symmetry switching symmetry applies pq1 and qp1 are misorientation quaternions not orientation quaternions!
							#pragma acc parallel loop copyin(ip_gpu[0:4],ip1_gpu[0:4],iq_gpu[0:4],iq1_gpu[0:4]) present(wk_pq1[0:nq4],wk_qp1[0:nq4],wk_Oi_pq1[0:nq4],wk_Oi_qp1[0:nq4],wk_Oi_pq1_Oj[0:nq4],wk_Oi_qp1_Oj[0:nq4])
							for( int k = 0; k < nqnow; k++ ) {
								//conjugate....
								//pq^-1
								wk_pq1[k+0] = ipg[] iqg[]; //quat multiplication

								//qp^-1
								wk_qp1[k+1] = iqg[] * ip1g[]
							}
							//wk_pq1 and wk_qp1 remain on device

							for ( int sysy = 0; sysy < 24*24; sysy++ ) {
								int iii = (sysy / 24) % 24;
								int jjj = sysy % 24;
								#pragma acc parallel loop present(wk_pq1[0:nq4],wk_qp1[0:nq4],wk_Oi_pq1[0:nq4],wk_Oi_qp1[0:nq4],wk_Oi_pq1_Oj[0:nq4],wk_Oi_qp1_Oj[0:nq4])

 present()


					//##MK::readable but slow because many ifs will evaluate to false
						squat Oi_pq1 = Oi.multiply_quaternion( pq1 );
						squat Oi_qp1 = Oi.multiply_quaternion( qp1 );
						for ( auto j = O.begin(); j != O.end(); j++ ) { //24x
							squat Oj = *j;
							//##MK::squat Oi_pq1_Oj = Oj.multiply_quaternion( Oi_pq1 );
							//##MK::squat Oi_qp1_Oj = Oj.multiply_quaternion( Oi_qp1 );
							squat Oi_pq1_Oj = Oi_pq1.multiply_quaternion( Oj );
							squat Oi_qp1_Oj = Oi_qp1.multiply_quaternion( Oj );
							//2x
							//check if axis is in the FZ for cubic crystal symmetry most frequently it is not
							if ( Oi_pq1_Oj.q3 >= 0.f ) {
								if ( Oi_pq1_Oj.q1 >= Oi_pq1_Oj.q2 && Oi_pq1_Oj.q2 >= Oi_pq1_Oj.q3 ) { //Oi_pq1_Oj is in fundamental zone
									if ( Oi_pq1_Oj.q0 >= q0_best ) {
										q0_best = Oi_pq1_Oj.q0;
										disori = Oi_pq1_Oj;
										valid = true;
									}
								}
							}
							if ( Oi_qp1_Oj.q3 >= 0.f ) {
								if ( Oi_qp1_Oj.q1 >= Oi_qp1_Oj.q2 && Oi_qp1_Oj.q2 >= Oi_qp1_Oj.q3 ) { //Oi_qp1_Oj is in fundamental zone
									if ( Oi_qp1_Oj.q0 >= q0_best ) {
										q0_best = Oi_qp1_Oj.q0;
										disori = Oi_qp1_Oj;
										valid = true;
									}
								}
							}
						} //next j
					} //next i
					//need to test all hyperphobic...




				}
				else {
					//##MK::error case
					continue;
				}

				//##MK::here just for test purposes present a completely unrolled GPU implementation using single precision
				//for the inner loop to maximize parallel work package


			}


				//##MK::here just for test purposes a completely unrolled GPU implementation using single precision


				if ( ij.first == true ) { //most likely case
					//collect only disorientation quaternions with angle <= thrshld
					apt_real theta = 2.f*acos(ij.second.q0);
					if ( theta > thrshld ) { //most likely case disorientation of two arbitrary orientations is higher than a low disorientation angle threshold
						continue;
					}
					else {
						myres.push_back( DisoriIJ(i, j, theta) );
					}
				}
				else {
					myerr.push_back( pair<size_t,size_t>(i,j) );
				}
			} //next j

			double mytoc = omp_get_wtime();
			#pragma omp critical
			{
				cout << "Rank " << get_myrank() << " thread " << omp_get_thread_num() << " i = " << i << " in " << (mytoc-mytic) <<
						" seconds " << (ncand-(i+1)) << " cases, myerr.size() " << myerr.size() << " myres.size() " << myres.size() << "\n";
			}
		} //next i

		#pragma omp critical
		{
			//concentrate all potential reallocations and feedback the cooperatively achieved results
			disori_matrix.reserve( disori_matrix.size() + myres.size() );
			for( auto it = myres.begin(); it != myres.end(); ++it ) { //fill in thread-local results to master
				disori_matrix.push_back( *it );
			}
			myres = vector<DisoriIJ>();

			//also collect all possible errors for debugging purposes
			disori_errors.reserve( disori_errors.size() + myerr.size() );
			for( auto it = myerr.begin(); it != myerr.end(); ++it ) { //fill in thread-local results to master
				disori_errors.push_back( *it );
			}
			myerr = vector<pair<size_t,size_t>>();
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	indexer_tictoc.prof_elpsdtime_and_mem( "BuildDisorientationLookUpTableCPUGPU", APT_XX, APT_IS_PAR, mm, tic, toc );

cout << "Indexer multi-threaded disori_matrix.size() " << disori_matrix.size() << " took " << (toc-tic) << " seconds " <<
			static_cast<double>(disori_matrix.size()) / (toc-tic) << " Disoris per second" << "\n";

	//now reporting the errors
	//MK::BEGIN DEBUG
cout << "Begin report which combinations failed!" << "\n";
	for( auto it = disori_errors.begin(); it != disori_errors.end(); it++ ) {
		cout << it->first << "\t\t" << it->second << "\n";
	}
cout << "End report which combinations failed!" << "\n";
	//MK::END DEBUG

	//MK::BEGIN DEBUG
cout << "Begin report which close situations!" << "\n";
	for( auto it = disori_matrix.begin(); it != disori_matrix.end(); it++ ) {
		cout << it->i << "\t" << it->j << "\t\t" << RADIANT2DEGREE(it->th) << "\n";
	}
cout << "End report which close situations!" << "\n";
//MK::END DEBUG

}
*/
#endif

void indexerHdl::test_disorientations()
{
	double tic = MPI_Wtime();

	//generate set of pairs of random orientations and compute disorientation quaternion/angle between them
	//should give MacKenzie distribution

	mt19937 mydice_ori;
	mydice_ori.seed( -1234 );
	mydice_ori.discard( 700000 );
	//get uniform random numbers to thin out randomly point cloud
	uniform_real_distribution<apt_real> unifrnd(0.f, 1.f);

	size_t N = 100000;
	for( size_t i = 0; i < N; i++ ) {
		squat ip = squat( unifrnd(mydice_ori), unifrnd(mydice_ori), unifrnd(mydice_ori) );
		squat iq = squat( unifrnd(mydice_ori), unifrnd(mydice_ori), unifrnd(mydice_ori) );

		pair<bool,squat> ij = disoriquaternion_cubic_cubic( ip, iq );

		if ( ij.first == true ) { //most likely case
			//collect only disorientation quaternions with angle <= thrshld
			apt_real theta = 2.f*acos(ij.second.q0) / MYPI * 180.0;

			cout << theta << "\n";
		}
		else {
			cerr << "--->Incorrect solution!" << "\n";
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	indexer_tictoc.prof_elpsdtime_and_mem( "TestDisorientations", APT_XX, APT_IS_SEQ, mm, tic, toc );
}



int indexerHdl::get_myrank()
{
	return this->myrank;
}


int indexerHdl::get_nranks()
{
	return this->nranks;
}


void indexerHdl::set_myrank( const int rr )
{
	this->myrank = rr;
}


void indexerHdl::set_nranks( const int nn )
{
	this->nranks = nn;
}


void indexerHdl::init_mpidatatypes()
{
	//define MPI datatype which assists us to logically structure the communicated data packages
	MPI_Type_contiguous(4, MPI_FLOAT, &MPI_Quaternion_Type);
	MPI_Type_commit(&MPI_Quaternion_Type);

	MPI_Type_contiguous(3, MPI_FLOAT, &MPI_ElevAzimXYZ_Type);
	MPI_Type_commit(&MPI_ElevAzimXYZ_Type);

	//commit utility data types to simplify and collate data transfer operations
	/*int cnts1[2] = {4, 2};
	MPI_Aint dsplc1[2] = {0, 4 * 4}; //4 B for a float
	MPI_Datatype otypes1[2] = {MPI_FLOAT, MPI_UNSIGNED};
	MPI_Type_create_struct(2, cnts1, dsplc1, otypes1, &MPI_Fourier_ROI_Type);
	MPI_Type_commit(&MPI_Fourier_ROI_Type);*/
}

