//##MK::GPLV3

#ifndef __PARAPROBE_CONFIG_INDEXER_H__
#define __PARAPROBE_CONFIG_INDEXER_H__

#include "../../paraprobe-utils/src/CONFIG_Shared.h"
#include "../../paraprobe-utils/src/PARAPROBE_FFT.h"


enum ANALYSIS_MODE {
	NOTHING,
	EVALUATE_DISORIENTATIONS,
	TEST_DISORIENTATIONS,
	PERFORM_INDEXING
};


class ConfigIndexer
{
public:
	
	static ANALYSIS_MODE AnalysisMode;
	static string InputfileAraullo;
	static string OrientationRefImages;
	static string InputfileTestOris;
	
	static unsigned int NumberOfS2Directions;

	static unsigned int IOUpToKthClosest;
	static apt_real IOSymmRedDisoriAngle;
	
	static bool IOSolutionQuality;
	static bool IOSolutionDisori;
	static bool IODisoriMatrix;
	static bool IOSymmReduction;
	
	static apt_real IOSymmRedMaxDisoriAngle;
	static unsigned int NumberOfSO3Candidates;
	static unsigned int NumberOfPhaseCandidates;
	static unsigned int NumberOfHighestPeaks;

	static int GPUsPerNode;
	static int GPUWorkload;

	static size_t CachePerIndexingEpoch;

	static bool readXML( string filename = "" );
	static bool checkUserInput();
	static void reportSettings( vector<pparm> & res );
};

#endif
