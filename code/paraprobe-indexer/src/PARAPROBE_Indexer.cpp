//##MK::GPLV3

#include "PARAPROBE_IndexerHdl.h"


//define what is passed upon execution
#define SIMID									1
#define CONTROLFILE								2

//##MK::for querying system maximum physical memory see
//https://stackoverflow.com/questions/2513505/how-to-get-available-memory-c-g

#ifndef GITSHA									//otherwise we could not compile without adding -DGITSHA="d123" a dummy GitSha
	#define GITSHA								0
#endif
//stringification of strings passed from cmake at compile time
//https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html
#define xstr(s)									str(s)
#define str(s) 									#s


void hello( void )
{
	cout << "paraprobe-indexer" << "\n";
	cout << xstr(GITSHA) << "\n";
	cout << "Collecting " << CiteMeIndexer::cite() << " publications for the tool to cite:" << "\n";
	for( auto it = CiteMeIndexer::Citations.begin(); it != CiteMeIndexer::Citations.end(); it++ ) {
		cout << it->author << "\n";
		cout << it->title << "\n";
		cout << it->journal << ", " << it->year << ", " << it->vol << ", pp" << it->pages << "\n";
		cout << it->doi << "\n";
	}
}


bool init(  int pargc, char** pargv )
{
	ConfigShared::SimID = stoul( pargv[SIMID] );
	try {
		ConfigIndexer::readXML(pargv[CONTROLFILE]);
	}
	catch (exception& e) {
		cerr << "\n" << "Unable to parse control file! Details:" << "\n" << e.what() << "\n"; return false;
	}
	if ( ConfigIndexer::checkUserInput() == false ) {
		cerr << "\n" << "Control file settings failed the validity check!" << "\n"; return false;
	}
	else {
		cout << "\n" << "\t\tInput is valid under SimulationID = " << "SimID." <<  ConfigShared::SimID << "\n";
	}
	cout << endl;
	return true;
}


void orientation_and_phase_indexing( const int r, const int nr, char** pargv )
{
	//allocate process-level instance of a indexerHdl. The instance handles all process-internal processing
	//the instances synchronize across and communicate with each other during execution
	indexerHdl* idx = NULL;

	int localhealth = 1;
	int globalhealth = nr;
	try {
		idx = new indexerHdl;
		idx->set_myrank(r);
		idx->set_nranks(nr);
		idx->init_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Rank " << r << " unable to allocate a sequential indexerHdl instance" << "\n"; localhealth = 0;
	}

	//load periodic table
	string xmlfn = pargv[CONTROLFILE];
	if ( idx->read_refimage_defs_from_xml( xmlfn ) == true ) {
		cout << "Rank " << r << " loaded RefImages successfully " << "\n";
	}
	else {
		cerr << "Rank " << r << " loading RefImages failed!" << "\n"; localhealth = 0;
	}

	//do we have all processes on board?
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete idx; idx = NULL; return;
	}
	
	//we have all on board grid of test orientations is required in every case, so read this first
	//MK::MASTER reads then broadcasts
	//##MK::should not be a problem because initialization is orders of magnitude cheaper than actual calculations...
	if ( idx->get_myrank() == MASTER ) {

		if ( idx->read_testoris_from_h5() == true ) {
			cout << "MASTER read successfully all test rotation/orientation quaternion values" << "\n";
		}
		else {
			cerr << "MASTER was unable to read test rotation/orientation quaternion values!" << "\n"; localhealth = 0;
		}
	}
	//else slave processes have to wait
	MPI_Barrier( MPI_COMM_WORLD );
	
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that MASTER was unable to read test rotations/orientations!" << "\n";
		delete idx; idx = NULL; return;
	}
	
	if ( idx->broadcast_testoris() == true ) {
		cout << "Rank " << r << " has synchronized test rotations/orientations" << "\n";
	}
	else {
		cerr << "Rank " << r << " failed to synchronize test rotations/orientations!" << "\n"; localhealth = 0;
	}
	
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes have synchronized the test rotations/orientations!" << "\n";
		delete idx; idx = NULL; return;
	}
	
	//all need to know how many actual orientations to test against during template matching are there
	ConfigIndexer::NumberOfSO3Candidates = idx->testoris.size();

	if ( ConfigIndexer::AnalysisMode == EVALUATE_DISORIENTATIONS ) {
		
		idx->evaluate_disorientations_cpu();
		
		//##MK::will report orientations to InputfileTestOris
	}
	else if ( ConfigIndexer::AnalysisMode == TEST_DISORIENTATIONS ) {
		
		idx->test_disorientations();
		//##MK::use the paraprobe-araullo method on specifically rotated clean crystal configurations

	}
	else { //ConfigIndexer::AnalysisMode == PERFORM_INDEXING
	
		if ( r == MASTER ) {

			if ( idx->read_directions_from_h5() == true ) {
				//defines the directions along which the results in ConfigIndexer::InputfileAraullo were mined
				cout << "Rank " << MASTER << " read directions from H5 successfully!" << "\n";
			}
			else {
				cerr << "Rank " << MASTER << " was unable to read directions!" << "\n"; localhealth = 0;
			}

			if ( idx->read_reference_templates_from_h5() == true ) {
				//defines the master intensity pattern for each phase used for indexing
				cout << "Rank " << MASTER << " read reference templates from H5 successfully!" << "\n";
			}
			else {
				cerr << "Rank " << MASTER << " was unable to read reference templates!" << "\n"; localhealth = 0;
			}
			if ( idx->read_materialpoint_ids() == true ) {
				//defines how many material point results were included in the ConfigIndexer::InputfileAraullo
				cout << "Rank " << MASTER << " read number of material points from H5 successfully!" << "\n";
			}
			else {
				cerr << "Rank " << MASTER << " was unable to read number of material points!" << "\n"; localhealth = 0;
			}
		}
		globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != nr ) {
			cerr << "Rank " << r << " has recognized that MASTER was unable to read directions and ref templates!" << "\n";
			delete idx; idx = NULL; return;
		}

		if ( idx->broadcast_directions() == true ) {
			cout << "Rank " << r << " has synchronized directions " << idx->directions.size() << "\n";
		}
		else {
			cerr << "Rank " << r << " failed to synchronize directions!" << "\n"; localhealth = 0;
		}
		ConfigIndexer::NumberOfS2Directions = idx->directions.size();

		if ( idx->broadcast_reference_templates() == true ) {
			cout << "Rank " << r << " has synchronized reference templates" << "\n";
		}
		else {
			cerr << "Rank " << r << " failed to synchronize reference templates!" << "\n"; localhealth = 0;
		}
		ConfigIndexer::NumberOfPhaseCandidates = idx->phlib.candidates.size();

		if ( idx->broadcast_matpoints() == true ) {
			cout << "Rank " << r << " has synchronized number of material points " << idx->mpid.size() << "\n";
		}
		else {
			cerr << "Rank " << r << " failed to synchronize number of material points!" << "\n"; localhealth = 0;
		}

		//we also need to initialize first the results file because there might be so many ROIs to index
		//that the processes cannot buffer all paraprobe-araullo before indexing and have thus to read/write incrementally
		//and work through portions of cache paraprobe-araullo results
		if ( r == MASTER ) {
			if ( idx->init_target_file() == true ) {
				cout << "Rank " << MASTER << " initialized the results file" << "\n";
			}
			else {
				cerr << "Rank " << MASTER << " failed to initialize the results file!" << "\n"; localhealth = 0;
			}
			if ( idx->write_environment_and_settings() == true ) {
				cout << "Rank " << MASTER << " environment and settings written!" << "\n";
			}
			else {
				cerr << "Rank " << MASTER << " writing environment and settings failed!" << "\n"; localhealth = 0;
			}
		}
		MPI_Barrier( MPI_COMM_WORLD );

		globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != nr ) {
			cerr << "Rank " << r << " has recognized that MASTER was unable to read directions, ref templates, and init results!" << "\n";
			delete idx; idx = NULL; return;
		}

		//now either all ranks have returned or they know a results file exists, so we can distribute workpackages
		if ( idx->distribute_matpoints_on_processes() == true ) {
			cout << "Rank " << r << " accepted its workpackages" << "\n";
		}
		else {
			cerr << "Rank " << r << " does not accept its workpackage!" << "\n"; localhealth = 0;
		}
		//check if all have accepted their workpackages and we do not ##MK::exceed data buffers
		globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != nr ) {
			cerr << "Rank " << r << " has recognized that not all were accepting their workpackage!" << "\n";
			delete idx; idx = NULL; return;
		}

		idx->directions_generate_kdtree();
		idx->phases_identify_peaks();

		//only once costly mapping of all possible rotations against all relevant, i.e. (geodesic grid) meshpoint with highest value per phase!
		if ( idx->directions_evaluate_rotations() == true ) {
			cout << "Rank " << r << " initialized the rotations library" << "\n";
		}
		else {
			cerr << "Rank " << r << " failed initialized in the rotations library" << "\n"; localhealth = 0;
		}
		//check if all have generated the match tables
		globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != nr ) {
			cerr << "Rank " << r << " has recognized that not all were able to initialize the rotations library!" << "\n";
			delete idx; idx = NULL; return;
		}

		//##MK::for now assume that enough ranks are used such that results fit in memory of each process at once
		//so only one I/O read phase, then processing/indexing, thereafter one I/O write phase
		//unsigned int nepochs = idx->plan_rawdata_import();
		//##MK::creates an I/O plan class object which tells the processes how the I/O intensity
		//read in and indexing workload will be splitted into multiple I/O steps

		//single I/O read in caching of results
		for( int rk = MASTER; rk < idx->get_nranks(); rk++ ) {
			if ( r == rk ) {
				idx->load_matpoints_araullo();
			}
			MPI_Barrier( MPI_COMM_WORLD );
		}

		if ( r == MASTER ) {
			cout << "Rank " << MASTER << " all processes have loaded paraprobe-araullo results!" << "\n";
		}

		//process-local parallelized indexing
		idx->local_indexing_epoch();

		//single I/O results write out
		for( int rk = MASTER; rk < idx->get_nranks(); rk++ ) {
			if ( r == rk ) {
				idx->write_solutions_to_h5();
			}
			MPI_Barrier( MPI_COMM_WORLD );
		}
	}
	
	idx->indexer_tictoc.spit_profiling( "Indexer", ConfigShared::SimID, idx->get_myrank() );

	//release resources
	delete idx; idx = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
	hello();

//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	//helloworld( argc, argv );

	if ( init( argc, argv ) == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks!" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else { 
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}
	cout << r << "-th MPI process initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";
	
//for this MPI/OMP/OpenACC, i.e. process/thread/accelerator heterogeneous program not every combination of mpiexec -n, OMP_NUM_THREADS and GPUs is admissible
	//##MK::if we dont want to use GPUs at all possible
	//##MK::current implementation initiates one MPI process per GPU on the node
	//##MK::problem is that the number of MPI processes per computing node is typically decided

	bool validsettings = true;
#ifdef UTILIZE_GPUS
	int devCount = acc_get_num_devices(acc_device_nvidia);
	if ( devCount < ConfigIndexer::GPUsPerNode ) {
		cerr << "Rank " << r << " there are only " << devCount << " GPUs on the node but user wanted " << ConfigIndexer::GPUsPerNode << "\n";
		validsettings = false;
	}
	else { //devCount >= GPUsPerNode
		//number of processes needs to be integer dividable through number of gpus
		if ( ConfigIndexer::GPUsPerNode > 0 ) {
			//MPI_Get_processor name algorithm to check on how many disjoint nodes the program instances are distributed
			//if nr > ConfigIndexer::GPUsPerNode*NumberOfNodesInUse == validsettings = false;
			if ( (nr % ConfigIndexer::GPUsPerNode) == 0 ) {
				//MK::e.g. 1 rank but 2 gpus: forbidden, 3 ranks but 2 gpus: forbidden, 4 ranks 2 gpus: allowed
				//##MK::2 processes per node, each using one gpu, 2 two such CPU/GPU pairs across the team of two nodes used
				//requires batch queue system variables like slurm ntasks-per-node=2 to be set properly
				cout << "GPUs will be utilized and number of MPI processes is an integer multiple of amount of GPUs planned" << "\n";
			}
			else {
				cerr << "GPUs should be used but number of MPI processes is not an integer multiple of GPUsPerNode!" << "\n";
				validsettings = false;
			}
		}
		//else{} GPUs exist but should not be used, setting remain valid will use sequential fallback!
	}
#endif

	if ( validsettings == true ) {
//EXECUTE SPECIFIC TASK
		orientation_and_phase_indexing( r, nr, argv );
	}
	
//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();

	double toc = omp_get_wtime();
	cout << "paraprobe-indexer took " << (toc-tic) << " seconds wall-clock time in total" << endl;
	
	return 0;
}
