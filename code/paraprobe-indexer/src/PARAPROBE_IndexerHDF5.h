//##MK::GPLV3

#ifndef __PARAPROBE_INDEXER_HDF_H__
#define __PARAPROBE_INDEXER_HDF_H__

//shared headers
#include "../../paraprobe-utils/src/PARAPROBE_APTH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_UtilsMetadataDefsH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_AraulloMetadataDefsH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_IndexerMetadataDefsH5.h"
#include "../../paraprobe-utils/src/PARAPROBE_XDMF.h"

//tool-specific headers
//#include "CONFIG_Indexer.h"
#include "PARAPROBE_IndexerCiteMe.h"


class indexer_h5 : public h5Hdl
{
	//tool-specific sub-class of a HDF5 inheriting all methods of the base class h5Hdl
	//coordinating instance handling all (sequential) writing to HDF5 file wrapping HDF5 low-level C library calls

public:
	indexer_h5();
	~indexer_h5();

	int create_indexer_apth5( const string h5fn );
	
//private:
};


#endif
