%% PARAPROBE.Indexer generate a equispaced grid in tricline orientation space
% Markus K\"uhbach, November 6, 2019

% requires: MTex >=5.0.3
% load MTex
	clear;
	clc;
	digits(32);
	format long;

%% Use MTex to generate SO3Grid('cubic','1')
    %cs = crystalSymmetry('C1'); %Schoenflies tricline with no inversion, i.e. 1
	gridresu = 1.0;
    cs = crystalSymmetry('Oh'); %Schoenflies cubic m-3m crystal symmetry
    so3g = equispacedSO3Grid(cs,'resolution',gridresu/180.0*pi); %'Bunge');
    %plot(so3g)

%% extract so3grid quaternion components
    tmp = normalize(quaternion(so3g));
    squat(1,:) = single(tmp.a); %angle argument of quaternion
    squat(2,:) = single(tmp.b); %q1
    squat(3,:) = single(tmp.c); %q2
    squat(4,:) = single(tmp.d); %q3
	clearvars tmp;
	
%% create InputfileTestOri.h5 file
    h5fn = 'PARAPROBE.Indexer.CubicDeg1SO3Grid.h5';
	grpnm = '/OriAndPhaseIndexing/Metadata/S2Grid/Quat'; %PARAPROBE_IDXR_META_ORI_QUAT
    h5create(h5fn, grpnm, size(squat));
    h5write(h5fn, grpnm, squat);
	
	save('PARAPROBE.Indexer.GenerateSO3Grid.mat','-v7.3');
	
%% done
    display(['Done']);
	

