//##MK::GPLV3

#include "CONFIG_Fourier.h"

VOLUME_SAMPLING_METHOD ConfigFourier::ROISamplingMethod = CUBOIDAL_GRID;
//string ConfigFourier::InputfilePSE = "";
string ConfigFourier::InputfileReconstruction;
string ConfigFourier::InputfileHullAndDistances;
//string ConfigFourier::Outputfile;

apt_real ConfigFourier::ROIRadiusMax = 0.f;
apt_real ConfigFourier::SamplingGridBinWidthX = 0.f;
apt_real ConfigFourier::SamplingGridBinWidthY = 0.f;
apt_real ConfigFourier::SamplingGridBinWidthZ = 0.f;
apt_real ConfigFourier::SignificantPksThreshold = 0.01;
apt_real ConfigFourier::DBScanSearchRadius = sqrt(2.0) * 1.0;
string ConfigFourier::SamplingPosition = "";
unsigned int ConfigFourier::FixedNumberOfROIs = 0;
unsigned int ConfigFourier::DBScanMinPeaks = 1;

hklgrid ConfigFourier::HKLGrid = hklgrid();

bool ConfigFourier::SamplingGridRemoveBndPoints = false;
bool ConfigFourier::IOFullGridPks = false;
bool ConfigFourier::IOSignificantPks = false;
bool ConfigFourier::IODBScanOnSgnPks = false;

int ConfigFourier::GPUsPerNode = 0;
int ConfigFourier::GPUWorkload = 10; //##MK::almost sure not optimal but sweep spot which number is optimal is case dependent, therefore here the possibility to do systematic studies


bool ConfigFourier::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.Input.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigFourier")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;

	//WHAT TYPE OF ANALYSIS TO DO
	mode = read_xml_attribute_uint32( rootNode, "VolumeSamplingMethod" );
	switch (mode)
	{
		case CUBOIDAL_GRID:
			ROISamplingMethod = CUBOIDAL_GRID; break;
		case SINGLE_POINT_USER:
			ROISamplingMethod = SINGLE_POINT_USER; break;
		case SINGLE_POINT_CENTER:
			ROISamplingMethod = SINGLE_POINT_CENTER; break;
		case RANDOM_POINTS_FIXED:
			ROISamplingMethod = RANDOM_POINTS_FIXED; break;
		default:
			cerr << "Unknown VolumeSamplingMethod!" << "\n";
			return false;
	}

	//InputfilePSE = read_xml_attribute_string( rootNode, "InputfilePeriodicTableOfElements" );
	InputfileReconstruction = read_xml_attribute_string( rootNode, "InputfileReconstruction" );
	InputfileHullAndDistances = read_xml_attribute_string( rootNode, "InputfileHullAndDistances" );
	//Outputfile = read_xml_attribute_string( rootNode, "Outputfile" );
	
	ROIRadiusMax = read_xml_attribute_float( rootNode, "ROIRadiusMax" );
	switch(ROISamplingMethod) {
		case CUBOIDAL_GRID:
			SamplingGridBinWidthX = read_xml_attribute_float( rootNode, "SamplingGridBinWidthX" );
			SamplingGridBinWidthY = read_xml_attribute_float( rootNode, "SamplingGridBinWidthY" );
			SamplingGridBinWidthZ = read_xml_attribute_float( rootNode, "SamplingGridBinWidthZ" );
			break;
		case SINGLE_POINT_USER:
			SamplingPosition = read_xml_attribute_string( rootNode, "SamplingPosition" );
			break;
		case SINGLE_POINT_CENTER:
			break;
		case RANDOM_POINTS_FIXED:
			FixedNumberOfROIs = read_xml_attribute_uint32( rootNode, "FixedNumberOfROIs" );
			break;
		default:
			return false;
	}

	SamplingGridRemoveBndPoints = read_xml_attribute_bool( rootNode, "RemoveROIsProtrudingOutside" );

	HKLGrid = hklgrid( 	read_xml_attribute_float( rootNode, "HKLGridBoundsHMin" ),
						read_xml_attribute_float( rootNode, "HKLGridBoundsHMax" ),
						read_xml_attribute_int32( rootNode, "HKLGridHVoxelResolution") );

	IOFullGridPks = read_xml_attribute_bool( rootNode, "IOStoreFullGridPeaks" );
	IOSignificantPks = read_xml_attribute_bool( rootNode, "IOStoreSignificantPeaks" );
	IODBScanOnSgnPks = read_xml_attribute_bool( rootNode, "IODBScanOnSignificantPeaks" );

	if ( IOSignificantPks == true ) {
		SignificantPksThreshold = read_xml_attribute_float( rootNode, "SignificantPeakIntensityThreshold" );
	}
	if ( IODBScanOnSgnPks == true ) {
		DBScanSearchRadius = read_xml_attribute_float( rootNode, "DBScanSearchRadius" );
		DBScanMinPeaks = read_xml_attribute_uint32( rootNode, "DBScanMinPksForReflector" );
	}

	GPUsPerNode = read_xml_attribute_int32( rootNode, "GPUsPerComputingNode" );
	GPUWorkload = read_xml_attribute_int32( rootNode, "GPUWorkload" );
	if ( GPUWorkload < 0 || GPUWorkload > 10000 ) {
		cout << "WARNING: The GPU workload has to be at least 1, the user input was errorneous, so I resetted to 10!" << "\n";
		GPUWorkload = 10;
	}
	
	return true;
}


bool ConfigFourier::checkUserInput()
{
	cout << "ConfigFourier::" << "\n";
	switch (ROISamplingMethod)
	{
		case CUBOIDAL_GRID:
			cout << "Building voxel volume grid at positions displaced from the specimen main axis" << "\n"; break;
		case SINGLE_POINT_USER:
			cout << "Building a single point user-specified at " << SamplingPosition << "\n"; break;
		case SINGLE_POINT_CENTER:
			cout << "Building a single point in the center of a specimen bounding box " << "\n"; break;
		case RANDOM_POINTS_FIXED:
			cout << "Building a cloud of randomly distribute ROI in the specimen bounding box " << "\n"; break;
		default:
			cerr << "Unknown Volume sampling method chosen" << "\n";
			return false;
	}

	if ( ROIRadiusMax < EPSILON ) {
		cerr << "ROIRadiusMax must be positive and finite!" << "\n"; return false;
	}
	cout << "ROIRadiusMax " << ROIRadiusMax << "\n";
	if ( ROISamplingMethod == CUBOIDAL_GRID ) {
		if ( SamplingGridBinWidthX < EPSILON ) {
			cerr << "SamplingGridBinWidthX needs to be positive and finite!" << "\n"; return false;
		}
		cout << "SamplingGridBinWidthX " << SamplingGridBinWidthX << "\n";
		if ( SamplingGridBinWidthY < EPSILON ) {
			cerr << "SamplingGridBinWidthY needs to be positive and finite!" << "\n"; return false;
		}
		cout << "SamplingGridBinWidthY " << SamplingGridBinWidthY << "\n";
		if ( SamplingGridBinWidthZ < EPSILON ) {
			cerr << "SamplingGridBinWidthY needs to be positive and finite!" << "\n"; return false;
		}
		cout << "SamplingGridBinWidthZ " << SamplingGridBinWidthZ << "\n";
	}
	if ( ROISamplingMethod == SINGLE_POINT_USER ) {
		stringstream parsethis;
		parsethis << SamplingPosition;
		string datapiece;
		size_t nsemicolon = count( SamplingPosition.begin(), SamplingPosition.end(), ';' );
		if ( nsemicolon != 2 ) {
			cerr << "SamplingPosition has invalid format must be x;y;z!" << "\n"; return false;
		}
		cout << "SamplingPosition " << SamplingPosition << "\n";
	}
	if ( ROISamplingMethod == RANDOM_POINTS_FIXED ) {
		if ( FixedNumberOfROIs < 1 ) {
			cerr << "FixedNumberOfROIs must be positive and finite!" << "\n"; return false;
		}
		cout << "FixedNumberOfROIs " << FixedNumberOfROIs << "\n";
	}

	if ( fabs(HKLGrid.imi) < EPSILON || fabs(HKLGrid.imx) < EPSILON || HKLGrid.imi >= HKLGrid.imx ) {
		cerr << "HKLGridBoundsHMin and Max have to span some finite positive distance!" << "\n"; return false;
	}
	if ( HKLGrid.ni < 32 || HKLGrid.ni > 1024 ) { //voxel grids use unsigned int
		cerr << "HKLGridVoxelResolution needs to be within interval [32, 1024] hkl grid points!" << "\n"; return false;
	}
	cout << "HKLGridBoundsHMin " << HKLGrid.imi << "\n";
	cout << "HKLGridBoundsHMax " << HKLGrid.imx << "\n";
	cout << "HKLGridBoundsKMin " << HKLGrid.jmi << "\n";
	cout << "HKLGridBoundsKMax " << HKLGrid.jmx << "\n";
	cout << "HKLGridBoundsLMin " << HKLGrid.kmi << "\n";
	cout << "HKLGridBoundsLMax " << HKLGrid.kmx << "\n";
	cout << "HKLGridHVoxelResolution " << HKLGrid.ni << "\n";
	cout << "HKLGridKVoxelResolution " << HKLGrid.nj << "\n";
	cout << "HKLGridLVoxelResolution " << HKLGrid.nk << "\n";

	cout << "IOStoreFullGridPeaks " << IOFullGridPks << "\n";
	cout << "IOStoreSignificantPeaks " << IOSignificantPks << "\n";
	cout << "IOStoreDBScanSgnPeaks" << IODBScanOnSgnPks << "\n";
	if ( IOSignificantPks == true ) {
		if ( SignificantPksThreshold < (1.0 / static_cast<apt_real>(255)) || SignificantPksThreshold >= (1.0 - EPSILON) ) {
			cerr << "SignificantPksThreshold must be between [" << 1.0 / static_cast<apt_real>(255) << ", 1.0)!" << "\n"; return false;
		}
		cout << "SignificantPksIntensityThreshold " << ConfigFourier::SignificantPksThreshold << "\n";
	}
	if( IODBScanOnSgnPks == true ) {
		if ( DBScanSearchRadius < EPSILON ) {
			cerr << "DBScanSearchRadius needs to be positive and finite on [" << TWO_PI*HKLGrid.imi << ", " << TWO_PI*HKLGrid.imx << ")" << "\n"; return false;
		}
		if ( DBScanSearchRadius > static_cast<apt_real>(HKLGrid.ni/2) ) {
			cerr << "DBScanSearchRadius must not be larger than " << HKLGrid.ni/2 << " !" << "\n"; return false;
		}
		if ( DBScanMinPeaks < 1 ) {
			cerr << "DBScanMinPksForReflector should be at least 1 !" << "\n"; return false;
		}
		cout << "DBScanSearchRadius " << DBScanSearchRadius << "\n";
		cout << "DBScanMinPksForReflector " << DBScanMinPeaks << "\n";
	}

	cout << "GPUsPerComputingNode " << GPUsPerNode << "\n";
	if ( GPUWorkload < 1 ) {
		cerr << "GPUWorkloadFactor needs to be integer and at least 1!" << "\n";
		cerr << "To delegate all work to CPUs instead set GPUsPerComputingNode to 0!" << "\n"; return false;
	}
	cout << "GPUWorkloadFactor " << GPUWorkload << "\n";
	
	//cout << "InputfilePeriodicTableOfElements read from " << InputfilePSE << "\n";
	cout << "InputReconstruction read from " << InputfileReconstruction << "\n";
	cout << "InputHullsAndDistances read from " << InputfileHullAndDistances << "\n";
	//cout << "Output written to " << Outputfile << "\n";
	
	if ( SamplingGridRemoveBndPoints == true ) {
		cout << "Material points outside the inside voxel or within closer than ROIRadiusMax to the triangle hull are discarded!" << "\n";
	}
	else {
		cout << "WARNING finite dataset boundary affects are not accounted for so some sampling points will have insignificant ion support!" << "\n";
	}
	
	cout << "\n";
	return true;
}


void ConfigFourier::reportSettings( vector<pparm> & res )
{
	//res.push_back( pparm( "InputfilePSE", InputfilePSE, "", "" ) );
	res.push_back( pparm( "InputfileReconstruction", InputfileReconstruction, "", "pre-computed reconstruction space x,y,z, and atom type" ) );
	res.push_back( pparm( "InputfileHullAndDistances", InputfileHullAndDistances, "", "pre-computed distances of ions to edge of dataset" ) );

	res.push_back( pparm( "VolumeSamplingMethod", sizet2str(ROISamplingMethod), "", "how to define where to place ROIs" ) );

	res.push_back( pparm( "ROIRadiusMax", real2str(ROIRadiusMax), "nm", "radius of the ROI" ) );
	res.push_back( pparm( "SamplingGridBinWidthX", real2str(SamplingGridBinWidthX), "nm", "for cuboidal ROI grid spacing between ROIs in x" ) );
	res.push_back( pparm( "SamplingGridBinWidthY", real2str(SamplingGridBinWidthY), "nm", "for cuboidal ROI grid spacing between ROIs in y" ) );
	res.push_back( pparm( "SamplingGridBinWidthZ", real2str(SamplingGridBinWidthZ), "nm", "for cuboidal ROI grid spacing between ROIs in z" ) );
	res.push_back( pparm( "SamplingPosition", SamplingPosition, "", "for single ROI, where to place in x, y, z" ) );
	res.push_back( pparm( "FixedNumberOfROIs", uint2str(FixedNumberOfROIs), "1", "for fixed number of ROIs, how many to place" ) );

	res.push_back( pparm( "SignificantPksThreshold", real2str(SignificantPksThreshold), "1.0", "above which (eventually normalised) intensity to consider the peak significant" ) );
	res.push_back( pparm( "DBScanSearchRadius", real2str(DBScanSearchRadius), "1.0", "equivalent to eps in DBScan, in which radius (fraction of voxels in reciprocal space) to search for neighboring peaks" ) );
	res.push_back( pparm( "DBScanMinPeaks", uint2str(DBScanMinPeaks), "1", "equivalent to k/K in DBScan, how many neighbors within radius/eps" ) );

	res.push_back( pparm( "HKLGridBoundsIMin", real2str(HKLGrid.imi), "", "for reciprocal space gridding, minimum position along i" ) );
	res.push_back( pparm( "HKLGridBoundsIIncr", uint2str(HKLGrid.ni), "", "for reciprocal space gridding, number of positions along i" ) );
	res.push_back( pparm( "HKLGridBoundsIMax", real2str(HKLGrid.imx), "", "for reciprocal space gridding, maximum position along i" ) );
	res.push_back( pparm( "HKLGridBoundsJMin", real2str(HKLGrid.jmi), "", "for reciprocal space gridding, minimum position along j" ) );
	res.push_back( pparm( "HKLGridBoundsJIncr", uint2str(HKLGrid.nj), "", "for reciprocal space gridding, number of positions along j" ) );
	res.push_back( pparm( "HKLGridBoundsJMax", real2str(HKLGrid.jmx), "", "for reciprocal space gridding, maximum position along i" ) );
	res.push_back( pparm( "HKLGridBoundsKMin", real2str(HKLGrid.kmi), "", "for reciprocal space gridding, minimum position along k" ) );
	res.push_back( pparm( "HKLGridBoundsKIncr", uint2str(HKLGrid.nk), "", "for reciprocal space gridding, number of positions along k" ) );
	res.push_back( pparm( "HKLGridBoundsKMax", real2str(HKLGrid.kmx), "", "for reciprocal space gridding, maximum position along k" ) );

	res.push_back( pparm( "SamplingGridRemoveBndPoints", sizet2str(SamplingGridRemoveBndPoints), "", "whether or not to have only ROIs completely embedded in dataset" ) );
	res.push_back( pparm( "IOFullGridPks", sizet2str(IOFullGridPks), "", "whether or not report FT magnitude for each reciprocal space grid point" ) );
	res.push_back( pparm( "IOSignificantPks", sizet2str(IOSignificantPks), "", "whether or not to compute/report only grid points with significant intensity" ) );
	res.push_back( pparm( "IODBScanOnSgnPks", sizet2str(IODBScanOnSgnPks), "", "whether or not to perform DBScan/report results to compress FT magnitude information" ) );

	res.push_back( pparm( "GPUsPerNode", uint2str(GPUsPerNode), "", "how many GPUs per node, make sure to have always one MPI process per GPU" ) );
	res.push_back( pparm( "GPUWorkload", uint2str(GPUWorkload), "", "how many ROIs per GPU and epoch" ) );
}

