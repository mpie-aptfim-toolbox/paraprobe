//##MK::GPLV3

#ifndef __PARAPROBE_CONFIG_FOURIER_H__
#define __PARAPROBE_CONFIG_FOURIER_H__

#include "../../paraprobe-utils/src/CONFIG_Shared.h"
#include "../../paraprobe-utils/src/PARAPROBE_FFT.h"


enum VOLUME_SAMPLING_METHOD {
	CUBOIDAL_GRID,
	SINGLE_POINT_USER,
	SINGLE_POINT_CENTER,
	RANDOM_POINTS_FIXED
};

struct hklgrid
{
	apt_real imi; //representing h
	apt_real imx;
	apt_real jmi; //k
	apt_real jmx;
	apt_real kmi; //l
	apt_real kmx;
	int ni;
	int nj;
	int nk;
	hklgrid() : imi(F32MX), imx(F32MI), jmi(F32MX), jmx(F32MI), kmi(F32MX), kmx(F32MI),
			ni(0), nj(0), nk(0) {}
	hklgrid( const apt_real _imin, const apt_real _imax, const int _ni ) :
		imi(_imin), imx(_imax), jmi(_imin), jmx(_imax), kmi(_imin), kmx(_imax),
			ni(_ni), nj(_ni), nk(_ni) {}
};


class ConfigFourier
{
public:
	
	static VOLUME_SAMPLING_METHOD ROISamplingMethod;
	//static string InputfilePSE;
	static string InputfileReconstruction;
	static string InputfileHullAndDistances;
	//static string Outputfile;
	
	static apt_real ROIRadiusMax;
	static apt_real SamplingGridBinWidthX;
	static apt_real SamplingGridBinWidthY;
	static apt_real SamplingGridBinWidthZ;
	static apt_real SignificantPksThreshold;
	static apt_real DBScanSearchRadius;
	static string SamplingPosition;
	static unsigned int FixedNumberOfROIs;
	static unsigned int DBScanMinPeaks;

	static hklgrid HKLGrid;

	static int GPUsPerNode;
	static int GPUWorkload;
	
	static bool SamplingGridRemoveBndPoints;
	static bool IOFullGridPks;
	static bool IOSignificantPks;
	static bool IODBScanOnSgnPks;
	
	//static unsigned int WorkloadPerProcess;
	//static unsigned int WorkloadPerThread;
		
	static bool readXML( string filename = "" );
	static bool checkUserInput();
	static void reportSettings( vector<pparm> & res );
};

#endif
