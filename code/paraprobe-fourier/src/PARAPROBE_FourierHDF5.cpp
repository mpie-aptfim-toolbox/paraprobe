//##MK::GPLV3

#include "PARAPROBE_FourierHDF5.h"
//https://www.learncpp.com/cpp-tutorial/114-constructors-and-initialization-of-derived-classes/


fourier_h5::fourier_h5()
{
}


fourier_h5::~fourier_h5()
{
}


int fourier_h5::create_fourier_apth5( const string h5fn )
{
	h5resultsfn = h5fn;
	fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );

	//generate a PARAPROBE APTH5 HDF5 file
	//##MK::catch errors

	//domain specific HDF5 keywords and data fields
	groupid = H5Gcreate2(fileid, PARAPROBE_FOURIER, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "PARAPROBE_FOURIER " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_FOURIER_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_FOURIER_META failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_FOURIER_META failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}
cout << "PARAPROBE_FOURIER_META " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_FOURIER_META_MP, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "PARAPROBE_FOURIER_META_MP " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_FOURIER_META_HKL, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "PARAPROBE_FOURIER_META_HKL " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_FOURIER_RES, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
	status = H5Gclose(groupid);
cout << "PARAPROBE_FOURIER_RES " << status << "\n";

	groupid = H5Gcreate2(fileid,  PARAPROBE_FOURIER_RES_HKL, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << " PARAPROBE_FOURIER_RES_HKL " << status << "\n";

	status = H5Fclose(fileid);
cout << "Closing APTH5 file" << "\n";

	return WRAPPED_HDF5_SUCCESS;
}


int fourier_h5::create_fourier2_apth5( const string h5fn )
{
	h5resultsfn = h5fn;
	fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );
	if ( fileid < 0 ) {
		cerr << "Create fourier h5 file creation failed!" << fileid << "\n"; return WRAPPED_HDF5_FAILED;
	}

	//generate a PARAPROBE APTH5 HDF5 file
	//domain specific HDF5 keywords and data fields
	groupid = H5Gcreate2(fileid, PARAPROBE_FOURIER, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_FOURIER failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_FOURIER failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid, PARAPROBE_FOURIER_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_FOURIER_META failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_FOURIER_META failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid, PARAPROBE_FOURIER_META_HRDWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_FOURIER_META_HRDWR failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_FOURIER_META_HRDWR failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid, PARAPROBE_FOURIER_META_SFTWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_FOURIER_META_SFTWR failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_FOURIER_META_SFTWR failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid, PARAPROBE_FOURIER_META_MP, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_FOURIER_META_MP failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_FOURIER_META_MP failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid, PARAPROBE_FOURIER_META_PHCAND, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_FOURIER_META_PHCAND failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_FOURIER_META_PHCAND failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid, PARAPROBE_FOURIER_META_HKL, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_FOURIER_META_HKL failed!" << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_FOURIER_META_HKL failed!" << status << "\n"; return WRAPPED_HDF5_FAILED;
	}


	groupid = H5Gcreate2(fileid, PARAPROBE_FOURIER_RES, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_ARAULLO_RES failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_ARAULLO_RES failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	if ( ConfigFourier::IOFullGridPks == true ) {
		groupid = H5Gcreate2(fileid, PARAPROBE_FOURIER_RES_HKLGRID, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_FOURIER_RES_HKLGRID failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_FOURIER_RES_HKLGRID failed!" << status << "\n";
		}
	}

	if ( ConfigFourier::IOSignificantPks == true ) {
		groupid = H5Gcreate2(fileid, PARAPROBE_FOURIER_RES_HKLREFL, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_FOURIER_RES_HKLREFL failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_FOURIER_RES_HKLREFL failed!" << status << "\n";
		}
	}

	if ( ConfigFourier::IODBScanOnSgnPks == true ) {
		groupid = H5Gcreate2(fileid, PARAPROBE_FOURIER_RES_HKLDBSCAN, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_FOURIER_RES_HKLDBSCAN failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_FOURIER_RES_HKLDBSCAN failed!" << status << "\n";
		}
	}

	//add environment and tool specific settings
	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_HRDWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_HRDWR failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_HRDWR failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_HRDWR_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_HRDWR_META failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_HRDWR_META failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_HRDWR_META_KEYS, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_HRDWR_META_KEYS failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_HRDWR_META_KEYS failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_SFTWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_SFTWR failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARARPROBE_UTILS_SFTWR failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_SFTWR_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_SFTWR_META failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_SFTWR_META failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid,  PARAPROBE_UTILS_SFTWR_META_KEYS, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_UTILS_SFTWR_META_KEYS failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_UTILS_SFTWR_META_KEYS failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	status = H5Fclose(fileid);
	if ( status < 0 ) {
		cerr << "Close file " << h5resultsfn << " failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	return WRAPPED_HDF5_SUCCESS;
}
