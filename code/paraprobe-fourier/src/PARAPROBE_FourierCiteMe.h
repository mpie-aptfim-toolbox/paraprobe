//##MK::GPLV3

#ifndef __PARAPROBE_FOURIER_CITEME_H__
#define __PARAPROBE_FOURIER_CITEME_H__

#include "CONFIG_Fourier.h"


class CiteMeFourier
{
public:
	
	static vector<bibtex> Citations;
	
	static size_t cite();
};

#endif

