//##MK::GPLV3

#include "PARAPROBE_FourierHdl.h"


fourierHdl::fourierHdl()
{
	myrank = MASTER;
	nranks = 1;

	maximum_iontype_uc = 0;
}


fourierHdl::~fourierHdl()
{
	for( size_t i = 0; i < workers.size(); i++ ) {
		if ( workers.at(i) != NULL ) {
			delete workers.at(i);
			workers.at(i) = NULL;
		}
	}
}


bool fourierHdl::read_periodictable()
{
	//all read PeriodicTable
	/*
	if ( rng.nuclides.load_isotope_library( ConfigFourier::InputfilePSE ) == false ) {
		cerr << "Rank " << get_myrank() << " loading of PeriodicTableOfElements failed!" << "\n"; return false;
	}
	*/
	if ( rng.nuclides.load_isotope_library() == false ) {
		cerr << "Rank " << get_myrank() << " loading of Config_SHARED PeriodicTableOfElements failed!" << "\n"; return false;
	}
	cout << "Rank " << get_myrank() << " loaded rng.nuclides.isotopes.size() " << rng.nuclides.isotopes.size() << " isotopes" << "\n";
	return true;
}


bool fourierHdl::read_iontype_combinations( string fn )
{
	//all read IontypeCombinations
	if ( itsk.load_iontype_combinations( fn ) == false ) {
		cerr << "Rank " << get_myrank() << " loading of IontypeCombinations failed!" << "\n"; return false;
	}
	cout << "Rank " << get_myrank() << " itsk.icombis.size() " << itsk.icombis.size() << " icombis " << "\n";
	return true;
}


bool fourierHdl::read_reconxyz_from_apth5()
{
	double tic = MPI_Wtime();

	//load xyz positions
	if ( inputReconH5Hdl.read_xyz_from_apth5( ConfigFourier::InputfileReconstruction, xyz ) == true ) {
		cout << ConfigFourier::InputfileReconstruction << " read positions successfully" << "\n";
	}
	else {
		cerr << ConfigFourier::InputfileReconstruction << " read positions failed!" << "\n"; return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "ReadReconXYZFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool fourierHdl::read_ranging_from_apth5()
{
	double tic = MPI_Wtime();

	//load ranging information
	if ( inputReconH5Hdl.read_iontypes_from_apth5( ConfigFourier::InputfileReconstruction, rng.iontypes ) == true ) {
		cout << ConfigFourier::InputfileReconstruction << " read ranging information successfully" << "\n";

		//create iontype_dictionary
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
			size_t id = static_cast<size_t>(it->id);
			size_t hashval = rng.hash_molecular_ion( it->strct.Z1, it->strct.Z2, it->strct.Z3 );
	cout << id << "\t\t" << hashval << "\t\t" << (int) it->strct.Z1 << ";" << (int) it->strct.Z2 << ";" << (int) it->strct.Z3 << "\n";
			if ( id != 0 && hashval != 0 ) { //skip adding existent UNKNOWN_IONTYPE
				auto jt = rng.iontypes_dict.find( hashval );
				if ( jt == rng.iontypes_dict.end() ) { //really a new iontype for which not yet mqival were defined
	//cout << "--->Adding new type " << id << "\t\t" << hashval << "\n";
					//add iontype in dictionary
					rng.iontypes_dict.insert( pair<size_t,size_t>( hashval, id ) );
				}
				else {
	cerr << "--->Detected an iontype where the triplet Z1, Z2, Z3 is a duplicate, which should not happen as iontypes have to be mutually exclusive!" << "\n";
					return false;
	//cout << "--->Adding to an existent type " << id << "\t\t" << hashval << "\t\t" << jt->second << "\n";
					//match to an already existent type
					//rng.iontypes_dict.insert( pair<size_t,size_t>( hashval, id ) ); //jt->second ) );
					//##MK::this fix makes it de facto a charge state specific 8;0,0;0;1 and 8;0;0;0;0;0;0;1 and 8;0;0;0;0;0;0;2 map to different types
					//return false; //##MK::switched of for FAU APT School
				}
			}
			else {
	cout << "--->Me skipping the UNKNOWN_IONTYPE" << "\n";
			}
		}

		//##MK::BEGIN DEBUG, make sure we use have correct ranging data
		cout << "--->Debugging ranging data..." << "\n";
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
			size_t id = static_cast<size_t>(it->id);
			size_t hashval = rng.hash_molecular_ion( it->strct.Z1, it->strct.Z2, it->strct.Z3 );
			cout << id << "\t\t" << hashval << "\t\t" << (int) it->strct.Z1 << ";" << (int) it->strct.Z2 << ";" << (int) it->strct.Z3 << "\n";
			for( auto jt = it->rng.begin(); jt != it->rng.end(); jt++ ) {
				cout << "\t\t\t" << jt->lo << "\t\t" << jt->hi << "\n";
			}
		}
		cout << "--->Debugging ion dictionary..."  << "\n";
		for( auto kt = rng.iontypes_dict.begin(); kt != rng.iontypes_dict.end(); kt++ ) {
			cout << kt->first << "\t\t" << kt->second << "\n";
		}
		//##MK::END DEBUG
	}
	else {
		cerr << ConfigFourier::InputfileReconstruction << " read ranging information failed!" << "\n"; return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "ReadRangingDataFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool fourierHdl::read_itypes_from_apth5()
{
	double tic = MPI_Wtime();

	//load actual itypes per xyz position
	if ( inputReconH5Hdl.read_ityp_from_apth5( ConfigFourier::InputfileReconstruction, ityp_org ) == true ) {
		cout << ConfigFourier::InputfileReconstruction << " read itypes successfully" << "\n";
	}
	else {
		cerr << ConfigFourier::InputfileReconstruction << " read itypes failed!" << "\n"; return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "ReadIontypesFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

    return true;
}


bool fourierHdl::read_trianglehull_from_apth5()
{
	double tic = MPI_Wtime();

	if ( ConfigFourier::InputfileHullAndDistances.substr(ConfigFourier::InputfileHullAndDistances.length()-3) != ".h5" ) {
		cerr << "The InputfileHullAndDistances " << ConfigFourier::InputfileHullAndDistances << " has not the proper h5 file extension!" << "\n";
		return false;
	}

	inputTriHullH5Hdl.h5resultsfn = ConfigFourier::InputfileHullAndDistances;
	if ( inputTriHullH5Hdl.read_trianglehull_from_apth5( ConfigFourier::InputfileHullAndDistances, trianglehull ) == true ) {
		cout << ConfigFourier::InputfileHullAndDistances << " triangle hull read successfully" << "\n";
	}
	else {
		cerr << ConfigFourier::InputfileHullAndDistances << " triangle hull read failed!" << "\n";
		return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "ReadTriangleHullFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool fourierHdl::read_voxelized_from_apth5()
{
	double tic = MPI_Wtime();

	if ( ConfigFourier::InputfileHullAndDistances.substr(ConfigFourier::InputfileHullAndDistances.length()-3) != ".h5" ) {
		cerr << "The InputfileHullAndDistances " << ConfigFourier::InputfileHullAndDistances << " has not the proper h5 file extension!" << "\n";
		return false;
	}

	if ( inputTriHullH5Hdl.read_voxelized_from_apth5( ConfigFourier::InputfileHullAndDistances, ca ) == true ) {
		cout << ConfigFourier::InputfileHullAndDistances << " voxelized read successfully" << "\n";
	}
	else {
		cerr << ConfigFourier::InputfileHullAndDistances << " voxelized read failed!" << "\n";
		return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "ReadVoxelizationFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;

}


bool fourierHdl::broadcast_reconxyz()
{
	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( xyz.size() >= (INT32MX-1) ) {
			cerr << "Current implementation does not handle the case of larger than 2 billion ion dataset transfer!" << "\n"; localhealth = 0;
		}
		if ( xyz.size() == 0 ) {
			cerr << "Ion positions is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the size and state of the dataset!" << "\n";
		return false;
	}

	//dataset on the master is okay, but do we need to broadcast at all to others, i.e. are there slaves?
	if ( get_nranks() < 2 ) {
		cout << "Skipping broadcast_reconxyz because single process execution" << "\n";
		return true;
	}

	//communicate size of the master's dataset
	int nevt = 0;
	if ( get_myrank() == MASTER ) {
		nevt = static_cast<size_t>(xyz.size());
	}
	MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	MPI_IonPositions* buf = NULL;
	try {
		buf = new MPI_IonPositions[nevt];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for buf!" << "\n"; localhealth = 0;
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL; return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < xyz.size(); ++i ) {
			buf[i] = MPI_IonPositions( xyz[i] );
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, nevt, MPI_IonPositions_Type, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			xyz.reserve( static_cast<size_t>(nevt) );
			//dist2hull.reserve( static_cast<size_t>(nevt) );
			for( int i = 0; i < nevt; i++ ) {
				xyz.push_back( p3d( buf[i].x, buf[i].y, buf[i].z ) );
				//dist2hull.push_back( buf1[i].d ); //##MK::fill with dummy values here!
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";
		return false;
	}
	else {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
	}

	/*
	cout << "Rank " << get_myrank() << " listened to MASTER xyz.size() " << xyz.size() << "\n";
	cout << "Rank " << get_myrank() << " listened to MASTER dist2hull.size() " << dist2hull.size() << "\n";
	cout << "Rank " << get_myrank() << " listened to MASTER triangle_hull.size() " << triangle_hull.size() << "\n";
	*/

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "BroadcastReconXYZ", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


bool fourierHdl::broadcast_ranging()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
		return true;
	}

	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( rng.iontypes.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " iontypesinfo!" << "\n"; localhealth = 0;
		}
		if ( rng.iontypes.size() == 0 ) {
			cerr << "MASTER's rng.iontypes is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
		if ( ityp_org.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " ityp_org per ion!" << "\n"; localhealth = 0;
		}
		if ( ityp_org.size() == 0 ) {
			cerr << "MASTER's ityp_org is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the MASTER's ranging size and state!" << "\n"; return false;
	}

	//communicate metadata first
	int nrng_ivl_evt_dict[4] = {0, 0, 0, 0};
	if ( get_myrank() == MASTER ) {
		nrng_ivl_evt_dict[0] = static_cast<int>(rng.iontypes.size());
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
			nrng_ivl_evt_dict[1] += static_cast<int>(it->rng.size());
		}
		nrng_ivl_evt_dict[2] = static_cast<int>(ityp_org.size());
		nrng_ivl_evt_dict[3] = static_cast<int>(rng.iontypes_dict.size());
	}
	MPI_Bcast( nrng_ivl_evt_dict, 4, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	MPI_Ranger_Iontype* buf1 = NULL;
	MPI_Ranger_MQIval* buf2 = NULL;
	unsigned char* buf3 = NULL;
	MPI_Ranger_DictKeyVal* buf4 = NULL;
	try {
		buf1 = new MPI_Ranger_Iontype[nrng_ivl_evt_dict[0]];
		buf2 = new MPI_Ranger_MQIval[nrng_ivl_evt_dict[1]];
		buf3 = new unsigned char[nrng_ivl_evt_dict[2]];
		buf4 = new MPI_Ranger_DictKeyVal[nrng_ivl_evt_dict[3]];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for buf1, buf2, buf3, and buf4!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf1; buf1 = NULL;
		delete [] buf2; buf2 = NULL;
		delete [] buf3; buf3 = NULL;
		delete [] buf4; buf4 = NULL;
		return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		size_t iv = 0;
		size_t id = 0;
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++, id++ ) {
			unsigned char typid = static_cast<unsigned char>(it->id);
			buf1[id] = MPI_Ranger_Iontype( typid, it->strct.Z1, it->strct.N1, it->strct.Z2, it->strct.N2,
												it->strct.Z3, it->strct.N3, it->strct.sign, it->strct.charge );
			for( auto jt = it->rng.begin(); jt != it->rng.end(); jt++, iv++ ) {
				buf2[iv] = MPI_Ranger_MQIval( jt->lo, jt->hi, typid );
			}
		}
		for( size_t i = 0; i < ityp_org.size(); i++ ) {
			buf3[i] = ityp_org.at(i);
		}
		size_t ii = 0;
		for( auto kvt = rng.iontypes_dict.begin(); kvt != rng.iontypes_dict.end(); kvt++, ii++ ) {
			buf4[ii] = MPI_Ranger_DictKeyVal( static_cast<unsigned long long>(kvt->first), static_cast<unsigned long long>(kvt->second) );
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf1, nrng_ivl_evt_dict[0], MPI_Ranger_Iontype_Type, MASTER, MPI_COMM_WORLD );
	MPI_Bcast( buf2, nrng_ivl_evt_dict[1], MPI_Ranger_MQIval_Type, MASTER, MPI_COMM_WORLD);
	MPI_Bcast( buf3, nrng_ivl_evt_dict[2], MPI_UNSIGNED_CHAR, MASTER, MPI_COMM_WORLD );
	MPI_Bcast( buf4, nrng_ivl_evt_dict[3], MPI_Ranger_DictKeyVal_Type, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			rng.iontypes.reserve( static_cast<size_t>(nrng_ivl_evt_dict[0]) );
			for( int i = 0; i < nrng_ivl_evt_dict[0]; i++ ) {
				rng.iontypes.push_back( rangedIontype() );
				rng.iontypes.back().id = static_cast<unsigned int>( buf1[i].id );
				rng.iontypes.back().strct = evapion3( buf1[i].Z1, buf1[i].N1, buf1[i].Z2, buf1[i].N2, buf1[i].Z3, buf1[i].N3, buf1[i].sign, buf1[i].charge );
				for( int j = 0; j < nrng_ivl_evt_dict[1]; j++ ) { //was [2], ##MK:avoid O(N^2) search, but should not be a problem because N << 256
					if ( static_cast<unsigned int>(buf2[j].id) != rng.iontypes.back().id ) {
						continue;
					}
					else {
						rng.iontypes.back().rng.push_back( mqival( buf2[j].low, buf2[j].high ) );
					}
				}
			}
			ityp_org.reserve( static_cast<size_t>(nrng_ivl_evt_dict[2]) );
			for( int k = 0; k < nrng_ivl_evt_dict[2]; k++ ) {
				ityp_org.push_back( buf3[k] );
			}

			for ( int kv = 0; kv < nrng_ivl_evt_dict[3]; kv++ ) {
				rng.iontypes_dict.insert( pair<size_t,size_t>(
						static_cast<size_t>(buf4[kv].key), static_cast<size_t>(buf4[kv].val) ) );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout to instantiate rng.iontypes and ityp_org!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf1; buf1 = NULL;
	delete [] buf2; buf2 = NULL;
	delete [] buf3; buf3 = NULL;
	delete [] buf4; buf4 = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";	return false;
	}

	//cout << "Rank " << get_myrank() << " listened to MASTER reconstruction.size() " << reconstruction.size() << "\n";
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "BroadcastRanging", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


bool fourierHdl::broadcast_triangles()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
		return true;
	}

	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( trianglehull.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " triangles!" << "\n"; localhealth = 0;
		}
		if ( trianglehull.size() == 0 ) {
			cerr << "Trianglehull is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the size and state of the trianglehull datasets!" << "\n";
		return false;
	}

	//communicate size of the master's dataset
	int nevt = 0;
	if ( get_myrank() == MASTER ) {
		nevt = static_cast<int>(trianglehull.size());
	}
	MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	MPI_Triangle3D* buf = NULL;
	try {
		buf = new MPI_Triangle3D[nevt];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for MPI_Triangle3D buffer!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL; return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < trianglehull.size(); i++ ) {
			buf[i] = MPI_Triangle3D( trianglehull.at(i) );
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, nevt, MPI_Triangle3D_Type, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			trianglehull.reserve( static_cast<size_t>(nevt) );
			for( int i = 0; i < nevt; i++ ) {
				trianglehull.push_back( tri3d( 	buf[i].x1, buf[i].y1, buf[i].z1,
												buf[i].x2, buf[i].y2, buf[i].z2,
												buf[i].x3, buf[i].y3, buf[i].z3  ) );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf1 readout!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";
		return false;
	}

//cout << "Rank " << get_myrank() << " listened to MASTER trianglehull.size() " << trianglehull.size() << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "BroadcastTriangleHull", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


bool fourierHdl::broadcast_vxlized()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
		return true;
	}

	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( ca.vxlgrid.nxyz >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " voxel!" << "\n"; localhealth = 0;
		}
		if ( ca.vxlgrid.nxyz == 0 ) {
			cerr << "Voxelgrid is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the size and state of the voxelgrid datasets!" << "\n";
		return false;
	}

	//communicate size of the master's dataset
	MPI_VxlizationInfo ifo = MPI_VxlizationInfo();
	if ( get_myrank() == MASTER ) {
		ifo = MPI_VxlizationInfo( ca.vxlgrid.box, ca.vxlgrid.width, ca.vxlgrid.width, ca.vxlgrid.width, ca.vxlgrid.nx, ca.vxlgrid.ny, ca.vxlgrid.nz );
	}
	MPI_Bcast( &ifo, 1, MPI_VxlizationInfo_Type, MASTER, MPI_COMM_WORLD );

	if ( get_myrank() != MASTER ) {
		ca.vxlgrid.nx = ifo.nx;
		ca.vxlgrid.ny = ifo.ny;
		ca.vxlgrid.nz = ifo.nz;
		ca.vxlgrid.nxy = ifo.nx*ifo.ny;
		ca.vxlgrid.nxyz = ifo.nx*ifo.ny*ifo.nz;
		ca.vxlgrid.width = ifo.dx;
		ca.vxlgrid.box = aabb3d( ifo.xmi, ifo.xmx, ifo.ymi, ifo.ymx, ifo.zmi, ifo.zmx );
		ca.vxlgrid.box.scale();
	}

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	unsigned char* buf = NULL;
	try {
		buf = new unsigned char[ca.vxlgrid.nxyz];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for unsigned char buffer!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL; return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < ca.vxlgrid.nxyz; i++ ) {
			buf[i] = ( ca.IsInside[i] == true ) ? IS_INSIDE : IS_VACUUM;
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, static_cast<int>(ca.vxlgrid.nxyz), MPI_UNSIGNED_CHAR, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		if ( ca.IsInside == NULL ) {
			try {
				ca.IsInside = new bool[ca.vxlgrid.nxyz];
				for( size_t i = 0; i < ca.vxlgrid.nxyz; i++ ) {
					ca.IsInside[i] = (buf[i] == IS_INSIDE ) ? true : false;
				}
			}
			catch(bad_alloc &mecroak) {
				cerr << "Rank " << get_myrank() << " allocation error during buf1 readout!" << "\n"; localhealth = 0;
			}
		}
		else {
			cerr << "Rank " << get_myrank() << " ca.IsInside was already allocated!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";
		return false;
	}

//cout << "Rank " << get_myrank() << " listened to MASTER ca.vxlgrid.nxyz " << ca.vxlgrid.nxyz << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "BroadcastVoxelgrid", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


bool fourierHdl::broadcast_distances()
{
cerr << "broadcast_distances currently not implemented, using dummy values at the moment!" << "\n";
	double tic = MPI_Wtime();

	try {
		dist2hull = vector<apt_real>( xyz.size(), F32MX );
	}
	catch (bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation of dist2hull failed!" << "\n";
		return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "BroadcastDistances", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}

/*
bool fourierHdl::broadcast_reconstruction_and_triangles()
{
	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far know state and size of the datasets!
		if ( xyz.size() >= (INT32MX-1) ) {
			cerr << "Current implementation does not handle the case of larger than 2 billion ion dataset transfer!" << "\n";
			localhealth = 0;
		}
		if ( xyz.size() != dist2hull.size() ) {
			cerr << "MASTER has a different number of ion positions than distance values!" << "\n";
			localhealth = 0;
		}
		if ( xyz.size() == 0 ) {
			cerr << "Ion positions is empty, so nothing to communicate!" << "\n";
			localhealth = 0;
		}
		if ( dist2hull.size() == 0 ) {
			cerr << "Ion distances is empty, so nothing to communicate!" << "\n";
			localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the size and state of the dataset!" << "\n";
		return false;
	}

	//shortcut if only a single MPI process across MPI_COMM_WORLD
	if ( get_nranks() > 1 ) {

		//communicate size of the master's dataset
		int nevt = 0;
		if ( get_myrank() == MASTER ) {
			nevt = static_cast<size_t>(xyz.size());
		}
		MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

		//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
		localhealth = 1;
		MPI_IonWithDistance* buf1 = NULL;
		MPI_Triangle3D* buf2 = NULL;
		try {
			buf1 = new MPI_IonWithDistance[nevt];
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error for buf1 and/or buf2" << "\n";
			localhealth = 0;
		}
		globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != get_nranks() ) {
			cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
			delete [] buf1; buf1 = NULL;
			delete [] buf2; buf2 = NULL;
			return false;
		}

		//master populates communication buffer
		if ( get_myrank() == MASTER ) {
			for( size_t i = 0; i < xyz.size(); ++i ) {
				buf1[i] = MPI_IonWithDistance( xyz[i], dist2hull[i] );
			}
		}

		//collective call MPI_Bcast, MASTER broadcasts, slaves listen
		MPI_Bcast( buf1, nevt, MPI_IonWithDistance_Type, MASTER, MPI_COMM_WORLD );

		//slave nodes pull from buffer
		if ( get_myrank() != MASTER ) {
			try {
				xyz.reserve( static_cast<size_t>(nevt) );
				dist2hull.reserve( static_cast<size_t>(nevt) );
				for( int i = 0; i < nevt; i++ ) {
					xyz.push_back( p3d( buf1[i].x, buf1[i].y, buf1[i].z ) );
					dist2hull.push_back( buf1[i].d ); //##MK::fill with dummy values here!
				}
			}
			catch(bad_alloc &mecroak) {
				cerr << "Rank " << get_myrank() << " allocation error during buf1 readout!" << "\n";
				localhealth = 0;
			}
		}

		//all release the buffer, delete on NULL pointer gets compiled as no op
		delete [] buf1; buf1 = NULL;
		delete [] buf2; buf2 = NULL;

		//final check whether all were successful so far
		globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != get_nranks() ) {
			cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";
			return false;
		}
	}
	else {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
	}

	//cout << "Rank " << get_myrank() << " listened to MASTER xyz.size() " << xyz.size() << "\n";
	//cout << "Rank " << get_myrank() << " listened to MASTER dist2hull.size() " << dist2hull.size() << "\n";
	//cout << "Rank " << get_myrank() << " listened to MASTER triangle_hull.size() " << triangle_hull.size() << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "BroadcastDataAcrossMPIProcesses", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}
*/

bool fourierHdl::define_phase_candidates()
{
	double tic = MPI_Wtime();

	size_t max_memory_per_node = fourier_tictoc.get_memory_max_on_node();
	size_t max_memory_for_buf = 0.1 * max_memory_per_node;

	cout << "Rank " << get_myrank() << " max_memory_per_node " << max_memory_per_node << " B" << "\n";
	cout << "Rank " << get_myrank() << " max_memory_for_buf " << max_memory_for_buf << " B" << "\n";

	//firstly, we transform the human-readable icombis targ/nbor strings pairs into evapion3 objects
	vector<Evapion3Combi> tmp;
	for( auto it = itsk.icombis.begin(); it != itsk.icombis.end(); it++ ) {
		tmp.push_back( Evapion3Combi() );

		rng.add_evapion3( it->targets, tmp.back().targets );

/*
#ifdef EMPLOY_SINGLEPRECISION
		apt_real parse_realspace = stof( it->realspacestr );
#else
		apt_real parse_realspace = stod( it->realspacestr );
#endif
		if ( parse_realspace < EPSILON || parse_realspace > ConfigFourier::ROIRadiusMax ) {
			cerr << "Rank " << get_myrank() << " attempt to use an invalid realspace value in a an evapion3 ityp combi, combi will be ignored!" << "\n";
		}
		else {
			tmp.back().realspace = parse_realspace;
		}
*/
		/*
		//##BEGIN DEBUG
		cout << "Rank " << get_myrank() << " targets __" << it->targets << "__ ---->" << "\n";
		for( auto jt = tmp.back().targets.begin(); jt != tmp.back().targets.end(); jt++ ) {
			cout << (int) jt->Z1 << ";" << (int) jt->Z2 << ";" << (int) jt->Z3  << "\n";
		}
		cout << "Rank " << get_myrank() << " realspace __" << it->realspacestr << "__ ---->" << "\n";
		cout << tmp.back().realspace << "\n";
		//##END DEBUG
		*/
	}

	//secondly, we have to understand that these user-combinations may not at all be present in the ranging data, i.e.
	//are possibly not all evapion3 types that we identified in the analyzed dataset as ranging data,
	//sure, in most cases user will make correct input but in every case we need to make sure
	map<size_t,size_t>::iterator does_not_exist = rng.iontypes_dict.end();
	for( auto jt = tmp.begin(); jt != tmp.end(); jt++ ) {
		UC8Combi valid;
		for( auto tg = jt->targets.begin(); tg != jt->targets.end(); tg++ ) {
			size_t hashval = rng.hash_molecular_ion( tg->Z1, tg->Z2, tg->Z3 );
			//MK::it is important to understand here that the user is not enforced to write the molecular ions
			//within the XML input file in order, because above preprocessing assures that combinations such as AlH: or H:Al will be sorted
			//into a unique representation namely Al, H, UNOCCUPIED as it holds also for the ranging
			//Consequently, internal ion type handling is handling commutativity to please the user
			map<size_t,size_t>::iterator vt = rng.iontypes_dict.find( hashval );
			if ( vt != does_not_exist ) { //kt is an ion type which exists within the ranging data
//cout << " Adding an identified ion from the ranging process as target\t\t" << vt->first << "\t\t" << vt->second << "\n";
				valid.targets.push_back( vt->second );
			}
		}
		//valid.realspace = jt->realspace;

		itsk.combinations.push_back( UC8Combi() );
		for( auto tgs = valid.targets.begin(); tgs != valid.targets.end(); tgs++ ) { //explicit deep copy
			itsk.combinations.back().targets.push_back( *tgs );
		}
		//itsk.combinations.back().realspace = valid.realspace;
	}

	//get rid of temporaries
	tmp = vector<Evapion3Combi>();

	//thirdly, and now having the IontypeCombinations we plan the actual PhaseCandidates or tasks
	//the key idea here is that we conduct eventually multiple Araullo Peters analyses per ROI
	//think like indexing in Electron Backscatter Diffraction (EBSD): we need to know crystal unit cells of possible candidates
	//with which to run the analysis a priori. for araullo it is the same, we probe the local spatial arrangement of
	//specific iontypes and for each such ityp candidate we probe intensity of a specific Fourier spectrum magnitude bin
	//as we know that for a pristine crystal of this phase and sub-lattice we would expect lattice planes in the unrotated
	//configuration, we can work in the unrotated configuration because we probe along different direction
	//one direction will be strong and aligned with the crystal [uvw] regardless the orientation if the grid of SDMs is
	//probed accurately


	unsigned int tskid = 0; //scientific analysis taskIDs start at 0
	size_t max_memory_expected = 0; //in bytes

	if ( itsk.combinations.size() < 1 ) {
		cerr << "Rank " << get_myrank() << " combinations were specified but not parsable after having evaluated InputfileReconstruction ranging data and settings!" << "\n";
		return false;
	}

	for( auto it = itsk.combinations.begin(); it != itsk.combinations.end(); it++ ) {
		if ( it->targets.size() <= TARGET_LIST_SIZE ) {

			itsk.iphcands.push_back( PhaseCandidate() );
			size_t i = 0;
			for(       ; i < it->targets.size(); i++ ) {
				itsk.iphcands.back().targets[i] = ( static_cast<int>(it->targets.at(i)) < static_cast<int>(UCHARMX) ) ?
						it->targets.at(i) : 0xFF;
			}
			for(       ; i < TARGET_LIST_SIZE; i++ ) {
				itsk.iphcands.back().targets[i] = 0xFF;
			}
			//itsk.iphcands.back().realspace = it->realspace;
			itsk.iphcands.back().candid = static_cast<unsigned int>(it - itsk.combinations.begin() );

			//memory consumption per iph
			//max_memory_expected += 0;
		}
		else {
			cerr << "Rank " << get_myrank() << " skipping a PhaseCandidate with too many target iontypes maximum is " << TARGET_LIST_SIZE << "\n";
		}
	}

	if ( itsk.iphcands.size() < 1 ) {
		cerr << "Rank " << get_myrank() << " no valid PhaseCandidate(s) were identifiable!" << "\n";
		return false;
	}

	/*
	//final check would all these buffers fit in memory?
	if ( ( max_memory_expected * static_cast<size_t>(1 + omp_get_max_threads()) ) >= max_memory_per_node ) { //+1 because there is the process-local copy to decouple critical regions of non-master threads and master thread
		cerr << "With " << max_memory_expected << " * " << omp_get_max_threads() << " threads we would exceed the max_memory_per_node!" << "\n"; return false;
	}
	else {
		cout << "Rank expected memory demands for buffering intermediate results to be " << max_memory_expected << " B" << "\n";
	}
	*/

	//##BEGIN DEBUG
	for( int rk = MASTER; rk < get_nranks(); rk++ ) {
		if ( rk == get_myrank() ) {
		cout << "Rank " << rk << " prepared all tasks now verifying successful deep copies of values..." << "\n";
			for( auto ic = itsk.icombis.begin(); ic != itsk.icombis.end(); ic++ ) {
				cout << ic->targets << "\n"; //"\t\t" << ic->realspacestr << "\n";
			}
			for( auto jt = itsk.combinations.begin(); jt != itsk.combinations.end(); jt++ ) {
				cout << "Rank " << rk << " itsk.combinations " << jt - itsk.combinations.begin() << "\n";
				for( auto tg = jt->targets.begin(); tg != jt->targets.end(); tg++ ) {
					cout << "Rank " << rk << " tg = " << (int) *tg << "\n";
				}
				//cout << "Rank " << rk << " rsp = " << jt->realspace << "\n";
			}
			for( auto tsk = itsk.iphcands.begin(); tsk != itsk.iphcands.end(); tsk++ ) {
				cout << "Rank " << rk << " isk.iphcands" << "\n";

				for( unsigned int uc = 0; uc < TARGET_LIST_SIZE; uc++ ) {
					cout << "Target\t\t" << (int) tsk->targets[uc] << "\n";
				}
				//cout << "realspc = " << tsk->realspace << "\n";
				cout << "candid = " << tsk->candid << "\n";
			}
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
	//##END DEBUG

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "DefinePhaseCandidates", APT_XX, APT_IS_PAR, mm, tic, toc );

	return true;
}


void fourierHdl::itype_sensitive_spatial_decomposition()
{
	double tic = MPI_Wtime();

	sp.tip_aabb_get_extrema( xyz );

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "SpecimenAABB3DGetExtrema", APT_XX, APT_IS_SEQ, mm, tic, toc);

	tic = MPI_Wtime();

	//get maximum iontype
	unsigned int maximum_ityp = 0;
	for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
		if ( it->id < static_cast<unsigned char>(UCHARMX) ) {
			if ( it->id > maximum_ityp ) {
				maximum_ityp = it->id;
			}
		}
		else {
			cerr << "Detected an invalid iontype during itype_sensitive_spatial_decomposition!" << "\n"; return;
		}
	}

	maximum_iontype_uc = static_cast<unsigned char>(maximum_ityp); //MK::not the number of disjoint types but the maximum ityp value!
cout << "Rank " << get_myrank() << " maximum itype identified is " << (int) maximum_iontype_uc << "\n";

	sp.loadpartitioning_subkdtree_per_ityp( xyz, dist2hull, ityp_org, ityp_org, maximum_iontype_uc ); //random labels will not be used!

	//first point were ityp_org and ityp_rnd are no longer necessary and could in principle be deleted

	toc = MPI_Wtime();
	mm = fourier_tictoc.get_memoryconsumption();
	fourier_tictoc.prof_elpsdtime_and_mem( "AABB3DLoadPartitioning", APT_XX, APT_IS_PAR, mm, tic, toc);
}


/*
void fourierHdl::spatial_decomposition()
{
	double tic, toc;
	tic = MPI_Wtime();

	sp.tip_aabb_get_extrema( xyz_ityp );

	toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "SpecimenAABB3DGetExtrema", APT_XX, APT_IS_SEQ, mm, tic, toc);

	tic = MPI_Wtime();

	sp.loadpartitioning( xyz_ityp );

	toc = MPI_Wtime();
	mm = fourier_tictoc.get_memoryconsumption();
	fourier_tictoc.prof_elpsdtime_and_mem( "AABB3DLoadPartitioning", APT_XX, APT_IS_PAR, mm, tic, toc);
}
*/

void fourierHdl::define_matpoint_volume_grid()
{
	double tic = MPI_Wtime();

	//##MK::to begin with use the entire tip as our reference window to analyze
	mp.define_probe_volume( sp.tip );

	if (ConfigFourier::ROISamplingMethod == CUBOIDAL_GRID ) {
		/*
		mp.define_regular_grid( ConfigFourier::SamplingGridBinWidthX,
								ConfigFourier::SamplingGridBinWidthY,
								ConfigFourier::SamplingGridBinWidthZ,
								ConfigFourier::ROIRadiusMax );
		*/

		mp.define_regular_grid( ConfigFourier::SamplingGridBinWidthX,
										ConfigFourier::SamplingGridBinWidthY,
										ConfigFourier::SamplingGridBinWidthZ,
										ConfigFourier::ROIRadiusMax,
										ca.vxlgrid, ca.IsInside, trianglehull );
	}
	else if ( ConfigFourier::ROISamplingMethod == SINGLE_POINT_USER ) {
		p3d here = p3d();
		stringstream parsethis;
		parsethis << ConfigFourier::SamplingPosition;
		string datapiece;
		getline( parsethis, datapiece, ';');	here.x = stof( datapiece );
		getline( parsethis, datapiece, ';');	here.y = stof( datapiece );
		getline( parsethis, datapiece, ';');	here.z = stof( datapiece );
cout << "SINGLE_POINT_USER " << here.x << ";" << here.y << ";" << here.z << "\n";

		mp.define_single_point( here, ConfigFourier::ROIRadiusMax );
	}
	else if ( ConfigFourier::ROISamplingMethod == SINGLE_POINT_CENTER ) {
		p3d here = sp.tip.center();
cout << "SINGLE_POINT_CENTER " << here.x << ";" << here.y << ";" << here.z << "\n";

		mp.define_single_point( here, ConfigFourier::ROIRadiusMax );
	}
	else if ( ConfigFourier::ROISamplingMethod == RANDOM_POINTS_FIXED ) {
		/*
		mp.define_random_cloud_fixed( ConfigFourier::FixedNumberOfROIs, ConfigFourier::ROIRadiusMax );
		*/

		mp.define_random_cloud_fixed( ConfigFourier::FixedNumberOfROIs, ConfigFourier::ROIRadiusMax,
										ca.vxlgrid, ca.IsInside, trianglehull );
	}
	else {
		return;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "DefineMaterialPointVolumeGrid", APT_XX, APT_IS_SEQ, mm, tic, toc);
}


void fourierHdl::distribute_matpoints_on_processes()
{
	double tic = MPI_Wtime();

	//all processes have the same matpoints and agree on the same global round-robin work partitioning
	//##MK::error handling
	if ( mp.matpoints.size() >= static_cast<size_t>(INT32MX-1) ) {
		cerr << "More material sampling points were defined that current implementation can handle!" << "\n"; return;
	}

	mp2rk.reserve( mp.matpoints.size() );
	//globally orchestrated round robin partitioning
	int nmp = static_cast<int>(mp.matpoints.size());
	int nr = get_nranks();
	int mr = get_myrank();
	for( int mpid = 0; mpid < nmp; mpid++ ) {
		int thisrank = mpid % nr;
		mp2rk.push_back( thisrank );
		if ( thisrank != mr ) {
			continue;
		}
		else {
			myworkload.push_back( mp.matpoints[mpid] );
//cout << "Rank " << thisrank << " takes care of material point/ROI " << mpid << "\n";
		}
	}

	//per-allocate local result buffers
	myhklval = vector<hklval>( myworkload.size(), hklval( NORESULTS_FOR_THIS_MATERIALPOINT, INT32MX, 0.0 ) );

	double toc = MPI_Wtime();
	memsnapshot mm = fourier_tictoc.get_memoryconsumption();
	fourier_tictoc.prof_elpsdtime_and_mem( "DistributeMaterialpointsAcrossMPIProcesses", APT_XX, APT_IS_PAR, mm, tic, toc);

	cout << "Rank " << get_myrank() << " will process " << myworkload.size() << " of a total of " << mp.matpoints.size() << " sampling points" << "\n";
}


void fourierHdl::distribute_matpoints_on_processes_roundrobin()
{
	//all processes have the same matpoints and agree on the same global round-robin work partitioning
	//##MK::error handling
	if ( mp.matpoints.size() >= static_cast<size_t>(INT32MX-1) ) {
		cerr << "More material sampling points were defined than current implementation can handle!" << "\n"; return;
	}
	if ( mp.matpoints.size() == 0 ) {
		cerr << "No material points to process!" << "\n"; return;
	}

	double tic = MPI_Wtime();

	mp2rk.reserve( mp.matpoints.size() );
	//globally orchestrated round-robin partitioning
	//MK::block partitioning is expected to result in strong load-imbalance because the implicit order on the ion positions
	//in xyz maps the specimen geometry, whether positions earlier or to the end of the array xyz are likely closer to
	//the specimen boundary and therefore processes taking such block on average have fewer ions per region and therefore
	//will finish earlier, even though the sequence of ion hits on the detector xy plane i.e. the xy plane is somewhat random
	//the order in z is not!
	int nmp = static_cast<int>(mp.matpoints.size());
	int nr = get_nranks();
	int mr = get_myrank();
	for( int mpid = 0; mpid < nmp; mpid++ ) {
		int thisrank = mpid % nr;
		mp2rk.push_back( thisrank );
		if ( thisrank != mr ) {
			continue;
		}
		else {
			mp2me.push_back( mpid ); //global mat point IDs!
//cout << "Rank " << thisrank << " takes care of material point/ROI " << mpid << "\n";
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = fourier_tictoc.get_memoryconsumption();
	fourier_tictoc.prof_elpsdtime_and_mem( "DistributeMatPointsOnMPIProcesses", APT_XX, APT_IS_PAR, mm, tic, toc);

	cout << "Rank " << get_myrank() << " will process " << mp2me.size() << " of a total of " << mp2rk.size() << " sampling points" << "\n";
}


gpu_cpu_max_workload fourierHdl::plan_max_per_epoch( const int ngpu, const int nthr, const int nmp )
{
	gpu_cpu_max_workload res = gpu_cpu_max_workload();
	if ( ngpu == 1 ) {
		if ( nthr > 1 ) { //master delegates work to GPUs, only if additional threads CPU also participates in FT
			if ( nmp <= nthr-1 ) { //##MK::small workload only on the GPU
				res.chunk_max_cpu = 0; //##MK::could be improved, but insignificant
				res.chunk_max_gpu = nmp;
			}
			else { //large workload distribute on GPUs and CPUs
				res.chunk_max_cpu = nthr-1;
				res.chunk_max_gpu = ngpu * ConfigFourier::GPUWorkload; //##MK::needs benchmarking, empirical!, per GPU!, assume multiple of the same accelerator e.g. 2x NVIDIA V100 as on TALOS
			}
		}
		else { //only a master which delegates work to GPUs but no own FT work scheduled to this master CPU !
			res.chunk_max_cpu = 0; //##MK::could be improved, but insignificant
			res.chunk_max_gpu = ngpu * ConfigFourier::GPUWorkload;
		}
	}
	else if ( ngpu == 0 ) { //no GPUs, everything on threads/CPU
		res.chunk_max_gpu = 0;
		res.chunk_max_cpu = nthr; //every thread gets an own FT to work on to maximize independent workload as such minimize sync
		//##MK::might be problematic for large NN, as memory consumption is at least nt*(NN^3)*3*8B, <=256 should no problem even for nt=40 for TALOS
	}
	else {
		cerr << "Rank " << get_myrank() << " attempt to use more than a single GPU per process is  invalid!" << "\n";
	}

	return res;
}


gpu_cpu_now_workload fourierHdl::plan_now_per_epoch(
		const int ngpu, const int nthr, const int nmp, const int mp, const gpu_cpu_max_workload mxl )
{
	gpu_cpu_now_workload res = gpu_cpu_now_workload();
	int rem = nmp - mp;

	if ( ngpu == 1 ) { //if we can use accelerators
		if ( nthr > 1 ) { //master delegates work to GPUs surplus additional threads CPUs also computing FTs
			if ( rem >= (mxl.chunk_max_gpu + mxl.chunk_max_cpu) ) { //still enough work for GPUs and CPUs
				res.chunk_now_gpu = mxl.chunk_max_gpu;
				res.chunk_now_cpu = mxl.chunk_max_cpu;
			}
			else if ( rem >= mxl.chunk_max_cpu ) { //more work than CPUs available, so GPUs assist
				res.chunk_now_gpu = rem - mxl.chunk_max_cpu; //GPUs assist
				res.chunk_now_cpu = mxl.chunk_max_cpu; //CPUs remainder
			}
			else {
				res.chunk_now_gpu = 0; //not even enough work to keep all CPUs busy, ##MK::can be improved...
				res.chunk_now_cpu = rem;
			}
		}
		else { //only master, so master delegates potentially to GPUs
			if ( rem >= mxl.chunk_max_gpu ) {
				res.chunk_now_gpu = mxl.chunk_max_gpu; //enough work to make a full gpu epoch
				res.chunk_now_cpu = 0;
			}
			else {
				res.chunk_now_gpu = rem; //not enough work for a complete gpu epoch but delegate then this less work still to the gpu
				res.chunk_now_cpu = 0;
			}
		}
	}
	else if ( ngpu == 0 ) { //we have no accelerators
		res.chunk_now_gpu = 0;
		if ( rem >= mxl.chunk_max_cpu ) { //more work than CPUs available all process full epoch
			res.chunk_now_cpu = mxl.chunk_max_cpu;
		}
		else { //not even enough work to keep all CPUs busy
			res.chunk_now_cpu = rem;
		}
	}
	else {
		cerr << "Rank " << get_myrank() << " attempt to use more than a single GPU per process is  invalid!" << "\n";
	}

	return res;
}


/*
void fourierHdl::generate_debug_roi( mt19937 & mydice, vector<dft_real> * out )
{
	//generate a pseudo lattice workload for testing purposes of the algorithm
	uniform_real_distribution<dft_real> unifrnd(0.f, 1.f); //no MT warmup
	int imi = -5;
	int imx = +5;
	dft_real a = 0.404; //lattice constant
	dft_real da = 0.10 * a; //some simple squared scatter about ideal lattice position
	for( int z = imi; z <= imx; z++ ) {
		for( int y = imi; y <= imx; y++ ) {
			for( int x = imi; x <= imx; x++ ) {
				//create dummy sc lattice
				dft_real xx = static_cast<dft_real>(x) * a * (2*unifrnd(mydice)-1.0) * 1.0 * da;
				dft_real yy = static_cast<dft_real>(y) * a * (2*unifrnd(mydice)-1.0) * 1.0 * da;
				dft_real zz = static_cast<dft_real>(z) * a * (2*unifrnd(mydice)-1.0) * 0.5 * da;
				//if ( unifrnd(mydice) < 1.0 ) { //random thinning
				out->push_back( xx );
				out->push_back( yy );
				out->push_back( zz );
				//}
			}
		}
	}
	//##MK::BEGIN OF DEBUGGING
	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " generated a dummy dataset with " << out->size() << " coordinates" << "\n";
	}
	//##MK::END OF DEBUGGING
}
*/


void fourierHdl::query_ions_within_roi( mp3d const & roicenter, vector<dft_real> * out )
{
	//CALLED FROM WITHIN PARALLEL REGION
	apt_xyz SearchRadiusSQR = SQR(roicenter.R);
	p3dm1 probesite = p3dm1( roicenter.x, roicenter.y, roicenter.z, DEFAULTTYPE );

	vector<p3dm1> neighbors3dm1;
	for( size_t thr = MASTER; thr < sp.db.size(); thr++ ) { //scan all regions O(lgN) to find neighboring candidates
		threadmemory* thisregion = sp.db.at(thr);
		if ( thisregion != NULL ) {
			vector<p3dm1> const & theseions = thisregion->ionpp3_kdtree;
			kd_tree* curr_kauri = thisregion->threadtree;
			if ( curr_kauri != NULL ) {
				curr_kauri->range_rball_noclear_nosort_p3d( probesite, theseions, SearchRadiusSQR, neighbors3dm1 );
			}
		}
	}

	//##MK::implement possible iontype filtering here!

	for( auto it = neighbors3dm1.begin(); it != neighbors3dm1.end(); it++ ) { //un-pack and layout into de-interleaved array of xyz distance differences
		out->push_back( it->x - probesite.x );
		out->push_back( it->y - probesite.y );
		out->push_back( it->z - probesite.z );
	}
}


void fourierHdl::debug_epoch_profiling( vector<epoch_log> const & in, vector<epoch_prof> const & timing )
{
	/*
	cout << "DEBUG reporting how threads spent time in the epochs" << "\n";
	cout << "ThreadID;NThreads;Epoch;WallClock(s);NChkuns;Ionsum" << "\n";
	for( auto it = in.begin(); it != in.end(); it++ ) {
		cout << *it << "\n";
	}
	*/

	//generate table with epochs as rows and columns detailing what the threads were doing
	//demands that number of threads did not change
	if ( in.size() < 1 ) {
		cerr << "Epoch diary is empty!" << "\n"; return;
	}
	size_t expected_nt = static_cast<size_t>(in.at(0).nthreads);
	if ( (in.size() % expected_nt) != 0 ) {
		cerr << in.size() << "\n";
		cerr << expected_nt << "\n";
		cerr << (in.size() % expected_nt) << "\n";
		cerr << "Total number of logs is not an integer multiple of the expected number of threads so data are faulty or data are missing!" << "\n"; return;
	}
	for( auto it = in.begin(); it != in.end(); it++ ) {
		if ( static_cast<size_t>(it->nthreads) == expected_nt ) {
			continue;
		}
		else {
			cerr << "The total number of threads was not the same for all epochs" << "\n"; return;
		}
	}
	size_t nepochs = in.size() / expected_nt;
	if ( timing.size() != nepochs ) {
		cerr << "The number of epoch_prof timing steps is inconsistent with the thread profiling data!" << "\n"; return;
	}

	//##MK::suboptimal... one file per rank
	string fn = "PARAPROBE.Fourier.SimID." + to_string(ConfigShared::SimID) + ".Rank." + to_string(get_myrank()) + ".EpochThreadProfiling.csv";

	ofstream csvlog;
	csvlog.open(fn.c_str(), ofstream::out | ofstream::trunc);
	if (csvlog.is_open() == true) {
		//header
		csvlog << "Epoch;MPITotalWallclock";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "OMPWallClock";
		//csvlog << ";";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "OMPIonTotal";
		//csvlog << ";";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "OMPChunkTotal";
		csvlog << "\n";
		csvlog << "Epoch;MPITotalWallClock";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "Thread" << thr;
		//csvlog << ";";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "Thread" << thr;
		//csvlog << ";";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "Thread" << thr;
		csvlog << "\n";

		for( size_t i = 0; i < in.size(); i += expected_nt ) {
			csvlog << in.at(i).epoch << ";" << timing.at(i/expected_nt).dt;
			for( size_t thr = MASTER; thr < expected_nt; thr++ )
				csvlog << ";" << in.at(i+thr).dt;
			//csvlog << ";";
			for( size_t thr = MASTER; thr < expected_nt; thr++ )
				csvlog << ";" << in.at(i+thr).ionsum;
			//csvlog << ";";
			for( size_t thr = MASTER; thr < expected_nt; thr++ )
				csvlog << ";" << in.at(i+thr).nchunks;
			csvlog << "\n";
		}

		csvlog.flush();
		csvlog.close();
		cout << "Rank " << get_myrank() << " successful generation of epoch profiling protocol file" << "\n";
	}
	else {
		cerr << "Unable to write process-local epoch profiling protocol file" << "\n";
	}
}


void fourierHdl::execute_local_workpackage1()
{
	cout << "Rank " << get_myrank() << " performing OpenMP/OpenACC direct Fourier transforms..." << "\n";

	double global_tic = MPI_Wtime();

	//initialize one GPUthe two NVIDIA GPUs on the TALOS node on which I, as get_myrank() am currently running
	int n_gpus = 0; //0 - no gpus, 1 or 2 gpus
	int n_mygpus = 0; //either 0, or 1, rank either has a gpu and can use it or not
	int devId = -1;
	int devCount = 0;

#ifdef UTILIZE_GPUS
	if ( ConfigFourier::GPUsPerNode > 0 ) {
		devId = -1;
		devCount = acc_get_num_devices(acc_device_nvidia);
		if ( devCount > 0  ) {
			n_gpus = devCount;
			n_mygpus = 1;
			cout << "Rank " << get_myrank() << " NVIDIA accelerator GPU initialization devCount " << devCount << "\n";
		}
		else {
			n_mygpus = 0;
			cout << "Rank " << get_myrank() << " no accelerators existent " << devCount << "\n";
		}
	}
	else

	//##MK::not necessarily portable, here system-specific accelerator initialization exemplified
	//for the MPCDF system TALOS, specifically its computing nodes with 2x20core + 2xNVIDIA V100 accelerators/GPGPUs/"GPUs"
	if ( n_gpus > 0 ) {
		//MK::assume user has instructed model with two processes per node each getting one GPUs
		devId = get_myrank() % ConfigFourier::GPUsPerNode;
		//cout << "Rank " << get_myrank() << " my GPU devID is " << devId << "\n";
		// creates a context on the GPUs
		//excludes initialization time from computations
		acc_set_device_num(devId, acc_device_nvidia);
		acc_init(acc_device_nvidia);
cout << "Rank " << get_myrank() << " running on GPU with device ID " << devId << "\n";
	}
	else {
		cerr << "Rank " << get_myrank() << " will use multi-threaded CPU fallback n_gpus " << n_gpus << " n_mygpus " << n_mygpus << "\n";
	}
#endif
	//##cout <<  report use fallback multithreaded CPU only
	cout << "Rank " << get_myrank() << " GPU and CPU initialized now processing... n_gpus " << n_gpus << " n_mygpus " << n_mygpus << "\n";

	//the passing of individual ROIs for each process onto the CPU/GPUs works as follows:
	//the CPU OpenMP thread team identifies the ions within each ROI
	//ROIs are group into packets (epoch), a packet to work on for the CPU and a packet to work for the GPUs
	//the CPUs process their package in parallel, each thread one material point, i.e. one DFT
	//the GPUs process the GPU packet
	//the GPU get a heuristic fraction of ROIs: when a CPU processes 1 ROI the GPU processes ConfigFourier::GPUWorkload ROIs at the same time
	//##MK::to get more fine tuning in the future allow CPUs to have more than one package
	//we work through the pairs of CPU/GPU ROI packages until all ROIs of the MPI process are completed

	int local_nmp = myworkload.size();
	cout << "Rank " << get_myrank() << " myworkload.size() " << local_nmp << "\n";

	//we need to initialize buffers first to store the ion positions from all ROIs per epoch
	vector<vector<dft_real>*> buffer_gpu;
	vector<vector<dft_real>*> buffer_cpu;
	vector<int> buffer_local_mpid_gpu; //buffer to store which specific ROIs are currently processed, this is handled by the CPU
	vector<int> buffer_local_mpid_cpu; //MK::do not jumble up these local ROI IDs with the global material point/ROI ids!
	//the local IDs live on the ID interval [0, myworkload.size() )] BUT
	//the global IDs live on the ID interval [ 0, mp2rk.size() ) !

	//we go parallel with the threads but orchestrate/guide them through their workplan
	//we need a parallel region to have the allocation of the buffers in the master
	//##MK::TO DO performance benchmark, if the master thread gets pinned to core 0 it has on average faster access
	//to the PCI control busses, therefore we let the master thread delegate and execute work on the CPUs, the master therefore
	//does not contribute in solving own Fourier transforms ##MK::TO BE DISCUSSED but dont underestimate might get  tricky to get orchestrated
	//otherwise because we have here heterogeneous parallelism with device (GPU) and host (CPU) + asynchronous execution on the device
	//we call the gpu for now the device and the cpu the host

	gpu_cpu_max_workload wl = gpu_cpu_max_workload();
	//cout << "Rank " << get_myrank() << " chk_max_gpu/chk_max_cpu " << wl.chunk_max_gpu << ";" << wl.chunk_max_cpu << "\n";

	#pragma omp parallel shared(n_gpus,n_mygpus,local_nmp,buffer_gpu,buffer_cpu,buffer_local_mpid_gpu,buffer_local_mpid_cpu,wl)
	{
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();
		#pragma omp master
		{
			//there is always a master thread
			//MK::idea is if GPUS+CPUs are used, in each iteration nt-1 FT are solved on the CPU, and an empirically identified number per GPU
			//this number, ##MK here just chosen as 4, will minimize CPU vs GPU async wait time if GPU runtime per epoch is about equal as per thread
			wl = plan_max_per_epoch( n_mygpus, nt, local_nmp );
			cout << "Rank " << get_myrank() << " MASTER " << mt << " chk_max_gpu/chk_max_cpu " << wl.chunk_max_gpu << ";" << wl.chunk_max_cpu << "\n";

			//thread team shared global address space buffers get populated by master
			//the reason for using a vector of vector of pointer rather than a vector of coordinates
			//is to collate raw pointers to different addresses, specifically pointer to different thread-local mem virtual mem sections
			if ( wl.chunk_max_gpu > 0 ) {
				buffer_gpu = vector<vector<dft_real>*>( wl.chunk_max_gpu, NULL );
				buffer_local_mpid_gpu = vector<int>( wl.chunk_max_gpu, INT32MI ); //flagged as negative IDs to enable consistency tests
			}
			if ( wl.chunk_max_cpu > 0 ) {
				buffer_cpu = vector<vector<dft_real>*>( wl.chunk_max_cpu, NULL );
				buffer_local_mpid_cpu = vector<int>( wl.chunk_max_cpu, INT32MI ); //flagged as negative IDs to enable consistency tests
			}
		} //master initialized buffer memory
	} //end of parallel region

	//start processing epochs
	vector<epoch_log> epoch_diary;
	vector<epoch_prof> epoch_wallclock;
	int epoch = 0;
	int mympid = 0;
	int ni = static_cast<int>(ConfigFourier::HKLGrid.ni);

	while ( mympid < local_nmp )
	{
		double tic = MPI_Wtime();
		gpu_cpu_now_workload wn = gpu_cpu_now_workload();

		//we define an epoch as a single chunk of FT for ROIs processed, the epoch contains at most chunk_plan_gpu+chunk_plan_cpu material points
		#pragma omp parallel shared(n_gpus,n_mygpus,epoch_diary,epoch,mympid,local_nmp,wl,wn,buffer_gpu,buffer_cpu,buffer_local_mpid_gpu,buffer_local_mpid_cpu,ni)
		{
			int mt = omp_get_thread_num();
			int nt = omp_get_num_threads();
			//we keep a thread-local buffer of results to avoid having to synchronize after each ROI transform a write-back of
			//results to the globally shared container which would choke parallel execution performance
			vector<hklval> myresults_buffer;
			double mytic = omp_get_wtime();
			epoch_log mystatus = epoch_log( static_cast<unsigned int>(mt), static_cast<unsigned int>(nt) );

			//buffer keeps pointers to ion position data for ROI of material points
			//we store the ROIs for the FT on the GPUs at [0,chunk_plan_gpu+chunk_plan_cpu-2] and
			//the ROI for the FT on the CPUs at [chunk_plan_gpu+chunk_plan_cpu-1]

			//once initialized the thread team processes through its material points using the GPU as accelerators,
			//meanwhile other processes do the same but on different material points/ROIs,
			//thereby using triple parallelism (hybrid MPI/OpenMP + OpenACC)
			#pragma omp single
			{
				//only a single thread must compute this, hence implicitly pragma omp critically writing on wn
				wn = plan_now_per_epoch( n_mygpus, nt, local_nmp, mympid, wl );

				int id = mympid; //at which ID on ID interval [0, myworkload.size() ) where we started the current epoch
				for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu; chk_gpu++ ) {
					buffer_local_mpid_gpu.at(chk_gpu) = id + chk_gpu;
				}
				id = mympid + wn.chunk_now_gpu;
				for( int chk_cpu = 0; chk_cpu < wn.chunk_now_cpu; chk_cpu++ ) {
					buffer_local_mpid_cpu.at(chk_cpu) = id + chk_cpu;
				}
				//now we know which global material point IDs are processed by the CPU/GPU team

			}
			//we need a barrier to assure that all threads see change of wn
			#pragma omp barrier

			#pragma omp master
			{
				cout << "Current epoch " << epoch << " on thread " << mt << " chunk_now_gpu " << wn.chunk_now_gpu << " chunk_now_cpu " << wn.chunk_now_cpu << "\n";
			}

			//firstly, we let the threads (all of the team), populate the buffers for GPU and CPUs
			//the concept of epochs is useful to avoid a very high memory consumption for storing ROI population with ions
			//e.g. lets assume 1000 mat points per process, each ROI having 5000 ions, this would require storing 5k * 1k points,
			//using epochs we need to buffer only 5k times as many as ROIs as processed per epoch which is typically << local_nmp
			for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu; chk_gpu++ ) {
				int whom = chk_gpu % nt;
				if ( whom == mt ) { //threads share the allocation and querying load
					vector<dft_real>* mybuf = NULL;
					mybuf = new vector<dft_real>;
					if ( mybuf != NULL ) {
						int mpid = buffer_local_mpid_gpu.at(chk_gpu);
						mp3d thisroi = myworkload.at(mpid);

						query_ions_within_roi( thisroi, mybuf );

						thisroi.nions = mybuf->size() / 3;
						//##MK::false sharing?
						myworkload.at(mpid) = thisroi; //no pragma omp critical, mpids are disjoint across the thread team!
						/*#pragma omp critical
						{
							cout << "Thread " << omp_get_thread_num() << " mpid " << mpid << " thisroi " << myworkload.at(mpid).x << ";" << myworkload.at(mpid).y << ";"
									<< myworkload.at(mpid).z << ";" << myworkload.at(mpid).R << "\t\t" << myworkload.at(mpid).nions << "\n";
						}*/
						buffer_gpu.at(chk_gpu) = mybuf; //no pragma omp critical necessary, enforced collision free through whom == mt
					}
					else {
						#pragma omp critical
						{
							cerr << "Rank " << get_myrank() << " thread " << mt << " allocation error for GPU mybuf!" << "\n";
						}
					}
				} //mt is done
			} //done with filling in GPU work

			//next, the thread team populates the CPU buffer
			for( int chk_cpu = 0; chk_cpu < wn.chunk_now_cpu; chk_cpu++ ) {
				int whom = chk_cpu % nt;
				if ( whom == mt ) {
					vector<dft_real>* mybuf = NULL;
					mybuf = new vector<dft_real>;
					if ( mybuf != NULL ) {
						int mpid = buffer_local_mpid_cpu.at(chk_cpu);
						mp3d thisroi = myworkload.at(mpid);

						query_ions_within_roi( thisroi, mybuf );

						thisroi.nions = mybuf->size() / 3;
						myworkload.at(mpid) = thisroi; //no pragma omp critical, mpids are disjoint across thread team!
						/*#pragma omp critical
						{
							cout << "Thread " << omp_get_thread_num() << " mpid " << mpid << " thisroi " << myworkload.at(mpid).x << ";" << myworkload.at(mpid).y << ";"
									<< myworkload.at(mpid).z << ";" << myworkload.at(mpid).R << "\t\t" << myworkload.at(mpid).nions << "\n";
						}*/
						buffer_cpu.at(chk_cpu) = mybuf;
					}
					else {
						#pragma omp critical
						{
							cerr << "Rank " << get_myrank() << " thread " << mt << " allocation error for CPU mybuf!" << "\n";
						}
					}
				} //mt done
			} //done with filling CPU buffers

			//process the MPI-process local ROI material point IDs critical and master only, threads get to know values because ID fields are shared across team
			//barrier is necessary, because all buffer (cpus, gpus) have to be filled, before asynchronous processing on cpu/gpus starts...
			#pragma omp barrier

			/*//##MK::BEGIN DEBUGGING, can later be deleted
			#pragma omp master
			{
				cout << "Buffer for all chunks of current epoch " << epoch << " populated" << "\n";
				size_t ntotal = 0;
				size_t nactive = 0;
				for(size_t ii = 0; ii < buffer_gpu.size(); ii++) {
					if ( buffer_gpu.at(ii) != NULL ) { //allocated
						if ( buffer_gpu.at(ii)->size() > 0 ) { //and at least one ion within ROI
							nactive++;
							ntotal += buffer_gpu.at(ii)->size();
						}
					}
				}
				for(size_t ii = 0; ii < buffer_cpu.size(); ii++) {
					if ( buffer_cpu.at(ii) != NULL ) { //allocated
						if ( buffer_cpu.at(ii)->size() > 0 ) { //and at least one ion within ROI
							nactive++;
							ntotal += buffer_cpu.at(ii)->size();
						}
					}
				}
				cout << "Average number of ions per point " << ntotal / (3*nactive) << "\n"; //3 because three coordinates values per ion
			} //no implicit barrier
			#pragma omp barrier
			//##MK::END DEBUGGING*/

			//handle heterogeneously and possibly asynchronously executed direct Fourier transforms of current epoch on the GPUs through MASTER OMP_NESTED=true
			if ( n_mygpus > 0 ) { //if there are accelerators...
#ifdef UTILIZE_GPUS
				if ( mt == MASTER ) { //...the master thread always schedules work to GPUs only
					//we have only one GPU per process
					/*double mmytic = omp_get_wtime();
					cout << "Current epoch " << epoch << " on thread MASTER processing GPU part" << "\n";*/

					//define 1d Fourier grid positions on [-2.0,+2.0]
					int nij = SQR(ni);
					int nijk = CUBE(ni);
					//dft_real* IV = (dft_real*) malloc( ni*sizeof(dft_real) ); //restrict ... (dft_real*)
					dft_real* IV = new dft_real[ni];
					//dft_real* const IV = malloc( ni*sizeof(dft_real) );

					dft_real start = ConfigFourier::HKLGrid.imi; //-1.0;
					dft_real stop = ConfigFourier::HKLGrid.imx; //+1.0;
					dft_real num = static_cast<dft_real>(ni-1);
					dft_real step = (stop-start)/num;
					for( int i = 0; i < ni; i++ ) {
						IV[i] = TWO_PI * (start + static_cast<dft_real>(i)*step);
					}

					for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu;         ) {
						//prepare processing on the process's GPU devId

						//efficiently solving the max, index search is involved, for discussion see here
						//https://www.pgroup.com/userforum/viewtopic.php?t=5096
						dft_real* A0 = NULL; ///restrict
						dft_real* Fmagn0 = NULL;
						//https://www.pgroup.com/resources/docs/18.7/x86/pgi-user-guide/index.htm

						//prepare asynchronous processing on GPU0
						int n0 = 0;
						bool gpuvalid = false;
						if ( buffer_gpu.at(chk_gpu) != NULL ) {
							if ( buffer_gpu.at(chk_gpu)->size() >= 3 ) { //at least one ion within ROI!
								gpuvalid = true;
							}
						}

						//master delegates work on the GPUs, the master itself does not process work but does host interaction
						if ( gpuvalid == true ) {
							n0 = buffer_gpu.at(chk_gpu)->size();
							//A0 = (dft_real*) malloc( n0 * sizeof(dft_real) );
							A0 = new dft_real[n0];
							//dft_real* const A0 = malloc( n0 * sizeof(dft_real) );
							//Fmagn0 = (dft_real*) calloc( nijk*1, sizeof(dft_real) );
							Fmagn0 = new dft_real[nijk];
							for( int i = 0; i < nijk; i++ ) {
								Fmagn0[i] = 0.f;
							}
							//dft_real* const Fmagn0 = calloc( nijk, sizeof(dft_real) );
							vector<dft_real> & here0 = *(buffer_gpu[chk_gpu]);
							for( int i = 0; i < n0; i++ ) { //fill buffer for the device first with values on the host
								A0[i] = here0[i];
							}

							//acc_set_device_num(devId, acc_device_nvidia);
							//do all on GPU0 ASYNCHRONOUSLY, use the default async queue because we use only one GPU
							//create a Fmagn0 on the device
							#pragma acc data copy(Fmagn0[0:nijk]) async
							{
								int ijk, iion;
								//#pragma acc enter data copyin(Fmagn0[0:nijk]) async(1)
								#pragma acc parallel loop copyin(A0[0:n0],IV[0:ni]) present(Fmagn0[0:nijk]) async private(iion,n0)
								for( ijk = 0; ijk < nijk; ijk++) {
									int i = ijk % ni;
									int j = (ijk / nij) % ni;
									int k = ijk / nij;
									dft_real fr = 0.0;
									dft_real fi = 0.0;
									for( iion = 0; iion < n0; iion += 3 ) {
										dft_real a = A0[iion+0]*IV[i] + A0[iion+1]*IV[j] + A0[iion+2]*IV[k];
										fr += cos(a);
										fi += sin(a);
									}
									Fmagn0[ijk] = sqrt( fr*fr + fi*fi );
								} //Fmagn0 continues to live happily on GPU0
								//#pragma acc update host(Fmagn0[0:nijk]) async(1)
								//#pragma acc exit data delete(Fmagn0[0:nijk]) async(1)
							}
							//destroyed Fmagn0 on the device
							//pragma acc data copy allocates memory on GPU and copies data from host to GPU when entering
							//the region and copies the data to the host when exiting region
						}

						//necessary, because once the host has dispatched work to the GPU
						//it could do own work ##MK:: host needs to wait for the GPU to complete all asynchronously executed tasks on the default async queue
						#pragma acc wait
						//now that the host/master threads knows that the GPUs has populated Fmagn0 with the results we can search for the maximum value on Fmagn0

						hklval max0 = hklval( NORESULTS_FOR_THIS_MATERIALPOINT, 0, 0.0 );
						if ( gpuvalid == true ) {
							for( int ijk = 0; ijk < nijk; ijk++ ) {
								if ( Fmagn0[ijk] < max0.val )
									continue;
								else
									max0 = hklval( WAIT_UNTIL_GLOBAL_EXTREMUM_FOUND, ijk, Fmagn0[ijk] );
							}
							max0.mpid = buffer_local_mpid_gpu.at(chk_gpu); //process-local material point ID, not global Material point ID!
							//free(A0);
							delete [] A0; A0 = NULL;
							//free(Fmagn0);
							delete [] Fmagn0; Fmagn0 = NULL;
						}
						//this is a process-local material point ID !, i.e. there are possibly as many 0-th local material points as there are MPI processes
						//use myworkload.at(max0.mpid).mpid to resolve the global material point ID
						//if max0.mpid is flagged with NORESULTS_FOR_THIS_MATERIALPOINT we can filter this out!
						myresults_buffer.push_back( max0 );

						/*
						//find those position in reciprocal space with significant signal, ##MK::currently we use a unsigned char, i.e. 8-bit wide intensity value binning
						if ( gpuvalid == true ) {
							//get maximum intensity in reciprocal space
							dft_real Fmax = F32MI;
							for( int ijk = 0; ijk < nijk; ijk++ ) {
								if ( Fmagn0[ijk] < Fmax ) {
									continue;
								}
								else {
									Fmax = Fmagn0[ijk];
								}
							}
							//based on Fmax define a cut-off threshold below which intensities are counted as noise
							dft_real Fmin = (1.0/256.0)*Fmax;
							//filter those positions in reciprocal space that are above threshold
							vector<hklspace_res> filtered;
							for( int ijk = 0; ijk < nijk; ijk++ ) { //strong peaks are seldom
								if ( Fmagn0[ijk] < Fmin ) {
									continue;
								}
								else {
									int i = ijk % ni;
									int j = (ijk / nij) % ni;
									int k = ijk / nij;
									filtered.push_back( hklspace_res( IV[i], IV[j], IV[k], Fmagn0[ijk] ) );
								}
							}
							max0.mpid = buffer_local_mpid_gpu.at(chk_gpu); //process-local material point ID, not global Material point ID!
							//free(A0);
							delete [] A0; A0 = NULL;
							//free(Fmagn0);
							delete [] Fmagn0; Fmagn0 = NULL;
						}
						*/


						/*//##MK::BEGIN DEBUG
						#pragma omp critical
						{
							cout << "Current epoch " << epoch << " on thread MASTER after pragma acc wait" << "\n";
							cout << "max0 " << max0 << "\n";
							if ( useboth == true )
								cout << "max1 " << max1 << "\n";
						}
						double gtoc = omp_get_wtime();
						#pragma omp critical
						{
							if ( useboth == true )
								cout << "Current epoch " << epoch << " on thread MASTER chk_gpu+0, chk_gpu+1 " << chk_gpu+0 << ";" << chk_gpu+1 << " two FT took " << (gtoc-gtic) << " seconds" << "\n";
							else
								cout << "Current epoch " << epoch << " on thread MASTER chk_gpu+0, chk_gpu+1 " << chk_gpu+0 << ";" << "xxxx" << " one FT took " << (gtoc-gtic) << " seconds" << "\n";
						}
						//##MK::END DEBUG*/

						chk_gpu++;

						//bookkeeping
						mystatus.nchunks++;
						mystatus.ionsum = mystatus.ionsum + n0/3;

					} //next chk_gpu

					//free(IV);
					delete [] IV; IV = NULL;

					/*	double mmytoc = omp_get_wtime();
					#pragma omp critical
					{
						cout << "Current epoch " << epoch << " on thread MASTER async with " << wn.chunk_now_gpu << " on GPUs took " << (mmytoc-mmytic) << " seconds" << "\n";
					}*/
				} //master done with its work
				else { //remainder non-master threads work asychronously on own direct Fourier transform
					/*double mmytic = omp_get_wtime();*/
					//##MK::for now we can with this model execute only one chk on the CPUs per epoch
					//because as master execution path is different than of non-master threads we cannot invoke a #pragma omp barrier here
					//MK::we could dispatch as chks for each accelerator and proceed with OMP threading but typically accelerators are
					//faster and hence could process multiple chks while the thread team hasnt even complete one
					if ( (mt-1) < wn.chunk_now_cpu ) {
						//MK::the construct mt-1 works because the MASTER thread never enters this section so mt is always >= 1
						//MK::mt-1 possible because this section is only executed by non-master thread if such exist

						hklval cpumt1res = hklval( NORESULTS_FOR_THIS_MATERIALPOINT, 0, 0.0 );
						size_t thisone = mt-1; //all threads contribute, if enough points left
						if ( buffer_cpu.at(thisone) != NULL ) {
							if ( buffer_cpu.at(thisone)->size() > 0 ) {

								dft_cpu solver = dft_cpu( ni );

								cpumt1res = solver.execute( *(buffer_cpu.at(thisone)) );

								cpumt1res.mpid = buffer_local_mpid_cpu.at(thisone); //process-local ID

								mystatus.ionsum = mystatus.ionsum + (buffer_cpu.at(thisone)->size() / 3);
								mystatus.nchunks++;
							}
						}
						myresults_buffer.push_back( cpumt1res );

						/*double mmytoc = omp_get_wtime();
						#pragma omp critical
						{
							cout << "Current epoch " << epoch << " on thread/chk " << mt << " FT took " << (mmytoc-mmytic) << " seconds" << "\n";
						}*/
					} //non-master threads done with processing the only cpu part of the current epoch
				} //non-master thread part
#endif
			} //handled case of asynchronous GPU + CPU parallel execution
			else {
				//handle multithreaded fallback in case there are no accelerators
				/*#pragma omp critical
				{
					cout << "Thread " << mt << " participates in multi-threaded fallback current epoch chunk_now_cpu " << wn.chunk_now_cpu << "\n";
				}*/
				double mmytic = omp_get_wtime();

				if ( mt < wn.chunk_now_cpu ) { //possibly all threads contribute, if enough points left
					hklval cpumtres = hklval( NORESULTS_FOR_THIS_MATERIALPOINT, 0, 0.0 );
					size_t thisone = mt; //all threads contribute, if enough points left
					if ( buffer_cpu.at(thisone) != NULL ) { //if the ROI was queried
						if ( buffer_cpu.at(thisone)->size() > 0 ) { //and it contains coordinate values

							dft_cpu solver = dft_cpu( ni );

							cpumtres = solver.execute( *(buffer_cpu.at(thisone)) );

							cpumtres.mpid = buffer_local_mpid_cpu.at(thisone); //process-local ID

							mystatus.ionsum = mystatus.ionsum + (buffer_cpu.at(thisone)->size() / 3);
							mystatus.nchunks++;
						}
					}
					myresults_buffer.push_back( cpumtres );
				}

				double mmytoc = omp_get_wtime();
				#pragma omp critical
				{
					cout << "Current epoch " << epoch << " on thread/chk " << mt << " FT took " << (mmytoc-mmytic) << " seconds" << "\n";
				}
			} //done for the CPU only multi-threaded fallback

			//threads dump their results now resulting in only as many criticals as there are threads
			//rather than as many individual criticals as FT transforms per epoch
			#pragma omp critical
			{
				for( auto kyvl = myresults_buffer.begin(); kyvl != myresults_buffer.end(); kyvl++ ) {
					if ( kyvl->mpid != NORESULTS_FOR_THIS_MATERIALPOINT ) {
						myhklval.at(kyvl->mpid) = *kyvl;
					}
				}
				//release thread-local buffer of collected results
				myresults_buffer = vector<hklval>();
			}

			mystatus.epoch = epoch;
			double mytoc = omp_get_wtime();
			mystatus.dt = mytoc-mytic;
			#pragma omp critical
			{
				epoch_diary.push_back( mystatus );
			}

			//a barrier is necessary, because we need to avoid that masters starts release buffers already while threads may still accumulate results
			#pragma omp barrier

			//clear the buffer and reinitialize for next use
			#pragma omp master
			{
				for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu; chk_gpu++ ) {
					if ( buffer_gpu.at(chk_gpu) != NULL ) {
						delete buffer_gpu.at(chk_gpu);
						buffer_gpu.at(chk_gpu) = NULL; //necessary to prepare for reusage
					}
				}
				for( int chk_cpu = 0; chk_cpu < wn.chunk_now_cpu; chk_cpu++ ) {
					if ( buffer_cpu.at(chk_cpu) != NULL ) {
						delete buffer_cpu.at(chk_cpu);
						buffer_cpu.at(chk_cpu) = NULL; //necessary to prepare for reusage
					}
				}
			} //no implicit barrier as were about to exit parallel region with implicit barrier anyway

		} //end of parallel region within the while loop for the current epoch

		mympid = mympid + wn.chunk_now_gpu + wn.chunk_now_cpu; //advance to begin a new epoch

		double toc = MPI_Wtime();
		epoch_wallclock.push_back( epoch_prof( epoch, toc-tic ) );

		cout << "Current epoch " << epoch << " with a total of " << wn.chunk_now_gpu + wn.chunk_now_cpu << " completed took " << (toc-tic) << " seconds" << "\n";
		epoch++;
	} //end of the while loop

	double global_toc = MPI_Wtime();
	memsnapshot mm = fourier_tictoc.get_memoryconsumption();
	fourier_tictoc.prof_elpsdtime_and_mem( "QueryROIsAndPerformDirectFourierTransforms", APT_XX, APT_IS_PAR, mm, global_tic, global_toc);

	cout << "Rank " << get_myrank() << " the direct FT took " << (global_toc-global_tic) << " seconds" << "\n";

	sort( epoch_diary.begin(), epoch_diary.end(), SortEpochLogAsc );

	debug_epoch_profiling( epoch_diary, epoch_wallclock );
}


void fourierHdl::execute_local_workpackage2()
{
	double global_tic = MPI_Wtime();

	if ( mp2me.size() == 0) {
		cout << "WARNING:: Rank " << get_myrank() << " has no work to do!" << "\n"; return;
	}

	//initialize one GPU, ##MK::for TALOS the two NVIDIA V100 GPUs per node which I, as get_myrank() am currently running
	int n_gpus = 0; //0 - no gpus, 1 or 2 gpus
	int n_mygpus = 0; //either 0, or 1, because a rank either has an assisting gpu and can use it or uses a multi-threaded fallback
	int devId = -1;
	int devCount = 0;

#ifdef UTILIZE_GPUS
	if ( ConfigFourier::GPUsPerNode > 0 ) {
		devId = -1;
		devCount = acc_get_num_devices(acc_device_nvidia);
		if ( devCount > 0 ) {
			n_gpus = devCount;
			n_mygpus = 1;
			cout << "Rank " << get_myrank() << " NVIDIA accelerator GPU initialization devCount " << devCount << "\n";
		}
		else {
			n_mygpus = 0;
			cout << "Rank " << get_myrank() << " no accelerators existent " << devCount << "\n";
		}
	}
	//##MK::not necessarily portable, here system-specific accelerator initialization exemplified
	//for the MPCDF system TALOS, specifically its computing nodes with 2x20core + 2xNVIDIA V100 accelerators/GPGPUs/"GPUs"
	if ( n_gpus > 0 ) {
		//MK::assume user has instructed model with two processes per node each getting one GPUs
		devId = get_myrank() % ConfigFourier::GPUsPerNode;
		//cout << "Rank " << get_myrank() << " my GPU devID is " << devId << "\n";
		// creates a context on the GPUs
		//excludes initialization time from computations
		acc_set_device_num(devId, acc_device_nvidia);
		acc_init(acc_device_nvidia);
cout << "Rank " << get_myrank() << " running on GPU with device ID " << devId << "\n";
	}
	else {
		cerr << "Rank " << get_myrank() << " will use multi-threaded CPU fallback n_gpus " << n_gpus << " n_mygpus " << n_mygpus << "\n";
	}
#endif
	//##cout <<  report use fallback multithreaded CPU only
	cout << "Rank " << get_myrank() << " GPU and CPU initialized now processing... n_gpus " << n_gpus << " n_mygpus " << n_mygpus << "\n";

	//identify which ion types we need to probe at all as they are targets in a PhaseCandidate
	//all other ions are immediately ignored thereby improving on ityp_specific ion querying
	//important is that material point positions in contrast to ions in paraprobe-spatstat mostly, except
	//for some seldom by chance events lay exactly at the positions of ions, therefore we only should query
	//external position in the point cloud, i.e. not atoms but real space positions wrt to the iontype specific kd sub-trees

	#pragma omp parallel shared(n_gpus,n_mygpus,devId)
	{
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();
		//mkl_set_num_threads_local(1);

		#pragma omp master
		{
			workers = vector<acquisor*>( nt, NULL );
		}
		//necessary as otherwise write back conflict on workers from threads
		#pragma omp barrier

		//setup thread-local worker and use thread-local memory
		acquisor* thrhdl = NULL;
		try {
			thrhdl = new acquisor;
		}
		catch (bad_alloc &mecroak) {
			#pragma omp critical
			{
				cerr << "Rank " << get_myrank() << " thread " << mt << " unable to allocate an acquisor instance!" << "\n";
			}
		}

		//now workers backreference holders can safely be modified by current thread
		//MK::pointer will point to thread-local memory
		#pragma omp critical
		{
			workers.at(mt) = thrhdl;
		}

		//thrhdl->configure();

		apt_real R = ConfigFourier::ROIRadiusMax;
		apt_real RSQR = SQR(R);
		//##MK::how is it assured that workers is always !NULL ?

		//make thread-local memory copies of the phases to reduce cache traffic
		vector<PhaseCandidate> mytsk;
		for( auto tsk = itsk.iphcands.begin(); tsk != itsk.iphcands.end(); tsk++ ) {
			mytsk.push_back( PhaseCandidate() );
			for( unsigned int tg = 0; tg < TARGET_LIST_SIZE; tg++ ) {
				mytsk.back().targets[tg] = tsk->targets[tg];
			}
			//mytsk.back().realspace = tsk->realspace;
			mytsk.back().candid = tsk->candid;
		}

		//identify which iontype kd-subtrees we should at all attempt to probe as targets across all regions
		//we need to probe only the _org trees
		set<int> relevant_trees;
		for( int thr = MASTER; thr < nt; thr++ ) {
			threadmemory* thisone = sp.db.at(thr);
			int nitypes = thisone->ionpp1_kdtree_org.size(); //int suffices, only at most 256 itypes!
			for( int ii = 0; ii < nitypes; ii++ ) {
				if ( thisone->ionpp1_kdtree_org.at(ii) != NULL ) { //for a sub-tree to be relevant it first of all needs to contain ions
					//but also it should handle an iontype which is part of an analysis task
					for( auto mtsk = mytsk.begin(); mtsk != mytsk.end(); mtsk++ ) {
						for( int jj = 0; jj < TARGET_LIST_SIZE; jj++ ) {
							unsigned char current_ityp = mtsk->targets[jj];
							if ( current_ityp != 0xFF ) {
								if ( static_cast<int>(current_ityp) != ii ) {
									continue;
								}
								relevant_trees.insert( ii );
								break; //we break because we scan for ityp ii only!
							}
						} //next target to test if of ityp ii
					} //next phase candidate to test delivering potentially different itypes to query for
				}
			} //next type to test
		} //next thread region to test

		//actual ion scanning
		size_t nivals = 1+static_cast<size_t>((int) maximum_iontype_uc); //MK::do forget that we have also the zero-th type so we need 1+nival accessors for subkdtrees

//##MK::BEGIN OF DEBUG
#pragma omp critical
{
	cout << "Rank " << get_myrank() << " thread " << mt << " nivals " << nivals << "\n";
}
//##MK::END OF DEBUG

		//barrier here is necessary because all threads have to be initialized and their settings configured
		#pragma omp barrier

		//now execute cooperative processing of material points by thread team
		#pragma omp for schedule(dynamic,1) nowait
		for( size_t i = 0; i < mp2me.size(); i++ ) { //threads distribute materials point dynamically
			int mpid = mp2me.at(i); //i is a process-local material point ID, mpid is a world-global material point ID !
			mp3d thisone = mp.matpoints.at(mpid);
			apt_real rlargestsqr = SQR(thisone.R);

			p3d here = p3d( thisone.x, thisone.y, thisone.z ); //a material point, i.e. location in the volume, NOT an ion!
			//p3dm1 probesite = p3dm1( thisone.x, thisone.y, thisone.z, 0 );

//threaded-searches for ions within spherical ROI and iontypes referred to in relevant_trees
			vector<p3d> ions_xyz;
			vector<iontype_ival> ions_ityp_block = vector<iontype_ival>( nivals, iontype_ival(0, 0) ); //build an array of neighbors within largest ROI
			size_t prev = 0;
			size_t next = 0;

			for( auto tt = relevant_trees.begin(); tt != relevant_trees.end(); tt++ ) { //its a set so implicitly sorted in ascending order!
//cout << "\t\t\t\t\ttt " << *tt << "\n";
				//scan all the relevant sub-trees
				prev = ions_xyz.size();

				//query all ions of ityp tt within a ROI at position here using ityp-subtree O(lgN_ityp)
				for( int thr = MASTER; thr < nt; thr++ ) {
					threadmemory* currentregion = sp.db.at(thr);
					if ( currentregion != NULL ) {
						kd_tree* thistree = currentregion->ityp_kdtree_org.at(*tt);
						//always external queries because matpoint against ions, not ions against other ions like in paraprobe-spatstat
						if ( thistree != NULL ) {
							vector<p3d> const & ppp = *(currentregion->ionpp1_kdtree_org.at(*tt));
							thistree->range_rball_append_external( here, ppp, rlargestsqr, ions_xyz );
						}
					}
				} //next threadregion

				next = ions_xyz.size();
				ions_ityp_block.at(*tt) = iontype_ival( prev, next );
			} //query next relevant itype, leave all others ityp_block values at 0, 0 indicating nothing to read from ions_xyz for these ityps

//thread-local processing of neighbors
			//walk through all PhaseCandidates with the querying results
			for( auto mtsk = mytsk.begin(); mtsk != mytsk.end(); mtsk++ ) {
				//do we have within the current ROI at all ions of at least one target ityp for the task?

				vector<p3d> myneighbors;
				for( int tg = 0; tg < TARGET_LIST_SIZE; tg++ ) {
					if ( mtsk->targets[tg] != 0xFF ) {
						int curr_ityp = static_cast<int>(mtsk->targets[tg]);
						for( size_t jj = ions_ityp_block.at(curr_ityp).incl_left; jj < ions_ityp_block.at(curr_ityp).excl_right; jj++ ) {
							myneighbors.push_back( ions_xyz.at(jj) );
						}
					}
				}
				if ( myneighbors.size() > 0 ) { //allocate a thread-local buffer for the temporaries and work on current matpoint
					fourier_res* hdl = NULL;
					try {
						hdl = new fourier_res;
					}
					catch( bad_alloc &mecroak ) {
						#pragma omp critical
						{
							cerr << "Rank " << get_myrank() << " thread " << mt << " failed to allocate a fourier_res solver!" << "\n";
						}
					}
					if ( hdl != NULL ) {
						/*#pragma omp critical
						{
							cout << "Rank " << get_myrank() << " thread " << mt << " processing hdl " << "\n";
						}*/
						hdl->owner = thrhdl; //MK::pointer to thrhdl not pointer to this class instance!!
						hdl->mp = here;

						//origin shift as proposed by F. Vurpillot
						for( size_t ii = 0; ii < myneighbors.size(); ii++ ) {
							myneighbors[ii].x = myneighbors[ii].x - here.x;
							myneighbors[ii].y = myneighbors[ii].y - here.y;
							myneighbors[ii].z = myneighbors[ii].z - here.z;
						}

						hdl->tictoc[TICTOC_IONSINROI] = static_cast<double>(myneighbors.size());
						hdl->tictoc[TICTOC_THREADID] = static_cast<double>(mt);

#ifdef UTILIZE_GPUS
						if ( (n_mygpus == 1 && mt != MASTER) || (n_mygpus == 0) ) {
							//in-case there is a GPU but I am slave thread, I do regular processing
							//or in case there are no thread and therefore use a multi-thread fallback

							hdl->direct_ft_cpu( myneighbors );

							hdl->postprocess_cpu();

							hdl->dbscanpks_cpu();
						}
						else { //will only be entered if n_mygpus != 0 ##MK::and provided above setting of n_mygpus to 1 when mt == MASTER

							hdl->direct_ft_gpu( myneighbors );

							hdl->postprocess_cpu(); //##MK::parallelize GPU and switch to findpks_gpu

							hdl->dbscanpks_cpu();
						}
#else

						hdl->direct_ft_cpu( myneighbors );

						hdl->postprocess_cpu();

						hdl->dbscanpks_cpu();
#endif

						thrhdl->res.push_back( fourier_res_node() );
						thrhdl->res.back().dat = hdl;
						thrhdl->res.back().mpid = mpid;
						thrhdl->res.back().phcandid = mtsk->candid;
					}
					else {
						#pragma omp critical
						{
							cerr << "Rank " << get_myrank() << " thread " << mt << " mpid " << mpid << " was unable to allocate araullo_res object, hdl == NULL!" << "\n";
						}
						//no results are stored for unallocatable guys
					}
				}
				//else{} no results are stored for unallocateable guys
			} //next task at the point
		} //next sampling point

		//#pragma omp barrier
		/*#pragma omp critical
		{
			cout << "Rank " << myrk << " Thread " << mt << " finished atom probe crystallography took " << (mytoc-mytic) << " seconds" << endl;
		}*/
	} //end of parallel region

	double global_toc = MPI_Wtime();
	memsnapshot mm = fourier_tictoc.get_memoryconsumption();
	fourier_tictoc.prof_elpsdtime_and_mem( "QueryROIsAndPerformDirectFourierTransforms", APT_XX, APT_IS_PAR, mm, global_tic, global_toc);

	//##MK::report timings

cout << "Rank " << get_myrank() << " the direct FT took " << (global_toc-global_tic) << " seconds" << "\n";
}



bool fourierHdl::init_target_file()
{
	//##MK::generate group hierarchy of an open source IFES APTTC APTH5 HDF5 file
	string h5fn_out = "PARAPROBE.Fourier.Results.SimID." + to_string(ConfigShared::SimID) + ".h5"; //".apth5";
	cout << "Initializing target file " << h5fn_out << "\n";

	if ( debugh5Hdl.create_fourier_apth5( h5fn_out ) == WRAPPED_HDF5_SUCCESS ) {
		return true;
	}
	else {
		return false;
	}
}


bool fourierHdl::init_target_file2()
{
	//##MK::generate group hierarchy of an open source IFES APTTC APTH5 HDF5 file
	double tic = MPI_Wtime();

	string h5fn_out = "PARAPROBE.Fourier.Results.SimID." + to_string(ConfigShared::SimID) + ".h5";

	debugh5Hdl.h5resultsfn = h5fn_out;

	//only master instantiates the file
	if ( get_myrank() == MASTER ) {
cout << "Rank " << MASTER << " initializing target file " << h5fn_out << "\n";

		if ( debugh5Hdl.create_fourier2_apth5( h5fn_out ) == WRAPPED_HDF5_SUCCESS ) {
			//instantiate containers for PhaseCandidate specific results
			string fwslash = "/";
			string grpnm = "";
			int status = WRAPPED_HDF5_SUCCESS;
			if ( ConfigFourier::IOFullGridPks == true ) {
				for( auto mtsk = itsk.iphcands.begin(); mtsk != itsk.iphcands.end(); mtsk++ ) {
					grpnm = PARAPROBE_FOURIER_RES_HKLGRID + fwslash + "PH" + to_string(mtsk->candid);
					status = debugh5Hdl.create_group( grpnm );
					if ( status != WRAPPED_HDF5_SUCCESS ) { cerr << grpnm << " create failed! " << status << "\n"; return false; }
					cout << grpnm << " create " << status << "\n";
				}
			}
			if ( ConfigFourier::IOSignificantPks == true ) {
				for( auto mtsk = itsk.iphcands.begin(); mtsk != itsk.iphcands.end(); mtsk++ ) {
					grpnm = PARAPROBE_FOURIER_RES_HKLREFL + fwslash + "PH" + to_string(mtsk->candid);
					status = debugh5Hdl.create_group( grpnm );
					if ( status != WRAPPED_HDF5_SUCCESS ) { cerr << grpnm << " create failed! " << status << "\n"; return false; }
					cout << grpnm << " create " << status << "\n";
				}
			}
			if ( ConfigFourier::IODBScanOnSgnPks == true ) {
				for( auto mtsk = itsk.iphcands.begin(); mtsk != itsk.iphcands.end(); mtsk++ ) {
					grpnm = PARAPROBE_FOURIER_RES_HKLDBSCAN + fwslash + "PH" + to_string(mtsk->candid);
					status = debugh5Hdl.create_group( grpnm );
					if ( status != WRAPPED_HDF5_SUCCESS ) { cerr << grpnm << " create failed! " << status << "\n"; return false; }
					cout << grpnm << " create " << status << "\n";
				}
			}
			//for the overview list for which material point and we have results for phase ph
			grpnm = PARAPROBE_FOURIER_RES_MP_IOID;
			status = debugh5Hdl.create_group( grpnm );
			if ( status != WRAPPED_HDF5_SUCCESS ) { cerr << grpnm << " create failed! " << status << "\n"; return false; }
			cout << grpnm << " create " << status << "\n";

			double toc = MPI_Wtime();
			memsnapshot mm = memsnapshot();
			fourier_tictoc.prof_elpsdtime_and_mem( "InitTargetResultsH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
			return true;
		}
		else {
			return false;
		}
	}
	else { //slaves just fill the names
		double toc = MPI_Wtime();
		memsnapshot mm = memsnapshot();
		fourier_tictoc.prof_elpsdtime_and_mem( "InitTargetResultsH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
		return true;
	}
}


bool fourierHdl::write_environment_and_settings()
{
	vector<pparm> hardware = fourier_tictoc.report_machine();
	string prfx = PARAPROBE_FOURIER_META_HRDWR; //PARAPROBE_UTILS_HRDWR_META_KEYS;
	for( auto it = hardware.begin(); it != hardware.end(); it++ ) {
		cout << it->keyword << "__" << it->value << "__" << it->unit << "__" << it->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *it ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing hardware setting " << it->keyword << " failed!" << "\n";
			return false;
		}
	}
	cout << "Rank " << MASTER << " hardware settings written to H5" << "\n";

	vector<pparm> software;
	ConfigFourier::reportSettings( software );
	prfx = PARAPROBE_FOURIER_META_SFTWR; //PARAPROBE_UTILS_SFTWR_META_KEYS;
	for( auto jt = software.begin(); jt != software.end(); jt++ ) {
		cout << jt->keyword << "__" << jt->value << "__" << jt->unit << "__" << jt->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *jt ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing software tool setting " << jt->keyword << " failed!" << "\n";
			return false;
		}
	}
	cout << "Rank " << MASTER << " software settings written to H5" << "\n";

	//add LatticeIontypeCombinations using the same prfx
	//##MK::
	/*
	for( auto kt = itsk.iifo.begin(); kt != itsk.iifo.end(); kt++ ) {
		cout << kt->keyword << "__" << kt->value << "__" << kt->unit << "__" << kt->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *kt ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing software tool setting " << kt->keyword << " failed!" << "\n";
			return false;
		}
	}
	*/
	return true;
}


bool fourierHdl::collect_results_on_masterprocess()
{
	double tic = MPI_Wtime();

	//then we have two options, the simplest, enforce sequential loop of individual slave sends, master receives
	//or leaner a MPI_Gatherv on the master
	//##MK::we opt first for the simpler strategy, because computing time for the fourier transform >> communication time

	//all processes first identify how many results they have to sent
	int localhealth = 1;
	int globalhealth = 0;
	if ( myhklval.size() >= UINT32MX ) {
		cerr << "Rank " << get_myrank() << " has too many results to send them using the currently implemented communcation protocol!";
		localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all processes are able to contribute results!" << "\n";
		return false;
	}

	unsigned int n_localresults = static_cast<unsigned int>(myhklval.size()); //we send also results on material points with no ions
	vector<unsigned int> rk2nres = vector<unsigned int>( get_nranks(), 0 );
	unsigned int* buf = NULL;
	if ( myrank == MASTER ) {
		buf = rk2nres.data(); //raw pointer to array ie &rk2nres[0]
	}
	MPI_Gather( &n_localresults, 1, MPI_UNSIGNED, buf, 1, MPI_UNSIGNED, MASTER, MPI_COMM_WORLD );

	size_t nrestotal = 0;
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < rk2nres.size(); i++ ) {
			cout << "Rank " << i << " contributes " << rk2nres[i] << " results" << "\n";
		}
		for( auto it = rk2nres.begin(); it != rk2nres.end(); it++ ) {
			nrestotal += static_cast<size_t>(*it);
		}
		cout << "Rank MASTER has counted " << nrestotal << " results and expects " << mp.matpoints.size() << "\n";

		//is this consistent with expectation?
		if ( nrestotal != mp.matpoints.size() ) {
			localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that the reported number of results is different from expectation!" << "\n";
		return false;
	}

	//no differences, so let master allocate sufficiently sized buffer
	//MK::we enforce here, at the cost of performance, and explicit writing of the result in order, such that regardless the number of processes used
	//the order of the results arrays in the HDF5 file is always the same, i.e. deterministic
	vector<MPI_Fourier_ROI> roisend;
	vector<MPI_Fourier_ROI> roirecv;
	vector<MPI_Fourier_HKLValue> hklsend;
	vector<MPI_Fourier_HKLValue> hklrecv;

	if ( get_myrank() == MASTER ) {
		//master allocates global results field, see above comments on strategy
		mproi = vector<MPI_Fourier_ROI>( nrestotal, MPI_Fourier_ROI() );
		mpres = vector<MPI_Fourier_HKLValue>( nrestotal, MPI_Fourier_HKLValue() );

		//master fills in all values
		//infos about the master's ROIs
		for( auto it = myworkload.begin(); it != myworkload.end(); it++ ) { //myworkload handling global material point IDs
			unsigned int global_mpid = it->mpid;
			mproi.at(global_mpid) = MPI_Fourier_ROI( it->x, it->y, it->z, it->R, it->mpid, it->nions ); //number of ions not yet known
		}
		//master also fills in all its actual results
		for( auto it = myhklval.begin(); it != myhklval.end(); it++ ) { //myhklval handling local material point IDs
			unsigned int local_mpid = it->mpid; //process-local ID
			if ( local_mpid != NORESULTS_FOR_THIS_MATERIALPOINT ) {
				unsigned int global_mpid = myworkload.at(local_mpid).mpid;
				mpres.at(global_mpid) = MPI_Fourier_HKLValue( global_mpid, it->ijk, it->val ); //from explicit global_mpid to implicit IDs
				//##MPI_Fourier_HKLValue change mpid from int to unsigned int
			}
			//if no result leave MPI_Fourier_HKLValue with the default datatype constructor value
		}
	}
	else { //meanwhile the slaves prepare their MPI send buffers
		//infos about the slaves's ROIs
		roisend.reserve( myworkload.size() );
		for( auto it = myworkload.begin(); it != myworkload.end(); it++ ) { //myworkload handling global material point IDs
			roisend.push_back( MPI_Fourier_ROI(it->x, it->y, it->z, it->R, it->mpid, it->nions) );
		}
		//the actual results of the slave
		hklsend.reserve( myhklval.size() );
		for( auto it = myhklval.begin(); it != myhklval.end(); it++ ) { //myhklval handling local material point IDs
			unsigned int local_mpid = it->mpid; //process-local ID
			if ( local_mpid != NORESULTS_FOR_THIS_MATERIALPOINT ) { //only if this is resolvable there is a result, else the ROI has no ion support
				unsigned int global_mpid = myworkload.at(local_mpid).mpid;
				hklsend.push_back( MPI_Fourier_HKLValue( global_mpid, it->ijk, it->val ) ); //from explicit global_mpid to implicit IDs
			}
			else {
				hklsend.push_back( MPI_Fourier_HKLValue( NORESULTS_FOR_THIS_MATERIALPOINT, INT32MX, 0.f ) );
			}
		}
	}

	//agglomeration stage, master collects results from slaves
	for( int rank = MASTER + 1; rank < get_nranks(); rank++ ) { //+1 because we have already the results from the master
		if ( get_myrank() == MASTER ) {

			roirecv = vector<MPI_Fourier_ROI>( rk2nres.at(rank), MPI_Fourier_ROI() );
			MPI_Recv( roirecv.data(), rk2nres.at(rank), MPI_Fourier_ROI_Type, rank, rank, MPI_COMM_WORLD, MPI_STATUS_IGNORE );

			hklrecv = vector<MPI_Fourier_HKLValue>( rk2nres.at(rank), MPI_Fourier_HKLValue() );
			MPI_Recv( hklrecv.data(), rk2nres.at(rank), MPI_Fourier_HKLValue_Type, rank, 1000*rank, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			//once results are received fill them in
			for( auto it = roirecv.begin(); it != roirecv.end(); it++ ) {
				unsigned int global_mpid = it->mpid;
				mproi.at(global_mpid) = *it;
			}
			for( auto jt = hklrecv.begin(); jt != hklrecv.end(); jt++ ) {
				unsigned int global_mpid = jt->mpid;
				if ( global_mpid != NORESULTS_FOR_THIS_MATERIALPOINT ) { //reset only those results values for which results exist
					mpres.at(global_mpid) = *jt;
				}
			}
		}
		else { //I am a slave to I have to send to MASTER
			if ( get_myrank() == rank ) {
				MPI_Send( roisend.data(), roisend.size(), MPI_Fourier_ROI_Type, MASTER, rank, MPI_COMM_WORLD );
				roisend = vector<MPI_Fourier_ROI>();

				MPI_Send( hklsend.data(), hklsend.size(), MPI_Fourier_HKLValue_Type, MASTER, 1000*rank, MPI_COMM_WORLD );
				hklsend = vector<MPI_Fourier_HKLValue>();
			}
			//else {} //nothing to do for me, I neither do I/O nor have the data for rank
		}
		MPI_Barrier(MPI_COMM_WORLD); //##MK::hyperphobic
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "CollectResultsOnMasterProcess", APT_XX, APT_IS_PAR, mm, tic, toc);

	cout << "Rank " << get_myrank() << " participated successfully in results collection stage took " << (toc-tic) << " seconds" << "\n";
	return true;
}


bool fourierHdl::write_materialpoints_to_apth5()
{
	//##MK::error management
	double tic = MPI_Wtime();

	if ( get_myrank() == MASTER ) { //only the master performs I/O of metadata

		if ( mproi.size() < 1 ) {
			cout << "WARNING:: There are no material point ROI infos to report" << "\n"; return true;
		}
		if ( mpres.size() < 1 ) {
			cout << "WARNING:: There are no material point results to report" << "\n"; return true;
		}

		vector<unsigned int> u32;
		//material point topology for rendering in paraview
		u32 = vector<unsigned int>( (1+1+1)*mproi.size(), 1 );
		//+1 first index identifies geometric primitive, here single vertex
		//+1 second index tells how many elements to except type for XDMF is XDMF keyword what we see, here point
		//+1 the value vertex ID to retrieve the coordinates of the point from the PARAPROBE_FOURIER_MATPOINT_XYZ array in the HDF5 file through XDMF in Paraview/VisIt
		for( size_t i = 0; i < mproi.size(); i++ ) {
			u32.at(3*i+2) = (i < static_cast<size_t>(UINT32MX)) ? static_cast<unsigned int>(i) : UINT32MX;
		}

		int status = 0;
		h5iometa ifo = h5iometa( PARAPROBE_FOURIER_META_MP_TOPO, u32.size()/PARAPROBE_FOURIER_META_MP_TOPO_NCMAX,
				PARAPROBE_FOURIER_META_MP_TOPO_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_MP_TOPO create failed! " << status << "\n"; return false;
		}
cout << "PARAPROBE_FOURIER_META_MP_TOPO create " << status << "\n";
		h5offsets offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_META_MP_TOPO_NCMAX, 0, PARAPROBE_FOURIER_META_MP_TOPO_NCMAX,
				u32.size()/PARAPROBE_FOURIER_META_MP_TOPO_NCMAX, PARAPROBE_FOURIER_META_MP_TOPO_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_MP_TOPO write failed! " << status << "\n"; return false;
		}
cout << "PARAPROBE_FOURIER_META_MP_TOPO write " << status << "\n";
		u32 = vector<unsigned int>();

		//material point IDs
		u32.reserve( mproi.size() );
		if ( mproi.size() >= static_cast<size_t>(UINT32MX) ) {
			cerr << "Too many mproi.size() to export!" << "\n"; return false;
		}
		unsigned int ni = static_cast<unsigned int>(mproi.size()); //downcast now safe
		for( unsigned int i = 0; i < ni; i++ ) {
			u32.push_back( mproi.at(i).mpid );
		}
		ifo = h5iometa( PARAPROBE_FOURIER_META_MP_ID, u32.size()/PARAPROBE_FOURIER_META_MP_ID_NCMAX,
				PARAPROBE_FOURIER_META_MP_ID_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_MP_ID create failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_MP_ID create " << status << "\n";
		offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_META_MP_ID_NCMAX, 0, PARAPROBE_FOURIER_META_MP_ID_NCMAX,
				u32.size()/PARAPROBE_FOURIER_META_MP_ID_NCMAX, PARAPROBE_FOURIER_META_MP_ID_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_MP_ID write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_MP_ID write " << status << "\n";
		u32 = vector<unsigned int>();

		//number of ions
		u32.reserve( mproi.size() );
		for( unsigned int i = 0; i < ni; i++ ) { //check for safe downcasting done before already
			u32.push_back( mproi.at(i).nions );
		}
		ifo = h5iometa( PARAPROBE_FOURIER_META_MP_NIONS, u32.size()/PARAPROBE_FOURIER_META_MP_NIONS_NCMAX,
				PARAPROBE_FOURIER_META_MP_NIONS_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_MP_NIONS create failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_MP_NIONS create " << status << "\n";
		offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_META_MP_NIONS_NCMAX, 0, PARAPROBE_FOURIER_META_MP_NIONS_NCMAX,
				u32.size()/PARAPROBE_FOURIER_META_MP_NIONS_NCMAX, PARAPROBE_FOURIER_META_MP_NIONS_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_MP_NIONS write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_MP_NIONS write " << status << "\n";
		u32 = vector<unsigned int>();

		vector<float> f32;
		f32.reserve( 3*mproi.size() );
		for( auto it = mproi.begin(); it != mproi.end(); it++ ) {
			f32.push_back( it->x );
			f32.push_back( it->y );
			f32.push_back( it->z );
		}
		ifo = h5iometa( PARAPROBE_FOURIER_META_MP_XYZ, f32.size()/PARAPROBE_FOURIER_META_MP_XYZ_NCMAX, PARAPROBE_FOURIER_META_MP_XYZ_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS  ) {
			cerr << "PARAPROBE_FOURIER_META_MP_XYZ_NCMAX create failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_MP_XYZ create " << status << "\n";
		offs = h5offsets( 0, f32.size()/PARAPROBE_FOURIER_META_MP_XYZ_NCMAX, 0, PARAPROBE_FOURIER_META_MP_XYZ_NCMAX,
				f32.size()/PARAPROBE_FOURIER_META_MP_XYZ_NCMAX, PARAPROBE_FOURIER_META_MP_XYZ_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_MP_XYZ write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_MP_XYZ write " << status << "\n";
		f32 = vector<float>();

		string xdmffn = "PARAPROBE.Fourier.Results.SimID." + to_string(ConfigShared::SimID) + ".xdmf";
		debugxdmf.create_materialpoint_file( xdmffn, mproi.size(), debugh5Hdl.h5resultsfn );

		//details about the grid used
		u32 = vector<unsigned int>( 3, static_cast<unsigned int>(ConfigFourier::HKLGrid.ni) );
		ifo = h5iometa( PARAPROBE_FOURIER_META_HKL_NIJK, u32.size()/PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX,
				PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_HKL_NIJK create failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_HKL_NIJK create " << status << "\n";
		offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX, 0, PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX,
				u32.size()/PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX, PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_HKL_NIJK write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_HKL_NIJK write " << status << "\n";
		u32 = vector<unsigned int>();

		//##MK::make small function which computes this mapping taking only FourierGridResolution as input
		f32 = vector<float>();
		dft_real start = ConfigFourier::HKLGrid.imi;
		dft_real stop = ConfigFourier::HKLGrid.imx;
		dft_real num = static_cast<dft_real>(ConfigFourier::HKLGrid.ni-1);
		dft_real step = (stop-start)/num;
		for( int i = 0; i < ConfigFourier::HKLGrid.ni; ++i ) {
			dft_real v = TWO_PI * (start + static_cast<dft_real>(i)*step);
			f32.push_back( v );
		}
		ifo = h5iometa( PARAPROBE_FOURIER_META_HKL_IVAL, f32.size()/PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX,
				PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_HKL_IVAL create failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_HKL_IVAL create " << status << "\n";
		offs = h5offsets( 0, f32.size()/PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX, 0, PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX,
				f32.size()/PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX, PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_HKL_IVAL write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_HKL_IVAL write " << status << "\n";
		f32 = vector<float>();
	}
	//implicit else

	MPI_Barrier( MPI_COMM_WORLD );

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "MASTERWriteMetadataToHDF5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	if ( get_myrank() == MASTER ) {
		cout << "Rank " << get_myrank() << " writing of material infos into H5 file took " << (toc-tic) << " seconds" << "\n";
	}

	return true;
}


bool fourierHdl::write_materialpoints2_to_apth5()
{
	double tic = MPI_Wtime();

	if ( workers.size() == 0 ) {
		cerr << "Rank " << get_myrank() << " workers is empty!" << "\n"; return false;
	}
	if ( workers.at(0) == NULL ) {
		cerr << "Rank " << get_myrank() << " MASTER worker does not exist!" << "\n"; return false;
	}

	string fwslash = "/";
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";

	//HKLGrid voxel grid


	//material point grid xyz
	vector<float> f32 = vector<float>( PARAPROBE_FOURIER_META_MP_XYZ_NCMAX*mp.matpoints.size(), 0.f ); //MK::we need to store a triplet of XDMF topology type count and vertex ID
	dsnm = PARAPROBE_FOURIER_META_MP_XYZ;
	for ( size_t i = 0; i < mp.matpoints.size(); i++ ) {
		f32.at(i*PARAPROBE_FOURIER_META_MP_XYZ_NCMAX+0) = mp.matpoints[i].x;
		f32.at(i*PARAPROBE_FOURIER_META_MP_XYZ_NCMAX+1) = mp.matpoints[i].y;
		f32.at(i*PARAPROBE_FOURIER_META_MP_XYZ_NCMAX+2) = mp.matpoints[i].z;
	}
	ifo = h5iometa( dsnm, f32.size()/PARAPROBE_FOURIER_META_MP_XYZ_NCMAX, PARAPROBE_FOURIER_META_MP_XYZ_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create MP_XYZ metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, f32.size()/PARAPROBE_FOURIER_META_MP_XYZ_NCMAX, 0, PARAPROBE_FOURIER_META_MP_XYZ_NCMAX,
			f32.size()/PARAPROBE_FOURIER_META_MP_XYZ_NCMAX, PARAPROBE_FOURIER_META_MP_XYZ_NCMAX );
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store MP_XYZ metadata! " << status << "\n"; return false;
	}
	f32 = vector<float>( PARAPROBE_FOURIER_META_MP_R_NCMAX*mp.matpoints.size(), 0.f ); //MK::we need to store a triplet of XDMF topology type count and vertex ID
	dsnm = PARAPROBE_FOURIER_META_MP_R;
	for ( size_t i = 0; i < mp.matpoints.size(); i++ ) {
		f32.at(i*PARAPROBE_FOURIER_META_MP_R_NCMAX+0) = mp.matpoints[i].R;
	}
	ifo = h5iometa( dsnm, f32.size()/PARAPROBE_FOURIER_META_MP_R_NCMAX, PARAPROBE_FOURIER_META_MP_R_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create MP_R metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, f32.size()/PARAPROBE_FOURIER_META_MP_R_NCMAX, 0, PARAPROBE_FOURIER_META_MP_R_NCMAX,
			f32.size()/PARAPROBE_FOURIER_META_MP_R_NCMAX, PARAPROBE_FOURIER_META_MP_R_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store MP_R metadata! " << status << "\n"; return false;
	}
	f32 = vector<float>();

	//matpoint IDs
	vector<unsigned int> u32 = vector<unsigned int>( PARAPROBE_FOURIER_META_MP_ID_NCMAX*mp.matpoints.size(), 0 );
	dsnm = PARAPROBE_FOURIER_META_MP_ID;
	for ( size_t i = 0; i < mp.matpoints.size(); i++ ) {
		u32.at(i*PARAPROBE_FOURIER_META_MP_ID_NCMAX+0) = mp.matpoints[i].mpid;
	}
	ifo = h5iometa( dsnm, u32.size()/PARAPROBE_FOURIER_META_MP_ID_NCMAX, PARAPROBE_FOURIER_META_MP_ID_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create MP_ID metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_META_MP_ID_NCMAX, 0, PARAPROBE_FOURIER_META_MP_ID_NCMAX,
			u32.size()/PARAPROBE_FOURIER_META_MP_ID_NCMAX, PARAPROBE_FOURIER_META_MP_ID_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store MP_ID metadata! " << status << "\n"; return false;
	}

	//XDMF topo
	u32 = vector<unsigned int>( 3*mp.matpoints.size(), 1 ); //MK::we need to store a triplet of XDMF topology type count and vertex ID
	dsnm = PARAPROBE_FOURIER_META_MP_TOPO;
	for ( size_t i = 0; i < mp.matpoints.size(); i++ ) {
		u32.at(3*i+2) = i;
	}
	ifo = h5iometa( dsnm, u32.size()/PARAPROBE_FOURIER_META_MP_TOPO_NCMAX, PARAPROBE_FOURIER_META_MP_TOPO_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create MP_TOPO metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_META_MP_TOPO_NCMAX, 0, PARAPROBE_FOURIER_META_MP_TOPO_NCMAX,
			u32.size()/PARAPROBE_FOURIER_META_MP_TOPO_NCMAX, PARAPROBE_FOURIER_META_MP_TOPO_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store MP_TOPO metadata! " << status << "\n"; return false;
	}
	u32 = vector<unsigned int>();

	//details about the reciprocal space grid
	//details about the grid used
	u32 = vector<unsigned int>( 3, static_cast<unsigned int>(ConfigFourier::HKLGrid.ni) );
	ifo = h5iometa( PARAPROBE_FOURIER_META_HKL_NIJK, u32.size()/PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX,
			PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_FOURIER_META_HKL_NIJK create failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_FOURIER_META_HKL_NIJK create " << status << "\n";
	offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX, 0, PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX,
			u32.size()/PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX, PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_FOURIER_META_HKL_NIJK write failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_FOURIER_META_HKL_NIJK write " << status << "\n";
	u32 = vector<unsigned int>();

	//##MK::make small function which computes this mapping taking only FourierGridResolution as input
	f32 = vector<float>();
	dft_real start = ConfigFourier::HKLGrid.imi;
	dft_real stop = ConfigFourier::HKLGrid.imx;
	dft_real num = static_cast<dft_real>(ConfigFourier::HKLGrid.ni-1);
	dft_real step = (stop-start)/num;
	for( int i = 0; i < ConfigFourier::HKLGrid.ni; ++i ) {
		dft_real v = TWO_PI * (start + static_cast<dft_real>(i)*step);
		f32.push_back( v );
	}
	ifo = h5iometa( PARAPROBE_FOURIER_META_HKL_IVAL, f32.size()/PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX,
			PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_FOURIER_META_HKL_IVAL create failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_FOURIER_META_HKL_IVAL create " << status << "\n";
	offs = h5offsets( 0, f32.size()/PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX, 0, PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX,
			f32.size()/PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX, PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_FOURIER_META_HKL_IVAL write failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_FOURIER_META_HKL_IVAL write " << status << "\n";

	if ( ConfigFourier::IOFullGridPks == true ) {
		vector<float> f32xyz;
		int ni = ConfigFourier::HKLGrid.ni;
		f32xyz.reserve( PARAPROBE_FOURIER_META_HKL_GRID_NCMAX * CUBE(ni) );
		for( int k = 0; k < ni; k++ ) {
			float ival_k = f32[k];
			for( int j = 0; j < ni; j++ ) {
				float ival_j = f32[j];
				for( int i = 0; i < ni; i++ ) {
					float ival_i = f32[i];
					f32xyz.push_back( ival_i );
					f32xyz.push_back( ival_j );
					f32xyz.push_back( ival_k );
				}
			}
		}
		ifo = h5iometa( PARAPROBE_FOURIER_META_HKL_GRID, f32xyz.size()/PARAPROBE_FOURIER_META_HKL_GRID_NCMAX,
					PARAPROBE_FOURIER_META_HKL_GRID_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_HKL_GRID create failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_HKL_GRID create " << status << "\n";
		offs = h5offsets( 0, f32xyz.size()/PARAPROBE_FOURIER_META_HKL_GRID_NCMAX, 0, PARAPROBE_FOURIER_META_HKL_GRID_NCMAX,
				f32xyz.size()/PARAPROBE_FOURIER_META_HKL_GRID_NCMAX, PARAPROBE_FOURIER_META_HKL_GRID_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32xyz );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_HKL_GRID write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_HKL_GRID write " << status << "\n";

		//topology
		unsigned int NIJK = CUBE(static_cast<unsigned int>(ni));
		vector<unsigned int> u32topo = vector<unsigned int>( 3*NIJK, 1 ); //XDMF keyword and point type
		for ( unsigned int i = 0; i < NIJK; i++ ) {
			u32topo.at(3*i+2) = i;
		}
		ifo = h5iometa( PARAPROBE_FOURIER_META_HKL_TOPO, u32topo.size()/PARAPROBE_FOURIER_META_HKL_TOPO_NCMAX,
					PARAPROBE_FOURIER_META_HKL_TOPO_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_HKL_TOPO create failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_HKL_TOPO create " << status << "\n";
		offs = h5offsets( 0, u32topo.size()/PARAPROBE_FOURIER_META_HKL_TOPO_NCMAX, 0, PARAPROBE_FOURIER_META_HKL_TOPO_NCMAX,
				u32topo.size()/PARAPROBE_FOURIER_META_HKL_TOPO_NCMAX, PARAPROBE_FOURIER_META_HKL_TOPO_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32topo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_HKL_TOPO write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_HKL_TOPO write " << status << "\n";

		f32xyz = vector<float>();
	}
	//IV inside f32 now no longer required
	f32 = vector<float>();

	//phases
	u32.push_back( itsk.iphcands.size() );
	dsnm = PARAPROBE_FOURIER_META_PHCAND_N;
	ifo = h5iometa( dsnm, u32.size()/PARAPROBE_FOURIER_META_PHCAND_N_NCMAX, PARAPROBE_FOURIER_META_PHCAND_N_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " PARAPROBE_FOURIER_META_PHCAND_N create failed! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_META_PHCAND_N_NCMAX, 0, PARAPROBE_FOURIER_META_PHCAND_N_NCMAX,
				u32.size()/PARAPROBE_FOURIER_META_PHCAND_N_NCMAX, PARAPROBE_FOURIER_META_PHCAND_N_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " PARAPROBE_FOURIER_META_PHCAND_N write failed! " << status << "\n"; return false;
	}
	u32 = vector<unsigned int>();

	for( auto mtsk = itsk.iphcands.begin(); mtsk != itsk.iphcands.end(); mtsk++ ) {
		string grpnm = PARAPROBE_FOURIER_META_PHCAND + fwslash + "PH" + to_string(mtsk->candid);
		status = debugh5Hdl.create_group( grpnm );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "Rank " << get_myrank() << " grpnm " << grpnm << " create failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " grpnm " << grpnm << " create " << status << "\n";

		vector<unsigned char> u8_id;
		vector<unsigned char> u8_wh;
		for( int tg = 0; tg < TARGET_LIST_SIZE; tg++ ) {
			unsigned char current_ityp = mtsk->targets[tg];
			if ( current_ityp != 0xFF ) {
				u8_id.push_back( current_ityp );
				evapion3 tmp = rng.iontypes.at(static_cast<int>(current_ityp)).strct;
				u8_wh.push_back( tmp.Z1 );
				u8_wh.push_back( tmp.N1 );
				u8_wh.push_back( tmp.Z2 );
				u8_wh.push_back( tmp.N2 );
				u8_wh.push_back( tmp.Z3 );
				u8_wh.push_back( tmp.N3 );
				u8_wh.push_back( tmp.sign );
				u8_wh.push_back( tmp.charge );
			}
		}

		dsnm = grpnm + fwslash + PARAPROBE_FOURIER_META_PHCAND_ID;
		ifo = h5iometa( dsnm, u8_id.size()/PARAPROBE_FOURIER_META_PHCAND_ID_NCMAX, PARAPROBE_FOURIER_META_PHCAND_ID_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " create failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " create " << status << "\n";
		offs = h5offsets( 0, u8_id.size()/PARAPROBE_FOURIER_META_PHCAND_ID_NCMAX, 0, PARAPROBE_FOURIER_META_PHCAND_ID_NCMAX,
						u8_id.size()/PARAPROBE_FOURIER_META_PHCAND_ID_NCMAX, PARAPROBE_FOURIER_META_PHCAND_ID_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, u8_id );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " write failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " write " << status << "\n";
		u8_id = vector<unsigned char>();

		dsnm = grpnm + fwslash + PARAPROBE_FOURIER_META_PHCAND_WHAT;
		ifo = h5iometa( dsnm, u8_wh.size()/PARAPROBE_FOURIER_META_PHCAND_WHAT_NCMAX, PARAPROBE_FOURIER_META_PHCAND_WHAT_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " create failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " create " << status << "\n";
		offs = h5offsets( 0, u8_wh.size()/PARAPROBE_FOURIER_META_PHCAND_WHAT_NCMAX, 0, PARAPROBE_FOURIER_META_PHCAND_WHAT_NCMAX,
						u8_wh.size()/PARAPROBE_FOURIER_META_PHCAND_WHAT_NCMAX, PARAPROBE_FOURIER_META_PHCAND_WHAT_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, u8_wh );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " write failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " write " << status << "\n";
		u8_wh = vector<unsigned char>();
		//instantiate groups for phase-specific results were instantiated at init_targetfile
	}

	string xdmffn = "PARAPROBE.Fourier.Results.SimID." + to_string(ConfigShared::SimID) + ".MatPoints.xdmf";
	debugxdmf.create_materialpoint_file( xdmffn, mp.matpoints.size(), debugh5Hdl.h5resultsfn );

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "WriteMatPointsResultsH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}




bool fourierHdl::write_results_to_apth5()
{
	//##MK::error management
	double tic = MPI_Wtime();

	if ( get_myrank() == MASTER ) { //only the master performs I/O

		if ( mpres.size() < 1 ) {
			cout << "WARNING:: There are no material point results to report" << "\n"; return true;
		}

		//hklvalues
		vector<unsigned int> u32;
		u32.reserve( mpres.size() );
		for( auto it = mpres.begin(); it != mpres.end(); it++ ) {
			u32.push_back( it->mpid );
		}
		h5iometa ifo = h5iometa( PARAPROBE_FOURIER_RES_HKL_MPID, u32.size()/PARAPROBE_FOURIER_RES_HKL_MPID_NCMAX,
				PARAPROBE_FOURIER_RES_HKL_MPID_NCMAX );
		int status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_RES_HKL_MPID create failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_RES_HKL_MPID create " << status << "\n";
		h5offsets offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_RES_HKL_MPID_NCMAX, 0, PARAPROBE_FOURIER_RES_HKL_MPID_NCMAX,
				u32.size()/PARAPROBE_FOURIER_RES_HKL_MPID_NCMAX, PARAPROBE_FOURIER_RES_HKL_MPID_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_RES_HKL_MPID write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_RES_HKL_MPID write " << status << "\n";
		u32 = vector<unsigned int>();

		u32.reserve( mpres.size() );
		for( auto it = mpres.begin(); it != mpres.end(); it++ ) {
			u32.push_back( it->ijk );
		}
		ifo = h5iometa( PARAPROBE_FOURIER_RES_HKL_MAXIJK, u32.size()/PARAPROBE_FOURIER_RES_HKL_MAXIJK_NCMAX,
				PARAPROBE_FOURIER_RES_HKL_MAXIJK_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_RES_HKL_MAXIJK create failed! " << status << "\n"; return false;
		}
cout << "PARAPROBE_FOURIER_RES_HKL_MAXIJK create " << status << "\n";
		offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_RES_HKL_MAXIJK_NCMAX, 0, PARAPROBE_FOURIER_RES_HKL_MAXIJK_NCMAX,
				u32.size()/PARAPROBE_FOURIER_RES_HKL_MAXIJK_NCMAX, PARAPROBE_FOURIER_RES_HKL_MAXIJK_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_RES_HKL_MAXIJK write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_RES_HKL_MAXIJK write " << status << "\n";
		u32 = vector<unsigned int>();

		vector<float> f32;
		f32.reserve( mpres.size() );
		for( auto it = mpres.begin(); it != mpres.end(); it++ ) {
			f32.push_back( it->value );
		}
		ifo = h5iometa( PARAPROBE_FOURIER_RES_HKL_MAXVAL, f32.size()/PARAPROBE_FOURIER_RES_HKL_MAXVAL_NCMAX,
				PARAPROBE_FOURIER_RES_HKL_MAXVAL_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_RES_HKL_MAXVAL create failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_RES_HKL_MAXVAL create " << status << "\n";
		offs = h5offsets( 0, f32.size()/PARAPROBE_FOURIER_RES_HKL_MAXVAL_NCMAX, 0, PARAPROBE_FOURIER_RES_HKL_MAXVAL_NCMAX,
				f32.size()/PARAPROBE_FOURIER_RES_HKL_MAXVAL_NCMAX, PARAPROBE_FOURIER_RES_HKL_MAXVAL_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_RES_HKL_MAXVAL write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_RES_HKL_MAXVAL_NCMAX write " << status << "\n";
		f32 = vector<float>();
	}
	//implicit else

	MPI_Barrier( MPI_COMM_WORLD );

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "MASTERWriteResultsToHDF5", APT_XX, APT_IS_PAR, mm, tic, toc);

	if ( get_myrank() == MASTER ) {
		cout << "Rank " << get_myrank() << " writing of materials results into H5 file took " << (toc-tic) << " seconds" << "\n";
	}

	return true;
}



bool fourierHdl::write_results2_to_apth5()
{
	double tic = MPI_Wtime();

	if ( workers.size() == 0 ) {
		cerr << "Rank " << get_myrank() << " workers is empty!" << "\n"; return true;
	}
	if ( workers.at(0) == NULL ) {
		cerr << "Rank " << get_myrank() << " MASTER worker does not exist!" << "\n"; return true;
	}

	string fwslash = "/";
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";
	size_t roffset = 0;

	//##MK::DEBUG simple OpenMP profiling per material point
	string tictoc_fn = "PARAPROBE.Fourier.SimID." + to_string(ConfigShared::SimID) + ".Rank." + to_string(get_myrank()) + ".DetailedProfiling.csv";
	ofstream csvlog;
	csvlog.open(tictoc_fn.c_str(), ofstream::out | ofstream::trunc);
	if (csvlog.is_open() == true) {
		//header
		csvlog << "MPID;PHCANDID;IonsInROI;Rank;ThreadID;DirectFT;FindPks;Fmin;Fmax;DBScanPks\n";
		csvlog << ";;;;;s;s;;;s\n";
		csvlog << "MPID;PHCANDID;IonsInROI;Rank;ThreadID;DirectFT;FindPks;Fmin;Fmax;DBScanPks\n";
	}
	else {
		cerr << "Unable to write process-local profiling files" << "\n";
	}
	int rank = get_myrank();
	//##MK::DEBUG

	for( size_t thr = 0; thr < workers.size(); thr++ ) {
		acquisor* thrhdl = workers.at(thr);
		if ( thrhdl != NULL ) {
			for( auto it = thrhdl->res.begin(); it != thrhdl->res.end(); it++ ) {

				int mpid = it->mpid;
				int phcid = it->phcandid;
				fourier_res* thisone = it->dat;

//cout << "Rank " << get_myrank() << " mpid/phcid/thisone " << mpid << ";" << phcid << ";" << thisone << "\n";

				//to separate or not the results of different images or use aggregated hyperslabs?
				//++ separation is much more intuitive to read and check for implementation errors, in particular for non experts
				//++ allows for explicit visualization of single material point results
				//-- is less performant because more datasets to create, in particular when doing so using PHDF5
				//##MK::here I opted to use the separated approach
				if ( thisone != NULL ) {

					if ( ConfigFourier::IOFullGridPks == true && thisone->AllPeaks != NULL ) {
//cout << "Rank " << get_myrank() << " IOFullGridPks mpid/phcid/thisone " << mpid << ";" << phcid << ";" << thisone << "\t\t" << thisone->SpecificPeaksTbl.size() << "\n";

						dsnm = PARAPROBE_FOURIER_RES_HKLGRID + fwslash + "PH" + to_string(phcid) + fwslash + to_string(mpid);
						size_t NIJK = CUBE(static_cast<size_t>(ConfigFourier::HKLGrid.ni));
						ifo = h5iometa( dsnm, NIJK/1, 1 );
						status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
						if ( status == WRAPPED_HDF5_SUCCESS ) {
							offs = h5offsets( 0, NIJK/1, 0, 1, NIJK/1, 1);
							status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, thisone->AllPeaks );
							if ( status == WRAPPED_HDF5_SUCCESS ) {
								delete [] thisone->AllPeaks; thisone->AllPeaks = NULL;
//cout << "Rank " << get_myrank() << " wrote SpecificPeak " << dsnm << " status " << status << "\n";
							}
							else {
								cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to store " << dsnm << " status " << status << "\n"; return false;
							}
						}
						else {
							cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to create " << dsnm << " status " << status << "\n"; return false;
						}
					}
					if ( ConfigFourier::IOSignificantPks == true && thisone->SignificantPeaks.size() > 0 ) {
//cout << "Rank " << get_myrank() << " IOSSignificantPks mpid/phcid/thisone " << mpid << ";" << phcid << ";" << thisone << "\t\t" << thisone->ThreePeaksTbl.size() << "\n";

						dsnm = PARAPROBE_FOURIER_RES_HKLREFL + fwslash + "PH" + to_string(phcid) + fwslash + to_string(mpid);
						//in-place writing of thisone->ThreePeaksTbl
						//##MK::replace struct by
						vector<float> f32val = vector<float>( PARAPROBE_FOURIER_RES_HKLREFL_NCMAX*thisone->SignificantPeaks.size(), 0.f);
						size_t ii = 0;
						for( auto it = thisone->SignificantPeaks.begin(); it != thisone->SignificantPeaks.end(); it++ ) {
							f32val.at(ii) = it->h; ii++;
							f32val.at(ii) = it->k; ii++;
							f32val.at(ii) = it->l; ii++;
							f32val.at(ii) = it->SQRIntensity; ii++;
						}
						thisone->SignificantPeaks = vector<hklspace_res>();
						ifo = h5iometa( dsnm, f32val.size()/PARAPROBE_FOURIER_RES_HKLREFL_NCMAX, PARAPROBE_FOURIER_RES_HKLREFL_NCMAX);
						status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
						if ( status == WRAPPED_HDF5_SUCCESS ) {
							offs = h5offsets( 0, f32val.size()/PARAPROBE_FOURIER_RES_HKLREFL_NCMAX,
										0, PARAPROBE_FOURIER_RES_HKLREFL_NCMAX,
										f32val.size()/PARAPROBE_FOURIER_RES_HKLREFL_NCMAX, PARAPROBE_FOURIER_RES_HKLREFL_NCMAX);
							status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32val );
							if ( status == WRAPPED_HDF5_SUCCESS ) {
//cout << "Rank " << get_myrank() << " wrote SignificantPeaks " << dsnm << " status " << status << "\n";
								f32val = vector<float>();
							}
							else {
								cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to store " << dsnm << " status " << status << "\n"; return false;
							}
						}
						else {
							cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to create " << dsnm << " status " << status << "\n"; return false;
						}
					}

					if ( ConfigFourier::IODBScanOnSgnPks == true && thisone->Reflectors.size() > 0 ) {
						dsnm = PARAPROBE_FOURIER_RES_HKLDBSCAN + fwslash + "PH" + to_string(phcid) + fwslash + to_string(mpid);
						//##MK::replace struct by
						vector<double> f64 = vector<double>( PARAPROBE_FOURIER_RES_HKLDBSCAN_NCMAX*thisone->Reflectors.size(), 0.0);
						size_t ii = 0;
						for( auto it = thisone->Reflectors.begin(); it != thisone->Reflectors.end(); it++ ) {
							f64.at(ii) = it->u; ii++;
							f64.at(ii) = it->v; ii++;
							f64.at(ii) = it->w; ii++;
							f64.at(ii) = it->SUMIntensity; ii++;
							f64.at(ii) = it->PksCnts; ii++;
						}
						thisone->Reflectors = vector<reflector>();
						ifo = h5iometa( dsnm, f64.size()/PARAPROBE_FOURIER_RES_HKLDBSCAN_NCMAX,
								PARAPROBE_FOURIER_RES_HKLDBSCAN_NCMAX);
						status = debugh5Hdl.create_contiguous_matrix_f64le( ifo );
						if ( status == WRAPPED_HDF5_SUCCESS ) {
							offs = h5offsets( 0, f64.size()/PARAPROBE_FOURIER_RES_HKLDBSCAN_NCMAX,
										0, PARAPROBE_FOURIER_RES_HKLDBSCAN_NCMAX,
										f64.size()/PARAPROBE_FOURIER_RES_HKLDBSCAN_NCMAX, PARAPROBE_FOURIER_RES_HKLDBSCAN_NCMAX);
							status = debugh5Hdl.write_contiguous_matrix_f64le_hyperslab( ifo, offs, f64 );
							if ( status == WRAPPED_HDF5_SUCCESS ) {
//cout << "Rank " << get_myrank() << " wrote Reflector " << dsnm << " status " << status << "\n";
								f64 = vector<double>();
							}
							else {
								cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to store " << dsnm << " status " << status << "\n"; return false;
							}
						}
						else {
							cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to create " << dsnm << " status " << status << "\n"; return false;
						}
					}

					if ( csvlog.is_open() == true ) {
						csvlog << mpid << ";" << phcid << ";" <<
								thisone->tictoc[TICTOC_IONSINROI] << ";" << rank << ";" << thisone->tictoc[TICTOC_THREADID] <<
								";" << thisone->tictoc[TICTOC_DIRECTFT] << ";" << thisone->tictoc[TICTOC_FINDPKS] << ";" <<
								thisone->intensity[HKLREFL_FMIN] << ";" << thisone->intensity[HKLREFL_FMAX] << ";" <<
								thisone->tictoc[TICTOC_DBSCANPKS] << "\n";
					}
					//MK::it->dat remains != NULL to indicate we have results for this one
				} //done writing all desired and populated fields for a specific material point and phase
				//no else because if no results then there were no results achieved for the material point maybe because there were no or not sufficient ions to support the analysis
			} //next result from this thread
		}
	} //next thread of this rank

	if ( csvlog.is_open() == true ) {
		csvlog.flush();
		csvlog.close();
	}

	//strict I/O policy, all matpoints with their results need to end up in file, otherwise we already went out with a false
	double toc = MPI_Wtime();
	memsnapshot mm = fourier_tictoc.get_memoryconsumption();
	fourier_tictoc.prof_elpsdtime_and_mem( "WriteResultsH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


int fourierHdl::get_myrank()
{
	return this->myrank;
}


int fourierHdl::get_nranks()
{
	return this->nranks;
}


void fourierHdl::set_myrank( const int rr )
{
	this->myrank = rr;
}


void fourierHdl::set_nranks( const int nn )
{
	this->nranks = nn;
}


void fourierHdl::init_mpidatatypes()
{
	MPI_Type_contiguous(2, MPI_UNSIGNED_LONG_LONG, &MPI_Ranger_DictKeyVal_Type);
	MPI_Type_commit(&MPI_Ranger_DictKeyVal_Type);

	MPI_Type_contiguous(3, MPI_FLOAT, &MPI_Synth_XYZ_Type);
	MPI_Type_commit(&MPI_Synth_XYZ_Type);

	MPI_Type_contiguous(9, MPI_UNSIGNED_CHAR, &MPI_Ranger_Iontype_Type);
	MPI_Type_commit(&MPI_Ranger_Iontype_Type);

	int cnts[2] = {2, 4};
	MPI_Aint dsplc[2] = {0, 2 * MPIIO_OFFSET_INCR_F32};
	MPI_Datatype otypes[2] = {MPI_FLOAT, MPI_UNSIGNED_CHAR};
	MPI_Type_create_struct(2, cnts, dsplc, otypes, &MPI_Ranger_MQIval_Type);
	MPI_Type_commit(&MPI_Ranger_MQIval_Type);

	//define MPI datatype which assists us to logically structure the communicated data packages
	MPI_Type_contiguous(3, MPI_FLOAT, &MPI_IonPositions_Type);
	MPI_Type_commit(&MPI_IonPositions_Type);

	MPI_Type_contiguous(4, MPI_FLOAT, &MPI_IonWithDistance_Type);
	MPI_Type_commit(&MPI_IonWithDistance_Type);

	MPI_Type_contiguous(9, MPI_FLOAT, &MPI_Triangle3D_Type);
	MPI_Type_commit(&MPI_Triangle3D_Type);

	int cnts0[2] = {6+3, 3};
	MPI_Aint dsplc0[2] = {0, (6+3)* MPIIO_OFFSET_INCR_F32};
	MPI_Datatype otypes0[2] = {MPI_FLOAT, MPI_UNSIGNED};
	MPI_Type_create_struct(2, cnts0, dsplc0, otypes0, &MPI_VxlizationInfo_Type);
	MPI_Type_commit(&MPI_VxlizationInfo_Type);

	//commit utility data types to simplify and collate data transfer operations
	int cnts1[2] = {4, 2};
	MPI_Aint dsplc1[2] = {0, 4 * 4}; //4 B for a float
	MPI_Datatype otypes1[2] = {MPI_FLOAT, MPI_UNSIGNED};
	MPI_Type_create_struct(2, cnts1, dsplc1, otypes1, &MPI_Fourier_ROI_Type);
	MPI_Type_commit(&MPI_Fourier_ROI_Type);

	int cnts2[2] = {2, 1};
	MPI_Aint dsplc2[2] = {0, 2 * 4 }; //4 B for an int32
	MPI_Datatype otypes2[2] = {MPI_INT, MPI_FLOAT};
	MPI_Type_create_struct(2, cnts2, dsplc2, otypes2, &MPI_Fourier_HKLValue_Type);
	MPI_Type_commit(&MPI_Fourier_HKLValue_Type);
}
