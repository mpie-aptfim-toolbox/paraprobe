//##MK::GPLV3

#include "PARAPROBE_FourierPhaseCandHdl.h"

ostream& operator << (ostream& in, TypeCombi const & val) {
	in << "__" << val.targets << "\n"; //"__\t\t__" << val.realspacestr << "__" << "\n";
	return in;
}


PhaseCandidate::PhaseCandidate()
{
	for( size_t i = 0; i < TARGET_LIST_SIZE; i++ ) {
		targets[i] = 0xFF;
	}
	//realspace = 0.0;
	candid = 0;
}


PhaseCandidate::PhaseCandidate( vector<unsigned char> const & tg, const unsigned int _tskid ) //const apt_real _a, const unsigned int _tskid )
{
	if ( tg.size() <= TARGET_LIST_SIZE ) {
		for( size_t i = 0; i < tg.size(); i++ ) {
			targets[i] = tg.at(i);
		}
		//realspace = _a;
		candid = _tskid;
	}
	else {
		cerr << "Attempting to pass more than " << TARGET_LIST_SIZE << " targets which is currently not supported, increase TARGET_LIST_SIZE and recompile!" << "\n";
		for( size_t i = 0; i < TARGET_LIST_SIZE; i++ ) { //##MK::no target is taken!
			targets[i] = 0xFF;
		}
		//realspace = 0.0;
		candid = 0;
	}
}


ostream& operator << (ostream& in, PhaseCandidate const & val) {
	for ( size_t i = 0; i < TARGET_LIST_SIZE; i++ ) {
		in << "Targets = " << (int) val.targets[i] << ";";
	}
	//in << "Realspace = " << val.realspace << "nm" << "\n";
	in << "CandidateID = " << val.candid << "\n";
	return in;
}



itypeCombiHdl::itypeCombiHdl()
{
}


itypeCombiHdl::~itypeCombiHdl()
{
}


bool itypeCombiHdl::load_iontype_combinations( string xmlfn )
{
	//cout << "Importing itypeCombinations..." << "\n";
	ifstream file( xmlfn );
	if ( file.fail() ) {
		cerr << "Unable to locate input file " << xmlfn << "\n"; return false;
	}
	else {
		cout << "Opening input file " << xmlfn << "\n";
	}

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigFourier")) {
		cerr << "Undefined parameters file!" << "\n"; return false;
	}

	xml_node<>* comb_node = rootNode->first_node("LatticeIontypeCombinations");
	int i = 0;
	for (xml_node<> * entry_node = comb_node->first_node("entry"); entry_node; entry_node = entry_node->next_sibling() ) {
		string trg = entry_node->first_attribute("targets")->value();
		//string rsp = entry_node->first_attribute("realspace")->value();

		if ( trg != "" ) { //&& rsp != "" ) {
			//this->icombis.push_back( TypeCombi( trg, rsp) );
			this->icombis.push_back( TypeCombi( trg ) );
cout << this->icombis.back() << "\n";

			string ky = "LatticeIontypeCombi" + to_string(i); i++;
			string vl = "Targets;" + trg; // + ";RealSpace;" + rsp
//##MK::	this->iifo.push_back( pparm( ky, vl, "", "" ) );
		}
		else {
			cout << "WARNING attempt to pass an inacceptable LatticeIontypeCombination entry both targets and realspace are empty!" << "\n"; return false;
		}
	}

	return true;
}
