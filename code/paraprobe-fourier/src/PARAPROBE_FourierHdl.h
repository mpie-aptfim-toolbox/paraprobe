//##MK::GPLV3

#ifndef __PARAPROBE_FOURIER_HDL_H__
#define __PARAPROBE_FOURIER_HDL_H__

//here is the level where we loop in all tool-specific utils
//#include "PARAPROBE_DFTHdlGPU.h" //##MK::move to utilities
#include "PARAPROBE_FourierAcquisor.h"


class fourierHdl
{
	//process-level class which implements the worker instance which executes direct Fourier transforms on reconstruction
	//using OpenMP and OpenACC i.e. CPU and GPUs
	//specimens result are written to a specific HDF5 file

public:
	fourierHdl();
	~fourierHdl();
	
	bool read_periodictable();
	bool read_iontype_combinations( string fn );

	bool read_reconxyz_ranging_from_apth5();
	bool read_reconxyz_from_apth5();
	bool read_ranging_from_apth5();
	bool read_itypes_from_apth5();
	bool read_trianglehull_from_apth5();
	bool read_voxelized_from_apth5();

	bool broadcast_reconxyz();
	bool broadcast_ranging();
	bool broadcast_triangles();
	bool broadcast_vxlized();
	bool broadcast_distances();
	//bool broadcast_reconstruction_and_triangles();
	
	bool define_phase_candidates();
	void itype_sensitive_spatial_decomposition();
	//void spatial_decomposition();
	void define_matpoint_volume_grid();
	void distribute_matpoints_on_processes();
	void distribute_matpoints_on_processes_roundrobin();

	gpu_cpu_max_workload plan_max_per_epoch( const int ngpu, const int nthr, const int nmp );
	gpu_cpu_now_workload plan_now_per_epoch( const int ngpu, const int nthr, const int nmp, const int mp, const gpu_cpu_max_workload mxl );
	//void generate_debug_roi( mt19937 & mydice, vector<dft_real> * out );
	void query_ions_within_roi( mp3d const & roicenter, vector<dft_real> * out );
	void debug_epoch_profiling( vector<epoch_log> const & in, vector<epoch_prof> const & timing );
	void execute_local_workpackage1();
	void execute_local_workpackage2();

	
	bool init_target_file();
	bool init_target_file2();
	bool write_environment_and_settings();

	bool write_materialpoints_to_apth5();
	bool write_materialpoints2_to_apth5();
	bool collect_results_on_masterprocess();
	bool write_results_to_apth5();
	bool write_results2_to_apth5();

	int get_myrank();
	int get_nranks();
	void set_myrank( const int rr );
	void set_nranks( const int nn );
	void init_mpidatatypes();

	rangeTable rng;
	itypeCombiHdl itsk;
	vector<PhaseCandidate> cands;
	unsigned char maximum_iontype_uc;

	decompositor sp;
	volsampler mp;
	vector<int> mp2rk;								//mapping of material points => to ranks,
	vector<int> mp2me;
	
	//xdmfHdl debugxdmfHdl;
	h5Hdl inputReconH5Hdl;
	h5Hdl inputTriHullH5Hdl;
	fourier_h5 debugh5Hdl;
	fourier_xdmf debugxdmf;

	vector<p3d> xyz;
	vector<unsigned char> ityp_org;					//iontypes ranged, original
	//vector<p3dm1> xyz_ityp;							//##MK::temporarily until proper ion type handling implemented
	vector<tri3d> trianglehull;						//a triangle hull to the data set
	vector<apt_real> dist2hull;						//distance field

	vxlizer ca;

	vector<mp3d> myworkload;						//the material points of the process
	vector<hklval> myhklval;						//##MK::DEBUG for now the actual data of interest for science


	vector<MPI_Fourier_ROI> mproi;					//only significant for MASTER to organize results writing
	vector<MPI_Fourier_HKLValue> mpres;				//only significant for MASTER to organize results writing

	vector<acquisor*> workers;

	profiler fourier_tictoc;

private:
	//MPI related
	int myrank;										//my MPI ID in the MPI_COMM_WORLD
	int nranks;

	MPI_Datatype MPI_Ranger_DictKeyVal_Type;
	MPI_Datatype MPI_Synth_XYZ_Type;
	MPI_Datatype MPI_Ranger_Iontype_Type;
	MPI_Datatype MPI_Ranger_MQIval_Type;

	MPI_Datatype MPI_IonPositions_Type;
	MPI_Datatype MPI_IonWithDistance_Type;
	MPI_Datatype MPI_Triangle3D_Type;
	MPI_Datatype MPI_VxlizationInfo_Type;
	MPI_Datatype MPI_Fourier_ROI_Type;
	MPI_Datatype MPI_Fourier_HKLValue_Type;
};





#endif

