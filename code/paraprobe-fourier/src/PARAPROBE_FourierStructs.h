//##MK::GPLV3

#ifndef __PARAPROBE_FOURIER_STRUCTS_H__
#define __PARAPROBE_FOURIER_STRUCTS_H__

#include "PARAPROBE_DFTHdlGPU.h"


struct iontype_ival
{
	size_t incl_left;
	size_t excl_right;
	size_t m;
	iontype_ival() : incl_left(0), excl_right(0), m(0) {}
	iontype_ival( const size_t _left, const size_t _right ) :
		incl_left(_left), excl_right(_right), m(0) {}
	iontype_ival( const size_t _left, const size_t _right, const size_t _m ) :
		incl_left(_left), excl_right(_right), m(_m) {}
};

ostream& operator << (ostream& in, iontype_ival const & val);

/*
struct binning_info
{
	apt_real R;
	apt_real dR;
	apt_real binner;			//multiplier
	apt_real RdR;				//intermediate
	unsigned int Rpadding; 		//per side
	unsigned int NumberOfBins;
	unsigned int NumberOfBinsHalf;
	binning_info() : R(0.f), dR(0.f), binner(0.f), RdR(0.f), Rpadding(0), NumberOfBins(0), NumberOfBinsHalf(0) {}
	binning_info( const apt_real _R, const apt_real _dR, const apt_real _binner, const apt_real _RdR,
			const unsigned int _rpad, const unsigned int _nbins, const unsigned int _nbins2 ) :
				R(_R), dR(_dR), binner(_binner), RdR(_RdR), Rpadding(_rpad), NumberOfBins(_nbins), NumberOfBinsHalf(_nbins2) {}
};

ostream& operator << (ostream& in, binning_info const & val);


struct threepeaks
{
	//apt_real elevation;
	//apt_real azimuth;
	pair<float,float> max1; //bin, val
	pair<float,float> max2;
	pair<float,float> max3;
	threepeaks() : 	max1(pair<float,float>(0.f,0.f)),
					max2(pair<float,float>(0.f,0.f)),
					max3(pair<float,float>(0.f,0.f)) {}
	threepeaks( pair<float,float> const & _m1, pair<float,float> const & _m2, pair<float,float> const & _m3 ) :
			max1(_m1), max2(_m2), max3(_m3) {}
};

ostream& operator << (ostream& in, threepeaks const & val);
*/

#endif
