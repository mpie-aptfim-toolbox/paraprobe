//##MK::GPLV3

#ifndef __PARAPROBE_FOURIER_ACQUISOR_H__
#define __PARAPROBE_FOURIER_ACQUISOR_H__

#include "PARAPROBE_FourierPhaseCandHdl.h"

class acquisor;

class fourier_res;

struct fourier_res_node
{
	fourier_res* dat;
	int mpid;
	int phcandid;
	fourier_res_node() : dat(NULL), mpid(INT32MX), phcandid(INT32MX) {}
	fourier_res_node( const int _mpid, const int _phcid ) :
		 dat(NULL), mpid(_mpid), phcandid(_phcid) {}
};

//storing relative peak intensities
#define HKLREFL_FMIN		0
#define HKLREFL_FMAX		1

//storing of timing data
#define TICTOC_IONSINROI	0
#define TICTOC_THREADID		1
#define TICTOC_DIRECTFT		2
#define TICTOC_FINDPKS		3
#define TICTOC_DBSCANPKS	4

class fourier_res
{
public:
	fourier_res();
	~fourier_res();
	
	p3d mp;
	bool healthy;

	void direct_ft_cpu( vector<p3d> const & cand );
	void postprocess_cpu();
	void dbscanpks_cpu();

#ifdef UTILIZE_GPUS
	void direct_ft_gpu( vector<p3d> const & cand );
#endif

	acquisor* owner;

	//implicit results arrays per material point
	dft_real* IV;
	dft_real* AllPeaks;
	//##MK::we use C-style arrays here to pass directly to GPUs
	vector<hklspace_res> SignificantPeaks;
	vector<reflector> Reflectors;
	
	dft_real intensity[2];
	double tictoc[5];
};


class acquisor
{
public:
	acquisor();
	~acquisor();

	vector<fourier_res_node> res;
};

#endif
