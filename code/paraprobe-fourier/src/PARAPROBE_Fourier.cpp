//##MK::GPLV3

#include "PARAPROBE_FourierHdl.h"


//define what is passed upon execution
#define SIMID									1
#define CONTROLFILE								2

//##MK::for querying system maximum physical memory see
//https://stackoverflow.com/questions/2513505/how-to-get-available-memory-c-g

#ifndef GITSHA									//otherwise we could not compile without adding -DGITSHA="d123" a dummy GitSha
	#define GITSHA								0
#endif
//stringification of strings passed from cmake at compile time
//https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html
#define xstr(s)									str(s)
#define str(s) 									#s


void hello( void )
{
	cout << "paraprobe-fourier" << "\n";
	cout << xstr(GITSHA) << "\n";
	cout << "Collecting " << CiteMeFourier::cite() << " publications for the tool to cite:" << "\n";
	for( auto it = CiteMeFourier::Citations.begin(); it != CiteMeFourier::Citations.end(); it++ ) {
		cout << it->author << "\n";
		cout << it->title << "\n";
		cout << it->journal << ", " << it->year << ", " << it->vol << ", pp" << it->pages << "\n";
		cout << it->doi << "\n";
	}
}


bool init(  int pargc, char** pargv )
{
	ConfigShared::SimID = stoul( pargv[SIMID] );
	try {
		ConfigFourier::readXML(pargv[CONTROLFILE]);
	}
	catch (exception& e) {
		cerr << "\n" << "Unable to parse control file! Details:" << "\n" << e.what() << "\n"; return false;
	}
	if ( ConfigFourier::checkUserInput() == false ) {
		cerr << "\n" << "Control file settings failed the validity check!" << "\n"; return false;
	}
	else {
		cout << "\n" << "\t\tInput is valid under SimulationID = " << "SimID." <<  ConfigShared::SimID << "\n";
	}
	cout << endl;
	return true;
}


void many_local_direct_fourier_transforms_on_reconstruction( const int r, const int nr, char** pargv  )
{
	//allocate process-level instance of a fourierHdl. The instance handles all process-internal
	//processing, the instances synchronize across and communicate with each other during execution
	double ttic = MPI_Wtime();

	fourierHdl* fourier = NULL;

	int localhealth = 1;
	int globalhealth = nr;
	try {
		fourier = new fourierHdl;
		fourier->set_myrank(r);
		fourier->set_nranks(nr);
		fourier->init_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Rank " << r << " unable to allocate a sequential surfacerHdl instance" << "\n"; localhealth = 0;
	}

	if ( fourier->read_periodictable() == true ) {
		cout << "Rank " << r << " reads PSE successfully rng.nuclides.isotopes.size() " << fourier->rng.nuclides.isotopes.size() << "\n";
	}
	else {
		cerr << "Rank " << r << " loading of PeriodicTableOfElements failed!" << "\n"; localhealth = 0;
	}

	string xmlfn = pargv[CONTROLFILE];
	if ( fourier->read_iontype_combinations( xmlfn ) == true ) {
		cout << "Rank " << r << " reads IontypeCombinations successfully " << "\n";
	}
	else {
		cerr << "Rank " << r << " loading of IontypeCombinations failed!" << "\n"; localhealth = 0;
	}

	//do we have all processes on board?
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete fourier; fourier = NULL; return;
	}
	
	double ttoc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier->fourier_tictoc.prof_elpsdtime_and_mem( "ReadXMLParameter", APT_XX, APT_IS_SEQ, mm, ttic, ttoc );
	ttic = MPI_Wtime();

	//we have all on board, read reconstruction and triangle hull
	//MK::we could have all processes loading from the same file, or have only MASTER do so and broadcast, we opt for the latter strategy
	//##MK::should not be a problem because initialization is orders of magnitude cheaper than actual calculations...
	if ( fourier->get_myrank() == MASTER ) {
		if ( fourier->read_reconxyz_from_apth5() == true ) {
			cout << "MASTER read successfully all ion coordinates" << "\n";
		}
		else {
			cerr << "MASTER was unable to read ion coordinates !" << "\n"; localhealth = 0;
		}
		if ( fourier->read_ranging_from_apth5() == true ) {
			cout << "MASTER read successfully ranging information" << "\n";
		}
		else {
			cerr << "MASTER was unable to read ranging information !" << "\n"; localhealth = 0;
		}
		if ( fourier->read_itypes_from_apth5() == true ) {
			cout << "MASTER read successfully iontypes" << "\n";
		}
		else {
			cerr << "MASTER was unable to read iontypes !" << "\n"; localhealth = 0;
		}
		if ( ConfigFourier::SamplingGridRemoveBndPoints == true ) {

			if ( fourier->read_trianglehull_from_apth5() == true ) {
				cout << "MASTER read successfully all triangles of the dataset boundary" << "\n";
			}
			else {
				cerr << "MASTER was unable to read the triangles of the dataset boundary!" << "\n"; localhealth = 0;
			}
			if ( fourier->read_voxelized_from_apth5() == true ) {
				cout << "MASTER read successfully a previous voxelization of the dataset" << "\n";
			}
			else {
				cerr << "MASTER was unable to read a previous voxelization of the dataset!" << "\n"; localhealth = 0;
			}
		}
		else {
			cerr << "WARNING SamplingGridRemoveBndPoints is currently not implemented!" << "\n";
		}
	}
	//else slaves processes wait
	MPI_Barrier( MPI_COMM_WORLD );
	
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that all data have arrived!" << "\n";
		delete fourier; fourier = NULL; return;
	}
	
	if ( fourier->broadcast_reconxyz() == true ) {
		cout << "Rank " << r << " has synchronized reconxyz" << "\n";
	}
	else {
		cerr << "Rank " << r << " failed to synchronize reconxyz!" << "\n"; localhealth = 0;
	}
	if ( fourier->broadcast_ranging() == true ) {
		cout << "Rank " << r << " has synchronized ranging" << "\n";
	}
	else {
		cerr << "Rank " << r << " failed to synchronize ranging" << "\n"; localhealth = 0;
	}
	if ( ConfigFourier::SamplingGridRemoveBndPoints == true ) {

		if ( fourier->broadcast_triangles() == true ) {
			cout << "Rank " << r << " has synchronized trianglehulls" << "\n";
		}
		else {
			cerr << "Rank " << r << " failed to synchronized trianglehulls" << "\n"; localhealth = 0;
		}
		if ( fourier->broadcast_vxlized() == true ) {
			cout << "Rank " << r << " has synchronized voxelization" << "\n";
		}
		else {
			cerr << "Rank " << r << " failed to synchronized voxelization" << "\n"; localhealth = 0;
		}
	}

	if ( fourier->broadcast_distances() == true ) {
		cout << "Rank " << r << " has synchronized distances" << "\n";
	}
	else {
		cerr << "Rank " << r << " failed to synchronize distances!" << "\n"; localhealth = 0;
	}

	/*
	//##MK::for now we do not account for ion types but paraprobe-utils defines methods for p3dm1 type only so we need to
	//replace at least for now and p3d type xyz with p3dm1 type xyz array
	try {
		fourier->xyz_ityp.reserve( fourier->xyz.size() );
		for( auto it = fourier->xyz.begin(); it != fourier->xyz.end(); it++ ) {
			fourier->xyz_ityp.push_back( p3dm1( it->x, it->y, it->z, UNKNOWN_IONTYPE ) );
		}
		fourier->xyz = vector<p3d>();
		cout << "Rank " << r << " xyz_ityp.size() " << fourier->xyz_ityp.size() << "\n";
	}
	catch (bad_alloc &mecroak) {
		cerr << "Temporary replacement of xyz by xyz_ityp had allocation error!" << "\n"; localhealth = 0;
	}
	//##MK::now we do account for ion types!
	*/

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes have synchronized the reconstruction and triangle hull!" << "\n";
		delete fourier; fourier = NULL; return;
	}
	
	MPI_Barrier( MPI_COMM_WORLD );

	//now processes can proceed and process massively parallel and independently
	//MK::each process computes the same grid and sets the same deterministic a priori known work partitioning for now
	if ( fourier->define_phase_candidates() == true ) {
		cout << "Rank " << r << " has defined the analysis tasks successfully" << "\n";
	}
	else {
		cout << "Rank " << r << " detected inconsistencies during defining analysis tasks!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes have synchronized the analysis tasks!" << "\n";
		delete fourier; fourier = NULL; return;
	}

	//fourier->spatial_decomposition(); //##MK::old version
	fourier->itype_sensitive_spatial_decomposition();

	fourier->define_matpoint_volume_grid();
	
	//fourier->distribute_matpoints_on_processes(); //##MK::old version
	fourier->distribute_matpoints_on_processes_roundrobin();
	
	//we need to create the results file as so many data might be stored that we have to dump some intermediatedly
	if ( fourier->init_target_file2() == true ) {
		cout << "Rank " << r << " successfully initialized APTH5 results file" << "\n";
	}
	else {
		cerr << "Rank " << r << " unable to initialize results APTH5 file" << "\n"; localhealth = 0;
	}
	if ( r == MASTER ) {
		if ( fourier->write_environment_and_settings() == true ) {
			cout << "Rank " << MASTER << " environment and settings written!" << "\n";
		}
		else {
			cerr << "Rank " << MASTER << " writing environment and settings failed!" << "\n"; localhealth = 0;
		}
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes have synchronized after generation of the target file!" << "\n";
		delete fourier; fourier = NULL; return;
	}

	//fourier->execute_local_workpackage1(); //##MK::old version, is not iontype-specific and only a proof of concept
	fourier->execute_local_workpackage2(); //new version is iontype-specific
	
	cout << "Rank " << r << " has completed its workpackage" << "\n";
	MPI_Barrier(MPI_COMM_WORLD);
	
	ttic = MPI_Wtime();

	if ( r == MASTER ) {
		if ( fourier->write_materialpoints2_to_apth5() == true ) { //does not require MPI communication all results already on the master
			cout << "Rank " << MASTER << " successfully wrote all materialpoints to an APTH5 file" << "\n";
		}
		else {
			cerr << "Rank " << MASTER << " unable to write all materialpoints to the APTH5 file" << "\n"; localhealth = 0;
		}
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that MASTER was not successful in writing material points to the APTH5 file" << "\n";
		delete fourier; fourier = NULL; return;
	}

	//required because phase-specific result groups need to exist before the processes can fill them with data!
	MPI_Barrier( MPI_COMM_WORLD );

	//##MK::naive sequential writing of results
	for( int rk = MASTER; rk < nr; rk++ ) {
		if ( rk == r ) {
			if ( fourier->write_results2_to_apth5() == true ) {
				cout << "Rank " << rk << " successfully wrote all materialpoint-related results to H5 file" << "\n";
			}
			else {
				cerr << "Rank " << rk << " encountered errors when attempting to write all materialpoint-related results!" << "\n"; localhealth = 0;
			}
		}
		MPI_Barrier( MPI_COMM_WORLD );
	}

	//check if all process have successfully been able to write their results to the HDF5 file
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes were successful in writing all their material points with results to the H5 file" << "\n";
		delete fourier; fourier = NULL; return;
	}

	//######################MK::write mat points for indexing

/*
	if ( fourier->get_myrank() == MASTER ) {
		if ( fourier->init_target_file() == true ) {
			cout << "Rank MASTER successfully initialized APTH5 results file" << "\n";
		}
		else {
			cerr << "Rank MASTER unable to initialize results APTH5 file" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes have synchronized after generation of the target file!" << "\n";
		delete fourier; fourier = NULL;
		return;
	}

	//##MK::currently, in this proof-of-concept we just report the extremum value positions of results in Fourier space
	//this is small enough to collect on the master process and do sequential I/O
	//##MK::for really storing all Fourier grid results for every material point, which are crazy many, take e.g. 100^3 cube
	//already 4MB per material point, so if 10 million material points, 40 TB!
	//##MK::in such future use case we need to employ the parallel HDF5 library and let the process write the results in parallel
	ttoc = MPI_Wtime();
	mm = memsnapshot();
	fourier->fourier_tictoc.prof_elpsdtime_and_mem( "InitializeAPTH5ResultsFile", APT_XX, APT_IS_SEQ, mm, ttic, ttoc);

	MPI_Barrier( MPI_COMM_WORLD ); //##MK::might be reduced to a single one see above barrier...

	if ( fourier->collect_results_on_masterprocess() == true ) {
		if ( r == MASTER ) {
			cout << "Rank MASTER successfully collected all proof-of-concept results on the MASTER MPI process" << "\n";
		}
	}
	else {
		cerr << "Rank " << r << " unable to collect proof-of-concept results on MASTER MPI process!" << "\n";
		delete fourier; return;
	}

	if ( fourier->write_materialpoints_to_apth5() == true ) { //does not require MPI communication all results already on the master
		if ( r == MASTER ) {
			cout << "Rank MASTER successfully wrote all materialpoints to an APTH5 file" << "\n";
		}
	}
	else {
		cerr << "Unable to write all materialpoints to the APTH5 file" << "\n";
		delete fourier; fourier = NULL; return;
	}

	if ( fourier->write_results_to_apth5() == true ) {
		if ( r == MASTER ) {
			cout << "Rank MASTER successfully wrote all materialpoint-related results to an APTH5 file" << "\n";
		}
	}
	else {
		cerr << "Rank " << r << " unable to write all materialpoint-related reults to the APTH5 file" << "\n";
		delete fourier; fourier = NULL; return;
	}
*/

	fourier->fourier_tictoc.spit_profiling( "Fourier", ConfigShared::SimID, fourier->get_myrank() );

	//release resources
	delete fourier; fourier = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
	hello();

//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	//helloworld( argc, argv );

	if ( init( argc, argv ) == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks!" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else { 
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}
	cout << r << "-th MPI process initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";
	
//for this MPI/OMP/OpenACC, i.e. process/thread/accelerator heterogeneous program not every combination of mpiexec -n, OMP_NUM_THREADS and GPUs is admissible
	//##MK::if we dont want to use GPUs at all possible
	//##MK::current implementation initiates one MPI process per GPU on the node
	//##MK::problem is that the number of MPI processes per computing node is typically decided

	bool validsettings = true;
#ifdef UTILIZE_GPUS
	int devCount = acc_get_num_devices(acc_device_nvidia);
	if ( devCount < ConfigFourier::GPUsPerNode ) {
		cerr << "Rank " << r << " there are only " << devCount << " GPUs on the node but user wanted " << ConfigFourier::GPUsPerNode << "\n";
		validsettings = false;
	}
	else { //devCount >= GPUsPerNode
		//number of processes needs to be integer dividable through number of gpus
		if ( ConfigFourier::GPUsPerNode > 0 ) {
			//MPI_Get_processor name algorithm to check on how many disjoint nodes the program instances are distributed
			//if nr > ConfigFourier::GPUsPerNode*NumberOfNodesInUse == validsettings = false;
			if ( (nr % ConfigFourier::GPUsPerNode) == 0 ) {
				//MK::e.g. 1 rank but 2 gpus: forbidden, 3 ranks but 2 gpus: forbidden, 4 ranks 2 gpus: allowed
				//##MK::2 processes per node, each using one gpu, 2 two such CPU/GPU pairs across the team of two nodes used
				//requires batch queue system variables like slurm ntasks-per-node=2 to be set properly
				cout << "GPUs will be utilized and number of MPI processes is an integer multiple of amount of GPUs planned" << "\n";
			}
			else {
				cerr << "GPUs should be used but number of MPI processes is not an integer multiple of GPUsPerNode!" << "\n";
				validsettings = false;
			}
		}
		//else{} GPUs exist but should not be used, setting remain valid will use sequential fallback!
	}
#endif

	if ( validsettings == true ) {
//EXECUTE SPECIFIC TASK
		many_local_direct_fourier_transforms_on_reconstruction( r, nr, argv );
	}
	
//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();

	double toc = omp_get_wtime();
	cout << "paraprobe-fourier took " << (toc-tic) << " seconds wall-clock time in total" << endl;
	return 0;
}
