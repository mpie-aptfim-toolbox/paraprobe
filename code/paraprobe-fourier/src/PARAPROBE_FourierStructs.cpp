//##MK::GPLV3

#include "PARAPROBE_FourierStructs.h"

ostream& operator << (ostream& in, iontype_ival const & val) {
	in << "[ " << val.incl_left << ", " << val.excl_right << ") m = " << val.m << "\n";
	return in;
}


/*
ostream& operator << (ostream& in, binning_info const & val) {
	in << "ROIRadius " << val.R << "\n";
	in << "Histogram dR " << val.dR << "\n";
	in << "Histogram mult " << val.binner << "\n";
	in << "Histogram RdR " << val.RdR << "\n";
	in << "Histogram pad " << val.Rpadding << "\n";
	in << "Histogram nbins " << val.NumberOfBins << "\n";
	in << "FFT nbins half " << val.NumberOfBinsHalf << "\n";
	return in;
}


ostream& operator << (ostream& in, threepeaks const & val) {
	in << val.max1.first << ";" << val.max1.second << "\n";
	in << val.max2.first << ";" << val.max2.second << "\n";
	in << val.max3.first << ";" << val.max3.second << "\n";
	return in;
}
*/
