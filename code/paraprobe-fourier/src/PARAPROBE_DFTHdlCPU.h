//##MK::GPLV3

#ifndef __PARAPROBE_FOURIER_DFTHDLCPU_H__
#define __PARAPROBE_FOURIER_DFTHDLCPU_H__

//#include "PARAPROBE_VolumeSampler.h"
#include "../../paraprobe-utils/src/PARAPROBE_VolumeSampler.h"
#include "PARAPROBE_FourierTicToc.h"

typedef float dft_real;
//#define SINGLEGPUWORKLOAD					10

#define NORESULTS_FOR_THIS_MATERIALPOINT	INT32MX
#define WAIT_UNTIL_GLOBAL_EXTREMUM_FOUND	INT32MX

struct hklval
{
	int mpid;
	int ijk;
	dft_real val;
	hklval() : mpid(NORESULTS_FOR_THIS_MATERIALPOINT), ijk(INT32MX), val(0.0) {}
	hklval( const int _mpid, const int _ijk, const dft_real _val ) :
		mpid(_mpid), ijk(_ijk), val(_val) {}
};

ostream& operator << (ostream& in, hklval const & val);



struct hklspace_res
{
	dft_real h;
	dft_real k;
	dft_real l;
	dft_real SQRIntensity;
	hklspace_res() : h(0.0), k(0.0), l(0.0), SQRIntensity(0.0) {}
	hklspace_res( const dft_real _h, const dft_real _k, const dft_real _l, const dft_real _sqr ) :
		h(_h), k(_k), l(_l), SQRIntensity(_sqr) {}
};

ostream& operator << (ostream& in, hklspace_res const & val);


struct reflector
{
	double u;
	double v;
	double w;
	double SUMIntensity;
	double PksCnts;

	/*dft_real u;
	dft_real v;
	dft_real w;
	dft_real SUMIntensity;
	unsigned int PksCnts;*/
	reflector() : u(0.0), v(0.0), w(0.0), SUMIntensity(0.0), PksCnts(0) {}
	//reflector( const dft_real _u, const dft_real _v, const dft_real _w,
	//		const dft_real _sum, const unsigned int _cnt ) :
	reflector( const double _u, const double _v, const double _w,
				const double _sum, const unsigned int _cnt ) :
		u(_u), v(_v), w(_w), SUMIntensity(_sum), PksCnts(_cnt) {}
};

ostream& operator << (ostream& in, reflector const & val);



class dft_cpu
{
	//the class is a utility tool it does the actual direct Fourier transform on the CPU
public:
	dft_cpu();
	dft_cpu( const size_t n );
	~dft_cpu();
	
	hklval execute( vector<dft_real> const & p );
	void reset();
		
private:
	dft_real* Fmagn;				//magnitude of the Fourier space intensity
	dft_real* ival;					//Fourier space grid position 1d of a 3d cube hkl <=> ijk
	int NI;							//Fourier space number of voxel 1d
	int NIJ;
	int NIJK;	
};


#endif
