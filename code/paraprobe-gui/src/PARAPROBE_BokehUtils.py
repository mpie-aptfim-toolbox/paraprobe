import sys, glob
import numpy as np
#import param
from bokeh.io import output_file, show, curdoc
from bokeh.events import ButtonClick
from bokeh.models import Button, CustomJS, Slider, Select
from bokeh.plotting import figure, output_file, show, ColumnDataSource
from bokeh.layouts import row, column
from bokeh.models.widgets import Tabs, Panel, TextInput, FileInput, Paragraph, Dropdown, RadioButtonGroup

class paraprobe_utils():
    def __init__(self, simid):
        self.simid = simid
        self.hello_txt = Paragraph(text="""Hello, walk step-by-step through this wizard""")
        self.simid_txb = TextInput(value=str(simid), title="Define an integer to associate with the simulation (uint32)")
        self.simid_txt = Paragraph(text="""Define a simulation ID!""", width=300, height=50)
        self.simid_btn = Button(label='Register this integer')
        def simid_init_click( txt1 = self.simid_txt ):
            if np.uint32(self.simid_txb.value) >= 1000000:
                self.simid = 0
            else:
                self.simid = np.uint32(self.simid_txb.value)
            txt1.text = 'SimID.' + str(self.simid)
        self.simid_btn.on_click( simid_init_click )
        self.mode_dpd = Select(title="What do you plan to analyze?", value="Build synthetic specimen recon, and analyze", 
                               options=["Build synthetic specimen recon, and analyze", 
                                        "Use existing recon with POS and analyze",
                                        "Use EPOS, build recon, and analyze",
                                        "Use APT, build recon and analyze"])
        self.guide_txt = Paragraph(text="""Choose what you want to do and let me help you!""", width=300, height=50)
        self.usecase = 0
        def mode_choice_callback(attr, old, new):
            self.usecase = 0
            if self.mode_dpd.value == "Build synthetic specimen recon, and analyze":
                self.usecase = 0
            if self.mode_dpd.value == "Use existing recon with POS and analyze":
                self.usecase = 1
            if self.mode_dpd.value == "Use EPOS, build recon, and analyze":
                self.usecase = 2
            if self.mode_dpd.value == "Use APT, build recon and analyze":
                self.usecase = 3
        self.mode_dpd.on_change( 'value', mode_choice_callback )
        self.choose_btn = Button(label='Get guidance what to do next')
        def choose_click( txt1 = self.guide_txt ):
            if self.usecase == 0: #PARAPROBE.Synthetic.SimID.0.apth5
                txt1.text = "Load RRNG file, then go to Synthetic tab, then Ranger tab, then analyses tabs"
            if self.usecase == 1: #PARAPROBE.Transcoder.SimID.0.apth5
                txt1.text = "Load RRNG + POS files, then use Transcoder, then Ranger tab, then analyses tabs"
            if self.usecase == 2: #PARAPROBE.Transcoder.SimID.0.apth5
                txt1.text = "Load RRNG + EPOS files, then use Transcoder, reconstruct with Reconstruct tab, then Ranger tab, then analyses tabs"
            if self.usecase == 3: #PARAPROBE.Transcoder.SimID.0.apth5
                txt1.text = "Load RRNG + APT files, then use Transcoder, reconstruct with Reconstruct tab, then Ranger tab, then analyses tabs"
        self.choose_btn.on_click( choose_click )
        self.trans_fnm = ''
        self.recon_fnm = ''
        self.rrng_fnm = ''
        self.trans_txt = Paragraph(text="""""", width=300, height=30)
        self.recon_txt = Paragraph(text="""Specify external ions (*.pos, *.epos, *.apt (APSuite/IVAS4))""", width=300, height=30)
        self.recon_fibx = FileInput(accept=".pos,.epos,.apt")
        self.rrng_txt = Paragraph(text="""Specify external ranging (*.rrng)""", width=300, height=30)
        self.rrng_fibx = FileInput(accept=".rrng")
        self.utils_xml_btn = Button(label='Proceeded ...')
        def utils_change_click( txt1 = self.recon_txt, txt2 = self.trans_txt, txt3 = self.rrng_txt ):
            #register ranging data
            self.rrng_fnm = ''
            if self.rrng_fibx.filename.endswith('.rrng') or self.rrng_fibx.filename.endswith('.RRNG'):
                self.rrng_fnm = self.rrng_fibx.filename
            else:
                self.rrng_fnm = "ERROR.Choose.A.Valid.RRNG.File"
            txt3.text = self.rrng_fnm
            #register input and filenames
            self.trans_fnm = ''
            self.recon_fnm = ''
            if self.usecase == 0:
                self.trans_fnm = "Transcoder not needed!"
                self.recon_fnm = 'PARAPROBE.Synthetic.SimID.' + str(self.simid) + '.apth5'
            if self.usecase == 1:
                if self.recon_fibx.filename.endswith('.pos') or self.recon_fibx.filename.endswith('.POS'):
                    self.trans_fnm = self.recon_fibx.filename
                    self.recon_fnm = 'PARAPROBE.Transcoder.SimID.' + str(self.simid) + '.apth5'
                else:
                    self.trans_fnm = "ERROR choose a valid POS file!"
                    self.recon_fnm = "ERROR choose a valid POS file!"
            if self.usecase == 2:
                if self.recon_fibx.filename.endswith('.epos') or self.recon_fibx.filename.endswith('.EPOS'):
                    self.trans_fnm = self.recon_fibx.filename
                    self.recon_fnm = 'PARAPROBE.Transcoder.SimID.' + str(self.simid) + '.apth5'
                else:
                    self.trans_fnm = "ERROR choose a valid EPOS file!"
                    self.recon_fnm = "ERROR choose a valid EPOS file!"                    
            if self.usecase == 3:
                if self.recon_fibx.filename.endswith('.apt') or self.recon_fibx.filename.endswith('.APT'):
                    self.trans_fnm = self.recon_fibx.filename
                    self.recon_fnm = 'PARAPROBE.Transcoder.SimID.' + str(self.simid) + '.apth5'
                else:
                    self.trans_fnm = "ERROR choose a valid APT file!"
                    self.recon_fnm = "ERROR choose a valid APT file!"
            txt1.text = self.recon_fnm         
            txt2.text = self.trans_fnm
        self.utils_xml_btn.on_click( utils_change_click )
        self.layout = column(  self.hello_txt,
                               self.simid_txb, 
                               self.simid_btn, 
                               self.simid_txt,
                               self.mode_dpd,
                               self.choose_btn,
                               self.guide_txt,
                               self.trans_txt,
                               self.recon_txt,
                               self.recon_fibx,
                               self.rrng_txt,
                               self.rrng_fibx,
                               self.utils_xml_btn )

        
        # #self.test_txb = TextInput(value="test.pos",  title="Which input?")
        # #self.test_txt = Paragraph(text="""<nothing chosen>""", width=300, height=14)
        # #https://stackoverflow.com/questions/57383505/python-bokeh-fileinput-widget 
        # #https://stackoverflow.com/questions/46357598/bokeh-python-how-to-interact-with-radio-buttons to
        # #load which file was taken
        # self.input_dpd = Select(title="Use a measured or synthetic specimen?", value="Synthetic", options=["Synthetic", "Measured"])
        # self.inputmode = 0
        # def utils_mode_callback(attr, old, new):
            # if self.input_dpd.value == "Synthetic":
                # self.inputmode = 0
            # else:
                # self.inputmode = 1
        # self.input_dpd.on_change( 'value', utils_mode_callback )
        # self.inputmode_txt = Paragraph(text="""<unknown input mode>""", width=300, height=14)
        # self.utils_xml_btn = Button(label='Transcode/synthesize specimen')
        # def utils_change_click( txt1 = self.recon_txt, txt2 = self.rrng_txt, txt3 = self.inputmode_txt ):
            # if self.inputmode == 0:
                # self.recon_fnm = 'PARAPROBE.Synthetic.SimID.' + str(simid) + '.apth5'
                # txt3.text = 'Generating a synthetic specimen'
            # else:
                # self.recon_fnm = self.recon_fibx.filename
                # txt3.text = 'Working with a measured specimen'
            # self.rrng_fnm = self.rrng_fibx.filename
            # if self.recon_fibx.value.endswith('.pos'):
                # self.recon_typid = 0
            # if self.recon_fibx.value.endswith('.epos'):
                # self.recon_typid = 1
            # if self.recon_fibx.value.endswith('.aptv2'):
                # self.recon_typid = 2
            # txt1.text = self.recon_fnm
            # txt2.text = self.rrng_fnm
        # self.utils_xml_btn.on_click( utils_change_click )
        
							   
# 						   self.simid_txt, self.recon_txt, self.recon_fibx, 
#                             self.rrng_txt, self.rrng_fibx, 
#                             self.input_dpd, self.inputmode_txt, 
#                             self.utils_xml_btn ) #self.test_txb, self.test_txt, self.utils_xml_btn )
