import sys, glob
import numpy as np
import os
#http://vipulchaskar.blogspot.com/2012/10/exploiting-eval-function-in-python.html
from bokeh.io import output_file, show, curdoc
from bokeh.events import ButtonClick
from bokeh.models import Button, CustomJS, Slider, Select
from bokeh.plotting import figure, output_file, ColumnDataSource
from bokeh.layouts import row, column
from bokeh.models.widgets import Tabs, Panel, FileInput, Paragraph, RadioButtonGroup, Dropdown, CheckboxGroup, TextInput #, DataRangeSlider

#https://github.com/bokeh/bokeh/issues/6219

class paraprobe_submit():
    
    def __init__(self, ifo):
        self.simid_txt = Paragraph(text="""SimulationID <unknown>""", width=300, height=14)
		###MK::add only those runs which were successfully calibrated and have valid XML files!
        #self.recon_txt = Paragraph(text="""Work with reconstruction <none>""", width=300, height=14)
        #self.hull_txt = Paragraph(text="""Read surface hull from <none>""", width=300, height=14)
        self.flw_pass_btn = Button(label='Read in previous data')
        def flw_pass_on_click( txt1 = self.simid_txt ): #, txt2 = self.recon_txt, txt3 = self.hull_txt ):
            #self.recon_fnm = ifo.recon_fnm
            #self.hull_fnm = 'PARAPROBE.Surfacer.Results.SimID.' + str(ifo.simid) + '.h5'
            txt1.text = str(ifo.simid)
            #txt2.text = ifo.recon_fnm
            #txt3.text = self.hull_fnm            
        self.flw_pass_btn.on_click( flw_pass_on_click )
        #self.pse_txb = TextInput(value="PARAPROBE.PeriodicTableOfElements.xml", title="XML file with the table of elements")
        self.what_txt = Paragraph(text="""Which tools to run?""", width=300, height=14)
        self.what_cbx = CheckboxGroup(labels=['paraprobe-transcoder', 'paraprobe-reconstruct', 'paraprobe-ranger', 'paraprobe-surfacer', 'paraprobe-spatstat', 'paraprobe-tessellator'], active=[0,1,2,3,4,5])
        self.do_trans = 0
        self.do_recon = 0
        self.do_range = 0
        self.do_surf = 0
        self.do_spat = 0
        self.do_tess = 0
        self.machine_dpd = Select(title="Which machine and type of script?", value="MAWS30, bash", options=["MAWS30, bash", "TALOS, slurm"])
        self.machine = 0        
        def machine_mode_callback(attr, old, new):
            if self.machine_dpd.value == "MAWS30, bash":
                self.machine = 0 #maws30, bash
            if self.machine_dpd.value == "TALOS, slurm":
                self.machine = 1
        self.machine_dpd.on_change( 'value', machine_mode_callback )
        self.nnodes_sl = Slider(start=1, end=80, value=1, step=1, title="How many nodes to use?")
        self.nprocs_pernode_sl = Slider(start=1, end=2, value=1, step=1, title="How many MPI processes per node?")
        self.nthreads_perproc_sl = Slider(start=1, end=40, value=1, step=1, title="How many OpenMP threads per MPI process?")
        self.submit_flw_btn = Button(label='Submit / write *.sh file')
        self.configtxt = Paragraph(text="""<config>""", width=400, height=300)
        def flw_write_on_click( txt1 = self.configtxt ):
            file_name = 'PARAPROBE.Wizard.MAWS30.Run.SimID.' + str(ifo.simid) + '.sh'
            flw = ''
            if self.machine == 0:
                flw += '#!/bin/bash' + '\n'
                flw += '## !! this section needs to be modified typically to reflect where paraprobe and libraries are on system !!' + '\n'
                flw += '## !! this is an example when using Linux environment modules and MPIE MAWS30 workstation !!' + '\n'                
                flw += 'module purge' + '\n'
                flw += 'module load Compilers/Intel/Intel2018' + '\n'
                flw += 'module load MPI/Intel/IntelMPI2018' + '\n'
                flw += 'module load Libraries/IntelMKL2018' + '\n'
                flw += 'module list' + '\n'
                flw += 'export OMP_NUM_THREADS=' + str(int(self.nthreads_perproc_sl.value)) + '\n'
                flw += 'export OMP_PLACES=cores' + '\n'
                flw += 'echo "OpenMP NUM THREADS env option"' + '\n'
                flw += 'echo $OMP_NUM_THREADS' + '\n'
                flw += 'echo "OpenMP PLACES env option"' + '\n'
                flw += 'echo $OMP_PLACES' + '\n'
                flw += 'ulimit -s unlimited' + '\n'
                flw += 'echo "ulimit env option"' + '\n'
                flw += 'ulimit' + '\n'
                flw += 'echo "Running on MAWS30"' + '\n'
                #flw += 'export I_MPI_SHM_LMT=shm' + '\n'  #to be used for instance for some desktop machine with front-end gui
                #flw += 'echo $I_MPI_SHM_LMT' + '\n'
                flw += '## !! the rest does typically need no modification' + '\n'
                flw += '' + '\n'
            if self.machine == 1:
                flw += '#!/bin/bash -l' + '\n'
                #flw += '## !! this section needs to be modified typically to reflect where paraprobe and libraries are on system !!' + '\n'
                #flw += '## !! this is an example when using Linux environment modules and FHI/MPIP/MPIE TALOS workstation !!' + '\n'                
                flw += '#SBATCH -o ./PARAPROBE.Wizard.TALOS.Run.SimID.' + str(ifo.simid) + '.STDOUT.%j' + '\n'
                flw += '#SBATCH -e ./PARAPROBE.Wizard.TALOS.Run.SimID.' + str(ifo.simid) + '.STDERR.%j' + '\n'
                flw += '#SBATCH -D ./' + '\n'
                flw += '#SBATCH -J paraprobe-wizard' + '\n'
                flw += '#SBATCH --partition=p.talos' + '\n'
                flw += '#SBATCH --nodes=' + str(int(self.nnodes_sl.value)) + '\n'
                flw += '#SBATCH --ntasks-per-node='+ str(int(self.nprocs_pernode_sl.value)) + '\n'
                flw += '#SBATCH --cpus-per-task=' + str(int(self.nthreads_perproc_sl.value)) + '\n'
                flw += '#SBATCH --mail-user=m.kuehbach@mpie.de' + '\n'
                flw += '#SBATCH 95:55:00' + '\n'
                flw += '#submits to p.talos make jobs currently by default to run exclusively on resources' + '\n'
                flw += 'module load cmake' + '\n'
                flw += 'module load intel' + '\n'
                flw += 'module load impi' + '\n'
                flw += 'module load mkl' + '\n'
                flw += 'module load boost' + '\n'
                flw += 'module list' + '\n'
                flw += 'if [ ! -z $SLURM_CPUS_PER_TASK ] ; then' + '\n'
                flw += '	export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK' + '\n'
                flw += 'else' + '\n'
                flw += '	export OMP_NUM_THREADS=1' + '\n'
                flw += 'fi' + '\n'
                flw += 'export OMP_PLACES=cores' + '\n'
                flw += 'echo "OpenMP NUM THREADS env option"' + '\n'
                flw += 'echo $OMP_NUM_THREADS' + '\n'
                flw += 'echo "OpenMP PLACES env option"' + '\n'
                flw += 'echo $OMP_PLACES' + '\n'
                flw += 'ulimit -s unlimited' + '\n'
                flw += 'echo "ulimit env option"' + '\n'
                flw += 'ulimit' + '\n'
                flw += 'echo $SLURM_JOB_NODELIST' + '\n'
                flw += 'echo $SLURM_JOB_NUM_NODES' + '\n'
                flw += 'echo "job $SLURM_JOB_NAME with job id $SLURM_JOB_ID is running on $SLURM_JOB_NUM_NODES node(s): $SLURM_JOB_NODELIST"' + '\n'
                flw += '## !! the rest does typically need no modification' + '\n'
            flw += '' + '\n'
            flw += '#this is the workflow' + '\n'
            #activate and loop in things that users wishes
            what_to_do = self.what_cbx.active
            if 0 in what_to_do:
                self.do_trans = 1
                if self.machine == 0:
                    flw += 'mpiexec -n ' + str(int(self.nprocs_pernode_sl.value)) + ' ./paraprobe-transcoder ' + str(ifo.simid) + ' PARAPROBE.Transcoder.SimID.' + str(ifo.simid) + '.xml' \
                                        ' 1>PARAPROBE.Transcoder.SimID.' + str(ifo.simid) + '.STDOUT.txt 2>PARAPROBE.Transcoder.SimID.' + str(ifo.simid) + '.STDERR.txt' + '\n'
                if self.machine == 1:
                    flw += 'srun paraprobe-transcoder ' + str(ifo.simid) + ' PARAPROBE.Transcoder.SimID.' + str(ifo.simid) + '.xml' + '\n'
            else:
                self.do_trans = 0
            if 1 in what_to_do:
                self.do_recon = 1
                if self.machine == 0:
                    flw += 'mpiexec -n ' + str(int(self.nprocs_pernode_sl.value)) + ' ./paraprobe-reconstruct ' + str(ifo.simid) + ' PARAPROBE.Reconstruct.SimID.' + str(ifo.simid) + '.xml' \
                                        ' 1>PARAPROBE.Reconstruct.SimID.' + str(ifo.simid) + '.STDOUT.txt 2>PARAPROBE.Reconstruct.SimID.' + str(ifo.simid) + '.STDERR.txt' + '\n'
                if self.machine == 1:
                    flw += 'srun paraprobe-reconstruct ' + str(ifo.simid) + ' PARAPROBE.Reconstruct.SimID.' + str(ifo.simid) + '.xml' + '\n'     
            else:
                self.do_recon = 0
            if 2 in what_to_do:
                self.do_range = 1
                if self.machine == 0:
                    flw += 'mpiexec -n ' + str(int(self.nprocs_pernode_sl.value)) + ' ./paraprobe-ranger ' + str(ifo.simid) + ' PARAPROBE.Ranger.SimID.' + str(ifo.simid) + '.xml' \
                                        ' 1>PARAPROBE.Ranger.SimID.' + str(ifo.simid) + '.STDOUT.txt 2>PARAPROBE.Ranger.SimID.' + str(ifo.simid) + '.STDERR.txt' + '\n'
                if self.machine == 1:
                    flw += 'srun paraprobe-ranger ' + str(ifo.simid) + ' PARAPROBE.Ranger.SimID.' + str(ifo.simid) + '.xml' + '\n'                
            else:
                self.do_range = 0
            if 3 in what_to_do:
                self.do_surf = 1
                if self.machine == 0:
                    flw += 'mpiexec -n ' + str(int(self.nprocs_pernode_sl.value)) + ' ./paraprobe-surfacer ' + str(ifo.simid) + ' PARAPROBE.Surfacer.SimID.' + str(ifo.simid) + '.xml' \
                                        ' 1>PARAPROBE.Surfacer.SimID.' + str(ifo.simid) + '.STDOUT.txt 2>PARAPROBE.Surfacer.SimID.' + str(ifo.simid) + '.STDERR.txt' + '\n'
                if self.machine == 1:
                    flw += 'srun paraprobe-surfacer ' + str(ifo.simid) + ' PARAPROBE.Surfacer.SimID.' + str(ifo.simid) + '.xml' + '\n'                   
            else:
                self.do_surf = 0
            if 4 in what_to_do:
                self.do_spat = 1
                if self.machine == 0:
                    flw += 'mpiexec -n ' + str(int(self.nprocs_pernode_sl.value)) + ' ./paraprobe-spatstat ' + str(ifo.simid) + ' PARAPROBE.Spatstat.SimID.' + str(ifo.simid) + '.xml' \
                                        ' 1>PARAPROBE.Spatstat.SimID.' + str(ifo.simid) + '.STDOUT.txt 2>PARAPROBE.Spatstat.SimID.' + str(ifo.simid) + '.STDERR.txt' + '\n'
                if self.machine == 1:
                    flw += 'srun paraprobe-spatstat ' + str(ifo.simid) + ' PARAPROBE.Spatstat.SimID.' + str(ifo.simid) + '.xml' + '\n'     
            else:
                self.do_spat = 0
            if 5 in what_to_do:
                self.do_tess = 1
                if self.machine == 0:
                    flw += 'mpiexec -n ' + str(int(self.nprocs_pernode_sl.value)) + ' ./paraprobe-tessellator ' + str(ifo.simid) + ' PARAPROBE.Tessellator.SimID.' + str(ifo.simid) + '.xml' \
                                        ' 1>PARAPROBE.Tessellator.SimID.' + str(ifo.simid) + '.STDOUT.txt 2>PARAPROBE.Tessellator.SimID.' + str(ifo.simid) + '.STDERR.txt' + '\n'
                if self.machine == 1:
                    flw += 'srun paraprobe-tessellator ' + str(ifo.simid) + ' PARAPROBE.Tessellator.SimID.' + str(ifo.simid) + '.xml' + '\n'     
            else:
                self.do_tess = 0
            ###MK::add add araullo, fourier, indexer
            #file_name = 'PARAPROBE.Spatstat.SimID.' + self.simid_txt.text + '.xml'
            flw += '\n'
            txt1.text = flw
            with open(file_name,'w') as f:
                f.write(flw)            
        self.submit_flw_btn.on_click( flw_write_on_click )
        self.test_delay_sl = Slider(start=1, end=10, value=1, step=1, title="How much delay to test for job submission?")
        self.submit_slm_btn = Button(label='Submit Slurm job')
        self.statustxt = Paragraph(text="""<Status>""", width=400, height=300)
        def slm_submit_on_click( txt1 = self.statustxt ):
            #https://janakiev.com/blog/python-shell-commands/
            #https://discourse.bokeh.org/t/change-the-button-color-and-height/2746/3
            #log = ''
            #log += eval("os.getcwd()")
            #log += eval("./test.sh 10")
            #log += 'Job submitted to cluster'
            #txt1.text = eval("./test.sh 10") #log #eval("os.getcwd()")
            cmd = './test.sh' + ' ' + str(int(self.test_delay_sl.value))
            stream = os.popen(cmd) #'./test.sh 3')
            output = stream.read()
            txt1.text = output
            #eval("cd MPIE")
            #eval("os.getcwd()")        
        self.submit_slm_btn.on_click( slm_submit_on_click )        
        self.layout = column( self.simid_txt, self.flw_pass_btn,
                              self.what_txt,
                              self.what_cbx,
                              self.machine_dpd,
                              self.nnodes_sl,
                              self.nprocs_pernode_sl,
                              self.nthreads_perproc_sl,
                              self.submit_flw_btn,
                              self.test_delay_sl,
                              self.submit_slm_btn,
                              self.statustxt,
                              self.configtxt )
