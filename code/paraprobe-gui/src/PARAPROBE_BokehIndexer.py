import sys, glob
import numpy as np
from bokeh.io import output_file, show, curdoc
from bokeh.events import ButtonClick
from bokeh.models import Button, CustomJS, Slider, Select
from bokeh.plotting import figure, output_file, ColumnDataSource
from bokeh.layouts import row, column
from bokeh.models.widgets import Tabs, Panel, FileInput, Paragraph, RadioButtonGroup, Dropdown, CheckboxGroup, TextInput #, DataRangeSlider

class paraprobe_indexer():
       
    def __init__(self, ifo):
        self.simid_txt = Paragraph(text="""SimulationID <unknown>""", width=300, height=14)
        self.araullo_txb = TextInput(value='PARAPROBE.Araullo.Results.SimID.' + str(ifo.simid) + '.h5', title="Which Araullo-Peters results to index?")
        self.so3grid_txb = TextInput(value="PARAPROBE.Indexer.CubicDeg1SO3Grid.h5", title="Which SO3 orientations (cubic) as candidates?")
        self.idx_pass_btn = Button(label='Read in previous data')
        def idx_pass_on_click( txt1 = self.simid_txt ):
            txt1.text = str(ifo.simid)           
        self.idx_pass_btn.on_click( idx_pass_on_click )
        self.refadd_txb = TextInput(value='<entry refname="PARAPROBE.Araullo.Results.SimID.210001.h5" imagename="/AraulloPeters/Results/SpecificPeak/PH0/0" usetoindex="PH0" refori="8.0;8.0;8.0" targets="Al"/>', title="Which reference to take?")      
        self.refs_txt = Paragraph(text="""References <none>""", width=500, height=200)
        self.refs_lst = []
        self.refadd_btn = Button(label='Add to references')
        def idx_ref_on_click( txt1 = self.refs_txt ):
            self.refs_lst.append( self.refadd_txb.value )
            txt1.text = ''
            for ky in self.refs_lst:
                txt1.text = ky + '\n'
        self.refadd_btn.on_click( idx_ref_on_click )
        self.refkill_btn = Button(label='Clear references')
        def idx_kill_on_click( txt1 = self.refs_txt ):
            txt1.text = ''
            self.refadd_txb.value = '<entry refname="PARAPROBE.Araullo.Results.SimID.210001.h5" imagename="/AraulloPeters/Results/SpecificPeak/PH0/0" usetoindex="PH0" refori="8.0;8.0;8.0" targets="Al"/>'
        self.refkill_btn.on_click( idx_kill_on_click )  
        self.analysismode = 3
        self.idx_mode_dpd = Select(title="Analysis mode", value="Index Araullo-Peters results", options=["Index Araullo-Peters results"])
        def idx_mode_callback(attr, old, new):
            self.analysismode = 3
        self.idx_mode_dpd.on_change( 'value', idx_mode_callback )
        self.npeaks_txb = TextInput(value="1000", title="How many nodes to evaluate per ROI?")
        self.nsol_sl = Slider(start=1, end=100, value=1, step=1, title="How many of the best solutions to report?")
        self.iosol_qual = 1
        self.iosol_disori = 0
        self.iodisori_mtx = 0
        self.iosymm_red = 0
        self.iosymm_dis = 0.5
        self.maxmem_sl = Slider(start=2, end=256, value=8, step=2, title="Cache memory (GB)") #per node or process?
        self.proc_per_node = 1
        self.gpus_per_node = 0
        self.gpu_wkload = 1
        self.idx_xml_btn = Button(label='Write *.xml config file')
        self.configtxt = Paragraph(text="""<config>""", width=400, height=200)
        def idx_write_on_click( txt1 = self.configtxt ):
            keyval = {  "InputfileAraullo": self.araullo_txb.value,
                        "InputfileTestOris": self.so3grid_txb.value,
                        "AnalysisMode": self.analysismode,
                        "IndexSoManyHighestPeaks": self.npeaks_txb.value,
                        "IOUpToKthClosest": self.nsol_sl.value,
                        "IOSolutionQuality": self.iosol_qual,
                        "IOSolutionDisori": self.iosol_disori,
                        "IODisoriMatrices": self.iodisori_mtx,
                        "IOSymmReduction": self.iosymm_red,
                        "IOSymmRedDisoriAngle": self.iosymm_dis,
                        "CachePerIndexingEpoch": self.maxmem_sl.value,
                        "GPUsPerNode": self.gpus_per_node,
                        "GPUWorkload": self.gpu_wkload  }
            toolname = 'ConfigIndexer'
            xml = '<?xml version="1.0" encoding="utf-8"?>' + '\n'
            xml += '<' + toolname + '>' + '\n'
            for key in keyval:
                xml += '\t<' + key + '>' + str(keyval[key]) + '</' + key + '>' + '\n'
            xml += '<InputfileRefImages>' + '\n'
            xml += self.refs_txt.text + '\n'
            xml += '</InputfileRefImages>' + '\n'
            xml += '</' + toolname + '>' + '\n'
            txt1.text = xml
            file_name = 'PARAPROBE.Indexer.SimID.' + self.simid_txt.text + '.xml'
            with open(file_name,'w') as f:
                f.write(xml)
        self.idx_xml_btn.on_click( idx_write_on_click )        
        self.layout = column( self.idx_pass_btn, self.simid_txt,
                              self.araullo_txb, self.so3grid_txb, self.refadd_txb, self.refadd_btn, 
                              self.refkill_btn, self.refs_txt, self.idx_mode_dpd, self.npeaks_txb, 
                              self.nsol_sl, self.maxmem_sl, self.idx_xml_btn, self.configtxt )
