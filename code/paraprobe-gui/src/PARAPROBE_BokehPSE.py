import sys, glob
import numpy as np
from bokeh.io import output_file, show, curdoc
from bokeh.events import ButtonClick
from bokeh.models import Button, CustomJS, Slider, Select
from bokeh.plotting import figure, output_file, ColumnDataSource
from bokeh.layouts import row, column
from bokeh.models.widgets import Tabs, Panel, FileInput, Paragraph, RadioButtonGroup, Dropdown, CheckboxGroup, TextInput #, DataRangeSlider
import periodictable as pse

##def generate_source_code:
#src = ''
#Z = -1
#for el in pse.elements:
#    Z += 1 
#    if Z > 100:
#        Zstr = str(Z)
#    elif Z > 10:
#        Zstr = '0' + str(Z)
#    else:
#        Zstr = '00' + str(Z)
#    Sstr = str(el.symbol)
#    if len(Sstr) == 1:
#        Nstr = Sstr + ':'
#    else:
#        Nstr = Sstr
#    src += 'self.btn' + Zstr + ' = Button(label=' + '\'' + Sstr + '\'' + ', width=ld)' + '\n'
#    src += 'def btn' + Zstr + '( val = ' + '\'' + Nstr + '\'' + ', txt = self.configtxt ):' + '\n'
#    src += '    if txt.text == \'<config>\':' + '\n'
#    src += '        txt.text = val' + '\n'
#    src += '    else:' + '\n'
#    src += '        txt.text += ' + ' ; val' + '\n'
#    src += 'self.btn' + Zstr + '.on_click( btn' + Zstr + ')' + '\n'
#
##define the rows
#row4 = ''
#for k in range(19,36+1):
#    row4 += 'self.btn0' + str(k) + ', '
#row5 = ''
#for k in range(37,54+1):
#    row5 += 'self.btn0' + str(k) + ', '
#row6 = ''
#for k in range(72,86+1):
#    row6  += 'self.btn0' + str(k) + ', '
#row7 = ''
#for k in range(87,36+1):
#    row4 += 'self.btn0' + str(k) + ', '
  
    

class paraprobe_pse():
       
    def __init__(self):
        ld = 36
        self.helptxt = Paragraph(text="""This app exemplifies how to generate target/neighbor ion-type combinations to be used for paraprobe-spatstat""", width=900, height=20)
        self.configtxt = Paragraph(text="""<config>""", width=900, height=14)
        self.lan_txt = Paragraph(text="""Lanthanoids:""", width=3*ld, height=14)
        self.act_txt = Paragraph(text="""Actinoids:""", width=3*ld, height=14)
        self.btnkill = Button(label='Clear', width=100)
        def clear_on_click( txt = self.configtxt ):
            txt.text = '<config>'            
        self.btnkill.on_click( clear_on_click )
        self.pfig = figure(x_range=(0, 0), y_range=(1, 1), plot_width=ld, plot_height=ld, title='', tools='')
        #self.pfig.rect(0, 0, width=ld, height=ld, fill_color='white', line_color = 'black') #, source=source)
        self.btn001 = Button(label='H', width=ld)
        self.btn001a = Button(label=' ', width=ld)
        self.btn001b = Button(label=' ', width=ld)
        self.btn001c = Button(label=' ', width=ld)
        self.btn001d = Button(label=' ', width=ld)
        self.btn001e = Button(label=' ', width=ld)
        self.btn001f = Button(label=' ', width=ld)
        self.btn001g = Button(label=' ', width=ld)
        self.btn001h = Button(label=' ', width=ld)
        self.btn001i = Button(label=' ', width=ld)
        self.btn001j = Button(label=' ', width=ld)
        self.btn001k = Button(label=' ', width=ld)
        self.btn001l = Button(label=' ', width=ld)
        self.btn001m = Button(label=' ', width=ld)
        self.btn001n = Button(label=' ', width=ld)
        self.btn001o = Button(label=' ', width=ld)
        self.btn001p = Button(label=' ', width=ld)
        def btn001( val = 'H:', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn001.on_click( btn001 )
        self.btn002 = Button(label='He', width=ld)
        def btn002( val = 'He', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn002.on_click( btn002 )
        self.btn003 = Button(label='Li', width=ld)
        def btn003( val = 'Li', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn003.on_click( btn003 )
        self.btn004 = Button(label='Be', width=ld)
        def btn004( val = 'Be', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn004.on_click( btn004 )
        self.btn005 = Button(label='B', width=ld)        
        def btn005( val = 'B:', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn005.on_click( btn005 )
        self.btn006 = Button(label='C', width=ld)
        def btn006( val = 'C:', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn006.on_click( btn006 )
        self.btn007 = Button(label='N', width=ld)
        def btn007( val = 'N:', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn007.on_click( btn007 ) 
        self.btn008 = Button(label='O', width=ld)
        def btn008( val = 'O:', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn008.on_click( btn008 ) 
        self.btn009 = Button(label='F', width=ld)
        def btn009( val = 'F:', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn009.on_click( btn009 )
        self.btn010 = Button(label='Ne', width=ld)
        def btn010( val = 'Ne', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn010.on_click( btn010 )
        self.btn011 = Button(label='Na', width=ld)
        def btn011( val = 'Na', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn011.on_click( btn011)
        self.btn012 = Button(label='Mg', width=ld)
        def btn012( val = 'Mg', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn012.on_click( btn012)
        self.btn013 = Button(label='Al', width=ld)
        def btn013( val = 'Al', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn013.on_click( btn013)
        self.btn014 = Button(label='Si', width=ld)
        def btn014( val = 'Si', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn014.on_click( btn014)
        self.btn015 = Button(label='P', width=ld)
        def btn015( val = 'P:', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn015.on_click( btn015)
        self.btn016 = Button(label='S', width=ld)
        def btn016( val = 'S:', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn016.on_click( btn016)
        self.btn017 = Button(label='Cl', width=ld)
        def btn017( val = 'Cl', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn017.on_click( btn017)
        self.btn018 = Button(label='Ar', width=ld)
        def btn018( val = 'Ar', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn018.on_click( btn018)
        self.btn019 = Button(label='K', width=ld)
        def btn019( val = 'K:', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn019.on_click( btn019)
        self.btn020 = Button(label='Ca', width=ld)
        def btn020( val = 'Ca', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn020.on_click( btn020)
        self.btn021 = Button(label='Sc', width=ld)
        def btn021( val = 'Sc', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn021.on_click( btn021)
        self.btn022 = Button(label='Ti', width=ld)
        def btn022( val = 'Ti', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn022.on_click( btn022)
        self.btn023 = Button(label='V', width=ld)
        def btn023( val = 'V:', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn023.on_click( btn023)
        self.btn024 = Button(label='Cr', width=ld)
        def btn024( val = 'Cr', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn024.on_click( btn024)
        self.btn025 = Button(label='Mn', width=ld)
        def btn025( val = 'Mn', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn025.on_click( btn025)
        self.btn026 = Button(label='Fe', width=ld)
        def btn026( val = 'Fe', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn026.on_click( btn026)
        self.btn027 = Button(label='Co', width=ld)
        def btn027( val = 'Co', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn027.on_click( btn027)
        self.btn028 = Button(label='Ni', width=ld)
        def btn028( val = 'Ni', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn028.on_click( btn028)
        self.btn029 = Button(label='Cu', width=ld)
        def btn029( val = 'Cu', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn029.on_click( btn029)
        self.btn030 = Button(label='Zn', width=ld)
        def btn030( val = 'Zn', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn030.on_click( btn030)
        self.btn031 = Button(label='Ga', width=ld)
        def btn031( val = 'Ga', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn031.on_click( btn031)
        self.btn032 = Button(label='Ge', width=ld)
        def btn032( val = 'Ge', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn032.on_click( btn032)
        self.btn033 = Button(label='As', width=ld)
        def btn033( val = 'As', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn033.on_click( btn033)
        self.btn034 = Button(label='Se', width=ld)
        def btn034( val = 'Se', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn034.on_click( btn034)
        self.btn035 = Button(label='Br', width=ld)
        def btn035( val = 'Br', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn035.on_click( btn035)
        self.btn036 = Button(label='Kr', width=ld)
        def btn036( val = 'Kr', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn036.on_click( btn036)
        self.btn037 = Button(label='Rb', width=ld)
        def btn037( val = 'Rb', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn037.on_click( btn037)
        self.btn038 = Button(label='Sr', width=ld)
        def btn038( val = 'Sr', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn038.on_click( btn038)
        self.btn039 = Button(label='Y', width=ld)
        def btn039( val = 'Y:', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn039.on_click( btn039)
        self.btn040 = Button(label='Zr', width=ld)
        def btn040( val = 'Zr', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn040.on_click( btn040)
        self.btn041 = Button(label='Nb', width=ld)
        def btn041( val = 'Nb', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn041.on_click( btn041)
        self.btn042 = Button(label='Mo', width=ld)
        def btn042( val = 'Mo', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn042.on_click( btn042)
        self.btn043 = Button(label='Tc', width=ld)
        def btn043( val = 'Tc', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn043.on_click( btn043)
        self.btn044 = Button(label='Ru', width=ld)
        def btn044( val = 'Ru', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn044.on_click( btn044)
        self.btn045 = Button(label='Rh', width=ld)
        def btn045( val = 'Rh', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn045.on_click( btn045)
        self.btn046 = Button(label='Pd', width=ld)
        def btn046( val = 'Pd', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn046.on_click( btn046)
        self.btn047 = Button(label='Ag', width=ld)
        def btn047( val = 'Ag', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn047.on_click( btn047)
        self.btn048 = Button(label='Cd', width=ld)
        def btn048( val = 'Cd', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn048.on_click( btn048)
        self.btn049 = Button(label='In', width=ld)
        def btn049( val = 'In', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn049.on_click( btn049)
        self.btn050 = Button(label='Sn', width=ld)
        def btn050( val = 'Sn', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn050.on_click( btn050)
        self.btn051 = Button(label='Sb', width=ld)
        def btn051( val = 'Sb', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn051.on_click( btn051)
        self.btn052 = Button(label='Te', width=ld)
        def btn052( val = 'Te', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn052.on_click( btn052)
        self.btn053 = Button(label='I', width=ld)
        def btn053( val = 'I:', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn053.on_click( btn053)
        self.btn054 = Button(label='Xe', width=ld)
        def btn054( val = 'Xe', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn054.on_click( btn054)
        self.btn055 = Button(label='Cs', width=ld)
        def btn055( val = 'Cs', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn055.on_click( btn055)
        self.btn056 = Button(label='Ba', width=ld)
        def btn056( val = 'Ba', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn056.on_click( btn056)
        self.btn057 = Button(label='La', width=ld)
        def btn057( val = 'La', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn057.on_click( btn057)
        self.btn058 = Button(label='Ce', width=ld)
        def btn058( val = 'Ce', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn058.on_click( btn058)
        self.btn059 = Button(label='Pr', width=ld)
        def btn059( val = 'Pr', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn059.on_click( btn059)
        self.btn060 = Button(label='Nd', width=ld)
        def btn060( val = 'Nd', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn060.on_click( btn060)
        self.btn061 = Button(label='Pm', width=ld)
        def btn061( val = 'Pm', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn061.on_click( btn061)
        self.btn062 = Button(label='Sm', width=ld)
        def btn062( val = 'Sm', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn062.on_click( btn062)
        self.btn063 = Button(label='Eu', width=ld)
        def btn063( val = 'Eu', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn063.on_click( btn063)
        self.btn064 = Button(label='Gd', width=ld)
        def btn064( val = 'Gd', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn064.on_click( btn064)
        self.btn065 = Button(label='Tb', width=ld)
        def btn065( val = 'Tb', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn065.on_click( btn065)
        self.btn066 = Button(label='Dy', width=ld)
        def btn066( val = 'Dy', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn066.on_click( btn066)
        self.btn067 = Button(label='Ho', width=ld)
        def btn067( val = 'Ho', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn067.on_click( btn067)
        self.btn068 = Button(label='Er', width=ld)
        def btn068( val = 'Er', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn068.on_click( btn068)
        self.btn069 = Button(label='Tm', width=ld)
        def btn069( val = 'Tm', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn069.on_click( btn069)
        self.btn070 = Button(label='Yb', width=ld)
        def btn070( val = 'Yb', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn070.on_click( btn070)
        self.btn071 = Button(label='Lu', width=ld)
        def btn071( val = 'Lu', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn071.on_click( btn071)
        self.btn072 = Button(label='Hf', width=ld)
        def btn072( val = 'Hf', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn072.on_click( btn072)
        self.btn073 = Button(label='Ta', width=ld)
        def btn073( val = 'Ta', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn073.on_click( btn073)
        self.btn074 = Button(label='W', width=ld)
        def btn074( val = 'W:', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn074.on_click( btn074)
        self.btn075 = Button(label='Re', width=ld)
        def btn075( val = 'Re', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn075.on_click( btn075)
        self.btn076 = Button(label='Os', width=ld)
        def btn076( val = 'Os', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn076.on_click( btn076)
        self.btn077 = Button(label='Ir', width=ld)
        def btn077( val = 'Ir', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn077.on_click( btn077)
        self.btn078 = Button(label='Pt', width=ld)
        def btn078( val = 'Pt', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn078.on_click( btn078)
        self.btn079 = Button(label='Au', width=ld)
        def btn079( val = 'Au', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn079.on_click( btn079)
        self.btn080 = Button(label='Hg', width=ld)
        def btn080( val = 'Hg', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn080.on_click( btn080)
        self.btn081 = Button(label='Tl', width=ld)
        def btn081( val = 'Tl', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn081.on_click( btn081)
        self.btn082 = Button(label='Pb', width=ld)
        def btn082( val = 'Pb', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn082.on_click( btn082)
        self.btn083 = Button(label='Bi', width=ld)
        def btn083( val = 'Bi', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn083.on_click( btn083)
        self.btn084 = Button(label='Po', width=ld)
        def btn084( val = 'Po', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn084.on_click( btn084)
        self.btn085 = Button(label='At', width=ld)
        def btn085( val = 'At', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn085.on_click( btn085)
        self.btn086 = Button(label='Rn', width=ld)
        def btn086( val = 'Rn', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn086.on_click( btn086)
        self.btn087 = Button(label='Fr', width=ld)
        def btn087( val = 'Fr', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn087.on_click( btn087)
        self.btn088 = Button(label='Ra', width=ld)
        def btn088( val = 'Ra', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn088.on_click( btn088)
        self.btn089 = Button(label='Ac', width=ld)
        def btn089( val = 'Ac', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn089.on_click( btn089)
        self.btn090 = Button(label='Th', width=ld)
        def btn090( val = 'Th', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn090.on_click( btn090)
        self.btn091 = Button(label='Pa', width=ld)
        def btn091( val = 'Pa', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn091.on_click( btn091)
        self.btn092 = Button(label='U', width=ld)
        def btn092( val = 'U:', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn092.on_click( btn092)
        self.btn093 = Button(label='Np', width=ld)
        def btn093( val = 'Np', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn093.on_click( btn093)
        self.btn094 = Button(label='Pu', width=ld)
        def btn094( val = 'Pu', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn094.on_click( btn094)
        self.btn095 = Button(label='Am', width=ld)
        def btn095( val = 'Am', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn095.on_click( btn095)
        self.btn096 = Button(label='Cm', width=ld)
        def btn096( val = 'Cm', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn096.on_click( btn096)
        self.btn097 = Button(label='Bk', width=ld)
        def btn097( val = 'Bk', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn097.on_click( btn097)
        self.btn098 = Button(label='Cf', width=ld)
        def btn098( val = 'Cf', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn098.on_click( btn098)
        self.btn099 = Button(label='Es', width=ld)
        def btn099( val = 'Es', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn099.on_click( btn099)
        self.btn0100 = Button(label='Fm', width=ld)
        def btn0100( val = 'Fm', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn0100.on_click( btn0100)
        self.btn101 = Button(label='Md', width=ld)
        def btn101( val = 'Md', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn101.on_click( btn101)
        self.btn102 = Button(label='No', width=ld)
        def btn102( val = 'No', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn102.on_click( btn102)
        self.btn103 = Button(label='Lr', width=ld)
        def btn103( val = 'Lr', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn103.on_click( btn103)
        self.btn104 = Button(label='Rf', width=ld)
        def btn104( val = 'Rf', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn104.on_click( btn104)
        self.btn105 = Button(label='Db', width=ld)
        def btn105( val = 'Db', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn105.on_click( btn105)
        self.btn106 = Button(label='Sg', width=ld)
        def btn106( val = 'Sg', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn106.on_click( btn106)
        self.btn107 = Button(label='Bh', width=ld)
        def btn107( val = 'Bh', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn107.on_click( btn107)
        self.btn108 = Button(label='Hs', width=ld)
        def btn108( val = 'Hs', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn108.on_click( btn108)
        self.btn109 = Button(label='Mt', width=ld)
        def btn109( val = 'Mt', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn109.on_click( btn109)
        self.btn110 = Button(label='Ds', width=ld)
        def btn110( val = 'Ds', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn110.on_click( btn110)
        self.btn111 = Button(label='Rg', width=ld)
        def btn111( val = 'Rg', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn111.on_click( btn111)
        self.btn112 = Button(label='Cn', width=ld)
        def btn112( val = 'Cn', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn112.on_click( btn112)
        self.btn113 = Button(label='Nh', width=ld)
        def btn113( val = 'Nh', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn113.on_click( btn113)
        self.btn114 = Button(label='Fl', width=ld)
        def btn114( val = 'Fl', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn114.on_click( btn114)
        self.btn115 = Button(label='Mc', width=ld)
        def btn115( val = 'Mc', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn115.on_click( btn115)
        self.btn116 = Button(label='Lv', width=ld)
        def btn116( val = 'Lv', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn116.on_click( btn116)
        self.btn117 = Button(label='Ts', width=ld)
        def btn117( val = 'Ts', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn117.on_click( btn117)
        self.btn118 = Button(label='Og', width=ld)
        def btn118( val = 'Og', txt = self.configtxt ):
            if txt.text == '<config>':
                txt.text = val
            else:
                txt.text += ';' + val
        self.btn118.on_click( btn118)
        
        self.layout = column( self.helptxt,
                              row( self.configtxt, self.btnkill ),
                              row( self.btn001, 
                                   self.btn001a, self.btn001b, self.btn001c, self.btn001d,
                                   self.btn001e, self.btn001f, self.btn001g, self.btn001h,
                                   self.btn001i, self.btn001j, self.btn001k, self.btn001l,
                                   self.btn001m, self.btn001n, self.btn001o, self.btn001p, 
                                   self.btn002 ),
                              row( self.btn003, self.btn004, self.btn001b, self.btn001c, self.btn001d,
                                   self.btn001e, self.btn001f, self.btn001g, self.btn001h,
                                   self.btn001i, self.btn001j, self.btn001k,
                                   self.btn005, self.btn006, self.btn007, self.btn008, self.btn009, self.btn010 ),
                              row( self.btn011, self.btn012, self.btn001b, self.btn001c, self.btn001d,
                                   self.btn001e, self.btn001f, self.btn001g, self.btn001h,
                                   self.btn001i, self.btn001j, self.btn001k,
                                   self.btn013, self.btn014, self.btn015, self.btn016, self.btn017, self.btn018 ),
                              row( self.btn019, self.btn020, self.btn021, self.btn022, self.btn023, self.btn024, 
                                   self.btn025, self.btn026, self.btn027, self.btn028, self.btn029, self.btn030, 
                                   self.btn031, self.btn032, self.btn033, self.btn034, self.btn035, self.btn036 ),
                              row( self.btn037, self.btn038, self.btn039, self.btn040, self.btn041, self.btn042, 
                                   self.btn043, self.btn044, self.btn045, self.btn046, self.btn047, self.btn048, 
                                   self.btn049, self.btn050, self.btn051, self.btn052, self.btn053, self.btn054 ),
                              row( self.btn055, self.btn056, self.btn057, 
                                   self.btn072, self.btn073, self.btn074, 
                                   self.btn075, self.btn076, self.btn077, self.btn078, self.btn079, self.btn080, 
                                   self.btn081, self.btn082, self.btn083, self.btn084, self.btn085, self.btn086 ),
                              row( self.lan_txt, self.btn058, self.btn059, self.btn060, self.btn061, 
                                   self.btn062, self.btn063, self.btn064, self.btn065, self.btn066, self.btn067, 
                                   self.btn068, self.btn069, self.btn070, self.btn071  ),
                              row( self.act_txt, self.btn090, self.btn091, self.btn092 )   )
                              
#compose these individual tool-specific panels into the dashboard
#dashboard = Tabs( tabs=[tab1, tab2, tab3, tab4, tab5, tab6, tab7, tab8] )
pse = paraprobe_pse()
tab = Panel( child=pse.layout, title='Spatstat target/neighbor keyword generator with PSE' )
dashboard = Tabs( tabs=[tab], width = 1200 )
show(dashboard)
curdoc().title = "PARAPROBE Periodic Table"
curdoc().add_root(dashboard)