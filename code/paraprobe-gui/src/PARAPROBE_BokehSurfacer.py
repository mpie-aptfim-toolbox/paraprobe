import sys, glob
import numpy as np
#import param
from bokeh.io import output_file, show, curdoc
from bokeh.events import ButtonClick
from bokeh.models import Button, CustomJS, Slider, Select
from bokeh.plotting import figure, output_file, show, ColumnDataSource
from bokeh.layouts import row, column
from bokeh.models.widgets import Tabs, Panel, FileInput, Paragraph, RadioButtonGroup, Dropdown, CheckboxGroup, TextInput

#see here for handling of dates ##MK
#https://stackoverflow.com/questions/45144032/how-daterangeslider-in-bokeh-works


class paraprobe_surfacer():
    
    def __init__(self, ifo):
        #add handing over of file names + self.recon_in,
        self.simid_txt = Paragraph(text="""SimulationID <unknown>""", width=300, height=14)
        self.recon_txt = Paragraph(text="""Work with reconstruction <none>""", width=300, height=14)
        self.surf_pass_btn = Button(label='Read in previous data')
        def surf_pass_on_click( txt1 = self.simid_txt, txt2 = self.recon_txt ):
            txt1.text = str(ifo.simid)
            txt2.text = ifo.recon_fnm
            self.recon_fnm = ifo.recon_fnm
        self.surf_pass_btn.on_click( surf_pass_on_click )

        self.surf_hull_dpd = Select(title="Surfacing mode", value="Alphashape, smallest to get solid", options=["Alphashape, smallest to get solid", "Alphashape, CGAL considered optimal"])
        self.surfmode = 1
        self.alphamode = 0
        def surf_hullmode_callback(attr, old, new):
            self.surfmode = 1
            self.alphamode = 0
            if self.surf_hull_dpd.value == "Alphashape, smallest to get solid":
                self.surfmode = 1
                self.alphamode = 0
            else:
                self.surfmode = 1
                self.alphamode = 1
        self.surf_hull_dpd.on_change( 'value', surf_hullmode_callback )

        self.surf_dist_dpd = Select(title="Distancing mode", value="Analytical skin", options=["No distancing", "Analytical complete", "Analytical skin"])
        self.distmode = 2
        def surf_distmode_callback(attr, old, new):
            self.distmode = 2
            if self.surf_dist_dpd.value == "No distancing":
                self.distmode = 0
            if self.surf_dist_dpd.value == "Analytical complete":
                self.distmode = 1
            if self.surf_dist_dpd.value == "Analytical skin":
                self.distmode = 2
        self.surf_dist_dpd.on_change( 'value', surf_distmode_callback )

        self.advprune_sl = Slider(start=0.5, end=5.0, value=1.0, step=0.1, title="AdvPruningBinWidth (nm)")
        self.advdist_sl = Slider(start=1.0, end=5.0, value=1.0, step=0.1, title="AdvDistancingBinWidth (nm)")
        self.dist_r_txb = TextInput(value="2.0", title="DistancingRadiusMax (nm)")
        self.requery_sl = Slider(start=0.5, end=0.95, value=0.7, step=0.05, title="RequeryingThreshold")
        self.surf_xml_btn = Button(label='Write *.xml config file')
        self.configtxt = Paragraph(text="""<config>""", width=400, height=200)
        def surf_write_on_click( txt1 = self.configtxt ):
            keyval = {  "InputfileReconstruction": self.recon_fnm,
                        "SurfacingMode": self.surfmode,
                        "AdvIonPruneBinWidthMin": np.around(np.float32(self.advdist_sl.value), decimals=2),
                        "AdvIonPruneBinWidthIncr": np.around(np.float32(1.0), decimals=2),
                        "AdvIonPruneBinWidthMax": np.around(np.float32(self.advdist_sl.value), decimals=2),
                        "AlphaShapeAlphaValue": self.alphamode,
                        "DistancingMode": self.distmode,
                        "AdvDistanceBinWidthMin": np.around(np.float32(self.advdist_sl.value), decimals=2),
                        "AdvDistanceBinWidthIncr": np.around(np.float32(1.0), decimals=2),
                        "AdvDistanceBinWidthMax": np.around(np.float32(self.advdist_sl.value), decimals=2),
                        "DistancingRadiusMax": np.around(np.float32(self.dist_r_txb.value), decimals=2),
                        "RequeryThreshold": np.around(np.float32(self.requery_sl.value), decimals=2) }
            toolname = 'ConfigSurfacer'
            xml = '<?xml version="1.0" encoding="utf-8"?>' + '\n'
            xml += '<' + toolname + '>' + '\n'
            for key in keyval:
                xml += '\t<' + key + '>' + str(keyval[key]) + '</' + key + '>' + '\n'
            xml += '</' + toolname + '>' + '\n'
            txt1.text = xml
            file_name = 'PARAPROBE.Surfacer.SimID.' + self.simid_txt.text + '.xml'
            with open(file_name,'w') as f:
                f.write(xml)
        self.surf_xml_btn.on_click( surf_write_on_click )
        self.layout = column( self.simid_txt, 
                              self.recon_txt,
                              self.surf_pass_btn,
                              row( self.surf_hull_dpd, self.surf_dist_dpd ),
                              row( self.advprune_sl, self.advdist_sl ),
                              self.dist_r_txb, self.requery_sl,
                              self.surf_xml_btn,
                              self.configtxt )
