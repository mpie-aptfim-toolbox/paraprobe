import sys, glob
import numpy as np
#import param
from bokeh.io import output_file, show, curdoc
from bokeh.events import ButtonClick
from bokeh.models import Button, CustomJS, Slider, Select
from bokeh.plotting import figure, output_file, show, ColumnDataSource
from bokeh.layouts import row, column
from bokeh.models.widgets import Tabs, Panel, FileInput, Paragraph, RadioButtonGroup, Dropdown, CheckboxGroup, TextInput

#see here for handling of dates ##MK
#https://stackoverflow.com/questions/45144032/how-daterangeslider-in-bokeh-works


class paraprobe_synthetic():
    
    def __init__(self, ifo):
        self.simid_txt = Paragraph(text="""SimulationID <unknown>""", width=300, height=14)
        #no transcoding needed self.recon_txt = Paragraph(text="""Transcoder <unknown>""", width=300, height=14)
        self.synth_pass_btn = Button(label='Read in previous data')
        def pass_on_click( txt1 = self.simid_txt ):
            txt1.text = str(ifo.simid)
        self.synth_pass_btn.on_click( pass_on_click )
        self.synth_mode_txt = Paragraph(text="""I detected you wish synthesize <none>""", width=300, height=14)
        self.synth_mode_dpd = Select(title="Which type of specimen?", value="Single crystal", options=["Single crystal", "Polycrystal"]) #Dropdown( label="Specimen type", button_type="warning", menu=synth_mode_dpd_menu)
        self.synthmode = 0
        def synth_mode_callback(attr, old, new):
            if self.synth_mode_dpd.value == "Single crystal":
                self.synthmode = 1
            elif self.synth_mode_dpd.value == "Polycrystal":
                self.synthmode = 2
            else:
                self.synthmode = 0
        self.synth_mode_dpd.on_change( 'value', synth_mode_callback )
        self.rel_bottom_r_sl = Slider(start=0.0, end=1.0, value=0.05, step=0.01, title="Relative radius specimen bottom")
        self.rel_bottom_cap_r_sl = Slider(start=0.0, end=1.0, value=0.05, step=0.01, title="Relative hemisphere cap radius bottom")
        self.rel_top_r_sl = Slider(start=0.0, end=1.0, value=0.05, step=0.01, title="Relative radius specimen top")
        self.rel_top_cap_r_sl = Slider(start=0.0, end=1.0, value=0.05, step=0.01, title="Relative hemisphere cap radius top")
        self.natom_txb = TextInput(value="2.0e6", title="Number of atoms in the specimen")
        self.det_eff_sl = Slider(start=0.25, end=1.0, value=1.0, step=0.01, title="Detection efficiency")
        self.spatresu_sigmax_sl = Slider(start=0.0, end=0.5, value=0.0, step=0.005, title="Gaussian stdev of position spread X (nm)")
        self.spatresu_sigmay_sl = Slider(start=0.0, end=0.5, value=0.0, step=0.005, title="Gaussian stdev of position spread Y (nm)")
        self.spatresu_sigmaz_sl = Slider(start=0.0, end=0.5, value=0.0, step=0.005, title="Gaussian stdev of position spread Z (nm)")
        #self.latticeA_abc_sl = Slider(start=0.2, end=0.5, value=0.404, step=0.001, title="LatticeA a,b,c (nm)")
        self.latticeA_abc_txb = TextInput(value="0.404", title="Lattice constant a, b, c (nm)")
        self.latticeA_alp_sl = Slider(start=0.0, end=180.0, value=90.0, step=1.0, title="LatticeA alpha, beta, gamma (deg)") ###MK::add figure rendering the geometry
        self.bunge_matrix_txb = TextInput(value="0.0;0.0;0.0", title="Orientation of the matrix in Bunge-Euler notation")
        self.nprec_txb = TextInput(value="0.0", title="Number of precipitates in the specimen") ###MK::make optional
        #self.latticeB_abc_sl = Slider(start=0.2, end=0.5, value=0.405, step=0.001, title="LatticeB a,b,c (nm)")
        self.latticeB_abc_txb = TextInput(value="0.410", title="Lattice constant a, b, c (nm)")
        self.latticeB_alp_sl = Slider(start=0.0, end=180.0, value=90.0, step=1.0, title="LatticeB alpha, beta, gamma (deg)")
        self.prec_rmean_sl = Slider(start=0.0, end=20.0, value=10.0, step=0.1, title="Precipitate radius mean (nm)")
        self.prec_rsigma_sl = Slider(start=0.0, end=0.1, value=0.0, step=0.005, title="Precipitate radius stdev (nm)")
        self.bunge_prec_txb = TextInput(value="0.0;0.0;0.0", title="Orientation of the precipitate in Bunge-Euler notation")
        self.voro_rmean_sl = Slider(start=1.0, end=20.0, value=10.0, step=1.0, title="Spherical equivalent grain radius mean (nm)")
        self.synth_dbg_dpd = Select(title="Crystal structure", value="Al", options=["Al", "Al3Sc", "W", "Al3Li"])
        self.debugphase = 0
        def synth_debugphase_callback(attr, old, new):
            self.debugphase = 0
            if self.synth_dbg_dpd.value == "Al":
                self.debugphase = 0
            if self.synth_dbg_dpd.value == "Al3Sc":
                self.debugphase = 1
            if self.synth_dbg_dpd.value == "W":
                self.debugphase = 2
            if self.synth_dbg_dpd.value == "Al3Li":
                self.debugphase = 3
        self.synth_dbg_dpd.on_change( 'value', synth_debugphase_callback )
        self.synth_xml_btn = Button(label='Write *.xml config file')
        self.configtxt = Paragraph(text="""<config>""", width=400, height=200)
        def synth_write_on_click( txt1 = self.configtxt ):
            keyval = {  "SynthesizingMode": self.synthmode,
                        "DebugPhaseID": self.debugphase,
                        "SimRelBottomRadius": np.around(self.rel_bottom_r_sl.value, decimals=2),
                        "SimRelTopRadius": np.around(self.rel_top_r_sl.value, decimals=2),
                        "SimRelBottomCapHeight": np.around(self.rel_bottom_cap_r_sl.value, decimals=2),
                        "SimRelTopCapHeight": np.around(self.rel_top_cap_r_sl.value, decimals=2),
                        "SimNumberOfAtoms": np.around(np.float64(self.natom_txb.value), decimals=0),
                        "SimDetectionEfficiency": np.around(self.det_eff_sl.value, decimals=2),
                        "SimFiniteSpatResolutionX": np.around(self.spatresu_sigmax_sl.value, decimals=3),
                        "SimFiniteSpatResolutionY": np.around(self.spatresu_sigmay_sl.value, decimals=3),
                        "SimFiniteSpatResolutionZ": np.around(self.spatresu_sigmaz_sl.value, decimals=3),
                        "a1": np.around(np.float32(self.latticeA_abc_txb.value), decimals=3),
                        "b1": np.around(np.float32(self.latticeA_abc_txb.value), decimals=3),
                        "c1": np.around(np.float32(self.latticeA_abc_txb.value), decimals=3),
                        "alpha1": np.around(self.latticeA_alp_sl.value, decimals=3),
                        "beta1": np.around(self.latticeA_alp_sl.value, decimals=3),
                        "gamma1": np.around(self.latticeA_alp_sl.value, decimals=3),
                        "SimMatrixOrientation": self.bunge_matrix_txb.value,
                        "NumberOfCluster": np.around(np.float64(self.nprec_txb.value), decimals=0),
                        "a2": np.around(np.float32(self.latticeB_abc_txb.value), decimals=3),
                        "b2": np.around(np.float32(self.latticeB_abc_txb.value), decimals=3),
                        "c2": np.around(np.float32(self.latticeB_abc_txb.value), decimals=3),
                        "alpha2": np.around(self.latticeB_alp_sl.value, decimals=3),
                        "beta2": np.around(self.latticeB_alp_sl.value, decimals=3),
                        "gamma2": np.around(self.latticeB_alp_sl.value, decimals=3),
                        "ClusterRadiusMean": np.around(self.prec_rmean_sl.value, decimals=3),
                        "ClusterRadiusSigmaSqr": np.around(self.prec_rsigma_sl.value, decimals=3),
                        "SimPrecipitateOrientation": self.bunge_prec_txb.value,
                        "CrystalloVoroMeanRadius": np.around(self.voro_rmean_sl.value, decimals=2) }
            toolname = 'ConfigSynthetic'
            xml = '<?xml version="1.0" encoding="utf-8"?>' + '\n'
            xml += '<' + toolname + '>' + '\n'
            for key in keyval:
                xml += '\t<' + key + '>' + str(keyval[key]) + '</' + key + '>' + '\n'
            xml += '</' + toolname + '>' + '\n'
            txt1.text = xml
            file_name = 'PARAPROBE.Synthetic.SimID.' + self.simid_txt.text + '.xml'
            with open(file_name, 'w') as f:
                f.write(xml)
        self.synth_xml_btn.on_click( synth_write_on_click )
        self.layout = column( self.simid_txt,
                              self.synth_pass_btn,                              
                              self.synth_mode_dpd,
                              self.synth_dbg_dpd,
                              row( self.rel_top_r_sl, self.rel_top_cap_r_sl ),
                              row( self.rel_bottom_r_sl, self.rel_bottom_cap_r_sl ),
                              row( self.natom_txb, self.det_eff_sl ), 
                              self.spatresu_sigmax_sl, self.spatresu_sigmay_sl, self.spatresu_sigmaz_sl,
                              row( self.latticeA_abc_txb, self.latticeA_alp_sl, self.bunge_matrix_txb ),
                              self.nprec_txb,
                              row( self.latticeB_abc_txb, self.latticeB_alp_sl, self.bunge_prec_txb ),
                              row( self.prec_rmean_sl, self.prec_rsigma_sl ),
                              self.voro_rmean_sl,
                              self.synth_xml_btn,
                              self.configtxt )

#row( self.synth_mode_dpd, self.synth_dbg_dpd ),
#def synth_dbg_callback(attr, old, new):
#    self.debugphase = self.synth_dbg_dpd.value
#self.synth_dbg_dpd.on_change( 'value', synth_dbg_callback )
#self.synth_dbg_dpd.on_click( synth_dbg_callback )
