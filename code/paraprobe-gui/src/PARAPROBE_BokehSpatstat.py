import sys, glob
import numpy as np
from bokeh.io import output_file, show, curdoc
from bokeh.events import ButtonClick
from bokeh.models import Button, CustomJS, Slider
from bokeh.plotting import figure, output_file, ColumnDataSource
from bokeh.layouts import row, column
from bokeh.models.widgets import Tabs, Panel, FileInput, Paragraph, RadioButtonGroup, Dropdown, CheckboxGroup, TextInput #, DataRangeSlider

#https://github.com/bokeh/bokeh/issues/6219

class paraprobe_spatstat():
    
    def __init__(self, ifo):
        self.simid_txt = Paragraph(text="""SimulationID <unknown>""", width=300, height=14)
        self.recon_txt = Paragraph(text="""Work with reconstruction <none>""", width=300, height=14)
        self.hull_txt = Paragraph(text="""Read surface hull from <none>""", width=300, height=14)
        self.spst_pass_btn = Button(label='Read in previous data')
        def spst_pass_on_click( txt1 = self.simid_txt, txt2 = self.recon_txt, txt3 = self.hull_txt ):
            self.recon_fnm = ifo.recon_fnm
            self.hull_fnm = 'PARAPROBE.Surfacer.Results.SimID.' + str(ifo.simid) + '.h5'
            txt1.text = str(ifo.simid)
            txt2.text = ifo.recon_fnm
            txt3.text = self.hull_fnm            
        self.spst_pass_btn.on_click( spst_pass_on_click )
        self.pse_txb = TextInput(value="PARAPROBE.PeriodicTableOfElements.xml", title="XML file with the table of elements")
        self.what_cbx = CheckboxGroup(labels=['AnalyzeRDF', 'AnalyzeKNN', 'AnalyzeSDM'], active=[0,1])
        self.do_rdf = 0
        self.do_knn = 0
        self.do_sdm = 0
        self.roi_rdf_icr_txb = TextInput(value="0.001", title="ROI radius increment for RDF (nm)")
        self.roi_rdf_r_sl = Slider(start=0.1, end=10.0, value=2.0, step=0.1, title="ROI radius maximum for RDF (nm)")
        self.roi_knn_icr_txb = TextInput(value="0.001", title="ROI radius increment for KNN (nm)")
        self.roi_knn_r_sl = Slider(start=0.1, end=10.0, value=2.0, step=0.1, title="ROI radius maximum for KNN (nm)")
        self.roi_sdm_icr_txb = TextInput(value="0.025", title="ROI radius increment for SDM (nm)")
        self.roi_sdm_r_sl = Slider(start=0.025, end=2.0, value=2.0, step=0.005, title="ROI radius maximum for SDM (nm)")
        self.remove_bnd_roi = 1
        self.knn_kth_txb = TextInput(value="1;10;100", title="Which k-th order neighbors to analyze for RDF and KNN ?")
        self.sdm_kth_txb = TextInput(value="1;10;100", title="Which k-th order neighbors to analyze for SDM ?")
        
        self.icmb_txt = Paragraph(text="""Analyze these targets/neighbors""", width=300, height=100)
        self.icmb_tg_txb = TextInput(value="Al,H:", title="Define which target ion types")
        self.icmb_nb_txb = TextInput(value="Al,Cu", title="Define which neighboring ion types")
        self.icombis = []
        self.icmb_take_btn = Button(label="Add new combination")
        def spst_icmb_on_click( txt1 = self.icmb_txt ):
            self.icombis.append('<entry targets="' + self.icmb_tg_txb.value + '" neighbors="' + self.icmb_nb_txb.value + '"/>')
            txt1.text = ''
            for ky in self.icombis:
                txt1.text += ky + '\n'
        self.icmb_take_btn.on_click( spst_icmb_on_click )
        self.icmb_kill_btn = Button(label="Clear combinations")
        def spst_kill_on_click( txt1 = self.icmb_txt ):
            self.icombis = []
            txt1.text = ''
        self.icmb_kill_btn.on_click( spst_kill_on_click )
        self.spst_xml_btn = Button(label='Write *.xml config file')
        self.configtxt = Paragraph(text="""<config>""", width=400, height=200)
        def write_on_click( txt1 = self.configtxt ):
            what_to_do = self.what_cbx.active
            if 0 in what_to_do:
                self.do_rdf = 1
            else:
                self.do_rdf = 0
            if 1 in what_to_do:
                self.do_knn = 1
            else:
                self.do_knn = 0
            if 2 in what_to_do:
                self.do_sdm = 1
            else:
                self.do_sdm = 0
            keyval = {  "InputfilePSE": self.pse_txb.value,
                        "InputfileReconstruction": self.recon_fnm,
                        "InputfileHullAndDistances": self.hull_fnm,
                        "AnalyzeRDF": self.do_rdf,
                        "AnalyzeKNN": self.do_knn,
                        "AnalyzeSDM": self.do_sdm,
                        "ROIRadiusRDFMin": np.around(np.float32(0.0), decimals=4),
                        "ROIRadiusRDFIncr": np.around(np.float32(self.roi_rdf_icr_txb.value), decimals=4),
                        "ROIRadiusRDFMax": np.around(np.float32(self.roi_rdf_r_sl.value), decimals=4),
                        "ROIRadiusKNNMin": np.around(np.float32(0.0), decimals=4),
                        "ROIRadiusKNNIncr": np.around(np.float32(self.roi_knn_icr_txb.value), decimals=4),
                        "ROIRadiusKNNMax": np.around(np.float32(self.roi_knn_r_sl.value), decimals=4),
                        "ROIRadiusSDMMin": np.around(np.float32(0.0), decimals=4),
                        "ROIRadiusSDMIncr": np.around(np.float32(self.roi_sdm_icr_txb.value), decimals=4),
                        "ROIRadiusSDMMax": np.around(np.float32(self.roi_sdm_r_sl.value), decimals=4),
                        "ROIVolumeInsideOnly": self.remove_bnd_roi,
                        "KOrderForKNN": self.knn_kth_txb.value,
                        "KOrderForSDM": self.sdm_kth_txb.value }
            toolname = 'ConfigSpatstat'
            xml = '<?xml version="1.0" encoding="utf-8"?>' + '\n'
            xml += '<' + toolname + '>' + '\n'
            for key in keyval:
                xml += '\t<' + key + '>' + str(keyval[key]) + '</' + key + '>' + '\n'
            xml += '<IontypeCombinations>' + '\n'
            for ky in self.icombis:
                xml += ky + '\n'
            xml += '</IontypeCombinations>' + '\n'
            xml += '</' + toolname + '>' + '\n'
            txt1.text = xml
            file_name = 'PARAPROBE.Spatstat.SimID.' + self.simid_txt.text + '.xml'
            with open(file_name,'w') as f:
                f.write(xml)
        self.spst_xml_btn.on_click( write_on_click )
        self.layout = column( self.simid_txt, self.recon_txt, self.hull_txt, 
                              self.spst_pass_btn, 
                              self.pse_txb, self.what_cbx,
                              row( self.roi_rdf_r_sl, self.roi_rdf_icr_txb ),
                              row( self.roi_knn_r_sl, self.roi_knn_icr_txb ),
                              row( self.roi_sdm_r_sl, self.roi_sdm_icr_txb ),
                              self.knn_kth_txb,
                              self.sdm_kth_txb,
                              self.icmb_txt,
                              row( self.icmb_take_btn, self.icmb_kill_btn), 
                              row( self.icmb_tg_txb, self.icmb_nb_txb),
                              self.spst_xml_btn,
                              self.configtxt  )
