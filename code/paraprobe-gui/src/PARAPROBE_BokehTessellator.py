import sys, glob
import numpy as np
from bokeh.io import output_file, show, curdoc
from bokeh.events import ButtonClick
from bokeh.models import Button, CustomJS, Slider, Select
from bokeh.plotting import figure, output_file, ColumnDataSource
from bokeh.layouts import row, column
from bokeh.models.widgets import Tabs, Panel, FileInput, Paragraph, RadioButtonGroup, Dropdown, CheckboxGroup, TextInput #, DataRangeSlider

#https://github.com/bokeh/bokeh/issues/6219

class paraprobe_tessellator():
    
    def __init__(self, ifo):
        self.simid_txt = Paragraph(text="""SimulationID <unknown>""", width=300, height=14)
        self.recon_txt = Paragraph(text="""Work with reconstruction <none>""", width=300, height=14)
        self.dist_txt = Paragraph(text="""Read surface distances from <none>""", width=300, height=14)
        self.tess_pass_btn = Button(label='Read in previous data')
        def tess_pass_on_click( txt1 = self.simid_txt, txt2 = self.recon_txt, txt3 = self.dist_txt ):
            self.recon_fnm = ifo.recon_fnm
            self.dist_fnm = 'PARAPROBE.Surfacer.Results.SimID.' + str(ifo.simid) + '.h5'
            txt1.text = str(ifo.simid)
            txt2.text = ifo.recon_fnm
            txt3.text = self.dist_fnm            
        self.tess_pass_btn.on_click( tess_pass_on_click )
        self.pse_txb = TextInput(value="PARAPROBE.PeriodicTableOfElements.xml", title="XML file with the table of elements")
        self.tess_split_dpd = Select(title="SpatialSplitting approach", value="Aim for equal number", options=["Aim for equal number", "Aim for equal distance weights"])
        self.splitmode = 0
        def split_mode_callback(attr, old, new):
            if self.tess_split_dpd.value == "Aim for equal number":
                self.splitmode = 0
            else:
                self.synthmode = 1
        self.tess_split_dpd.on_change( 'value', split_mode_callback )
        self.tess_dero_sl = Slider(start=0.25, end=10.0, value=1.0, step=0.25, title="Cell erosion distance (nm)")
        self.what_cbx = CheckboxGroup(labels=['IOCellVolume', 'IOCellNeighbors', 'IOCellShape', 'IOCellProfiling'], active=[0,1,3])
        self.do_vol = 0        
        self.do_nbs = 0
        self.do_shp = 0
        self.do_prf = 0        
        self.tess_xml_btn = Button(label='Write *.xml config file')
        self.configtxt = Paragraph(text="""<config>""", width=400, height=200)        
        def write_on_click( txt1 = self.configtxt ):
            what_to_do = self.what_cbx.active
            if 0 in what_to_do:
                self.do_vol = 1
            else:
                self.do_vol = 0
            if 1 in what_to_do:
                self.do_nbs = 1
            else:
                self.do_nbs = 0
            if 2 in what_to_do:
                self.do_shp = 1
            else:
                self.do_shp = 0
            if 3 in what_to_do:
                self.do_prf = 1
            else:
                self.do_prf = 0
            keyval = {  "InputfilePeriodicTableOfElements": self.pse_txb.value,
                        "InputfileReconstruction": self.recon_fnm,
                        "InputfileDistances": self.dist_fnm,
                        "CellErosionDistance": np.around(np.float32(self.tess_dero_sl.value), decimals=2),
                        "SpatialSplittingModel": self.splitmode,
                        "IOCellVolume": self.do_vol,
                        "IOCellNeighbors": self.do_nbs,
                        "IOCellShape": self.do_shp,
                        "IOCellProfiling": self.do_prf }
            toolname = 'ConfigTessellator'
            xml = '<?xml version="1.0" encoding="utf-8"?>' + '\n'
            xml += '<' + toolname + '>' + '\n'
            for key in keyval:
                xml += '\t<' + key + '>' + str(keyval[key]) + '</' + key + '>' + '\n'
            #xml += '<IontypeCombinations>' + '\n'
            #for ky in self.icombis:
            #    xml += ky + '\n'
            #xml += '</IontypeCombinations>' + '\n'
            xml += '</' + toolname + '>' + '\n'
            txt1.text = xml
            file_name = 'PARAPROBE.Tessellator.SimID.' + self.simid_txt.text + '.xml'
            with open(file_name,'w') as f:
                f.write(xml)
        self.tess_xml_btn.on_click( write_on_click )
        self.layout = column( self.simid_txt, self.recon_txt, self.dist_txt, 
                              self.tess_pass_btn,  
                              self.pse_txb, 
                              self.tess_dero_sl, 
                              self.what_cbx,
                              self.tess_xml_btn,
                              self.configtxt  )
