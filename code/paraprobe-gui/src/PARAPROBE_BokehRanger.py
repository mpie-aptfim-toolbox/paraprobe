import sys, glob
import numpy as np
#import param
from bokeh.io import output_file, show, curdoc
from bokeh.events import ButtonClick
from bokeh.models import Button, CustomJS, Slider, Select
from bokeh.plotting import figure, output_file, show, ColumnDataSource
from bokeh.layouts import row, column
from bokeh.models.widgets import Tabs, Panel, FileInput, Paragraph, RadioButtonGroup, Dropdown, CheckboxGroup, TextInput

#see here for handling of dates ##MK
#https://stackoverflow.com/questions/45144032/how-daterangeslider-in-bokeh-works


class paraprobe_ranger():
    
    def __init__(self, ifo):
        #add handing over of file names + self.recon_in,
        self.simid_txt = Paragraph(text="""SimulationID <unknown>""", width=300, height=14)
        self.recon_txt = Paragraph(text="""I detected you wish work with reconstruction <none>""", width=300, height=14)
        self.range_txt = Paragraph(text="""I detected you wish range externally with <none>""", width=300, height=14)
        self.rang_pass_btn = Button(label='Read in previous data')
        def rang_pass_on_click( txt1 = self.simid_txt, txt2 = self.recon_txt, txt3 = self.range_txt ):
            txt1.text = str(ifo.simid)
            txt2.text = ifo.recon_fnm
            txt3.text = ifo.rrng_fnm
            self.recon_fnm = ifo.recon_fnm
            self.range_fnm = ifo.rrng_fnm
        self.rang_pass_btn.on_click( rang_pass_on_click )
        self.pse_txb = TextInput(value="PARAPROBE.PeriodicTableOfElements.xml", title="XML file with the table of elements")
        self.rangemode_txt = Paragraph(text="""Currently only RangeMode 0 !""", width=300, height=14)
        self.rangemode = 0
        self.rang_xml_btn = Button(label='Write *.xml config file')
        self.configtxt = Paragraph(text="""<config>""", width=400, height=200)
        def write_on_click( txt1 = self.configtxt ):
            keyval = { "InputfilePSE": self.pse_txb.value,
                        "InputfileReconstruction": self.recon_fnm,
                        "InputfileRangingData": self.range_fnm,
                        "RangingMode": self.rangemode }
            toolname = 'ConfigRanger'
            xml = '<?xml version="1.0" encoding="utf-8"?>' + '\n'
            xml += '<' + toolname + '>' + '\n'
            for key in keyval:
                xml += '\t<' + key + '>' + str(keyval[key]) + '</' + key + '>' + '\n'
            xml += '</' + toolname + '>' + '\n'
            txt1.text = xml
            file_name = 'PARAPROBE.Ranger.SimID.' + self.simid_txt.text + '.xml'
            with open(file_name,'w') as f:
                f.write(xml)
        self.rang_xml_btn.on_click( write_on_click ) #ButtonClick, lambda _: self.write_xml() ) #on_event(ButtonClick, self.write_xml( simid ) )
        self.layout = column( self.simid_txt,
                              self.recon_txt, 
                              self.range_txt,
                              self.rang_pass_btn,
                              self.pse_txb,     
                              self.rangemode_txt,
                              self.rang_xml_btn,
                              self.configtxt )
