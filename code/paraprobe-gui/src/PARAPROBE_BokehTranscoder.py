import sys, glob
import numpy as np
#import param
from bokeh.io import output_file, show, curdoc
from bokeh.events import ButtonClick
from bokeh.models import Button, CustomJS, Slider
from bokeh.plotting import figure, output_file, show, ColumnDataSource
from bokeh.layouts import row, column
from bokeh.models.widgets import Tabs, Panel, FileInput, Paragraph, RadioButtonGroup


class paraprobe_transcoder():
    
    def __init__(self, ifo):
        #add handing over of file names + self.recon_in,
        self.simid_txt = Paragraph(text="""SimulationID <unknown>""", width=300, height=14)
        self.trans_txt = Paragraph(text="""I detected you work with <none>""", width=300, height=14)
        self.recon_txt = Paragraph(text="""I detected you wish to transcode <none>""", width=300, height=14)
        self.range_txt = Paragraph(text="""I detected you wish to range <none>""", width=300, height=14)
        self.mode_txt = Paragraph(text="""TranscodingMode is <none>""", width=300, height=14)
        self.trsc_pass_btn = Button(label='Read in previous data')
        def pass_on_click( txt1 = self.simid_txt, txt2 = self.trans_txt, txt3 = self.recon_txt, txt4 = self.range_txt, txt5 = self.mode_txt ):
            txt1.text = str(ifo.simid)
            txt2.text = ifo.trans_fnm
            txt3.text = ifo.recon_fnm
            txt4.text = ifo.rrng_fnm
            txt5.text = str(0) #synthetic
            if ifo.usecase == 1:
                txt5.text = str(1) #pos
            if ifo.usecase == 2:
                txt5.text = str(2) #epos
            if ifo.usecase == 3:
                txt5.text = str(3) #apt
        self.trsc_pass_btn.on_click( pass_on_click )
        self.trsc_xml_btn = Button(label='Write *.xml config file')
        #upon clicking the button execute callback
        #callback = xml_btn_clk( me, simid ):
        self.configtxt = Paragraph(text="""<config>""", width=400, height=200)
        def write_on_click( txt1 = self.configtxt ):
            keyval = { "TranscodingMode": self.mode_txt.text, 
                       "Inputfile": self.trans_txt.text }
            toolname = 'ConfigTranscoder'
            xml = '<?xml version="1.0" encoding="utf-8"?>' + '\n'
            xml += '<' + toolname + '>' + '\n'
            for key in keyval:
                xml += '\t<' + key + '>' + str(keyval[key]) + '</' + key + '>' + '\n'
            xml += '</' + toolname + '>' + '\n'
            txt1.text = xml
            file_name = 'PARAPROBE.Transcoder.SimID.' + self.simid_txt.text + '.xml'
            with open(file_name,'w') as f:
                f.write(xml)
        self.trsc_xml_btn.on_click( write_on_click ) #ButtonClick, lambda _: self.write_xml() ) #on_event(ButtonClick, self.write_xml( simid ) )
        self.layout = column( self.trsc_pass_btn, 
                              self.simid_txt, self.trans_txt, self.recon_txt, self.range_txt, self.mode_txt, 
                              self.trsc_xml_btn, self.configtxt )

#    def write_xml(self):
#        self.keyval = { "TranscodingMode": self.mode_txt.text, 
#                        "Inputfile": self.recon_txt.text }
#        self.toolname = 'ConfigTranscoder'
#        
#        self.xml = '<?xml version="1.0" encoding="utf-8"?>' + '\n'
#        self.xml += '<' + self.toolname + '>' + '\n'
#        for key in self.keyval:
#            self.xml += '\t<' + key + '>' + str(self.keyval[key]) + '</' + key + '>' + '\n'
#        self.xml += '</' + self.toolname + '>' + '\n'
#        #file_path = Path(file_name)
#        #file_path.write_text(self.to_utf8())
#        file_name = 'PARAPROBE.Transcoder.SimID.' + str(self.simid_txt.text) + '.xml'
#        with open(file_name,'w') as f:
#             f.write(self.xml)
