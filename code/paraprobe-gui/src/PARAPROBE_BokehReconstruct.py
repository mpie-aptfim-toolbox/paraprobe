import sys, glob
import numpy as np
#import param
from bokeh.io import output_file, show, curdoc
from bokeh.events import ButtonClick
from bokeh.models import Button, CustomJS, Slider, Select
from bokeh.plotting import figure, output_file, show, ColumnDataSource
from bokeh.layouts import row, column
from bokeh.models.widgets import Tabs, Panel, FileInput, Paragraph, RadioButtonGroup, Dropdown, CheckboxGroup, TextInput

#see here for handling of dates ##MK
#https://stackoverflow.com/questions/45144032/how-daterangeslider-in-bokeh-works


class paraprobe_reconstruct():
    
    def __init__(self, ifo):
        self.simid_txt = Paragraph(text="""SimulationID <unknown>""", width=300, height=14)
        self.recon_txt = Paragraph(text="""Transcoder <unknown>""", width=300, height=14)
        self.recon_pass_btn = Button(label='Read in previous data')
        def pass_on_click( txt1 = self.simid_txt, txt2 = self.recon_txt ):
            txt1.text = str(ifo.simid)
            txt2.text = ifo.recon_fnm
        self.recon_pass_btn.on_click( pass_on_click )
        self.pse_txb = TextInput(value="PARAPROBE.PeriodicTableOfElements.xml", title="XML file with the table of elements")
        #self.space_mode_txt = Paragraph(text="""I detected you wish recon <none>""", width=300, height=14)
        self.space_mode_dpd = Select(title="Which reference space?", value="Reconstruction space", options=["Reconstruction space"]) #, "Detector space"])
        self.spacemode = 0
        def space_mode_callback(attr, old, new):
            if self.space_mode_dpd.value == "Reconstruction space":
                self.spacemode = 0
            else:
                self.spacemode = 1 #"Detector space"
        self.space_mode_dpd.on_change( 'value', space_mode_callback )        
        self.recon_mode_txt = Paragraph(text="""I detected you wish recon <none>""", width=300, height=14)
        self.recon_mode_dpd = Select(title="Which reconstruction model?", value="Modified Bas (Gault2011)", options=["Modified Bas (Gault2011)"])
        self.reconmode = 0
        def recon_mode_callback(attr, old, new):
            if self.recon_mode_dpd.value == "Modified Bas (Gault2011)":
                self.reconmode = 0
            else:
                self.reconmode = 0
        self.recon_mode_dpd.on_change( 'value', recon_mode_callback )
        self.flightlen_sl = Slider(start=50.0, end=300.0, value=100.0, step=0.5, title="Flight length (cm)")
        self.adensity_txb = TextInput(value="60.24", title="Atomic density (atoms/nm^3)")
        self.evapfield_sl = Slider(start=1.0, end=60.0, value=19.0, step=0.1, title="Evaporation field (V/nm)")
        #self.det_eff_min_sl = Slider(start=0.25, end=1.0, value=1.0, step=0.01, title="Detection efficiency minimum")
        #self.det_eff_max_sl = Slider(start=0.25, end=1.0, value=1.0, step=0.01, title="Detection efficiency maximum")
        self.det_eff_sl = Slider(start=0.25, end=1.0, value=1.0, step=0.01, title="Detection efficiency")
        self.kf_sl = Slider(start=1.0, end=10.0, value=3.0, step=0.1, title="Field factor")
        self.icf_sl = Slider(start=1.00, end=2.00, value=1.05, step=0.01, title="Image compression factor") #MK::m or ICF = m-1?
        self.recon_xml_btn = Button(label='Write *.xml config file')
        self.configtxt = Paragraph(text="""<config>""", width=400, height=200)
        def recon_write_on_click( txt1 = self.configtxt ):
            keyval = {  "InputfilePeriodicTableOfElements": self.pse_txb.value,
                        "InputfileAPTH5": self.recon_txt.text,
                        "WhichSpace": self.spacemode,
                        "ReconstructionModel": self.reconmode, 
                        "FlightLength": np.around(np.float32(self.flightlen_sl.value), decimals=3),
                        "AtomicDensity": np.around(np.float32(self.adensity_txb.value), decimals=3),
                        "EvaporationField": np.around(np.float32(self.evapfield_sl.value), decimals=3),
                        "DetectionEfficiencyMin": np.around(np.float32(self.det_eff_sl.value), decimals=3),
                        "DetectionEfficiencyIncr": np.around(np.float32(0.01), decimals=3),
                        "DetectionEfficiencyMax": np.around(np.float32(self.det_eff_sl.value), decimals=3),
                        "FieldFactorMin": np.around(np.float32(self.kf_sl.value), decimals=3),
                        "FieldFactorIncr": np.around(np.float32(0.01), decimals=3),
                        "FieldFactorMax": np.around(np.float32(self.kf_sl.value), decimals=3),
                        "ImageCompressionFactorMin": np.around(np.float32(self.icf_sl.value), decimals=3),
                        "ImageCompressionFactorIncr": np.around(np.float32(0.01), decimals=3),
                        "ImageCompressionFactorMax": np.around(np.float32(self.icf_sl.value), decimals=3) }
            toolname = 'ConfigReconstruct'
            xml = '<?xml version="1.0" encoding="utf-8"?>' + '\n'
            xml += '<' + toolname + '>' + '\n'
            for key in keyval:
                xml += '\t<' + key + '>' + str(keyval[key]) + '</' + key + '>' + '\n'
            xml += '</' + toolname + '>' + '\n'
            txt1.text = xml
            file_name = 'PARAPROBE.Reconstruct.SimID.' + self.simid_txt.text + '.xml'
            with open(file_name, 'w') as f:
                f.write(xml)
        self.recon_xml_btn.on_click( recon_write_on_click )
        self.layout = column( self.simid_txt,
                              self.recon_txt,
                              self.recon_pass_btn,
                              self.pse_txb,
                              self.space_mode_dpd,
                              self.recon_mode_dpd,
                              self.flightlen_sl,
                              self.adensity_txb,
                              self.evapfield_sl,
                              self.det_eff_sl,
                              self.kf_sl,
                              self.icf_sl,
                              self.recon_xml_btn,
                              self.configtxt )
