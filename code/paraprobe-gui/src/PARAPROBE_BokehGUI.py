import sys, glob
import numpy as np
#import param
from bokeh.io import output_file, show, curdoc
from bokeh.events import ButtonClick
from bokeh.models import Button, CustomJS, Slider
from bokeh.plotting import figure, output_file, show, ColumnDataSource
from bokeh.layouts import row, column
from bokeh.models.widgets import Tabs, Panel, FileInput, Paragraph, RadioButtonGroup

from PARAPROBE_BokehUtils import *
from PARAPROBE_BokehTranscoder import *
from PARAPROBE_BokehReconstruct import *
from PARAPROBE_BokehSynthetic import *
from PARAPROBE_BokehRanger import *
from PARAPROBE_BokehSurfacer import *
from PARAPROBE_BokehSpatstat import *
from PARAPROBE_BokehTessellator import *
from PARAPROBE_BokehAraullo import *
from PARAPROBE_BokehFourier import *
from PARAPROBE_BokehIndexer import *
from PARAPROBE_BokehSubmit import *

#define a simulation ID to use for the job
SIMID = 0
utils = paraprobe_utils( SIMID )

#MK::README
#paraprobe works in general in reconstruction space
#the utils tab gives information how to proceed and configure a run in certain cases

transcoder = paraprobe_transcoder( utils )
reconstruct = paraprobe_reconstruct( utils )
synthetic = paraprobe_synthetic( utils )

#analysis tools
ranger = paraprobe_ranger( utils )
surfacer = paraprobe_surfacer( utils )
spatstat = paraprobe_spatstat( utils )
tessellator = paraprobe_tessellator( utils )
#araullo = paraprobe_araullo( utils )
#fourier = paraprobe_fourier( utils )
##MK::pass results from araullo
#indexer = paraprobe_indexer( utils )

workflow = paraprobe_submit( utils )

#define panels/tabs of the dashboard
tab01 = Panel( child=utils.layout, title='Load specimen' )
tab02 = Panel( child=transcoder.layout, title='Transcoder' )
tab03 = Panel( child=reconstruct.layout, title='Reconstruct' )
tab04 = Panel( child=synthetic.layout, title='Synthetic' )
tab05 = Panel( child=ranger.layout, title='Ranger' )
tab06 = Panel( child=surfacer.layout, title='Surfacer' )
tab07 = Panel( child=spatstat.layout, title='Spatstat' )
tab08 = Panel( child=tessellator.layout, title='Tessellator' )
#tab09 = Panel( child=araullo.layout, title='Araullo' )
#tab10 = Panel( child=fourier.layout, title='Fourier')
#tab11 = Panel( child=indexer.layout, title='Indexer' )
tab12 = Panel( child=workflow.layout, title ='Submit' )

#compose these individual tool-specific panels into the dashboard
dashboard = Tabs( tabs=[tab01, tab02, tab03, tab04, tab05, tab06, tab07, tab08, tab12] )#, tab09, tab10, tab11] )] )
#dashboard = Tabs( tabs=[tab12] )



#activate and display the dashboard in the browser, tested with Firefox
#show(dashboard)

curdoc().title = "PARAPROBE Configuration Wizard"
curdoc().add_root(dashboard)
#session = push_session(curdoc())
#session.show()
#session.loop_until_closed()



#x = np.linspace(0, 10, 500)
#y = np.sin(x)
#source = ColumnDataSource(data=dict(x=x, y=y))
#plot = figure(y_range=(-10, 10), plot_width=400, plot_height=400)
#plot.line('x', 'y', source=source, line_width=3, line_alpha=0.6)
#amp_slider = Slider(start=0.1, end=10, value=1, step=.1, title="Amplitude")
#freq_slider = Slider(start=0.1, end=10, value=1, step=.1, title="Frequency")
#phase_slider = Slider(start=0, end=6.4, value=0, step=.1, title="Phase")
#offset_slider = Slider(start=-5, end=5, value=0, step=.1, title="Offset")
#callback = CustomJS(args=dict(source=source, amp=amp_slider, freq=freq_slider, phase=phase_slider, offset=offset_slider),
#                    code="""
#    const data = source.data;
#    const A = amp.value;
#    const k = freq.value;
#    const phi = phase.value;
#    const B = offset.value;
#    const x = data['x']
#    const y = data['y']
#    for (var i = 0; i < x.length; i++) {
#        y[i] = B + A*Math.sin(k*x[i]+phi);
#    }
#    source.change.emit();
#""")
#amp_slider.js_on_change('value', callback)
##freq_slider.js_on_change('value', callback)
##phase_slider.js_on_change('value', callback)
##offset_slider.js_on_change('value', callback)
#layout = column(amp_slider, freq_slider, phase_slider, offset_slider)
