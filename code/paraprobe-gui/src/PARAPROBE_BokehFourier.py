import sys, glob
import numpy as np
from bokeh.io import output_file, show, curdoc
from bokeh.events import ButtonClick
from bokeh.models import Button, CustomJS, Slider, Select
from bokeh.plotting import figure, output_file, ColumnDataSource
from bokeh.layouts import row, column
from bokeh.models.widgets import Tabs, Panel, FileInput, Paragraph, RadioButtonGroup, Dropdown, CheckboxGroup, TextInput #, DataRangeSlider

#https://github.com/bokeh/bokeh/issues/6219

class paraprobe_fourier():
    
    def __init__(self, ifo):
        self.simid_txt = Paragraph(text="""SimulationID <unknown>""", width=300, height=14)
        self.recon_txt = Paragraph(text="""Work with reconstruction <none>""", width=300, height=14)
        self.hull_txt = Paragraph(text="""Read surface hull from <none>""", width=300, height=14)
        self.recon_fnm = ''
        self.hull_fnm = ''
        self.fou_pass_btn = Button(label='Read in previous data')
        def fou_pass_on_click( txt1 = self.simid_txt, txt2 = self.recon_txt, txt3 = self.hull_txt ):
            self.recon_fnm = ifo.recon_fnm
            self.hull_fnm = 'PARAPROBE.Surfacer.Results.SimID.' + str(ifo.simid) + '.h5'
            txt1.text = str(ifo.simid)
            txt2.text = ifo.recon_fnm
            txt3.text = self.hull_fnm            
        self.fou_pass_btn.on_click( fou_pass_on_click )
        self.pse_txb = TextInput(value="PARAPROBE.PeriodicTableOfElements.xml", title="XML file with the table of elements")
        self.samplingmode = 0
        self.volsampling_dpd = Select(title="Volume sampling method", value="Cuboidal grid", options=["Cuboidal grid", "User-defined point", "Specimen center", "N random points"])
        def fou_mode_callback(attr, old, new):
            if self.volsampling_dpd.value == "Cuboidal grid":
                self.samplingmode = 0
            elif self.volsampling_dpd.value == "User-defined point":
                self.samplingmode = 1
            elif self.volsampling_dpd.value == "Specimen center":
                self.samplingmode = 2
            else:
                self.samplingmode = 3
        self.volsampling_dpd.on_change( 'value', fou_mode_callback )
        self.icmb_txb = TextInput(value='<entry targets="Al" realspace="0.405"/>', title="Define which ion combinations to probe")
        self.roi_r_sl = Slider(start=0.1, end=10.0, value=2.0, step=0.1, title="ROI radius maximum (nm)")
        self.roi_grid_x_sl = Slider(start=0.5, end=10.0, value=4.0, step=0.5, title="ROI grid spacing X (nm)")
        self.roi_grid_y_sl = Slider(start=0.5, end=10.0, value=4.0, step=0.5, title="ROI grid spacing Y (nm)")
        self.roi_grid_z_sl = Slider(start=0.5, end=10.0, value=4.0, step=0.5, title="ROI grid spacing Z (nm)")
        self.roi_where_txb = TextInput(value="0.0;0.0;0.0", title="Sampling position x,y,z (nm)")
        self.roi_n_txb = TextInput(value="10000", title="ROI fixed number")
        self.remove_bnd_roi = 1 ###MK::make optional
        self.hkl_min_sl = Slider(start=-1.0, end=-0.5, value=-1.0, step=0.1, title="Reciprocal space h,k,l min")
        self.hkl_max_sl = Slider(start=+0.5, end=+1.0, value=+1.0, step=0.1, title="Reciprocal space h,k,l min")
        self.hkl_nvoxel_sl = Slider(start=32, end=512, value=64, step=2, title="Reciprocal space voxel")
        self.sgnpks_thrshld_sl = Slider(start=0.01, end=1.0, value=0.01, step=0.01, title="Significant peak threshold")
        self.dbscan_r_sl = Slider(start=0.1, end=2.0, value=1.41, step=0.01, title="DBScan search radius (voxel)")
        self.dbscan_minpks_sl = Slider(start=1,end=100, value=1, step=1, title="DBScan minimum number of h,k,l per cluster")
        self.what_cbx = CheckboxGroup(labels=['Store full reciprocal space', 'Store significant peaks only', 'Store DBScan'], active=[1])
        self.gpus_per_node_sl = Slider(start=0, end=2, value=0, step=1, title="How many nodes exist per node?")
        self.gpu_wkload = 1
        self.iohkl = 0
        self.iosgnpks = 0
        self.iodbscan = 0
        self.fou_xml_btn = Button(label='Write *.xml config file')
        self.configtxt = Paragraph(text="""<config>""", width=400, height=200)
        def fou_write_on_click( txt1 = self.configtxt ):
            what_to_store = self.what_cbx.active
            if 0 in what_to_store:
                self.iohkl = 1
            if 1 in what_to_store:
                self.iosgnpks = 1
            if 2 in what_to_store:
                self.iodbscan = 1
            keyval = {  "InputfilePSE": self.pse_txb.value,
                        "InputfileReconstruction": self.recon_fnm,
                        "InputfileHullAndDistances": self.hull_fnm,
                        "VolumeSamplingMethod": self.samplingmode,
                        "ROIRadiusMax": self.roi_r_sl.value,
                        "SamplingGridBinWidthX": self.roi_grid_x_sl.value,
                        "SamplingGridBinWidthY": self.roi_grid_y_sl.value,
                        "SamplingGridBinWidthZ": self.roi_grid_z_sl.value,
                        "SamplingPosition": self.roi_where_txb.value,
                        "FixedNumberOfROIs": self.roi_n_txb.value,
                        "RemoveROIsProtrudingOutside": self.remove_bnd_roi,
                        "HKLGridBoundsHMin": self.hkl_min_sl.value,
                        "HKLGridBoundsHMax": self.hkl_max_sl.value,
                        "HKLGridHVoxelResolution": self.hkl_nvoxel_sl.value,
                        "SignificantPeakIntensityThreshold": self.sgnpks_thrshld_sl.value,
                        "DBScanSearchRadius": self.dbscan_r_sl.value,
                        "DBScanMinPksForReflector": self.dbscan_minpks_sl.value, 
                        "IOStoreFullGridPeaks": self.iohkl,
                        "IOStoreSignificantPeaks": self.iosgnpks,
                        "IODBScanOnSignificantPeaks": self.iodbscan,
                        "GPUsPerComputingNode": self.gpus_per_node_sl.value,
                        "GPUWorkload": self.gpu_wkload }
            toolname = 'ConfigFourier'
            xml = '<?xml version="1.0" encoding="utf-8"?>' + '\n'
            xml += '<' + toolname + '>' + '\n'
            for key in keyval:
                xml += '\t<' + key + '>' + str(keyval[key]) + '</' + key + '>' + '\n'
            xml += '<LatticeIontypeCombinations>' + '\n'
            xml += self.icmb_txb.value + '\n'
            xml += '</LatticeIontypeCombinations>' + '\n'
            xml += '</' + toolname + '>' + '\n'
            txt1.text = xml
            file_name = 'PARAPROBE.Fourier.SimID.' + self.simid_txt.text + '.xml'
            with open(file_name,'w') as f:
                f.write(xml)
        self.fou_xml_btn.on_click( fou_write_on_click )        
        self.layout = column( self.fou_pass_btn, self.simid_txt, self.recon_txt, self.hull_txt,
                              self.pse_txb,
                              self.volsampling_dpd,
                              self.icmb_txb,
                              self.roi_r_sl,
                              self.roi_grid_x_sl, self.roi_grid_y_sl, self.roi_grid_z_sl,
                              self.roi_where_txb,
                              self.roi_n_txb,
                              self.hkl_min_sl,
                              self.hkl_max_sl,
                              self.hkl_nvoxel_sl,
                              self.sgnpks_thrshld_sl,
                              self.dbscan_r_sl,
                              self.dbscan_minpks_sl,
                              self.what_cbx,
                              self.gpus_per_node_sl,
                              self.fou_xml_btn,
                              self.configtxt )
