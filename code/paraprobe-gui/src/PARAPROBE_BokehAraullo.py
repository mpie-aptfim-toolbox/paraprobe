import sys, glob
import numpy as np
from bokeh.io import output_file, show, curdoc
from bokeh.events import ButtonClick
from bokeh.models import Button, CustomJS, Slider, Select
from bokeh.plotting import figure, output_file, ColumnDataSource
from bokeh.layouts import row, column
from bokeh.models.widgets import Tabs, Panel, FileInput, Paragraph, RadioButtonGroup, Dropdown, CheckboxGroup, TextInput #, DataRangeSlider

#https://github.com/bokeh/bokeh/issues/6219

class paraprobe_araullo():
    
    def __init__(self, ifo):
        self.simid_txt = Paragraph(text="""SimulationID <unknown>""", width=300, height=14)
        self.recon_txt = Paragraph(text="""Work with reconstruction <none>""", width=300, height=14)
        self.hull_txt = Paragraph(text="""Read surface hull from <none>""", width=300, height=14)
        self.recon_fnm = ''
        self.hull_fnm = ''
        self.ara_pass_btn = Button(label='Read in previous data')
        def ara_pass_on_click( txt1 = self.simid_txt, txt2 = self.recon_txt, txt3 = self.hull_txt ):
            self.recon_fnm = ifo.recon_fnm
            self.hull_fnm = 'PARAPROBE.Surfacer.Results.SimID.' + str(ifo.simid) + '.h5'
            txt1.text = str(ifo.simid)
            txt2.text = ifo.recon_fnm
            txt3.text = self.hull_fnm            
        self.ara_pass_btn.on_click( ara_pass_on_click )
        self.pse_txb = TextInput(value="PARAPROBE.PeriodicTableOfElements.xml", title="XML file with the table of elements")
        self.s2grid_txb = TextInput(value="PARAPROBE.Araullo.GeodesicSphere.40962.h5", title="Which directions to use?")
        self.samplingmode = 0
        self.volsampling_dpd = Select(title="Volume sampling method", value="Cuboidal grid", options=["Cuboidal grid", "User-defined point", "Specimen center", "N random points"])
        def ara_mode_callback(attr, old, new):
            if self.volsampling_dpd.value == "Cuboidal grid":
                self.samplingmode = 0
            elif self.volsampling_dpd.value == "User-defined point":
                self.samplingmode = 1
            elif self.volsampling_dpd.value == "Specimen center":
                self.samplingmode = 2
            else:
                self.samplingmode = 3
        self.volsampling_dpd.on_change( 'value', ara_mode_callback )
        self.icmb_txb = TextInput(value='<entry targets="Al" realspace="0.2025" realspacemin="0.18225" realspacemax="0.22275"/>', title="Define which ion combinations to probe")
        self.roi_r_sl = Slider(start=0.1, end=10.0, value=2.0, step=0.1, title="ROI radius maximum (nm)")
        self.roi_grid_x_sl = Slider(start=0.5, end=10.0, value=4.0, step=0.5, title="ROI grid spacing X (nm)")
        self.roi_grid_y_sl = Slider(start=0.5, end=10.0, value=4.0, step=0.5, title="ROI grid spacing Y (nm)")
        self.roi_grid_z_sl = Slider(start=0.5, end=10.0, value=4.0, step=0.5, title="ROI grid spacing Z (nm)")
        self.roi_where_txb = TextInput(value="0.0;0.0;0.0", title="Sampling position x,y,z (nm)")
        self.roi_n_txb = TextInput(value="10000", title="ROI fixed number")
        self.remove_bnd_roi = 1 ###MK::make optional
        self.m_sl = Slider(start=8, end=12, value=8, step=1, title="Power-of-two exponent for bins")
        self.normalize = 1 ###MK::make the following three optional
        self.windowingmode = 0
        self.kaiser = 8.0
        self.what_cbx = CheckboxGroup(labels=['Store histograms', 'Store Fourier spectra', 'Store three strongest frequencies/amplitude', 'Store specific frequency'], active=[3])
        self.maxmem_sl = Slider(start=2, end=256, value=8, step=2, title="Cache memory (GB)") #per node or process?
        self.proc_per_node = 1
        self.gpus_per_node_sl = Slider(start=0, end=2, value=0, step=1, title="How many nodes exist per node?")
        self.gpus_per_proc = 1
        self.gpu_wkload = 1
        self.iohst = 0
        self.iofft = 0
        self.io3 = 0
        self.iospecific = 0
        self.ara_xml_btn = Button(label='Write *.xml config file')
        self.configtxt = Paragraph(text="""<config>""", width=400, height=200)
        def ara_write_on_click( txt1 = self.configtxt ):
            what_to_store = self.what_cbx.active
            if 0 in what_to_store:
                self.iohst = 1
            if 1 in what_to_store:
                self.iofft = 1
            if 2 in what_to_store:
                self.io3 = 1
            if 3 in what_to_store:
                self.iospecific = 1
            keyval = {  "InputfilePSE": self.pse_txb.value,
                        "InputfileElevAzimGrid": self.s2grid_txb.value,
                        "InputfileReconstruction": self.recon_fnm,
                        "InputfileHullAndDistances": self.hull_fnm,
                        "VolumeSamplingMethod": self.samplingmode,
                        "ROIRadiusMax": self.roi_r_sl.value,
                        "SamplingGridBinWidthX": self.roi_grid_x_sl.value,
                        "SamplingGridBinWidthY": self.roi_grid_y_sl.value,
                        "SamplingGridBinWidthZ": self.roi_grid_z_sl.value,
                        "SamplingPosition": self.roi_where_txb.value,
                        "FixedNumberOfROIs": self.roi_n_txb.value,
                        "RemoveROIsProtrudingOutside": self.remove_bnd_roi,
                        "SDMBinExponent": self.m_sl.value,
                        "SDMNormalize": self.normalize,
                        "SDMWindowingMethod": self.windowingmode,
                        "KaiserAlpha": self.kaiser,
                        "IOStoreHistoCnts": self.iohst,
                        "IOStoreHistoFFTMagn": self.iofft,
                        "IOStoreThreeStrongestPeaks": self.io3,
                        "IOStoreSpecificPeaks": self.iospecific,
                        "MaxMemoryPerNode": self.maxmem_sl.value,
                        "ProcessesPerMPITeam": self.proc_per_node,
                        "GPUsPerComputingNode": self.gpus_per_node_sl.value,
                        "GPUPerProcess": self.gpus_per_proc,
                        "GPUWorkloadFactor": self.gpu_wkload }
            toolname = 'ConfigAraullo'
            xml = '<?xml version="1.0" encoding="utf-8"?>' + '\n'
            xml += '<' + toolname + '>' + '\n'
            for key in keyval:
                xml += '\t<' + key + '>' + str(keyval[key]) + '</' + key + '>' + '\n'
            xml += '<LatticeIontypeCombinations>' + '\n'
            xml += self.icmb_txb.value + '\n'
            xml += '</LatticeIontypeCombinations>' + '\n'
            xml += '</' + toolname + '>' + '\n'
            txt1.text = xml
            file_name = 'PARAPROBE.Araullo.SimID.' + self.simid_txt.text + '.xml'
            with open(file_name,'w') as f:
                f.write(xml)
        self.ara_xml_btn.on_click( ara_write_on_click )        
        self.layout = column( self.ara_pass_btn, self.simid_txt, self.recon_txt, self.hull_txt,
                              self.pse_txb, self.s2grid_txb,
                              self.volsampling_dpd,
                              self.icmb_txb,
                              self.roi_r_sl,
                              self.roi_grid_x_sl, self.roi_grid_y_sl, self.roi_grid_z_sl,
                              self.roi_where_txb,
                              self.roi_n_txb,
                              self.m_sl,
                              self.what_cbx,
                              self.maxmem_sl,
                              self.gpus_per_node_sl,
                              self.ara_xml_btn,
                              self.configtxt )
