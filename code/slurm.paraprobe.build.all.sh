#!bin/bash

#MK::welcome we are need to execute from paraprobe/code

##DEFINE WHICH TOOLS TO COMPILE, set to 1 to activate else not compiled
build_transcoder=1
build_synthetic=1
build_reconstruct=1 #not released yet
build_ranger=1
build_surfacer=1
build_spatstat=1
build_dbscan=1
build_tessellator=1 #not released yet
build_araullo=1
build_fourier=0
build_indexer=0
build_intersector=0

##set to true to always clear the build folder before compiling
clean_build=false #true #true
##set to true to copy all executables to a single location to be used in a workflow
collect_executables=true
#target_folder="/home/m.kuehbach/GITHUB/MPIE_APTFIM_TOOLBOX/paraprobe/research/"
#target_folder="/home/m.kuehbach/GITHUB/MPIE_APTFIM_TOOLBOX/paraprobe/code/"
##working directory on MPI30
#target_folder="/home/m.kuehbach/Paper14/code/"
target_folder="/home/m.kuehbach/Paper18/code/"
##working directory on TALOS
#target_folder="/talos/scratch/mkuehbac/Paper14/code/"

##WHICH MACHINE TO USE
which_machine="mpi30" #"talos" #mpi30" #"mpi30"

##WHICH COMPILER TO USE?
compile_which="gcc" #"itl" "gcc" "pgi"

##CHECK CURRENT GITVERSION?
check_codeversion=0 #1

##################################################################################################################################################
##SEMI-AUTOMATIC SECTION##########################################################################################################################
##users need to set environment variables specific for target machine here########################################################################
##################################################################################################################################################
##################################################################################################################################################

##SET UP YOUR ENVIRONMENT VARIABLE USING ENVIRONMENT MODULES
#start clean
echo "Preparing environment"
module purge
module list
#make choice based on compiler
if [ "$compile_which" == "itl" ]; then
	##HERE IS AN EXAMPLE FOR INTEL and NO CUDA
	echo "Loading environment for compiling with Intel and using IMKL"
	if [ "$which_machine" == "mpi30" ]; then
		module load Compilers/Intel/Intel2018
		module load MPI/Intel/IntelMPI2018
		module load Libraries/IntelMKL2018
	fi
	if [ "$which_machine" == "talos" ]; then
		module load cmake
		module load intel
		module load impi
	fi
	MYWHO_COMPILER="ITL"
	MYCCC_COMPILER=icc
	MYCXX_COMPILER=icpc
fi
if [ "$compile_which" == "gcc" ]; then
	##HERE IS AN EXAMPLE FOR GCC and NO CUDA
	echo "Loading environment for compiling with GCC"
	if [ "$which_machine" == "mpi30" ]; then
		module load Compilers/GCC/GCC-7.5.0
		module load MPI/GCC/MPICH-GCC-7.5.0
	fi
	if [ "$which_machine" == "talos" ]; then
		module load cmake
		module load gcc
		module load impi
	fi
	MYWHO_COMPILER="GCC"
	MYCCC_COMPILER=gcc
	MYCXX_COMPILER=g++
fi
if [ "$compile_which" == "pgi" ]; then
	echo "Loading environment for compiling with PGI"
	if [ "$which_machine" == "mpi30" ]; then
		module load Compilers/PGI/PGI-19.4
		module load Compilers/PGI/OpenMPI-PGI-19.4
	fi
	if [ "$which_machine" == "talos" ]; then
		module load cmake
		module load pgi
		##mpi and cuda
	fi
	MYWHO_COMPILER="PGI"	
	MYCCC_COMPILER=pgcc
	MYCXX_COMPILER=pg++
fi	
#show me those
module list
echo "Environment prepared"


##################################################################################################################################################
##AUTOMATIC SECTION###############################################################################################################################
##users should not be required to make any changes here###########################################################################################
##################################################################################################################################################
##################################################################################################################################################

##PREPROCESSING INSTALL EXTERNAL LIBRARIES AND DEPENDENCIES
##tbd

MYGITSHA="unknown"
if [ "$check_codeversion" == 1 ]; then
	##get GitSHA of the project to include into the compile such that we know with which code the tool works
	echo "Identifying the paraprobe version"
	#MYGITSHA="$(git describe --abbrev=50 --always --tags)" 
	MYGITSHA="$(git describe --abbrev=50 --dirty --broken --always --tags)"
	echo $MYGITSHA
fi

##COMPILE THE SHARED TOOL UTILITIES
echo "Compiling paraprobe-utils ..."
mkdir -p paraprobe-utils/build
cd paraprobe-utils/build
if [ "$clean_build" = true ]; then
	echo "Cleaning $PWD"
	rm -rf *
fi
echo "Configuring paraprobe-utils ..."
#rm -rf * #-DCMAKE_C_COMPILER=$MYCCC_COMPILER 
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER .. 1>PARAPROBE.Utils.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Utils.CMake.$MYWHO_COMPILER.STDERR.txt
echo "Compiling paraprobe-utils ..."
make 1>PARAPROBE.Utils.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Utils.Make.$MYWHO_COMPILER.STDERR.txt
#DO NOT DELETE THESE GENERATED FILES below TOOLS WILL INCLUDE SOME OF THEM
cd ../../

##COMPILE THE FORMAT TRANSCODING TOOL
if [ "$build_transcoder" == 1 ]; then
	echo "Compiling paraprobe-transcoder ..."
	mkdir -p paraprobe-transcoder/build
	cd paraprobe-transcoder/build
	if [ "$clean_build" = true ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-transcoder ..."	
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Transcoder.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Transcoder.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compiling paraprobe-transcoder ..."
	make 1>PARAPROBE.Transcoder.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Transcoder.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_transcoder
	#cp paraprobe_transcoder ../run/
	if [ "$collect_executables" = true ]; then
		cp paraprobe_transcoder $target_folder/paraprobe_transcoder
	fi
	#rm -rf *
	cd ../../
fi


##COMPILE THE SYNTHETIC STRUCTURE GENERATOR
if [ "$build_synthetic" == 1 ]; then
	echo "Compiling paraprobe-synthetic ..."
	mkdir -p paraprobe-synthetic/build
	cd paraprobe-synthetic/build
	if [ "$clean_build" = true ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-synthetic ..."
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Synthetic.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Synthetic.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compiling paraprobe-synthetic ..."
	make 1>PARAPROBE.Synthetic.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Synthetic.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_synthetic
	#cp paraprobe_synthetic ../run/
	if [ "$collect_executables" = true ]; then
		cp paraprobe_synthetic $target_folder/paraprobe_synthetic
	fi
	#rm -rf *
	cd ../../
fi


##COMPILE THE RECONSTRUCTION TOOL
if [ "$build_reconstruct" == 1 ]; then
	echo "Compiling paraprobe-reconstruct ..."
	mkdir -p paraprobe-reconstruct/build
	cd paraprobe-reconstruct/build
	if [ "$clean_build" = true ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-reconstruct ..."
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Reconstruct.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Reconstruct.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compiling paraprobe-reconstruct ..."
	make 1>PARAPROBE.Reconstruct.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Reconstruct.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_reconstruct
	#cp paraprobe_reconstruct ../run/
	if [ "$collect_executables" = true ]; then
		cp paraprobe_reconstruct $target_folder/paraprobe_reconstruct
	fi
	#rm -rf *
	cd ../../
fi


##COMPILE THE RANGER RANGING TOOL
if [ "$build_ranger" == 1 ]; then
	echo "Compiling paraprobe-ranger ..."
	mkdir -p paraprobe-ranger/build
	cd paraprobe-ranger/build
	if [ "$clean_build" = true ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-ranger ..."
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Ranger.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Ranger.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compiling paraprobe-ranger ..."
	make 1>PARAPROBE.Ranger.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Ranger.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_ranger
	#cp paraprobe_ranger ../run/
	if [ "$collect_executables" = true ]; then
		cp paraprobe_ranger $target_folder/paraprobe_ranger
	fi
	#rm -rf *
	cd ../../
fi


##COMPILE THE SURFACING AND DISTANCING TOOL
if [ "$build_surfacer" == 1 ]; then
	echo "Compiling paraprobe-surfacer ..."
	mkdir -p paraprobe-surfacer/build
	cd paraprobe-surfacer/build
	if [ "$clean_build" = true ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-surfacer ..."
	if [ "$which_machine" == "mpi30" ]; then	
		#MYCGALHOME=/home/m.kuehbach/APT3DOIM/src/thirdparty/CGAL/CGAL-4.11.3/
		MYCGALHOME=/home/m.kuehbach/cgal/CGAL-4.11.3/
	fi
	if [ "$which_machine" == "talos" ]; then
		MYCGALHOME=/talos/u/mkuehbac/CGAL/CGAL-4.11.3/
	fi
	cmake -DCMAKE_BUILD_TYPE=Release -DCGAL_DIR=$MYCGALHOME -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Surfacer.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Surfacer.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compile paraprobe-surfacer ..."
	make 1>PARAPROBE.Surfacer.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Surfacer.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_surfacer
	#cp paraprobe_surfacer ../run/
	if [ "$collect_executables" = true ]; then
		cp paraprobe_surfacer $target_folder/paraprobe_surfacer
	fi
	#rm -rf *
	cd ../../
fi


##COMPILE DESCRIPTIVE SPATIAL STATISTICS TOOL
if [ "$build_spatstat" == 1 ]; then
	echo "Compiling paraprobe-spatstat ..."
	mkdir -p paraprobe-spatstat/build
	cd paraprobe-spatstat/build
	if [ "$clean_build" = true ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-spatstat ..."
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Spatstat.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Spatstat.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compiling paraprobe-spatstat ..."
	make 1>PARAPROBE.Spatstat.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Spatstat.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_spatstat
	#cp paraprobe_spatstat ../run/
	if [ "$collect_executables" = true ]; then
		cp paraprobe_spatstat $target_folder/paraprobe_spatstat
	fi
	#rm -rf *
	cd ../../
fi


##COMPILE DBSCAN CLUSTERING TOOL
if [ "$build_dbscan" == 1 ]; then
	echo "Compiling paraprobe-dbscan ..."
	mkdir -p paraprobe-dbscan/build
	cd paraprobe-dbscan/build
	if [ "$clean_build" = true ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-dbscan ..."
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.DBScan.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.DBScan.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compiling paraprobe-dbscan ..."
	make 1>PARAPROBE.DBScan.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.DBScan.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_dbscan
	#cp paraprobe_dbscan ../run/
	if [ "$collect_executables" = true ]; then
		cp paraprobe_dbscan $target_folder/paraprobe_dbscan
	fi
	#rm -rf *
	cd ../../
fi


##COMPILE TESSELLATOR TOOL
if [ "$build_tessellator" == 1 ]; then
	echo "Compiling paraprobe-tessellator ..."
	mkdir -p paraprobe-tessellator/build
	cd paraprobe-tessellator/build
	if [ "$clean_build" = true ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Tessellator.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Tessellator.CMake.$MYWHO_COMPILER.STDERR.txt
	make 1>PARAPROBE.Tessellator.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Tessellator.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_tessellator
	#cp paraprobe_tessellator ../run/
	if [ "$collect_executables" = true ]; then
		cp paraprobe_tessellator $target_folder/paraprobe_tessellator
	fi
	#rm -rf *
	cd ../../
fi


##COMPILE THE APTCRYSTALLOGRAPHY DIRECT FOURIER TRANSFORMATION TOOL
#needs the PGI compiler to support all three MPI, OpenMP, and OpenACC
#MK::CURRENTLY DEBUGGED FIRST WITH THE INTEL COMPILER AND INTEL MPI
#dont forget to set Cuda and multi gpu environment
if [ "$build_araullo" == 1 ]; then
	echo "Compiling paraprobe-araullo ..."
	mkdir -p paraprobe-araullo/build
	cd paraprobe-araullo/build
	if [ "$clean_build" = true ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Araullo.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Araullo.CMake.$MYWHO_COMPILER.STDERR.txt
	make 1>PARAPROBE.Araullo.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Araullo.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_araullo
	#cp paraprobe_araullo ../run/
	if [ "$collect_executables" = true ]; then
		cp paraprobe_araullo $target_folder/paraprobe_araullo
	fi
	#rm -rf *
	cd ../../
fi


if [ "$build_fourier" == 1 ]; then
	echo "Compiling paraprobe-fourier ..."
	mkdir -p paraprobe-fourier/build
	cd paraprobe-fourier/build
	if [ "$clean_build" = true ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Fourier.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Fourier.CMake.$MYWHO_COMPILER.STDERR.txt
	make 1>PARAPROBE.Fourier.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Fourier.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_fourier
	#cp paraprobe_fourier ../run/
	if [ "$collect_executables" = true ]; then
		cp paraprobe_fourier $target_folder/paraprobe_fourier
	fi
	#rm -rf *
	cd ../../
fi


if [ "$build_indexer" == 1 ]; then
	echo "Compiling paraprobe-indexer ..."
	mkdir -p paraprobe-indexer/build
	cd paraprobe-indexer/build
	if [ "$clean_build" = true ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Indexer.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Indexer.CMake.$MYWHO_COMPILER.STDERR.txt
	make 1>PARAPROBE.Indexer.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Indexer.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_indexer
	#cp paraprobe_indexer ../run/
	if [ "$collect_executables" = true ]; then
		cp paraprobe_indexer $target_folder/paraprobe_indexer
	fi
	#rm -rf *
	cd ../../
fi


if [ "$build_intersector" == 1 ]; then
	echo "Compiling TetGen library..."
	cd paraprobe-intersector/src/thirdparty/TetGen/tetgen1.5.1
	rm *.o *.a
	make CC=$MYCCC_COMPILER CXX=$MYCXX_COMPILER tetlib 1>TetGen.Make.$MYWHO_COMPILER.STDOUT.txt 2>TetGen.Make.$MYWHO_COMPILER.STDERR.txt
	cd ../../../../../
	echo "Compiling paraprobe-intersector ..."
	mkdir -p paraprobe-intersector/build
	cd paraprobe-intersector/build
	if [ "$clean_build" = true ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Intersector.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Intersector.CMake.$MYWHO_COMPILER.STDERR.txt
	make 1>PARAPROBE.Intersector.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Intersector.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_intersector
	#cp paraprobe_indexer ../run/
	if [ "$collect_executables" = true ]; then
		cp paraprobe_intersector $target_folder/paraprobe_intersector
	fi
	cd ../../
fi

#add additional tools

#useful resources for further improvement and simplifications
#https://stackoverflow.com/questions/24460486/cmake-build-type-is-not-being-used-in-cmakelists-txt
