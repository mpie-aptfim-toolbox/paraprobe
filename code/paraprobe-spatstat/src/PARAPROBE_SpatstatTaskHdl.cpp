//##MK::GPLV3

#include "PARAPROBE_SpatstatTaskHdl.h"

ostream& operator << (ostream& in, TypeCombi const & val) {
	in << "__" << val.targets << "__\t\t__" << val.nbors << "__" << "\n";
	return in;
}



itypeCombiHdl::itypeCombiHdl()
{
}


itypeCombiHdl::~itypeCombiHdl()
{
}


bool itypeCombiHdl::load_iontype_combinations( string xmlfn )
{
	//cout << "Importing itypeCombinations..." << "\n";
	ifstream file( xmlfn );
	if ( file.fail() ) {
		cerr << "Unable to locate input file " << xmlfn << "\n"; return false;
	}
	else {
		cout << "Opening input file " << xmlfn << "\n";
	}

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigSpatstat")) {
		cerr << "Undefined parameters file!" << "\n"; return false;
	}

	xml_node<>* comb_node = rootNode->first_node("IontypeCombinations");
	int i = 0;
	for (xml_node<> * entry_node = comb_node->first_node("entry"); entry_node; entry_node = entry_node->next_sibling() ) {
		string trg = entry_node->first_attribute("targets")->value();
		string nbr = entry_node->first_attribute("neighbors")->value();

		if ( trg != "" && nbr != "" ) {
			this->icombis.push_back( TypeCombi( trg, nbr) );
cout << this->icombis.back() << "\n";

			string ky = "IontypeCombi" + to_string(i); i++;
			string vl = "Targets;" + trg + ";Neighbors;" + nbr;
			this->iifo.push_back( pparm( ky, vl, "", "" ) );
		}
		else {
			cout << "WARNING attempt to pass an inacceptable IontypeCombination entry both targets and neighbors are empty!" << "\n"; return false;
		}
	}

	return true;
}

/*
bool itypeCombiHdl::prepare_iontype_combinations( rangeTable & ranger, const size_t max_memory_per_node )
{
	//paraprobe-spatstat buffers for each thread and each task 2x histograms and/or 2x SDMs respectively, to
	//allow for thread-memory-local parallelized writing back of results without global sync
	//this may create situations where the computing node provides not enough memory to allocate all the buffers
	//consequently, we want to catch this as earliest as possible to warn the user and advise less workpackages per run

	//max_memory_per_node in Byte, approximately MK::just a not too naive mechanism to avoid
	//that unexperienced user issue instruct e.g. 1000^3 large SDM with 100-th of combinations
	cout << "At most " << max_memory_per_node << " B will be used to store temporary results!" << "\n";

	//firstly, we transform the human-readable icombis targ/nbor strings pairs into evapion3 objects
	vector<Evapion3Combi> tmp;
	for( auto it = this->icombis.begin(); it != this->icombis.end(); it++ ) {
		tmp.push_back( Evapion3Combi() );

		ranger.add_evapion3( it->targets, tmp.back().targets );

		ranger.add_evapion3( it->nbors, tmp.back().nbors );

		//##BEGIN DEBUG
		cout << "Targets __" << it->targets << "__ ---->" << "\n";
		for( auto jt = tmp.back().targets.begin(); jt != tmp.back().targets.end(); jt++ ) {
			cout << (int) jt->Z1 << ";" << (int) jt->Z2 << ";" << (int) jt->Z3  << "\n";
		}
		cout << "Nbors __" << it->nbors << "__ ---->" << "\n";
		for( auto jt = tmp.back().nbors.begin(); jt != tmp.back().nbors.end(); jt++ ) {
			cout << (int) jt->Z1 << ";" << (int) jt->Z2 << ";" << (int) jt->Z3  << "\n";
		}
		//##END DEBUG
	}

	//secondly, we have to understand that these user-combinations may not at all be present in the ranging data, i.e.
	//are possibly not all evapion3 types that we identifiable in the analyzed dataset as ranging data,
	//sure, in most cases user will make correct input but in every case we need to make sure
	map<size_t,size_t>::iterator does_not_exist = ranger.iontypes_dict.end();
	for( auto jt = tmp.begin(); jt != tmp.end(); jt++ ) {
		UC8Combi valid;
		for( auto tg = jt->targets.begin(); tg != jt->targets.end(); tg++ ) {
			size_t hashval = ranger.hash_molecular_ion( tg->Z1, tg->Z2, tg->Z3 );
			//MK::it is important to understand here that the user is not enforced to write the molecular ions
			//within the XML input file in order, because above preprocessing assures that combinations such as AlH: or H:Al will be sorted
			//into a unique representation namely Al, H, UNOCCUPIED as it holds also for the ranging
			//Consequently, internal ion type handling is handling commutativity to please the user
			map<size_t,size_t>::iterator vt = ranger.iontypes_dict.find( hashval );
			if ( vt != does_not_exist ) { //kt is an ion type which exists within the ranging data
cout << " Adding an identified ion from the ranging process as target\t\t" << vt->first << "\t\t" << vt->second << "\n";
				valid.targets.push_back( vt->second );
			}
		}
		for( auto nb = jt->nbors.begin(); nb != jt->nbors.end(); nb++ ) {
			size_t hashval = ranger.hash_molecular_ion( nb->Z1, nb->Z2, nb->Z3 );
			//MK::it is important to understand here that the user is not enforced to write the molecular ions
			//within the XML input file in order, because above preprocessing assures that combinations such as AlH: or H:Al will be sorted
			//into a unique representation namely Al, H, UNOCCUPIED as it holds also for the ranging
			//Consequently, internal ion type handling is handling commutativity to please the user
			map<size_t,size_t>::iterator vt = ranger.iontypes_dict.find( hashval );
			if ( vt != does_not_exist ) { //kt is an ion type which exists within the ranging data
cout << " Adding an identified ion from the ranging process as neighb\t\t" << vt->first << "\t\t" << vt->second << "\n";
				valid.nbors.push_back( vt->second );
			}
		}

		this->combinations.push_back( UC8Combi() );
		for( auto tgs = valid.targets.begin(); tgs != valid.targets.end(); tgs++ ) { //explicit deep copy
			this->combinations.back().targets.push_back( *tgs );
		}
		for( auto nbs = valid.nbors.begin(); nbs != valid.nbors.end(); nbs++ ) {
			this->combinations.back().nbors.push_back( *nbs );
		}

		//1.) check if any evapion3 matches to any known ranged
		//2.) if so create a new UC8Combi object and
		//3.) translate evapion3 to known unsigned char iontype values from the ConfigSpatstat::InputfileReconstruction "/Ranging..."
		//MK::the trick here is to end up that eventually the actual analysis of iontypes will use exclusively
		//unsigned char keys. e.g. lets assume we have Al-Al and Al was ranged from the input as unsigned char == 0x01 then
		//the pairing Al-Al becomes UC8Combi.targets = 0x01 and UC8Combi.nbors = 0x01
		//targets/nbors == 0x00 means always all ions
		//we do not allow for negations of types as this can always be reformulated
		//5.) we store eventually in this->
		//this->combinations.push_back( UC8Combi() );
		//combinations.back().targets.push_back()
		//combinations.back().nbors.push_back();
	}
	//get rid of temporaries
	tmp = vector<Evapion3Combi>();


	//thirdly, and now having the IontypeCombinations we plan the actual tasks
	unsigned int tskid = 0; //scientific analysis tasks start at 0
	size_t max_memory_expected = 0; //in bytes

	if ( this->combinations.size() < 1 ) {
		cerr << "Combinations were specified but not parsable after having evaluated InputfileReconstruction ranging data and settings!" << "\n";
		return false;
	}

cout << "About to prepare RDF tasks..." << "\n";
	if ( ConfigSpatstat::AnalyzeRDF == true ) {
		linear_ival iv_rdf = linear_ival( ConfigSpatstat::ROIRadiiRDF.min, ConfigSpatstat::ROIRadiiRDF.incr, ConfigSpatstat::ROIRadiiRDF.max );
		histogram hst1d_rdf = histogram( iv_rdf );
		size_t mem_per_hst1d_rdf = static_cast<size_t>(hst1d_rdf.nbins)*sizeof(double) + sizeof(histogram);

		for( auto it = this->combinations.begin(); it != this->combinations.end(); it++ ) {
			//for ORIGINAL labels
			this->itasks.push_back( SpatstatTask( ConfigSpatstat::ROIRadiiRDF, tskid, ORIGINAL_IONTYPES, WANT_RDF, WANT_NONE, WANT_NONE ) );
			for( auto tgs = it->targets.begin(); tgs != it->targets.end(); tgs++ ) {
				this->itasks.back().targ_nbors.push_back( *tgs );
			}
			this->itasks.back().pos_firstnbor = this->itasks.back().targ_nbors.size(); //first all target iontypes on [0, pos_firstnbor), next all neighbors on [pos_firstnbor,targ_nbors.size())
			for( auto nbs = it->nbors.begin(); nbs != it->nbors.end(); nbs++ ) {
				this->itasks.back().targ_nbors.push_back( *nbs );
			}
			tskid++;
			//for RANDOMIZED labels
			this->itasks.push_back( SpatstatTask( ConfigSpatstat::ROIRadiiRDF, tskid, RANDOMIZED_IONTYPES, WANT_RDF, WANT_NONE, WANT_NONE ) );
			for( auto tgs = it->targets.begin(); tgs != it->targets.end(); tgs++ ) {
				this->itasks.back().targ_nbors.push_back( *tgs );
			}
			this->itasks.back().pos_firstnbor = this->itasks.back().targ_nbors.size();
			for( auto nbs = it->nbors.begin(); nbs != it->nbors.end(); nbs++ ) {
				this->itasks.back().targ_nbors.push_back( *nbs );
			}
			tskid++;
			//res buffer size expectation
			max_memory_expected += 2*mem_per_hst1d_rdf; //because one for ORIGINAL and one for RANDOMIZED
		}
	}

	//for KNN multiple KNN values are possible need to parse ConfigSpatstat::KOrderForKNN
cout << "About to prepare KNN tasks..." << "\n";
	if ( ConfigSpatstat::AnalyzeKNN == true ) {
		vector<unsigned int> knn_kth;
		size_t nsemicolon_knn = count( ConfigSpatstat::KOrderForKNN.begin(), ConfigSpatstat::KOrderForKNN.end(), ';' );
		stringstream parsethis_knn;
		parsethis_knn << ConfigSpatstat::KOrderForKNN;
		string datapiece_knn;
		for( size_t i = 0; i < nsemicolon_knn + 1; i++ ) { //parse at least the single value
			getline( parsethis_knn, datapiece_knn, ';');
			unsigned long val = stoul(datapiece_knn);
			if ( val < static_cast<unsigned long>(UINT32MX) ) {
				knn_kth.push_back( static_cast<unsigned int>(val) );
			}
		}

		if ( knn_kth.size() > 0 ) {
			linear_ival iv_knn = linear_ival( ConfigSpatstat::ROIRadiiKNN.min, ConfigSpatstat::ROIRadiiKNN.incr, ConfigSpatstat::ROIRadiiKNN.max );
			histogram hst1d_knn = histogram( iv_knn );
			size_t mem_per_hst1d_knn = static_cast<size_t>(hst1d_knn.nbins)*sizeof(double) + sizeof(histogram);

			for( auto kt = knn_kth.begin(); kt != knn_kth.end(); kt++ ) {
	cout << "k = " << *kt << "\n";
				for( auto it = this->combinations.begin(); it != this->combinations.end(); it++ ) {
					//for ORIGINAL labels
					this->itasks.push_back( SpatstatTask( ConfigSpatstat::ROIRadiiKNN, *kt, tskid,
							ORIGINAL_IONTYPES, WANT_NONE, WANT_KNN, WANT_NONE ) );
					for( auto tgs = it->targets.begin(); tgs != it->targets.end(); tgs++ ) {
						this->itasks.back().targ_nbors.push_back( *tgs );
					}
					this->itasks.back().pos_firstnbor = this->itasks.back().targ_nbors.size(); //first all target iontypes on [0, pos_firstnbor), next all neighbors on [pos_firstnbor,targ_nbors.size())
					for( auto nbs = it->nbors.begin(); nbs != it->nbors.end(); nbs++ ) {
						this->itasks.back().targ_nbors.push_back( *nbs );
					}
					tskid++;
					//for RANDOMIZED labels
					this->itasks.push_back( SpatstatTask( ConfigSpatstat::ROIRadiiKNN, *kt, tskid,
							RANDOMIZED_IONTYPES, WANT_NONE, WANT_KNN, WANT_NONE ) );
					for( auto tgs = it->targets.begin(); tgs != it->targets.end(); tgs++ ) {
						this->itasks.back().targ_nbors.push_back( *tgs );
					}
					this->itasks.back().pos_firstnbor = this->itasks.back().targ_nbors.size();
					for( auto nbs = it->nbors.begin(); nbs != it->nbors.end(); nbs++ ) {
						this->itasks.back().targ_nbors.push_back( *nbs );
					}
					tskid++;
					//res buffer size expectation
					max_memory_expected += 2*mem_per_hst1d_knn; //because one for ORIGINAL and one for RANDOMIZED
				} //next combination
			} //next kth
			knn_kth = vector<unsigned int>();
		}
		else {
			cout << "WARNING::Even though KNN jobs were desired no k-th order values were parsable from " << ConfigSpatstat::KOrderForKNN << "\n";
		}
	}

	//also for SDM multiple KNN values are possible need to parse ConfigSpatstat::KOrderForSDM
cout << "About to prepare SDM tasks..." << "\n";
	if ( ConfigSpatstat::AnalyzeSDM == true ) {
		vector<unsigned int> sdm_kth;
		size_t nsemicolon_sdm = count( ConfigSpatstat::KOrderForSDM.begin(), ConfigSpatstat::KOrderForSDM.end(), ';' );
		stringstream parsethis_sdm;
		parsethis_sdm << ConfigSpatstat::KOrderForSDM;
		string datapiece_sdm;
		for( size_t i = 0; i < nsemicolon_sdm + 1; i++ ) { //parse at least the single value
			getline( parsethis_sdm, datapiece_sdm, ';');
			unsigned long val = stoul(datapiece_sdm);
			if ( val < static_cast<unsigned long>(UINT32MX) ) {
				sdm_kth.push_back( static_cast<unsigned int>(val) );
			}
		}

		if ( sdm_kth.size() > 0 ) {
			//sdm3d sdmprobe = sdm3d( ConfigSpatstat::ROIRadii.max, ConfigSpatstat::ROIRadii.incr, 201 );
			size_t mem_per_sdm3d = CUBE(201) * sizeof(unsigned int) + sizeof(sdm3d); //sdmprobe.box.NXYZ * sizeof(unsigned int) + sizeof(sdm3d);
			for( auto kt = sdm_kth.begin(); kt != sdm_kth.end(); kt++ ) {
	cout << "k = " << *kt << "\n";
				for( auto it = this->combinations.begin(); it != this->combinations.end(); it++ ) {
					//for ORIGINAL labels
					this->itasks.push_back( SpatstatTask( ConfigSpatstat::ROIRadiiSDM, *kt, tskid,
							ORIGINAL_IONTYPES, WANT_NONE, WANT_NONE, WANT_SDM ) );
					for( auto tgs = it->targets.begin(); tgs != it->targets.end(); tgs++ ) {
						this->itasks.back().targ_nbors.push_back( *tgs );
					}
					this->itasks.back().pos_firstnbor = this->itasks.back().targ_nbors.size(); //first all target iontypes on [0, pos_firstnbor), next all neighbors on [pos_firstnbor,targ_nbors.size())
					for( auto nbs = it->nbors.begin(); nbs != it->nbors.end(); nbs++ ) {
						this->itasks.back().targ_nbors.push_back( *nbs );
					}
					tskid++;
					//for RANDOMIZED labels
					this->itasks.push_back( SpatstatTask( ConfigSpatstat::ROIRadiiSDM, *kt, tskid,
							RANDOMIZED_IONTYPES, WANT_NONE, WANT_NONE, WANT_SDM ) );
					for( auto tgs = it->targets.begin(); tgs != it->targets.end(); tgs++ ) {
						this->itasks.back().targ_nbors.push_back( *tgs );
					}
					this->itasks.back().pos_firstnbor = this->itasks.back().targ_nbors.size();
					for( auto nbs = it->nbors.begin(); nbs != it->nbors.end(); nbs++ ) {
						this->itasks.back().targ_nbors.push_back( *nbs );
					}
					tskid++;
					//res buffer size expectation
					max_memory_expected += 2*mem_per_sdm3d; //because one for ORIGINAL and one for RANDOMIZED
				} //next combination
			} //next kth
		}
		else {
			cout << "WARNING::Even though SDM jobs were desired no k-th order values were parsable from " << ConfigSpatstat::KOrderForSDM << "\n";
		}
		sdm_kth = vector<unsigned int>();
	}

	//final check would all these buffers fit in memory?
	if ( ( max_memory_expected * static_cast<size_t>(1 + omp_get_max_threads()) ) >= max_memory_per_node ) { //+1 because there is the process-local copy to decouple critical regions of non-master threads and master thread
		cerr << "With " << max_memory_expected << " * " << omp_get_max_threads() << " threads we would exceed the max_memory_per_node!" << "\n";
		return false;
	}
	else {
		cout << "Rank expected memory demands for buffering intermediate results to be " << max_memory_expected << " B" << "\n";
	}

	return true;
}
*/

