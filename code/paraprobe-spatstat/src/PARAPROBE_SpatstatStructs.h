//##MK::GPLV3


#ifndef __PARAPROBE_SPATSTAT_STRUCTS_H__
#define __PARAPROBE_SPATSTAT_STRUCTS_H__

//here is the level where we loop in all tool-specific utils
//#include "PARAPROBE_AraulloHDF5.h"
#include "PARAPROBE_SpatstatXDMF.h"

//##MK::moves this to the ranger in utilities
#define UNKNOWN_IONTYPE		0

struct ityp
{
	unsigned char m_org;	//the physically-based assigned label
	unsigned char m_rnd;	//the pseudo-randomized uncorrelated label
	ityp() : m_org(UNKNOWN_IONTYPE), m_rnd(UNKNOWN_IONTYPE) {}
	ityp( const unsigned char _morg, const unsigned char _mrnd ) : m_org(_morg), m_rnd(_mrnd) {}
};

ostream& operator << (ostream& in, ityp const & val);

/*
struct p3dm2
{
	//ion 3d coordinates, original, i.e. measured ion type and randomized type
	apt_real x;
	apt_real y;
	apt_real z;
	unsigned int m_org;	//the physically measured label
	unsigned int m_rnd;	//the randomized label
	p3dm2() : x(0.f), y(0.f), z(0.f), m_org(UNKNOWN_IONTYPE), m_rnd(UNKNOWN_IONTYPE) {}
	p3dm2( const apt_real _x, const apt_real _y, const apt_real _z, const unsigned int _morg, const unsigned int _mrnd ) :
		x(_x), y(_y), z(_z), m_org(_morg), m_rnd(_mrnd) {}
};

ostream& operator << (ostream& in, p3dm2 const & val);
*/

struct ival
{
	size_t incl_left;
	size_t excl_right;
	ival() : incl_left(0), excl_right(0) {}
	ival( const size_t _left, const size_t _right ) :
		incl_left(_left), excl_right(_right) {}
};

ostream& operator << (ostream& in, ival const & val);

#endif
