//##MK::CODESPLIT

#ifndef __PARAPROBE_SPATSTAT_ACQUISOR_H__
#define __PARAPROBE_SPATSTAT_ACQUISOR_H__

#include "PARAPROBE_SpatstatTaskHdl.h"

class acquisor;

class spatstat_res
{
public:
	spatstat_res();
	~spatstat_res();
	
	//void plan( vector<p3dm1> const & cand );
	//cfftn ft;
	acquisor* owner;

	//implicit results arrays per material point
	//vector<float> SpecificPeaksTbl;			//nrows = NumberOfFFTMagnImages * NumberOfSO2Directions, ncols = 1
	//temporaries
};


class acquisor
{
public:
	acquisor();
	~acquisor();

	void configure();

	//vector<float> window_coeff;
	//vector<float> reciprocal_pos;
	//vector<pair<int,araullo_res*>> res;
};

#endif
