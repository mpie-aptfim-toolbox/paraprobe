//##MK::GPLV3


#ifndef __PARAPROBE_SPATSTAT_CITEME_H__
#define __PARAPROBE_SPATSTAT_CITEME_H__

#include "CONFIG_Spatstat.h"


class CiteMeSpatstat
{
public:
	
	static vector<bibtex> Citations;
	
	static size_t cite();
};

#endif

