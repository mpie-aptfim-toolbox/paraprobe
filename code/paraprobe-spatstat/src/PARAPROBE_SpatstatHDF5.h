//##MK::GPLV3

#ifndef __PARAPROBE_SPATSTAT_HDF_H__
#define __PARAPROBE_SPATSTAT_HDF_H__

//shared headers
//#include "../../paraprobe-utils/src/PARAPROBE_APTH5.h"
//#include "../../paraprobe-utils/src/PARAPROBE_XDMF.h"
//##already included through utils/src/PARAPROBE_VolumeSampler.h

#include "../../paraprobe-utils/src/PARAPROBE_APTH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_UtilsMetadataDefsH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_SpatstatMetadataDefsH5.h"

#include "../../paraprobe-utils/src/PARAPROBE_PeriodicTable.h"

//tool-specific headers
//#include "CONFIG_Spatstat.h"
#include "PARAPROBE_SpatstatCiteMe.h"
/*
#include "METADATA_SpatstatDefsHDF5.h"
*/

class spatstat_h5 : public h5Hdl
{
	//tool-specific sub-class of a HDF5 inheriting all methods of the base class h5Hdl but adds tool specific read/write functions
	//coordinating instance handling all (sequential) writing to HDF5 file wrapping HDF5 low-level C library calls

public:
	spatstat_h5();
	~spatstat_h5();


	int create_spatstat_apth5( const string h5fn );

//private:
};


#endif
