//##MK::GPLV3

#include "PARAPROBE_SpatstatStructs.h"

ostream& operator << (ostream& in, ityp const & val) {
	in << (int) val.m_org << ";" << (int) val.m_rnd << "\n";
	return in;
}


ostream& operator << (ostream& in, ival const & val) {
	in << "[ " << val.incl_left << ", " << val.excl_right << " ]" << "\n";
	return in;
}


/*
ostream& operator << (ostream& in, p3dm2 const & val) {
	in << val.x << ";" << val.y << ";" << val.z << ";" << val.m_org << ";" << val.m_rnd << "\n";
	return in;
}
*/
