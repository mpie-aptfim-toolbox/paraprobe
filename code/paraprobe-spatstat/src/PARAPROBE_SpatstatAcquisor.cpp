//##MK::CODESPLIT

#include "PARAPROBE_SpatstatAcquisor.h"


spatstat_res::spatstat_res()
{
	owner = NULL;
	//ft.init_plan( ConfigAraullo::SDMBinExponent );
}


spatstat_res::~spatstat_res()
{
	//##MK::do not dereference the owner, only backreference!
}


/*
void araullo_res::plan( vector<p3dm1> const & cand )
{
	double mytic = omp_get_wtime();

	size_t accumulator = 0;
	//for each FFT magnitude spherical image to acquire/measure/compute sort the respective iontypes needed
	for( unsigned int img = 0; img < ConfigAraullo::NumberOfFFTMagnImages; img++ ) {
		iontype_ival tmp = iontype_ival( 0, 0, 0 );
		unsigned int iontype = ConfigAraullo::ProbeThisBinThisType.at(img).second; //bin is first, iontype is second
		for( auto it = cand.begin(); it != cand.end(); it++ ) {
			if ( it->m != iontype ) {
				continue;
			}
			else {
				DiffVecNeighbors.push_back( p3d(it->x - mp.x, it->y - mp.y, it->z - mp.z) );
				tmp.m++;
			}
		}
		tmp.incl_left = accumulator;
		tmp.excl_right = DiffVecNeighbors.size();
		accumulator += tmp.m;
		tmp.m = iontype;
		IV.push_back( tmp );

		#pragma omp critical
		{
			cout << "Image " << img << "\t[\t" << IV.back().incl_left << "\t" << IV.back().excl_right << "\t)\t" << IV.back().m << "\n";
		}
	}

	double mytoc = omp_get_wtime();
	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " plan created " << DiffVecNeighbors.size() << " in " << (mytoc-mytic) << " seconds" << "\n";
	}
}
*/


acquisor::acquisor()
{
	//cfg = binning_info();
}


acquisor::~acquisor()
{
	/*
	for( auto it = res.begin(); it != res.end(); it++ ) {
		if ( it->second != NULL ) {
			delete it->second;
			it->second = NULL;
		}
	}
	res = vector<pair<int,araullo_res*>>();
	*/
}


/*
void acquisor::configure()
{
	//for every point we need to scan through elevation and azimuth space, we generate 1D spatial distribution maps, and Fourier transform
	apt_real val = pow( static_cast<apt_real>(2.0), static_cast<apt_real>(ConfigAraullo::SDMBinExponent) );

	cfg.NumberOfBins = static_cast<unsigned int>(val + 0.5);
	cfg.NumberOfBinsHalf = static_cast<unsigned int>(ceil(0.5*static_cast<double>(cfg.NumberOfBins)));
	cfg.R = ConfigAraullo::ROIRadiusMax;

	//how much padding bins on either side of the histogram to append
	cfg.Rpadding = 1;
	cfg.dR = static_cast<apt_real>(2.0)*cfg.R / (static_cast<apt_real>(cfg.NumberOfBins) - static_cast<apt_real>(2*cfg.Rpadding));
	cfg.RdR = cfg.R + static_cast<apt_real>(cfg.Rpadding)*cfg.dR;
	cfg.binner = static_cast<apt_real>(cfg.NumberOfBins) /
			( static_cast<apt_real>(2.f)*cfg.R + static_cast<apt_real>(2.f)*static_cast<apt_real>(cfg.Rpadding)*cfg.dR );

	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << "\n" << cfg << "\n";
	}

	//precompute windowing coefficients
	//J. F. Kaiser, R. W. Schafer, 1980, On the use of the I_0-sinh window for spectrum analysis,
	//doi::10.1109/TASSP.1980.1163349
	window_coeff = vector<float>( cfg.NumberOfBins, 1.f );
	//window_coeff = vector<double>( NumberOfBins, 1.f );
	if ( ConfigAraullo::WindowingMethod == KAISER ) {
#ifdef UTILIZE_BOOST
		double alpha = ConfigAraullo::KaiserAlpha;
		double zero = 0.f;
		double I0a = boost::math::cyl_bessel_i( zero, alpha );

		for ( unsigned int i = 0; i < cfg.NumberOfBins; i++ ) {
			//w(n) modified Bessel function of first kind with shape parameter WindowingAlpha
			//w(n) = I_0(alpha*sqrt(1-(n-(N/2)/(N/2)))^2)) / I_0(alpha) for 0<= n <= N-1  otherwise w(n) = 0

			double nN = (static_cast<double>(i) - (static_cast<double>(cfg.NumberOfBins) / 2.0)) / (static_cast<double>(cfg.NumberOfBins) / 2.0);
			double x = alpha * sqrt( 1.0 - SQR(nN) );
			double I0x = boost::math::cyl_bessel_i( zero, x );

			window_coeff.at(i) = static_cast<float>(I0x / I0a); //high precision bessel then cut precision
//cout << setprecision(32) << "Computing modified Bessel function of first kind I0(v,x) coefficients " << i << "\t\t" << window_coeff.at(i) << "\n";
		}
//cout << "Kaiser windowing defined" << endl;
#else
		cout << "Kaiser windowing uses Boost to compute Bessel functions but Boost is currently not used so fallback to rectangular windowing" << "\n";
#endif
	}
	else { //E_RECTANGULAR_WINDOW
		window_coeff.at(0) = 0.f;
		window_coeff.back() = 0.f;
//cout << "Rectangular windowing defined" << endl;
	}

	//mapping of FFT bins to reciprocal frequency bins
	reciprocal_pos = vector<float>( cfg.NumberOfBinsHalf, 0.f);
	float NormalizerFreq = cfg.RdR / static_cast<float>(cfg.NumberOfBinsHalf);
	for( unsigned int i = 0; i < cfg.NumberOfBinsHalf; i++) {
		reciprocal_pos.at(i) = static_cast<float>(i) * NormalizerFreq;
	}

	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " configured" << "\n";
	}
}
*/


