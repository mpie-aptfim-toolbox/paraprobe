//##MK::CODESPLIT

#include "PARAPROBE_SpatstatHdl.h"


//define what is passed upon execution
#define SIMID									1
#define CONTROLFILE								2

//##MK::for querying system maximum physical memory see
//https://stackoverflow.com/questions/2513505/how-to-get-available-memory-c-g

#ifndef GITSHA									//otherwise we could not compile without adding -DGITSHA="d123" a dummy GitSha
	#define GITSHA								0
#endif
//stringification of strings passed from cmake at compile time
//https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html
#define xstr(s)									str(s)
#define str(s) 									#s


void hello( void )
{
	cout << "paraprobe-spatstat" << "\n";
	cout << xstr(GITSHA) << "\n";
	cout << "Collecting " << CiteMeSpatstat::cite() << " publications for the tool to cite:" << "\n";
	for( auto it = CiteMeSpatstat::Citations.begin(); it != CiteMeSpatstat::Citations.end(); it++ ) {
		cout << it->author << "\n";
		cout << it->title << "\n";
		cout << it->journal << ", " << it->year << ", " << it->vol << ", pp" << it->pages << "\n";
		cout << it->doi << "\n";
	}
}


bool init(  int pargc, char** pargv )
{
	ConfigShared::SimID = stoul( pargv[SIMID] );
	try {
		ConfigSpatstat::readXML(pargv[CONTROLFILE]);
	}
	catch (exception& e) {
		cerr << "\n" << "Unable to parse control file! Details:" << "\n" << e.what() << "\n"; return false;
	}
	if ( ConfigSpatstat::checkUserInput() == false ) {
		cerr << "\n" << "Control file settings failed the validity check!" << "\n"; return false;
	}
	else {
		cout << "\n" << "\t\tInput is valid under SimulationID = " << "SimID." <<  ConfigShared::SimID << "\n";
	}
	cout << endl;
	return true;
}


void descriptive_spatial_and_twopoint_statistics_on_reconstruction( const int r, const int nr, char** pargv )
{
	//allocate process-level instance of a spatstatHdl. The instance handles all process-internal
	//processing, the instances synchronize across and communicate with each other during execution
	spatstatHdl* spst = NULL;

	double ttic = MPI_Wtime();

	int localhealth = 1;
	int globalhealth = nr;
	try {
		spst = new spatstatHdl;
		spst->set_myrank(r);
		spst->set_nranks(nr);
		spst->init_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Unable to allocate an spatstatHdl class object instance" << "\n"; localhealth = 0;
	}

	if ( spst->read_periodictable() == true ) {
		cout << "Rank " << r << " reads PSE successfully rng.nuclides.isotopes.size() " << spst->rng.nuclides.isotopes.size() << "\n";
	}
	else {
		cerr << "Rank " << r << " loading of PeriodicTableOfElements failed!" << "\n"; localhealth = 0;
	}

	string xmlfn = pargv[CONTROLFILE];
	if ( spst->read_iontype_combinations( xmlfn ) == true ) {
		cout << "Rank " << r << " reads IontypeCombinations successfully " << "\n";
	}
	else {
		cerr << "Rank " << r << " loading of IontypeCombinations failed!" << "\n"; localhealth = 0;
	}

	//do we have all processes on board?
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete spst; spst = NULL; return;
	}
	
	double ttoc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	spst->spatstat_tictoc.prof_elpsdtime_and_mem( "ReadXMLParameter", APT_XX, APT_IS_PAR, mm, ttic, ttoc );
	ttic = MPI_Wtime();

	//we have all on board, now read xyz, ranging, possibly precomputed distance information for all ions
	//MK::we could have all processes loading from the same file, or have only MASTER do so and broadcast, we opt for the latter strategy
	//##MK::should not be a problem because initialization is orders of magnitude cheaper than actual calculations...
	if ( spst->get_myrank() == MASTER ) {
		if ( spst->read_reconxyz_from_apth5() == true ) {
			cout << "MASTER read successfully all ion coordinates" << "\n";
		}
		else {
			cerr << "MASTER was unable to read ion coordinates !" << "\n"; localhealth = 0;
		}
		if ( spst->read_ranging_from_apth5() == true ) {
			cout << "MASTER read successfully ranging information" << "\n";
		}
		else {
			cerr << "MASTER was unable to read ranging information !" << "\n"; localhealth = 0;
		}
		if ( spst->read_itypes_from_apth5() == true ) {
			cout << "MASTER read successfully iontypes for xyz coordinates" << "\n";
		}
		else {
			cerr << "MASTER was unable to read iontypes !" << "\n"; localhealth = 0;
		}
		if ( ConfigSpatstat::ROIVolumeInsideOnly == true ) {
			if ( spst->read_dist2hull_from_apth5() == true ) {
				cout << "MASTER read successfully all ion distance to triangularized dataset boundary!" << "\n";
			}
			else {
				cerr << "MASTER was unable to read ion distance values!" << "\n"; localhealth = 0;
			}
			if ( spst->xyz.size() != spst->dist2hull.size() ) {
				cerr << "MASTER found that not for every position there is at all or only one distance value!" << "\n"; localhealth = 0;
			}
		}
		else {
			cout << "WARNING:: MASTER defines as default very large dist2hull values to use for evaluating where ions are considered!" << "\n";
			cout << "WARNING:: MASTER does so because ROIVolumeInsideOnly == 0 so spatial statistics may be biased!" << "\n";
			try {
				spst->dist2hull = vector<apt_real>( spst->xyz.size(), F32MX );
			}
			catch (bad_alloc &mecroak) {
				cerr << "MASTER allocation of dist2hull failed!"; localhealth = 0;
			}
		}
	}
	//else {} //slaves processes wait
	ttoc = MPI_Wtime();
	mm = spst->spatstat_tictoc.get_memoryconsumption();
	spst->spatstat_tictoc.prof_elpsdtime_and_mem( "ReadMaterialsDataAPTH5", APT_XX, APT_IS_SEQ, mm, ttic, ttoc );
	ttic = MPI_Wtime();
	//necessary? second order issue wrt to performance for as few as 80 processes like on TALOS...?
	MPI_Barrier( MPI_COMM_WORLD );

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that all data have arrived!" << "\n";
		delete spst; spst = NULL; return;
	}

	if ( spst->broadcast_reconxyz() == true ) {
		cout << "Rank " << r << " has synchronized reconstruction" << "\n";
	}
	else {
		cerr << "Rank " << r << " failed to synchronize reconstruction" << "\n"; localhealth = 0;
	}
	if ( spst->broadcast_ranging() == true ) {
		cout << "Rank " << r << " has synchronized ranging" << "\n";
	}
	else {
		cerr << "Rank " << r << " failed to synchronize ranging" << "\n"; localhealth = 0;
	}
	if ( spst->broadcast_distances() == true ) { //GB/s bidirectional bandwidth one 4B float per ion, so at most 8 GB ~ a few seconds...
	 	cout << "Rank " << r << " has synchronized distances" << "\n";
	}
	else {
	 	cerr << "Rank " << r << " failed to synchronized distances" << "\n"; localhealth = 0;
	}

	ttoc = MPI_Wtime();
	mm = spst->spatstat_tictoc.get_memoryconsumption();
	spst->spatstat_tictoc.prof_elpsdtime_and_mem( "BroadcastMaterialsData", APT_XX, APT_IS_PAR, mm, ttic, ttoc );

	MPI_Barrier( MPI_COMM_WORLD );

	if ( spst->define_analysis_tasks() == true ) {
		cout << "Rank " << r << " has defined the analysis tasks successfully" << "\n";
	}
	else {
		cout << "Rank " << r << " detected inconsistencies during defining analysis tasks!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes have synchronized the ion coordinates, ranging and possible distances!" << "\n";
		delete spst; spst = NULL; return;
	}

	//PRNG, same deterministic order of the ions and range information + same deterministic seed and PRNG algo generates same permutation across processes
	spst->randomize_iontype_labels();

	//now processes can proceed and process massively parallel and independently
	//MK::as M. M. Padwary et al. have shown ideally the processes build a globally distributed tree
	//in APT it is freely the case though that large radii are used, here Lu et al. 2018 have shown that the tree structures
	//eventually do at some point provide no longer any improvements because anyway a query needs to inspect almost all ions
	//well even more so an argument to execute the queries only once!
	//for APT it is as of 2019/2020 unrealistic that larger than 2 billion ion tips will become in the near future
	//a frequent use case, rather the most use cases handle 10 - 200 million ions per tip for such tips
	//however we can life for now with the solution to have a duplicate of the entire tip, this cost more memory but 
	//typically spatial partitioning on threads and tree build take only a few percent of total execution time
	//anyway in the future we could always follow Padwarys approach which was documented to work for trillion ion point clouds
	//another argument for having each process a full copy is dynamic workpartitioning
	//preliminary studies with well balanced statically partitioned point cloud slices along tip main axis have shown to suffer
	//from substantial load imbalance (20-50% total time increase than dynamic), this is because of dissimilar workload per ion 
	//in different portions of the sample therefore even though nominally a process has the same number of ions than another
	//it may end up with fewer work, strongly dependent on querying case, and spatial distribution of ion types queried in the
	//specimen 
	
	//MK::each process computes the same grid and sets the same deterministic a priori known work partitioning for now
	spst->itype_sensitive_spatial_decomposition();

    //ions are distributed round robin to the processes because number of processes (~0.1k-1k) << number of ions 1000k, typically
	spst->prepare_processlocal_resultsbuffer();

	spst->execute_local_workpackage();

cout << "Rank " << r << " has completed its workpackage" << "\n";
	MPI_Barrier(MPI_COMM_WORLD);

	if ( spst->get_myrank() == MASTER ) {
		if ( spst->init_target_file() == true ) {
			cout << "Rank MASTER successfully initialized APTH5 results file" << "\n";
		}
		else {
			cerr << "Rank MASTER unable to initialize results APTH5 file" << "\n"; localhealth = 0;
		}
		if ( spst->write_environment_and_settings() == true ) {
			cout << "Rank " << MASTER << " environment and settings written!" << "\n";
		}
		else {
			cerr << "Rank " << MASTER << " writing environment and settings failed!" << "\n"; localhealth = 0;
		}
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes have synchronized after generation of the target file!" << "\n";
		delete spst; spst = NULL; return;
	}

	MPI_Barrier( MPI_COMM_WORLD ); //reduce individual process-local histogram values and SDM to a world histogram and SDMs...

	if ( spst->collect_results_on_masterprocess() == true ) {
		if ( spst->get_myrank() == MASTER ) {
			cout << "Rank MASTER successfully collected description information on the material points" << "\n";
		}
	}
	else {
		cerr << "Rank " << r << " unable to provide descriptive information on material points to the MASTER!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes participated in collection of descriptive information on material points!" << "\n";
		delete spst; spst = NULL; return;
	}

	if ( spst->get_myrank() == MASTER ) {
		if ( spst->write_materialpoints_to_apth5() == true ) { //does not require MPI communication all results already on the master
			cout << "Rank MASTER successfully wrote all materialpoints to an APTH5 file" << "\n";
		}
		else {
			cerr << "Rank MASTER unable to write all materialpoints to the APTH5 file" << "\n"; localhealth = 0;
		}
	}

/*
	double ttoc = MPI_Wtime();
	memsnapshot mm = spst->spatstat_tictoc.get_memoryconsumption();
	//spst->spatstat_tictoc.prof_elpsdtime_and_mem( "InitializeH5ResultsFile", APT_XX, APT_IS_SEQ, mm, ttic, ttoc);

	ttic = MPI_Wtime();

	
	ttoc = MPI_Wtime();
	mm = spst->spatstat_tictoc.get_memoryconsumption();
	spst->spatstat_tictoc.prof_elpsdtime_and_mem( "WritingH5ResultsFile", APT_XX, APT_IS_SEQ, mm, ttic, ttoc);
*/
	spst->spatstat_tictoc.spit_profiling( "Spatstat", ConfigShared::SimID, spst->get_myrank() );

	//release resources
	delete spst; spst = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();

	hello();
//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	//helloworld( argc, argv );

	if ( init( argc, argv ) == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks!" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else { 
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}
	cout << r << "-th MPI process initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";
	
//EXECUTE SPECIFIC TASK
	descriptive_spatial_and_twopoint_statistics_on_reconstruction( r, nr, argv );
	
//DESTROY MPI
	//##MK::output profiling results

	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	cout << "Rank " << r << " left MPI_COMM_WORLD deconstructed" << "\n";

	double toc = omp_get_wtime();
	cout << "paraprobe-spatstat took " << (toc-tic) << " seconds wall-clock time in total" << endl;
	return 0;
}


/*
	unsigned int a = 0;
	unsigned int b = 1;
	unsigned int c = 2;
	unsigned int d = 3;
	unsigned int nrr = 2;
	unsigned int amod = a % nrr;
	unsigned int bmod = b % nrr;
	unsigned int cmod = c % nrr;
	unsigned int dmod = d % nrr;
	cout << amod << "\n";
	cout << bmod << "\n";
	cout << cmod << "\n";
	cout << dmod << "\n";
	return 0;
*/

