# -*- coding: utf-8 -*-
"""
created 2020/08/03, Markus K\"uhbach, m.kuehbach@mpie.de
utility class for creating configuration of customized build scripts for paraprobe via Jupyter/Python
"""

import os
import sys
#sys.path.append('../src/')
import glob, subprocess
import pandas as pd
import numpy as np

"""
examples how to define commands associated with loading environment variables
for computer clusters
"""

envmod_mpi30 = { 'pre': ['module purge', 'module list'],
                 'gnu': ['module load Compilers/GCC/GCC-7.5.0', 'module load MPI/GCC/MPICH-GCC-7.5.0'], 
                 'itl': ['module load Compilers/Intel/Intel2018', 'module load MPI/Intel/IntelMPI2018'],
                 'pgi': ['module load Compilers/PGI/PGI-19.4', 'module load Compilers/PGI/OpenMPI-PGI-19.4'],
                 'post': ['module list'] }

envmod_talos = { 'pre': ['module purge', 'module list', 'module load cmake'],
                 'gnu': ['module load gcc', 'module load impi'], 
                 'itl': ['module load intel', 'module load impi'],
                 'pgi': ['module load pgi',],
                 'post': ['module list'] }

envmod_mypc = { 'pre': [], 
                'gnu': [],
                'itl': [],
                'pgi': [],
                'post': []  }

class paraprobe_tool():
    """
    abstract template for a paraprobe tool
    """
    def __init__(self, nm, *args, **kwargs):
        self.toolname = nm
        self.compile = False
    
    def activate(self):
        self.compile = True
        
    def deactivate(self):
        self.compile = False
        
    def get_name(self):
        return 'paraprobe_' + self.toolname

tools = { 'utils': paraprobe_tool('utils'),
          'transcoder': paraprobe_tool('transcoder'),
          'synthetic': paraprobe_tool('synthetic'),
          'reconstruct': paraprobe_tool('reconstruct'),
          'ranger': paraprobe_tool('ranger'),
          'surfacer': paraprobe_tool('surfacer'),
          'tessellator': paraprobe_tool('tessellator'),
          'nanochem': paraprobe_tool('nanochem'),
          'spatstat': paraprobe_tool('spatstat'),
          'dbscan': paraprobe_tool('dbscan'),
          'araullo': paraprobe_tool('araullo'),
          'fourier': paraprobe_tool('fourier'),
          'indexer': paraprobe_tool('indexer'),
          'intersector': paraprobe_tool('intersector')  }

class paraprobe_configwizard():
    """
    a utility class implementing functionality to create a 
    PARAPROBE.Build.UserConfiguration.sh bash script for compiling the tools
    """
    def __init__(self, *args, **kwargs):
        self.tools = tools
        self.tools['utils'].activate()
        self.opts = {}
        self.opts['BUILDTYPE'] = 'clean'
        self.opts['COPYEXEC'] = True
        self.opts['COMPILER'] = 'gnu'
        self.opts['ENVMOD_BOOL'] = False
        self.opts['ENVMOD_PRE'] = ['module purge', 'module list']
        self.opts['ENVMOD_WHICH'] = []
        self.opts['ENVMOD_POST'] = ['module list']
        self.opts['GITSHA_BOOL'] = True
        self.header = r'#!bin/bash' + '\n'
        
    def set_buildtype_clean(self):
        """
        if activated will always clean build folder, will take longer but helps resolving cmake issues
        """
        self.opts['BUILDTYPE'] = 'clean'
        
    def set_buildtype_fast(self):
        """
        if activated will not clean build folder, so cmake will check if files were already compiled
        and can skip these, will make build substantially faster especial when minor code changes and
        code changes not in paraprobe-utils
        """
        self.opts['BUILDTYPE'] = 'fast'
              
    def set_home_project(self, fullpath_to_projectroot):
        self.opts['HOME'] = fullpath_to_projectroot
    
    def set_home_hdf5(self, fullpath_to_hdf5root):
        self.opts['HDF5'] = fullpath_to_hdf5root
        
    def set_home_cgal(self, fullpath_to_cgalroot):
        self.opts['CGAL'] = fullpath_to_cgalroot
        
    def set_modules_use(self, arr):
        if len(arr) >= 1:
            self.opts['ENVMOD_BOOL'] = True
            self.opts['ENVMOD_WHICH'] = arr
            
    def set_compiler_gnu(self):
        self.opts['COMPILER'] = 'gnu'
        
    def set_compiler_intel(self):
        self.opts['COMPILER'] = 'itl'
        
    def set_compiler_pgi(self):
        self.opts['COMPILER'] = 'pgi'

    def set_code_version(self):
        self.opts['GITSHA_BOOL'] = True            
        
    def set_tools_none(self):
        self.tools = tools
        self.tools['utils'].activate()
        
    def set_tools_basic(self):
        self.set_tools_none()
        self.tools['utils'].activate()
        self.tools['transcoder'].activate()
        self.tools['synthetic'].activate()
        #self.tools['reconstruct'].activate()
        self.tools['ranger'].activate()
        self.tools['surfacer'].activate()
        self.tools['tessellator'].activate()
        #self.tools['nanochem'].activate()
        self.tools['spatstat'].activate()
        self.tools['dbscan'].activate()
        #self.tools['araullo'].activate()
        #self.tools['fourier'].activate()
        #self.tools['indexer'].activate()
        
    def set_tools_all(self):
        self.set_tools_none()
        self.tools['utils'].activate()
        self.tools['transcoder'].activate()
        self.tools['synthetic'].activate()
        #self.tools['reconstruct'].activate()
        self.tools['ranger'].activate()
        self.tools['surfacer'].activate()
        self.tools['tessellator'].activate()
        #self.tools['nanochem'].activate()
        self.tools['spatstat'].activate()
        self.tools['dbscan'].activate()
        self.tools['araullo'].activate()
        self.tools['fourier'].activate()
        self.tools['indexer'].activate()
              
    #activation and deactivation of individual tools
    def set_tools_intersector_activate(self):
        self.tools['intersector'].activate()
        
    def set_tools_surfacer_deactivate(self):
        self.tools['surfacer'].deactivate()
        
    def set_tools_tessellator_deactivate(self):
        self.tools['tessellator'].deactivate()

    #def set_tools_nanochem_deactivate(self):
    #    self.tools['nanochem'].deactivate()

    def set_tools_spatstat_deactivate(self):
        self.tools['spatstat'].deactivate()
        
    def set_tools_dbscan_deactivate(self):
        self.tools['dbscan'].deactivate()
        
    def set_tools_araullo_deactivate(self):
        self.tools['araullo'].deactivate()
        
    def set_tools_fourier_deactivate(self):
        self.tools['fourier'].deactivate()
        
    def set_tools_indexer_deactivate(self):
        self.tools['indexer'].deactivate()
        
    def set_tools_intersector_deactivate(self):
        self.tools['intersector'].deactivate()
        
    def write(self):
        self.fn = 'PARAPROBE.Build.UserConfiguration.sh'
        self.txt = self.header
        self.txt += '\n'
        self.txt += r'#which tools to compile?' + '\n'
        for key, val in self.tools.items():
            self.txt += 'build_' + str(key) + '=' + str(np.uint32(val.compile)) + '\n'
        self.txt += '\n'
        self.txt += r'#always clear the build directory prior configurating and compilating?' + '\n'
        if self.opts['BUILDTYPE'] == 'clean':
            self.txt += r'clean_build=1' + '\n'
        elif self.opts['BUILDTYPE'] == 'fast':
            self.txt += r'clean_build=0' + '\n'
        else:
            raise ValueError('BUILDTYPE should be either clean or fast !')
        self.txt += r'#collect executables after successful build of a tool?' + '\n'
        if self.opts['COPYEXEC'] == True:
            self.txt += r'collect_executables=1' + '\n'
        else:
            self.txt += r'collect_executables=0' + '\n'
        self.txt += r'#what are the target folders where are libraries?' + '\n'
        self.txt += r'target_home="' + self.opts['HOME'] + '"' + '\n'
        self.txt += r'hdf5_home="' + self.opts['HDF5'] + '"' + '\n'
        self.txt += r'cgal_home="' + self.opts['CGAL'] + '"' + '\n'
        self.txt += r'MYHDF5HOME=$hdf5_home' + '\n'
        self.txt += r'MYCGALHOME=$cgal_home' + '\n'
        self.txt += r'#check code version integrate GitSHA?' + '\n'
        if self.opts['GITSHA_BOOL'] == True:
            self.txt += r'check_codeversion=1' + '\n'
        else:
            self.txt += r'check_codeversion=0' + '\n'
        #integrate above into handling of options into a looping over a dict
        self.txt += r'#working with environment modules?' + '\n'
        if self.opts['ENVMOD_BOOL'] == True:
            self.txt += r'modules_used=1' + '\n'
        else:
            self.txt += r'modules_used=0' + '\n'
        self.txt += r'#which compiler to use (gnu, itel, pgi)?' + '\n'
        self.txt += r'compiler_which="' + self.opts['COMPILER'] + '"' + '\n'
        self.txt += r'echo "Preparing environment"' + '\n'
        if self.opts['ENVMOD_BOOL'] == True:
            for i in self.opts['ENVMOD_PRE']:
                self.txt += i + '\n'
            for i in self.opts['ENVMOD_WHICH']:
                self.txt += i + '\n'
            for i in self.opts['ENVMOD_POST']:
                self.txt += i + '\n'
        self.txt += r'#define alias names for the compilers and prep them' + '\n'
        if self.opts['COMPILER'] == 'gnu':
            self.txt += 'MYWHO_COMPILER=GNU' + '\n'
            self.txt += 'MYCCC_COMPILER=gcc' + '\n'
            self.txt += 'MYCXX_COMPILER=g++' + '\n'
        elif self.opts['COMPILER'] == 'itl':
            self.txt += 'MYWHO_COMPILER=ITL' + '\n'
            self.txt += 'MYCCC_COMPILER=icc' + '\n'
            self.txt += 'MYCXX_COMPILER=icpc' + '\n'
        elif self.opts['COMPILER'] == 'PGI':
            self.txt += 'MYWHO_COMPILER=PGI' + '\n'
            self.txt += 'MYCCC_COMPILER=pgcc' + '\n'
            self.txt += 'MYCXX_COMPILER=pg++' + '\n'
        else:
            raise ValueError('Unknown compiler !')
        self.txt += r'echo "Compiler choice $MYWHO_COMPILER"' + '\n'
        self.txt += r'echo "C compiler $MYCCC_COMPILER"' + '\n'
        self.txt += r'echo "C++ compiler $MYCXX_COMPILER"' + '\n'
        self.txt += r'echo "PARAPROBE HOME ' + self.opts['HOME'] + r'"' + '\n'
        self.txt += r'echo "HDF5 HOME ' + self.opts['HDF5'] + r'"' + '\n'        
        self.txt += r'echo "CGAL HOME ' + self.opts['CGAL'] + r'"' + '\n'
        self.txt += '\n'
        self.txt += r'echo "Identifying the paraprobe version"' + '\n'
        if self.opts['GITSHA_BOOL'] == True:
            self.txt += r'MYGITSHA="$(git describe --abbrev=50 --dirty --broken --always --tags)"' + '\n'
        else:
            self.txt += r'MYGITSHA="unknown"' + '\n'
        self.txt += r'echo $MYGITSHA' + '\n'
        self.txt += '\n'
        self.txt += r'echo "Compiling paraprobe-utils..."' + '\n'
        self.txt += r'mkdir -p paraprobe-utils/build' + '\n'
        self.txt += r'cd paraprobe-utils/build' + '\n'
        self.txt += r'if [ "$clean_build" == 1 ]; then' + '\n'
        self.txt += '\t' + r'echo "Cleaning $PWD"' + '\n'
        self.txt += '\t' + r'rm -rf *' + '\n'
        self.txt += r'fi' + '\n'
        self.txt += r'echo "Configuring paraprobe-utils..."' + '\n'
        self.txt += r'cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER .. 1>PARAPROBE.Utils.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Utils.CMake.$MYWHO_COMPILER.STDERR.txt' + '\n'
        self.txt += r'echo "Compiling paraprobe-utils..."' + '\n'
        self.txt += r'make 1>PARAPROBE.Utils.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Utils.Make.$MYWHO_COMPILER.STDERR.txt' + '\n'
        self.txt += r'#DO NOT DELETE THESE GENERATED FILES below TOOLS WILL INCLUDE SOME OF THEM' + '\n'
        self.txt += r'cd ../../' + '\n'
        for tl in self.tools.keys():
            if not tl == 'utils':
                if not tl == 'intersector':
                    self.txt += r'if [ "$build_' + tl + r'" == 1 ]; then' + '\n'
                    self.txt += '\t' + r'echo "Compiling paraprobe-' + tl + r'..."' + '\n'
                    self.txt += '\t' + r'mkdir -p paraprobe-' + tl + r'/build' + '\n'
                    self.txt += '\t' + r'cd paraprobe-' + tl + r'/build' + '\n'
                    self.txt += '\t' + r'if [ "$clean_build" == 1 ]; then' + '\n'
                    self.txt += '\t' + '\t' + r'echo "Cleaning $PWD"' + '\n'
                    self.txt += '\t' + '\t' + r'rm -rf *' + '\n'
                    self.txt += '\t' + r'fi' + '\n'
                    self.txt += '\t' + r'echo "Configuring paraprobe-' + tl + r'..."' + '\n'
                    if tl == 'surfacer':
                        self.txt += '\t' + r'cmake -DCMAKE_BUILD_TYPE=Release -DCGAL_DIR=$MYCGALHOME -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.' + tl.capitalize() + r'.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.' + tl.capitalize() + r'.CMake.$MYWHO_COMPILER.STDERR.txt' + '\n'
                    else:
                        self.txt += '\t' + r'cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.' + tl.capitalize() + r'.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.' + tl.capitalize() + r'.CMake.$MYWHO_COMPILER.STDERR.txt' + '\n'
                    self.txt += '\t' + r'echo "Compiling paraprobe-' + tl + r'..."' + '\n'
                    self.txt += '\t' + r'make 1>PARAPROBE.' + tl.capitalize() + r'.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.' + tl.capitalize() + r'.Make.$MYWHO_COMPILER.STDERR.txt' + '\n'
                    self.txt += '\t' + r'chmod +x paraprobe_' + tl + '\n'
                    #self.txt += '\t' + r'cp paraprobe_' + tl + r' ../run/' + '\n'
                    self.txt += '\t' + r'if [ "$collect_executables" == 1 ]; then' + '\n'
                    self.txt += '\t' + '\t' + r'cp paraprobe_' + tl + r' $target_home/paraprobe_' + tl + '\n'
                    self.txt += '\t' + r'fi' + '\n'
                    self.txt += '\t' + r'cd ../../' + '\n'
                    self.txt += r'fi' + '\n'
                else: #intersector
                    self.txt += r'if [ "$build_intersector" == 1 ]; then' + '\n'
                    self.txt += '\t' + r'echo "Compiling TetGen library..."' + '\n'
                    self.txt += '\t' + r'cd paraprobe-intersector/src/thirdparty/TetGen/tetgen1.5.1' + '\n'
                    self.txt += '\t' + r'rm *.o *.a' + '\n'
                    self.txt += '\t' + r'make CC=$MYCCC_COMPILER CXX=$MYCXX_COMPILER tetlib 1>TetGen.Make.$MYWHO_COMPILER.STDOUT.txt 2>TetGen.Make.$MYWHO_COMPILER.STDERR.txt' + '\n'
                    self.txt += '\t' + r'cd ../../../../../' + '\n'
                    self.txt += '\t' + r'echo "Compiling paraprobe-intersector..."' + '\n'
                    self.txt += '\t' + r'mkdir -p paraprobe-intersector/build' + '\n'
                    self.txt += '\t' + r'cd paraprobe-intersector/build' + '\n'
                    self.txt += '\t' + r'if [ "$clean_build" = true ]; then' + '\n'
                    self.txt += '\t' + '\t' + r'echo "Cleaning $PWD"' + '\n'
                    self.txt += '\t' + '\t' + r'rm -rf *' + '\n'
                    self.txt += '\t' + r'fi' + '\n'
                    self.txt += '\t' + r'cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Intersector.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Intersector.CMake.$MYWHO_COMPILER.STDERR.txt' + '\n'
                    self.txt += '\t' + r'make 1>PARAPROBE.Intersector.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Intersector.Make.$MYWHO_COMPILER.STDERR.txt' + '\n'
                    self.txt += '\t' + r'chmod +x paraprobe_intersector' + '\n'
                    #self.txt += '\t' + r'cp paraprobe_indexer ../run/' + '\n'
                    self.txt += '\t' + r'if [ "$collect_executables" = true ]; then' + '\n'
                    self.txt += '\t' + '\t' + r'cp paraprobe_intersector $target_home/paraprobe_intersector' + '\n'
                    self.txt += '\t' + r'fi' + '\n'
                    self.txt += '\t' + r'cd ../../' + '\n'
                    self.txt += r'fi' + '\n'
                    self.txt += '\n'
                    self.txt += r'#add additional tools' + '\n'

        with open(self.fn, 'w') as f:
             f.write(self.txt)


##a = paraprobe_configwizard()
##a.set_buildtype_clean()
##a.set_home_project('/home/markus/ParaprobeTraining/paraprobe/code/')
##a.set_home_hdf5('/home/markus/ParaprobeTraining/paraprobe/code/thirdparty/HDF5')
##a.set_home_cgal('/home/markus/ParaprobeTraining/paraprobe/code/thirdparty/CGAL/CGAL-4.11.3/')
##a.set_modules_use( envmod_talos['gnu'] )
##a.set_compiler_gnu()
##a.set_code_version()
##a.set_tools_all()
##a.write()
