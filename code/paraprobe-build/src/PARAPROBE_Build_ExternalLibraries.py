# -*- coding: utf-8 -*-
"""
created 2020/08/03, Markus K\"uhbach, m.kuehbach@mpie.de
utility class for creating customized build scripts for paraprobe from Jupyter/Python
"""

import os
import sys
#sys.path.append('../src/')
import glob, subprocess
import pandas as pd
import numpy as np

class paraprobe_options_cmake():
    """
    a utility class implementing functionality to create a 
    PARAPROBE.ExternalLibraries.cmake file, essentially an Options file for cmake
    it holds user-customizations, and locations of user-specific local libraries
    """
    def __init__(self, machinename, *args, **kwargs):
        self.opts = {}
        self.opts['NAME'] = machinename
        self.header = r'cmake_minimum_required(VERSION 3.10)' + '\n'
        #self.header += r'################################################################################################################################' + '\n'
        #self.header += r'##USER INTERACTION##############################################################################################################' + '\n'
        #self.header += r'##in this section modifications and local paths need to be modified by the user#################################################' + '\n'
        #self.header += r'################################################################################################################################' + '\n'
        #self.target = r''
        #self.prjpath = r''
        #self.extlibs = r''
        #self.utils = r'#automatically define where to find the utility tools, given the directory structure' + '\n'
        #self.utils += r'set(MYUTILSPATH "${MYPROJECTPATH}/paraprobe-utils/build/CMakeFiles/paraprobe-utils.dir/src/")' + '\n'
        
    def set_home_project(self, fullpath_to_projectroot):
        self.opts['HOME'] = fullpath_to_projectroot
    
    def set_home_hdf5(self, fullpath_to_hdf5root):
        self.opts['HDF5'] = fullpath_to_hdf5root
        
    def set_home_cgal(self, fullpath_to_cgalroot):
        self.opts['CGAL'] = fullpath_to_cgalroot
         
    def write(self):
        self.fn = 'PARAPROBE.ExternalLibraries.cmake'
        self.txt = self.header
        self.txt += '\n'
        self.txt += r'#give the name of your computer' + '\n'
        self.txt += r'set(' + self.opts['NAME'] + ' ON)' + '\n'
        self.txt += '\n'
        self.txt += r'#define the full path to the top level directory with local PARAPROBE' + '\n'
        self.txt += r'set(MYPROJECTPATH "' + self.opts['HOME'] + '")' + '\n'
        self.txt += '\n'
        self.txt += r'#define the full path to the top level directory with local HDF5' + '\n'
        self.txt += r'set(MYHDFPATH "' + self.opts['HDF5'] + '")' + '\n'
        self.txt += '\n'
        #self.txt += r'#define the full path to the top level directory with local CGAL' + '\n'
        #self.txt += r'set(MYCGALPATH "' + self.opts['CGAL'] + '")' + '\n'
        #self.txt += '\n'
        self.txt += r'#where to find the compiled utils object in the paraprobe directory structure' + '\n'
        self.txt += r'set(MYUTILSPATH "${MYPROJECTPATH}/paraprobe-utils/build/CMakeFiles/paraprobe-utils.dir/src/")' + '\n'
        with open(self.fn, 'w') as f:
             f.write(self.txt)

##mypc
#cm = paraprobe_options_cmake('MYPC')
#cm.set_home_project('/home/markus/ParaprobeTraining/paraprobe/code/')
#cm.set_home_hdf5('/home/markus/ParaprobeTraining/paraprobe/code/thirdparty/HDF5/CMake-hdf5-1.10.6/HDF5-1.10.6-Linux/HDF5_Group/HDF5/1.10.6')
##cm.set_home_cgal('/home/markus/ParaprobeTraining/paraprobe/code/thirdparty/CGAL/CGAL-4.11.3')
#cm.write()

##mpi30
#cm = paraprobe_options_cmake('MPI30')
#cm.set_home_project('/home/m.kuehbach/Paper18/code/')
#cm.set_home_hdf5('/home/m.kuehbach/Maintain/paraprobe/src/thirdparty/HDF5/CMake-hdf5-1.10.2/build/HDF5-1.10.2-Linux/HDF_Group/HDF5/1.10.2')
##cm.set_home_cgal('')
#cm.write()

##talos
#cm = paraprobe_options_cmake('TALOS')
#cm.set_home_project('/talos/scratch/mkuehbac/Paper18/MPIE_APTFIM_TOOLBOX/paraprobe/code/')
#cm.set_home_hdf5('/talos/u/mkuehbac/HDF5/CMake-hdf5-1.10.2/build/HDF5-1.10.2-Linux/HDF_Group/HDF5/1.10.2')
##cm.set_home_cgal('')
#cm.write()
