# -*- coding: utf-8 -*-
"""
created 2020/05/28, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for strongly automatized reporting
of results from the paraprobe-araullo tool
"""

import os, sys
import glob, subprocess
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
try:
    import periodictable as pse
except ImportError as e:
    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')
try:
    import h5py
except ImportError as e:
    raise ValueError('Install h5py Python package via e.g. pip install h5py !')

from PARAPROBE_Autoreporter_Numerics import *
from PARAPROBE_Autoreporter_CorpDesign import *
from PARAPROBE_Autoreporter_DecodeIontypeHash import *
from PARAPROBE_Autoreporter_Plots import *
#from PARAPROBE_Autoreporter_MetadataTranscoder import *
#from PARAPROBE_Autoreporter_MetadataSurfacer import *
#from PARAPROBE_Autoreporter_MetadataSpatstat import *
from PARAPROBE_Autoreporter_MetadataAraullo import *

#user-defined plots

class araullo_profiling_file():
    def __init__(self, *args, **kwargs):
        self.metadata = {}
        
    def add_metadata_rank(self, csvfn, *args, **kwargs):
        """
        read out meta data about profiling from a paraprobe-araullo CSV results file
        csvfn the CSV filename
        """
        fn = csvfn #'C:/Users/kaiob/eclipse-workspace/mpie-aptfim-toolbox/paraprobe-autoreporter/BenchmarkPaper18/PARAPROBE.Araullo.SimID.0.Rank.0.DetailedProfiling.csv'
        csv = pd.read_csv(fn, delimiter=';', header=2)
        csv = csv.drop(['ThreadID','Project','FFT','FindPks'], axis=1)
        #find automatically how many phases were stored in this file
        phcand = csv['PHCANDID'].unique()
        for cand in phcand:
            key = 'PH' + str(cand)
            if not key in self.metadata.keys():
                self.metadata[key] = {} #create a new dictionary for results specific for this phase
            
            #filter out all values for phaseID phcand
            filtered = csv[csv['PHCANDID'] == cand].drop(['PHCANDID'], axis=1)
            self.metadata[key] = filtered.set_index('MPID').T.to_dict('list')
            del filtered

config_araullo = {}

class autoreporter_araullo():
    
    def __init__(self, simid, *args, **kwargs):
        self.simid = simid
        self.toolname = 'Araullo'
        self.h5src_ara = 'PARAPROBE.Araullo.Results.SimID.' + str(self.simid) + '.h5'
                
        #add here all functions which parse by default results from the source
        self.config = config_araullo
        self.results = {}
        self.get_tool_config()
        self.results['KeyQuantiles'] = {}
        self.results['Histogram'] = {}
        self.results['FFTMagn'] = {}
        self.results['SpecificPeak'] = {}

    def get_tool_config(self):
        fn = self.h5src_ara
        hf = h5py.File( fn, 'r' )
        self.config = config_araullo
        #to read all entries use
        grpnm = '/SoftwareDetails/Metadata'
        if grpnm in hf.keys(): #old location of metadata, will become deprecated
            grpnm += '/Keywords'
        else: #new location of metadata
            grpnm = PARAPROBE_ARAULLO_META_SFTWR
        print('Parsing configuration from group ' + grpnm)
        keys = list(hf[grpnm])
        self.config = {}
        for key in keys:
            print(key)
            tmp = hf[grpnm + '/' + key][:]
            self.config[key] = [ tmp[0].decode(), tmp[1].decode(), tmp[2].decode() ]
        self.results['Config'] = self.config
        
    def get_histogram(self, phaseID, roiID, sdmID, *args, **kwargs):
        """
        for a specific phase, specific ROI, read a specific one-dimensional SDM
        """
        fn = self.h5src_ara
        hf = h5py.File( fn, 'r' )
        #tell me which groups, sub-groups, and datasets exists in the HDF5 file
        araullo = list(hf[PARAPROBE_ARAULLO_RES_KEYQUANT])
        ###MK::currently only one phase supported
        phcand = 'PH' + str(phaseID)
        if phcand in araullo:
            mpids = list(hf[PARAPROBE_ARAULLO_RES_HISTO_CNTS + '/' + str(phcand)])
            #load the largest quantiles
            if str(roiID) in mpids:
                #get m binning exponent
                try:
                    m = np.uint32(self.config['SDMBinExponent'][0])
                except KeyError:
                    raise KeyError('Key SDMBinExponent does not exist !')
                #R
                try:
                    R = np.float64(self.config['ROIRadiusMax'][0])
                except KeyError:
                    raise KeyError('Key ROIRadiusMax does not exist !')
                nbins = 2**m
                dR = R/((2**(m-1))-1)
                #binID
                x = np.linspace(-dR - 1.0*R, +dR + 1.0*R, nbins, endpoint = True )
                y = hf[PARAPROBE_ARAULLO_RES_HISTO_CNTS + '/' + str(phcand) + '/' + str(roiID)][sdmID,:]
                print('Plotting histogram phaseID ' + str(phaseID) + ' roiID ' + str(roiID) + ' sdmID ' + str(sdmID) + ' ...')
                
                imgfn = (fn + '.Histogram.' + str(phcand) + '.' + str(roiID) + '.' + str(sdmID)).replace('/','.')
                imgttl = (PARAPROBE_ARAULLO_RES_HISTO_CNTS + '/' + str(phcand) + '/' + str(roiID) + '/' + str(sdmID)).replace('/','.').lstrip('.')
                imglegend = ['1D SDM']

                #render the figure
                self.results['Histogram'][imgfn] = plot_araullo_histogram( imgfn, x, y, imgttl, imglegend )
            else:
                raise ValueError('ROI ' + str(roiID) + ' does not exist in ' + fn + ' !')
        else:
            raise ValueError('Phase ' + str(phaseID)  + ' does not exist in ' + fn + ' !')
    
    def get_fftspectrum(self, phaseID, roiID, sdmID, *args, **kwargs):
        """
        for a specific phase (phaseID), specific ROI, read fast Fourier transformed amplitude spectrum of specific 1D SDM (sdmID)
        """
        fn = self.h5src_ara
        hf = h5py.File( fn, 'r' )
        #tell me which groups, sub-groups, and datasets exists in the HDF5 file
        araullo = list(hf[PARAPROBE_ARAULLO_RES_KEYQUANT])
        ###MK::currently only one phase supported
        phcand = 'PH' + str(phaseID)
        if phcand in araullo:
            mpids = list(hf[PARAPROBE_ARAULLO_RES_HISTO_MAGN + '/' + str(phcand)])
            #load the largest quantiles
            if str(roiID) in mpids:
                #get m binning exponent
                try:
                    m = np.uint32(self.config['SDMBinExponent'][0])
                except KeyError:
                    raise KeyError('Key SDMBinExponent does not exist !')
                nbins = 2**m
                #binID
                x = np.linspace(0, nbins-1, nbins, endpoint = True )
                y = hf[PARAPROBE_ARAULLO_RES_HISTO_MAGN + '/' + str(phcand) + '/' + str(roiID)][sdmID,:]
                print('Plotting fftspectrum phaseID ' + str(phaseID) + ' roiID ' + str(roiID) + ' sdmID ' + str(sdmID) + ' ...')
                
                imgfn = (fn + '.FFTMagn.' + str(phcand) + '.' + str(roiID) + '.' + str(sdmID)).replace('/','.')
                imgttl = (PARAPROBE_ARAULLO_RES_HISTO_MAGN + '/' + str(phcand) + '/' + str(roiID) + '/' + str(sdmID)).replace('/','.').lstrip('.')
                imglegend = ['Amplitude spectrum of FFT-ed 1D SDM']

                #render the figure
                self.results['FFTMagn'][imgfn] = plot_araullo_fftspectrum( imgfn, x, y, imgttl, imglegend )
            else:
                raise ValueError('ROI ' + str(roiID) + ' does not exist in ' + fn + ' !')
        else:
            raise ValueError('Phase ' + str(phaseID)  + ' does not exist in ' + fn + ' !')

    def get_specificpeaks(self, phaseID, roiID, *args, **kwargs):
        """
        for a specific phase (phaseID), for specific ROI (roiID), show flat signature with values ordered according to elevation/azimuth list
        only convenience better inspect these via HDF5/XDMF in Paraview
        """
        fn = self.h5src_ara
        hf = h5py.File( fn, 'r' )
        #tell me which groups, sub-groups, and datasets exists in the HDF5 file
        araullo = list(hf[PARAPROBE_ARAULLO_RES_SPECPEAK])
        ###MK::currently only one phase supported
        phcand = 'PH' + str(phaseID)
        if phcand in araullo:
            mpids = list(hf[PARAPROBE_ARAULLO_RES_SPECPEAK + '/' + str(phcand)])
            #load the largest quantiles
            if str(roiID) in mpids:
                y = hf[PARAPROBE_ARAULLO_RES_SPECPEAK + '/' + str(phcand) + '/' + str(roiID)][:]
                nnodes = len(y)
                #binID
                x = np.linspace(0, nnodes-1, nnodes, endpoint = True )
                print('Plotting specific peaks phaseID ' + str(phaseID) + ' roiID ' + str(roiID) + ' ...')
                
                imgfn = (fn + '.SpecPeaks.' + str(phcand) + '.' + str(roiID)).replace('/','.')
                imgttl = (PARAPROBE_ARAULLO_RES_SPECPEAK + '/' + str(phcand) + '/' + str(roiID)).replace('/','.').lstrip('.')
                imglegend = ['Highest amplitude in frequency range']

                #render the figure
                self.results['SpecificPeak'][imgfn] = plot_araullo_specificpeaks( imgfn, x, y, imgttl, imglegend )
            else:
                raise ValueError('ROI ' + str(roiID) + ' does not exist in ' + fn + ' !')
        else:
            raise ValueError('Phase ' + str(phaseID)  + ' does not exist in ' + fn + ' !')

    def get_keyquantiles(self, *args, **kwargs):
        """
        read the collection of precomputed key quantiles of specific FFT bin intensities to be used as a characterization
        at how many ROIs in the dataset we can get crystallographic information
        """
        self.metadata = {}
        self.keyquantiles = {}
        
        #pull number of ions within each ROI, currently only one rank supported!
        tmp = araullo_profiling_file()
        tmp.add_metadata_rank( 'PARAPROBE.Araullo.SimID.' + str(self.simid) + '.Rank.0.DetailedProfiling.csv' )
        self.metadata = tmp.metadata
        del tmp
                
        fn = self.h5src_ara
        hf = h5py.File( fn, 'r' )
        #tell me which groups, sub-groups, and datasets exists in the HDF5 file
        araullo=list(hf[PARAPROBE_ARAULLO_RES_KEYQUANT])
        for phcand in araullo:
            self.keyquantiles[phcand] = {}
            mpids = list(hf[PARAPROBE_ARAULLO_RES_KEYQUANT + '/' + phcand])
            #load the largest quantiles
            for mp in mpids:
                #here -1 so the last value of array, i.e. the highest image intensity
                self.keyquantiles[phcand][np.uint32(mp)] = hf[PARAPROBE_ARAULLO_RES_KEYQUANT + '/' + phcand + '/' + mp][-1,0]
                
            print('Reorganizing x, y data for phase ' + phcand + ' ...')
            #reorganize x,y data
            xy = np.zeros( [len(self.metadata[phcand].keys()), 2], np.float64 )
            i = 0
            for key in self.metadata[phcand].keys():
                xy[i,0] = self.metadata[phcand][key][0]
                xy[i,1] = self.keyquantiles[phcand][key]
                i += 1
            #print(xy)
            print('Plotting signal quality overview for phase ' + phcand + ' ...')

            imgfn = (fn + '.KeyQuantiles.' + phcand + '.IntsityVsIonCnts').replace('/','.')
            imgttl = (PARAPROBE_ARAULLO_RES_KEYQUANT + '/' + phcand).replace('/','.').lstrip('.')
            imglegend = ['Signature ' + phcand]

            #render the figure
            self.results['KeyQuantiles'][imgfn] = plot_araullo_keyquantiles_overview( imgfn, xy, imgttl, imglegend )
            
            #compute cumulative distribution intensity
            N = len(self.metadata[phcand].keys())
            cdfintsity = np.zeros( [N, 2], np.float64 )
            cdfintsity[:,0] = np.sort( xy[:,1], kind='stable' )
            cdfintsity[:,1] = np.linspace( 1.0/N, 1.0, N, endpoint = True )
            
            imgfn = (fn + '.KeyQuantiles.' + phcand + '.CDFIntsity').replace('/','.')
            imgttl = (PARAPROBE_ARAULLO_RES_KEYQUANT + '/' + phcand).replace('/','.').lstrip('.')
            imglegend = ['Signature ' + phcand]
            
            self.results['KeyQuantiles'][imgfn] = plot_araullo_keyquantiles_intensity( imgfn, cdfintsity, imgttl, imglegend )
            del cdfintsity
            
            #compute cumulative distribution number of ion counts
            cdfcnts = np.zeros( [N, 2], np.float64 )
            cdfcnts[:,0] = np.sort( xy[:,0], kind='stable' )
            cdfcnts[:,1] = np.linspace( 1.0/N, 1.0, N, endpoint = True )
            
            imgfn = (fn + '.KeyQuantiles.' + phcand + '.CDFIonCnts').replace('/','.')
            imgttl = (PARAPROBE_ARAULLO_RES_KEYQUANT + '/' + phcand).replace('/','.').lstrip('.')
            imglegend = ['Signature ' + phcand]
            
            self.results['KeyQuantiles'][imgfn] = plot_araullo_keyquantiles_ioncounts( imgfn, cdfcnts, imgttl, imglegend )
            
    def get_positions_keyquantiles(self, phaseID, threshold, *args, **kwargs):
        """
        read the collection of precomputed key quantiles of specific FFT bin intensities to be used as a characterization
        at how many ROIs in the dataset we can get crystallographic information then report me 
        for which ROIs the key quantile is above threshold, should be at the pole regions, return as a HDF5 file
        can be used for iterative refinement
        """
        self.metadata = {}
        self.keyquantiles = {}
        
        #pull number of ions within each ROI, currently only one rank supported!
        tmp = araullo_profiling_file()
        tmp.add_metadata_rank( 'PARAPROBE.Araullo.SimID.' + str(self.simid) + '.Rank.0.DetailedProfiling.csv' )
        self.metadata = tmp.metadata
        del tmp
                
        fn = self.h5src_ara
        hf = h5py.File( fn, 'r' )
        #tell me which groups, sub-groups, and datasets exists in the HDF5 file
        araullo=list(hf[PARAPROBE_ARAULLO_RES_KEYQUANT])
        ###MK::currently only one phase supported
        phcand = 'PH' + str(phaseID)
        if phcand in araullo:
            self.keyquantiles[phcand] = {}
            mpids = list(hf[PARAPROBE_ARAULLO_RES_KEYQUANT + '/' + phcand])
            #load the largest quantiles
            for mp in mpids:
                #here -1 so the last value of array, i.e. the highest image intensity
                intsity = hf[PARAPROBE_ARAULLO_RES_KEYQUANT + '/' + phcand + '/' + mp][-1,0]
                if intsity < threshold:
                    continue
                else:
                    self.keyquantiles[phcand][np.uint32(mp)] = intsity
            print('We found ' + str(len(self.keyquantiles[phcand].keys())) + ' for which the -1 key quantile, i.e. the highest intensity is >=' + str(threshold))
            print('Collecting ROI positions organizing x, y data for phase ' + phcand + ' ...')
            
            #load ROI IDs and positions XYZ, remember, paraprobe-araullo reports these in order
            roi_id = hf[PARAPROBE_ARAULLO_META_MP_ID][:,0]
            roi_xyz = hf[PARAPROBE_ARAULLO_META_MP_XYZ][:,:]
            
            #find corresponding positions
            ##MK::vectorize at some point
            N = len(self.keyquantiles[phcand].keys())
            filtered_xyz = np.zeros( [N, 3], np.float64 )
            filtered_mpids = np.zeros( [N, 1], np.uint32 )
            i = 0
            for mp in self.keyquantiles[phcand].keys():
                thisone = np.where( roi_id == mp )
                if len(thisone) == 1:
                    filtered_xyz[i,:] = roi_xyz[thisone[0][0],:]
                    filtered_mpids[i,:] = mp
                    i += 1
                else:
                    raise ValueError('For mp ' + str(mp) + ' I found none or more than one location in the ROI ID array, this is unexpected!')
            print('np.shape(filtered_xyz) is ' + str(np.shape(filtered_xyz)))
            print('np.shape(filtered_mpids) is ' + str(np.shape(filtered_mpids)))
            
            #add topology information for visualization with Paraview, ##MK::range check
            filtered_topo = np.ones([3*N, 1], np.uint32 ) #1 XDMF geometric primitive keyword for points each 3th element of a block is an ID
            #start at 2 move 3 forward
            filtered_topo[2::3,0] = np.uint32(np.linspace(0, N-1, N, endpoint=True))
            
            #report position to import them again with paraprobe-araullo for an iterative refinement of the ROI grid
            fn_res = fn[0:-3] + '.' + phcand + '.UserROIs.h5' #assuming '.h5'
            hf = h5py.File( fn_res, 'w')
            hf.create_dataset( PARAPROBE_ARAULLO_META_MP_XYZ, data=filtered_xyz )
            hf.create_dataset( PARAPROBE_ARAULLO_META_MP_ID, data=filtered_mpids )
            hf.create_dataset( PARAPROBE_ARAULLO_META_MP_TOPO, data=filtered_topo )
            hf.close()
            #add XDMF file for Paraview
            print(fn_res + ' written for the next paraprobe-araullo run with a locally refined ROI grid')
            #print(filtered_xyz)
            #print(filtered_mpids)

    def report(self):
        return self.results

# =============================================================================
#example usage, tell me how many ions of a certain type
#task = autoreporter_araullo( 0 )
#task.get_keyquantiles()
# =============================================================================
