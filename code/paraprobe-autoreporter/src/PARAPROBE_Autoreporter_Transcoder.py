# -*- coding: utf-8 -*-
"""
created 2020/05/28, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for strongly automatized reporting
of results from the paraprobe-transcoder tool
"""

import os, sys

import glob, subprocess
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
try:
    import periodictable as pse
except ImportError as e:
    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')
try:
    import h5py
except ImportError as e:
    raise ValueError('Install h5py Python package via e.g. pip install h5py !')

from PARAPROBE_Autoreporter_Numerics import *
from PARAPROBE_Autoreporter_CorpDesign import *
from PARAPROBE_Autoreporter_MetadataTranscoder import *
from PARAPROBE_Autoreporter_MetadataRanger import *
from PARAPROBE_Autoreporter_Plots import *

#dictionary of settings to read from a paraprobe-spatstat runs which are worth to be reported
#paraprobe settings always have the same format within an paraprobe H5 file, they are
#3x1 matrices of strings the [0].decode() gives the value, [1].decode() the unit, [2].decode() a description
config_transcoder = { 'Inputfile': '',
                      'TranscodingMode': '' }

class autoreporter_transcoder():
    
    def __init__(self, fn, *args, **kwargs): #ds_rg
        #self.simid = simid
        self.toolname = 'Transcoder'
        #we need an HDF5 file as source from which to load the data for filling the transcoder results object with results
        #self.ds_rg_n = 0
        #single = kwargs.get('single_dataset', [])
        #if not single == []:
        #    if np.shape(single)[0] > 0:
        #        self.ds_rg_n = 1
        #multi = kwargs.get('multi_dataset', [])
        #if not multi == []:
        #    if np.shape(multi)[0] > 0:
        #        self.ds_rg_n = np.shape(multi)[0]
        #if np.shape(ds_rg)[0] == 1:
        #    self.ds_rg_n = 1 #np.shape(ds_rg)[0]
        #    self.h5src = ds_rg
        #else:
        #    self.ds_rg_n = np.shape(ds_rg)[0]
        #    self.h5src = ds_rg
        #    raise ValueError('No dataset or too many datasets and range files provided, only one supported currently')
        self.h5src = fn
        self.natoms = 0
        self.config = config_transcoder
        self.results = {}
        #add here all functions which parse by default results from the source
        self.get_tool_config()
        self.get_natoms()

    def get_tool_config(self):
        #fn = 'C:/Users/kaiob/eclipse-workspace/mpie-aptfim-toolbox/paraprobe-autoreporter/HuanZhaoScrMatCaseStudy/PARAPROBE.Transcoder.Results.SimID.26120.h5'
        fn = self.h5src
        hf = h5py.File( fn, 'r' )
        self.config = config_transcoder
        #for key in self.config.keys(): #replace the defaults by the actual values
        #    print(key)
        #    tmp = hf['/SoftwareDetails/Metadata/Keywords/' + key][:]
        #    self.config[key] = [ tmp[0].decode(), tmp[1].decode(), tmp[2].decode() ]
        grpnm = '/SoftwareDetails/Metadata'
        if grpnm in hf.keys(): #old location of metadata, will become deprecated
            grpnm += '/Keywords'
        else: #new location of metadata
            grpnm = PARAPROBE_TRANS_META_SFTWR
        print('Parsing configuration from group ' + grpnm)
        keys = list(hf[grpnm])
        self.config = {}
        for key in keys:
            print(key)
            tmp = hf[grpnm + '/' + key][:]
            self.config[key] = [ tmp[0].decode(), tmp[1].decode(), tmp[2].decode() ]
        self.results['Config'] = self.config 

    def get_natoms(self):
        """
        read natoms from the HDF5 file source
        """
        #fn = 'PARAPROBE.Transcoder.Results.SimID.7.apth5'
        self.natoms = 0
        fn = self.h5src #[0][0]
        hf = h5py.File( fn, 'r')
        tmp = hf[PARAPROBE_TRANS_RES_XYZ][:,:]
        self.natoms = np.shape(tmp)[0]
        self.results['NumberOfAtoms'] = self.natoms

    def report(self):
        return self.results

    #def report_natoms(self):
    #    """
    #    create Latex code presenting the number of atoms
    #    """
    #    print(self.h5src[0][0] + ' has a reconstruction with ' + str(self.natoms) + ' ions')
    #    self.tex = ''
        
        #the object keeps the Latex string snippet for itself

# =============================================================================
#     def report_mass2charge_vs_ranging_assessment(self, mqmin, mqicr, mqmax, figwidth, figheight ):
#         """
#         plot a barcode profile of multiple range file rangings to compare them heads up, ds is a np.array of dataset names
#         """
#         #here is an example how to generate a range file genome to quickly compare many rangefiles
#         #for overlap and similarity from the ranges
#         if mqmin < 0.0 or mqicr <= 0.0 or mqmin <= mqright:
#             raise ValueError('Error on the mass-to-charge interval bounds 0.0 <= mqmin < mqmax, with mqicr > 0.0 !')
#        
#         #fig, ax = plt.subplots( figsize=(16.53, 11.69) ) #A3 11.69 x 16.53
#         fig, ax = plt.subplots( figsize=(figwidth, figheight) ) #A3 11.69 x 16.53
#         
#         xmi = 1.0e36
#         xmx = -1.0e36
#         ymi = 1.0e36
#         ymx = -1.0e36
#         
#         for i in np.arange(0,self.ds_rg_n):
#             tmp_
#             tmp_rng = autoreporter_ranger( self.h5src[i][1] )
#             offset = (i+1)*10.0
#             height = 5.0
#             #rangeboxes = []
#             for key in tmp_rng.rngTbl.itypes.keys():
#                 niv = len(tmp_rng.rngTbl.itypes[key].mq)
#                 for iv in np.arange(0,niv):
#                     mqmi = tmp_rng.rngTbl.itypes[key].mq[iv][0]
#                     mqmx = tmp_rng.rngTbl.itypes[key].mq[iv][1]
#                     if mqmi <= xmi:
#                         xmi = mqmi
#                     if mqmx >= xmx:
#                         xmx = mqmx
#                     width = mqmx - mqmi
#                     #print(str(width))
#                     x = mqmi
#                     y = offset - 0.5*height #offset
#                     if y-0.5*height <= ymi:
#                         ymi = y-0.5*height
#                     if y+0.5*height >= ymx:
#                         ymx = y+0.5*height
#                     ax.add_patch( plt.Rectangle( (x, y), width, height, facecolor='black', alpha=0.25 ) )
#                     #plt.xlim([0.95*xmi, +1.05*xmx])
#             del task
#             ax.text( mqleft - 8.0, offset, ds[i], fontsize=6 ) #, fontdict=font)
#         plt.xlim([mqleft, mqright])
#         #plt.xlim([65.0, 75.0])
#         #plt.xlim([-10.0, 140.0])
#         #plt.ylim([-5.0, +5.0])
#         plt.ylim([5.0, offset+5.0])
#         plt.yticks([])
#         plt.xlabel('Mass-to-charge (Da)')
#         #plt.set_title('Range file assessment')
#         #plt.legend( [lgnd], loc='upper right')
#         #plt.xlabel('Distance (nm)')
#         #plt.ylabel(r'Radial distribution function')
#         #xy.vlines([xmx], 0, 1, transform=xy.get_xaxis_transform(), colors=MYPARULA[2], linestyles='dotted') #'solid') #'dotted')
#         #plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
#         #plt.yscale('log')
#         figfn = fn + '.pdf' #'.png'
#         figfn = fn + '.png'
#         #https://stackoverflow.com/questions/32185411/break-in-x-axis-of-matplotlib
#         #fig.savefig( figfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
#         #                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
#         fig.savefig( figfn, dpi=600, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
#                         transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None) #frameon=None,
#         fig.clf()
# =============================================================================


# =============================================================================
# #example usage for a single dataset, tell me how many atoms of a certain type
#task = autoreporter_transcoder( single_dataset = 'PARAPROBE.Transcoder.Results.SimID.7.apth5' )
# task.report_natoms()

# #example usage for a dataset collection as with dataset filenames and ranging filenames ds_rg
#compare multiple mass-to-charge histograms for given ranging
#answers in how far a user-defined set of rangings for a collection of datasets is similar or different
#and possibly can be replaced by a single range file

#huan
#ds_rg = np.array([ ['PARAPROBE.Transcoder.Results.SimID.24909.h5', 'R76-24909-30min.rng.h5'], 
#                   ['PARAPROBE.Transcoder.Results.SimID.26120.h5', 'R76-26120-OA.rng.h5']     ])
#ds_rg = np.array([ ['PARAPROBE.Transcoder.Results.SimID.24909.h5', 'R76-24909-30min.rng.h5'] ])

#priyanshu
#ds_rg = np.array([ ['PARAPROBE.Transcoder.Results.SimID.2763201.h5', ''],
#                   ['PARAPROBE.Transcoder.Results.SimID.2764502.h5', ''],
#                   ['PARAPROBE.Transcoder.Results.SimID.2765601.h5', ''],
#                   ['PARAPROBE.Transcoder.Results.SimID.2804201.h5', ''],
#                   ['PARAPROBE.Transcoder.Results.SimID.2805001.h5', ''],
#                   ['PARAPROBE.Transcoder.Results.SimID.2806302.h5', ''],
#                   ['PARAPROBE.Transcoder.Results.SimID.2975901.h5', '']   ])
                   
#task = autoreporter_transcoder( ds_rg )
#task.report_mqhistogram( 0.0, 0.001, 140.0 )

#task.report_mass2charge_vs_ranging_assessment( 0.0, 0.01, 140.0, 20.0, 10.0 )
# =============================================================================
