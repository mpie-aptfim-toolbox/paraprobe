# -*- coding: utf-8 -*-
"""
created 2020/05/28, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for strongly automatized reporting of results
here we have utility functions mostly low-level coding and decoding of information from HDF5 files
"""

import os
import sys
import glob, subprocess
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
try:
    import periodictable as pse
except ImportError as e:
    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')
try:
    import h5py
except ImportError as e:
    raise ValueError('Install h5py Python package via e.g. pip install h5py !')

def get_paraprobe_molecularion_code( strng ):
    #currently only single element molecular ions support for this type of plot
    z = np.array([ np.uint8(0), np.uint8(0), np.uint8(0) ])            
    for el in pse.elements:
        if not strng == el.symbol:
            continue
        else:
            jZ = el.number
            z[0] = np.uint8(jZ)
            break
    return z
