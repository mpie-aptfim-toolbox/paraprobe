# -*- coding: utf-8 -*-
"""
created 2020/05/28, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for strongly automatized reporting
of results from the paraprobe-ranger tool
"""

import os, sys
import glob, subprocess
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
try:
    import periodictable as pse
except ImportError as e:
    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')
try:
    import h5py
except ImportError as e:
    raise ValueError('Install h5py Python package via e.g. pip install h5py !')

from PARAPROBE_Autoreporter_Numerics import *
from PARAPROBE_Autoreporter_CorpDesign import *
from PARAPROBE_Autoreporter_Plots import *

from PARAPROBE_Autoreporter_MetadataTranscoder import *
from PARAPROBE_Autoreporter_MetadataSurfacer import *

#dictionary of settings to read from a paraprobe-spatstat runs which are worth to be reported
#paraprobe settings always have the same format within an paraprobe H5 file, they are
#3x1 matrices of strings the [0].decode() gives the value, [1].decode() the unit, [2].decode() a description
config_surfacer = { 'InputfileReconstruction': '',
                    'SurfacingMode': '',
                    'AlphaShapeAlphaValue': '',
                    'AdvIonPruneBinWidthMin': '',
                    'AdvIonPruneBinWidthIncr': '',
                    'AdvIonPruneBinWidthMax': '',
                    'AdvDistanceBinWidthMin': '',
                    'AdvDistanceBinWidthIncr': '',
                    'AdvDistanceBinWidthMax': '',
                    'DistancingMode': '',
                    'DistancingRadiusMax': '',                    
                    'RequeryingThreshold': '' }

class autoreporter_surfacer():
    
    def __init__(self, ions, hull , *args, **kwargs):
        #self.simid = simid
        self.toolname = 'Surfacer'
        self.h5src_ions = ions
        self.h5src_hull = hull
        
        self.config = config_surfacer
        #add here all functions which parse by default results from the source
        #self.get_distances()
        self.results = {}
        #add here all functions which parse by default results from the source
        self.get_tool_config()
        
    def get_tool_config(self):
        #fn = 'C:/Users/kaiob/eclipse-workspace/mpie-aptfim-toolbox/paraprobe-autoreporter/HuanZhaoScrMatCaseStudy/PARAPROBE.Transcoder.Results.SimID.26120.h5'
        fn = self.h5src_hull
        hf = h5py.File( fn, 'r' )
        self.config = config_surfacer
        #for key in self.config.keys(): #replace the defaults by the actual values
        #    print(key)
        #    tmp = hf['/SoftwareDetails/Metadata/Keywords/' + key][:]
        #    self.config[key] = [ tmp[0].decode(), tmp[1].decode(), tmp[2].decode() ]
        #above reads what the config_surfacer default dictates, instead
        #to read all entries use
        grpnm = '/SoftwareDetails/Metadata'
        if grpnm in hf.keys(): #old location of metadata, will become deprecated
            grpnm += '/Keywords'
        else: #new location of metadata
            grpnm = PARAPROBE_SURF_META_SFTWR
        print('Parsing configuration from group ' + grpnm)
        keys = list(hf[grpnm])
        self.config = {}
        for key in keys:
            print(key)
            tmp = hf[grpnm + '/' + key][:]
            self.config[key] = [ tmp[0].decode(), tmp[1].decode(), tmp[2].decode() ]
        self.results['Config'] = self.config
        
    def get_distances(self):
        """
        read computed distances to the alpha shape from h5src_hull
        """
        fn = self.h5src_hull #'PARAPROBE.Surfacer.Results.SimID.7.h5'
        hf = h5py.File( fn, 'r' ) #self.h5src, 'r')
        self.d = hf[PARAPROBE_SURF_DIST_RES_D][:,0]  #float32 max indicate values larger threshold
        
        imgfn = fn + '.CDF.Ion2EdgeDist'
        ashp_val = hf[PARAPROBE_SURF_ASHAPE_META_AVALUE][:,:].flatten()
        imgttl = fn + ' ' + r'$\alpha$-shape, $\alpha = $' + str(np.around(ashp_val[0], decimals=3))
        grpnm = '/SoftwareDetails/Metadata/Keywords'
        if grpnm in hf.keys(): #old location of metadata, will become deprecated
            grpnm = grpnm + '/DistancingMode'
        else: #new location of metadata
            grpnm = PARAPROBE_SURF_META_SFTWR + '/DistancingMode'
        #distmode = np.uint32(hf['/SoftwareDetails/Metadata/Keywords/DistancingMode'][0])
        distmode = np.uint32(hf[grpnm][0])
        
        #render the figure 
        self.results['Ion2EdgeDistanceFigure'] = plot_cdf_ion2edge_distance( imgfn, self.d, distmode, imgttl )
        
        #surfacer class object keeps a dictionary to the file now the surfacer object keeps the file name
        #if os.path.isfile( imgfn ) == True:
        #    self.results['Ion2EdgeDistanceFigure'] = imgfn
            
    def report(self):
        return self.results

# =============================================================================
#example usage, tell me how many ions of a certain type
#task = autoreporter_surfacer( 'PARAPROBE.Transcoder.Results.SimID.7.apth5', 'PARAPROBE.Surfacer.Results.SimID.7.h5' )
#task.get_distances()
#task.cnts
# task.report_natoms()
# =============================================================================
