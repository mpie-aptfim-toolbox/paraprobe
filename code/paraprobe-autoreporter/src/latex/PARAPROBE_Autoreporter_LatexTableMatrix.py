# -*- coding: utf-8 -*-
"""
created 2020/06/19, Markus K\"uhbach, m.kuehbach@mpie.de
utility class for organizing writing of 2D tables with all cells occupied
no fusing of cells possible for generating Latex tables using dictionaries
"""

import os
import sys
#sys.path.append('../src/')
import glob, subprocess
import pandas as pd
import numpy as np

class table_matrix_settings():
    #we test here a class for generating Latex code for tables with len(tbldict.keys()) rows
    #and 4 fixed columns: keyword, value, unit, description
    
    def __init__(self, tbldict, caption, lbl, *arg, **kwargs):
        """
        tbldict is a dictionary with numpy string arrays
        ##MK here the same len of these arrays for every val of a dict keyointing to figure files (e.g. png, tif)
        caption is a string giving the table caption
        lbl is a string giving the table caption
        """
        if not 'Header' in tbldict:
            raise ValueError('The table has no header but it needs one !')
        #if len(figdict.keys()) != len(scapdict.keys()):
        #    raise ValueError('Not every figure has a sub-caption, this is currently not allowed !')
        #for key in figdict.keys():
        #    if key in scapdict:
        #        continue
        #    else:
        #        raise ValueError('The dictionaries for the figure filenames and the sub-captions need to use the same keys !')
        #generate the tex source code
        self.tex = '\n'
        self.tex += r'\begin{center}' + '\n'
        self.tex += r'\begin{table}[!htb]' + '\n'
        self.tex += r'\caption{' + caption + '}' + '\n'
        self.tex += r'\centering' + '\n'
        self.tex += r'\begin{tabular}{llll}' + '\n' #specific for four column tables
        self.tex += r'%\addlinespace[0.2em]' + '\n'
        self.tex += r'\toprule' + '\n'
        self.tex += '\t' + r'\bf{Keyword}' + '\t' + r' & '
        for i in np.arange(0,2):
            self.tex += '\t' + r'\bf{' + tbldict['Header'][i] + r'} ' + '\t' + r' & '
        self.tex += '\t' + r'\bf{' + tbldict['Header'][2] + r'} ' + '\t' + r'\\ \midrule' + '\n'
        for key in tbldict:
            if not key == 'Header':
                self.tex += '\t' + str(key) + '\t' + r' & '
                for i in np.arange(0,2):
                    self.tex += '\t' + str(tbldict[key][i]).replace('_','\_') + '\t' + r' & '
                #self.tex += '\t' + str(tbldict[key][2])
                self.tex += '\t' + ''
                if key != list(tbldict)[-1]:
                    self.tex += '\t' + r'\\' + '\n'
                else: #catch the last row because it requires a different formatting bottomrule...
                    self.tex += '\t' + r'\\ \bottomrule' + '\n'
        self.tex += r'%\addlinespace[1em]' + '\n'
        self.tex += r'\end{tabular}' + '\n'
        self.tex += r'\label{' + lbl + '}' + '\n'
        self.tex += r'\end{table}' + '\n'
        self.tex += r'\end{center}' + '\n'
        
class table_matrix_profiling():
    #we test here a class for generating Latex code for tables with len(tbldict.keys()) rows
    #and 4 fixed columns: toolname, #mpi, #omp, telapsed
    
    def __init__(self, tbldict, caption, lbl, *arg, **kwargs):
        """
        tbldict is a dictionary with numpy string arrays
        ##MK here the same len of these arrays for every val of a dict keyointing to figure files (e.g. png, tif)
        caption is a string giving the table caption
        lbl is a string giving the table caption
        """
        if not 'Header' in tbldict:
            raise ValueError('The table has no header but it needs one !')
        #generate the tex source code
        self.tex = '\n'
        self.tex += r'\begin{center}' + '\n'
        self.tex += r'\begin{table}[!htb]' + '\n'
        self.tex += r'\caption{' + caption + '}' + '\n'
        self.tex += r'\centering' + '\n'
        self.tex += r'\begin{tabular}{lrrr}' + '\n' #specific for four column tables
        self.tex += r'%\addlinespace[0.2em]' + '\n'
        self.tex += r'\toprule' + '\n'
        #self.tex += '\t' + r'\bf{ToolnameKeyword}' + '\t' + r' & '
        for i in np.arange(0,3):
            self.tex += '\t' + r'\bf{' + tbldict['Header'][i] + r'} ' + '\t' + r' & '
        self.tex += '\t' + r'\bf{' + tbldict['Header'][3] + r'} ' + '\t' + r'\\ \midrule' + '\n'
        for key in tbldict:
            if not key == 'Header':
                #self.tex += '\t' + str(key) + '\t' + r' & '
                for i in np.arange(0,3):
                    self.tex += '\t' + str(tbldict[key][i]).replace('_','\_') + '\t' + r' & '
                self.tex += '\t' + str(tbldict[key][3]).replace('_','\_')
                #self.tex += '\t' + ''
                if key != list(tbldict)[-1]:
                    self.tex += '\t' + r'\\' + '\n'
                else: #catch the last row because it requires a different formatting bottomrule...
                    self.tex += '\t' + r'\\ \bottomrule' + '\n'
        self.tex += r'%\addlinespace[1em]' + '\n'
        self.tex += r'\end{tabular}' + '\n'
        self.tex += r'\label{' + lbl + '}' + '\n'
        self.tex += r'\end{table}' + '\n'
        self.tex += r'\end{center}' + '\n'
