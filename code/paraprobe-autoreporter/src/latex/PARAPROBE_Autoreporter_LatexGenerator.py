# -*- coding: utf-8 -*-
"""
created 2020/06/19, Markus K\"uhbach, m.kuehbach@mpie.de
utility class for organizing writing of 2D tables with all cells occupied
no fusing of cells possible for generating Latex tables using dictionaries
"""

import os
import sys
#sys.path.append('../src/')
import glob, subprocess
import pandas as pd
import numpy as np

from PARAPROBE_Autoreporter_LatexFigureMatrix import *
from PARAPROBE_Autoreporter_LatexTableMatrix import *
from PARAPROBE_Autoreporter_Transcoder import *

class autoreporter_latex():
    #we test here a class for generating Latex code for tables with len(tbldict.keys()) rows
    #and 4 fixed columns: keyword, value, unit, description
    
    def __init__(self, fn, title, author, *arg, **kwargs):
        """
        core class implementing a wizard for generating automatic tex reports from results of paraprobe runs
        fn is a string giving the filename for the to be generated tex file
        title is the title of the report
        author is an r'string' telling the author name
        """
        #filename
        if fn.endswith('.TEX') or fn.endswith('.tex'):
            self.fn = fn
        else:
            raise ValueError('Paraprobe Tex reports need to have a file ending .tex !')
        #title
        if title == '':
            raise ValueError('We need to have a title !')
        else:
            self.title = title
        #author
        if author == '':
            raise ValueError('We need to have an author')
        else:
            self.author = str(author)
            
        #tool specific sections needs to be initialized as they are populated externally
        self.tex_trns = ''
        self.tex_synt = ''
        self.tex_rngr = ''
        self.tex_surf = ''
        self.tex_tess = ''
        self.tex_spst = ''
        self.tex_dbsc = ''
        self.tex_arau = ''
        self.tex_four = ''
        self.tex_prof = ''
        
    def add_header_section(self):
        self.tex_header = ''
        self.tex_header += r'%\documentclass[a4paper,draft,onecolumn]{article}' + '\n'
        self.tex_header += r'\documentclass[a4paper,final,onecolumn]{article}' + '\n'
        self.tex_header += r'\usepackage[utf8]{inputenc}' + '\n'
        self.tex_header += r'\usepackage[english]{babel}' + '\n'
        self.tex_header += r'\usepackage{lineno,hyperref}' + '\n'
        self.tex_header += r'\usepackage{amssymb}' + '\n'
        self.tex_header += r'\usepackage{mathtools}' + '\n'
        self.tex_header += r'\DeclarePairedDelimiter\ceil{\lceil}{\rceil}' + '\n'
        self.tex_header += r'\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}' + '\n'
        self.tex_header += r'\usepackage[binary-units=true]{siunitx}' + '\n'
        self.tex_header += r'\usepackage{subfig}' + '\n'
        self.tex_header += r'%\modulolinenumbers[5]' + '\n'
        self.tex_header += r'\usepackage{cleveref}' + '\n'
        self.tex_header += r'%has to be loaded after hyperref \usepackage[capitalise]{cleveref} % to have all fig. eq. figs. appearing as Fig. https://texblog.org/2013/05/06/cleveref-a-clever-way-to-reference-in-latex/' + '\n'
        self.tex_header += r'%on abbreviations see https://tex.stackexchange.com/questions/256849/cleveref-change-behaviour-of-cref-to-use-the-abbreviated-form' + '\n'
        self.tex_header += r'%specific modifications to cleveref' + '\n'
        self.tex_header += r'\Crefname{equation}{Eq.}{Eqs.}' + '\n'
        self.tex_header += r'\Crefname{figure}{Fig.}{Figs.}' + '\n'
        self.tex_header += r'\Crefname{tabular}{Tab.}{Tabs.}' + '\n'
        self.tex_header += r'\usepackage{xcolor}' + '\n'
        self.tex_header += r'%\usepackage{soul}' + '\n'
        self.tex_header += r'%turn on draft figures for document' + '\n'
        self.tex_header += r'%\setkeys{Gin}{draft}' + '\n'
        self.tex_header += r'%\journal{Ultramicroscopy}' + '\n'
        self.tex_header += r'%%Elsevier LaTeX style' + '\n'
        self.tex_header += r'%\bibliographystyle{elsarticle-num}' + '\n'
        self.tex_header += r'%\biboptions{sort&compress}' + '\n'
        self.tex_header += r'%%user-defined commands' + '\n'
        self.tex_header += r'\newcommand{\anaconda}{Anaconda}' + '\n'
        self.tex_header += r'\newcommand{\bokeh}{Bokeh}' + '\n'
        self.tex_header += r'\newcommand{\cxx}{C/C++}' + '\n'
        self.tex_header += r'\newcommand{\fortran}{Fortran}' + '\n'
        self.tex_header += r'\newcommand{\python}{Python}' + '\n'
        self.tex_header += r'\newcommand{\matlab}{MATLAB}' + '\n'
        self.tex_header += r'\newcommand{\paraprobe}{PARAPROBE}' + '\n'
        self.tex_header += r'\newcommand{\cgal}{CGAL}' + '\n'
        self.tex_header += r'\newcommand{\qhull}{QHull}' + '\n'
        self.tex_header += r'\newcommand{\voroxx}{Voro$++$}' + '\n'
        self.tex_header += r'\newcommand{\hdf}{HDF5}' + '\n'
        self.tex_header += r'\newcommand{\xdmf}{XDMF}' + '\n'
        self.tex_header += r'\newcommand{\hdmf}{HDF5/XDMF}' + '\n'
        self.tex_header += r'\newcommand{\omp}{OpenMP}' + '\n'
        self.tex_header += r'\newcommand{\imkl}{Intel Math Kernel Library}' + '\n'
        self.tex_header += r'\newcommand{\imk}{Intel Math Kernel}' + '\n'
        self.tex_header += r'\newcommand{\itbb}{Intel Threaded Building Blocks}' + '\n'
        self.tex_header += r'\newcommand{\paraview}{Paraview}' + '\n'
        self.tex_header += r'\newcommand{\visit}{VisIt}' + '\n'
        self.tex_header += r'\newcommand{\ivas}{IVAS}' + '\n'
        self.tex_header += r'\newcommand{\mtex}{MTEX Toolbox}' + '\n'
        self.tex_header += r'\newcommand{\blender}{Blender}' + '\n'
        self.tex_header += r'\newcommand{\rapidxml}{RapidXML}' + '\n'
        self.tex_header += r'\newcommand{\ashapes}{$\alpha$-shapes}' + '\n'
        self.tex_header += r'\newcommand{\ashape}{$\alpha$-shape}' + '\n'
        self.tex_header += r'\newcommand{\gshapes}{$\gamma$-shapes}' + '\n'
        self.tex_header += r'\newcommand{\gshape}{$\gamma$-shape}' + '\n'
        self.tex_header += r'\newcommand{\cameca}{CAMECA/AMETEK}' + '\n'
        self.tex_header += r'\newcommand{\bunge}[3]{($\varphi_1=\SI[mode=math]{#1}{\degree}, \Phi=\SI[mode=math]{#2}{\degree},\varphi_2=\SI[mode=math]{#3}{\degree}$)}' + '\n'
        self.tex_header += r'\newcommand{\swisswatch}[2]{$\SI[mode=math]{#1}{\hour}$:$\SI[mode=math]{#2}{\minute}$}' + '\n'
        self.tex_header += r'%\newcommand{\althreesc}{Al${}_3$Sc}' + '\n'
        self.tex_header += r'%\newcommand{\ival}[3]{$[#1,#2,#3]$}' + '\n'
        self.tex_header += r'%\newcommand{\alscsi }[2]{Al-\SI[mode=text]{#1}{}Sc-\SI[mode=text]{#2}{}Si}' + '\n'
        self.tex_header += r'%\newcommand{\itswtpercent}{(wt. \SI[mode=text]{}{\percent})}' + '\n'
        self.tex_header += r'%simplifying the typesetting of parameter settings' + '\n'
        self.tex_header += r'\newcommand{\maxsep}[4]{$d_{max} = [#1,#2,#3] \SI[mode=math]{}{\nano\meter}, N_{min} = \SI[mode=math]{#4}{}$}' + '\n'
        self.tex_header += r'%\newcommand{\spatstat}[5]{$k = #1, r = [#2,#3,#4] \SI[mode=math]{}{\nano\meter}, d_{srf} \geq \SI[mode=math]{#5}{\nano\meter}$}' + '\n'
        self.tex_header += r'\newcommand{\spatstatnok}[4]{$r = [#1,#2,#3] \SI[mode=math]{}{\nano\meter}, d_{srf} \geq \SI[mode=math]{#4}{\nano\meter}$}  %the more general with specific k' + '\n'
        self.tex_header += r'\newcommand{\spatstatnor}[4]{$k = #1, r = [#2,#3,#4] \SI[mode=math]{}{\nano\meter}$}  %the more general with specific k' + '\n'
        self.tex_header += r'\newcommand{\ionsurf}[1]{$d_{prb} = \SI[mode=math]{#1}{\nano\meter}$}' + '\n'
        self.tex_header += r'\newcommand{\vorotess}[1]{$d_{ero} = \SI[mode=math]{#1}{\nano\meter}$}' + '\n'
        self.tex_header += r'\newcommand{\twostat}[4]{$r = [#1,#2,#3] \SI[mode=math]{}{\nano\meter}, \Delta r = \SI[mode=math]{#4}{\nano\meter}$}' + '\n'
        self.tex_header += r'\newcommand{\aptcryst}[8]{$el = [#1,#2,#3], az = [#4,#5,#6], r_{max} = \SI[mode=math]{#7}{\nano\meter}, m = \SI[mode=math]{#8}{}$}' + '\n'
        self.tex_header += r'\newcommand{\ashapeinit}[1]{$d_{bin} = \SI[mode=math]{#1}{\nano\meter}$}' + '\n'
        self.tex_header += r'\usepackage{miller}' + '\n'
        self.tex_header += r'%names of paraprobe-tools' + '\n'
        self.tex_header += r'\newcommand{\parmsetup}{paraprobe-parmsetup}' + '\n'
        self.tex_header += r'\newcommand{\transcoder}{paraprobe-transcoder}' + '\n'
        self.tex_header += r'\newcommand{\synthetic}{paraprobe-synthetic}' + '\n'
        self.tex_header += r'\newcommand{\ranger}{paraprobe-ranger}' + '\n'
        self.tex_header += r'\newcommand{\surfacer}{paraprobe-surfacer}' + '\n'
        self.tex_header += r'\newcommand{\tessellator}{paraprobe-tessellator}' + '\n'
        self.tex_header += r'\newcommand{\spatstat}{paraprobe-spatstat}' + '\n'
        self.tex_header += r'\newcommand{\dbscan}{paraprobe-dbscan}' + '\n'
        self.tex_header += r'\newcommand{\araullo}{paraprobe-araullo}' + '\n'
        self.tex_header += r'\newcommand{\fourier}{paraprobe-fourier}' + '\n'
        self.tex_header += r'\newcommand{\indexer}{paraprobe-indexer}' + '\n'
        self.tex_header += r'\newcommand{\intersector}{paraprobe-intersector}' + '\n'
        self.tex_header += r'\newcommand{\autoreporter}{paraprobe-autoreporter}' + '\n'
        self.tex_header += r'%% annotating debug stuff' + '\n'
        self.tex_header += r'\newcommand{\dbg}[1]{\textcolor{red}{\textbf{ #1 }}}' + '\n'
        self.tex_header += r'\newcommand{\suppl}[1]{\textcolor{green}{#1}}' + '\n'
        self.tex_header += r'\newcommand{\kick}[1]{\textcolor{orange}{#1}}' + '\n'
        self.tex_header += r'\newcommand{\whom}[1]{\textcolor{blue}{\textbf{ #1 }}}' + '\n'
        self.tex_header += r'%%tables' + '\n'
        self.tex_header += r'\usepackage{booktabs}' + '\n'
        self.tex_header += r'\usepackage{tabularx}' + '\n'
        self.tex_header += r'%specific implementation algorithmic charts' + '\n'
        self.tex_header += r'\usepackage{algorithm}' + '\n'
        self.tex_header += r'\usepackage[noend]{algpseudocode}' + '\n'
        self.tex_header += r'\def\BState{\State\hskip-\ALG@thistlm}' + '\n'
        self.tex_header += r'\algdef{SE}[DOWHILE]{Do}{doWhile}{\algorithmicdo}[1]{\algorithmicwhile\ #1}' + '\n'
        self.tex_header += r'%https://tex.stackexchange.com/questions/115709/do-while-loop-in-pseudo-code' + '\n'
        self.tex_header += r'%where to find the figures' + '\n'
        self.tex_header += r'%\graphicspath{{../dd_figures/}}' + '\n'
        self.tex_header += r'%\graphicspath{.}' + '\n'        
        self.tex_header += r'\usepackage{authblk}' + '\n'
        self.tex_header += r'\usepackage[backend=biber,style=alphabetic,sorting=ynt]{biblatex}' + '\n'
        self.tex_header += r'\addbibresource{paraprobe-autoreporter.bib}' + '\n'
        self.tex_header += r'%landscape pages' + '\n'
        self.tex_header += r'\usepackage[landscape=true]{geometry}' + '\n'
        self.tex_header += r'%allow entire page floats' + '\n'
        self.tex_header += r'%each section a new page' + '\n'
        self.tex_header += r'\renewcommand{\floatpagefraction}{1.0}' + '\n'
        self.tex_header += r'\usepackage{titlesec}' + '\n'
        self.tex_header += r'\newcommand{\sectionbreak}{\clearpage}' + '\n'
        self.tex_header += '\n'
        return self.tex_header
        
    def add_authors_section(self):
        self.tex_authors = ''
        self.tex_authors += r'%header' + '\n'
        self.tex_authors += r'\title{' + self.title + r'}' + '\n'
        self.tex_authors += r'\author[1]{' + self.author + r'}' + '\n'
        self.tex_authors += r'%\author[2]{\paraprobe{}}' + '\n'
        self.tex_authors += r'%\author[1]{Markus K\"uhbach}' + '\n'
        self.tex_authors += r'\affil[1]{Max-Planck-Institut f\"ur Eisenforschung GmbH} %,\\ Max-Planck-Stra{\ss}e 1, D-40237 D\"usseldorf}' + '\n' #' \\ m.kuehbach@mpie.de}' + '\n'
        self.tex_authors += r'%\affil[2]{Automatic}' + '\n'
        self.tex_authors += r'\date{}  %% if you dont need date to appear' + '\n'
        self.tex_authors += r'\setcounter{Maxaffil}{0}' + '\n'
        self.tex_authors += r'\renewcommand\Affilfont{\itshape\small}' + '\n'
        self.tex_authors += '\n'        
        self.tex_authors += r'\begin{document}' + '\n'
        self.tex_authors += r'\maketitle' + '\n'
        self.tex_authors += '\n'
        self.tex_authors += r'\section*{Disclaimer}' + '\n'
        self.tex_authors += r'\dbg{This report has been auto-generated using \autoreporter{}. \\ It is the responsibility of the author to check the validity and correctness of these results!}' + '\n'
        self.tex_authors += '\n'
        return self.tex_authors
    
    def add_footer_section(self):
        self.tex_footer = ''
        self.tex_footer += '\n'
        self.tex_footer += r'%\section*{References}' + '\n'
        self.tex_footer += r'\printbibliography' + '\n'
        self.tex_footer += r'%\bibliography{paraprobe-autoreporter.bib} %.bib}' + '\n'
        self.tex_footer += r'\end{document}' + '\n'
        self.tex_footer += '\n'
        return self.tex_footer

    def write_report(self):
        """
        dump the txt to a tex file
        """      
        #build together the entire document from its sections
        report = ''
        #start
        report += self.add_header_section()
        report += self.add_authors_section()
        #results
        if self.tex_trns != '':
            report += self.tex_trns
        if self.tex_synt != '':
            report += self.tex_synt
        #report += '\newpage' + '\n'
        report += self.tex_rngr
        report += self.tex_surf
        report += self.tex_tess
        report += self.tex_spst
        report += self.tex_dbsc
        report += self.tex_arau
        report += self.tex_prof
        #end
        report += self.add_footer_section()
        #write to file
        with open(self.fn,'w') as f:
             f.write(report)

    #implementation of tool-specific sections in the report
    def add_section_transcoder(self, resdict, authcomments, *args, **kwargs):
        """
        #rngr is a paraprobe_autoreporter_ranger class object with the data
        resdict is a dictionary
        authcomments is a r'string' author comment preceeding the results
        this can be latex code
        """
        self.tex_trns = ''
        self.tex_trns += r'\section{\transcoder{}}' + '\n'
        self.tex_trns += authcomments + '\n'
        self.tex_trns += '\n'
        self.tex_trns += r'The dataset contains ' + str(resdict['NumberOfAtoms']) + r' ions in total.' + '\n'
        tbdct = {}
        tbdct = resdict['Config']
        tbdct['Header'] = ['Value', 'Unit', 'Description']
        #print('Plotting...')
        #for ky in tbdct.keys():
        #    print(np.shape(ky))
        mtrx = table_matrix_settings( tbdct, r'Which XML settings were used for \transcoder{}?', r'TblTranscoderSettings' )
        self.tex_trns += mtrx.tex
        self.tex_trns += '\n'
 
    def add_section_synthetic(self, resdict, authcomments, *args, **kwargs):
        """
        #rngr is a paraprobe_autoreporter_synthetic class object with the data
        resdict is a dictionary
        authcomments is a r'string' author comment preceeding the results
        this can be latex code
        """
        self.tex_synt = ''
        self.tex_synt += r'\section{\synthetic{}}' + '\n'
        self.tex_synt += authcomments + '\n'
        self.tex_synt += '\n'
        self.tex_synt += r'The dataset contains ' + str(resdict['NumberOfAtoms']) + r' ions in total.' + '\n'
        tbdct = {}
        tbdct = resdict['Config']
        tbdct['Header'] = ['Value', 'Unit', 'Description']
        #print('Plotting...')
        #for ky in tbdct.keys():
        #    print(np.shape(ky))
        mtrx = table_matrix_settings( tbdct, r'Which XML settings were used for \synthetic{}?', r'TblSyntheticSettings' )
        self.tex_synt += mtrx.tex
        self.tex_synt += '\n'
        
    def add_section_ranger(self, resdict, authcomments, *args, **kwargs):
        """
        #rngr is a paraprobe_autoreporter_ranger class object with the data
        resdict is a dictionary
        authcomments is a r'string' author comment preceeding the results
        this can be latex code
        """
        self.tex_rngr = ''
        self.tex_rngr += r'\section{\ranger{}}' + '\n'
        self.tex_rngr += authcomments + '\n'
        self.tex_rngr += '\n'
        fgdct = {}
        fgdct['Composition'] = resdict['CompositionFigure']
        if fgdct['Composition'] != None:
            mtrx = figure_matrix( fgdct, r'What is the composition for the entire dataset?', r'FigIonComposition', 0.75 )
            self.tex_rngr += mtrx.tex
        self.tex_rngr += '\n'
        fgdct = {}
        fgdct['MassToChargeFigure'] = resdict['MassToChargeFigure']
        mtrx = figure_matrix( fgdct, r'What is the mass-to-charge histogram/diagram of the entire dataset?', r'FigMass2Charge', 1.0 )
        self.tex_rngr += mtrx.tex
        self.tex_rngr += '\n'
        tbdct = {}
        tbdct = resdict['Config']
        tbdct['Header'] = ['Value', 'Unit', 'Description']
        #print('Plotting...')
        #for ky in tbdct.keys():
        #    print(np.shape(ky))
        mtrx = table_matrix_settings( tbdct, r'Which XML settings were used for \ranger{}?', r'TblRangerSettings' )
        self.tex_rngr += mtrx.tex
        self.tex_rngr += '\n'
   
    def add_section_surfacer(self, resdict, authcomments, *args, **kwargs):
        """
        #surf is a paraprobe_autoreporter_surfacer class object with the data
        resdict is a dictionary
        authcomments is a r'string' author comment preceeding the results
        this can be latex code
        """
        self.tex_surf = ''
        self.tex_surf += r'\section{\surfacer{}}' + '\n'
        self.tex_surf += authcomments + '\n'
        self.tex_surf += '\n'
        fgdct = {}
        fgdct['Ion2EdgeDistance'] = resdict['Ion2EdgeDistanceFigure']
        mtrx = figure_matrix( fgdct, r'How many ions are so and so far away from the dataset edge?', r'FigIon2EdgeDistance', 0.75 )
        self.tex_surf += mtrx.tex
        self.tex_surf += '\n'
        tbdct = {}
        tbdct = resdict['Config']
        tbdct['Header'] = ['Value', 'Unit', 'Description']
        #print('Plotting...')
        #for ky in tbdct.keys():
        #    print(np.shape(ky))
        mtrx = table_matrix_settings( tbdct, r'Which XML settings were used for \surfacer{}?', r'TblSurfacerSettings' )
        self.tex_surf += mtrx.tex
        self.tex_surf += '\n'
        
    def add_section_tessellator(self, resdict, authcomments, *args, **kwargs):
        """
        #tess is a paraprobe_autoreporter_tessellator class object with the data
        resdict is a dictionary
        authcomments is a r'string' author comment preceeding the results
        this can be latex code
        """
        self.tex_tess = ''
        self.tex_tess += r'\section{\tessellator{}}' + '\n'
        self.tex_tess += authcomments + '\n'
        self.tex_tess += '\n'
        fgdct = {}
        fgdct['VoronoiSizeDistroFigure'] = resdict['VoronoiSizeDistroFigure']
        mtrx = figure_matrix( fgdct, r'What is the distribution of volume for the Voronoi cells?', r'FigVoroSizeDistro', 0.75 )
        self.tex_tess += mtrx.tex
        self.tex_tess += '\n'
        fgdct = {}
        fgdct['SumVoronoiVolvsDistanceFigure'] = resdict['SumVoronoiVolvsDistanceFigure']
        mtrx = figure_matrix( fgdct, r'How much accumulated volume of Voronoi cells remains when successively eroding Voronoi cells from the dataset edge towards the interior?', r'FigVoroVolVsDistance', 0.75 )
        self.tex_tess += mtrx.tex
        self.tex_tess += '\n'        
        tbdct = {}
        tbdct = resdict['Config']
        tbdct['Header'] = ['Value', 'Unit', 'Description']
        #print('Plotting...')
        #for ky in tbdct.keys():
        #    print(np.shape(ky))
        mtrx = table_matrix_settings( tbdct, r'Which XML settings were used for \tessellator{}?', r'TblTessellatorSettings' )
        self.tex_tess += mtrx.tex
        self.tex_tess += '\n'
    
    def add_section_spatstat(self, resdict, authcomments, *args, **kwargs):
        """
        #spst is a paraprobe_autoreporter_spatstat class object with the data
        resdict is a dictionary
        authcomments is a r'string' author comment preceeding the results
        this can be latex code
        """
        self.tex_spst = ''
        self.tex_spst += r'\section{\spatstat{}}' + '\n'
        self.tex_spst += authcomments + '\n'
        self.tex_spst += '\n'
        i = 1
        for key in resdict['RDF'].keys():
            fgdct = {}
            fgdct[key] = resdict['RDF'][key]
            mtrx = figure_matrix( fgdct, r'What is the radial distribution function for particular ion types about specific ion types?', r'FigRDF' + str(i), 0.75 )
            i += 1
            self.tex_spst += mtrx.tex
            self.tex_spst += '\n'
        i = 1
        for key in resdict['KNN'].keys():
            fgdct = {}
            fgdct[key] = resdict['KNN'][key]
            mtrx = figure_matrix( fgdct, r'What is the kth nearest neighbor distribution function for particular ion types about specific ion types?', r'FigKNN' + str(i), 0.75 )
            i += 1
            self.tex_spst += mtrx.tex
            self.tex_spst += '\n'
        i = 1
        for key in resdict['SDM'].keys():
            fgdct = {}
            fgdct[key] = resdict['SDM'][key]
            mtrx = figure_matrix( fgdct, r'What is the 3D spatial distribution map for particular ion types about specific ion types?', r'FigSDM' + str(i), 0.75 )
            i += 1
            self.tex_spst += mtrx.tex
            self.tex_spst += '\n'
        i = 1
        for key in resdict['USER'].keys():
            fgdct = {}
            fgdct[key] = resdict['USER'][key]
            mtrx = figure_matrix( fgdct, r'Result of user-defined analysis?', r'FigUSER' + str(i), 0.75 )
            i += 1
            self.tex_spst += mtrx.tex
            self.tex_spst += '\n'
        tbdct = {}
        tbdct = resdict['Config']
        tbdct['Header'] = ['Value', 'Unit', 'Description']
        #print('Plotting...')
        #for ky in tbdct.keys():
        #   print(np.shape(ky))
        mtrx = table_matrix_settings( tbdct, r'Which XML settings were used for \spatstat{}?', r'TblSpatstatSettings' )
        self.tex_spst += mtrx.tex
        self.tex_spst += '\n'

    def add_section_dbscan(self, resdict, authcomments, *args, **kwargs):
        """
        #dbsc is a paraprobe_autoreporter_dbscan class object with the data
        resdict is a dictionary
        authcomments is a r'string' author comment preceeding the results
        this can be latex code
        """
        self.tex_dbsc = ''
        self.tex_dbsc += r'\section{\dbscan{}}' + '\n'
        self.tex_dbsc += authcomments + '\n'
        self.tex_dbsc += '\n'
        if len(resdict['SizeDistros'].keys()) > 0:
            self.tex_dbsc += r'\paragraph{Distribution of number of ions per cluster}' + '\n'
        i = 1
        for key in resdict['SizeDistros'].keys():
            #if key != 'Config':
            fgdct = {}
            fgdct[key] = resdict['SizeDistros'][key]
            mtrx = figure_matrix( fgdct, r'What are the distribution of cluster sizes, based on the number of atoms per cluster?', r'FigDBScanGSD' + str(i), 0.75 )
            i += 1
            self.tex_dbsc += mtrx.tex
            self.tex_dbsc += '\n'
        if len(resdict['HighThroughput'].keys()) > 0:
            self.tex_dbsc += r'\paragraph{Number density of clusters composed from task collection}' + '\n'
        i = 1
        for key in resdict['HighThroughput'].keys():
            #if key != 'Config':
            fgdct = {}
            fgdct[key] = resdict['HighThroughput'][key]
            mtrx = figure_matrix( fgdct, r'What are the number of clusters per unit volume, quantified using high-throughput studies of the DBScan/maximum separation clustering parameter?', r'FigDBScanHighThroughput' + str(i), 0.75 )
            i += 1
            self.tex_dbsc += mtrx.tex
            self.tex_dbsc += '\n'
        if len(resdict['Config'].keys()) > 0:
            self.tex_dbsc += r'\paragraph{Metadata}' + '\n'
        tbdct = {}
        tbdct = resdict['Config']
        tbdct['Header'] = ['Value', 'Unit', 'Description']
        #print('Plotting...')
        #for ky in tbdct.keys():
        #    print(np.shape(ky))
        mtrx = table_matrix_settings( tbdct, r'Which XML settings were used for \dbscan{}?', r'TblDBScanSettings' )
        self.tex_dbsc += mtrx.tex
        self.tex_dbsc += '\n'
        
    def add_section_araullo(self, resdict, authcomments, *args, **kwargs):
        """
        #arau is a paraprobe_autoreporter_araullo class object with the data
        resdict is a dictionary
        authcomments is a r'string' author comment preceeding the results
        this can be latex code
        """
        self.tex_arau = ''
        self.tex_arau += r'\section{\araullo{}}' + '\n'
        self.tex_arau += authcomments + '\n'
        self.tex_arau += '\n'
        i = 1
        for key in resdict['KeyQuantiles'].keys():
            #if key != 'Config':
            fgdct = {}
            fgdct[key] = resdict['KeyQuantiles'][key]
            mtrx = figure_matrix( fgdct, r'For each ROI and its specific image intensities, report me the largest image intensity. Do so for all ROIs. Then compare me for how many ROIs we have at all substantial signal, (the closer to 1.0) the better, and significant signal (the more ions included in the ROI sphere the more significant)?', r'FigAraulloKeyQuants' + str(i), 0.75 )
            i += 1
            self.tex_arau += mtrx.tex
            self.tex_arau += '\n'
        tbdct = {}
        tbdct = resdict['Config']
        tbdct['Header'] = ['Value', 'Unit', 'Description']
        #print('Plotting...')
        #for ky in tbdct.keys():
        #    print(np.shape(ky))
        mtrx = table_matrix_settings( tbdct, r'Which XML settings were used for \araullo{}?', r'TblAraulloSettings' )
        self.tex_arau += mtrx.tex
        self.tex_arau += '\n'   
    
    def add_section_fourier(self, resdict, authcomments, *args, **kwargs):
        """
        #four is a paraprobe_autoreporter_fourier class object with the data
        resdict is a dictionary
        authcomments is a r'string' author comment preceeding the results
        this can be latex code
        """
        self.tex_four = ''
        self.tex_four += r'\section{\fourier{}}' + '\n'
        self.tex_four += authcomments + '\n'
        self.tex_four += '\n'
        tbdct = {}
        tbdct = resdict['Config']
        tbdct['Header'] = ['Value', 'Unit', 'Description']
        mtrx = table_matrix_settings( tbdct, r'Which XML settings were used for \fourier{}?', r'TblFourierSettings' )
        self.tex_four += mtrx.tex
        self.tex_four += '\n'

    def add_section_profiling(self, resdict, authcomments, *args, **kwargs):
        """
        resdict is a dictionary
        authcomments is a r'string' author comment preceeding the results
        this can be latex code
        """
        self.tex_prof = ''
        self.tex_prof += r'\section{Profiling}' + '\n'
        self.tex_prof += authcomments + '\n'
        self.tex_prof += '\n'
        tbdct = {}
        tbdct = resdict
        tbdct['Header'] =  [r'Toolname', r'\#MPI', r'\#OMP', r'$t_{elapsed}^{max}$ (s)']
        #print('Plotting...')
        #for ky in tbdct.keys():
        #    print(np.shape(ky))
        mtrx = table_matrix_profiling( tbdct, r'How many MPI processes times OpenMP threads respectively were used, and what was the elapsed time for each tool run?', r'TblProfiling' )
        self.tex_prof += mtrx.tex
        self.tex_prof += '\n' 
