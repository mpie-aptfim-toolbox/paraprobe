# -*- coding: utf-8 -*-
"""
created 2020/06/19, Markus K\"uhbach, m.kuehbach@mpie.de
utility class for organizing the plotting of generic Latex figures using dictionaries
"""

import os
import sys
#sys.path.append('../src/')
import glob, subprocess
import pandas as pd
import numpy as np

class figure_matrix():
    
    def __init__(self, figdict, caption, lbl, imgwdth, *arg, **kwargs):
        """
        figdict is a dictionary with string values pointing to figure files (e.g. png, tif)
        scapdict is a dictionary with string values reading sub-captions; one for each figure
        caption is a string giving the figure caption
        lbl is label of the figure
        imgwdth is the image width
        """
        #if len(figdict.keys()) != len(scapdict.keys()):
        #    raise ValueError('Not every figure has a sub-caption, this is currently not allowed !')
        #for key in figdict.keys():
        #    if key in scapdict:
        #        continue
        #    else:
        #        raise ValueError('The dictionaries for the figure filenames and the sub-captions need to use the same keys !')
        #generate the tex source code
        self.tex = '\n'
        self.tex += r'\begin{figure}[!ht]' + '\n'
        self.tex += r'\centering' + '\n'
        for key in figdict.keys():
            if figdict[key] is not None:
                slbl = lbl + str(key)
                self.tex += r'\subfloat[][]{\includegraphics[width=' + str(imgwdth) + r'\textwidth]{' + figdict[key] + '}\label{' + slbl + '}}' + '\n'
                #no scapdict
        self.tex += '\caption{' + caption + '}' + '\n'
        self.tex += '\label{' + lbl + '}' + '\n'
        self.tex += '\end{figure}' + '\n'
