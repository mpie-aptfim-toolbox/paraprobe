# -*- coding: utf-8 -*-
"""
created 2020/06/07, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for strongly automatized reporting
of results from the paraprobe-dbscan tool
"""

import os, sys
import glob, subprocess
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
try:
    import periodictable as pse
except ImportError as e:
    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')
try:
    import h5py
except ImportError as e:
    raise ValueError('Install h5py Python package via e.g. pip install h5py !')

from PARAPROBE_Autoreporter_Numerics import *
from PARAPROBE_Autoreporter_CorpDesign import *
from PARAPROBE_Autoreporter_DecodeIontypeHash import *
from PARAPROBE_Autoreporter_Plots import *

from PARAPROBE_Autoreporter_MetadataTranscoder import *
from PARAPROBE_Autoreporter_MetadataSurfacer import *
from PARAPROBE_Autoreporter_MetadataTessellator import *
from PARAPROBE_Autoreporter_MetadataDBScan import *

from PARAPROBE_Autoreporter_Tessellator import *

class dbscan_task():
    def __init__(self, h5fn, tasknm):
        #fn = 'PARAPROBE.DBScan.Results.SimID.7.h5'
        fn = h5fn
        hf = h5py.File( fn, 'r' )
        #tell me which groups, sub-groups, and datasets exists in the HDF5 file
        tasks = list(hf[PARAPROBE_DBSC_RES])
        if tasknm in tasks:
            #get the metadata first
            #tsk = 'Task0'
            thismeta = list(hf[PARAPROBE_DBSC_META + '/' + tasknm])
            self.taskname = tasknm
            self.targets = ''
            #get target single/molecular ion type
            if 'IontypeTargets' in thismeta:
                #the format of these type keys is as follows:
                #first entry is the corresponding ion type key, the remainder 8x1 uint8 nparray is the molecular ion hash
                tg = []
                tg = hf[PARAPROBE_DBSC_META + '/' + tasknm + '/IontypeTargets'][:,:]
                nrows = np.shape(tg)[0] ###MK::handle multiple ions
                if nrows > 0:
                    for row in np.arange(0,nrows):
                        self.targets += decode_molecularion(tg[row,1:9])
                        if row != nrows-1:
                            self.targets += ';'
                else:
                    raise ValueError('Unknown iontype targets, while parsing ' + PARAPROBE_DBSC_META + '/' + tasknm + '/IontypeTargets')
            else:
                raise ValueError('IontypeTargets does not exist in ' + h5fn + ' !')
            if 'Epsilon' in thismeta:
                self.eps = hf[PARAPROBE_DBSC_META + '/' + tasknm + '/' + PARAPROBE_DBSC_META_EPS][0,0]
            else:
                raise ValueError('Epsilon does not exist in ' + h5fn + ' !')
            if 'KthNearestNeighbor' in thismeta:
                self.kth = hf[PARAPROBE_DBSC_META + '/' + tasknm + '/' + PARAPROBE_DBSC_META_KTH][0,0]
            else:
                raise ValueError('KthNearestNeighbor does not exist in ' + h5fn + ' !')
            if 'MinNumberOfIonsForSignificantCluster' in thismeta:
                self.minpts = hf[PARAPROBE_DBSC_META + '/' + tasknm + '/' + PARAPROBE_DBSC_META_MINPTS][0,0]
            else:
                raise ValueError('MinNumberOfIonsForSignificantCluster does not exist in ' + h5fn + ' !')        

config_dbscan = {}

class autoreporter_dbscan():
    
    def __init__(self, ions, dbsc , *args, **kwargs):
        #self.simid = simid
        self.toolname = 'DBScan'
        self.h5src_ions = ions
        self.h5src_dbsc = dbsc
        #extract the global dataset edge threshold distance below which clusters with ions with such a small distance are
        #expected trunctated
        #fn = 'PARAPROBE.DBScan.Results.SimID.26120.h5'
        hf = h5py.File( self.h5src_dbsc, 'r' )
        self.dthr = np.float32(0.0)
        grpnm = '/SoftwareDetails/Metadata'
        if grpnm in hf.keys(): #old location of metadata, will become deprecated
            grpnm += '/Keywords'
        else: #new location of metadata
            grpnm = PARAPROBE_DBSC_META_SFTWR
        print('Parsing configuration from group ' + grpnm)
        self.dthr = np.float32(hf[grpnm + '/DatasetEdgeThresholdDistance'][0].decode()) #in nm
        #self.dthr = np.float32(hf['SoftwareDetails/Metadata/Keywords/DatasetEdgeThresholdDistance'][0].decode()) #in nm
        
        #add here all functions which parse by default results from the source
        self.config = config_dbscan
        self.results = {}
        self.get_tool_config()
        self.results['SizeDistros'] = {}
        self.results['HighThroughput'] = {}        
        #self.gsd_img = {}
    
    def get_tool_config(self):
        #fn = 'C:/Users/kaiob/eclipse-workspace/mpie-aptfim-toolbox/paraprobe-autoreporter/HuanZhaoScrMatCaseStudy/PARAPROBE.Transcoder.Results.SimID.26120.h5'
        fn = self.h5src_dbsc
        hf = h5py.File( fn, 'r' )
        self.config = config_dbscan
        #for key in self.config.keys(): #replace the defaults by the actual values
        #    print(key)
        #    tmp = hf['/SoftwareDetails/Metadata/Keywords/' + key][:]
        #    self.config[key] = [ tmp[0].decode(), tmp[1].decode(), tmp[2].decode() ]
        #above reads what the config_surfacer default dictates, instead
        #to read all entries use
        grpnm = '/SoftwareDetails/Metadata'
        if grpnm in hf.keys(): #old location of metadata, will become deprecated
            grpnm += '/Keywords'
        else: #new location of metadata
            grpnm = PARAPROBE_DBSC_META_SFTWR
        print('Parsing configuration from group ' + grpnm)
        keys = list(hf[grpnm])
        #keys = list(hf['/SoftwareDetails/Metadata/Keywords/'])
        self.config = {}
        for key in keys:
            print(key)
            tmp = hf[grpnm + '/' + key][:]
            #tmp = hf['/SoftwareDetails/Metadata/Keywords/' + key][:]
            self.config[key] = [ tmp[0].decode(), tmp[1].decode(), tmp[2].decode() ]
        self.results['Config'] = self.config
    
    def get_cluster_size_distros(self, eps, kth, minpts):
        """
        read computed cluster size distributions from h5src_dbsc if any
        """
        #first, build a dictionary of different targets we have for DBScan tasks with kth == kth
        trgs = {}
        #fn = 'PARAPROBE.DBScan.Results.SimID.2765601.h5'
        fn = self.h5src_dbsc
        hf = h5py.File( fn, 'r' )
        #tell me which groups, sub-groups, and datasets exists in the HDF5 file
        tasks = list(hf[PARAPROBE_DBSC_RES])
        #kth = 1
        #minpts = 5
        for tsk in tasks:
            tsk_metadata = dbscan_task( fn, tsk )
            #build a dictionary of list of task objects
            #the dbscan_task class objects hold the metadata and the paraprobe-dbscan taskname
            #this allows to compile a diagram for each target individually
            if abs(tsk_metadata.eps - eps) < 0.001: #filter out the relevant tasks ##MK::hope work to do
                if tsk_metadata.kth == kth:
                    if tsk_metadata.minpts == minpts: #build dictionary of dictionarys
                        if tsk_metadata.targets in trgs.keys(): #add a task for existing targets
                            trgs[tsk_metadata.targets].append( tsk_metadata )
                        else: #add the first task for a not-yet existing target
                            trgs[tsk_metadata.targets] = []
                            trgs[tsk_metadata.targets].append( tsk_metadata )
        #now that we know the iontype specis and DBScan results we have we want to read how many cluster per task
        if trgs != {}:
            for key in trgs.keys():
                for obj in trgs[key]:
                    #obj = trgs['Sc;ScH:'][100]
                    thisres = list(hf[PARAPROBE_DBSC_RES + '/' + obj.taskname])
                    if PARAPROBE_DBSC_RES_CDFRAW in thisres and PARAPROBE_DBSC_RES_CDFCLN in thisres:
                        cdf_raw = hf[PARAPROBE_DBSC_RES + '/' + obj.taskname + '/' + PARAPROBE_DBSC_RES_CDFRAW][:,:]
                        cdf_cln = hf[PARAPROBE_DBSC_RES + '/' + obj.taskname + '/' + PARAPROBE_DBSC_RES_CDFCLN][:,:]
                        eps = hf[PARAPROBE_DBSC_META + '/' + obj.taskname + '/' + PARAPROBE_DBSC_META_EPS][0,0]
                        kth = hf[PARAPROBE_DBSC_META + '/' + obj.taskname + '/' + PARAPROBE_DBSC_META_KTH][0,0]
                        minpts = hf[PARAPROBE_DBSC_META + '/' + obj.taskname + '/' + PARAPROBE_DBSC_META_MINPTS][0,0]
                        dthr = hf[PARAPROBE_DBSC_META + '/' + obj.taskname + '/' + PARAPROBE_DBSC_META_THRS][0,0]
     
                        imgfn = (fn + PARAPROBE_DBSC_RES + '/' + obj.taskname + '.GSD').replace('/','.').replace(';','.').replace(':','.')
                        imgttl = (obj.taskname + ', ' + obj.targets + ', ' + r'$\epsilon = $' + str(np.around(np.float64(eps), decimals=2)) + 'nm' + ', ' + r'$k^{th} = $' + str(np.uint32(kth)) + ', ' + r'$N_{min} = $' + str(np.uint32(minpts)) + ', ' + r'$d_{thr} = $' + str(np.around(np.float64(dthr), decimals=2)) + 'nm').replace('/','.').lstrip('.')
                        imglgnd = ['All clusters', 'Only interior cluster']
                        print(imgfn)
                        
                        #render the figure 
                        self.results['SizeDistros'][imgfn] = plot_cluster_size_distro( imgfn, cdf_raw, cdf_cln, imgttl, imglgnd )
        
        #fn = 'C:/Users/kaiob/eclipse-workspace/mpie-aptfim-toolbox/paraprobe-dbscan/PARAPROBE.DBScan.Results.SimID.7.h5'
        #fn = 'PARAPROBE.DBScan.Results.SimID.7.h5'
        #fn = self.h5src_dbsc
        #hf = h5py.File( fn, 'r' )
        #tell me which groups, sub-groups, and datasets exists in the HDF5 file
        #tasks = list(hf[PARAPROBE_DBSC_RES])
        #for tsk in tasks:
        #    if 'Task' in tsk:
        #        #tsk = tasks[0]
        #        print(tsk)
        #        #get the metadata first
        #        thismeta = list(hf[PARAPROBE_DBSC_META + '/' + tsk])
        #        targets = ''
        #        #get target single/molecular ion type
        #        if 'IontypeTargets' in thismeta:
        #            #the format of these type keys is as follows:
        #            #first entry is the corresponding ion type key, the remainder 8x1 uint8 nparray is the molecular ion hash
        #            tg = []
        #            tg = hf[PARAPROBE_DBSC_META + '/' + tsk + '/IontypeTargets'][:,:]
        #            nrows = np.shape(tg)[0] ###MK::handle multiple ions
        #            if nrows > 0:
        #                for row in np.arange(0,nrows):
        #                    targets += decode_molecularion(tg[row,1:9])
        #                    if row != nrows-1:
        #                        targets += ';'
        #            else:
        #                raise ValueError('Unknown iontype targets, while parsing ' + PARAPROBE_DBSC_META + '/' + tsk + '/IontypeTargets')
        #        print('Targets ' + targets)
        #        #plotting size distribution
        #        thisres = list(hf[PARAPROBE_DBSC_RES + '/' + tsk])
        #        if PARAPROBE_DBSC_RES_CDFRAW in thisres and PARAPROBE_DBSC_RES_CDFCLN in thisres:
        #            cdf_raw = hf[PARAPROBE_DBSC_RES + '/' + tsk + '/' + PARAPROBE_DBSC_RES_CDFRAW][:,:]
        #            cdf_cln = hf[PARAPROBE_DBSC_RES + '/' + tsk + '/' + PARAPROBE_DBSC_RES_CDFCLN][:,:]
        #            eps = hf[PARAPROBE_DBSC_META + '/' + tsk + '/' + PARAPROBE_DBSC_META_EPS][0,0]
        #            kth = hf[PARAPROBE_DBSC_META + '/' + tsk + '/' + PARAPROBE_DBSC_META_KTH][0,0]
        #            minpts = hf[PARAPROBE_DBSC_META + '/' + tsk + '/' + PARAPROBE_DBSC_META_MINPTS][0,0]
        #            dthr = hf[PARAPROBE_DBSC_META + '/' + tsk + '/' + PARAPROBE_DBSC_META_THRS][0,0]
 
        #            imgfn = (fn + PARAPROBE_DBSC_RES + '/' + tsk + '.GSD').replace('/','.').replace(';','.')
        #            imgttl = (tsk + ', ' + targets + ', ' + r'$\epsilon = $' + str(np.around(np.float64(eps), decimals=2)) + 'nm' + ', ' + r'$k^{th} = $' + str(np.uint32(kth)) + ', ' + r'$N_{min} = $' + str(np.uint32(minpts)) + ', ' + r'$d_{thr} = $' + str(np.around(np.float64(dthr), decimals=2)) + 'nm').replace('/','.').lstrip('.')
        #            imglgnd = ['All clusters', 'Only interior cluster']
                    
        #            #render the figure 
        #            self.results['SizeDistros'][imgfn] = plot_cluster_size_distro( imgfn, cdf_raw, cdf_cln, imgttl, imglgnd )
                        
    def get_number_density_vs_eps_minpts(self, kth, minpts, vol):
        """
        compose a 3D diagram how many cluster per dataset volume = f( eps, minPts ) @fixed kth and fixed marking of cluster to boundary
        the dataset volume is computed from the aggregated Voronoi cell volume
        """
        ###MK::currently taking the same volume to normalize the cluster numbers!        
        #first, build a dictionary of different targets we have for DBScan tasks with kth == kth
        trgs = {}
        #fn = 'PARAPROBE.DBScan.Results.SimID.7.h5'
        fn = self.h5src_dbsc
        hf = h5py.File( fn, 'r' )
        #tell me which groups, sub-groups, and datasets exists in the HDF5 file
        tasks = list(hf[PARAPROBE_DBSC_RES])
        #kth = 1
        #minpts = 5
        for tsk in tasks:
            tsk_metadata = dbscan_task( fn, tsk )
            #build a dictionary of list of task objects
            #the dbscan_task class objects hold the metadata and the paraprobe-dbscan taskname
            #this allows to compile a diagram for each target individually
            if tsk_metadata.kth == kth:
                if tsk_metadata.minpts == minpts: #build dictionary of dictionarys
                    if tsk_metadata.targets in trgs.keys(): #add a task for existing targets
                        trgs[tsk_metadata.targets].append( tsk_metadata )
                    else: #add the first task for a not-yet existing target
                        trgs[tsk_metadata.targets] = []
                        trgs[tsk_metadata.targets].append( tsk_metadata )
        #now that we know the iontype specis and DBScan results we have we want to read how many cluster per task
        if trgs != {}:
            for key in trgs.keys():
                #collect data x == eps/dmax, y = minpts/Nmin, z = #cluster / volume tip
                X = []
                Y = []
                Zraw = []
                Zcln = []
                for obj in trgs[key]:
                    #print(obj.taskname)
                    #get the number of clusters for that task
                    if PARAPROBE_DBSC_RES + '/' + obj.taskname + '/' + PARAPROBE_DBSC_RES_CDFRAW in hf and \
                       PARAPROBE_DBSC_RES + '/' + obj.taskname + '/' + PARAPROBE_DBSC_RES_CDFCLN in hf:
                        n_raw = np.shape(hf[PARAPROBE_DBSC_RES + '/' + obj.taskname + '/' + PARAPROBE_DBSC_RES_CDFRAW][:,:])[0]
                        n_cln = np.shape(hf[PARAPROBE_DBSC_RES + '/' + obj.taskname + '/' + PARAPROBE_DBSC_RES_CDFCLN][:,:])[0]
                        #report only those results which exist
                        X.append( obj.eps )
                        Y.append( obj.minpts )
                        Zraw.append( n_raw / vol * 1.0e27 ) #1 nm-^3 to 1 m^-3
                        Zcln.append( n_cln / vol * 1.0e27 )
                    
                imgfn = (fn + PARAPROBE_DBSC_RES + '/' + key + '.NumberDensityVsEps').replace('/','.').replace(';','.').replace(':','.')
                #add dthr
                imgttl = (key + ', ' + r'$minPts = $' + str(np.uint32(minpts)) + ', ' + r'$k^{th} = $' + str(np.uint32(kth))).replace('/','.').lstrip('.')
                imglgnd = ['All clusters', 'Only interior cluster']
            
                #render the figure 
                self.results['HighThroughput'][imgfn] = plot_cluster_numberdensity_vs_eps( imgfn, np.array(X, np.float64), np.array(Zraw, np.float64), np.array(Zcln, np.float64), imgttl, imglgnd )
                #from mpl_toolkits.mplot3d import Axes3D
                #fig = plt.figure()
                #ax = fig.add_subplot(111, projection='3d')
                #ax.scatter( X, Y, Zraw, zdir='z', s=20, c=MYPARULA[0], depthshade=True)
                #ax.set_title('title')
                #ax.legend( ['test'], loc='upper right') 
                #plt.xlabel(r'$\epsilon / d_{max}$ (nm)')
                #plt.ylabel(r'$minPts / N_{min}$')
                #plt.zlabel(r'Cluster number density ($m^{-3}$)')
                #plt.set_tight()
        else:
            raise ValueError('We did not found any results for kth = ' + str(kth) + ' !')
            
    def report(self):
        return self.results

# =============================================================================
#example usage, tell me how many ions of a certain type
#task = autoreporter_dbscan( 'PARAPROBE.Transcoder.Results.SimID.7.apth5', 'PARAPROBE.DBScan.Results.SimID.7.h5' )
#evaluates by default all cluster size distributions and renders them into human-readable figures
#task.get_cluster_size_distros()
#evaluates if data exist for the maximum separation method with kth=1 the cluster number density as a function of eps/minPts
#i.e. dmax and Nmin, to compute a number density we need a reference volume this volume could be the volume of the tip
#here we show how to compute it from the Voronoi cell volume of all those ions farther away from the dataset edge than threshold
#tess = autoreporter_tessellator( 'PARAPROBE.Transcoder.Results.SimID.7.apth5',   \
#                                 'PARAPROBE.Surfacer.Results.SimID.7.h5',        \
#                                 'PARAPROBE.Tessellator.Results.SimID.7.Stats.h5' )
#define here the threshold (in nm)
#dthr = 1.0
#vol = tess.get_sum_voronoi_volume( dthr )
#define for which fixed kth and minpts we want to visualize the number density
#kth = 1
#minpts = 5
#task.get_number_density_vs_eps_minpts( kth, minpts, vol )
# =============================================================================
