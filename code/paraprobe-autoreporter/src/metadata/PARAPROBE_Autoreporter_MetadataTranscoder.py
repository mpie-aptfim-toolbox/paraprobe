# -*- coding: utf-8 -*-
"""
created 2020/05/28, Markus K\"uhbach, m.kuehbach@mpie.de
definitions for HDF5 groups and datasets for results of the paraprobe-transcoder tool
"""

PARAPROBE_TRANS='/Transcoder'
PARAPROBE_TRANS_HRDWR='/Transcoder/HardwareDetails'
PARAPROBE_TRANS_HRDWR_META='/Transcoder/HardwareDetails/Metadata'
PARAPROBE_TRANS_HRDWR_META_KEYS='/Transcoder/HardwareDetails/Metadata/Keywords'
PARAPROBE_TRANS_SFTWR='/Transcoder/SoftwareDetails'
PARAPROBE_TRANS_SFTWR_META='/Transcoder/SoftwareDetails/Metadata'
PARAPROBE_TRANS_SFTWR_META_KEYS='/Transcoder/SoftwareDetails/Metadata/Keywords'

PARAPROBE_TRANS_META='/Transcoder/Metadata'
PARAPROBE_TRANS_META_HRDWR='/VolumeRecon/Metadata/ToolEnvironment'
PARAPROBE_TRANS_META_SFTWR='/VolumeRecon/Metadata/ToolConfiguration'

PARAPROBE_TRANS_RES='/VolumeRecon' #outdated will become '/Transcoder/ReconstructionSpace/Reconstructions/Task00/Results' in the future...
PARAPROBE_TRANS_RES_XYZ='/VolumeRecon/Results/XYZ'
PARAPROBE_TRANS_RES_XYZ_NCMAX=3
PARAPROBE_TRANS_RES_MQ='/VolumeRecon/Results/MQ'
PARAPROBE_TRANS_RES_MQ_NCMAX=1
###MK::not for TOPO
