# -*- coding: utf-8 -*-
"""
created 2020/06/11, Markus K\"uhbach, m.kuehbach@mpie.de
definitions for HDF5 groups and datasets for results of the paraprobe-tessellator tool
"""

PARAPROBE_TESS='/VoroTess'
PARAPROBE_TESS_META='/VoroTess/Metadata'
PARAPROBE_TESS_META_HRDWR='/VoroTess/Metadata/ToolEnvironment'
PARAPROBE_TESS_META_SFTWR='/VoroTess/Metadata/ToolConfiguration'

PARAPROBE_TESS_META_DERO='/VoroTess/Metadata/CellErosionDistance'
PARAPROBE_TESS_META_DERO_NCMAX=1
PARAPROBE_TESS_META_PROCESSID='/VoroTess/Metadata/ProcessID'
PARAPROBE_TESS_META_PROCESSID_NCMAX=1
PARAPROBE_TESS_META_THREADID='/VoroTess/Metadata/ThreadID'
PARAPROBE_TESS_META_THREADID_NCMAX=1
PARAPROBE_TESS_META_WORKPART='/VoroTess/Metadata/Workpartitioning'
PARAPROBE_TESS_META_WORKPART_NCMAX=1

PARAPROBE_TESS_RES='/VoroTess/Results'
PARAPROBE_TESS_RES_ID='/VoroTess/Results/IonIDCellID'
PARAPROBE_TESS_RES_ID_NCMAX=1
PARAPROBE_TESS_RES_VOL='/VoroTess/Results/CellVolume'
PARAPROBE_TESS_RES_VOL_NCMAX=1
