# -*- coding: utf-8 -*-
"""
created 2020/05/28, Markus K\"uhbach, m.kuehbach@mpie.de
definitions for HDF5 groups and datasets for results of the paraprobe-surfacer tool
"""

PARAPROBE_SURF='/SurfaceRecon'
PARAPROBE_SURF_META='/SurfaceRecon/Metadata'
PARAPROBE_SURF_META_HRDWR='/SurfaceRecon/Metadata/ToolEnvironment'
PARAPROBE_SURF_META_SFTWR='/SurfaceRecon/Metadata/ToolConfiguration'

PARAPROBE_SURF_VXLBOX='/SurfaceRecon/Voxelization'
PARAPROBE_SURF_VXLBOX_META='/SurfaceRecon/Voxelization/Metadata'
PARAPROBE_SURF_VXLBOX_META_VXL_NXYZ='/SurfaceRecon/Voxelization/Metadata/VoxelDimsXYZ'
PARAPROBE_SURF_VXLBOX_META_VXL_NXYZ_NCMAX=3
PARAPROBE_SURF_VXLBOX_META_VXL_WIDTH='/SurfaceRecon/Voxelization/Metadata/VoxelWidthXYZ'
PARAPROBE_SURF_VXLBOX_META_VXL_WIDTH_NCMAX=3
PARAPROBE_SURF_VXLBOX_META_VXL_MIMX='/SurfaceRecon/Voxelization/Metadata/VoxelBoxMinMax'
PARAPROBE_SURF_VXLBOX_META_VXL_MIMX_NCMAX=2

PARAPROBE_SURF_CDIST='/SurfaceRecon/CoarseDists'
PARAPROBE_SURF_CDIST_META='/SurfaceRecon/CoarseDists/Metadata'
PARAPROBE_SURF_CDIST_META_VXL_NXYZ='/SurfaceRecon/CoarseDists/Metadata/VoxelDimsXYZ'
PARAPROBE_SURF_CDIST_META_VXL_NXYZ_NCMAX=3
PARAPROBE_SURF_CDIST_META_VXL_WIDTH='/SurfaceRecon/CoarseDists/Metadata/VoxelWidthXYZ'
PARAPROBE_SURF_CDIST_META_VXL_WIDTH_NCMAX=3
PARAPROBE_SURF_CDIST_META_VXL_MIMX='/SurfaceRecon/CoarseDists/Metadata/VoxelBoxMinMax'
PARAPROBE_SURF_CDIST_META_VXL_MIMX_NCMAX=2

PARAPROBE_SURF_VXLBOX_RES='/SurfaceRecon/Voxelization/Results'
PARAPROBE_SURF_VXLBOX_RES_VXL_STATE='/SurfaceRecon/Voxelization/Results/VoxelState'
PARAPROBE_SURF_VXLBOX_RES_VXL_STATE_NCMAX=1

PARAPROBE_SURF_CDIST_RES='/SurfaceRecon/CoarseDists/Results'
PARAPROBE_SURF_CDIST_RES_VXL_STATE='/SurfaceRecon/CoarseDists/Results/VoxelState'
PARAPROBE_SURF_CDIST_RES_VXL_STATE_NCMAX=1
PARAPROBE_SURF_CDIST_RES_VXL_DIST='/SurfaceRecon/CoarseDists/Results/CoarseDistance'
PARAPROBE_SURF_CDIST_RES_VXL_DIST_NCMAX=1

PARAPROBE_SURF_ASHAPE='/SurfaceRecon/AlphaShape'
PARAPROBE_SURF_ASHAPE_META='/SurfaceRecon/AlphaShape/Metadata'
PARAPROBE_SURF_ASHAPE_META_AVALUE='/SurfaceRecon/AlphaShape/Metadata/AlphaValue'
PARAPROBE_SURF_ASHAPE_META_AVALUE_NCMAX=1
PARAPROBE_SURF_ASHAPE_META_CAND_N='/SurfaceRecon/AlphaShape/Metadata/CandidatesN'
PARAPROBE_SURF_ASHAPE_META_CAND_N_NCMAX=1
PARAPROBE_SURF_ASHAPE_META_CAND_XYZ='/SurfaceRecon/AlphaShape/Metadata/CandidatesXYZ'
PARAPROBE_SURF_ASHAPE_META_CAND_XYZ_NCMAX=3
PARAPROBE_SURF_ASHAPE_META_CAND_TOPO='/SurfaceRecon/AlphaShape/Metadata/CandidatesTopo'
PARAPROBE_SURF_ASHAPE_META_CAND_TOPO_NCMAX=1
PARAPROBE_SURF_ASHAPE_META_VXL_WIDTH='/SurfaceRecon/AlphaShape/Metadata/VoxelizationWidth'
PARAPROBE_SURF_ASHAPE_META_VXL_WIDTH_NCMAX=1

###MK::add results from HoshenKopelman analyses

PARAPROBE_SURF_ASHAPE_RES='/SurfaceRecon/AlphaShape/Results'
PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ='/SurfaceRecon/AlphaShape/Results/HullXYZ'
PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ_NCMAX=9
PARAPROBE_SURF_ASHAPE_RES_HULL_TOPO='/SurfaceRecon/AlphaShape/Results/HullTopo'
PARAPROBE_SURF_ASHAPE_RES_HULL_TOPO_NCMAX=1

#PARAPROBE_SURFRECON_ASHAPE_INFO			'/SurfaceRecon/AlphaShape/DescrStats'

PARAPROBE_SURF_DIST='/SurfaceRecon/Ion2Surface'
PARAPROBE_SURF_DIST_META='/SurfaceRecon/Ion2Surface/Metadata'
PARAPROBE_SURF_DIST_META_NTRI='/SurfaceRecon/Ion2Surface/Metadata/TriangleTests'
PARAPROBE_SURF_DIST_META_NTRI_NCMAX=1
PARAPROBE_SURF_DIST_RES='/SurfaceRecon/Ion2Surface/Results'
PARAPROBE_SURF_DIST_RES_D='/SurfaceRecon/Ion2Surface/Results/Distance'
PARAPROBE_SURF_DIST_RES_D_NCMAX=1