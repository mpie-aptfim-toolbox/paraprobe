# -*- coding: utf-8 -*-
"""
created 2020/06/29, Markus K\"uhbach, m.kuehbach@mpie.de
definitions for HDF5 groups and datasets for results of the paraprobe-araullo tool
"""

PARAPROBE_ARAULLO='/AraulloPeters'

PARAPROBE_ARAULLO_META='/AraulloPeters/Metadata'
PARAPROBE_ARAULLO_META_HRDWR='/AraulloPeters/Metadata/ToolEnvironment'
PARAPROBE_ARAULLO_META_SFTWR='/AraulloPeters/Metadata/ToolConfiguration'

PARAPROBE_ARAULLO_META_MP='/AraulloPeters/Metadata/MatPoints'

PARAPROBE_ARAULLO_META_MP_XYZ='/AraulloPeters/Metadata/MatPoints/XYZ'
PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX=3
PARAPROBE_ARAULLO_META_MP_TOPO='/AraulloPeters/Metadata/MatPoints/Topo'
PARAPROBE_ARAULLO_META_MP_TOPO_NCMAX=1
PARAPROBE_ARAULLO_META_MP_ID='/AraulloPeters/Metadata/MatPoints/IDs'
PARAPROBE_ARAULLO_META_MP_ID_NCMAX=1
PARAPROBE_ARAULLO_META_MP_R='/AraulloPeters/Metadata/MatPoints/Radius'
PARAPROBE_ARAULLO_META_MP_R_NCMAX=1

PARAPROBE_ARAULLO_META_PHCAND='/AraulloPeters/Metadata/PhaseCandidates'
PARAPROBE_ARAULLO_META_PHCAND_N='/AraulloPeters/Metadata/PhaseCandidates/NumberOfCandidates'
PARAPROBE_ARAULLO_META_PHCAND_N_NCMAX=1
#//+per candidate
PARAPROBE_ARAULLO_META_PHCAND_ID='IontypesID'
PARAPROBE_ARAULLO_META_PHCAND_ID_NCMAX=1
PARAPROBE_ARAULLO_META_PHCAND_WHAT='IontypesWhat'
PARAPROBE_ARAULLO_META_PHCAND_WHAT_NCMAX=8
PARAPROBE_ARAULLO_META_PHCAND_RSP='RealspacePosition'
PARAPROBE_ARAULLO_META_PHCAND_RSP_NCMAX=3
PARAPROBE_ARAULLO_META_PHCAND_VAL='RefIntensity'
PARAPROBE_ARAULLO_META_PHCAND_VAL_NCMAX=1

PARAPROBE_ARAULLO_META_DIR='/AraulloPeters/Metadata/Directions'
PARAPROBE_ARAULLO_META_DIR_ELAZ='/AraulloPeters/Metadata/Directions/ElevAzim'
PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX=2
PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ='/AraulloPeters/Metadata/Directions/XYZ'
PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX=3
PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO='/AraulloPeters/Metadata/Directions/Topo'
PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO_NCMAX=1

PARAPROBE_ARAULLO_META_KEYQUANT='/AraulloPeters/Metadata/KeyQuantsForMatPoints'
PARAPROBE_ARAULLO_META_KEYQUANT_QNT='/AraulloPeters/Metadata/KeyQuantsForMatPoints/UserDefQuantiles'
PARAPROBE_ARAULLO_META_KEYQUANT_QNT_NCMAX=1
PARAPROBE_ARAULLO_META_MP_NIONS='/AraulloPeters/Metadata/NumberOfIonsForMatPoints'
PARAPROBE_ARAULLO_META_MP_NIONS_NCMAX=1

PARAPROBE_ARAULLO_RES='/AraulloPeters/Results'
PARAPROBE_ARAULLO_RES_MP_IOID='/AraulloPeters/Results/MatPointIDsWithResults'
PARAPROBE_ARAULLO_RES_MP_IOID_NCMAX=1

PARAPROBE_ARAULLO_RES_SPECPEAK='/AraulloPeters/Results/SpecificPeak'
PARAPROBE_ARAULLO_RES_SPECPEAK_NCMAX=1
PARAPROBE_ARAULLO_RES_THREEPEAK='/AraulloPeters/Results/ThreeStrongest'
PARAPROBE_ARAULLO_RES_THREEPEAK_NCMAX=6
PARAPROBE_ARAULLO_RES_KEYQUANT='/AraulloPeters/Results/KeyQuantsForMatPoints'
#//one dataset for each quantile, so '/AraulloPeters/Results/KeyQuantsForMatPoints/Quantile0, .../Quantile1, ... which quantiles these are tells PARAPROBE_ARAULLO_META_KEYQUANT_QNT
PARAPROBE_ARAULLO_RES_KEYQUANT_NCMAX=1

PARAPROBE_ARAULLO_RES_HISTO_CNTS='/AraulloPeters/Results/Histogramm'
PARAPROBE_ARAULLO_RES_HISTO_MAGN='/AraulloPeters/Results/FFTMagnitude'

