# -*- coding: utf-8 -*-
"""
created 2020/05/29, Markus K\"uhbach, m.kuehbach@mpie.de
definitions for HDF5 groups and datasets for results of the paraprobe-spatstat tool
"""

PARAPROBE_SPST='/SpatialStatistics'
PARAPROBE_SPST_META='/SpatialStatistics/Metadata'
PARAPROBE_SPST_META_HRDWR='/SpatialStatistics/Metadata/ToolEnvironment'
PARAPROBE_SPST_META_SFTWR='/SpatialStatistics/Metadata/ToolConfiguration'

#for RDF, KNN
#one result per ion group e.g two combinations of ion types are tested Al-Fe and Al-Al
#string dsnm = PARAPROBE_SPST_RDF_META + to_string(CaseID) + PARAPROBE_SPST_RDF_META_TYPID --> '/SpatialStatistics/RDF/Metadata/1/IontypeIDs'

PARAPROBE_SPST_RDF='/SpatialStatistics/RDF'
PARAPROBE_SPST_RDF_META='/SpatialStatistics/RDF/Metadata'  
#+per case 
PARAPROBE_SPST_RDF_META_TYPID_TG='IontypeTargets' #evapion3 as many rows as combinations, zero-th row is always central ion
PARAPROBE_SPST_RDF_META_TYPID_TG_NCMAX=9
PARAPROBE_SPST_RDF_META_TYPID_NB='IontypeNeighbors'
PARAPROBE_SPST_RDF_META_TYPID_NB_NCMAX=9

PARAPROBE_SPST_RDF_RES='/SpatialStatistics/RDF/Results'
#+per case
#PARAPROBE_SPST_RDF_RES_RVAL					'ROIRadii'
#PARAPROBE_SPST_RDF_RES_RVAL_NCMAX			1
PARAPROBE_SPST_RDF_RES_CNTS='Cnts'
PARAPROBE_SPST_RDF_RES_CNTS_NCMAX=2

PARAPROBE_SPST_KNN='/SpatialStatistics/KNN'
PARAPROBE_SPST_KNN_META='/SpatialStatistics/KNN/Metadata'
#+per case
PARAPROBE_SPST_KNN_META_TYPID_TG='IontypeTargets'
PARAPROBE_SPST_KNN_META_TYPID_TG_NCMAX=9
PARAPROBE_SPST_KNN_META_TYPID_NB='IontypeNeighbors'
PARAPROBE_SPST_KNN_META_TYPID_NB_NCMAX=9

PARAPROBE_SPST_KNN_META_KTH='KthOrder'
PARAPROBE_SPST_KNN_META_KTH_NCMAX=1

PARAPROBE_SPST_KNN_RES='/SpatialStatistics/KNN/Results'
#+per case
#PARAPROBE_SPST_KNN_RES_RVAL='ROIRadii'
#PARAPROBE_SPST_KNN_RES_RVAL_NCMAX=1
PARAPROBE_SPST_KNN_RES_CNTS='Cnts'
PARAPROBE_SPST_KNN_RES_CNTS_NCMAX=2

PARAPROBE_SPST_SDM='/SpatialStatistics/SDM'
PARAPROBE_SPST_SDM_META='/SpatialStatistics/SDM/Metadata'
PARAPROBE_SPST_SDM_META_TYPID_TG='IontypeTargets'
PARAPROBE_SPST_SDM_META_TYPID_TG_NCMAX=9
PARAPROBE_SPST_SDM_META_TYPID_NB='IontypeNeighbors'
PARAPROBE_SPST_SDM_META_TYPID_NB_NCMAX=9

PARAPROBE_SPST_SDM_META_R='ROIRadius'
PARAPROBE_SPST_SDM_META_R_NCMAX=1
PARAPROBE_SPST_SDM_META_NXYZ='ROIDiscretization'
PARAPROBE_SPST_SDM_META_NXYZ_NCMAX=3

PARAPROBE_SPST_SDM_META_XYZ='/SpatialStatistics/SDM/Metadata/BincenterXYZ'
PARAPROBE_SPST_SDM_META_XYZ_NCMAX=3
###MK::currently we use the same Radii settings for all SDM tasks
PARAPROBE_SPST_SDM_META_TOPO='/SpatialStatistics/SDM/Metadata/BincenterTopo'
PARAPROBE_SPST_SDM_META_TOPO_NCMAX=1

PARAPROBE_SPST_SDM_RES='/SpatialStatistics/SDM/Results'
#+per case
PARAPROBE_SPST_SDM_RES_CNTS='Cnts'
PARAPROBE_SPST_SDM_RES_CNTS_NCMAX=1

