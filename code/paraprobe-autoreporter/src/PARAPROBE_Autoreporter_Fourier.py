# -*- coding: utf-8 -*-
"""
created 2020/08/17, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for strongly automatized reporting
of results from the paraprobe-fourier tool
"""

import os, sys
import glob, subprocess
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
try:
    import periodictable as pse
except ImportError as e:
    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')
try:
    import h5py
except ImportError as e:
    raise ValueError('Install h5py Python package via e.g. pip install h5py !')

from PARAPROBE_Autoreporter_Numerics import *
from PARAPROBE_Autoreporter_CorpDesign import *
from PARAPROBE_Autoreporter_DecodeIontypeHash import *
from PARAPROBE_Autoreporter_Plots import *
from PARAPROBE_Autoreporter_MetadataFourier import *

#user-defined plots


config_fourier = {}

class autoreporter_fourier():
    
    def __init__(self, simid, *args, **kwargs):
        self.simid = simid
        self.toolname = 'Fourier'
        self.h5src_fou = 'PARAPROBE.Fourier.Results.SimID.' + str(self.simid) + '.h5'
                
        #add here all functions which parse by default results from the source
        self.config = config_fourier
        self.results = {}
        self.get_tool_config()
        #self.results['KeyQuantiles'] = {}

    def get_tool_config(self):
        fn = self.h5src_fou
        hf = h5py.File( fn, 'r' )
        self.config = config_fourier
        #to read all entries use
        #keys = list(hf['/SoftwareDetails/Metadata/Keywords/'])
        grpnm = '/SoftwareDetails/Metadata'
        if grpnm in hf.keys(): #old location of metadata, will become deprecated
            grpnm += '/Keywords'
        else: #new location of metadata
            grpnm = PARAPROBE_FOURIER_META_SFTWR
        print('Parsing configuration from group ' + grpnm)
        keys = list(hf[grpnm])
        self.config = {}
        for key in keys:
            print(key)
            tmp = hf[grpnm + '/' + key][:]
            self.config[key] = [ tmp[0].decode(), tmp[1].decode(), tmp[2].decode() ]
        self.results['Config'] = self.config

    def report(self):
        return self.results

# =============================================================================
#example usage, tell me how many ions of a certain type
#task = autoreporter_fourier( 0 )
# =============================================================================
