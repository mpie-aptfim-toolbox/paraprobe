# -*- coding: utf-8 -*-
"""
created 2020/05/28, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for strongly automatized reporting
of results, here exemplified for how users can generate their own style of figures and
use them and pass them to the next person, 100% open, using FAIR metadata, and reproducible,
so to say the genome of a figure
"""

import os, sys
import glob, subprocess
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
try:
    import periodictable as pse
except ImportError as e:
    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')
try:
    import h5py
except ImportError as e:
    raise ValueError('Install h5py Python package via e.g. pip install h5py !')

from PARAPROBE_Autoreporter_Numerics import *
from PARAPROBE_Autoreporter_CorpDesign import *

def plot_huanzhao_2018_cij( CijDict, fnarr, iarr, jarr, title, *args, **kwargs):
    """
    plot a Cij(r) plot as proposed by Huan Zhao et al. in Scripta Mat, 2018
    """
    #CijDict = Cij
    #for every figure we have C_{Al-Mg}
    #jarr = ['Zn', 'Mg', 'Cu']
    col = {}
    ###MK::hardcoded now, make more adaptive here...
    col['Mg'] = 0
    col['Zn'] = 1        
    col['Cu'] = 2
    xmi = +1.0e36
    xmx = -1.0e36
    ymi = +1.0e36
    ymx = -1.0e36
    
    #autodetect limits and use the same for all fnarr to compare results headsup and
    #avoid changes in the scale
    for fn in fnarr:
        #fn = fnarr[0]
        for j in jarr:
            #j = arr[0]
            xmin = np.min(CijDict[fn][j][:,0])
            xmax = np.max(CijDict[fn][j][:,0])
            ymin = np.min(CijDict[fn][j][:,1])
            ymax = np.max(CijDict[fn][j][:,1])
            xmi = np.min((xmin, xmi))
            xmx = np.max((xmax, xmx))
            ymi = np.min((ymin, ymi))
            ymx = np.max((ymax, ymx))
    
    #now that we know the and have the same global limits we can safely compare all datasets
    for fn in fnarr:
        figfn = fn
        #fig, ((al100, al110, al111),  (aa100, aa110, aa111), (sc100, sc110, sc111), (w100, w110, w111) ) = plt.subplots(4, 3)
        fig, ( (xy) ) = plt.subplots(1, 1)
        #plot log10 counts so replace 0 with log10(eps)
        #cnts[cnts < 1] = 0.1
        lgnd = []
        #for j in jarr:
        #    xy.plot( CijDict[dsarr][j][:,0], CijDict[dsarr][j][:,1], color=MYPARULA[0], alpha=MYALPHA, linewidth=MYWIDTH ) #np.log10( cnts )
        #    lgnd.append(j)
        for j in jarr: #add the lines
            xy.plot( CijDict[fn][j][:,0], CijDict[fn][j][:,1], color=MYPARULA[col[j]], alpha=MYALPHA, linewidth=MYWIDTH ) #np.log10( cnts )
            lgnd.append( 'i = ' + iarr + ', j = ' + j )
            #xy.plot( CijDict[fn]['Zn'][:,0], CijDict[fn]['Zn'][:,1], color=MYPARULA[1], alpha=MYALPHA, linewidth=MYWIDTH ) #np.log10( cnts )
            #lgnd.append( 'Zn' )
        #plt.set_title(fn + '.HuanZhaoCij')
        plt.legend( lgnd, loc='upper right')
        plt.xlabel('r (nm)')
        #plt.ylabel(r'$log_{10}$ counts')
        plt.ylabel(r'$C_{i-j}$ (1)')
        #xy.vlines([xmx], 0, 1, transform=xy.get_xaxis_transform(), colors=MYPARULA[2], linestyles='dotted') #'solid') #'dotted')
        #plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
        #fig.yscale('log')
        #scale bar with add margin to the bottom and top of the yaxis to avoid that lines fall on x axis
        margin=0.05
        plt.xlim([-margin*(xmx-xmi)+xmi, +margin*(xmx-xmi)+xmx])
        xlen = (+margin*(xmx-xmi)+xmx) - (-margin*(xmx-xmi)+xmi)
        plt.ylim([ymi, +margin*(ymx-ymi)+ymx])
        ylen = (+margin*(ymx-ymi)+ymx) - ymi
    
        fig = plt.gcf()
        fig.set_size_inches( MYFIG_WIDTH, MYFIG_HEIGHT )
        #plt.scatter(qq[:,0],qq[:,1],s=30,color='#9DC943',alpha=1.0,linewidths=0.0)
        figfn = fn + 'HuanZhaoCij' + '.pdf' #'.png'
        figfn = fn + 'HuanZhaoCij' + '.png'
        #https://stackoverflow.com/questions/32185411/break-in-x-axis-of-matplotlib
        #fig.savefig( figfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
        #                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
        fig.savefig( figfn, dpi=MYFIG_DPI, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
                            transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None) #frameon=None, 
        fig.clf()
        return figfn
