# -*- coding: utf-8 -*-
"""
created 2020/05/28, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for strongly automatized reporting
of results from the paraprobe-ranger tool
"""

import os, sys
import glob, subprocess
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
try:
    import periodictable as pse
except ImportError as e:
    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')
try:
    import h5py
except ImportError as e:
    raise ValueError('Install h5py Python package via e.g. pip install h5py !')

from PARAPROBE_Autoreporter_Numerics import *
from PARAPROBE_Autoreporter_CorpDesign import *

def plot_naive_mass2charge_histogram( fn, mq, cnts, title, *args, **kwargs):
    """
    plot a naive log10 cnts mass-to-charge histogram
    """
    ##MK::make adaptive
    xmi = 0.0 #np.min(x)
    xmx = 140.0 #np.max(x[x < 1.0e19]) #see comment on maximum value
    #ymi = np.log10(0.5) #np.min(y)
    #ymx = np.log10(1.0e7) #np.max(y)
    ymi = 0.5
    ymx = 1.0e8
    
    #fig, ((al100, al110, al111),  (aa100, aa110, aa111), (sc100, sc110, sc111), (w100, w110, w111) ) = plt.subplots(4, 3)
    fig, ( (xy) ) = plt.subplots(1, 1)
    #plot log10 counts so replace 0 with log10(eps)
    #cnts[cnts < 1] = 0.1
    xy.plot( mq, cnts, color=MYPARULA[0], alpha=MYALPHA, linewidth=MYWIDTH ) #np.log10( cnts )
    #xy.set_title(title)
    lgnd = 'Mass-to-charge histogram' #dataset edge
    plt.legend( [lgnd], loc='upper right')
    plt.xlabel('Mass-to-charge (Da)')
    #plt.ylabel(r'$log_{10}$ counts')
    plt.ylabel('Counts')
    #xy.vlines([xmx], 0, 1, transform=xy.get_xaxis_transform(), colors=MYPARULA[2], linestyles='dotted') #'solid') #'dotted')
    #plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
    plt.yscale('log')
    #scale bar with add margin to the bottom and top of the yaxis to avoid that lines fall on x axis
    margin=0.01
    plt.xlim([-margin*(xmx-xmi)+xmi, +margin*(xmx-xmi)+xmx])
    xlen = (+margin*(xmx-xmi)+xmx) - (-margin*(xmx-xmi)+xmi)
    plt.ylim([ymi, +margin*(ymx-ymi)+ymx])
    ylen = (+margin*(ymx-ymi)+ymx) - ymi

    fig = plt.gcf()
    fig.set_size_inches( 2*MYFIG_WIDTH, MYFIG_HEIGHT )
    #plt.scatter(qq[:,0],qq[:,1],s=30,color='#9DC943',alpha=1.0,linewidths=0.0)
    figfn = fn + '.pdf' #'.png'
    figfn = fn + '.png'
    #https://stackoverflow.com/questions/32185411/break-in-x-axis-of-matplotlib
    #fig.savefig( figfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
    #                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
    fig.savefig( figfn, dpi=MYFIG_DPI, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
                        transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None) #frameon=None, 
    plt.close('all')
    return figfn


def plot_naive_composition( fn, cnts, title, *args, **kwargs):
    """
    plot naive composition as a pie chart
    """
    # Pie chart, where the slices will be ordered and plotted counter-clockwise:
    #get resolved element-containing names from the pandas table
    #labels = np.array(res['2_Ranger'].cnts.columns.values[:])
    #sizes = np.float64(res['2_Ranger'].cnts.values)
    #cnts = res['2_Ranger'].cnts

    labels = cnts.columns.values
    sizes = np.float64(cnts.values)
    #print(sizes)
    #print(np.shape(sizes))
    #currently not exploding anything
    explode = np.zeros([len(labels)], np.float32) #set a value > 0.0 to explode by val
    #print(explode)
    #explode for the largest contribution
    mx = -1.0e36
    mxid = -1
    if len(cnts.values) > 1:
        for i in np.arange(0,np.shape(sizes)[1]):
            #print('->' + str(sizes[0,i]))
            if sizes[0,i] >= mx:
                mx = sizes[0,i]
                mxid = i
                #print('mx ' + str(mx))
        #mx = sizes
        #mxid = 0                
        #print(mxid)
        #explode[np.where(sizes == np.max(sizes))] = 0.1
        explode[mxid] = 0.2
    
        fig1, ax = plt.subplots(1, 1)
        #ax.pie(sizes, explode=explode, labels=labels, autopct='%.1f', shadow=False, startangle=90,
        #       pctdistance=0.9, textprops={'size': 'xx-small'} ) # colors = parula_map ) #shadow=True, smaller
        #ax.axis('equal')  #equal aspect ratio to ensure pie is drawn as a circle
        #ax.set_title(title)
        #print(len(sizes[0,:]))
        #print(len(explode))
        #print(len(labels))
        ax.pie(sizes[0,:], explode=explode, labels=labels, autopct=None, shadow=False, startangle=90,
               pctdistance=0.9, textprops=None) #dict(size="xx-small") ) #textprops={'size': 'smaller'} ) #'xx-small'} ) # colors = parula_map ) #shadow=True, smaller
        ax.axis('equal')  #equal aspect ratio to ensure pie is drawn as a circle '%.1f'
        #ax.set_title(title)
        fig = plt.gcf()
        fig.set_size_inches( MYFIG_WIDTH, MYFIG_HEIGHT )
        figfn = fn + '.pdf' #'.png'
        figfn = fn + '.png'
        #fig.savefig( figfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
        #                    transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None)
        fig.savefig( figfn, dpi=MYFIG_DPI, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
                            transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None)
        plt.close('all')
        return figfn
    else:
        return None


def plot_cdf_ion2edge_distance( fn, dist, distmode, title, *args, **kwargs):
    """
    plot cumulative distribution ion-to-edge distances 
    from dist numpy array of shape(N,1) and dtype np.float32
    """
    #paraprobe marks distances above a threshold with the maximum real value which a np.float32 can take
    #compute CDF
    #dist = tmp
    x = np.sort(dist)
    y = np.linspace( 1.0/len(x), 1.0, len(x), endpoint=True)
    xmi = np.min(x)
    xmx = np.max(x[x < 1.0e19]) #see comment on maximum value
    ymi = np.min(y)
    ymx = np.max(y)
    
    #fig, ((al100, al110, al111),  (aa100, aa110, aa111), (sc100, sc110, sc111), (w100, w110, w111) ) = plt.subplots(4, 3)
    fig, ( (xy) ) = plt.subplots(1, 1)
    xy.plot(x, y, color=MYPARULA[0], alpha=MYALPHA, linewidth=MYWIDTH )
    xy.set_title(title)
    if distmode == np.uint32(2):
        lgnd = 'Skin distancing' + ' (remaining ions ' + r'$\geq$' + str(np.around(xmx, decimals=1)) + 'nm to edge)' #dataset edge
    else:
        lgnd = 'Complete distancing'
    plt.legend( [lgnd], loc='upper left')
    plt.xlabel('Ion-to-edge distance (nm)')
    plt.ylabel(r'Cumulative distribution')
    xy.vlines([xmx], 0, 1, transform=xy.get_xaxis_transform(), colors=MYPARULA[2], linestyles='dotted') #'solid') #'dotted')

    #plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
    #plt.yscale('log')
    #scale bar with add margin to the bottom and top of the yaxis to avoid that lines fall on x axis
    margin=0.05
    plt.xlim([-margin*(xmx-xmi)+xmi, +margin*(xmx-xmi)+xmx])
    xlen = (+margin*(xmx-xmi)+xmx) - (-margin*(xmx-xmi)+xmi)
    plt.ylim([-margin*(ymx-ymi)+ymi, +margin*(ymx-ymi)+ymx])
    ylen = (+margin*(ymx-ymi)+ymx) - (-margin*(ymx-ymi)+ymi)
    #inform what is potentially with the rest of the ions
    #y_at_xmx = y[x == xmx][-1]
    #if y_at_xmx <= (1.0 - EPSILON): #cumulative distribution
    #    plt.text( (xmx - (-margin*(xmx-xmi)+xmi))/xlen, (y_at_xmx - (-margin*(ymx-ymi)+ymi)) / ylen, 'Rest of the ions ' + r'$\geq$' + str(xmx) + 'nm inside', ha='right', va='bottom', transform=ax.transAxes)
    #ymi = 0.0
    #ymx = 1.0
    #plt.ylim([-margin, +1.05])
    #plt.yticks([0.0:0.1:1.0]) #, ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
    fig = plt.gcf()
    fig.set_size_inches( MYFIG_WIDTH, MYFIG_HEIGHT )
    #plt.scatter(qq[:,0],qq[:,1],s=30,color='#9DC943',alpha=1.0,linewidths=0.0)
    figfn = fn + '.pdf' #'.png'
    figfn = fn + '.png'
    #https://stackoverflow.com/questions/32185411/break-in-x-axis-of-matplotlib
    #fig.savefig( figfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
    #                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
    fig.savefig( figfn, dpi=MYFIG_DPI, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
                        transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None) #frameon=None, 
    plt.close('all')
    return figfn


def plot_rdf( fn, hist, normalizer, title, lgnd, *args, **kwargs ):
    """
    plot radial distribution function from hist if significant data in there numpy array of shape(N,2) and dtype np.float64
    normalize the counts based on normalizer
    """
    #hist = hst1d_res
    nrows = np.shape(hist[:,1])[0]
    #is statistic significant, then go from empirical histogram hist to ECDF
    if np.any(hist[0:nrows-1,1] > EPSILON):
        #following the definitions on page 281ff of B. Gault, M. P. Moody, J. M. Cairney and S. P. Ringer
        #Atom Probe Microscopy, dx.doi.org/10.1007/978-1-4614-3436-8
        #RDF(r) = \frac{1}{\bar{\rho}} \frac{n_{RDF}(r)}{\frac{4}{3}\pi(r+0.5\Delta r)^3-(r-0.5\Delta r)^3}$
        #mind that n_{RDF}(r) in between we find that accumulated counts in hist estimate RipleyK but not the RDF !!
        rdfdist = np.zeros([nrows-1,4],np.float64)
        norm = normalizer ##CHECK THIS ONE
        for i in np.arange(1,nrows-1,1):
            ##MK::possibly an r-offset here by half the bin-width!, likely can be made faster using numpy
            rdfdist[i,0] = 0.5*(hist[i,0]+hist[i-1,0]) #r
            rdfdist[i,1] = hist[i,1]-hist[i-1,1] #diff
            rdfdist[i,2] = rdfdist[i,0]**3 - rdfdist[i-1,0]**3 #sphvol
            if rdfdist[i,2] > EPSILON:
                rdfdist[i,3] = rdfdist[i,1] / rdfdist[i,2] * norm; #norm*diff/sphvol
                
        x = rdfdist[:,0]
        y = rdfdist[:,3]
        xmi = np.min(x)
        xmx = np.max(x[x < 1.0e19]) #see comment on maximum value
        ymi = np.min(y)
        ymx = np.max(y)
        
        #fig, ((al100, al110, al111),  (aa100, aa110, aa111), (sc100, sc110, sc111), (w100, w110, w111) ) = plt.subplots(4, 3)
        fig, ( (xy) ) = plt.subplots(1, 1)
        xy.plot(x, y, color=MYPARULA[0], alpha=MYALPHA, linewidth=MYWIDTH )
        xy.set_title(title)
        plt.legend( [lgnd], loc='upper right')
        plt.xlabel('Distance (nm)')
        plt.ylabel(r'Radial distribution function')
        #xy.vlines([xmx], 0, 1, transform=xy.get_xaxis_transform(), colors=MYPARULA[2], linestyles='dotted') #'solid') #'dotted')
        #plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
        #plt.yscale('log')
        #scale bar with add margin to the bottom and top of the yaxis to avoid that lines fall on x axis
        #margin=0.05
        #plt.xlim([-margin*(xmx-xmi)+xmi, +margin*(xmx-xmi)+xmx])
        #xlen = (+margin*(xmx-xmi)+xmx) - (-margin*(xmx-xmi)+xmi)
        #plt.ylim([-margin*(ymx-ymi)+ymi, +margin*(ymx-ymi)+ymx])
        #ylen = (+margin*(ymx-ymi)+ymx) - (-margin*(ymx-ymi)+ymi)
        #inform what is potentially with the rest of the ions
        #y_at_xmx = y[x == xmx][-1]
        #if y_at_xmx <= (1.0 - EPSILON): #cumulative distribution
        #    plt.text( (xmx - (-margin*(xmx-xmi)+xmi))/xlen, (y_at_xmx - (-margin*(ymx-ymi)+ymi)) / ylen, 'Rest of the ions ' + r'$\geq$' + str(xmx) + 'nm inside', ha='right', va='bottom', transform=ax.transAxes)
        #ymi = 0.0
        #ymx = 1.0
        #plt.ylim([-margin, +1.05])
        #plt.yticks([0.0:0.1:1.0]) #, ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
        fig = plt.gcf()
        fig.set_size_inches( MYFIG_WIDTH, MYFIG_HEIGHT )
        #plt.scatter(qq[:,0],qq[:,1],s=30,color='#9DC943',alpha=1.0,linewidths=0.0)
        figfn = fn + '.pdf' #'.png'
        figfn = fn + '.png'
        #https://stackoverflow.com/questions/32185411/break-in-x-axis-of-matplotlib
        #fig.savefig( figfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
        #                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
        fig.savefig( figfn, dpi=MYFIG_DPI, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
                    transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None) #frameon=None,
        plt.close('all')
        return figfn

def plot_knn( fn, hist, title, lgnd, *args, **kwargs ):
    """
    plot single distribution function histogram + cumulative from hist if significant array of shape(N,2) and dtype np.float64
    """
    #hist = hst1d
    nrows = np.shape(hist[:,1])[0]
    #is statistic significant, then go from empirical histogram hist to ECDF
    if np.any(hist[0:nrows-1,1] > EPSILON):
        #following the definitions on page 281ff of B. Gault, M. P. Moody, J. M. Cairney and S. P. Ringer
        #Atom Probe Microscopy, dx.doi.org/10.1007/978-1-4614-3436-8
        #RDF(r) = \frac{1}{\bar{\rho}} \frac{n_{RDF}(r)}{\frac{4}{3}\pi(r+0.5\Delta r)^3-(r-0.5\Delta r)^3}$
        #mind that n_{RDF}(r) in between we find that accumulated counts in hist estimate RipleyK but not the RDF !!
        
        #histogram, not probability density!
        epdf = np.zeros([nrows-1,2],np.float64)
        epdf[:,0] = hist[0:nrows-1,0]
        epdf[:,1] = hist[0:nrows-1,1]
        xmi = np.min(epdf[:,0])
        xmx = np.max(epdf[:,0][epdf[:,0] < 1.0e19]) #last value of paraprobe-spatstat histograms place np.float64 maximum to encode
        #if no neighbors were found for processed ions, such that we do not loose ions here that have to be accounted for
        
        #add all sorts of fitting for histograms you want here ...

        #empirical cumulative        
        ecdf = np.zeros([nrows,2],np.float64) #not -1 ! we also account for target ions with no neighbors within tested ROI 
        ecdf[:,0] = hist[0:nrows,0]
        ecdf[-1,0] = ecdf[-2,0] #replace last R value, which was a dummy with maximum value to avoid plot distortion
        ecdf[:,1] = np.cumsum(hist[0:nrows,1])
        ecdf[:,1] = ecdf[:,1] / ecdf[-1,1]
        
        ymi0 = np.min(epdf[:,1])
        ymx0 = np.max(epdf[:,1])
        ymi1 = np.min(ecdf[:,1])
        ymx1 = np.max(ecdf[:,1])
        
        fig, ax_epdf = plt.subplots(1, 1)
        ax_epdf.set_xlabel('Distance (nm)')
        col = MYPARULA[1]
        #ax_epdf.set_ylabel(r'log$_{10}$' + ' histogram counts', color = col) #(1) unit
        ax_epdf.set_ylabel(r'Histogram counts', color = col) #(1) unit
        ax_epdf.set_yscale('log')
        ax_epdf.plot( epdf[:,0], epdf[:,1], color = col)
        ax_epdf.tick_params(axis='y', labelcolor = col)
        
        ax_ecdf = ax_epdf.twinx()  # instantiate a second axes that shares the same x-axis
        col = MYPARULA[0]
        ax_ecdf.set_ylabel('Cumulative distribution', color=col)  # we already handled the x-label with ax1
        ax_ecdf.plot( ecdf[:,0], ecdf[:,1], color=col)
        ax_ecdf.tick_params(axis='y', labelcolor=col)
        fig.tight_layout()  # otherwise the right y-label is slightly clipped
 #       ax_epdf.set_title(title)
        ax_ecdf.legend( [lgnd], loc='upper right')
        #xy.vlines([xmx], 0, 1, transform=xy.get_xaxis_transform(), colors=MYPARULA[2], linestyles='dotted') #'solid') #'dotted')
        #plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
        #plt.yscale('log')
        #scale bar with add margin to the bottom and top of the yaxis to avoid that lines fall on x axis
        margin=0.05
        #plt.xlim([-margin*(xmx-xmi)+xmi, +margin*(xmx-xmi)+xmx])
        #xlen = (+margin*(xmx-xmi)+xmx) - (-margin*(xmx-xmi)+xmi)
        plt.ylim([-margin*(ymx1-ymi1)+ymi1, +3*margin*(ymx1-ymi1)+ymx1])
        #ylen = (+margin*(ymx1-ymi1)+ymx1) - (-margin*(ymx1-ymi1)+ymi1)
        #inform what is potentially with the rest of the ions
        #y_at_xmx = y[x == xmx][-1]
        #if y_at_xmx <= (1.0 - EPSILON): #cumulative distribution
        #    plt.text( (xmx - (-margin*(xmx-xmi)+xmi))/xlen, (y_at_xmx - (-margin*(ymx-ymi)+ymi)) / ylen, 'Rest of the ions ' + r'$\geq$' + str(xmx) + 'nm inside', ha='right', va='bottom', transform=ax.transAxes)
        #ymi = 0.0
        #ymx = 1.0
        #plt.ylim([-margin, +1.05])
        #plt.yticks([0.0:0.1:1.0]) #, ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
        fig = plt.gcf()
        fig.set_size_inches( MYFIG_WIDTH, MYFIG_HEIGHT )
        #plt.scatter(qq[:,0],qq[:,1],s=30,color='#9DC943',alpha=1.0,linewidths=0.0)
        figfn = fn + '.pdf' #'.png'
        figfn = fn + '.png'
        #https://stackoverflow.com/questions/32185411/break-in-x-axis-of-matplotlib
        #fig.savefig( figfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
        #                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
        fig.savefig( figfn, dpi=MYFIG_DPI, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
                    transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None) 
        plt.close('all')
        return figfn

def plot_voronoi_size_distro( fn, vol, dero, title, lgnd, *args, **kwargs ):
    """
    plot single cumulative distribution of Voronoi cell volume from vol shape(N,1) and dtype np.float64
    """
    #vol = np.array([120.0, 34.5, 45.6, 76.0, 0.0])
    #dero = 2.0
    nrows = np.shape(vol)[0]
    #is statistic significant, then go from empirical histogram hist to ECDF
    if np.any(vol > EPSILON):
        #empirical cumulative distribution
        ecdf = np.zeros([nrows,2], np.float64)
        ecdf[:,0] = np.sort(vol) #ascendingly
        #we report all N
        #count based, i.e. distributions answer how many cells/ions of all have a total volume of
        ecdf[:,1] = np.linspace(1, nrows, nrows, endpoint=True) / nrows
        xmi = np.min(ecdf[:,0])
        xmx = np.max(ecdf[:,0])   
        ymi = np.min(ecdf[:,1])
        ymx = np.max(ecdf[:,1])
        
        fig, ax_ecdf = plt.subplots(1, 1)
        ax_ecdf.set_xlabel(r'Voronoi cell volume (${nm}^3$)')
        col = MYPARULA[0]
        ax_ecdf.set_ylabel('Cumulative distribution') #(1) unit
        ax_ecdf.plot( ecdf[:,0], ecdf[:,1], color = col)
        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        ax_ecdf.set_title(title)
        ax_ecdf.legend( lgnd, loc='lower right')
        ax_ecdf.vlines([dero], 0, 1, transform=ax_ecdf.get_xaxis_transform(), colors=MYPARULA[2], linestyles='dotted') #'solid') #'dotted')
        #scale bar with add margin to the bottom and top of the yaxis to avoid that lines fall on x axis
        margin=0.05
        plt.xlim([-margin*(xmx-xmi)+xmi, +margin*(xmx-xmi)+xmx])
        plt.ylim([-margin*(ymx-ymi)+ymi, +margin*(ymx-ymi)+ymx])
        fig = plt.gcf()
        fig.set_size_inches( MYFIG_WIDTH, MYFIG_HEIGHT )
        figfn = fn + '.pdf' #'.png'
        figfn = fn + '.png'
        #https://stackoverflow.com/questions/32185411/break-in-x-axis-of-matplotlib
        #fig.savefig( figfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
        #                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
        fig.savefig( figfn, dpi=MYFIG_DPI, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
                    transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None) 
        plt.close('all')
        return figfn

def plot_sum_voronoi_volume_vs_distance( fn, x, y, dero, title, lgnd, *args, **kwargs ):
    """
    plot curve which tells how much accumulated volume the Voronoi cells of the dataset have for a given distance of
    the ions to the edge of the dataset, enables to judge the dataset volume to estimate e.g. number density of objects
    """
    #is statistic significant
    if np.any(x > EPSILON) and np.any(y > EPSILON):
        #empirical cumulative distribution
        xmi = np.min(x[:])
        xmx = np.max(x[:])   
        ymi = np.min(y[:])
        ymx = np.max(y[:])
        
        fig, ax_xy = plt.subplots(1, 1)
        ax_xy.set_xlabel(r'Distance to the dataset edge (nm)')
        col = MYPARULA[0]
        ax_xy.set_ylabel(r'$\sum$ Voronoi cells (${nm}^3$)') #(1) unit
        ax_xy.plot( x, y, color = col)
        ax_xy.vlines([dero], 0, 1, transform=ax_xy.get_xaxis_transform(), colors=MYPARULA[2], linestyles='dotted') #'solid') #'dotted')
        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        ax_xy.set_title(title)
        ax_xy.legend( lgnd, loc='upper right')
        #ax_ecdf.vlines([dero], 0, 1, transform=ax_ecdf.get_xaxis_transform(), colors=MYPARULA[2], linestyles='dotted') #'solid') #'dotted')
        #scale bar with add margin to the bottom and top of the yaxis to avoid that lines fall on x axis
        margin=0.05
        plt.xlim([-margin*(xmx-xmi)+xmi, +margin*(xmx-xmi)+xmx])
        plt.ylim([-margin*(ymx-ymi)+ymi, +margin*(ymx-ymi)+ymx])
        fig = plt.gcf()
        fig.set_size_inches( MYFIG_WIDTH, MYFIG_HEIGHT )
        figfn = fn + '.pdf' #'.png'
        figfn = fn + '.png'
        #https://stackoverflow.com/questions/32185411/break-in-x-axis-of-matplotlib
        #fig.savefig( figfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
        #                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
        fig.savefig( figfn, dpi=MYFIG_DPI, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
                    transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None) 
        plt.close('all')
        return figfn

def plot_cluster_size_distro( fn, cdfall, cdfisd, title, lgnd, *args, **kwargs ):
    """
    plot cluster size distribution for all clusters (cdfall) vs excluding clusters close to the boundary of the dataset (cdfisd)
    """
    if np.any(cdfall > EPSILON) and np.any(cdfisd > EPSILON):
        #title = 'Test'
        #lgnd = ['All clusters', 'Only interior cluster']
        xmi = np.min( np.concatenate((cdfall[:,0], cdfisd[:,0]), axis=0) )
        xmx = np.max( np.concatenate((cdfall[:,0], cdfisd[:,0]), axis=0) ) 
        ymi = np.min( np.concatenate((cdfall[:,1], cdfisd[:,1]), axis=0) )
        ymx = np.max( np.concatenate((cdfall[:,1], cdfisd[:,1]), axis=0) )
       
        fig, ax_ecdf = plt.subplots(1, 1)
        ax_ecdf.set_xlabel('Number of ions in the cluster')
        ax_ecdf.set_xscale('log')
        col_unbiased = MYPARULA[0]
        col_biased = MYPARULA[2]
        ax_ecdf.set_ylabel(r'Cumulative distribution' ) #(1) unit
        ax_ecdf.plot( cdfall[:,0], cdfall[:,1], color = col_biased )
        ax_ecdf.plot( cdfisd[:,0], cdfisd[:,1], color = col_unbiased )
        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        ax_ecdf.set_title(title)
        ax_ecdf.legend( lgnd, loc='lower right')
        #scale bar with add margin to the bottom and top of the yaxis to avoid that lines fall on x axis
        margin=0.05
        plt.ylim([-margin*(ymx-ymi)+ymi, +margin*(ymx-ymi)+ymx])
        fig = plt.gcf()
        fig.set_size_inches( MYFIG_WIDTH, MYFIG_HEIGHT )
        figfn = fn + '.pdf' #'.png'
        figfn = fn + '.png'
        #https://stackoverflow.com/questions/32185411/break-in-x-axis-of-matplotlib
        #fig.savefig( figfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
        #                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
        fig.savefig( figfn, dpi=MYFIG_DPI, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
                    transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None) 
        plt.close('all')
        return figfn
        
def plot_cluster_numberdensity_vs_eps( fn, x, yall, yisd, title, lgnd, *args, **kwargs ):
    """
    plot cluster number density distribution for all clusters (yall) and clusters far enough from the dataset boundary (yisd)
    as a function of epsilon/dmax (x)
    """
    if np.any(x > EPSILON) and np.any(yall > EPSILON) and np.any(yisd > EPSILON):
        #title = 'Test'
        #lgnd = ['All clusters', 'Only interior cluster']
        xmi = np.min( x )
        xmx = np.max( x ) 
        ymi = np.min( np.concatenate((yall, yisd), axis=0) )
        ymx = np.max( np.concatenate((yall, yisd), axis=0) )
       
        fig, ax_xy = plt.subplots(1, 1)
        ax_xy.set_xlabel(r'$\epsilon$ (nm)')
        #ax_xy.set_xscale('log')
        col_unbiased = MYPARULA[0]
        col_biased = MYPARULA[2]
        ax_xy.set_ylabel(r'Cluster number density ($m^{-3}$)' ) #(1) unit
        ax_xy.scatter( x, yall, s = 50, c = col_biased )
        ax_xy.scatter( x, yisd, s = 50, c = col_unbiased )
        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        ax_xy.set_title(title)
        ax_xy.legend( lgnd, loc='upper right')
        #scale bar with add margin to the bottom and top of the yaxis to avoid that lines fall on x axis
        margin=0.05
        plt.ylim([-margin*(ymx-ymi)+ymi, +margin*(ymx-ymi)+ymx])
        fig = plt.gcf()
        fig.set_size_inches( MYFIG_WIDTH, MYFIG_HEIGHT )
        figfn = fn + '.pdf' #'.png'
        figfn = fn + '.png'
        #https://stackoverflow.com/questions/32185411/break-in-x-axis-of-matplotlib
        #fig.savefig( figfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
        #                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
        fig.savefig( figfn, dpi=MYFIG_DPI, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
                    transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None) 
        plt.close('all')
        return figfn
    
def plot_araullo_histogram( fn, x, y, title, lgnd, *args, **kwargs ):
    """
    plot as a scatter plot histogram counts for given phase, given ROI, given SDM as a function of binID
    """
    if np.any(x > EPSILON) or np.any(y > EPSILON):
        #title = 'Test'
        #lgnd = ['All clusters', 'Only interior cluster']
        xmi = np.min( x[:] )
        xmx = np.max( x[:] )
        ymi = np.min( y[:] )
        ymx = np.max( y[:] )

        fig, ax_xy = plt.subplots(1, 1)
        ax_xy.set_xlabel(r'Projected distance (nm)')
        #ax_xy.set_xscale('log')
        col = MYPARULA[0]
        ax_xy.set_ylabel(r'Counts' ) #(1) unit
        ax_xy.plot( x[:], y[:], color = col ) #scatter( x[:], y[:], s = 50, c = col, alpha=0.1 )
        #plt.text( 10*np.ceil(np.log10(0.8)), 0.1, r'ROIs with intensities closer to 1.0 are likely better indexable' + '\n' 'combed', ha='right', va='bottom', transform=ax_xy.transAxes)
        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        #ax_xy.set_title(title)
        ax_xy.legend( lgnd, loc='upper right')
        #ax_xy.arrow( 0, 0, 0.5, 0.5, head_width=0.05, head_length=0.1, fc='k', ec='k')
        #ax_xy.arrow( xmi, 0.1, xmi, 0.8, head_width=0.05, head_length=0.1, facecolor='black', edgecolor='black', alpha=0.5 )
        #scale bar with add margin to the bottom and top of the yaxis to avoid that lines fall on x axis
        margin=0.05
        plt.xlim([xmi, xmx])
        plt.ylim([-margin*(ymx-ymi)+ymi, +margin*(ymx-ymi)+ymx])
        fig = plt.gcf()
        fig.set_size_inches( MYFIG_WIDTH, MYFIG_HEIGHT )
        figfn = fn + '.pdf' #'.png'
        figfn = fn + '.png'
        #https://stackoverflow.com/questions/32185411/break-in-x-axis-of-matplotlib
        #fig.savefig( figfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
        #                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
        fig.savefig( figfn, dpi=MYFIG_DPI, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
                    transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None) 
        plt.close('all')
        return figfn

def plot_araullo_fftspectrum( fn, x, y, title, lgnd, *args, **kwargs ):
    """
    plot as a scatter plot fft spectrum amplitude for given phase, given ROI, given SDM as a function of binID
    """
    if np.any(x > EPSILON) or np.any(y > EPSILON):
        #title = 'Test'
        #lgnd = ['All clusters', 'Only interior cluster']
        xmi = np.min( x[:] )
        xmx = np.max( x[:] )
        ymi = np.min( y[:] )
        ymx = np.max( y[:] )

        fig, ax_xy = plt.subplots(1, 1)
        ax_xy.set_xlabel(r'Frequency bin ID')
        #ax_xy.set_xscale('log')
        col = MYPARULA[0]
        ax_xy.set_ylabel(r'Amplitude' ) #(1) unit
        ax_xy.plot( x[:], y[:], color = col ) #scatter( x[:], y[:], s = 50, c = col, alpha=0.1 )
        #plt.text( 10*np.ceil(np.log10(0.8)), 0.1, r'ROIs with intensities closer to 1.0 are likely better indexable' + '\n' 'combed', ha='right', va='bottom', transform=ax_xy.transAxes)
        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        #ax_xy.set_title(title)
        ax_xy.legend( lgnd, loc='upper right')
        #ax_xy.arrow( 0, 0, 0.5, 0.5, head_width=0.05, head_length=0.1, fc='k', ec='k')
        #ax_xy.arrow( xmi, 0.1, xmi, 0.8, head_width=0.05, head_length=0.1, facecolor='black', edgecolor='black', alpha=0.5 )
        #scale bar with add margin to the bottom and top of the yaxis to avoid that lines fall on x axis
        margin=0.05
        plt.xlim([xmi, xmx])
        plt.ylim([-margin*(ymx-ymi)+ymi, +margin*(ymx-ymi)+ymx])
        fig = plt.gcf()
        fig.set_size_inches( MYFIG_WIDTH, MYFIG_HEIGHT )
        figfn = fn + '.pdf' #'.png'
        figfn = fn + '.png'
        #https://stackoverflow.com/questions/32185411/break-in-x-axis-of-matplotlib
        #fig.savefig( figfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
        #                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
        fig.savefig( figfn, dpi=MYFIG_DPI, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
                    transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None) 
        plt.close('all')
        return figfn

def plot_araullo_specificpeaks( fn, x, y, title, lgnd, *args, **kwargs ):
    """
    plot as a scatter plot flat signature with values ordered according to elevation/azimuth list for given phase, given ROI, given SDM as a function of binID
    """
    if np.any(x > EPSILON) or np.any(y > EPSILON):
        #title = 'Test'
        #lgnd = ['All clusters', 'Only interior cluster']
        xmi = np.min( x[:] )
        xmx = np.max( x[:] )
        ymi = np.min( y[:] )
        ymx = np.max( y[:] )

        fig, ax_xy = plt.subplots(1, 1)
        ax_xy.set_xlabel(r'Projected direction ID')
        #ax_xy.set_xscale('log')
        col = MYPARULA[0]
        ax_xy.set_ylabel(r'Normalized intensity' ) #(1) unit
        ax_xy.plot( x[:], y[:], color = col ) #scatter( x[:], y[:], s = 50, c = col, alpha=0.1 )
        #plt.text( 10*np.ceil(np.log10(0.8)), 0.1, r'ROIs with intensities closer to 1.0 are likely better indexable' + '\n' 'combed', ha='right', va='bottom', transform=ax_xy.transAxes)
        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        #ax_xy.set_title(title)
        ax_xy.legend( lgnd, loc='upper right')
        #ax_xy.arrow( 0, 0, 0.5, 0.5, head_width=0.05, head_length=0.1, fc='k', ec='k')
        #ax_xy.arrow( xmi, 0.1, xmi, 0.8, head_width=0.05, head_length=0.1, facecolor='black', edgecolor='black', alpha=0.5 )
        #scale bar with add margin to the bottom and top of the yaxis to avoid that lines fall on x axis
        margin=0.05
        plt.xlim([xmi, xmx])
        plt.ylim([-margin*(ymx-ymi)+ymi, +margin*(ymx-ymi)+ymx])
        fig = plt.gcf()
        fig.set_size_inches( 2*MYFIG_WIDTH, MYFIG_HEIGHT )
        figfn = fn + '.pdf' #'.png'
        figfn = fn + '.png'
        #https://stackoverflow.com/questions/32185411/break-in-x-axis-of-matplotlib
        #fig.savefig( figfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
        #                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
        fig.savefig( figfn, dpi=MYFIG_DPI, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
                    transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None) 
        plt.close('all')
        return figfn

def plot_araullo_keyquantiles_overview( fn, xy, title, lgnd, *args, **kwargs ):
    """
    plot as a scatter plot for analyzed ROIs in paraprobe-araullo the key quantile image intensity as a function of the number of ions in the ROI
    """
    if np.any(xy > EPSILON):
        #title = 'Test'
        #lgnd = ['All clusters', 'Only interior cluster']
        xmi = 0.8 #np.min( xy[:,0] )
        #next order of magnitude base 10
        xmx = 10.0**np.ceil( np.log10( np.max( xy[:,0] ) ) )
        ymi = 0.0 #np.min( xy[:,1] )
        ymx = 1.0 #np.max( xy[:,1] )
       
        fig, ax_xy = plt.subplots(1, 1)
        ax_xy.set_xlabel(r'Number of ions in ROI')
        ax_xy.set_xscale('log')
        col = MYPARULA[0]
        ax_xy.set_ylabel(r'Key quantile image intensity per ROI' ) #(1) unit
        ax_xy.scatter( xy[:,0], xy[:,1], s = 50, c = col, alpha=0.1 )
        #plt.text( 10*np.ceil(np.log10(0.8)), 0.1, r'ROIs with intensities closer to 1.0 are likely better indexable' + '\n' 'combed', ha='right', va='bottom', transform=ax_xy.transAxes)
        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        #ax_xy.set_title(title)
        ax_xy.legend( lgnd, loc='lower left')
        #ax_xy.arrow( 0, 0, 0.5, 0.5, head_width=0.05, head_length=0.1, fc='k', ec='k')
        #ax_xy.arrow( xmi, 0.1, xmi, 0.8, head_width=0.05, head_length=0.1, facecolor='black', edgecolor='black', alpha=0.5 )
        #scale bar with add margin to the bottom and top of the yaxis to avoid that lines fall on x axis
        margin=0.05
        plt.xlim([xmi, xmx])
        plt.ylim([-margin*(ymx-ymi)+ymi, +margin*(ymx-ymi)+ymx])
        fig = plt.gcf()
        fig.set_size_inches( MYFIG_WIDTH, MYFIG_HEIGHT )
        figfn = fn + '.pdf' #'.png'
        figfn = fn + '.png'
        #https://stackoverflow.com/questions/32185411/break-in-x-axis-of-matplotlib
        #fig.savefig( figfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
        #                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
        fig.savefig( figfn, dpi=MYFIG_DPI, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
                    transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None) 
        plt.close('all')
        return figfn

def plot_araullo_keyquantiles_intensity( fn, cdf, title, lgnd, *args, **kwargs ):
    """
    plot as a cumulative distribution for analyzed ROIs in paraprobe-araullo the cumulative distribution of image intensity for analyzed ROIs
    """
    if np.any(cdf > EPSILON):
        #title = 'Test'
        #lgnd = ['All clusters', 'Only interior cluster']
        xmi = np.min( cdf[:,0] )
        xmx = np.max( cdf[:,0] )
        ymi = 0.0 #np.min( cdf[:,1] )
        ymx = 1.0 #np.max( cdf[:,1] )
       
        fig, ax_xy = plt.subplots(1, 1)
        ax_xy.set_xlabel(r'Key quantile image intensity per ROI')
        #ax_xy.set_xscale('log')
        col = MYPARULA[0]
        ax_xy.set_ylabel(r'Cumulative distribution' ) #(1) unit
        ax_xy.plot( cdf[:,0], cdf[:,1], color = col )
        #plt.text( 10*np.ceil(np.log10(0.8)), 0.1, r'ROIs with intensities closer to 1.0 are likely better indexable' + '\n' 'combed', ha='right', va='bottom', transform=ax_xy.transAxes)
        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        #ax_xy.set_title(title)
        ax_xy.legend( lgnd, loc='upper left')
        #ax_xy.arrow( 0, 0, 0.5, 0.5, head_width=0.05, head_length=0.1, fc='k', ec='k')
        #ax_xy.arrow( xmi, 0.1, xmi, 0.8, head_width=0.05, head_length=0.1, facecolor='black', edgecolor='black', alpha=0.5 )
        #scale bar with add margin to the bottom and top of the yaxis to avoid that lines fall on x axis
        margin=0.05
        plt.xlim([-margin*(xmx-xmi)+xmi, +margin*(xmx-xmi)+xmx])
        plt.ylim([-margin*(ymx-ymi)+ymi, +margin*(ymx-ymi)+ymx])
        fig = plt.gcf()
        fig.set_size_inches( MYFIG_WIDTH, MYFIG_HEIGHT )
        figfn = fn + '.pdf' #'.png'
        figfn = fn + '.png'
        #https://stackoverflow.com/questions/32185411/break-in-x-axis-of-matplotlib
        #fig.savefig( figfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
        #                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
        fig.savefig( figfn, dpi=MYFIG_DPI, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
                    transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None) 
        plt.close('all')
        return figfn
        
def plot_araullo_keyquantiles_ioncounts( fn, cdf, title, lgnd, *args, **kwargs ):
    """
    plot as a cumulative distribution for analyzed ROIs in paraprobe-araullo the cumulative distribution of ion counts for analyzed ROIs
    """
    if np.any(cdf > EPSILON):
        #title = 'Test'
        #lgnd = ['All clusters', 'Only interior cluster']
        xmi = np.min( cdf[:,0] )
        xmx = np.max( cdf[:,0] )
        ymi = 0.0 #np.min( cdf[:,1] )
        ymx = 1.0 #np.max( cdf[:,1] )
       
        fig, ax_xy = plt.subplots(1, 1)
        ax_xy.set_xlabel(r'Number of ions in ROI')
        #ax_xy.set_xscale('log')
        col = MYPARULA[0]
        ax_xy.set_ylabel(r'Cumulative distribution' ) #(1) unit
        ax_xy.plot( cdf[:,0], cdf[:,1], color = col )
        #plt.text( 10*np.ceil(np.log10(0.8)), 0.1, r'ROIs with intensities closer to 1.0 are likely better indexable' + '\n' 'combed', ha='right', va='bottom', transform=ax_xy.transAxes)
        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        #ax_xy.set_title(title)
        ax_xy.legend( lgnd, loc='upper left')
        #ax_xy.arrow( 0, 0, 0.5, 0.5, head_width=0.05, head_length=0.1, fc='k', ec='k')
        #ax_xy.arrow( xmi, 0.1, xmi, 0.8, head_width=0.05, head_length=0.1, facecolor='black', edgecolor='black', alpha=0.5 )
        #scale bar with add margin to the bottom and top of the yaxis to avoid that lines fall on x axis
        margin=0.05
        plt.xlim([-margin*(xmx-xmi)+xmi, +margin*(xmx-xmi)+xmx])
        plt.ylim([-margin*(ymx-ymi)+ymi, +margin*(ymx-ymi)+ymx])
        fig = plt.gcf()
        fig.set_size_inches( MYFIG_WIDTH, MYFIG_HEIGHT )
        figfn = fn + '.pdf' #'.png'
        figfn = fn + '.png'
        #https://stackoverflow.com/questions/32185411/break-in-x-axis-of-matplotlib
        #fig.savefig( figfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
        #                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
        fig.savefig( figfn, dpi=MYFIG_DPI, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
                    transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None) 
        plt.close('all')
        return figfn
