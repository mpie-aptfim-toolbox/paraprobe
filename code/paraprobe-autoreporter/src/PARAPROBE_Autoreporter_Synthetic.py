# -*- coding: utf-8 -*-
"""
created 2020/05/28, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for strongly automatized reporting
of results from the paraprobe-transcoder tool
"""

import os, sys
import glob, subprocess
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
try:
    import periodictable as pse
except ImportError as e:
    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')
try:
    import h5py
except ImportError as e:
    raise ValueError('Install h5py Python package via e.g. pip install h5py !')

from PARAPROBE_Autoreporter_Numerics import *
from PARAPROBE_Autoreporter_CorpDesign import *
from PARAPROBE_Autoreporter_MetadataSynthetic import *
#from PARAPROBE_Autoreporter_Plots import *

#dictionary of settings to read from a paraprobe-spatstat runs which are worth to be reported
#paraprobe settings always have the same format within an paraprobe H5 file, they are
#3x1 matrices of strings the [0].decode() gives the value, [1].decode() the unit, [2].decode() a description
config_synthetic = {}

class autoreporter_synthetic():
    
    def __init__(self, fn, *args, **kwargs): #ds_rg
        #self.simid = simid
        self.toolname = 'Synthetic'
        self.h5src = fn
        self.natoms = 0
        self.config = config_synthetic
        self.results = {}
        #add here all functions which parse by default results from the source
        self.get_tool_config()
        self.get_natoms()
        
    def get_tool_config(self):
        fn = self.h5src
        hf = h5py.File( fn, 'r' )
        self.config = config_synthetic
        grpnm = '/SoftwareDetails/Metadata'
        if grpnm in hf.keys(): #old location of metadata, will become deprecated
            grpnm += '/Keywords'
        else: #new location of metadata
            grpnm = PARAPROBE_SYNTH_META_SFTWR
        print('Parsing configuration from group ' + grpnm)
        keys = list(hf[grpnm])
        for key in keys:
            print(key)
            tmp = hf[grpnm + '/' + key][:]
            self.config[key] = [ tmp[0].decode(), tmp[1].decode(), tmp[2].decode() ]
        self.results['Config'] = self.config 
        
    def get_natoms(self):
        """
        read natoms from the HDF5 file source
        """
        #fn = 'PARAPROBE.Synthetic.Results.SimID.7.h5'
        self.natoms = 0
        fn = self.h5src #[0][0]
        hf = h5py.File( fn, 'r')
        tmp = hf[PARAPROBE_SYNTH_VOLRECON_RES_XYZ][:,:]
        self.natoms = np.shape(tmp)[0]
        self.results['NumberOfAtoms'] = self.natoms
        
    def report(self):
        return self.results

# =============================================================================
# #example usage for a single dataset, tell me how many atoms of a certain type
#task = autoreporter_synthetic( 'PARAPROBE.Synthetic.Results.SimID.0.h5' )
#task.report_natoms()
# =============================================================================