# -*- coding: utf-8 -*-
"""
created 2020/06/22 Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for strongly automatized reporting
of results from the paraprobe tool on matters of profiling
"""

import os, fnmatch
import sys
import re
import glob, subprocess
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
try:
    import periodictable as pse
except ImportError as e:
    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')
try:
    import h5py
except ImportError as e:
    raise ValueError('Install h5py Python package via e.g. pip install h5py !')

from PARAPROBE_Autoreporter_Numerics import *
from PARAPROBE_Autoreporter_CorpDesign import *

class autoreporter_profiler():
    def __init__(self, toolnm, simid, *args, **kwargs):
        """
        read out csv profiler data
        simid is the simulation ID
        """
        self.results = []
        #simid = '3013903'
        path = '.'
        pattern = 'PARAPROBE.' + toolnm + '.SimID.' + simid + '.Rank.*.MyProfiling.csv'
        csvs = fnmatch.filter(os.listdir(path), pattern)
        #files = [f for f in os.listdir('.') if re.match(r'*3013903*\.csv', f)]
        if len(csvs) < 1:
            raise ValueError('No profiling data for this simulation ID !')
        nranks = len(csvs)
        telapsed_max = -1.0e36
        toolname = ''
        for csvfn in csvs:
            #csvfn = csvs[0]
            print(csvfn)
            tmp = csvfn.split('.')
            if len(tmp) != 8:
                raise ValueError('Unexpected filename formatting for paraprobe profiler files !')
            #rank = tmp[5]
            if tmp[1] != toolnm:
                raise ValueError('Unexpected filename formatting for paraprobe profiler files !')
            #toolname = tmp[1]
            #parse out profiling data from the csv file
            df = []
            df = pd.read_csv(csvfn, header=2, sep=';')
            telapsed_total = np.sum(df['WallClock'])
            if telapsed_total >= telapsed_max:
                telapsed_max = telapsed_total
            #qq=[]
            #qq=np.zeros((NRANKS,4)) #rank, write H5 rank-alone, elapsed time araullo, total          
            #qq[rank,0] = rank
            #qq[rank,1] = df.loc[df['What'] == 'WriteResultsH5']['WallClock'].iloc[0]
            #qq[rank,2] = df.loc[df['What'] == 'ExtractCrystallographyAraullo']['WallClock'].iloc[0]
            #qq[rank,3] = np.sum(df['WallClock']) - df.loc[df['What'] == 'WriteResultsH5']['WallClock'].iloc[0]
        #collect machine names and number of OpenMP threads
        suffix = '.h5'
        if toolnm == 'Tessellator':
            suffix = '.Stats.h5'
        #if toolnm == 'Ranger':
        #implement extra rule because ranging data can be in any type of file
        h5fn = path + '/PARAPROBE.' + toolnm + '.Results.SimID.' + simid + suffix # + '.h5'
        hf = h5py.File( h5fn, 'r' )
        grpnm = '/HardwareDetails/Metadata/Keywords' #old location, deprecated
        if grpnm in hf.keys():
            keyword = grpnm + '/' + 'OMPGetNumThreads'
            nthreads = hf[keyword][0].decode()
        else: #new location
            grpnm = '' #new, now tool-dependent
            if toolnm == 'Transcoder': ###MK::replace by metadata
                grpnm = '/VolumeRecon/Metadata/ToolEnvironment'
            if toolnm == 'Synthetic':
                grpnm = '/VolumeRecon/Metadata/ToolEnvironment'
            if toolnm == 'Ranger':
                grpnm = '/Ranging/Metadata/ToolEnvironment'
            if toolnm == 'Surfacer':
                grpnm = '/SurfaceRecon/Metadata/ToolEnvironment'
            if toolnm == 'Tessellator':
                grpnm = '/VoroTess/Metadata/ToolEnvironment'
            if toolnm == 'Spatstat':
                grpnm = '/SpatialStatistics/Metadata/ToolEnvironment'
            if toolnm == 'DBScan':
                grpnm = '/DBScan/Metadata/ToolEnvironment'
            if toolnm == 'Araullo':
                grpnm = '/AraulloPeters/Metadata/ToolEnvironment'
            if toolnm == 'Fourier':
                grpnm = '/DirectFT/Metadata/ToolEnvironment'
            if toolnm == 'Indexer':
                grpnm = '/OriAndPhaseIndexing/Metadata/ToolEnvironment'
            keyword = grpnm + '/' + 'OMPGetNumThreads'
        nthreads = hf[keyword][0].decode()
        self.results = [str(toolnm), str(nranks), nthreads, str(np.around( telapsed_max, decimals=0))]
   
    def report(self):
        return self.results
        
# =============================================================================
#example usage, tell me how many ions of a certain type
#task = autoreporter_spatstat( 'PARAPROBE.Transcoder.Results.SimID.7.apth5', 'PARAPROBE.Spatstat.Results.SimID.7.h5' )
#evaluates by default all RDFs and renders them into human-readable figures
#task.get_rdf()
#task.get_knn()

#ds = np.array( ['PARAPROBE.Spatstat.Results.SimID.2997401.h5' ])
#iarr = np.array( ['Al'] )
#jarr = np.array( ['Mg', 'Zn', 'Cu'] )
#task = autoreporter_spatstat( '', '' )
#task.get_huan_zhao_2018_Cij( ds, iarr, jarr )

#task.cnts
# task.report_natoms()
# =============================================================================
