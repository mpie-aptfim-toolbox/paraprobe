# -*- coding: utf-8 -*-
"""
created 2020/05/28, Markus K\"uhbach, m.kuehbach@mpie.de
defining numerics and precision
"""

EPSILON = 1.0e-6