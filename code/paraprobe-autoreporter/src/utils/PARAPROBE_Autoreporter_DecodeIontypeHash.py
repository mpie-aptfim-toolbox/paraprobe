# -*- coding: utf-8 -*-
"""
created 2020/05/28, Markus K\"uhbach, m.kuehbach@mpie.de
low-level utility function for decoding the paraprobes internal representation for ions into human-readable form
"""

import os, sys
import glob, subprocess
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
try:
    import periodictable as pse
except ImportError as e:
    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')
try:
    import h5py
except ImportError as e:
    raise ValueError('Install h5py Python package via e.g. pip install h5py !')

def decode_molecularion(hashval):
    """
    low-level function specific for paraprobe-ranger which extracts a human-readable
    molecular ion string description from an np.uint8 type and shaped (8,) numpy array using periodictable
    """
    #first six column values give Z/N pair of first, second, and third isotope component of the molecular ion
    #seventh column gives sign and eigth the charge of the molecular ion
    #first one is either 0 indicating UNKNOWN_IONTYPE, i.e. unkwown, or single isotope ion
    if hashval[0] == 0:
        print(str('unknown'))
        return 'unknown'
    else:
        #hashval = ityp_hashvals[1,:]
        #tmp = '['  #use later for r'''$\textnormal{[}{\textnormal{Al}}_2\textnormal{O}]^{3+}$''', unlikely example though...
        tmp = ''
        #old way, handles only a single element, no molecular ions
        #if hashval[1] == 0:
        #   tmp += str(pse.elements[hashval[0]])
        #else:
        #    tmp += '^{'+ str(hashval[1]) + '}' + str(pse.elements[hashval[0]])
        #    #can use latex notation, no fuss any more with formatting :), see the beauty here
        #new way, molecular ions keyword, at most three atoms in decreasing Z
        if hashval[0] != 0:
            strng = str(pse.elements[hashval[0]].symbol)
            if len(strng) == 2:
                tmp += strng[0:2]
            else:
                tmp += strng[0:1] + ':'
        if hashval[2] != 0:
            strng = str(pse.elements[hashval[2]].symbol)
            if len(strng) == 2:
                tmp += strng[0:2]
            else:
                tmp += strng[0:1] + ':'
        else:
            return tmp
        if hashval[4] != 0:
            strng = str(pse.elements[hashval[4]].symbol)
            if len(strng) == 2:
                tmp += strng[0:2]
            else:
                tmp += strng[0:1] + ':'
        else:
            return tmp
        #tmp += ']'
        ###MK::add the charges
        #print(str(tmp))
        return tmp

# =============================================================================
#example usage, tell me how many ions of a certain type
#molecularion_str = decode_molecularion(np.array([13, 13, 12, 1, 44, 77, 1, 12]))
# =============================================================================


###MK::the more advanced concept of using unsigned short int (uint16) hashes which encode number of neutrons and number of protons
#-np.sort(-np.unique(np.array([20+28*256,20+28*256,1+0*256]), return_counts=True))

def get_protons_neutrons( hashval ):
    nneutrons = int(hashval / 256);
    nprotons = hashval - (nneutrons*256);
    return np.array([nprotons, nneutrons], np.int32 )

def get_hash( nprotons, nneutrons ):
    return nprotons + nneutrons*256

def stringify_molecular_ion( nparr, charge ):
    """
    in:
        takes a numpy array of shape (N pairs of protons/neutrons encoded with uint8 + uint8 for charge)
        removes duplicates nuclids
    out:
        string, LaTex encoded
    """
    #nparr = what[4,:]
    #charge = charge[4,0]
    if np.shape(nparr)[0] % 2 == 0:
        M = np.uint32(np.shape(nparr)[0]/2)
        hashed = np.zeros( M, np.uint16 )
        for i in np.arange(0,M): #print(str(i))
            hashed[i] = get_hash( nparr[2*i+0], nparr[2*i+1] ) # + 256*nparr[2*i+1]
        disjoint = np.unique( hashed, return_counts=True)
        L = np.shape(disjoint[0])[0]
        s = r'''${['''
        for i in np.arange(1,L+1):
            #i = 2
            pro_neu = get_protons_neutrons(disjoint[0][-i])
            count = disjoint[1][-i]
            if pro_neu[0] != 0:
                s += r'''{^{''' + str(int(pro_neu[0])+int(pro_neu[1])) + r'''}'''
                for el in pse.elements:
                    if pro_neu[0] != el.number:
                        continue
                    else:
                        s += el.symbol
                        break
                if count > 1:
                    s += r'''}_{''' + str(int(count)) + r'''}'''
                else:
                    s += r'''}'''
        s += r''']}^{''' + str(int(charge)) + r'''+}$'''
    else:
        raise ValueError('nparr needs to be a np array of pairs of uint8 !')
    return s
