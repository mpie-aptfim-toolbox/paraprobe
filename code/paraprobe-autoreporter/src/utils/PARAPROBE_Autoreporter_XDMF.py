# -*- coding: utf-8 -*-
"""
created 2020/10/12, Markus K\"uhbach, m.kuehbach@mpie.de
Python utility class for paraprobe-autoreporter to be used for writing in XDMF
files for visualising data in Paraview and VisIt
"""

import numpy as np

XDMF_HEADER_LINE1=r'''<?xml version="1.0" ?>''' + '\n'
XDMF_HEADER_LINE2=r'''<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd" []>''' + '\n'
XDMF_HEADER_LINE3=r'''<Xdmf xmlns:xi="http://www.w3.org/2003/XInclude" Version="2.2">''' + '\n'

class xdmf_item():
    def __init__(self, key, h5fnm, dsnm, val, dtyp, shp, *args, **kwargs):
        """
        utility class holding values that build the XDMF file, e.g. matpoint ids

        """
        self.keyword = key
        self.h5fn = h5fnm
        self.h5dsnm = dsnm
        self.values = val
        self.dtyp = dtyp
        self.shp = shp
        
example_p3d = { 'Topology': xdmf_item( 'Topo', 'undefined.h5', 'undefined', np.ones([3,1], np.uint32), np.uint32, [3,1] ),
            'XYZ': xdmf_item( 'XYZ', 'undefined.h5', 'undefined', np.zeros([1,3], np.float32), np.float32, [1,3] ),
            'IDs': xdmf_item( 'IDs', 'undefined.h5', 'undefined', np.zeros([1,1], np.uint32), np.uint32, [1,1] ) }

example_v3d = { 'Topology': xdmf_item( 'Topo', 'undefined.h5', 'undefined', np.ones([3,1], np.uint32), np.uint32, [3,1] ),
                'XYZ': xdmf_item( 'XYZ', 'undefined.h5', 'undefined', np.zeros([1,3], np.float32), np.float32, [1,3] ),
                'Unitvectors': xdmf_item( 'Unitvectors', 'undefined.h5', 'undefined', np.zeros([1,3], np.float32), np.float32, [1,3] ) }

class xdmf():
    def __init__(self, *args, **kwargs ):
        self.xdmf = ''
        
    def visualize_points_3d(self, xdmffn, mydict ):
        """
        generate supplemental file to help visualizing a collection of points at locations XYZ with point IDs
        mydict, a dictionary of xdmf_items I need to check to find what we should write an XDMF file for
        """
        #mydict = example_p3d
        self.xdmf = XDMF_HEADER_LINE1
        self.xdmf = self.xdmf + XDMF_HEADER_LINE2
        self.xdmf = self.xdmf + XDMF_HEADER_LINE3
        self.xdmf = self.xdmf + r'''  <Domain name="AtomProbeTomography">''' + '\n'
        #self.xdmf = self.xdmf + r'''    <Grid Name="CellTime" GridType="Collection" CollectionType="Temporal">''' + "\n"
        self.xdmf = self.xdmf + r'''     <Grid Name="ROIs" GridType="Uniform">''' + '\n'
        #if mydict['Topology'].values == None:
        if mydict['Topology'].shp[0] % 3 == 0 and mydict['Topology'].shp[1] == 1:
            self.xdmf = self.xdmf + r'''        <Topology TopologyType="Mixed" NumberOfElements="''' + str(int(mydict['Topology'].shp[0]/3)) + r'''">''' + '\n'
            self.xdmf = self.xdmf + r'''          <DataItem Dimensions="''' + str(mydict['Topology'].shp[0]) + r'''" NumberType="UInt" Precision="4" Format="HDF">''' + '\n'
            self.xdmf = self.xdmf + r'''            ''' + mydict['Topology'].h5fn + r''':''' + mydict['Topology'].h5dsnm + '\n'
            self.xdmf = self.xdmf + r'''          </DataItem>''' + '\n'
            self.xdmf = self.xdmf + r'''        </Topology>''' + '\n'
            #if mydict['XYZ'].values not None:
            if mydict['XYZ'].shp[0] >= 1 and mydict['XYZ'].shp[1] == 3:
                self.xdmf = self.xdmf + r'''        <Geometry GeometryType="XYZ">''' + '\n'
                self.xdmf = self.xdmf + r'''          <DataItem Dimensions="''' + str(mydict['XYZ'].shp[0]) + r''' 3" NumberType="Float" Precision="8" Format="HDF">''' + '\n'
                self.xdmf = self.xdmf + r'''            ''' + mydict['XYZ'].h5fn + r''':''' + mydict['XYZ'].h5dsnm + '\n'
                self.xdmf = self.xdmf + r'''          </DataItem>''' + '\n'
                self.xdmf = self.xdmf + r'''        </Geometry>''' + '\n'
                #if mydict['IDs'].values not None:
                if mydict['IDs'].shp[0] == mydict['XYZ'].shp[0]:
                    self.xdmf = self.xdmf + r'''        <Attribute Name="''' + mydict['IDs'].keyword + r'''" AttributeType="Scalar" Center="Cell">''' + '\n'
                    self.xdmf = self.xdmf + r'''          <DataItem Dimensions="''' + str(mydict['IDs'].shp[0]) + r''' ''' + str(mydict['IDs'].shp[1]) + r'''" DataType="Float" Precision="4" Format="HDF">''' + '\n'
                    self.xdmf = self.xdmf + r'''            ''' + mydict['IDs'].h5fn + r''':''' + mydict['IDs'].h5dsnm + '\n'
                    self.xdmf = self.xdmf + r'''          </DataItem>''' + '\n'
                    self.xdmf = self.xdmf + r'''        </Attribute>''' + '\n'
                else:
                    raise ValueError('Mydict does not contain IDs information which is formatted as expected !')
            else:
                raise ValueError('Mydict does not contain XYZ coordinate information which is formatted as expected !')
            #else: 
            #    raise ValueError('Mydict does not contain XYZ coordinate information !')
        else: 
            raise ValueError('Mydict does not contain Topology information which is formatted as expected !')
        #else:
        #    raise ValueError('Mydict does not contain Topology information !')
        self.xdmf = self.xdmf + r'''      </Grid>''' + "\n"
        self.xdmf = self.xdmf + r'''  </Domain>''' + "\n"
        self.xdmf = self.xdmf + r'''</Xdmf>''' + "\n"
        xdmf = open( xdmffn, "wt")
        n = xdmf.write(self.xdmf)
        xdmf.close()
        self.xdmf = ''

    def visualize_vectors_3d(self, xdmffn, mydict ):
        """
        generate supplemental file to help visualizing a collection of 3d unit vectors located at XYZ     
        mydict, a dictionary of xdmf_items I need to check to find what we should write an XDMF file for
        """
        #mydict = example_v3d
        self.xdmf = XDMF_HEADER_LINE1
        self.xdmf = self.xdmf + XDMF_HEADER_LINE2
        self.xdmf = self.xdmf + XDMF_HEADER_LINE3
        self.xdmf = self.xdmf + r'''  <Domain name="AtomProbeTomography">''' + '\n'
        #self.xdmf = self.xdmf + r'''    <Grid Name="CellTime" GridType="Collection" CollectionType="Temporal">''' + "\n"
        self.xdmf = self.xdmf + r'''     <Grid Name="ROIs" GridType="Uniform">''' + '\n'
        if mydict['Topology'].shp[0] % 3 == 0 and mydict['Topology'].shp[1] == 1:
            self.xdmf = self.xdmf + r'''        <Topology TopologyType="Mixed" NumberOfElements="''' + str(int(mydict['Topology'].shp[0]/3)) + r'''">''' + '\n'
            self.xdmf = self.xdmf + r'''          <DataItem Dimensions="''' + str(mydict['Topology'].shp[0]) + r'''" NumberType="UInt" Precision="4" Format="HDF">''' + '\n'
            self.xdmf = self.xdmf + r'''            ''' + mydict['Topology'].h5fn + r''':''' + mydict['Topology'].h5dsnm + '\n'
            self.xdmf = self.xdmf + r'''          </DataItem>''' + '\n'
            self.xdmf = self.xdmf + r'''        </Topology>''' + '\n'
            if mydict['XYZ'].shp[0] >= 1 and mydict['XYZ'].shp[1] == 3:
                self.xdmf = self.xdmf + r'''        <Geometry GeometryType="XYZ">''' + '\n'
                self.xdmf = self.xdmf + r'''          <DataItem Dimensions="''' + str(mydict['XYZ'].shp[0]) + r''' 3" NumberType="Float" Precision="8" Format="HDF">''' + '\n'
                self.xdmf = self.xdmf + r'''            ''' + mydict['XYZ'].h5fn + r''':''' + mydict['XYZ'].h5dsnm + '\n'
                self.xdmf = self.xdmf + r'''          </DataItem>''' + '\n'
                self.xdmf = self.xdmf + r'''        </Geometry>''' + '\n'
                #if mydict['IDs'].values not None:
                if mydict['Unitvectors'].shp[0] == mydict['XYZ'].shp[0] and mydict['Unitvectors'].shp[1] == 3:
                    self.xdmf = self.xdmf + r'''        <Attribute Name="''' + mydict['Unitvectors'].keyword + r'''" AttributeType="Vector" Center="Node">''' + '\n'
                    self.xdmf = self.xdmf + r'''          <DataItem Dimensions="''' + str(mydict['Unitvectors'].shp[0]) + r''' ''' + str(mydict['Unitvectors'].shp[1]) + r'''" DataType="Float" Precision="4" Format="HDF">''' + '\n'
                    self.xdmf = self.xdmf + r'''            ''' + mydict['Unitvectors'].h5fn + r''':''' + mydict['Unitvectors'].h5dsnm + '\n'
                    self.xdmf = self.xdmf + r'''          </DataItem>''' + '\n'
                    self.xdmf = self.xdmf + r'''        </Attribute>''' + '\n'
                else:
                    raise ValueError('Mydict does not contain Unitvectors information which is formatted as expected !')
            else:
                raise ValueError('Mydict does not contain XYZ coordinate information which is formatted as expected !')
        else: 
            raise ValueError('Mydict does not contain Topology information which is formatted as expected !')
        self.xdmf = self.xdmf + r'''      </Grid>''' + "\n"
        self.xdmf = self.xdmf + r'''  </Domain>''' + "\n"
        self.xdmf = self.xdmf + r'''</Xdmf>''' + "\n"
        xdmf = open( xdmffn, "wt")
        n = xdmf.write(self.xdmf)
        xdmf.close()
        self.xdmf = ''
        
    def visualize_vectors_3d_xml(self, xdmffn, gridnm, mydict ):
        """
        generate supplemental file to help visualizing a collection of 3d unit vectors located at XYZ     
        mydict, a dictionary of xdmf_items I need to check to find what we should write an XDMF file for
        """
        #mydict = example_v3d
        self.xdmf = XDMF_HEADER_LINE1
        self.xdmf = self.xdmf + XDMF_HEADER_LINE2
        self.xdmf = self.xdmf + XDMF_HEADER_LINE3
        self.xdmf = self.xdmf + r'''  <Domain name="AtomProbeTomography">''' + '\n'
        #self.xdmf = self.xdmf + r'''    <Grid Name="CellTime" GridType="Collection" CollectionType="Temporal">''' + "\n"
        self.xdmf = self.xdmf + r'''     <Grid Name="''' + gridnm + r'''" GridType="Uniform">''' + '\n'
        if mydict['Topology'].shp[0] % 3 == 0 and mydict['Topology'].shp[1] == 1:
            self.xdmf = self.xdmf + r'''        <Topology TopologyType="Mixed" NumberOfElements="''' + str(int(mydict['Topology'].shp[0]/3)) + r'''">''' + '\n'
            self.xdmf = self.xdmf + r'''          <DataItem Dimensions="''' + str(mydict['Topology'].shp[0]) + r'''" NumberType="UInt" Precision="4" Format="XML">''' + '\n'
            N = len(mydict['Topology'].values[:,0])
            for i in np.arange(0,N):
                self.xdmf = self.xdmf + str(mydict['Topology'].values[i,0]) + '\n'
            self.xdmf = self.xdmf + r'''          </DataItem>''' + '\n'
            self.xdmf = self.xdmf + r'''        </Topology>''' + '\n'
            if mydict['XYZ'].shp[0] >= 1 and mydict['XYZ'].shp[1] == 3:
                self.xdmf = self.xdmf + r'''        <Geometry GeometryType="XYZ">''' + '\n'
                self.xdmf = self.xdmf + r'''          <DataItem Dimensions="''' + str(mydict['XYZ'].shp[0]) + r''' 3" NumberType="Float" Precision="8" Format="XML">''' + '\n'
                N = len(mydict['XYZ'].values[:,0])
                for i in np.arange(0,N):
                    self.xdmf = self.xdmf + str(mydict['XYZ'].values[i,0]) + '  ' + str(mydict['XYZ'].values[i,1]) + '  ' + str(mydict['XYZ'].values[i,2]) + '\n'
                self.xdmf = self.xdmf + r'''          </DataItem>''' + '\n'
                self.xdmf = self.xdmf + r'''        </Geometry>''' + '\n'
                if mydict['Unitvectors'].shp[0] == mydict['XYZ'].shp[0] and mydict['Unitvectors'].shp[1] == 3:
                    self.xdmf = self.xdmf + r'''        <Attribute Name="''' + mydict['Unitvectors'].keyword + r'''" AttributeType="Vector" Center="Node">''' + '\n'
                    self.xdmf = self.xdmf + r'''          <DataItem Dimensions="''' + str(mydict['Unitvectors'].shp[0]) + r''' ''' + str(mydict['Unitvectors'].shp[1]) + r'''" DataType="Float" Precision="4" Format="XML">''' + '\n'
                    N = len(mydict['Unitvectors'].values[:,0])
                    for i in np.arange(0,N):
                        self.xdmf = self.xdmf + str(mydict['Unitvectors'].values[i,0]) + '  ' + str(mydict['Unitvectors'].values[i,1]) + '  ' + str(mydict['Unitvectors'].values[i,2]) + '\n'
                    self.xdmf = self.xdmf + r'''          </DataItem>''' + '\n'
                    self.xdmf = self.xdmf + r'''        </Attribute>''' + '\n'
                else:
                    raise ValueError('Mydict does not contain Unitvectors information which is formatted as expected !')
            else:
                raise ValueError('Mydict does not contain XYZ coordinate information which is formatted as expected !')
        else: 
            raise ValueError('Mydict does not contain Topology information which is formatted as expected !')
        self.xdmf = self.xdmf + r'''      </Grid>''' + "\n"
        self.xdmf = self.xdmf + r'''  </Domain>''' + "\n"
        self.xdmf = self.xdmf + r'''</Xdmf>''' + "\n"
        xdmf = open( xdmffn, "wt")
        n = xdmf.write(self.xdmf)
        xdmf.close()
        self.xdmf = ''
        
    

#example how to use
#xdmf = xdmf()
#xdmf.visualize_points_3d( 'TestPoints3d.xdmf', example_p3d )
#xdmf.visualize_vectors_3d( 'TestVector3d.xdmf', example_v3d )
        
# =============================================================================
#     def add_timestep(self, iter, t, h5fn, ntris, nverts ):
#         self.xdmf = self.xdmf + r'''      <Grid Name="voronoi" GridType="Uniform">''' + "\n" 
#         self.xdmf = self.xdmf + r'''        <Time Value="''' + str(t) + r'''"/>''' + "\n"
#         self.xdmf = self.xdmf + r'''        <Topology TopologyType="Mixed" NumberOfElements="''' + str(ntris) + r'''">''' + "\n"
#         self.xdmf = self.xdmf + r'''          <DataItem Dimensions="''' + str(int(5*ntris)) + r'''" NumberType="UInt" Precision="4" Format="HDF">''' + "\n"
#         self.xdmf = self.xdmf + r'''            ''' + h5fn + r''':/03_SimulationResults/Snapshot/Iteration''' + str(int(iter)) + r'''/ContourHulls/XDMFTopo''' + "\n"
#         self.xdmf = self.xdmf + r'''          </DataItem>''' + "\n"
#         self.xdmf = self.xdmf + r'''        </Topology>''' + "\n"
#         self.xdmf = self.xdmf + r'''        <Geometry GeometryType="XYZ">''' + "\n"
#         self.xdmf = self.xdmf + r'''          <DataItem Dimensions="''' + str(int(nverts)) + r''' 3" NumberType="Float" Precision="8" Format="HDF">''' + "\n"
#         self.xdmf = self.xdmf + r'''            ''' + h5fn + r''':/03_SimulationResults/Snapshot/Iteration''' + str(int(iter)) + r'''/ContourHulls/XDMFVertices''' + "\n"
#         self.xdmf = self.xdmf + r'''          </DataItem>''' + "\n"
#         self.xdmf = self.xdmf + r'''        </Geometry>''' + "\n"
#         self.xdmf = self.xdmf + r'''        <Attribute Name="MobilityTimesEnergy" AttributeType="Scalar" Center="Cell">''' + "\n"
#         self.xdmf = self.xdmf + r'''          <DataItem Dimensions="''' + str(int(ntris)) + r''' 1" DataType="Float" Precision="4" Format="HDF">''' + "\n"
#         self.xdmf = self.xdmf + r'''            ''' + h5fn + r''':/03_SimulationResults/Snapshot/Iteration''' + str(int(iter)) + r'''/ContourHulls/MobilityTimesEnergy''' + "\n"
#         self.xdmf = self.xdmf + r'''          </DataItem>''' + "\n"
#         self.xdmf = self.xdmf + r'''        </Attribute>''' + "\n"
#         self.xdmf = self.xdmf + r'''      </Grid>''' + "\n"
# 
# =============================================================================
