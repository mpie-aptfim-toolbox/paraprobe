# -*- coding: utf-8 -*-
"""
created 2020/05/28, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for showing how to condense results of APT analysis tools
into Latex and PDF reports using strong automation. Testing here for a dataset with precipitates from
H. Zhao et al. Scripta Mat, 2018, dx.doi.org/10.1016/j.scriptamat.2018.05.024
"""

import os, sys, glob, subprocess
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import periodictable as pse
import h5py

#MYTOOLPATH='H:/BIGMAX_RELATED/MPIE-APTFIM-TOOLBOX/paraprobe/code/paraprobe-autoreporter'
#h5fn=MYTOOLPATH + '/' + 'run' + '/' + 'PARAPROBE.Spatstat.Results.SimID.20001.h5'
#texfn=MYTOOLPATH + '/' + 'run' + '/' + 'PARAPROBE.Autoreporter.SimID.20001.tex'
#sys.path.append(MYTOOLPATH + '/' + 'src')
#sys.path.append(MYTOOLPATH + '/' + 'src' + '/' + 'metadata')
#be determinsitic
#np.random.seed(0)
#if it is in Cstyle stored in the HDF5 file it will remain Cstyle ordered in numpy so no implicit transformation
#http://christopherlovell.co.uk/blog/2016/04/27/h5py-intro.html

#tool-specific includes
#from MarkusTexDocumentStructure_01 import *
#from MarkusTexUserTables_01 import *
#from MarkusTexUserFigures_01 import *
#from MarkusTexUserPeakIdent_01 import *

#----------------------------------------------------------->begin JUPYTER notebook part

from PARAPROBE_Autoreporter_Numerics import *
from PARAPROBE_Autoreporter_CorpDesign import *

from PARAPROBE_Autoreporter_Transcoder import *
from PARAPROBE_Autoreporter_Ranger import *
from PARAPROBE_Autoreporter_Surfacer import *
from PARAPROBE_Autoreporter_Spatstat import *
from PARAPROBE_Autoreporter_Tessellator import *
from PARAPROBE_Autoreporter_DBScan import *

#from PARAPROBE_MetadataSpatstatDefsH5 import * 
#from PARAPROBE_Profiler import *

#define the datasets (reconstructed and with ranging data)
ds = np.array([  'R76-25946-2h', 
                 'R76-26865-PA',
                 'R76-26737-PA',
                 'R76-26110-30min',
                 'R76-25139-2h',
                 'R76-24909-30min',
                 'R76-26120-OA'    ])
ds = np.array([  'R76-26120-OA'    ])

N = np.uint32(1)
for dataset in ds:
    tic()
    SimulationID = 7 #N
    
    #dictionary of results inside own results objects specific for the task and tool
    res = {}
    recon = 'PARAPROBE.Transcoder.Results.SimID.' + str(SimulationID) + '.apth5'
    res['1_Transcoder'] = autoreporter_transcoder( recon )
    res['1_Transcoder'].report_natoms() #single number
    #make sure that arguments to res[<keyword>] are keywords only!

    res['2_Ranger'] = autoreporter_ranger( recon )
    res['2_Ranger'].get_iontypes() #pandas table with cnts
    #show me the composition
    res['2_Ranger'].get_composition()

    hull = 'PARAPROBE.Surfacer.Results.SimID.' + str(SimulationID) + '.h5'
    res['3_Surfacer'] = autoreporter_surfacer( recon, hull )
    res['3_Surfacer'].get_distances()

    spst = 'PARAPROBE.Spatstat.Results.SimID.' + str(SimulationID) + '.h5'
    #cross-dependencies ! spatstat may need ranges but these are not in the spatstat object
    #could have all in the same file, versioned HDF5 files not a good idea write access!
    #sure better than having 1000s of csv of excel files for your spatial statistics
    res['4_Spatstat'] = autoreporter_spatstat( recon, spst )
    res['4_Spatstat'].get_rdf()
    res['4_Spatstat'].get_knn() #currently do all
    #res['4_Spatstat'].get_knn( 'all' ) #all of them
    #res['4_Spatstat'].get_knn( 'all' ) #only Task32 if available    
    res['4_Spatstat'].get_sdm()
    #MK::functionalize further by encapsulating the results of each analysis Task in an object, e.g. of type knn inheriting from histogram
    
    #example show the results of a particular task in Jupyter
    #res['4_Spatstat'].knn_tasks['Task10'].plot() 
    #how do you know KNN Task? a dictionary of dictionaries of task objects each object with own methods

    tess = 'PARAPROBE.Tessellator.Results.SimID.' + str(SimulationID) + '.Stats.h5'
    res['5_Tessellator'] = autoreporter_tessellator( recon, hull, tess )
    res['5_Tessellator'].get_voronoi_volume_distro()
    res['5_Tessellator'].get_sum_voronoi_volume_vs_distance()

    dbsc = 'PARAPROBE.DBScan.Results.SimID.' + str(SimulationID) + '.h5'
    res['6_DBScan'] = autoreporter_dbscan( recon, dbsc )
    res['6_DBScan'].get_cluster_size_distros()

    #prec = 'PARAPROBE.Particles.Results.SimID.7.h5'
    #res['6_Particles'] = autoreporter_particles( recon, prec )
    #res['6_Particles'].report_sizedistros()

    #make a report into a PDF and a Latex source code
    latex = autoreporter_latex( 'PARAPROBE.Autoreporter.Results.SimID.' + str(SimulationID), res )
    #will give a PDF with chapters and a tex document







#13 lines of code against hundreds of GUI hours takes all boring stuff to the computer
#reproducibility have all scripts in versions to complete rerun each analysis 
#this scripts are what the end user needs to get high-level access to results
#from state-of-the-art APT data mining methods!
#the interesting thing about APT is the methods used are very established numerics and computer science wise
#why does everybody have to rerun such analysis, we dont, we use IVAS but this is not transparent
#and that is the key difference also we achieve these results with transparent code
#and we can interface with results from others

hdl = spst.spatstatHdl( h5fn )
report_rdf = hdl.explore_rdf()

#report_knn = hdl.explore_knn()
report_sdm = hdl.explore_sdm()

toc()

#general class instance defining the meat of the TeX file and specifying the methods
#which define the Corporate Design of the report
import PARAPROBE_XelatexHdl as xtex

report = xtex.xelatexHdl( texfn );
report.init( r'''Markus K\"uhbach''' ) #adds Tex package definitions

#example of how to add a comment to your analysis
comment = r'''This is a personal comment to this analysis can use heavy formulas and \LaTeX notation and formulas'''

report.add_introduction( comment )

#automatically generate figures histograms, CDFs, tables and append to report
report.add_section_rdf( 'Data mining radial distribution function for multiple ion types ...' )
report.add( report_rdf )

report.add_section_knn( '... and k-nearest neighbor analyses have seldom be so conveniently accessible!' )
#report += spst.explore_knn( h5fn )

report.add_section_sdm( 'Surplus we can have now even more fun two-point statistics of our APT data!' )

#report += spst.explore_sdm( h5fn )

#saves me hours in front of the GUI, fully transparent ...
report.submit()


#go to your favorite xelatex Tex editor and compile into a PDF report
#sweet, now I can flesh out figures from the report into paper
#sweet, now I can focus on the science within the dataset again

#----------------------------------------------------------->end JUPYTER notebook part













#which statistics were harvested?
spatstat=list(hf['SpatialStatistics'])

if 'KNN' in spatstat:
    knn = list(hf['SpatialStatistics/KNN'])
    print('KNN exists')
    if 'Metadata' in knn:
        tasks = list(hf[PARAPROBE_SPATSTAT_KNN_META])
        for tsk in tasks:
            this = list(hf[PARAPROBE_SPATSTAT_KNN_META + '/' + tsk])
            if 'RandomizedLabels' in this:
                print(tsk + ' used randomized labels')
            else:
                print(tsk + ' used original labels')
            if 'kthNearest' in this:
                kth = tsk['kthNearest']
            else:
                kth = 0
            print(kth)
            #get target single/molecular ion type
            if 'IontypeTargets' in this:
                tg = np.array(list(hf[PARAPROBE_SPATSTAT_KNN_META + '/' + tsk + '/IontypeTargets'])).flatten()
                targets = ""
                if tg[2] > 0:
                    targets += str(pse.mass(tg[2]).symbol)
                if tg[4] > 0:
                    targets += str(pse.elements[tg[4]].symbol)
                if tg[6] > 0:
                    targets += str(pse.elements[tg[6]].symbol)
            if 'IontypeNeighbors' in this:
                tg = np.array(list(hf[PARAPROBE_SPATSTAT_KNN_META + '/' + tsk + '/IontypeNeighbors'])).flatten()
                nbors = ""
                if tg[2] > 0:
                    nbors += str(pse.elements[tg[2]].symbol) ###SYMBOLS DONT MATCH!
                if tg[4] > 0:
                    nbors += str(pse.elements[tg[4]].symbol)
                if tg[6] > 0:
                    nbors += str(pse.elements[tg[6]].symbol)
            print('Combination ' + targets + '\t' + nbors)
    if 'Results' in knn:
        tasks = list(hf[PARAPROBE_SPATSTAT_KNN_RES])
        for tsk in tasks: 
            this = list(hf[PARAPROBE_SPATSTAT_KNN_RES + '/' + tsk])
            if 'Cnts' in this:
                hst1d_res = hf[PARAPROBE_SPATSTAT_KNN_RES + '/' + tsk + '/Cnts']
                nl = len(hst1d_res[:,1])
                #is statistic significant, then go from hst1d_res empirical histogram to ECDF
                if np.any(hst1d_res[:,1] > EPSILON):
                    ecdf = np.zeros([nl-1,2],np.float64)
                    ecdf[:,0] = hst1d_res[0:nl-1,0]
                    ecdf[:,1] = np.cumsum(hst1d_res[0:nl-1,1])/np.sum(hst1d_res[0:nl-1,1])
                    
                    #generate file name for the figure like in the HDF5
                    pngfn = (PARAPROBE_SPATSTAT_KNN_RES + '/' + tsk + '/Cnts').replace('/','_') + '.png'
                   
                    ####PLOT( ecdf, pngfn )
                    ####ADD SUBFIGURE( pngfn, targets, nbors, 0.5 )
                    ####plt.plot(ecdf[:,0],ecdf[:,1])



dsnm=list(hf.keys())

def descend_obj(obj,sep='\t'):
    """
    Iterate through groups in a HDF5 file and prints the groups and datasets names and datasets attributes
    """
    if type(obj) in [h5py._hl.group.Group,h5py._hl.files.File]:
        for key in obj.keys():
            print (sep,'-',key,':',obj[key])
            descend_obj(obj[key],sep=sep+'\t')
    elif type(obj)==h5py._hl.dataset.Dataset:
        for key in obj.attrs.keys():
            print(sep,'\t','-',key,':',obj.attrs[key])

descend_obj(hf)
