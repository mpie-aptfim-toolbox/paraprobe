# -*- coding: utf-8 -*-
"""
created 2020/06/07, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for strongly automatized reporting
of results from the paraprobe-dbscan tool
"""

import os, sys
import glob, subprocess
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
try:
    import periodictable as pse
except ImportError as e:
    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')
try:
    import h5py
except ImportError as e:
    raise ValueError('Install h5py Python package via e.g. pip install h5py !')

from PARAPROBE_Autoreporter_Numerics import *
from PARAPROBE_Autoreporter_CorpDesign import *
from PARAPROBE_Autoreporter_DecodeIontypeHash import *
from PARAPROBE_Autoreporter_Plots import *

from PARAPROBE_Autoreporter_MetadataTranscoder import *
from PARAPROBE_Autoreporter_MetadataSurfacer import *
from PARAPROBE_Autoreporter_MetadataTessellator import *

config_tessellator = { 'InputfilePSE': '',
                       'InputfileReconstruction': '',
                       'InputfileDistances': '',
                       'SpatialSplittingMethod': '',
                       'CellErosionDistance': '',
                       'IOCellVolume': '',
                       'IOCellNeighbors': '',
                       'IOCellShape': '',
                       'IOCellProfiling': '',
                       'GuardZoneFactor': '',
                       'IonsPerBlock':  '' }                       

class autoreporter_tessellator():
    
    def __init__(self, ions, hull, tess, *args, **kwargs):
        #self.simid = simid
        self.toolname = 'Tessellator'
        self.h5src_ions = ions
        self.h5src_hull = hull
        self.h5src_tess = tess
        
        self.config = config_tessellator
        #add here all functions which parse by default results from the source
        self.results = {}
        #add here all functions which parse by default results from the source
        self.get_tool_config()
        
    def get_tool_config(self):
        #fn = 'C:/Users/kaiob/eclipse-workspace/mpie-aptfim-toolbox/paraprobe-autoreporter/HuanZhaoScrMatCaseStudy/PARAPROBE.Transcoder.Results.SimID.26120.h5'
        fn = self.h5src_tess
        hf = h5py.File( fn, 'r' )
        self.config = config_tessellator
        grpnm = '/SoftwareDetails/Metadata'
        if grpnm in hf.keys(): #old location of metadata, will become deprecated
            grpnm += '/Keywords'
        else: #new location of metadata
            grpnm = PARAPROBE_TESS_META_SFTWR
        print('Parsing configuration from group ' + grpnm)
        keys = list(hf[grpnm])
        self.config = {}
        for key in keys:
            print(key)
            tmp = hf[grpnm + '/' + key][:]
            self.config[key] = [ tmp[0].decode(), tmp[1].decode(), tmp[2].decode() ]
        self.results['Config'] = self.config

    def get_sum_voronoi_volume(self, dthr):
        """
        report sum of Voronoi cell volume for all ions at least dthr from the dataset edge in hull
        if dthr smaller than use thresholding upon tessellation, warns and return 0
        """
        #fn = 'C:/Users/kaiob/eclipse-workspace/mpie-aptfim-toolbox/paraprobe-dbscan/PARAPROBE.DBScan.Results.SimID.7.h5'
        #fn = 'PARAPROBE.Tessellator.Results.SimID.7.Stats.h5'
        fn = self.h5src_tess
        hf_tess = h5py.File( fn, 'r' )
        #get used thresholding distance
        dero = hf_tess[PARAPROBE_TESS_META_DERO][0,0]
        if not dero == None:
            #check that dthr is >= dmin
            if dthr >= dero:
                #parse out the Voronoi volume
                V = hf_tess[PARAPROBE_TESS_RES_VOL][:,0]
                #and distance of ion to dataset edge
                #fn_hull = 'PARAPROBE.Surfacer.Results.SimID.7.h5'
                fn_hull = self.h5src_hull
                hf_hull = h5py.File( fn_hull, 'r' )
                d = hf_hull[PARAPROBE_SURF_DIST_RES_D][:,0]
                if np.shape(V)[0] == np.shape(d)[0]:
                    Vd = np.zeros([np.shape(V)[0], 2], np.float64 )
                    Vd[:,0] = V
                    del V
                    Vd[:,1] = d
                    del d
                    #dthr = 2.0
                    return np.sum(Vd[Vd[:,1] >= dthr][:,0])
                else:
                    return 0.0
            else:
                raise ValueError('dthr is not larger than used erosion distance during tessellating ' + str(dero) + ' !')
        else:
            raise ValueError('Unable to parse the thresholding distance used upon constructing the Voronoi tessellation of the dataset !')

    def get_voronoi_volume_distro(self):
        #replace large values for which the volume is 0.0 because paraprobe-tessellator detected the cells were too close to the dataset edge
        #such that it was impossible to compute a correct cell geometry that was unaffected by the dataset edge
        #fn = 'PARAPROBE.Tessellator.Results.SimID.7.Stats.h5'
        fn = self.h5src_tess
        hf_tess = h5py.File( fn, 'r' )
        V = hf_tess[PARAPROBE_TESS_RES_VOL][:,0]
        dero = hf_tess[PARAPROBE_TESS_META_DERO][0,0]
        
        imgfn = (fn + PARAPROBE_TESS_RES + '.VolCDF').replace('/','.')
        imgttl = (r'$d_{ero} = $' + str(np.around(np.float64(dero), decimals=2)) + 'nm').replace('/','.').lstrip('.')
        imglgnd = ['Voronoi cell volume']
        
        #render the figure 
        self.results['VoronoiSizeDistroFigure'] = plot_voronoi_size_distro( imgfn, V, dero, imgttl, imglgnd )
            
    def get_sum_voronoi_volume_vs_distance(self):
        """
        report sum of the Voronoi cell volume for all ions that are at least a certain distance from the dataset edge in hull
        """
        #fn = 'PARAPROBE.Tessellator.Results.SimID.7.Stats.h5'
        fn = self.h5src_tess
        hf_tess = h5py.File( fn, 'r' )
        #parse out the Voronoi volume
        V = hf_tess[PARAPROBE_TESS_RES_VOL][:,0]
        dero = hf_tess[PARAPROBE_TESS_META_DERO][0,0]
        #and the distance of the ions to the dataset edge
        #fn_hull = 'PARAPROBE.Surfacer.Results.SimID.7.h5'
        fn_hull = self.h5src_hull
        hf_hull = h5py.File( fn_hull, 'r' )
        d = hf_hull[PARAPROBE_SURF_DIST_RES_D][:,0]
        #which distancing mode was used? if it was skin there are many ions with very large distances
        #we need to replace these distances with the threshold used in paraprobe-surfacer
        grpnm = '/SoftwareDetails/Metadata/Keywords'
        if grpnm in hf_hull.keys(): #old location of metadata, will become deprecated
            grpnm = grpnm + '/DistancingMode'
        else: #new location of metadata
            grpnm = PARAPROBE_SURF_META_SFTWR + '/DistancingMode'
        #distmode = hf_hull['SoftwareDetails/Metadata/Keywords/DistancingMode'][0].decode('utf-8')
        distmode = hf_hull[grpnm][0].decode('utf-8')
        
        if distmode == '2': #2 is skin mode, 1 is complete mode
            grpnm = '/SoftwareDetails/Metadata/Keywords'
            if grpnm in hf_hull.keys(): #old location of metadata, will become deprecated
                grpnm = grpnm + '/DistancingRadiusMax'
            else: #new location of metadata
                grpnm = PARAPROBE_SURF_META_SFTWR + '/DistancingRadiusMax'
            #distrmax = np.around(np.float32(hf_hull['SoftwareDetails/Metadata/Keywords/DistancingRadiusMax'][0].decode('utf-8')), decimals=2)
            distrmax = np.around(np.float32(hf_hull[grpnm][0].decode('utf-8')), decimals=2)
            d[d > 1.0e10] = distrmax
        if np.shape(V)[0] == np.shape(d)[0]:
            Vd = np.zeros([np.shape(V)[0], 2], np.float64 )
            Vd[:,0] = V
            del V
            Vd[:,1] = d
            del d
            #sort the Vd by increasing distance
            idx = np.argsort(Vd[:,1])
            srtVd = np.zeros([np.shape(Vd)[0], 2], np.float64 )
            srtVd[:,0] = Vd[idx,0]
            srtVd[:,1] = Vd[idx,1]            
            del Vd
            csum = np.sum(srtVd[:,0]) - np.cumsum(srtVd[:,0])
            #plt.plot(srtVd[:,1], csum[:])
            
            imgfn = (fn + PARAPROBE_TESS_RES + '.VolVsDist').replace('/','.')
            if distmode == '2':
                imgttl = (r'Skin distancing $d_{max} = $' + str(np.around(np.float64(distrmax), decimals=2)) + 'nm, ' + r'$d_{ero} = $' + str(np.around(np.float64(dero), decimals=2)) + 'nm').replace('/','.').lstrip('.')
            else:
                imgttl = (r'Complete distancing, ' + r'$d_{ero} = $' + str(np.around(np.float64(dero), decimals=2)) + 'nm').replace('/','.').lstrip('.')
            imglgnd = [r'$\sum$ Voronoi volume']
        
            #render the figure 
            self.results['SumVoronoiVolvsDistanceFigure'] = plot_sum_voronoi_volume_vs_distance( imgfn, srtVd[:,1], csum[:], dero, imgttl, imglgnd )
            
            #tess class object keeps a dictionary for all successfully created files to include them into the latex report
            #if os.path.isfile( imgfn ) == True:
            #    self.tess_img[imgfn] = imgfn
            
        else:
            raise ValueError('Either V or d do not exist or have different length !')
            
    def report(self):
        return self.results
  
# =============================================================================
#example usage, tell me the Voronoi cell volume for the ions
#task = autoreporter_tessellator( 'PARAPROBE.Transcoder.Results.SimID.7.apth5',           \
#                                 'PARAPROBE.Surfacer.Results.SimID.7.h5',                \
#                                 'PARAPROBE.Tessellator.Results.SimID.7.Stats.h5' )
#evaluates the sum of the Voronoi cell volume for all ions at least dthr distance from the dataset edge
#here, we use a threshold of 1.5nm for instance
#dthr = 1.0
#print(str(task.get_sum_voronoi_volume( dthr )))
#how is the distribution of Voronoi cell volume?
#task.get_voronoi_volume_distro()
#how large does the dataset remain when we erode more and more ions from the dataset edge?
#task.get_sum_voronoi_volume_vs_distance()
# =============================================================================
