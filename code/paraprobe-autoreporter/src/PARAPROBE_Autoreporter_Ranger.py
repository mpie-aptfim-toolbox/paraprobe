# -*- coding: utf-8 -*-
"""
created 2020/05/28, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for strongly automatized reporting
of results from the paraprobe-ranger tool
"""

import os, sys
import glob, subprocess
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
try:
    import periodictable as pse
except ImportError as e:
    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')
try:
    import h5py
except ImportError as e:
    raise ValueError('Install h5py Python package via e.g. pip install h5py !')

from PARAPROBE_Autoreporter_Numerics import *
from PARAPROBE_Autoreporter_CorpDesign import *
from PARAPROBE_Autoreporter_DecodeIontypeHash import *
from PARAPROBE_Autoreporter_Plots import *

from PARAPROBE_Autoreporter_MetadataTranscoder import *
from PARAPROBE_Autoreporter_MetadataSynthetic import *
from PARAPROBE_Autoreporter_MetadataRanger import *

#dictionary of settings to read from a paraprobe-spatstat runs which are worth to be reported
#paraprobe settings always have the same format within an paraprobe H5 file, they are
#3x1 matrices of strings the [0].decode() gives the value, [1].decode() the unit, [2].decode() a description
config_ranger = {} #there are none

class autoreporter_ranger():
    
    def __init__(self, fn, *args, **kwargs):
        #self.simid = simid
        self.toolname = 'Ranger'
        self.h5src = fn
        
        #add here all functions which parse by default results from the source
        #self.get_iontypes()
        #self.get_composition()
        self.config = config_ranger
        #dictionary keeping references to the latest results
        self.results = {}        
        #add here all functions which parse by default results from the source
        self.get_tool_config()
        
    def get_tool_config(self):
        fn = self.h5src
        hf = h5py.File( fn, 'r' )
        self.config = config_ranger
        grpnm = '/SoftwareDetails/Metadata'
        if grpnm in hf.keys(): #old location of metadata, will become deprecated
            grpnm += '/Keywords'
        else: #new location of metadata
            grpnm = PARAPROBE_RANGER_META_SFTWR
        print('Parsing configuration from group ' + grpnm)
        keys = list(hf[grpnm])
        self.config = {}
        for key in keys:
            print(key)
            tmp = hf[grpnm + '/' + key][:]
            self.config[key] = [ tmp[0].decode(), tmp[1].decode(), tmp[2].decode() ]
        self.results['Config'] = self.config 
        
    def get_iontypes(self):
        """read which ion types ranger detected on h5src
        """
        #fn = 'PARAPROBE.Synthetic.Results.SimID.0.h5'
        #fn = 'PARAPROBE.Transcoder.Results.SimID.2765601.h5'
        fn = self.h5src
        hf = h5py.File( fn, 'r')
        #read ion type labels from HDF5 file using h5py into a numpy array and from this
        #use pandas to compile a pivot table for the composition
        pdfr = pd.DataFrame( columns=['IontypeID'], data = hf[PARAPROBE_RANGER_RES_TYPID][:,0] )
        #print(pdfr)
        pdfr['Cnts'] = 1
        cnts = pd.pivot_table( pdfr, values='Cnts', columns='IontypeID', aggfunc = "count" )
        print(cnts)
        del pdfr
        #replace the uint8 ion type ID keys which paraprobe uses internally to sth more human readable
        #eg the element names most likely associated with a certain element type
        #this is ranging information stored also in the h5src source file
        #paraprobe uses a particular format to encode molecular ions their elements and the charge
        ityp_hashvals = hf[PARAPROBE_RANGER_META_TYPID_DICT_WHAT][:,:]
        if np.shape(ityp_hashvals)[0] < cnts.shape[1]:
            raise ValueError('Data in ' + fn + ' are inconsistent: number of iontypes referred to in ' + PARAPROBE_RANGER_RES_TYPID + ' doesnt match number of rows in ' + PARAPROBE_RANGER_META_TYPID_DICT_WHAT)
        else:
            newnames = {}
            for i in np.arange(0,np.shape(ityp_hashvals)[0]):
                newnames[i] = decode_molecularion( ityp_hashvals[i,:] )
            print(newnames)
            cnts = cnts.rename( columns = newnames )
        #get total ion count
        #cnts['Total'] = cnts.sum(axis=1)
        self.cnts = cnts

    def get_composition(self):
        #add total as a column
        #and compute naive composition
        #cnts.append( cnts.divide(cnts['Total']) )
        imgfn = (self.h5src + '.NaiveComposition').replace('/','.')
        imgttl = (self.h5src).replace('/','.').lstrip('.')
        #render the figure 
        self.results['CompositionFigure'] = plot_naive_composition( imgfn, self.cnts, imgttl )
        
    def report_mqhistogram(self, bool_synthetic_data, mqmin, mqicr, mqmax):
        #fn = '../HuanZhaoScrMatCaseStudy/PARAPROBE.Transcoder.Results.SimID.26120.h5'
        if mqmin < 0.0 or mqicr < 0.001 or mqmax <= mqmin:
            raise ValueError('We need to plot a finite mass-to-charge range and at least a binning width of 0.001 Dalton !')

        #for dsrg in self.h5src:
        #if self.ds_rg_n == 1:
        #fn = self.h5src[0][0]
        #fn = 'PARAPROBE.Transcoder.Results.SimID.26120.h5'
        #fn = 'PARAPROBE.Synthetic.Results.SimID.0.h5'
        #fn = 'PARAPROBE.Transcoder.Results.SimID.2765601.h5'
        #fn = dsrg[0]
        fn = self.h5src
        #mqmin = 0.0
        #mqicr = 0.001
        #mqmax = 140.0
        hf = h5py.File( fn, 'r')
        if bool_synthetic_data == True:
            mq = hf[PARAPROBE_SYNTH_VOLRECON_RES_MQ][:,0]
        else:
            mq = hf[PARAPROBE_TRANS_RES_MQ][:,0]
        #build histogram by
        #first, filtering mass-to-charge values on [mqmin, mqmax]
        #second, binning and count summarize bin counts
        binIDs = np.uint64( (mq[np.logical_and(mq >= mqmin, mq <= mqmax)] + 0.5*mqicr ) / mqicr )#- mqmin
        hst1d = np.unique( binIDs, return_counts=True)
        del binIDs
        #y = np.array([nbinsmax,1], )
        #old
        #x = np.float64(hst1d[0][:] * mqicr) #back to Da
        #y = np.float64(hst1d[1][:])
        #new
        nbinsmax = np.uint32(mqmax/mqicr - mqmin/mqicr)
        x = np.linspace( mqmin+mqicr, mqmax+mqicr, nbinsmax, endpoint=True)
        y = 0.5*np.ones([nbinsmax], np.float64 ) #replace 0.0 with low number to be able to compute a log10, half a counts
        for i in np.arange(0,len(hst1d[0])):
            b = hst1d[0][i]
            y[b] = hst1d[1][i]
    
        imgfn = fn + '.MassToCharge'
        imgttl = fn
        
        self.results['MassToChargeFigure'] = plot_naive_mass2charge_histogram( imgfn, x, y, imgttl )

    def get_molecularions_latextable(self, searchtaskID ):
        #basePath = 'D:/Markus/MPIE/GITHUB/MPIE_APTFIM_TOOLBOX/paraprobe/development/paraprobe-ranger/xxxx_AutomaticTexDocumenting'
        #h5fn = basePath + '/PARAPROBE.Ranger.Results.SimID.1.h5'
        fn = self.h5src
        hf = h5py.File( fn, 'r' )
        grpnm = PARAPROBE_RANGER_RES_PEAKFIND + '/' + str(searchtaskID)
        dsnm = grpnm + '/' + PARAPROBE_RANGER_RES_PEAKFIND_WHAT
        adaptive_nrnc = np.shape(hf[dsnm])
        if adaptive_nrnc[0] >= 1 and adaptive_nrnc[1] % 2 == 1 and adaptive_nrnc[1] >= 3:
            what = hf[dsnm][:,0:adaptive_nrnc[1]-1] #extract only proton/neutron pair columns
            charge = hf[dsnm][:,adaptive_nrnc[1]-1:adaptive_nrnc[1]] #extract charge column
        else:
            raise ValueError('Can not parse out molecular architecture and charge from ' + dsnm + ' !')
        dsnm = grpnm + '/' + PARAPROBE_RANGER_RES_PEAKFIND_MQ
        if np.shape(hf[dsnm]) == (adaptive_nrnc[0],1):
            mq = hf[dsnm][:,0:1]
        else:
            raise ValueError('Can not parse out mass-to-charge from ' + dsnm + ' !')
        dsnm = grpnm + '/' + PARAPROBE_RANGER_RES_PEAKFIND_ABUN
        if np.shape(hf[dsnm]) == (adaptive_nrnc[0],1):
            abun = hf[dsnm][:,0:1]
        else:
            raise ValueError('Can not parse out natural abundance score from ' + dsnm + ' !')
            
        tbcaption = ''
        tblabel = ''
        N = np.uint32(np.shape(what)[0])
        M = np.uint32(np.shape(what)[1]/2)
        s = '\n'
        s += r'''\begin{center}''' + '\n'
        s += r'''\begin{table}[h]''' + '\n'
        s += r'''\caption{''' + tbcaption + r'''}''' + '\n'
        s += r'''\centering''' + '\n'
        s += r'''\begin{tabular}{ll} %\addlinespace[0.2em]''' + '\n'
        s += r'''\toprule''' + '\n'
        s += r'''\bf{Molecular ion} & \bf{Mass-to-charge (\SI{}{\dalton}}) & \bf{$\prod_{n=1}^{3} abundance_i} \\ \midrule''' + '\n'
        
        for cand in np.arange(0, N):
            s += stringify_molecular_ion( what[cand,:], charge[cand,0] ) #cand = 1213
            #s += r'''${[''' + stringify( what[cand,0], what[cand,1] ) # + r''' '''
            #for i in range(1,M):
            #    s += stringify( what[cand,2*i+0], what[cand,2*i+1] ) # + r''' '''
            #s += r''']}^{''' + str(int(charge[cand])) + r'''+}$'''
            if cand < N-1:
                s += r''' & ''' + str(mq[cand,0]) + r''' & ''' + str(abun[cand,0]) + r''' \\ ''' + '\n'
            else:
                s += r''' & ''' + str(mq[cand,0]) + r''' & ''' + str(abun[cand,0]) + r''' \\ \bottomrule ''' + '\n'
        s += r'''\end{tabular} %\addlinespace[1em]''' + '\n'
        s += r'''\label{''' + tblabel + r'''}''' + '\n'
        s += r'''\end{table}''' + '\n'
        s += r'''\end{center}''' + '\n'
        return s

    def report(self):
        return self.results

# =============================================================================
#     def decode_molecularion(self, hashval):
#         """
#         low-level function specific for paraprobe-ranger which extracts a human-readable
#         molecular ion string description from an np.uint8 type and shaped (8,) numpy array using periodictable
#         """
#         #first six column values give Z/N pair of first, second, and third isotope component of the molecular ion
#         #seventh column gives sign and eigth the charge of the molecular ion
#         #first one is either 0 indicating UNKNOWN_IONTYPE, i.e. unkwown, or single isotope ion
#         if hashval[0] == 0:
#             print(str('unknown'))
#             return 'unknown'
#         else:
#             #hashval = ityp_hashvals[1,:]
#             #tmp = '['  #use later for r'''$\textnormal{[}{\textnormal{Al}}_2\textnormal{O}]^{3+}$''', unlikely example though...
#             tmp = ''
#             if hashval[1] == 0:
#                 tmp += str(pse.elements[hashval[0]])
#             else:
#                 tmp += '^{'+ str(hashval[1]) + '}' + str(pse.elements[hashval[0]])
#                 #can use latex notation, no fuss any more with formatting :), see the beauty here
#             #tmp += ']'
#             ###MK::add the charges
#             print(str(tmp))
#             return tmp
# 
# =============================================================================

# =============================================================================
#example usage, tell me how many ions of a certain type
#task = autoreporter_ranger( 'PARAPROBE.Transcoder.Results.SimID.7.apth5' )
#show me the composition
#task.cnts
#task.report_natoms()
#get a Latex-formatted table for all possible ion candidates for a specific searchtask, here 0
#task.get_molecularions_latextable( 0 )
# =============================================================================
