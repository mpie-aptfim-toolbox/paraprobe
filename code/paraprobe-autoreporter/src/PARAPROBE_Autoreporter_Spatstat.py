# -*- coding: utf-8 -*-
"""
created 2020/05/28, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for strongly automatized reporting
of results from the paraprobe-ranger tool
"""

import os, sys
import glob, subprocess
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
try:
    import periodictable as pse
except ImportError as e:
    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')
try:
    import h5py
except ImportError as e:
    raise ValueError('Install h5py Python package via e.g. pip install h5py !')
try:
    import warnings
except ImportError as e:
    raise ValueError('Install warnings package !')

from PARAPROBE_Autoreporter_Numerics import *
from PARAPROBE_Autoreporter_CorpDesign import *
from PARAPROBE_Autoreporter_DecodeIontypeHash import *
from PARAPROBE_Autoreporter_Plots import *

from PARAPROBE_Autoreporter_MetadataTranscoder import *
from PARAPROBE_Autoreporter_MetadataSurfacer import *
from PARAPROBE_Autoreporter_MetadataSpatstat import *

#user-defined plots
from PARAPROBE_HuanZhao_Plots import *

def get_paraprobe_molecularion_code( strng ):
    #currently only single element molecular ions support for this type of plot
    z = np.array([ np.uint8(0), np.uint8(0), np.uint8(0) ])
    for el in pse.elements:
        if not strng == el.symbol:
            continue
        else:
            jZ = el.number
            z[0] = np.uint8(jZ)
            break
    return z

class spatstat_task():
    def __init__(self, h5fn, *args, **kwargs):
        """
        read out meta data from a paraprobe-spatstat H5 results file
        h5fn the HDF5 filename
        h5grpnm the HDF5 group which contains the metadata
        """
        self.h5src = h5fn
        self.healthy = True
        self.org_itypes = False
        self.rnd_itypes = False
        self.targets = ''
        self.nbors = ''
        self.kth = 0
        self.hst1d = []
    
    def get_metadata(self, h5grpnm, *args, **kwargs):
        #add self.kth = 1
        #h5fn = 'PARAPROBE.Spatstat.Results.SimID.24909.h5'
        #h5grpnm = PARAPROBE_SPST_RDF_META + '/Task0'
        hf = h5py.File( self.h5src, 'r' )
        thisone = None
        try:
            thisone = hf[h5grpnm]
        except:
            print(h5grpnm + ' does not exist!')
        if not thisone == None:
            thismeta = list(hf[h5grpnm])
            #in paraprobe we by default compute all spatial statistics always for original and randomized labels
            #so we get pairs of spatial statistics per task and expect the same target ions and neighbor ions for each task
            if 'OriginalLabels' in thismeta:
                self.org_itypes = True
            if 'RandomizedLabels' in thismeta:
                self.rnd_itypes = True
            if self.org_itypes == False and self.rnd_itypes == False:
                raise ValueError('Unknown ion labeling convention in ' + h5grpnm + ' !')
            if self.org_itypes == True and self.rnd_itypes == True:
                raise ValueError('Ill-posed ion labeling convention in ' + h5grpnm + ' !')
            #get target single/molecular ion type
            if 'IontypeTargets' in thismeta:
                #the format of these type keys is as follows:
                #first entry is the corresponding ion type key, the remainder 8x1 uint8 nparray is the molecular ion hash
                tg = []
                tg = hf[h5grpnm + '/IontypeTargets'][:,:]
                nrows = np.shape(tg)[0] ###MK::handle multiple ions
                if nrows > 0:
                    for row in np.arange(0,nrows):
                        self.targets += decode_molecularion(tg[row,1:9])
                        if row != nrows-1:
                            self.targets += ',' #';'
                else:
                    warnings.warn('I found unknown iontype targets, while parsing ' + h5grpnm + ', I skip this task but better you check...')
                    self.healthy = False
                    pass
            if 'IontypeNeighbors' in thismeta:
                nb = []
                nb = hf[h5grpnm + '/IontypeNeighbors'][:,:]
                nrows = np.shape(nb)[0]
                if nrows > 0:
                    for row in np.arange(0,nrows):
                        self.nbors += decode_molecularion(nb[row,1:9])
                        if row != nrows-1:
                            self.nbors += ',' #';'
                else:
                    warnings.warn('I found unknown iontype neighbors, while parsing ' + h5grpnm + ', I skip this task but better you check...')
                    self.healthy = False
                    pass
            if 'kthNearestNeighbor' in thismeta:
                kth = np.uint32(hf[h5grpnm + '/kthNearestNeighbor'][0,0])
                if kth > 0:
                    self.kth = kth
                else:
                    warnings.warn(' found unknown kth nearest neighbor, while parsing ' + h5grpnm + ', I skip this task but better you check...')
                    self.healthy = False
                    pass                    

    def get_results(self, h5grpnm, *args, **kwargs):
        hf = h5py.File( self.h5src, 'r' )
        thisone = None
        try:
            thisone = hf[h5grpnm]
        except:
            print(h5grpnm + ' does not exist!')
        if not thisone == None:
            thisres = list(hf[h5grpnm])
            if 'Cnts' in thisres:
                self.hst1d = hf[h5grpnm + '/Cnts'][:,:]

config_spatstat = {}

class autoreporter_spatstat():
    
    def __init__(self, ions, spst , *args, **kwargs):
        #self.simid = simid
        self.toolname = 'Spatstat'
        self.h5src_ions = ions
        self.h5src_spst = spst
        
        #add here all functions which parse by default results from the source
        #self.rdf_img = {}
        #self.knn_img = {}
        #self.sdm_img = {}
        self.config = config_spatstat
        #add here all functions which parse by default results from the source
        self.results = {}
        #add here all functions which parse by default results from the source
        self.get_tool_config()
        self.results['RDF'] = {}
        self.results['KNN'] = {}
        self.results['SDM'] = {}
        self.results['USER'] = {}
        #self.get_rdf()
        #self.get_knn()
        #self.get_sdm()
        
    def get_tool_config(self):
        #fn = 'C:/Users/kaiob/eclipse-workspace/mpie-aptfim-toolbox/paraprobe-autoreporter/HuanZhaoScrMatCaseStudy/PARAPROBE.Transcoder.Results.SimID.26120.h5'
        fn = self.h5src_spst
        hf = h5py.File( fn, 'r' )
        self.config = config_spatstat
        #for key in self.config.keys(): #replace the defaults by the actual values
        #    print(key)
        #    tmp = hf['/SoftwareDetails/Metadata/Keywords/' + key][:]
        #    self.config[key] = [ tmp[0].decode(), tmp[1].decode(), tmp[2].decode() ]
        #above reads what the config_surfacer default dictates, instead
        #to read all entries use
        grpnm = '/SoftwareDetails/Metadata'
        if grpnm in hf.keys(): #old location of metadata, will become deprecated
            grpnm += '/Keywords'
        else: #new location of metadata
            grpnm = PARAPROBE_SPST_META_SFTWR
        print('Parsing configuration from group ' + grpnm)
        keys = list(hf[grpnm])
        self.config = {}
        for key in keys:
            print(key)
            tmp = hf[grpnm + '/' + key][:]
            self.config[key] = [ tmp[0].decode(), tmp[1].decode(), tmp[2].decode() ]
        self.results['Config'] = self.config

    def get_rdf(self, vol, *args, **kwargs):
        """
        read computed radial distributions functions from h5src_spst if any
        """
        fn = self.h5src_spst #fn = 'PARAPROBE.Spatstat.Results.SimID.7.h5'
        hf = h5py.File( fn, 'r' )
        #tell me which groups, sub-groups, and datasets exists in the HDF5 file
        spatstat=list(hf[PARAPROBE_SPST])
        if 'RDF' in spatstat:
            rdf = list(hf[PARAPROBE_SPST_RDF])
            if 'Metadata' in rdf and 'Results' in rdf:
                #get meta data for the tasks and check if corresponding results exists
                tasks = list(hf[PARAPROBE_SPST_RDF_META])
                #check if the tasks contains what we need
                for tsk in tasks:
                    #spatstat_task class object will handle all low-level details of loading the data
                    #for the task and check data integriety, will throw if there are problems
                    d = spatstat_task( fn )
                    d.get_metadata(PARAPROBE_SPST_RDF_META + '/' + tsk)
                    
                    #implement here potential filtering to load and render only specific user output
                    tgnb = kwargs.get('tgnb', None)
                    if not tgnb == None:
                        if not tgnb == []:
                            for key in tgnb:
                                #print(str(key[0]) + ';' + str(key[1]) + '\t\t' + str(d.targets) + ';' + str(d.nbors))                             
                                if key[0] == d.targets and key[1] == d.nbors:
                                    print('Plotting RDF ' + tsk + '...')
                                    d.get_results(PARAPROBE_SPST_RDF_RES + '/' + tsk)
                                    
                                    imgfn = (fn + PARAPROBE_SPST_RDF_RES + '/' + tsk + '.RDF').replace('/','.')
                                    imgttl = (PARAPROBE_SPST_RDF_RES + '/' + tsk).replace('/','.').lstrip('.')
                                    if d.org_itypes == 1:                            
                                        imglegend = 'RDF, original labels, ' + d.targets + ' vs ' + d.nbors
                                    if d.rnd_itypes == 1:
                                        imglegend = 'RDF, randomized labels, ' + d.targets + ' vs ' + d.nbors
                                    
                                    #render the figure 
                                    self.results['RDF'][imgfn] = plot_rdf( imgfn, d.hst1d, vol, imgttl, imglegend )
                                    
                                    #spatstat class object keeps a dictionary for all successfully created files to include them into the latex report
                                    #if os.path.isfile( imgfn ) == True:
                                    #    self.rdf_img[fn] = imgfn

                    else: #normal mode, user did not made restrictions so we plot all results
                        print('Plotting RDF ' + tsk + '...')
                        d.get_results(PARAPROBE_SPST_KNN_RES + '/' + tsk)
                        
                        imgfn = (fn + PARAPROBE_SPST_RDF_RES + '/' + tsk + '.RDF').replace('/','.')
                        imgttl = (PARAPROBE_SPST_RDF_RES + '/' + tsk).replace('/','.').lstrip('.')
                        if d.org_itypes == 1:                            
                            imglegend = 'RDF, original labels, ' + d.targets + ' vs ' + d.nbors
                        if d.rnd_itypes == 1:
                            imglegend = 'RDF, randomized labels, ' + d.targets + ' vs ' + d.nbors
                        
                        #render the figure 
                        self.results['RDF'][imgfn] = plot_rdf( imgfn, d.hst1d, vol, imgttl, imglegend )
                        
                        #spatstat class object keeps a dictionary for all successfully created files to include them into the latex report
                        #if os.path.isfile( imgfn ) == True:
                        #    self.rdf_img[fn] = imgfn
                                        
    def get_knn(self, *args, **kwargs):
        """
        read computed k-th nearest neigbor distribution functions from h5src_spst if any, 
        ###MK::later add automatically search for original and random pairs and plot both into one diagram
        """
        fn = self.h5src_spst #fn = 'PARAPROBE.Spatstat.Results.SimID.7.h5'
        hf = h5py.File( fn, 'r' )
        #tell me which groups, sub-groups, and datasets exists in the HDF5 file
        spatstat=list(hf[PARAPROBE_SPST])
        if 'KNN' in spatstat:
            knn = list(hf[PARAPROBE_SPST_KNN])
            if 'Metadata' in knn and 'Results' in knn:
                #get meta data for the tasks and check if corresponding results exists
                tasks = list(hf[PARAPROBE_SPST_KNN_META])
                #check if the tasks contains what we need
                for tsk in tasks:
                    #spatstat_task class object will handle all low-level details of loading the data
                    #for the task and check data integriety, will throw if there are problems
                    d = spatstat_task( fn )
                    d.get_metadata(PARAPROBE_SPST_KNN_META + '/' + tsk)
                    
                    #implement here potential filtering to load and render only specific user output
                    tgnb = kwargs.get('tgnb', None)
                    if not tgnb == None:
                        if not tgnb == []:
                            for key in tgnb:
                                #print('Key0: ' + str(key[0]) + '; Key1: ' + str(key[1]) + '\t\t Targets: ' + str(d.targets) + '; Nbors ' + str(d.nbors))             
                                if key[0] == d.targets and key[1] == d.nbors:
                                    print('Plotting KNN ' + tsk + '...')
                                    d.get_results(PARAPROBE_SPST_KNN_RES + '/' + tsk)
                                    
                                    imgfn = (fn + PARAPROBE_SPST_KNN_RES + '/' + tsk + '.KNN').replace('/','.')
                                    imgttl = (PARAPROBE_SPST_KNN_RES + '/' + tsk).replace('/','.').lstrip('.')
                                    if d.org_itypes == True:                            
                                        imglegend = str(d.kth) + 'NN, original labels, ' + d.targets + ' vs ' + d.nbors
                                    if d.rnd_itypes == True:
                                        imglegend = str(d.kth) + 'NN, randomized labels, ' + d.targets + ' vs ' + d.nbors
                                    
                                    #render the figure 
                                    self.results['KNN'][imgfn] = plot_knn( imgfn, d.hst1d, imgttl, imglegend )
                                    
                                    #spatstat class object keeps a dictionary for all successfully created files to include them into the latex report
                                    #if os.path.isfile( imgfn ) == True:
                                    #    self.knn_img[fn] = imgfn

                    else: #normal mode, user did not made restrictions so we plot all results
                        print('Plotting KNN ' + tsk + '...')
                        d.get_results(PARAPROBE_SPST_KNN_RES + '/' + tsk)
                        
                        imgfn = (fn + PARAPROBE_SPST_KNN_RES + '/' + tsk + '.KNN').replace('/','.')
                        imgttl = (PARAPROBE_SPST_KNN_RES + '/' + tsk).replace('/','.').lstrip('.')
                        if d.org_itypes == True:                            
                            imglegend = str(d.kth) + 'NN, original labels, ' + d.targets + ' vs ' + d.nbors
                        if d.rnd_itypes == True:
                            imglegend = str(d.kth) + 'NN, randomized labels, ' + d.targets + ' vs ' + d.nbors
                        
                        #render the figure 
                        self.results['KNN'][imgfn] = plot_knn( imgfn, d.hst1d, imgttl, imglegend )
                        
                        #spatstat class object keeps a dictionary for all successfully created files to include them into the latex report
                        #if os.path.isfile( imgfn ) == True:
                        #    self.knn_img['KNN'][fn] = imgfn
            
    def get_sdm(self):
        print('AutoreporterSpatstat get_sdm currently not implemented !')
        pass
    #def get_path_to_figure

    def get_huan_zhao_2018_Cij(self, *args, **kwargs ):
       	"""
    	based on H. Zhao et. al. paper create a joint plot of Ci_j(r) for different datasets
    	a fixed i ion type/speci and multiple j ion typs/specis
    	Ci_j(r) = ni_j(r) / \sum_j ni_j(r)
    	fnarr, numpy array of string defining paraprobe-spatstat H5 files with the spatial statistics results 
    	to compute Ci_j(r) for every j and every file
    	iarr, [uint8, uint8, uint8 ] array telling the element that should be the target for i
        jarr, numpy array of [uint8, uint8, uint8] arrays that should be the element specis for j
    	"""
        #fnarr = np.array( 
        #            ['PARAPROBE.Spatstat.Results.SimID.24909.h5',
        #            'PARAPROBE.Spatstat.Results.SimID.25139.h5',
        #            'PARAPROBE.Spatstat.Results.SimID.25946.h5',
        #            'PARAPROBE.Spatstat.Results.SimID.26110.h5',
        #            'PARAPROBE.Spatstat.Results.SimID.26120.h5',
        #            'PARAPROBE.Spatstat.Results.SimID.26737.h5',
        #            'PARAPROBE.Spatstat.Results.SimID.26865.h5'] )
        iarr = kwargs.get('i', None)
        if iarr == None:
            raise ValueError('Cij analysis requires specification of i !')
        jarr = kwargs.get('j', None)
        if jarr == None:
            raise ValueError('Cij analysis requires specification of j !')
            
        #iarr = np.array( ['Al'] )
        ##create as one figure for each element in jarr, i.e. len(jarr) using sub-plots
        #jarr = np.array( ['Al', 'Mg', 'Zn', 'Cu'] )
        #a dictionary of dictionary of computed C_{i-j} results with metadata
        nij = {}
        nijsum = {}
        Cij = {}
        fnarr = np.array( [self.h5src_spst] )
        for ds in fnarr:
            #fn = fnarr[0]
            fn = ds
            nij[fn] = {} #nested dictionary
            nijsum[fn] = []
            Cij[fn] = {}
            #define i
            #i = 'Al'
            i = iarr[0] #define i
            for j in jarr: #define j
                hf = h5py.File( fn, 'r' )
                #tell me which groups, sub-groups, and datasets exists in the HDF5 file
                spatstat=list(hf[PARAPROBE_SPST])
                if 'RDF' in spatstat:
                    rdf = list(hf[PARAPROBE_SPST_RDF])
                    #print('RDF exists')
                    if 'Metadata' in rdf and 'Results' in rdf:
                        #get meta data for the tasks and check if corresponding results exists
                        tasks = list(hf[PARAPROBE_SPST_RDF_META])
                        #check if the tasks contains what we need
                        for tsk in tasks:
                            #
                            d = spatstat_task( fn )
                            d.get_metadata(PARAPROBE_SPST_RDF_META + '/' + tsk)
                            if d.org_itypes == True and d.targets == i and d.nbors == j:
                                d.get_results(PARAPROBE_SPST_RDF_RES + '/' + tsk)
                                nij[fn][j] = d
                                break
            #all rawdata collected, now summarize cnts nij for particular speci j
            if len(nij[fn].keys()) > 0:
                nrows = np.shape(nij[fn][i].hst1d)[0] #-1 for first and last row, that accumulate lost counts
                ncols = 2
                hst1dsum = np.zeros( [nrows-2, ncols], np.float64 )    
                hst1dsum[:,0] = nij[fn][i].hst1d[1:nrows-1,0]
                for j in jarr:
                    hst1dsum[:,1] += nij[fn][j].hst1d[1:nrows-1,1]

                #compute sought after Cij = nij/nijsum for each j
                for j in jarr:
                    Cij[fn][j] = np.zeros( [nrows-2, ncols], np.float64 )
                    Cij[fn][j][:,0] = hst1dsum[:,0]
                    Cij[fn][j][:,1] = nij[fn][j].hst1d[1:nrows-1,1] / hst1dsum[:,1]
                    
                #all results there generate the plot
            else:
                raise ValueError('jarr results not found!')
            print(ds + ' Cij computed')
        
        #all data collected, plot
        self.results['USER']['HuanZhao2018CijFigure'] = plot_huanzhao_2018_cij( Cij, fnarr, iarr[0], jarr[1:4], '')
        
    def report(self):
        return self.results
        
# =============================================================================
#example usage, tell me how many ions of a certain type
#task = autoreporter_spatstat( 'PARAPROBE.Transcoder.Results.SimID.7.apth5', 'PARAPROBE.Spatstat.Results.SimID.7.h5' )
#evaluates by default all RDFs and renders them into human-readable figures
#task.get_rdf()
#task.get_knn()

#ds = np.array( ['PARAPROBE.Spatstat.Results.SimID.2997401.h5' ])
#iarr = np.array( ['Al'] )
#jarr = np.array( ['Mg', 'Zn', 'Cu'] )
#task = autoreporter_spatstat( '', '' )
#task.get_huan_zhao_2018_Cij( ds, iarr, jarr )

#task.cnts
# task.report_natoms()
# =============================================================================
