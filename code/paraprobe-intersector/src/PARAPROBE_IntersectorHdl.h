//##MK::CODESPLIT


#ifndef __PARAPROBE_INTERSECTOR_HDL_H__
#define __PARAPROBE_INTERSECTOR_HDL_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_IntersectorUtils.h"


class isectHdl
{
	//process-level class which implements the worker instance which performs the indexing
	//using OpenMP and OpenACC i.e. CPU and GPUs
	//specimens result are written to a specific HDF5 files

public:
	isectHdl();
	~isectHdl();
	
	bool init_polyhedron( const unsigned int _id, vector<fctifo> const & f,
			vector<unsigned int> const & v, vector<float> const & p );
	bool read_points_from_h5();
	bool read_polyhedra_from_h5();
	bool read_polyhedra_oris_from_h5();
	bool read_solutions_from_h5();
	bool read_orientations_from_h5();

	bool init_target_file();
	bool write_environment_and_settings();

	bool tetrahedralize_polyhedra();
	bool build_tetraeder_bvh();
	bool visualize_tetrahedra_decomposition( const size_t id, vector<tetrahedra_io_ifo> & obj_ifo );
	bool build_polyhedra_bvh();
	void compute_spheres_polyhedra_intersections();
	void compare_with_tkd();
	void write_solutions_isect_to_h5();
	void write_solutions_tkd_to_h5();

	int get_myrank();
	int get_nranks();
	void set_myrank( const int rr );
	void set_nranks( const int nn );
	void init_mpidatatypes();
	
	h5Hdl inputPointsH5Hdl;
	h5Hdl inputPolyhedraH5Hdl;
	h5Hdl inputOrisH5Hdl;
	h5Hdl inputSolutionsH5Hdl;
	intersector_h5 debugh5Hdl;
	intersector_xdmf debugxdmf;

	vector<roi> points;						//the material points aka region of interest aka ROI
	vector<polyhedron> polyhedra;			//the Voronoi cells
	map<unsigned int, squat> polyhedra_ori;	//the crystal orientation assigned to each polyhedron
	vector<squat> oris;						//test rotations, interpreted first as ##MK::passive orientations
	map<unsigned int, unsigned int> mpid2solidx; //mapping from ROI IDs to array indices on the solutions
	vector<idxres> solutions;				//paraprobe-indexer results harvested at a point
	
	Tree* poly_bvh;
	
	profiler isect_tictoc;

private:
	//MPI related
	int myrank;								//my MPI ID in the MPI_COMM_WORLD
	int nranks;

	//MPI_Datatype MPI_Quaternion_Type;
	//MPI_Datatype MPI_ElevAzimXYZ_Type;
};


#endif

