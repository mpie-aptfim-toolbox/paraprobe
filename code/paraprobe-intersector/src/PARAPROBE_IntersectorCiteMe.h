//##MK::GPLV3


#ifndef __PARAPROBE_ISECT_CITEME_H__
#define __PARAPROBE_ISECT_CITEME_H__

#include "CONFIG_Intersector.h"


class CiteMeIntersector
{
public:
	
	static vector<bibtex> Citations;
	
	static size_t cite();
};

#endif

