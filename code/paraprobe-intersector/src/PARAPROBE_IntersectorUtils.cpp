//##MK::GPLV3

#include "PARAPROBE_IntersectorUtils.h"

/*
bool SortOIMFilterAscValue( oim_filter & first, oim_filter & second )
{
	return first.val < second.val;
}
*/


polyhedron::polyhedron()
{
	tet_bvh = NULL;
	box = aabb3d64();
	id = UINT32MX;
}


polyhedron::~polyhedron()
{
	delete tet_bvh;
	tet_bvh = NULL;
}


bool polyhedron::tete_a_tete()
{
	if ( v_xyz.size() >= static_cast<size_t>(INT32MX) ) {
		cerr << "Number of vertices exceeds INT32MX!" << "\n"; return false;
	}
	if ( fcts.size() >= static_cast<size_t>(INT32MX) ) {
		cerr << "Number of facets exceeds INT32MX!" << "\n"; return false;
	}

	if ( v_xyz.size() < 1 || fcts.size() < 1 ) {
		cerr << "Polyhedron " << id << " has either no vertices or facets!" << "\n"; return false;
	}
	else {
		cout << "Tetrahedralizing polyhedron " << id << " v_xyz.size() " << v_xyz.size() << " fcts.size() " << fcts.size() << "\n";
	}

	//wias-berlin.de/software/tetgen/1.5/doc/manual/manual.pdf
	tetgenio in, out;
	tetgenio::facet *f;
	tetgenio::polygon *p;

	//initialize();

	tetgenbehavior* calls = NULL;
	try {
		calls = new tetgenbehavior;
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation of tetgenbehavior call object failed!" << "\n"; return false;
	}

	string what = ConfigIntersector::TetGenFlags; //"pq", "pY"; //"pYv"; //"pq2.0a10.0v";
//cout << "what " << what << "\n";
	char* cstr = NULL;
	try {
		cstr = new char[what.length() + 1];
	}
	catch(bad_alloc &croak) {
		cerr << "Allocation of tetrahedralization parameter failed!" << "\n"; return false;
	}
	strcpy(cstr, what.c_str());
	bool tetstatus = calls->parse_commandline( cstr ); //cstr //"pYv", "pYv" "pq2.0a10.0v"
	delete [] cstr; cstr = NULL;

	in.firstnumber = 0;	 //facet indices start at 0
	int nvertices_in = static_cast<int>( v_xyz.size() ); //with above sanity check now safe to convert downwards
	int nfaces_in = static_cast<int>( fcts.size() );

	in.numberofpoints = nvertices_in;
	in.pointlist = NULL;
	try {
		in.pointlist = new double[nvertices_in*3];
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation of in.pointlist failed!" << "\n"; return false;
	}
//cout << "Allocated pointlist to tetgen" << "\n";

	//pass vertices into tetgen data structure
	for ( int v = 0; v < nvertices_in; v++ ) {
		in.pointlist[v*3+0] = static_cast<double>(v_xyz[v].x);
		in.pointlist[v*3+1] = static_cast<double>(v_xyz[v].y);
		in.pointlist[v*3+2] = static_cast<double>(v_xyz[v].z);
	}
//cout << "Passed trimesh_v to tetgen" << "\n";

	in.numberoffacets = nfaces_in;
	in.facetlist = NULL;
	try {
		in.facetlist = new tetgenio::facet[in.numberoffacets];
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation of in.facetlist failed!" << "\n"; return false;
	}
//cout << "Allocated facet in tetgen" << "\n";

	in.facetmarkerlist = NULL; //##MK::we have no marks for this PLC

	for ( int ft = 0; ft < nfaces_in; ft++ ) { //add facets
		f = &in.facetlist[ft];
		f->numberofpolygons = 1;
		f->polygonlist = NULL;
		try {
			f->polygonlist = new tetgenio::polygon[f->numberofpolygons];
		}
		catch (bad_alloc &croak) {
			cerr << "Allocation of f.polygonlist failed!" << "\n"; return false;
		}
		f->numberofholes = 0;
		f->holelist = NULL;

		p = &f->polygonlist[0];
		int nverts_polygon = static_cast<int>(fcts.at(ft).second - fcts.at(ft).first); //3;
		p->numberofvertices = nverts_polygon;
		p->vertexlist = NULL;
		try {
			p->vertexlist = new int[p->numberofvertices];
		}
		catch (bad_alloc &croak) {
			cerr << "Allocation for p.vertexlist failed!" << "\n"; return false;
		}
		size_t roffset = fcts.at(ft).first;
		for( int vv = 0; vv < nverts_polygon; vv++, roffset++ ) {
			p->vertexlist[vv] = static_cast<int>( v_ids.at(roffset) );
		}
	}
//cout << "Allocated facet vertices in tetgen" << "\n";

	// Set 'in.facetmarkerlist'
	//in.facetmarkerlist[0] = -1; in.facetmarkerlist[1] = -2; in.facetmarkerlist[2] = 0; in.facetmarkerlist[3] = 0;
	//in.facetmarkerlist[4] = 0; in.facetmarkerlist[5] = 0;

	// Output the PLC to files 'barin.node' and 'barin.poly'.
	//in.save_nodes("barin"); in.save_poly("barin");

	//use tetgen library to perform actual meshing
	try {
		tetrahedralize( calls, &in, &out);
	}
	catch (int tetgencroak) {
		cerr << "TetGen internal error " << tetgencroak << "\n"; return false;
	}
//cout << "Tetrahedralized complex" << "\n";

	//transfer results of the tetgen intermediate data structure
	int nvertices_out = out.numberofpoints;
	int ntetrahedra_out = out.numberoftetrahedra;

	if ( nvertices_out < 1 || ntetrahedra_out < 1 ) {
		cerr << "NumberOfPoints or tetraeder is <1 !" << "\n"; return false;
	}
	try {
		tet_v.reserve( nvertices_out );
		//tet_id.reserve( ntetrahedra_out );
		tet_xyz.reserve( ntetrahedra_out );
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error for tet_v or tet_o !" << "\n"; return false;
	}
//cout << "Number of vertices out " << nvertices_out << "\n";
//cout << "Number of tetraedra out " << ntetrahedra_out << "\n";
	for ( int v = 0; v < nvertices_out; v++ ) {
		tet_v.push_back( p3d64( out.pointlist[v*3+0], out.pointlist[v*3+1], out.pointlist[v*3+2] ) );
//cout << "xyz\t\t" << tet_v.back().x << "\t\t" << tet_v.back().y << "\t\t" << tet_v.back().z << "\n";
	}
//cout << "Transferred " << nvertices_out << "/" << tet_v.size() << " tetrahedron vertices" << endl;

	for ( int cc = 0; cc < ntetrahedra_out; cc++ ) {
		//quad3d qd = quad3d(	out.tetrahedronlist[cc*4+0],
		//				out.tetrahedronlist[cc*4+1],
		//				out.tetrahedronlist[cc*4+2],
		//				out.tetrahedronlist[cc*4+3] );
		//tet_id.push_back( qd );
		int q1 = out.tetrahedronlist[cc*4+0];
		int q2 = out.tetrahedronlist[cc*4+1];
		int q3 = out.tetrahedronlist[cc*4+2];
		int q4 = out.tetrahedronlist[cc*4+3];
		tet_id.push_back( quad3d( 	static_cast<unsigned int>(q1),
									static_cast<unsigned int>(q2),
									static_cast<unsigned int>(q3),
									static_cast<unsigned int>(q4)    ) );
		tet_xyz.push_back( tet3d64( tet_v.at(q1), tet_v.at(q2), tet_v.at(q3), tet_v.at(q4) ) );
		//tet_v3d.push_back( tet3d( tet_v.at(qd.v1), tet_v.at(qd.v2), tet_v.at(qd.v3), tet_v.at(qd.v4) ) );
//cout << "v1234\t\t" << tet_o.back().v1 << "\t\t" << tet_o.back().v2 << "\t\t" << tet_o.back().v3 << "\t\t" << tet_o.back().v4 << "\n";
	}
//cout << "Transferred " << ntetrahedra_out << "/" << tet_o.size() << " tetraeder" << "\n";

	cout << "Tetraeder in polyhedron " << id << " has tet_xyz.size() " << tet_xyz.size() << "\n";
	// Output mesh to files 'barout.node', 'barout.ele' and 'barout.face'.
	//out.save_nodes("barout");	out.save_elements("barout"); out.save_faces("barout");

//	deinitialize();

	delete calls;

//cout << "TetGen completed" << "\n";
	return true;
}


bool polyhedron::tets_in_the_box()
{
	//builds an AABBTree / RTree of tetraeder in $\mathcal{R}^3$
	if ( tet_xyz.size() < 1 ) {
		cerr << "WARNING There are no tetrahedra" << "\n"; return false;
	}
	if ( tet_xyz.size() >= static_cast<size_t>(UINT32MX-1) ) {
		cerr << "Implementation of tetraeder BVH supports currently at most UINT32MX individuals!" << "\n";
		return false;
	}

	unsigned int ntets = static_cast<unsigned int>(tet_xyz.size());
	tet_bvh = NULL;
	try {
		tet_bvh = new class Tree( ntets );
	}
	catch (bad_alloc &croak) {
		cerr << "Unable to allocate memory for the tree of the tetrahedra BVH for polyhedron " << id << "\n";
		return false;
	}

	aabb3d64 tmp = aabb3d64();
	double eps = static_cast<double>(AABBINCLUSION_EPSILON);
	unsigned int triid = 0;
	for( auto it = tet_xyz.begin(); it != tet_xyz.end(); it++, triid++ ) {
		//p3d p1 = v_xyz.at(it->v1);
		//p3d p2 = v_xyz.at(it->v2);
		//p3d p3 = v_xyz.at(it->v3);
		//p3d p4 = v_xyz.at(it->v4);

		double txmin = min(min(min(it->v1.x, it->v2.x), it->v3.x), it->v4.x);
		double tymin = min(min(min(it->v1.y, it->v2.y), it->v3.y), it->v4.y);
		double tzmin = min(min(min(it->v1.z, it->v2.z), it->v3.z), it->v4.z);

		double txmax = max(max(max(it->v1.x, it->v2.x), it->v3.x), it->v4.x);
		double tymax = max(max(max(it->v1.y, it->v2.y), it->v3.y), it->v4.y);
		double tzmax = max(max(max(it->v1.z, it->v2.z), it->v3.z), it->v4.z);
		//##MK::-+ eps assures that down casting from float to double keeps bounding boxes slightly larger than actual tetraeder

		double dxmin = txmin - eps;
		double dymin = tymin - eps;
		double dzmin = tzmin - eps;
		double dxmax = txmax - eps;
		double dymax = tymax - eps;
		double dzmax = tzmax - eps;

		//add tetraeder in the polyhedron local AABB with guard zone
		tet_bvh->insertParticle( triid, trpl(	static_cast<float>(dxmin),
												static_cast<float>(dymin),
												static_cast<float>(dzmin)  ),
										trpl(	static_cast<float>(dxmax),
												static_cast<float>(dymax),
												static_cast<float>(dzmax)  )     );

		//use bounding tmp values to find bounding tmp to polyhedron
		tmp.xmi = min( txmin, tmp.xmi );
		tmp.ymi = min( tymin, tmp.ymi );
		tmp.zmi = min( tzmin, tmp.zmi );

		tmp.xmx = max( txmax, tmp.xmx );
		tmp.ymx = max( tymax, tmp.ymx );
		tmp.zmx = max( tzmax, tmp.zmx );
	}

	//set polyhedron bounding box with guard zone
	tmp.xmi -= eps;
	tmp.ymi -= eps;
	tmp.zmi -= eps;
	tmp.xmx += eps;
	tmp.ymx += eps;
	tmp.zmx += eps;
	box = tmp;

	cout << "Polyhedron " << id << " tet_bvh->getNodeCount() " << tet_bvh->getNodeCount() << " triid " << triid << "\n";

	return true;
}


idxres::idxres()
{
	dsnm = "";
	TotalVolumeROI = 0.0;
	FourStrongestOverlap[0] = static_cast<double>(UINT32MX);
	FourStrongestOverlap[1] = 0.0;
	FourStrongestOverlap[2] = static_cast<double>(UINT32MX);
	FourStrongestOverlap[3] = 0.0;
	FourStrongestOverlap[4] = static_cast<double>(UINT32MX);
	FourStrongestOverlap[5] = 0.0;
	FourStrongestOverlap[6] = static_cast<double>(UINT32MX);
	FourStrongestOverlap[7] = 0.0;
}


/*
idxres::idxres( const string _dsnm, const size_t _nsol )
{
	dsnm = _dsnm;
	dat = vector<unsigned int>( _nsol, UINT32MX );	//##MK::flag as invalid
}
*/


idxres::~idxres()
{
	dsnm = "";
}
