//##MK::GPLV3

#include "PARAPROBE_IntersectorXDMF.h"


intersector_xdmf::intersector_xdmf()
{
	
}

	
intersector_xdmf::~intersector_xdmf()
{
	
}


int intersector_xdmf::create_tetrahedra_files( const string prefix, const string h5ref, vector<tetrahedra_io_ifo> const & ifo )
{
	string fwslash = "/";

	for( auto it = ifo.begin(); it != ifo.end(); it++ ) {
		string xmlfn = prefix + ".Polyhedron" + to_string(it->Plyid) + ".xdmf";

		xdmfout.open( xmlfn.c_str() );
		if ( xdmfout.is_open() == true ) {
			xdmfout << XDMF_HEADER_LINE1 << "\n";
			xdmfout << XDMF_HEADER_LINE2 << "\n";
			xdmfout << XDMF_HEADER_LINE3 << "\n";
			string grpnm = PARAPROBE_ISECT_META_TETS + fwslash + to_string(it->Plyid);
			xdmfout << "  <Domain>" << "\n";
			xdmfout << "    <Grid Name=\"polyhedron" << it->Plyid << "\" GridType=\"Uniform\">" << "\n";
			//xdmfout << "      <Topology TopologyType=\"Mixed\" NumberOfElements=\"" << it->TopoNumberOfElements << "\">" << "\n";
			xdmfout << "      <Topology TopologyType=\"Tetrahedron\" NumberOfElements=\"" << it->TopoNumberOfElements << "\">" << "\n";
			xdmfout << "        <DataItem Dimensions=\"" << it->TopoNumberOfElements << " 4\" NumberType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
			xdmfout << "          " << h5ref << ":" << grpnm + fwslash + PARAPROBE_ISECT_META_TETS_TOPO  << "\n";
			xdmfout << "        </DataItem>" << "\n";
			xdmfout << "      </Topology>" << "\n";

			xdmfout << "      <Geometry GeometryType=\"XYZ\">" << "\n";
			xdmfout << "        <DataItem Dimensions=\"" << it->GeoDimensions << " 3\" NumberType=\"Float\" Precision=\"8\" Format=\"HDF\">" << "\n";
			xdmfout << "          " << h5ref << ":" << grpnm + fwslash + PARAPROBE_ISECT_META_TETS_XYZ << "\n";
			xdmfout << "        </DataItem>" << "\n";
			xdmfout << "      </Geometry>" << "\n";

			xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"TetraederID\">" << "\n";
			xdmfout << "        <DataItem Dimensions=\"" << it->TetidDimensions << " 1\" DataType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
			xdmfout << "           " << h5ref << ":" << grpnm + fwslash + PARAPROBE_ISECT_META_TETS_TIDS << "\n";
			xdmfout << "        </DataItem>" << "\n";
			xdmfout << "      </Attribute>" << "\n";

			xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"PolyhedronID\">" << "\n";
			xdmfout << "        <DataItem Dimensions=\"" << it->PlyidDimensions << " 1\" DataType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
			xdmfout << "           " << h5ref << ":" << grpnm + fwslash + PARAPROBE_ISECT_META_TETS_PIDS << "\n";
			xdmfout << "        </DataItem>" << "\n";
			xdmfout << "      </Attribute>" << "\n";
			xdmfout << "    </Grid>" << "\n";
			xdmfout << "  </Domain>" << "\n";
			xdmfout << "</Xdmf>" << "\n";

			xdmfout.flush();
			xdmfout.close();
			//return WRAPPED_XDMF_SUCCESS;
		}
		else {
			return WRAPPED_XDMF_IOFAILED;
		}
	}
	return WRAPPED_XDMF_SUCCESS;
}


