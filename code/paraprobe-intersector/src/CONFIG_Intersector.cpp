//##MK::GPLV3

#include "CONFIG_Intersector.h"

E_ANALYSIS_MODE ConfigIntersector::AnalysisMode = SPHERES_POLYHEDRA_INTERSECTIONS;
string ConfigIntersector::InputfilePoints = "";
string ConfigIntersector::DatasetPoints = "";
string ConfigIntersector::InputfilePolyhedra = "";
string ConfigIntersector::DatasetPolyhedra = "";
string ConfigIntersector::InputfilePolyhedraOris = "";
string ConfigIntersector::DatasetPolyhedraOris = "";

string ConfigIntersector::InputfileSolutions = "";
string ConfigIntersector::DatasetSolutions = "";
string ConfigIntersector::InputfileOris = "";
string ConfigIntersector::DatasetOris = "";

unsigned int ConfigIntersector::DatasetSolutionsMin = 0;
unsigned int ConfigIntersector::DatasetSolutionsIncr = 1;
unsigned int ConfigIntersector::DatasetSolutionsMax = 0;

bool ConfigIntersector::IOVisualizeTetrahedra = false;

string ConfigIntersector::TetGenFlags = "pY"; //do not verbose "";

bool ConfigIntersector::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.Input.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigIntersect")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;

	//WHAT TYPE OF ANALYSIS TO DO
	mode = read_xml_attribute_uint32( rootNode, "AnalysisMode" );
	switch(mode)
	{
		case SPHERES_POLYHEDRA_INTERSECTIONS:
			AnalysisMode = SPHERES_POLYHEDRA_INTERSECTIONS;
			break;
		case COMPARE_WITH_TKD_RESULTS:
			AnalysisMode = COMPARE_WITH_TKD_RESULTS;
			break;
		default:
			AnalysisMode = SPHERES_POLYHEDRA_INTERSECTIONS;
	}

	if ( AnalysisMode == SPHERES_POLYHEDRA_INTERSECTIONS ) {
		InputfilePoints = read_xml_attribute_string( rootNode, "InputfilePoints" );
		DatasetPoints = read_xml_attribute_string( rootNode, "DatasetPoints" );
		InputfilePolyhedra = read_xml_attribute_string( rootNode, "InputfilePolyhedra" );
		DatasetPolyhedra = read_xml_attribute_string( rootNode, "DatasetPolyhedra" );
		InputfilePolyhedraOris = read_xml_attribute_string( rootNode, "InputfilePolyhedraOris" );
		DatasetPolyhedraOris = read_xml_attribute_string( rootNode, "DatasetPolyhedraOris" );

		InputfileSolutions = read_xml_attribute_string( rootNode, "InputfileSolutions" );
		DatasetSolutions = read_xml_attribute_string( rootNode, "DatasetSolutions" );
		InputfileOris = read_xml_attribute_string( rootNode, "InputfileOris" );
		DatasetOris = read_xml_attribute_string( rootNode, "DatasetOris" );

		DatasetSolutionsMin = read_xml_attribute_uint32( rootNode, "DatasetSolutionsMin" );
		DatasetSolutionsIncr = 1;
		DatasetSolutionsMax = read_xml_attribute_uint32( rootNode, "DatasetSolutionsMax" );

		IOVisualizeTetrahedra = read_xml_attribute_bool( rootNode, "IOVisualizeTetrahedra" );
		TetGenFlags = read_xml_attribute_string( rootNode, "TetGenFlags" ); //##MK::be careful here
	}
	
	if ( AnalysisMode == COMPARE_WITH_TKD_RESULTS ) {
		InputfilePoints = read_xml_attribute_string( rootNode, "InputfilePoints" );
		DatasetPoints = read_xml_attribute_string( rootNode, "DatasetPoints" );
		InputfileSolutions = read_xml_attribute_string( rootNode, "InputfileSolutions" );
		DatasetSolutions = read_xml_attribute_string( rootNode, "DatasetSolutions" );
		InputfileOris = read_xml_attribute_string( rootNode, "InputfileOris" );
		DatasetOris = read_xml_attribute_string( rootNode, "DatasetOris" );

		DatasetSolutionsMin = read_xml_attribute_uint32( rootNode, "DatasetSolutionsMin" );
		DatasetSolutionsIncr = 1;
		DatasetSolutionsMax = read_xml_attribute_uint32( rootNode, "DatasetSolutionsMax" );
	}
	
	return true;
}


bool ConfigIntersector::checkUserInput()
{
	cout << "ConfigIntersector::" << "\n";
	if ( AnalysisMode == SPHERES_POLYHEDRA_INTERSECTIONS ) {
		cout << "We evaluate analytically the intersection volume between spheres and polyhedra!" << "\n";
		cout << "InputfilePoints " << InputfilePoints << "\n";
		cout << "DatasetPoints " << DatasetPoints << "\n";
		cout << "InputfilePolyhedra " << InputfilePolyhedra << "\n";
		cout << "DatasetPolyhedra " << DatasetPolyhedra << "\n";
		cout << "InputfilePolyhedraOris " << InputfilePolyhedraOris << "\n";
		cout << "DatasetPolyhedraOris " << DatasetPolyhedraOris << "\n";

		cout << "InputfileSolutions " << InputfileSolutions << "\n";
		cout << "DatasetSolutions " << DatasetSolutions << "\n";
		cout << "InputfileOris " << InputfileOris << "\n";
		cout << "DatasetOris " << DatasetOris << "\n";

		cout << "DatasetSolutionsMin " << DatasetSolutionsMin << "\n";
		cout << "DatasetSolutionsIncr " << DatasetSolutionsIncr << "\n";
		cout << "DatasetSolutionsMax " << DatasetSolutionsMax << "\n";

		cout << "IOVisualizeTetrahedra " << IOVisualizeTetrahedra << "\n";
		cout << "TetGenFlags " << TetGenFlags << "\n";
	}
	if ( AnalysisMode == COMPARE_WITH_TKD_RESULTS ) {
		cout << "DEBUG MODE::We evaluate the indexed candidate orientations against a set of !! CURRENTLY HARDCODED TEST ORIENTATIONS !!" << "\n";
		cerr << "DEBUG MODE::We evaluate the indexed candidate orientations against a set of !! CURRENTLY HARDCODED TEST ORIENTATIONS !!" << "\n";
		cout << "InputfilePoints " << InputfilePoints << "\n";
		cout << "DatasetPoints " << DatasetPoints << "\n";

		cout << "InputfileSolutions " << InputfileSolutions << "\n";
		cout << "DatasetSolutions " << DatasetSolutions << "\n";
		cout << "InputfileOris " << InputfileOris << "\n";
		cout << "DatasetOris " << DatasetOris << "\n";

		cout << "DatasetSolutionsMin " << DatasetSolutionsMin << "\n";
		cout << "DatasetSolutionsIncr " << DatasetSolutionsIncr << "\n";
		cout << "DatasetSolutionsMax " << DatasetSolutionsMax << "\n";
	}
	
	cout << "\n";
	return true;
}


void ConfigIntersector::reportSettings( vector<pparm> & res )
{
	res.push_back( pparm( "AnalysisMode", sizet2str(AnalysisMode), "", "" ) );

	res.push_back( pparm( "InputfilePoints", InputfilePoints, "", "" ) );
	res.push_back( pparm( "DatasetPoints",  DatasetPoints, "", "" ) );
	res.push_back( pparm( "InputfileSolutions", InputfileSolutions, "", "" ) );
	res.push_back( pparm( "DatasetSolutions", DatasetSolutions, "", "" ) );
	res.push_back( pparm( "InputfileOris", InputfileOris, "", "" ) );
	res.push_back( pparm( "DatasetOris", DatasetOris, "", "" ) );

	res.push_back( pparm( "DatasetSolutionsMin", uint2str(DatasetSolutionsMin), "", "" ) );
	res.push_back( pparm( "DatasetSolutionsIncr", uint2str(DatasetSolutionsIncr), "", "" ) );
	res.push_back( pparm( "DatasetSolutionsMax", uint2str(DatasetSolutionsMax), "", "" ) );

	if ( AnalysisMode == SPHERES_POLYHEDRA_INTERSECTIONS ) {
		res.push_back( pparm( "InputfilePolyhedra", InputfilePolyhedra, "", "" ) );
		res.push_back( pparm( "DatasetPolyhedra", DatasetPolyhedra, "", "" ) );
		res.push_back( pparm( "InputfilePolyhedraOris", InputfilePolyhedraOris, "", "" ) );
		res.push_back( pparm( "DatasetPolyhedraOris", DatasetPolyhedraOris, "", "" ) );

		res.push_back( pparm( "IOVisualizeTetrahedra", sizet2str(static_cast<size_t>(IOVisualizeTetrahedra)), "", "" ) );
		res.push_back( pparm( "TetGenFlags", TetGenFlags, "", "" ) );
	}
}
