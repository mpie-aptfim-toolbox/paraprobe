//##MK::GPLV3

#ifndef __PARAPROBE_INTERSECTOR_XDMF_H__
#define __PARAPROBE_INTERSECTOR_XDMF_H__

#include "PARAPROBE_IntersectorHDF5.h"

struct tetrahedra_io_ifo
{
	/*string Plyid;
	string TopoNumberOfElements;
	string TopoDimensions;
	string GeoDimensions;
	string TetidDimensions;
	string PlyidDimensions;*/
	size_t Plyid;
	size_t TopoNumberOfElements;
	//size_t TopoDimensions;
	size_t GeoDimensions;
	size_t TetidDimensions;
	size_t PlyidDimensions;
	/*tetrahedra_io_ifo() : Plyid(""), TopoNumberOfElements(""), TopoDimensions(""), GeoDimensions(""),
			TetidDimensions(""), PlyidDimensions("") {}
	tetrahedra_io_ifo( const string _ply, const string _topo, const string _tdim, const string _gdim,
			const string _tetid, const string _plyid ) : Plyid(_ply), TopoNumberOfElements(_topo), TopoDimensions(_tdim),
					GeoDimensions(_gdim), TetidDimensions(_tetid), PlyidDimensions(_plyid) {}*/
	tetrahedra_io_ifo() : Plyid(0), TopoNumberOfElements(0), GeoDimensions(0),
			TetidDimensions(0), PlyidDimensions(0) {} //TopoDimensions(0),
	tetrahedra_io_ifo( const size_t _ply, const size_t _topo, const size_t _gdim,
			const size_t _tetid, const size_t _plyid ) : Plyid(_ply), TopoNumberOfElements(_topo), GeoDimensions(_gdim),
					TetidDimensions(_tetid), PlyidDimensions(_plyid) {} //const size_t _tdim,TopoDimensions(_tdim),
};


class intersector_xdmf : public xdmfHdl
{
	//tool-specific sub-class of a XDMF inheriting all methods of the base class h5Hdl
	//coordinating instance handling all (sequential) writing to HDF5 file wrapping HDF5 low-level C library calls

public:
	intersector_xdmf();
	~intersector_xdmf();

	int create_tetrahedra_files( const string prefix, const string h5ref, vector<tetrahedra_io_ifo> const & ifo );

//private:
};

#endif
