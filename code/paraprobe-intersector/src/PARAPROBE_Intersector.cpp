//##MK::GPLV3

#include "PARAPROBE_IntersectorHdl.h"


//define what is passed upon execution
#define SIMID									1
#define CONTROLFILE								2

//##MK::for querying system maximum physical memory see
//https://stackoverflow.com/questions/2513505/how-to-get-available-memory-c-g

#ifndef GITSHA									//otherwise we could not compile without adding -DGITSHA="d123" a dummy GitSha
	#define GITSHA								0
#endif
//stringification of strings passed from cmake at compile time
//https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html
#define xstr(s)									str(s)
#define str(s) 									#s


void hello( void )
{
	cout << "paraprobe-indexer" << "\n";
	cout << xstr(GITSHA) << "\n";
	cout << "Collecting " << CiteMeIntersector::cite() << " publications for the tool to cite:" << "\n";
	for( auto it = CiteMeIntersector::Citations.begin(); it != CiteMeIntersector::Citations.end(); it++ ) {
		cout << it->author << "\n";
		cout << it->title << "\n";
		cout << it->journal << ", " << it->year << ", " << it->vol << ", pp" << it->pages << "\n";
		cout << it->doi << "\n";
	}
}


bool init(  int pargc, char** pargv )
{
	ConfigShared::SimID = stoul( pargv[SIMID] );
	try {
		ConfigIntersector::readXML(pargv[CONTROLFILE]);
	}
	catch (exception& e) {
		cerr << "\n" << "Unable to parse control file! Details:" << "\n" << e.what() << "\n"; return false;
	}
	if ( ConfigIntersector::checkUserInput() == false ) {
		cerr << "\n" << "Control file settings failed the validity check!" << "\n"; return false;
	}
	else {
		cout << "\n" << "\t\tInput is valid under SimulationID = " << "SimID." <<  ConfigShared::SimID << "\n";
	}
	cout << endl;
	return true;
}


void spheres_polyhedra_analytical_intersection_volumina( const int r, const int nr, char** pargv )
{
	//allocate process-level instance of a indexerHdl. The instance handles all process-internal processing
	//the instances synchronize across and communicate with each other during execution
	isectHdl* cut = NULL;

	int localhealth = 1;
	int globalhealth = nr;
	try {
		cut = new isectHdl;
		cut->set_myrank(r);
		cut->set_nranks(nr);
		cut->init_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Rank " << r << " unable to allocate a sequential isectHdl instance" << "\n"; localhealth = 0;
	}

	if ( nr > 1 ) {
		if ( r == MASTER ) {
			cerr << "paraprobe-intersector is multi-threaded only currently!" << "\n"; localhealth = 0;
		}
	}
	//##MK::see paraprobe-indexer and other tools how to make MPI parallel
	//currently we use multi-threading only

	//do we have all processes on board?
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete cut; cut = NULL; return;
	}
	
	if ( ConfigIntersector::AnalysisMode == SPHERES_POLYHEDRA_INTERSECTIONS ) {

		if ( cut->read_points_from_h5() == true ) {
			cout << "MASTER read successfully the positions and radii of all spheres" << "\n";
		}
		else {
			cerr << "MASTER was unable to read points!" << "\n";
			delete cut; cut = NULL; return;
		}

		if ( cut->read_polyhedra_from_h5() == true ) {
			cout << "MASTER read successfully the geometry of all polyhedra" << "\n";
		}
		else {
			cerr << "MASTER was unable to read geometry of all polyhedra!" << "\n";
			delete cut; cut = NULL; return;
		}

		if ( cut->read_polyhedra_oris_from_h5() == true ) {
			cout << "MASTER read successfully the orientations synthesized for all polyhedra" << "\n";
		}
		else {
			cerr << "MASTER was unable to read orientations of all polyhedra!" << "\n";
			delete cut; cut = NULL; return;
		}

		if ( cut->read_orientations_from_h5() == true ) {
			cout << "MASTER read successfully the orientation set to index with" << "\n";
		}
		else {
			cerr << "MASTER was unable to read orientation set!" << "\n";
			delete cut; cut = NULL; return;
		}

		//##MK::in most realistic cases the expected size of the solutions matrices contains an uint32 ori ID + float solution quality = 8 B / (solution*ROI)
		//solutions beyond a few dozens are rotated versions or false solutions anyway so at most 1k solutions to keep per ROI
		//3D ebsd grids with 10 million ROIs spanning a 2 billion ion APT dataset currently likely way too large so
		//10^7 * 1000 * 8 B so approx 80 GB to load
		//##MK::assume fits in main memory, read all at once

		if ( cut->read_solutions_from_h5() == true ) {
			cout << "MASTER read successfully the geometry of all solutions" << "\n";
		}
		else {
			cerr << "MASTER was unable to read solutions!" << "\n";
			delete cut; cut = NULL; return;
		}

		if ( r == MASTER ) {
			if ( cut->init_target_file() == true ) {
				cout << "Rank " << MASTER << " initialized the results file" << "\n";
			}
			else {
				cerr << "Rank " << MASTER << " failed to initialize the results file!" << "\n";
				delete cut; cut = NULL; return;
			}
			if ( cut->write_environment_and_settings() == true ) {
				cout << "Rank " << MASTER << " environment and settings written!" << "\n";
			}
			else {
				cerr << "Rank " << MASTER << " writing environment and settings failed!" << "\n"; return;
			}
		}
	
		if ( cut->tetrahedralize_polyhedra() == true ) {
			cout << "MASTER tetrahedralized all polyhedra" << "\n";
		}
		else {
			cerr << "MASTER was unable to tetrahedralize polyhedra!" << "\n";
			delete cut; cut = NULL; return;
		}

		if ( cut->build_tetraeder_bvh() == true ) {
			cout << "MASTER built BVH for polyhedra tetraeder" << "\n";
		}
		else {
			cerr << "MASTER was unable to build BVH for polyhedra tetraeder!" << "\n";
			delete cut; cut = NULL; return;
		}

		if ( cut->build_polyhedra_bvh() == true ) {
			cout << "MASTER built BVH for polyhedra" << "\n";
		}
		else {
			cerr << "MASTER was unable to build BVH for polyhedra!" << "\n";
			delete cut; cut = NULL; return;
		}

		cut->compute_spheres_polyhedra_intersections();

		//single I/O results write out
		for( int rk = MASTER; rk < cut->get_nranks(); rk++ ) {
			if ( r == rk ) {
				cut->write_solutions_isect_to_h5();
			}
			MPI_Barrier( MPI_COMM_WORLD );
		}
	}

	cut->isect_tictoc.spit_profiling( "Intersector", ConfigShared::SimID, cut->get_myrank() );

	//release resources
	delete cut; cut = NULL;
}


void compare_with_tkd_results( const int r, const int nr, char** pargv )
{
	//allocate process-level instance of a indexerHdl. The instance handles all process-internal processing
	//the instances synchronize across and communicate with each other during execution
	isectHdl* cut = NULL;

	int localhealth = 1;
	int globalhealth = nr;
	try {
		cut = new isectHdl;
		cut->set_myrank(r);
		cut->set_nranks(nr);
		cut->init_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Rank " << r << " unable to allocate a sequential isectHdl instance" << "\n"; localhealth = 0;
	}

	if ( nr > 1 ) {
		if ( r == MASTER ) {
			cerr << "paraprobe-intersector is multi-threaded only currently!" << "\n"; localhealth = 0;
		}
	}
	//##MK::see paraprobe-indexer and other tools how to make MPI parallel
	//currently we use multi-threading only

	//do we have all processes on board?
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete cut; cut = NULL; return;
	}

	if ( cut->read_points_from_h5() == true ) {
		cout << "MASTER read successfully the positions and radii of all spheres" << "\n";
	}
	else {
		cerr << "MASTER was unable to read points!" << "\n";
		delete cut; cut = NULL; return;
	}

	if ( cut->read_orientations_from_h5() == true ) {
		cout << "MASTER read successfully the orientation set to index with" << "\n";
	}
	else {
		cerr << "MASTER was unable to read orientation set!" << "\n";
		delete cut; cut = NULL; return;
	}

	//##MK::in most realistic cases the expected size of the solutions matrices contains an uint32 ori ID + float solution quality = 8 B / (solution*ROI)
	//solutions beyond a few dozens are rotated versions or false solutions anyway so at most 1k solutions to keep per ROI
	//3D ebsd grids with 10 million ROIs spanning a 2 billion ion APT dataset currently likely way too large so
	//10^7 * 1000 * 8 B so approx 80 GB to load
	//##MK::assume fits in main memory, read all at once

	if ( cut->read_solutions_from_h5() == true ) {
		cout << "MASTER read successfully the geometry of all solutions" << "\n";
	}
	else {
		cerr << "MASTER was unable to read solutions!" << "\n";
		delete cut; cut = NULL; return;
	}

	if ( r == MASTER ) {
		if ( cut->init_target_file() == true ) {
			cout << "Rank " << MASTER << " initialized the results file" << "\n";
		}
		else {
			cerr << "Rank " << MASTER << " failed to initialize the results file!" << "\n";
			delete cut; cut = NULL; return;
		}
		if ( cut->write_environment_and_settings() == true ) {
			cout << "Rank " << MASTER << " environment and settings written!" << "\n";
		}
		else {
			cerr << "Rank " << MASTER << " writing environment and settings failed!" << "\n"; return;
		}
	}

	cut->compare_with_tkd();

	//single I/O results write out
	for( int rk = MASTER; rk < cut->get_nranks(); rk++ ) {
		if ( r == rk ) {
			cut->write_solutions_tkd_to_h5();
		}
		MPI_Barrier( MPI_COMM_WORLD );
	}

	cut->isect_tictoc.spit_profiling( "Intersector", ConfigShared::SimID, cut->get_myrank() );

	//release resources
	delete cut; cut = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
	hello();
//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	//helloworld( argc, argv );

	if ( init( argc, argv ) == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks!" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else { 
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}
	cout << r << "-th MPI process initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";
	
//for this MPI/OMP/OpenACC, i.e. process/thread/accelerator heterogeneous program not every combination of mpiexec -n, OMP_NUM_THREADS and GPUs is admissible
	//##MK::if we dont want to use GPUs at all possible
	//##MK::current implementation initiates one MPI process per GPU on the node
	//##MK::problem is that the number of MPI processes per computing node is typically decided

//EXECUTE SPECIFIC TASK
	if ( ConfigIntersector::AnalysisMode == SPHERES_POLYHEDRA_INTERSECTIONS ) {

		spheres_polyhedra_analytical_intersection_volumina( r, nr, argv );

	}
	if ( ConfigIntersector::AnalysisMode == COMPARE_WITH_TKD_RESULTS ) {

		compare_with_tkd_results( r, nr, argv );

	}
	
//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();

	double toc = omp_get_wtime();
	cout << "paraprobe-intersector took " << (toc-tic) << " seconds wall-clock time in total" << endl;
	return 0;
}
