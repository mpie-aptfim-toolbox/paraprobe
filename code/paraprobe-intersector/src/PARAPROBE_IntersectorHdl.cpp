//##MK::GPLV3

#include "PARAPROBE_IntersectorHdl.h"


isectHdl::isectHdl()
{
	myrank = MASTER;
	nranks = 1;
	poly_bvh = NULL;
}


isectHdl::~isectHdl()
{
	/*for( size_t i = 0; i < solutions.size(); i++ ) {
		if ( solutions.at(i) != NULL ) {
			delete solutions[i];
			solutions[i] = NULL;
		}
	}*/

	delete poly_bvh;
	poly_bvh = NULL;
}


bool isectHdl::init_polyhedron( const unsigned int _id, vector<fctifo> const & f,
			vector<unsigned int> const & v, vector<float> const & p )
{
	//##MK::instantiate a polyhedron translate global vertex ID to polyhedron-local vertex IDs to make local processing better cache contained
	polyhedra.push_back( polyhedron() );
	polyhedra.back().id = _id;
	vector<pair<size_t,size_t>> & fifo = polyhedra.back().fcts;
	vector<int> & vids = polyhedra.back().v_ids; //implicitly defined geometry of each facet
	vector<p3d> & vxyz = polyhedra.back().v_xyz; //unique local vertices to build all vertices of the polyhedron, referred to by v_ids

	map<unsigned int, unsigned int> old2new; //mapping an old (global) vertex ID to a new (local) one
	size_t ii = 0;
	pair<size_t, size_t> tmp = pair<size_t, size_t>( 0, 0 ); //start end of vertices to read on contiguous arrays v_ids, v_xyz to parse the vertices of a particular facet polygon
	size_t vi = 0; //read linearly from v and f
	for( auto ft = f.begin(); ft != f.end(); ft++ ) {
		//init facets in winding order
		tmp = pair<size_t,size_t>( vi, vi );
		for( size_t j = 0; j < ft->end; j++, vi++ ) {
			unsigned int oldid = v.at(vi);
			map<unsigned int, unsigned int>::iterator cand = old2new.find( oldid );
			if ( cand != old2new.end() ) {
				//oldid was previously already relabelled so use this new id
				vids.push_back( cand->second );
			}
			else {
				if ( vxyz.size() < UINT32MX ) {
					unsigned int newid = static_cast<unsigned int>(vxyz.size());
					vxyz.push_back( p3d( p.at(3*oldid+0), p.at(3*oldid+1), p.at(3*oldid+2)) );
					old2new.insert( pair<unsigned int, unsigned int>( oldid, newid ) );
					vids.push_back( newid );
				}
				else {
					cerr << "Attempting to add more than UINT32MX-1 points for polyhedron id " << _id << "\n";
					return false;
				}
			}
		} //done with this polyhedron
		tmp.second = tmp.second + ft->end;
		fifo.push_back( pair<size_t,size_t>( tmp.first, tmp.second ) );
	}

	//##MK::BEGIN DEBUG
	cout << "Polyhedron " << polyhedra.back().id << "\n";
	cout << "fifo.size() " << polyhedra.back().fcts.size() << "\n";
	for( auto it = polyhedra.back().fcts.begin(); it != polyhedra.back().fcts.end(); it++ ) {
		cout << it->first << ";" << it->second << "\n";
	}
	cout << "v_ids.size() " << polyhedra.back().v_ids.size() << "\n";
	for( auto it = polyhedra.back().v_ids.begin(); it != polyhedra.back().v_ids.end(); it++ ) {
		cout << *it << "\n";
	}
	cout << "v_xyz.size() " << polyhedra.back().v_xyz.size() << "\n";
	for( auto it = polyhedra.back().v_xyz.begin(); it != polyhedra.back().v_xyz.end(); it++ ) {
		cout << it->x << ";" << it->y << ";" << it->z << "\n";
	}
	cout << "\n";
	//##MK::END DEBUG

	//##MK::BEGIN DEBUG
	//construct outer unit normals to the polyhedron faces
	//##MK::works here only because we handle Poisson-Voronoi cells which are always convex polyhedra and not degenerated
	p3d center = p3d();
	for( auto it = polyhedra.back().v_xyz.begin(); it != polyhedra.back().v_xyz.end(); it++ ) {
		center.x += it->x;
		center.y += it->y;
		center.z += it->z;
	}
	apt_real _nrm = 1.0 / static_cast<float>(polyhedra.back().v_xyz.end()-polyhedra.back().v_xyz.begin());
	cout << "Finding outer unit normals" << "\n";
	center.x *= _nrm;
	center.y *= _nrm;
	center.z *= _nrm;
	//MK::center is inside because Poisson-Voronoi cell polyhedron
	cout << "Center " << center.x << ";" << center.y << ";" << center.z << "\n";
	for( auto it = polyhedra.back().fcts.begin(); it != polyhedra.back().fcts.end(); it++ ) {
		//we follow the winding order implied by the input, currently see paraprobe-synthetic the default from Voro++
		size_t start = it->first;
		size_t pastend = it->second;
		if( (pastend - start) > 2 ) {
			size_t vprv = polyhedra.back().v_ids.at(pastend-1);
			size_t vmid = polyhedra.back().v_ids.at(start);
			size_t vnxt = polyhedra.back().v_ids.at(start+1);
			p3d prv = polyhedra.back().v_xyz.at(vprv);
			p3d mid = polyhedra.back().v_xyz.at(vmid);
			p3d nxt = polyhedra.back().v_xyz.at(vnxt);
			p3d prv_mid = p3d( prv.x - mid.x, prv.y - mid.y, prv.z - mid.z );
			p3d nxt_mid = p3d( nxt.x - mid.x, nxt.y - mid.y, nxt.z - mid.z );
			p3d normal = cross( nxt_mid, prv_mid ); //##MK::here might be a silent right-hand rule invalidation if so also invert winding order and ounrm sign flips below!
			_nrm = 1.0 / sqrt(SQR(normal.x)+SQR(normal.y)+SQR(normal.z));
			normal.x *= _nrm;
			normal.y *= _nrm;
			normal.z *= _nrm;
			//on which side using that center is inside the polyhedron
			p3d center_mid = p3d( center.x - mid.x, center.y - mid.y, center.z - mid.z );
			//project center_mid on normal, if negative normal is the outer unit normal, otherwise flip sign
			apt_real projection = dot( center_mid, normal );
			//##MK::Voro++ returns facet contour polygon vertices in anticlockwise order wrt to outer unit normal
			//##MK::currently single precision accuracy here only...!
			if ( projection > static_cast<apt_real>(0.0) ) { //positive on the same side than is the cellcenter so for a Voronoi cell this is
				//candidate normal was the inner unit normal, so flip signs
				polyhedra.back().v_ounrm.push_back( p3d( -normal.x, -normal.y, -normal.z ) );
			}
			else { //normal is sought after outer unit normal
				polyhedra.back().v_ounrm.push_back( normal );
			}

cout << polyhedra.back().v_ounrm.back().x << ";" << polyhedra.back().v_ounrm.back().y << ";" << polyhedra.back().v_ounrm.back().z << "\n";
		}
		else {
			cerr << "Found a degenerate facet of polyhedron " << polyhedra.back().id << "\n"; return false;
		}
	}
	//##MK::END DEBUG

	return true;
}


bool isectHdl::read_points_from_h5()
{
	double tic = MPI_Wtime();

	if ( ConfigIntersector::InputfilePoints.substr(ConfigIntersector::InputfilePoints.length()-3) != ".h5" ) {
		cerr << "InputfilePoints file has not the proper h5 extension!" << "\n";
		return false;
	}

	h5iometa ifo = h5iometa();
	h5offsets offs_xyz = h5offsets();
	h5offsets offs_r = h5offsets();
	int status = 0;

	//probe existence of quaternion array in the input file and get its dimensions
	inputPointsH5Hdl.h5resultsfn = ConfigIntersector::InputfilePoints;
	if ( inputPointsH5Hdl.query_contiguous_matrix_dims( PARAPROBE_ARAULLO_META_MP_XYZ, offs_xyz ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable to access and parse dimensions of the XYZ position array in InputfilePoints!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_ARAULLO_META_MP_XYZ is a " << offs_xyz.nrows() << " rows " << offs_xyz.ncols() << " cols matrix" << "\n";
	}

	if ( inputPointsH5Hdl.query_contiguous_matrix_dims(  PARAPROBE_ARAULLO_META_MP_R, offs_r ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable to access and parse dimensions of the R position array in InputfilePoints!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_ARAULLO_META_MP_R is a " << offs_r.nrows() << " rows " << offs_r.ncols() << " cols matrix" << "\n";
	}

	if ( offs_xyz.nrows() < 1 || offs_r.nrows() < 1 || offs_xyz.nrows() != offs_r.nrows() ||
			offs_xyz.ncols() != PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX ||
				offs_r.ncols() != PARAPROBE_ARAULLO_META_MP_R_NCMAX ) {
		cerr << "PARAPROBE_ARAULLO_META_MP_XYZ and/or PARAPROBE_ARAULLO_META_MP_R have unexpectedly different number of rows or columns or are empty!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	ifo = h5iometa( PARAPROBE_ARAULLO_META_MP_XYZ, offs_xyz.nrows(), offs_xyz.ncols() );
	vector<float> f32xyz;
	status = inputPointsH5Hdl.read_contiguous_matrix_f32le_hyperslab( ifo, offs_xyz, f32xyz );
cout << "PARAPROBE_ARAULLO_META_MP_XYZ read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_ARAULLO_META_XYZ on InputfilePoints failed" << "\n"; return false;
	}

	//read the content and transfer to working array
	ifo = h5iometa( PARAPROBE_ARAULLO_META_MP_R, offs_r.nrows(), offs_r.ncols() );
	vector<float> f32r;
	status = inputPointsH5Hdl.read_contiguous_matrix_f32le_hyperslab( ifo, offs_r, f32r );
cout << "PARAPROBE_ARAULLO_META_MP_R read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_ARAULLO_META_R on InputfilePoints failed" << "\n"; return false;
	}

	try {
		points.reserve( offs_xyz.nrows() );
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error for points during reading points!" << "\n"; return false;
	}

	for( size_t i = 0; i < offs_xyz.nrows(); i++ ) {
		points.push_back( roi( f32xyz.at(3*i+0), f32xyz.at(3*i+1), f32xyz.at(3*i+2), f32r.at(i) ) );
	}
	f32xyz = vector<float>();
	f32r = vector<float>();

	//ConfigIndexer::NumberOfSO3Candidates = testoris.size();
	//dont forget to set ConfigIndexer::NumberOfSO3Candidates once all values were broadcast across process team!
/*
	//##MK::BEGIN DEBUG
	cout << "ROIs" << "\n";
	for( auto it = points.begin(); it != points.end(); it++ ) {
		cout << it->x << ";" << it->y << ";" << it->z << ";" << it->R << "\n";
	}
	//##MK::END DEBUG
*/
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	isect_tictoc.prof_elpsdtime_and_mem( "ReadPointsFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool isectHdl::read_polyhedra_from_h5()
{
	//##MK::here naive approach cache everything at once, in the future better read in blocks
	double tic = MPI_Wtime();

	//if ( ConfigIntersector::InputfilePolyhedra.substr(ConfigIntersector::InputfilePolyhedra.length()-6) != ".apth5" ) {
	if ( ConfigIntersector::InputfilePolyhedra.substr(ConfigIntersector::InputfilePolyhedra.length()-3) != ".h5") {
		cerr << "InputfilePolyhedra file has not the proper h5 extension!" << "\n";
		return false;
	}

	h5iometa ifo = h5iometa();
	h5offsets offs_fct = h5offsets();
	h5offsets offs_xyz = h5offsets();
	h5offsets offs_ids = h5offsets();
	int status = 0;

	//probe existence of quaternion array in the input file and get its dimensions
	inputPolyhedraH5Hdl.h5resultsfn = ConfigIntersector::InputfilePolyhedra;
	if ( inputPolyhedraH5Hdl.query_contiguous_matrix_dims( PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO, offs_fct ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable to access and parse dimensions of the CellTopology array in InputfilePolyhedra!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO is a " << offs_fct.nrows() << " rows " << offs_fct.ncols() << " cols matrix" << "\n";
	}

	if ( inputPolyhedraH5Hdl.query_contiguous_matrix_dims( PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ, offs_xyz ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable to access and parse dimensions of the CellXYZ array in InputfilePolyhedra!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ is a " << offs_xyz.nrows() << " rows " << offs_xyz.ncols() << " cols matrix" << "\n";
	}

	if ( inputPolyhedraH5Hdl.query_contiguous_matrix_dims( PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO, offs_ids ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable to access and parse dimensions of the CellHalo array in InputfilePolyhedra!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO is a " << offs_ids.nrows() << " rows " << offs_ids.ncols() << " cols matrix" << "\n";
	}

	if ( offs_fct.nrows() < 1 || offs_xyz.nrows() < 1 || offs_ids.nrows() < 1 ||
			offs_fct.ncols() != PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO_NCMAX ||
				offs_xyz.ncols() != PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ_NCMAX ||
					offs_ids.ncols() != PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO_NCMAX ) {
		cerr << "At least some H5 arrays are either empty or have unexpected number of columns!" << "\n";
		return false;
	}
	//read the cell topology
	ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO, offs_fct.nrows(), offs_fct.ncols() );
	vector<unsigned int> u32fct;
	status = inputPolyhedraH5Hdl.read_contiguous_matrix_u32le_hyperslab( ifo, offs_fct, u32fct );
cout << "PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO on InputfilePolyhedra failed" << "\n"; return false;
	}

	//read the cell vertices, all of them ##MK::at once, global vertices
	ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ, offs_xyz.nrows(), offs_xyz.ncols() );
	vector<float> f32xyz;
	status = inputPolyhedraH5Hdl.read_contiguous_matrix_f32le_hyperslab( ifo, offs_xyz, f32xyz );
cout << "PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ on InputfilePolyhedra failed" << "\n"; return false;
	}

	//read the cell halo ID, block of cell ID one for each neighbor a cell has ##MK::store leaner
	ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO, offs_ids.nrows(), offs_ids.ncols() );
	vector<int> i32;
	status = inputPolyhedraH5Hdl.read_contiguous_matrix_i32le_hyperslab( ifo, offs_ids, i32 );
cout << "PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO on InputfilePolyhedra failed" << "\n"; return false;
	}

	//reconstruct and instantiate the individual Polyhedra, here from the implicitly defined Poisson-Voronoi tessellation
	//##MK::halo ID specify to which cell each polygon facet belongs, halo ID come always in one contiguous block on i32
	vector<fctifo> fcts; //extractor array, serve isolating all vertices of a single polyhedron
	vector<unsigned int> vrts;
	size_t topo_i = 0;
	int clpid = INT32MX;
	for( size_t ft = 0; ft < i32.size(); ft++ ) { //each cell ID from the halo array implies one facet of the polyhedron named cell ID
		//get current facet
		clpid = i32[ft];
		bool thesame = false; //a facet of which polyhedron?
		if ( ft > 0 ) {
			int prev = i32[ft-1];
			if ( clpid == prev ) { //working still on the same polyhedron
				thesame = true;
			}
			else {
				thesame = false;
			}
		}
		else { //first time cold missing, working still on the same
			thesame = true;
		}
		if ( thesame == true ) { //most likely working on still the same polyhedron, same halo ID as the previous one, there are still facet for the cell which is named clpid
			//keep pulling references to vertices from u32fct to decode the geometry of the facet
			topo_i++; //skip the XDMF type keyword
			unsigned int ncand = u32fct.at(topo_i); //decode a number of edges of the polygon which the facet is
			fcts.push_back( fctifo( 0, ncand, clpid ) ); //a facet to polyhedron named clpid with ncand edges
			topo_i++; //proceed reading these vertex references, ##MK::here the arrangement defines the winding order, currently the default from Voro++
			for( unsigned int j = 0; j < ncand; j++, topo_i++ ) {
				vrts.push_back( u32fct.at(topo_i) ); //pull a global vertex ID, if already the same ID was pulled has no effect
			}
		}
		else { //entering start of description of a new polyhedron
			//so store the old one first, clpid of the prev!
			int prev_clpid = ( ft > 0 ) ? i32[ft-1] : clpid;
			init_polyhedron( prev_clpid, fcts, vrts, f32xyz );

			//then refresh extractor array and counters
			fcts = vector<fctifo>();
			vrts = vector<unsigned int>();

			//keep pulling anew i.e. coldstart for the next polyhedron
			topo_i++;
			unsigned int ncand = u32fct.at(topo_i);
			fcts.push_back( fctifo( 0, ncand, clpid ) );
			topo_i++;
			for( unsigned int j = 0; j < ncand; j++, topo_i++ ) {
				vrts.push_back( u32fct.at(topo_i) ); //pull a global vertex ID, if already the same ID was pulled has no effect
			}
		}
	}
	//instantiate last polyhedron
	init_polyhedron( clpid, fcts, vrts, f32xyz );

	u32fct = vector<unsigned int>();
	f32xyz = vector<float>();
	i32 = vector<int>();

	//ConfigIndexer::NumberOfSO3Candidates = testoris.size();
	//dont forget to set ConfigIndexer::NumberOfSO3Candidates once all values were broadcast across process team!

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	isect_tictoc.prof_elpsdtime_and_mem( "ReadPolyhedraFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool isectHdl::read_polyhedra_oris_from_h5()
{
	double tic = MPI_Wtime();

	if ( ConfigIntersector::InputfilePolyhedraOris.substr(ConfigIntersector::InputfilePolyhedraOris.length()-6) != ".apth5" ) {
		cerr << "InputfilePolyhedraOris file has not the proper h5 extension!" << "\n";
		return false;
	}

	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	int status = 0;

	//probe existence of quaternion array in the input file and get its dimensions
	inputPolyhedraH5Hdl.h5resultsfn = ConfigIntersector::InputfilePolyhedraOris;
	if ( inputPolyhedraH5Hdl.query_contiguous_matrix_dims( PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable to access and parse dimensions of the quaternion array in InputfilePolyhedraOris!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI is a " << offs.nrows() << " rows " << offs.ncols() << " cols matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI_NCMAX ) {
		cerr << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI has unexpectedly different number of rows or columns or is empty!" << "\n";
		return false;
	}

	if ( offs.nrows() != polyhedra.size() ) {
		cerr << "The number of orientations differs from the number of detected polyhedra!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = inputPolyhedraH5Hdl.read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
cout << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI on InputfilePolyhedraOris failed" << "\n"; return false;
	}

	for( size_t i = 0; i < offs.nrows(); i++ ) { //MK::currently the ORDERING OF THE paraprobe-synthetic metadata is implicitly 0, 1, 2,...
		polyhedra_ori.insert( pair<unsigned int, squat>( static_cast<unsigned int>(i),
				squat(f32.at(4*i+0), f32.at(4*i+1), f32.at(4*i+2), f32.at(4*i+3)) ) );
	}
	f32 = vector<float>();

	//ConfigIndexer::NumberOfSO3Candidates = testoris.size();
	//dont forget to set ConfigIndexer::NumberOfSO3Candidates once all values were broadcast across process team!
/*
	//##MK::BEGIN DEBUG
	cout << "PolyhedraOris" << "\n";
	for( auto it = polyhedra_ori.begin(); it != polyhedra_ori.end(); it++ ) {
		cout << it->first << "\t\t" << it->second.q0 << ";" << it->second.q1 << ";" << it->second.q2 << ";" << it->second.q3 << "\n";
	}
	//##MK::END DEBUG
*/
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	isect_tictoc.prof_elpsdtime_and_mem( "ReadPolyhedraOrientationsFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool isectHdl::read_solutions_from_h5()
{
	double tic = MPI_Wtime();

	if ( ConfigIntersector::InputfileSolutions.substr(ConfigIntersector::InputfileSolutions.length()-3) != ".h5" ) {
		cerr << "InputfileSolutions file has not the proper h5 extension!" << "\n"; return false;
	}

	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	int status = 0;

	//probe existence of quaternion array in the input file and get its dimensions
	inputSolutionsH5Hdl.h5resultsfn = ConfigIntersector::InputfileSolutions;
	string grpnm = ConfigIntersector::DatasetSolutions;

	if ( inputSolutionsH5Hdl.probe_group_existence( grpnm ) == false ) {
		cerr << "Unable to access group " << grpnm << " !" << "\n"; return false;
	}
	cout << "Group " << grpnm << " exists, now analyzing dataset inside this group" << "\n";

	/*
	vector<string> ds;
	if ( inputSolutionsH5Hdl.probe_datasets_in_group( grpnm, ds ) == false ) {
		cerr << "Unable to analyze content of the group!" << "\n"; return false;
	}
	cout << "Content of the group analyzed successfully, there are ds.size() " << ds.size() << " datasets at the top-level" << "\n";
	*/

	//now pull the content of these datasets
	string fwslash = "/";
	//for( size_t i = 0; i < ds.size(); i++ ) {
	for( unsigned int i = ConfigIntersector::DatasetSolutionsMin; i <= ConfigIntersector::DatasetSolutionsMax;
			i += ConfigIntersector::DatasetSolutionsIncr ) {
		string dsnm = ConfigIntersector::DatasetSolutions + fwslash + to_string(i); //ds.at(i);
		mpid2solidx.insert( pair<unsigned int,unsigned int>( i, i) );

		if ( inputSolutionsH5Hdl.query_contiguous_matrix_dims( dsnm, offs ) != WRAPPED_HDF5_SUCCESS ) {
			cerr << "Unable to access and parse dimensions of " << dsnm << "\n";
			continue; //##MK::can happen for instance for regular grids where ROI protrude outside of domain and hence
			//paraprobe-indexer refuses them as indexing would be biased anyway!
		}
		else {
			cout << dsnm << " is a " << offs.nrows() << " rows " << offs.ncols() << " cols matrix" << "\n";
		}

		if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_IDXR_RES_SOL_NCMAX ) {
			cerr << dsnm << " is either empty or has an unexpected number of columns!" << "\n"; return false;
		}

		//read the content and transfer to working array
		ifo = h5iometa( dsnm, offs.nrows(), offs.ncols() );
		vector<float> f32;
		status = inputSolutionsH5Hdl.read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
		//cout << "PARAPROBE_IDXR_RES_SOL read " << status << "\n";
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "Reading " << dsnm << " on InputfileSolutions failed" << "\n"; return false;
		}

		try {
			solutions.push_back( idxres() );
			solutions.back().dsnm = to_string(i); //ds.at(i);
			solutions.back().dat = vector<unsigned int>( offs.nrows(), UINT32MX );
			if ( ConfigIntersector::AnalysisMode == SPHERES_POLYHEDRA_INTERSECTIONS) {
				solutions.back().res = vector<float>( offs.nrows()*MAX_INTERSECTING_POLYHEDRA, F32MX );
			}
			else { //COMPARE_WITH_TKD_RESULTS
				solutions.back().res = vector<float>( offs.nrows()*MAX_TKD_TESTS, F32MX );
			}
		}
		catch (bad_alloc &croak) {
			cerr << "Allocation error for solution " << dsnm << " during reading points!" << "\n"; return false;
		}

		if ( ConfigIntersector::AnalysisMode == SPHERES_POLYHEDRA_INTERSECTIONS ) {
			if ( solutions.back().dat.size() != offs.nrows() || solutions.back().res.size() != offs.nrows()*MAX_INTERSECTING_POLYHEDRA ) {
				cerr << "Other dataset related container problem dat.size() != offs.nrows() || res.size() != offs.nrows() !" << "\n"; return false;
			}
		}
		else { //COMPARE_WITH_TKD_RESULTS
			if ( solutions.back().dat.size() != offs.nrows() || solutions.back().res.size() != offs.nrows()*MAX_TKD_TESTS ) {
				cerr << "Other dataset related container problem dat.size() != offs.nrows() || res.size() != offs.nrows() !" << "\n"; return false;
			}
		}

		vector<unsigned int> & thisone = solutions.back().dat;
		for( size_t r = 0; r < offs.nrows(); r++ ) {
			thisone.at(r) = static_cast<unsigned int>( f32.at(2*r+0) ); //##MK::skip solution quality
		}
		f32 = vector<float>();
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	isect_tictoc.prof_elpsdtime_and_mem( "ReadSolutionsFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool isectHdl::read_orientations_from_h5()
{
	double tic = MPI_Wtime();

	if ( ConfigIntersector::InputfileOris.substr(ConfigIntersector::InputfileOris.length()-3) != ".h5" ) {
		cerr << "InputfileOris file has not the proper h5 extension!" << "\n";
		return false;
	}

	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	int status = 0;

	//probe existence of quaternion array in the input file and get its dimensions
	inputOrisH5Hdl.h5resultsfn = ConfigIntersector::InputfileOris;
	if ( inputOrisH5Hdl.query_contiguous_matrix_dims( PARAPROBE_IDXR_META_ORI_QUAT, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable to access and parse dimensions of the Orientations matrix in InputfileOris!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_IDXR_META_ORI_QUAT is a " << offs.nrows() << " rows " << offs.ncols() << " cols matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_IDXR_META_ORI_QUAT_NCMAX ) {
		cerr << "PARAPROBE_IDXR_META_ORI_QUAT is either empty or has unexpectedly different number of columns!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	ifo = h5iometa( PARAPROBE_IDXR_META_ORI_QUAT, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = inputOrisH5Hdl.read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
cout << "PARAPROBE_IDXR_META_ORI_QUAT read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_IDXR_META_ORI_QUAT on InputfileOris failed" << "\n"; return false;
	}

	try {
		oris.reserve( offs.nrows() );
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error for oris during reading orientations!" << "\n"; return false;
	}

	for( size_t i = 0; i < offs.nrows(); i++ ) {
		oris.push_back( squat( f32.at(4*i+0), f32.at(4*i+1), f32.at(4*i+2), f32.at(4*i+3) ) );
	}
	f32 = vector<float>();

	//ConfigIndexer::NumberOfSO3Candidates = testoris.size();
	//dont forget to set ConfigIndexer::NumberOfSO3Candidates once all values were broadcast across process team!
/*
	//##MK::BEGIN DEBUG
	cout << "OrientationCandidates" << "\n";
	for( auto it = oris.begin(); it != oris.end(); it++ ) {
		cout << it->q0 << ";" << it->q1 << ";" << it->q2 << ";" << it->q3 << "\n";
	}
	//##MK::END DEBUG
*/
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	isect_tictoc.prof_elpsdtime_and_mem( "ReadOrientationsFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool isectHdl::init_target_file()
{
	double tic = MPI_Wtime();

	string h5fn_out = "PARAPROBE.Intersector.Results.SimID." + to_string(ConfigShared::SimID) + ".h5";
	debugh5Hdl.h5resultsfn = h5fn_out;

	//only master instantiates the file
	if ( get_myrank() == MASTER ) {
cout << "Rank " << MASTER << " initializing target file " << h5fn_out << "\n";

		if ( debugh5Hdl.create_isect_apth5( h5fn_out ) == WRAPPED_HDF5_SUCCESS ) {
			double toc = MPI_Wtime();
			memsnapshot mm = memsnapshot();
			isect_tictoc.prof_elpsdtime_and_mem( "InitTargetResultsH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
			return true;
		}
		else {
			return false;
		}
	}
	else { //slaves just fill the names
		double toc = MPI_Wtime();
		memsnapshot mm = memsnapshot();
		isect_tictoc.prof_elpsdtime_and_mem( "InitTargetResultsH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
		return true;
	}
	return true;
}


bool isectHdl::write_environment_and_settings()
{
	vector<pparm> hardware = isect_tictoc.report_machine();
	string prfx = PARAPROBE_UTILS_HRDWR_META_KEYS;
	for( auto it = hardware.begin(); it != hardware.end(); it++ ) {
		cout << it->keyword << "__" << it->value << "__" << it->unit << "__" << it->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *it ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing hardware setting " << it->keyword << " failed!" << "\n";
			return false;
		}
	}
	cout << "Rank " << MASTER << " hardware settings written to H5" << "\n";

	vector<pparm> software;
	ConfigIntersector::reportSettings( software );
	prfx = PARAPROBE_UTILS_SFTWR_META_KEYS;
	for( auto jt = software.begin(); jt != software.end(); jt++ ) {
		cout << jt->keyword << "__" << jt->value << "__" << jt->unit << "__" << jt->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *jt ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing software tool setting " << jt->keyword << " failed!" << "\n";
			return false;
		}
	}
	cout << "Rank " << MASTER << " software settings written to H5" << "\n";

	return true;
}


bool isectHdl::tetrahedralize_polyhedra()
{
	double tic = MPI_Wtime();
	//we use TetGen which is not guaranteed thread-safe, it may use static variables as well
	//##MK::for distributed memory how and C/C++ programs each program daemon has a one bss storage for static variables
	//https://www.geeksforgeeks.org/memory-layout-of-c-program/
	//hence calling TetGen via processes where each rank maps on a different physical system should not interfere
	//to get consistent outer unit normals to our marching cubes triangle hulls we tetrahedralize first the piecewise linear complex
	//next we compute for every triangle the adjoining interior tetrahedra, we compute the normal
	//and we pick a reference tetrahedron such that we can evaluate the distance of the triangle barycenter to the tetrahedron
	//barycenter. given that the barycenter of the tetrahedron is guarantee inside the PLC, as the PLC has no holes (<--what we assume!)
	//we can eventually flip normals and will get consistent. this is the same trick we played for our DAMASK Voronoi composition algorithm
	//##MK::MSMSE, 2019 K�hbach, Roters
	//upon tetrahedralization however steiner points are added inside the volume of the PLC to build "well-behaved tetrahedra"
	//this point insertion is agnostic to the actual vertex ID of the original PLC
	//hence there are multiple tetrahedra adjoint the 3 vertices of each triangle
	//we need therefore to filter out that particular tetrahedron whose barycenter projects inside the triangle face
	//when we know that even at troublesome non-convex portions of our polyhedron we are safe with respect to our unit normal
	if ( polyhedra.size() < 1 ) {
		cerr << "There are no polyhedra!" << "\n";
		return false;
	}

	bool status = true;
	vector<unsigned int> failed;
	for( size_t i = 0; i < polyhedra.size(); i++ ) {
		double mtic = omp_get_wtime();
		//if ( g->props.gid == 271 ) {
			if ( polyhedra.at(i).tete_a_tete() == true ) {
				continue;
			}
			else {
				failed.push_back( polyhedra.at(i).id );
			}
		//}

		double mtoc = omp_get_wtime();
cout << "Tetrahedralize polyhedron.id " << i << ";" << polyhedra.at(i).id << " in " << (mtoc-mtic) << " seconds" << "\n";
	}

	double toc = MPI_Wtime();
	memsnapshot mm = isect_tictoc.get_memoryconsumption();
	isect_tictoc.prof_elpsdtime_and_mem( "Tetrahedralize", APT_XX, APT_IS_SEQ, mm, tic, toc);

	if ( failed.size() > 0  ) {
		cerr << "It was not possible to successfully tetrahedralize all polyhedra!" << "\n";
		return false;
	}
	else {
		return true;
	}
}


bool isectHdl::build_tetraeder_bvh()
{
	double tic = MPI_Wtime();

	//can get parallelized
//	#pragma omp parallel
//	{
//		#pragma omp for schedule(dynamic,1)
	bool status = true;
	vector<unsigned int> failed;
	for( size_t i = 0; i < polyhedra.size(); i++ ) {
		double mtic = omp_get_wtime();

		//if ( g->props.gid == 271 ) {
			if ( polyhedra.at(i).tets_in_the_box() == true ) {
				continue;
			}
			else {
				failed.push_back( polyhedra.at(i).id );
			}
		//}

		double mtoc = omp_get_wtime();
cout << "Building tetraeder BVH on i polyhedron.id " << i << ";" << polyhedra.at(i).id << " in " << (mtoc-mtic) << " seconds" << "\n";
	}

	if ( failed.size() > 0 ) {
		cerr << "It was not possible to successfully build BVH of the tetraeder all polyhedra!" << "\n";
		return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isect_tictoc.get_memoryconsumption();
	isect_tictoc.prof_elpsdtime_and_mem( "BuildTetraederBVH", APT_XX, APT_IS_SEQ, mm, tic, toc);

	tic = MPI_Wtime();

	if ( ConfigIntersector::IOVisualizeTetrahedra == true ) {
		vector<tetrahedra_io_ifo> xdmf_info;

		for( size_t i = 0; i < polyhedra.size(); i++ ) {
			//double mtic = omp_get_wtime();

			if ( visualize_tetrahedra_decomposition( i, xdmf_info ) == true ) {
				continue;
			}
			else {
				cerr << "Writing visualization files for tetrahedra decomposition of polyhedron " << i << " failed!" << "\n";
				return false;
			}
			//double mtoc = omp_get_wtime();
			//cout << "Building tetraeder BVH on i polyhedron.id " << i << ";" << polyhedra.at(i).id << " in " << (mtoc-mtic) << " seconds" << "\n";
		}

		string prfx = "PARAPROBE.Intersector.Results.SimID." + to_string(ConfigShared::SimID);
		string h5fn = prfx + ".h5";
		if ( debugxdmf.create_tetrahedra_files( prfx, h5fn, xdmf_info ) != WRAPPED_XDMF_SUCCESS ) {
			cerr << "Writing supportive XDMF file to visualize the tetrahedralized polyhedra failed!" << "\n";
			return false;
		}
	}

	toc = MPI_Wtime();
	mm = memsnapshot();
	isect_tictoc.prof_elpsdtime_and_mem( "VisualizeTetraederBVH", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool isectHdl::visualize_tetrahedra_decomposition( const size_t id, vector<tetrahedra_io_ifo> & obj_ifo )
{
	string fwslash = "/";
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";
	size_t roffset = 0;

	//create one group per polyhedron
	string grpnm = PARAPROBE_ISECT_META_TETS + fwslash + to_string(polyhedra.at(id).id);
	status = debugh5Hdl.create_group( grpnm );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << grpnm << " create failed! " << status << "\n"; return false;
	}
	cout << grpnm << " create " << status << "\n";

	//write topology and IDs
	vector<unsigned int> u32topo;
	vector<unsigned int> u32tids;
	unsigned int tetid = 0;
	for( auto it = polyhedra.at(id).tet_id.begin(); it != polyhedra.at(id).tet_id.end(); it++, tetid++ ) {
		u32topo.push_back( it->v1 );
		u32topo.push_back( it->v2 );
		u32topo.push_back( it->v3 );
		u32topo.push_back( it->v4 );
		u32tids.push_back( tetid );
	}
	vector<unsigned int> u32pids = vector<unsigned int>( polyhedra.at(id).tet_id.size(), polyhedra.at(id).id ); //all tets belong to polyhedron id

	//write unique vertices xyz
	vector<double> f64;
	for( auto jt = polyhedra.at(id).tet_v.begin(); jt != polyhedra.at(id).tet_v.end(); jt++ ) {
		f64.push_back( jt->x );
		f64.push_back( jt->y );
		f64.push_back( jt->z );
	}

	//topo
	dsnm = grpnm + fwslash + PARAPROBE_ISECT_META_TETS_TOPO;
	ifo = h5iometa( dsnm, u32topo.size()/PARAPROBE_ISECT_META_TETS_TOPO_NCMAX, PARAPROBE_ISECT_META_TETS_TOPO_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status == WRAPPED_HDF5_SUCCESS ) {
		offs = h5offsets( 0, u32topo.size()/PARAPROBE_ISECT_META_TETS_TOPO_NCMAX, 0, PARAPROBE_ISECT_META_TETS_TOPO_NCMAX,
							u32topo.size()/PARAPROBE_ISECT_META_TETS_TOPO_NCMAX, PARAPROBE_ISECT_META_TETS_TOPO_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32topo );
		if ( status == WRAPPED_HDF5_SUCCESS ) {
//cout << "Rank " << get_myrank() << " wrote " << dsnm << " status " << status << "\n";
			u32topo = vector<unsigned int>();
		}
		else {
			cerr << "Rank " << get_myrank() << " failed writing dsnm " << dsnm << "!" << "\n";
		}
	}

	//tids
	dsnm = grpnm + fwslash + PARAPROBE_ISECT_META_TETS_TIDS;
	ifo = h5iometa( dsnm, u32tids.size()/PARAPROBE_ISECT_META_TETS_TIDS_NCMAX, PARAPROBE_ISECT_META_TETS_TIDS_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status == WRAPPED_HDF5_SUCCESS ) {
		offs = h5offsets( 0, u32tids.size()/PARAPROBE_ISECT_META_TETS_TIDS_NCMAX, 0, PARAPROBE_ISECT_META_TETS_TIDS_NCMAX,
							u32tids.size()/PARAPROBE_ISECT_META_TETS_TIDS_NCMAX, PARAPROBE_ISECT_META_TETS_TIDS_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32tids );
		if ( status == WRAPPED_HDF5_SUCCESS ) {
//cout << "Rank " << get_myrank() << " wrote " << dsnm << " status " << status << "\n";
			u32tids = vector<unsigned int>();
		}
		else {
			cerr << "Rank " << get_myrank() << " failed writing dsnm " << dsnm << "!" << "\n";
		}
	}

	//pids
	dsnm = grpnm + fwslash + PARAPROBE_ISECT_META_TETS_PIDS;
	ifo = h5iometa( dsnm, u32pids.size()/PARAPROBE_ISECT_META_TETS_PIDS_NCMAX, PARAPROBE_ISECT_META_TETS_PIDS_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status == WRAPPED_HDF5_SUCCESS ) {
		offs = h5offsets( 0, u32pids.size()/PARAPROBE_ISECT_META_TETS_PIDS_NCMAX, 0, PARAPROBE_ISECT_META_TETS_PIDS_NCMAX,
							u32pids.size()/PARAPROBE_ISECT_META_TETS_PIDS_NCMAX, PARAPROBE_ISECT_META_TETS_PIDS_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32pids );
		if ( status == WRAPPED_HDF5_SUCCESS ) {
//cout << "Rank " << get_myrank() << " wrote " << dsnm << " status " << status << "\n";
			u32pids = vector<unsigned int>();
		}
		else {
			cerr << "Rank " << get_myrank() << " failed writing dsnm " << dsnm << "!" << "\n";
		}
	}

	//xyz
	dsnm = grpnm + fwslash + PARAPROBE_ISECT_META_TETS_XYZ;
	ifo = h5iometa( dsnm, f64.size()/PARAPROBE_ISECT_META_TETS_XYZ_NCMAX, PARAPROBE_ISECT_META_TETS_XYZ_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f64le( ifo );
	if ( status == WRAPPED_HDF5_SUCCESS ) {
		offs = h5offsets( 0, f64.size()/PARAPROBE_ISECT_META_TETS_XYZ_NCMAX, 0, PARAPROBE_ISECT_META_TETS_XYZ_NCMAX,
							f64.size()/PARAPROBE_ISECT_META_TETS_XYZ_NCMAX, PARAPROBE_ISECT_META_TETS_XYZ_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_f64le_hyperslab( ifo, offs, f64 );
		if ( status == WRAPPED_HDF5_SUCCESS ) {
//cout << "Rank " << get_myrank() << " wrote " << dsnm << " status " << status << "\n";
			f64 = vector<double>();
		}
		else {
			cerr << "Rank " << get_myrank() << " failed writing dsnm " << dsnm << "!" << "\n";
		}
	}

	obj_ifo.push_back( tetrahedra_io_ifo( 	polyhedra.at(id).id,
											polyhedra.at(id).tet_id.size(),
											polyhedra.at(id).tet_v.size(),
											polyhedra.at(id).tet_id.size(),
											polyhedra.at(id).tet_id.size()  )   ); //polyhedra.at(id).tet_id.size()*4,
	return true;
}


bool isectHdl::build_polyhedra_bvh()
{
	double tic = MPI_Wtime();

	//builds an AABBTree / RTree of tetraeder in $\mathcal{R}^3$
	if ( polyhedra.size() < 1 ) {
		cerr << "WARNING There are no polyhedra" << "\n"; return false;
	}
	if ( polyhedra.size() >= static_cast<size_t>(UINT32MX-1) ) {
		cerr << "Implementation of polyhedra BVH supports currently at most UINT32MX individuals!" << "\n";
		return false;
	}

	unsigned int npoly = static_cast<unsigned int>(polyhedra.size());
	poly_bvh = NULL;
	try {
		poly_bvh = new class Tree( npoly );
	}
	catch (bad_alloc &croak) {
		cerr << "Unable to allocate memory for the tree of the polyhedra BVH!" << "\n";
		return false;
	}

	for( auto it = polyhedra.begin(); it != polyhedra.end(); it++ ) {

		poly_bvh->insertParticle( it->id, trpl(	static_cast<float>(it->box.xmi),
												static_cast<float>(it->box.ymi),
												static_cast<float>(it->box.zmi)  ),
								     	  trpl(	static_cast<float>(it->box.xmx),
												static_cast<float>(it->box.ymx),
												static_cast<float>(it->box.zmx)  )     );
cout << it->id << "\t\t" << it->box.xmi << ";" << it->box.ymi << ";" << it->box.zmi << "\t\t" << it->box.xmx << ";" << it->box.ymx << ";" << it->box.zmx << "\n";
	}

	cout << "Polyhedra BVH poly_bvh->getNodeCount() " << poly_bvh->getNodeCount() << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isect_tictoc.get_memoryconsumption();
	isect_tictoc.prof_elpsdtime_and_mem( "BuildPolyhedraBVH", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


void isectHdl::compute_spheres_polyhedra_intersections()
{
	double tic = MPI_Wtime();

	//then for each ROI compute which polyhedra it potentially intrudes
	//polyhedra arrive  unordered so build access dictionary

	//#pragma omp parallel
	//##MK::need to make solution dump res independent!
	//{
		map<unsigned int,size_t> polyid2idx;
		for( size_t i = 0; i < polyhedra.size(); i++ ) {
			polyid2idx.insert( pair<unsigned int,size_t>( polyhedra.at(i).id, i ) );
		}

		//threads distribute processing of intersections
		//#pragma omp for schedule(dynamic,1) nowait
		for( size_t p = 0; p < points.size(); p++ ) {
			roi mp = points.at(p);
//cout << "ROI " << mp.x << ";" << mp.y << ";" << mp.z << ";" << mp.R << "\n";

			map<unsigned int, unsigned int>::iterator whichsolution = mpid2solidx.find( p );

			vector_t origin( static_cast<double>(mp.x), static_cast<double>(mp.y), static_cast<double>(mp.z) );
			Sphere s( origin, static_cast<double>(mp.R) );

			double Vroi = (4.0 / 3.0) * MYPI * CUBE(static_cast<double>(mp.R)); //nm^3
			double Vsum = 0.0;
			double Vtet = 0.0;

			//find which polyhedra the roi possibly intrudes
			hedgesAABB roi_aabb( 	trpl( mp.x-mp.R, mp.y-mp.R, mp.z-mp.R ),
									trpl( mp.x+mp.R, mp.y+mp.R, mp.z+mp.R )   ); //##MK::epsilon

			//for each polyhedron candidate...
			vector<isect_res> polyhedra_intersections;
			vector<unsigned int> polyhedra_cand;
			polyhedra_cand = poly_bvh->query( roi_aabb );
//cout << "polyhedra_cand.size() " << polyhedra_cand.size() << "\n";

			for( auto plyit = polyhedra_cand.begin(); plyit != polyhedra_cand.end(); plyit++ ) {
				map<unsigned int,size_t>::iterator here = polyid2idx.find( *plyit );
				if ( here != polyid2idx.end() ) {
					//...use the polyhedron tetraeder BVH to check which of its tetraeder the ROI touches or intrudes

					double Vply = 0.0;

					vector<unsigned int> tet_cand;
					size_t thisone = here->second;
					tet_cand = polyhedra.at(thisone).tet_bvh->query( roi_aabb );
//cout << "\t\t" << "plyit " << *plyit << " thisone " << thisone << " tet_cand.size() " << tet_cand.size() << "\n";

					//accumulate all partial intersection volumina into per polyhedron intersection net volume
					vector<tet3d64> & these = polyhedra.at(thisone).tet_xyz;
					for( auto tetit = tet_cand.begin(); tetit != tet_cand.end(); tetit++ ) {
						//get tetrahedron and pass to ...
						tet3d64 t = these.at(*tetit);
						vector_t p0( t.v1.x, t.v1.y, t.v1.z );
						vector_t p1( t.v2.x, t.v2.y, t.v2.z );
						vector_t p2( t.v3.x, t.v3.y, t.v3.z );
						vector_t p3( t.v4.x, t.v4.y, t.v4.z );
//cout << t.v1.x << ";" << t.v1.y << ";" << t.v1.z << "\n";
//cout << t.v2.x << ";" << t.v2.y << ";" << t.v2.z << "\n";
//cout << t.v3.x << ";" << t.v3.y << ";" << t.v3.z << "\n";
//cout << t.v4.x << ";" << t.v4.y << ";" << t.v4.z << "\n";

						Tetrahedron tt(p0, p1, p2, p3);

						//...S. Strobl et al.'s algorithm to compute intersection volume of sphere and tetrahedron analytically
						Vtet = overlap(s, tt);

						Vply = Vply + Vtet;
//cout << "\t\t\t\t" << Vply << "\n";
					}

					//compute disorientation between solutions and polyhedron ground truth
					//compute if there is a positive net intersection volume in which case compute disorientation angle to polyhedron and solutions of the ROI
					Vsum = Vsum + Vply;
					polyhedra_intersections.push_back( isect_res( Vply, *plyit ) );
				}
				else {
					cerr << "Unexpected not finding of a polyhedron!" << "\n";
				}
			} //next possibly intruding polyhedron

			sort( polyhedra_intersections.begin(), polyhedra_intersections.end(), SortForDecreasingVin );

			/*
			//##MK::BEGIN DEBUG
			cout << "PolyhedraIntersections" << "\n";
			for( auto jt = polyhedra_intersections.begin(); jt != polyhedra_intersections.end(); jt++ ) {
				cout << jt->id << "\t\t" << jt->Vin << "\n";
			}
			//##MK::END DEBUG
			*/

			if ( whichsolution != mpid2solidx.end() ) {
				size_t solid = static_cast<size_t>(whichsolution->second);
				idxres & dump = solutions.at(solid);
				dump.TotalVolumeROI = Vroi;
				size_t ni = polyhedra_intersections.size();
				if ( ni > 0 ) {
					for( size_t j = 0; j < MAX_INTERSECTING_POLYHEDRA; j++ ) {
						if ( j < ni ) {
							//we assume now that the grain with the j-th largest volume contribution is the ground truth
							//so compute disorientation to that ground truth
							unsigned int plythis = polyhedra_intersections.at(j).id;
							map<unsigned int,squat>::iterator where = polyhedra_ori.find( plythis );
							if ( where != polyhedra_ori.end() ) {
								squat qgroundtruth = where->second;

								//size_t solid = static_cast<size_t>(whichsolution->second);
								//idxres & dump = solutions.at(solid);
								for( size_t ii = 0; ii < dump.dat.size(); ii++ ) {
									//pull quaternion solution from the indexing paraprobe-indexer
									unsigned int oriid = dump.dat.at(ii);
									squat qsol = oris.at(oriid);

									//compute disorientation (angle) to ground truth
									apt_real theta = F32MX;
									pair<bool,squat> thisdisori = disoriquaternion_cubic_cubic( qgroundtruth, qsol );
									if ( thisdisori.first == true ) {
										theta = RADIANT2DEGREE(2.0*acos(thisdisori.second.q0));
									}

									//report disorentation to ground truth
									dump.res.at(MAX_INTERSECTING_POLYHEDRA*ii+j) = theta;
		//cout << "\t\t\t\t\t\t" << ii << "\t\t" << theta << "\n";
								}
							}

							dump.FourStrongestOverlap[2*j+0] = static_cast<double>(polyhedra_intersections.at(j).id);
							dump.FourStrongestOverlap[2*j+1] = static_cast<double>(polyhedra_intersections.at(j).Vin);
						}
						else {
							break;
						}
					}
				}
				else {
					cerr << "There is a ROI with no largest volume polyhedron, this is unexpected, please check! whichsolution->second " << whichsolution->second << "\n";
				}
			}
//cout << "Vroi " << setprecision(18) << Vroi << " Vsum " << setprecision(18) << Vsum << "\n";
		} //next ROI
	//}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isect_tictoc.get_memoryconsumption();
	isect_tictoc.prof_elpsdtime_and_mem( "ComputeIntersections", APT_XX, APT_IS_PAR, mm, tic, toc);
}


void isectHdl::compare_with_tkd()
{
	double tic = MPI_Wtime();

	//Andrew Breens PowerPoint slide to PLOS One paper
	orimatrix g1_om = orimatrix( 	-0.9238532, 	-0.35945523, 	+0.13152905,
									+0.36735857,	-0.73619806,	+0.56840582,
									-0.1074796,		+0.573456139,	+0.81216579   );
cout << "g1_om" << "\n";
cout << g1_om << "\n";
	orimatrix g2_om = orimatrix( 	+0.2140449,		-0.97396431,	+0.07463923,
									+0.31938075,	+0.141983661,	+0.93692933,
									-0.9231381,		-0.17669742,	+0.34145718   );
cout << "g2_om" << "\n";
cout << g2_om << "\n";
	orimatrix g1_om_iv = g1_om.inv();
cout << "g1_om_iv" << "\n";
cout << g1_om_iv << "\n";
	orimatrix g2_om_iv = g2_om.inv();
cout << "g2_om_iv" << "\n";
cout << g2_om_iv << "\n";

	squat q_g1_om = g1_om.om2qu();
cout << "q_g1_om" << "\n";
cout << q_g1_om;
	squat q_g2_om = g2_om.om2qu();
cout << "q_g2_om" << "\n";
cout << q_g2_om;
	squat q_g1_om_iv = g1_om_iv.om2qu();
cout << "q_g1_om_iv" << "\n";
cout << q_g1_om_iv;
	squat q_g2_om_iv = g2_om_iv.om2qu();
cout << "q_g2_om_iv" << "\n";
cout << q_g2_om_iv;

	//PLOS one reported bunge Euler angles
	//Figure 1
	bunge g1_eu1 = bunge( DEGREE2RADIANT(324.1), DEGREE2RADIANT(50.3), DEGREE2RADIANT(2.6) );
cout << "g1_eu1" << "\n";
cout << g1_eu1;
	bunge g2_eu1 = bunge( DEGREE2RADIANT(32.3), DEGREE2RADIANT(44.9), DEGREE2RADIANT(6.7) );
cout << "g2_eu1" << "\n";
cout << g2_eu1;
	squat q_g1_eu1 = g1_eu1.eu2qu();
cout << "q_g1_eu1" << "\n";
cout << q_g1_eu1;
	squat q_g2_eu1 = g2_eu1.eu2qu();
cout << "q_g2_eu1" << "\n";
cout << q_g2_eu1;

	//Figure 2
	bunge g1_eu2 = bunge( DEGREE2RADIANT(19.2), DEGREE2RADIANT(68.9), DEGREE2RADIANT(21.0) );
cout << "g1_eu2" << "\n";
cout << g1_eu2;
	bunge g2_eu2 = bunge( DEGREE2RADIANT(298.2), DEGREE2RADIANT(60.9), DEGREE2RADIANT(57.5) );
cout << "g2_eu2" << "\n";
cout << g2_eu2;
	squat q_g1_eu2 = g1_eu2.eu2qu();
cout << "q_g1_eu2" << "\n";
cout << q_g1_eu2;
	squat q_g2_eu2 = g2_eu2.eu2qu();
cout << "q_g2_eu2" << "\n";
cout << q_g2_eu2;

	vector<squat> groundtruth;
	groundtruth.push_back( q_g1_om ); //PowerPoint g1
	groundtruth.push_back( q_g2_om ); //PowerPoint g2
	groundtruth.push_back( q_g1_om_iv ); //PowerPoint inv(g1)
	groundtruth.push_back( q_g2_om_iv ); //PowerPoint inv(g2)
	groundtruth.push_back( q_g1_eu1 ); //PLOS one Fig1 g1
	groundtruth.push_back( q_g2_eu1 ); //PLOS one Fig1 g2
	groundtruth.push_back( q_g1_eu2 ); //PLOS one Fig2 g1
	groundtruth.push_back( q_g2_eu2 ); //PLOS one Fig2 g2
	//size_t ncases = MAX_TKD_TESTS;

	#pragma omp parallel shared(groundtruth)
	{
		//vector<tkd_res> myres;
		//then for each ROI get the solutions for each phase and compare with a ##MK::hardcoded set of orientations
		#pragma omp for schedule(dynamic,1)
		for( size_t p = 0; p < points.size(); p++ ) {
			roi mp = points.at(p);
	//cout << "ROI " << mp.x << ";" << mp.y << ";" << mp.z << ";" << mp.R << "\n";

			map<unsigned int, unsigned int>::iterator whichsolution = mpid2solidx.find( p );
			if ( whichsolution != mpid2solidx.end() ) {
				size_t solid = static_cast<size_t>(whichsolution->second);
				idxres & dump = solutions.at(solid);

				for( size_t ii = 0; ii < dump.dat.size(); ii++ ) {
					//pull quaternion solution from the indexing paraprobe-indexer
					unsigned int oriid = dump.dat.at(ii);
					squat qsol = oris.at(oriid);

					for( size_t c = 0; c < MAX_TKD_TESTS; c++ ) {
						//compute disorientation (angle) to ground truth
						apt_real theta = F32MX;
						squat qthis = groundtruth.at(c);
						pair<bool,squat> thisdisori = disoriquaternion_cubic_cubic( qthis, qsol );
						if ( thisdisori.first == true ) {
							theta = RADIANT2DEGREE(2.0*acos(thisdisori.second.q0));
						}

						//report disorentation to ground truth
						dump.res.at(MAX_TKD_TESTS*ii+c) = theta;

	//cout << setprecision(32) << dump.res.at(MAX_TKD_TESTS*ii+c) << ";" << theta << "\n";
					} //next consistence test
				} //next solution
			} //when a solution was found/matpoint results to test exist
	cout << p << "\n";
		}

	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isect_tictoc.get_memoryconsumption();
	isect_tictoc.prof_elpsdtime_and_mem( "CompareWithTKD", APT_XX, APT_IS_PAR, mm, tic, toc);
}


void isectHdl::write_solutions_isect_to_h5()
{
	double tic = MPI_Wtime();

	string fwslash = "/";
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";
	size_t roffset = 0;

	/*
	//##MK::DEBUG simple OpenMP profiling per material point
	string tictoc_fn = "PARAPROBE.Araullo.SimID." + to_string(ConfigShared::SimID) + ".Rank." + to_string(get_myrank()) + ".DetailedProfiling.csv";
	ofstream csvlog;
	csvlog.open(tictoc_fn.c_str(), ofstream::out | ofstream::trunc);
	if (csvlog.is_open() == true) {
		//header
		csvlog << "MPID;PHCANDID;IonsInROI;ThreadID;Project;FFT;FindPks\n";
		csvlog << ";;;;s;s;s\n";
		csvlog << "MPID;PHCANDID;IonsInROI;ThreadID;Project;FFT;FindPks\n";
	}
	else {
		cerr << "Unable to write process-local profiling files" << "\n";
	}
	//##MK::DEBUG
	*/

	for( auto it = solutions.begin(); it != solutions.end(); it++ ) {
		//##MK::currently handles only a single-phase!
		dsnm = PARAPROBE_ISECT_RES_DIS + fwslash + "PH0" + fwslash + it->dsnm;
		if ( it->res.size()/MAX_INTERSECTING_POLYHEDRA == it->dat.size() && it->res.size() > 0 && dsnm != "" ) {
			vector<float> f32;
			for( size_t i = 0; i < it->dat.size(); i++ ) {
				f32.push_back( static_cast<float>(it->dat.at(i)) );
				f32.push_back( it->res.at(MAX_INTERSECTING_POLYHEDRA*i+0) );
				f32.push_back( it->res.at(MAX_INTERSECTING_POLYHEDRA*i+1) );
				f32.push_back( it->res.at(MAX_INTERSECTING_POLYHEDRA*i+2) );
				f32.push_back( it->res.at(MAX_INTERSECTING_POLYHEDRA*i+3) );
			}
			it->dat = vector<unsigned int>();
			it->res = vector<float>();

			ifo = h5iometa( dsnm, f32.size()/PARAPROBE_ISECT_RES_DIS_NCMAX, PARAPROBE_ISECT_RES_DIS_NCMAX );
			status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
			if ( status == WRAPPED_HDF5_SUCCESS ) {
				offs = h5offsets( 0, f32.size()/PARAPROBE_ISECT_RES_DIS_NCMAX, 0, PARAPROBE_ISECT_RES_DIS_NCMAX,
										f32.size()/PARAPROBE_ISECT_RES_DIS_NCMAX, PARAPROBE_ISECT_RES_DIS_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
				if ( status == WRAPPED_HDF5_SUCCESS ) {
//cout << "Rank " << get_myrank() << " wrote SpecificPeak " << dsnm << " status " << status << "\n";
					f32 = vector<float>();
				}
				else {
					cerr << "Rank " << get_myrank() << " failed writing dsnm " << dsnm << "!" << "\n";
				}
			}
		}
		else {
			cerr << "Rank " << get_myrank() << " no results for dsnm " << dsnm << "!" << "\n";
		}
	} //next solution

	//report overlap
	vector<double> f64;
	for( auto it = solutions.begin(); it != solutions.end(); it++ ) {
		if ( it->dsnm != "" ) {
			f64.push_back( static_cast<double>( stoul(it->dsnm) ) );
			f64.push_back( it->TotalVolumeROI );
			for( size_t j = 0; j < MAX_INTERSECTING_POLYHEDRA; j++ ) {
				f64.push_back( it->FourStrongestOverlap[2*j+0] );
				f64.push_back( it->FourStrongestOverlap[2*j+1] );
			}
		}
	}
/*
	//##MK::BEGIN DEBUG
	for( auto kt = f64.begin(); kt != f64.end(); kt++ ) {
		cout << *kt << "\n";
	}
	//##MK::END DEBUG
*/
	dsnm = PARAPROBE_ISECT_RES_OVER;
	ifo = h5iometa( dsnm, f64.size()/PARAPROBE_ISECT_RES_OVER_NCMAX, PARAPROBE_ISECT_RES_OVER_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f64le( ifo );
	if ( status == WRAPPED_HDF5_SUCCESS ) {
		offs = h5offsets( 0, f64.size()/PARAPROBE_ISECT_RES_OVER_NCMAX, 0, PARAPROBE_ISECT_RES_OVER_NCMAX,
								f64.size()/PARAPROBE_ISECT_RES_OVER_NCMAX, PARAPROBE_ISECT_RES_OVER_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_f64le_hyperslab( ifo, offs, f64 );
		if ( status == WRAPPED_HDF5_SUCCESS ) {
cout << "Rank " << get_myrank() << " wrote dsnm " << dsnm << " status " << status << "\n";
		}
		else {
			cerr << "Rank " << get_myrank() << " failed writing dsnm " << dsnm << "!" << "\n";
		}
	}
	f64 = vector<double>();

	/*
	if ( csvlog.is_open() == true ) {
		csvlog.flush();
		csvlog.close();
	}
	*/

	//collect XDMF results
	/*
	if ( get_nranks() == SINGLEPROCESS && get_myrank() == MASTER ) { //##MK::implement communication across processes to identify which results exist
		for( auto mtsk = itsk.iphcands.begin(); mtsk != itsk.iphcands.end(); mtsk++ ) {
			vector<unsigned int> attrnm;
			int current_phcid = mtsk->candid;

			for( size_t thr = 0; thr < workers.size(); thr++ ) {
				acquisor* thrhdl = workers.at(thr);
				if ( thrhdl != NULL ) {
					for( auto it = thrhdl->res.begin(); it != thrhdl->res.end(); it++ ) {
						int mpid = it->mpid;
						int phcid = it->phcandid;
						araullo_res* thisone = it->dat;
						if ( phcid != current_phcid || thisone == NULL ) {
							continue;
						}
						else {
							attrnm.push_back( mpid );
						}
					} //test if results for next guy are there
				} //only valid threadregions
			} //next threadregion

			string xmlfn = "PARAPROBE.Araullo.Results.SimID." + to_string(ConfigShared::SimID) + ".PH" + to_string(current_phcid) + ".xdmf";
			debugxdmf.create_phaseresults_file( xmlfn, ConfigAraullo::NumberOfSO2Directions, debugh5Hdl.h5resultsfn, current_phcid, attrnm );
		} //next candidate
	}
	*/

	double toc = MPI_Wtime();
	memsnapshot mm = isect_tictoc.get_memoryconsumption();
	isect_tictoc.prof_elpsdtime_and_mem( "WriteIntersectionResultsToH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
cout << "Rank " << get_myrank() << " wrote indexing results to H5 file in " << (toc-tic) << " seconds" << "\n";
}


void isectHdl::write_solutions_tkd_to_h5()
{
	double tic = MPI_Wtime();

	string fwslash = "/";
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";
	size_t roffset = 0;

	for( auto it = solutions.begin(); it != solutions.end(); it++ ) {
		//##MK::currently handles only a single-phase!
		dsnm = PARAPROBE_ISECT_RES_DIS + fwslash + "PH0" + fwslash + it->dsnm;
		if ( it->res.size()/MAX_TKD_TESTS == it->dat.size() && it->res.size() > 0 && dsnm != "" ) {
			vector<float> f32;
			for( size_t i = 0; i < it->dat.size(); i++ ) {
				f32.push_back( static_cast<float>(it->dat.at(i)) );
				for( size_t j = 0; j < MAX_TKD_TESTS; j++ ) {
					f32.push_back( it->res.at(MAX_TKD_TESTS*i+j) );
				}
			}
			it->dat = vector<unsigned int>();
			it->res = vector<float>();

			ifo = h5iometa( dsnm, f32.size()/PARAPROBE_ISECT_RES_TKD_NCMAX, PARAPROBE_ISECT_RES_TKD_NCMAX );
			status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
			if ( status == WRAPPED_HDF5_SUCCESS ) {
				offs = h5offsets( 0, f32.size()/PARAPROBE_ISECT_RES_TKD_NCMAX, 0, PARAPROBE_ISECT_RES_TKD_NCMAX,
										f32.size()/PARAPROBE_ISECT_RES_TKD_NCMAX, PARAPROBE_ISECT_RES_TKD_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
				if ( status == WRAPPED_HDF5_SUCCESS ) {
//cout << "Rank " << get_myrank() << " wrote SpecificPeak " << dsnm << " status " << status << "\n";
					f32 = vector<float>();
				}
				else {
					cerr << "Rank " << get_myrank() << " failed writing dsnm " << dsnm << "!" << "\n";
				}
			}
		}
		else {
			cerr << "Rank " << get_myrank() << " no results for dsnm " << dsnm << "!" << "\n";
		}
	} //next solution

	double toc = MPI_Wtime();
	memsnapshot mm = isect_tictoc.get_memoryconsumption();
	isect_tictoc.prof_elpsdtime_and_mem( "WriteCompareTKDResultsToH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
cout << "Rank " << get_myrank() << " wrote compare TKD results to H5 file in " << (toc-tic) << " seconds" << "\n";
}


int isectHdl::get_myrank()
{
	return this->myrank;
}


int isectHdl::get_nranks()
{
	return this->nranks;
}


void isectHdl::set_myrank( const int rr )
{
	this->myrank = rr;
}


void isectHdl::set_nranks( const int nn )
{
	this->nranks = nn;
}


void isectHdl::init_mpidatatypes()
{
/*
	//define MPI datatype which assists us to logically structure the communicated data packages
	MPI_Type_contiguous(4, MPI_FLOAT, &MPI_Quaternion_Type);
	MPI_Type_commit(&MPI_Quaternion_Type);

	MPI_Type_contiguous(3, MPI_FLOAT, &MPI_ElevAzimXYZ_Type);
	MPI_Type_commit(&MPI_ElevAzimXYZ_Type);

	//commit utility data types to simplify and collate data transfer operations
	//int cnts1[2] = {4, 2};
	//MPI_Aint dsplc1[2] = {0, 4 * 4}; //4 B for a float
	//MPI_Datatype otypes1[2] = {MPI_FLOAT, MPI_UNSIGNED};
	//MPI_Type_create_struct(2, cnts1, dsplc1, otypes1, &MPI_Fourier_ROI_Type);
	//MPI_Type_commit(&MPI_Fourier_ROI_Type);
*/
}

