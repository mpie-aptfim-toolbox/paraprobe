//##MK::GPLV3


#ifndef __PARAPROBE_INTERSECTOR_UTILS_H__
#define __PARAPROBE_INTERSECTOR_UTILS_H__

//#include "PARAPROBE_VolumeSampler.h"
#include "../../paraprobe-utils/src/PARAPROBE_VolumeSampler.h"

#include "PARAPROBE_IntersectorAdvCG.h"

//bool SortOIMFilterAscValue( oim_filter & first, oim_filter & second );

struct fctifo
{
	size_t start;
	size_t end;
	int clpid;
	int pad;
	fctifo() : start(0), end(0), clpid(INT32MX), pad(0) {}
	fctifo( const size_t _s, const size_t _e, const int _clpid ) : start(_s), end(_e), clpid(_clpid), pad(0) {}
};


struct roi
{
	apt_real x;
	apt_real y;
	apt_real z;
	apt_real R;

	vector<pair<double,unsigned int>> portions;

	roi() : x(0.0), y(0.0), z(0.0), R(0.0), portions(vector<pair<double,unsigned int>>()) {}
	roi( const apt_real _x, const apt_real _y, const apt_real _z, const apt_real _r ) :
		x(_x), y(_y), z(_z), R(_r), portions(vector<pair<double,unsigned int>>()) {}
};


struct tet3d64
{
	p3d64 v1;
	p3d64 v2;
	p3d64 v3;
	p3d64 v4;
	tet3d64() : v1(p3d64()), v2(p3d64()), v3(p3d64()), v4(p3d64()) {}
	tet3d64( p3d const & _v1, p3d const & _v2, p3d const & _v3, p3d const & _v4 ) :
		v1(_v1), v2(_v2), v3(_v3), v4(_v4) {}
	tet3d64( const p3d64 _v1, const p3d64 _v2, const p3d64 _v3, const p3d64 _v4 ) :
		v1(_v1), v2(_v2), v3(_v3), v4(_v4) {}
};


struct isect_res
{
	double Vin;
	unsigned int id;
	unsigned int pad;
	isect_res() : Vin(0.0), id(0), pad(0) {}
	isect_res( const double _Vin, const unsigned int _id ) : Vin(_Vin), id(_id), pad(0) {}
};


inline bool SortForDecreasingVin(const isect_res & a, const isect_res & b)
{
	return a.Vin > b.Vin;
}


class polyhedron
{
public:
	polyhedron();
	~polyhedron();

	bool tete_a_tete();
	bool tets_in_the_box();

	//piece-wise linear complex triangulation

	//geometry of a closed polyhedron can be convex or non-convex geometry described by piecewise-linear complex
	vector<pair<size_t,size_t>> fcts;	//facets referencing implicitly [start, end) on v_ids to get a particular facet
	vector<p3d> v_ounrm;				//outer unit normals
	vector<int> v_ids; 					//vertex IDs implicitly describing all facets in ##MK::winding order
	vector<p3d> v_xyz;					//vertex 3d coordinates currently ##MK::floating point accurate only!

	//space filling tessellation of polyhedron volume into tetraeder
	vector<p3d64> tet_v;				//tetraeder vertices with possible Steiner points
	vector<quad3d> tet_id;				//tetraeder referencing vertex
	vector<tet3d64> tet_xyz;				//tetraeder coordinates

	Tree* tet_bvh;						//bounding volume hierarchy of the tetraeder

	aabb3d64 box;						//bounding box to this polyhedron
	unsigned int id;					//ID/name of this polyhedron
};

#define MAX_INTERSECTING_POLYHEDRA		4

#define MAX_TKD_TESTS					8

class idxres
{
public:
	idxres();
	//idxres( const string _dsnm, const size_t _nsol );
	~idxres();

	string dsnm;
	vector<unsigned int> dat;				//##MK::currently the orientation candidates
	vector<float> res;						//##MK::currently the disorientation to ground truth,
											//##MK::currently 2d implicit MAX_INTERSECTING_POLYHEDRA*nsolutions
	double TotalVolumeROI;
	double FourStrongestOverlap[2*MAX_INTERSECTING_POLYHEDRA];			//ID strongest, overlap Vin, ID 2nd strongest, overlap Vin, ...
};


/*
struct tkd_res
{
	unsigned int mpid;
	float Comparisons[MAX_TKD_TESTS];
	tkd_res() : mpid(UINT32MX) {} //other values will get by default initialized with 0.0
};
*/

/*
//storing of timing data
struct tictoc
{
	double dt;
	unsigned int mpid;
	unsigned int phcid;
	int threadid;
	tictoc() :
		dt(0.0), mpid(UINT32MX), phcid(UINT32MX), threadid(INT32MX) {}
	tictoc( const double _dt, const unsigned int _mpid, const unsigned int _phcid, const int _thrid ) :
		dt(_dt), mpid(_mpid), phcid(_phcid), threadid(_thrid) {}
};
*/


#endif
