//##MK::GPLV3

#ifndef __PARAPROBE_CONFIG_INTERSECTOR_H__
#define __PARAPROBE_CONFIG_INTERSECTOR_H__

#include "../../paraprobe-utils/src/CONFIG_Shared.h"
#include "../../paraprobe-utils/src/PARAPROBE_FFT.h"


enum E_ANALYSIS_MODE {
	SPHERES_POLYHEDRA_INTERSECTIONS,
	COMPARE_WITH_TKD_RESULTS
};


class ConfigIntersector
{
public:
	
	static E_ANALYSIS_MODE AnalysisMode;
	static string InputfilePoints;
	static string DatasetPoints;
	static string InputfilePolyhedra;
	static string DatasetPolyhedra;
	static string InputfilePolyhedraOris;
	static string DatasetPolyhedraOris;
	
	static string InputfileSolutions;
	static string DatasetSolutions;
	static string InputfileOris;
	static string DatasetOris;

	static string TetGenFlags;

	static unsigned int DatasetSolutionsMin;
	static unsigned int DatasetSolutionsIncr;
	static unsigned int DatasetSolutionsMax;

	static bool IOVisualizeTetrahedra;

	static bool readXML( string filename = "" );
	static bool checkUserInput();
	static void reportSettings( vector<pparm> & res );
};

#endif
