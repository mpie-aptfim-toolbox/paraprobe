//##MK::GPLV3


#ifndef __PARAPROBE_INTERSECTOR_ADVCG_H__
#define __PARAPROBE_INTERSECTOR_ADVCG_H__

#include "PARAPROBE_IntersectorTicToc.h"

//use Eigen library
#include "thirdparty/Eigen/Core"
#include "thirdparty/Eigen/Geometry"
#include "thirdparty/Eigen/Dense"


//use tetgen, specify custom location
#include "thirdparty/TetGen/tetgen1.5.1/tetgen.h"

//use the continuum scale code to compute analytical exact overlap of geometrical primitives by S. Strobl et al.
#include "thirdparty/OverlapStrobl/overlap-master/overlap.hpp"


#endif
