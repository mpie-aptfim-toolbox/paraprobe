//##MK::GPLV3

#ifndef __PARAPROBE_INTERSECTOR_HDF_H__
#define __PARAPROBE_INTERSECTOR_HDF_H__

//shared headers
#include "../../paraprobe-utils/src/PARAPROBE_APTH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_UtilsMetadataDefsH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_SyntheticMetadataDefsH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_AraulloMetadataDefsH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_IntersectionMetadataDefsH5.h"
#include "../../paraprobe-utils/src/PARAPROBE_XDMF.h"

//tool-specific headers
//#include "CONFIG_Intersector.h"
#include "PARAPROBE_IntersectorCiteMe.h"


class intersector_h5 : public h5Hdl
{
	//tool-specific sub-class of a HDF5 inheriting all methods of the base class h5Hdl
	//coordinating instance handling all (sequential) writing to HDF5 file wrapping HDF5 low-level C library calls

public:
	intersector_h5();
	~intersector_h5();

	int create_isect_apth5( const string h5fn );


//private:
};


#endif
