# -*- coding: utf-8 -*-
"""
created 2020/05/26, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for creating XML and shell config files for paraprobe-transcoder
"""
import sys
import glob
from pathlib import Path
import numpy as np

from PARAPROBE_XML import *
#import PARAPROBE_Shell.py

#we should inform on particular input data. We use a dictionary for this
config = { 'Healthy': True,
           'InputfilePSE': 'PARAPROBE.PeriodicTableOfElements.xml',
           'TranscodingMode': 0,
           'Inputfile': '' }

#we should inform on particular config keys, does not need inform for every config key
info = { 'TranscodingMode': '0 nothing, 1 pos2hdf, 2 epos2hdf, 3 apt2hdf' }

class paraprobe_transcoder():
    
    def __init__(self, simid, fnm, *args, **kwargs):
        self.simid = simid
        self.toolname = 'Transcoder'
        self.recon_filename = 'PARAPROBE.Transcoder.Results.SimID.' + str(np.uint32(self.simid)) + '.h5' #'.apth5'
        self.config = config
        self.help = info
        #check if fnm is a supported format     
        if fnm.find('.') < 0:
            print('Filename need to have an ending. Files need to be of one of these types (POS/EPOS/APT/pos/epos/apt)')
            self.config['Healthy'] = False
            return
        if not fnm.endswith('.pos') and not fnm.endswith('.POS') and not fnm.endswith('.epos') and not fnm.endswith('.EPOS') and not fnm.endswith('.apt') and not fnm.endswith('.APT'):
            print('Filename need to have an ending. Files need to be of one of these types (POS/EPOS/APT/pos/epos/apt)')
            self.config['Healthy'] = False
            return

        self.config['Inputfile'] = fnm              
        if fnm.endswith('.pos') or fnm.endswith('.POS'):
            self.config['TranscodingMode'] = 1
        if fnm.endswith('.epos') or fnm.endswith('.EPOS'):
            self.config['TranscodingMode'] = 2
        if fnm.endswith('.apt') or fnm.endswith('.APT'):
            self.config['TranscodingMode'] = 3
    
    def get_recon_filename(self):
        return self.recon_filename    
    
    def run(self, nmpi = 1): #*args, **kwargs):
        #let XML generator class create the XML file
        #needs an XML top-level group name, here 'ConfigTranscoder' because we want sth specific for transcoder tool
        #and a dictionary with the settings
        self.xml = paraprobe_xml( 'Transcoder', self.simid, 'ConfigTranscoder', self.config, self.help )
        #sh = 'paraprobe_bash()'
        #return xml, sh

        #create the specific bash command to run this analysis
        if nmpi >= 1:
            sid = str(np.uint32(self.simid))
            prefix = 'PARAPROBE.Transcoder.SimID.' + sid
            execnm = 'paraprobe_transcoder' #'./paraprobe_transcoder'
            #sh = 'mpiexec -n ' + str(np.uint32(nmpi)) + ' ' + execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            sh = execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            return sh
        else:
            raise ValueError('We need to use at least one process when working with the Message Passing Interface !')

# =============================================================================
# exit  
# #example usage
# #import a reconstruction from IVAS via a POS file by transcoding it into HDF5 which paraprobe uses
# task = paraprobe_transcoder( 1, 'R57664.pos' )
# cmd = task.run( 1 )
# =============================================================================
