# -*- coding: utf-8 -*-
"""
created 2020/06/06, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for creating XML and shell config files for paraprobe-dbscan
"""

import glob
from pathlib import Path
import numpy as np

from PARAPROBE_XML import *
#import PARAPROBE_Shell.py

config = { 'Healthy': True,
           'InputfilePSE': 'PARAPROBE.PeriodicTableOfElements.xml',
           'InputfileReconstruction': '', 
           'InputfileHullAndDistances': '',
           'ClusteringMethod': 2,
           'MaskingMethod': 0,
           'DBScanEpsilonMin': 0.25,
           'DBScanEpsilonIncr': 0.25,
           'DBScanEpsilonMax': 0.25,
           'DBScanMinPtsMin': 1,
           'DBScanMinPtsIncr': 1,
           'DBScanMinPtsMax': 1,
           'MaxSepNumberOfIonsMin': 5,
           'MaxSepNumberOfIonsIncr': 10,
           'MaxSepNumberOfIonsMax': 5,
           'DatasetEdgeThresholdDistance': 1.0,
           'IOStoreClusterIDs': 0,
           'IOStoreClusters': 1  }
           
#we should inform on particular config keys, does not need inform for every config key
info = { 'ClusteringMethod': '0 none, 1 original DBScan, 2 maximum separation method', 
         'MaskingMethod': 'only 0, currently working on entire dataset',
         'DBScanEpsilonMax': 'in nm, epsilon radius about target ion, eqv to dmax',
         'DBScanMinPtsMax': 'integer, MSM method is a DBScan with kth = K = 1 see p1663 of https://doi.org/10.1017/S1431927614013294',
         'MaxSepNumberOfIonsMax': 'minimum number of points to consider a cluster a significant one',
         'DatasetEdgeThresholdDistance': 'in nm, mark all clusters with ions that are closer than this distance as boundary contact',
         'IOStoreClusterIDs': 'inactive for 0, active for all other val, memory costly data for visualizing all ions of a cluster',
         'IOStoreClusters': 'leave 1, gives summarized cumulative cluster size distribution'   }

class paraprobe_dbscan():

    def __init__(self, simid, recon, hull, *args, **kwargs):
        self.simid = simid
        self.toolname = 'DBScan'
        self.config = config
        self.help = info
        self.targets = []
        
        if not recon.endswith('.h5'): #'.apth5'):
            raise ValueError('The filename pointing to the reconstruction is not an H5 file !')
        else:
            self.config['InputfileReconstruction'] = recon
        if not hull.endswith('.h5'):
            raise ValueError('The filename pointing to the dataset edge triangulation and distances is not an HDF5 file !')
        else:
            self.config['InputfileHullAndDistances'] = hull
    
    def set_high_throughput_maximum_separation_method(self, epsbnds, minptsbnds, ion2edge_dist ):
        self.config['ClusteringMethod'] = np.uint32(2)
        if np.shape(epsbnds) == (3,):
            if epsbnds[0] >= 0.0 and epsbnds[1] > 0.0 and epsbnds[2] >= epsbnds[0]:
                self.config['DBScanEpsilonMin'] = np.around(np.float32(epsbnds[0]), decimals=3)
                self.config['DBScanEpsilonIncr'] = np.around(np.float32(epsbnds[1]), decimals=3)
                self.config['DBScanEpsilonMax'] = np.around(np.float32(epsbnds[2]), decimals=3)
            else:
                raise ValueError('Epsilon min must be at least 0.0, increment > 0.0, and max >= min !')
        else:
            raise ValueError('epsbnds is should be a (3,) numpy array!')
        #maximum separation is a DBScan with kth nearest neighbor kth = 1
        self.config['DBScanMinPtsMin'] = np.uint32(1)
        self.config['DBScanMinPtsIncr'] = np.uint32(1)
        self.config['DBScanMinPtsMax'] = np.uint32(1)
        if np.shape(minptsbnds) == (3,):
            if minptsbnds[0] >= 1 and minptsbnds[1] >= 1 and minptsbnds[2] >= minptsbnds[0]:
                self.config['MaxSepNumberOfIonsMin'] = np.uint32(minptsbnds[0])
                self.config['MaxSepNumberOfIonsIncr'] = np.uint32(minptsbnds[1])
                self.config['MaxSepNumberOfIonsMax'] = np.uint32(minptsbnds[2])
            else:
                raise ValueError('minpts min must be at least 1, increment > 1, and max >= min !')
        else:
            raise ValueError('minptsbnds is should be a (3,) numpy array!')
        if ion2edge_dist >= 0.0:
            self.config['DatasetEdgeThresholdDistance'] = np.around(np.float32(ion2edge_dist), decimals=3)
        else:
            raise ValueError('Ion2Edge_dist needs to be at least 0.0 !')
        self.config['IOStoreClusterIDs'] = np.uint32(0)
        self.config['IOStoreClusters'] = np.uint32(1)        
 
    def add_targets(self, trg ):
        if not trg == '':
            #add more checks to it using Python periodic table
            self.targets.append( trg )
        else:
            raise ValueError('The target iontype keys are incorrect !')
    
    def run(self, nmpi = 1): #*args, **kwargs):
        #let XML generator class create the XML file
        #needs an XML top-level group name, here 'ConfigSurfacer' because we want sth specific for surfacer tool
        #and a dictionary with the settings
        self.xml = paraprobe_xml( 'DBScan', self.simid, 'ConfigDBScan', self.config, self.help, targets = self.targets )
        #sh = 'paraprobe_bash()'
        
        #create the specific bash command to run this analysis
        if nmpi >= 1:
            sid = str(np.uint32(self.simid))
            prefix = 'PARAPROBE.DBScan.SimID.' + sid
            execnm = 'paraprobe_dbscan' #'./paraprobe_dbscan'
            #sh = 'mpiexec -n ' + str(np.uint32(nmpi)) + ' ' + 
            sh = execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            return sh
        else:
            raise ValueError('We need to use at least one process when working with the Message Passing Interface !')     

# =============================================================================
# exit
# #example usage
# #take your reconstruction and computed distances to perform high-throughput maximum separation method runs
#task = paraprobe_dbscan( 1, 'PARAPROBE.Transcoder.Results.SimID.1.apth5', 'PARAPROBE.Surfacer.Results.SimID.1.h5' )
# 
# #clustering for Sc ions in a dataset with e.g. Al3Sc precipitates, mark all clusters with Sc closer than 1.0 nm to dataset edge as boundary contact
#task.set_high_throughput_maximum_separation_method( np.array([0.25, 0.25, 0.25]), np.array([5, 10, 5]), 1.0 )
#task.add_targets( 'Sc' )

#and create the XML configuration file and the command line entry for running spatstat using mpi and openmp
# ##MK::give dependencies to identify what the file needs to build workflows automatically, for now manual scripting
# #three equivalent version to instruct running with one process
#cmd1a = task.run()
# cmd1b = task.run( 1 )
# cmd1c = task.run( nmpi = 1 )
# 
# #alternatively, run with two process
# cmd2 = task.run( 2 )
# =============================================================================
