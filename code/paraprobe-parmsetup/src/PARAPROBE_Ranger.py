# -*- coding: utf-8 -*-
"""
created 2020/05/26, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for creating XML and shell config files for paraprobe-ranger
"""
import sys
import glob
from pathlib import Path
import numpy as np

from PARAPROBE_XML import *
#import PARAPROBE_Shell.py

config = { 'Healthy': True,
           'RangingMode': 0,
           'InputfilePSE': 'PARAPROBE.PeriodicTableOfElements.xml',
           'InputfileReconstruction': '',
           'InputfileRangingData': '',
           'PeakSearchMaximumNumberOfComponents': 3,
           'PeakSearchMaximumCharge': 4 }

#we should inform on particular config keys, does not need inform for every config key
info = { 'RangingMode': '0 use existent rangefile via h5, 1 define list of mass-to-charge intervals and check peaks in there-->',
         'PeakSearchMaximumNumberOfComponents': 'what is the maximum number of (not necessarily disjoint) nuclids building the (molecular) ions we are searching',
         'PeakSearchMaximumCharge': 'what is the maximum charge of the (molecular) ions we are searching' }

EPSILON = 1.0e-6

class paraprobe_ranger():

    def __init__(self, simid, recon, ranging, *args, **kwargs):
        self.simid = simid
        self.toolname = 'Ranger'
        self.config = config
        self.help = info
        self.ivals = []

        if not recon.endswith('.h5'): #('.apth5'):
            raise ValueError('The filename pointing to the reconstruction is not an H5 file !')
        else:
            self.config['InputfileReconstruction'] = recon
        #if not ranging.endswith('.rrng') and not ranging.endswith('.RRNG') and not ranging.endswith('.rng') and not ranging.endswith('.RNG'):
        if ranging.endswith('.h5') == True or ranging.endswith('.H5') == True:
            self.config['InputfileRangingData'] = ranging
        else:
            raise ValueError('The filename pointing to ranging data is not an .h5 file !') #'either an RRNG or RNG file !')

    def find_molecular_ions(self, nnuclids, ncharge, arr ):
        """
        nnuclids : uint32, maximum number of components per molecular ion
        ncharge : uint32, maximum multiples of e charge per molecular ion
        arr : e.g. pairs of mass-to-charge interval bounds np.array([[0.001, 1000.0, ''], [0.001, 1000.0, 'Mg,O:,H:']])
        """
        self.config['RangingMode'] = 1
        if nnuclids >= 1 and nnuclids <= 22:
            self.config['PeakSearchMaximumNumberOfComponents'] = np.uint32(nnuclids)
        else:
            raise ValueError('nnuclids needs to be an integer on [1, 22] !')
        if ncharge >= 1 and ncharge <= 7:
            self.config['PeakSearchMaximumCharge'] = np.uint32(ncharge)
        else:
            raise ValueError('ncharge needs to be an integer on [1, 7] !')
        #arr = np.array([[10.0, 12.0], [30.0, 32.0]])
        #default mass-to-charge array arr = np.array([[0.001, 1000.0]])
        self.ivals = []
        if not arr == []:
            for iv in arr:
                if iv[0] < EPSILON or iv[1] < EPSILON:
                    raise ValueError( 'Mass-to-charge intervals are defined by positive and non-zero values !')
                    continue
                if (iv[1] - iv[0]) < EPSILON:
                    raise ValueError( 'Mass-to-charge interval, the right bound needs to have a higher value than the left bound !')
                    continue
                self.ivals.append( [iv[0], iv[1], iv[2]] )
        else:
            self.ivals = [[0.001, 1000.0, '']]

    def run(self, nmpi = 1): #*args, **kwargs):
        #let XML generator class create the XML file
        #needs an XML top-level group name, here 'ConfigRanger' because we want sth specific for ranger tool
        #and a dictionary with the settings
        self.xml = paraprobe_xml( 'Ranger', self.simid, 'ConfigRanger', self.config, self.help, ivals = self.ivals )
        #sh = 'paraprobe_bash()'
        #return xml, sh

        #create the specific bash command to run this analysis
        if nmpi >= 1:
            sid = str(np.uint32(self.simid))
            prefix = 'PARAPROBE.Ranger.SimID.' + sid
            execnm = 'paraprobe_ranger' #'./paraprobe_ranger'
            #sh = 'mpiexec -n ' + str(np.uint32(nmpi)) + ' ' + execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            sh = execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            return sh
        else:
            raise ValueError('We need to use at least one process when working with the Message Passing Interface !')

# =============================================================================
# exit
# #example usage
# #import a ranging from IVAS and the transcoded reconstruction (dataset)
# task = paraprobe_ranger( 1, 'PARAPROBE.Transcoder.Results.SimID.1.h5', 'R6745.h5' )
# finding peaks on default interval like so
# task.find_molecular_ions( 3, 4, [] )
# finding peaks on specific intervals allowing all elements like so and only specific elements like so
# task.find_molecular_ions( 3, 4, [[0.001, 1000.0, ''], [0.001, 1000.0, 'Mg,O:,H:']] )
# cmd = task.run( 1 )
# =============================================================================
