# -*- coding: utf-8 -*-
"""
created 2020/05/31, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for creating XML and shell config files for paraprobe-fourier
"""

import glob
from pathlib import Path
import numpy as np

from PARAPROBE_XML import *
#import PARAPROBE_Shell.py

config = { 'Healthy': True,
           'InputfilePSE': 'PARAPROBE.PeriodicTableOfElements.xml',
           'InputfileReconstruction': '', 
           'InputfileHullAndDistances': '',
           'VolumeSamplingMethod': 3,
           'ROIRadiusMax': 2.0,
           'SamplingGridBinWidthX': 4.0,
           'SamplingGridBinWidthY': 4.0,
           'SamplingGridBinWidthZ': 4.0,
           'SamplingPosition': '0.0;0.0;0.0',
           'FixedNumberOfROIs': 10,
           'RemoveROIsProtrudingOutside': 1,
           'HKLGridBoundsHMin': -1.0,
           'HKLGridBoundsHMax': +1.0,
           'HKLGridHVoxelResolution': 64,
           'IOStoreFullGridPeaks': 1,
           'IOStoreSignificantPeaks': 0,
           'IODBScanOnSignificantPeaks': 0,
           'SignificantPeakIntensityThreshold': 0.01,
           'DBScanSearchRadius': 0.01,
           'DBScanMinPksForReflector': 1,
           'GPUsPerComputingNode': 1,
           'GPUWorkload': 1  }

#we should inform on particular config keys, does not need inform for every config key
info = { 'VolumeSamplingMethod': '0 3D regular ROI grid, 1 single ROI at user-defined position, 2 single ROI in dataset center, 3 random ROIs',
         'IOStoreFullGridPeaks': 'all IO options are active when 1, else inactive',
         'IOStoreSignificantPeaks': 'only voxels with intensity above SignificantPeakIntensityThreshold',
         'DBScanSearchRadius': '3**0.5/2*1/HKLGridHVoxelResolution',
         'GPUsPerComputingNode': 'needs to match with number of MPI processes per computing node!, each MPI process controls one GPU' }

class paraprobe_fourier():

    def __init__(self, simid, recon, hull, *args, **kwargs):
        self.simid = simid
        self.toolname = 'Fourier'
        self.config = config
        self.help = info
        self.candidates = []
        
        if not recon.endswith('.h5'): #'apth5'):
            raise ValueError('The filename pointing to the reconstruction is not an H5 file !')
        else:
            self.config['InputfileReconstruction'] = recon
        if not hull.endswith('.h5'):
            raise ValueError('The filename pointing to the dataset edge triangulation and distances is not an HDF5 file !')
        else:
            self.config['InputfileHullAndDistances'] = hull
    
    def set_roi_rmax(self, rmax):
        if rmax >= 0.5:
            self.config['ROIRadiusMax'] = np.around(np.float32(rmax), decimals=3)
        else:
            raise ValueError('The radius of the ROIs should be at least 0.5nm to include some ions !')
            
    def set_roi_3dgrid(self, dx, dy, dz):
        self.config['VolumeSamplingMethod'] = 0
        if dx >= 0.1 and dy >= 0.1 and dz >= 0.1:
            self.config['SamplingGridBinWidthX'] = dx
            self.config['SamplingGridBinWidthY'] = dy
            self.config['SamplingGridBinWidthZ'] = dz
        else:
            raise ValueError('All grid spacings need to be positive and at least 0.1nm !')            
    
    def set_roi_single_here(self, x, y, z):
        self.config['VolumeSamplingMethod'] = 1
        self.config['SamplingPosition'] = str(np.around(np.float32(x), decimals=3)) + ';' + str(np.around(np.float32(y), decimals=3)) + ';' + str(np.around(np.float32(z), decimals=3))
    
    def set_roi_single_center(self):
        self.config['VolumeSamplingMethod'] = 2
        
    def set_roi_random(self, N):
        if N >= 1 and N < 2**32-1:
            self.config['VolumeSamplingMethod'] = 3
            self.config['FixedNumberOfROIs'] = np.uint32(N)
        else:
            raise ValueError('For random sampling of ROIs we the number of ROIs needs to be at least 1 and at most ' + str(2*32-1) + ' !')
    
    def set_hklgrid_resolution(self, nvxl):
        if nvxl >= 32 and nvxl <= 1024:
            self.config['HKLGridHVoxelResolution'] = np.uint32(nvxl)
        else:
            raise ValueError('HKLGridHVoxelResolution of current implementation of paraprobe-fourier demands 32 <= nvxl <= 1024 !')

    def set_characterize_crystallinity(self, threshold):
        self.config['IOStoreFullGridPeaks'] = 0
        self.config['IOStoreSignificantPeaks'] = 1
        self.config['IODBScanOnSignificantPeaks'] = 0
        if threshold >= 0.0:
            self.config['SignificantPeakIntensityThreshold'] = np.around(np.float32(threshold), decimals=3)
        else:
            raise ValueError('SignificantPeakIntensityThreshold needs to be positive or zero !')
        self.config['DBScanSearchRadius'] = (3**0.5)/2 * 1.0/self.config['HKLGridHVoxelResolution'] #check if BoundsHMin and BoundsHMax are not +-1.0
        self.config['DBScanMinPksForReflector'] = 1
    
    def set_prepare_indexing(self, threshold, epsVxl, minPts):
        self.config['IOStoreFullGridPeaks'] = 0
        self.config['IOStoreSignificantPeaks'] = 1
        self.config['IODBScanOnSignificantPeaks'] = 1
        if threshold > EPSILON:
            self.config['SignificantPeakIntensityThreshold'] = np.around(np.float32(threshold), decimals=3)
        else:
            raise ValueError('SignificantPeakIntensityThreshold needs to be positive and finite !')
        if epsVxl >= (1.0-EPSILON) and epsVxl <= (self.config['HKLGridHVoxelResolution']-1)/2:
            self.config['DBScanSearchRadius'] = np.around(np.float32((3**0.5)/2 * epsVxl/self.config['HKLGridHVoxelResolution']), decimals=4) #check if BoundsHMin and BoundsHMax are not +-1.0
        else:
            raise ValueError('DBScanSearchRadius via epsVxl needs to be 1.0 <= epsVxl <= (HKLGridHVoxelResolution-1)/2  !')
        if minPts >= 1:
            self.config['DBScanMinPksForReflector'] = np.uint32(minPts)
        else:
            raise ValueError('DBScanMinPksForReflector via minPts needs to be at least 1 !')
            
    def add_candidate(self, trg, rpos):
        if not trg == '' and rpos > 0.0:
            #add more checks to it using Python periodic table
            ccmb = '<entry targets="' + trg + '" realspace="' + str(np.around(rpos, decimals=4)) + '"/>'
            self.candidates.append( ccmb )
        else:
            raise ValueError('Either the target is empty, or 0.0 < rpos does not hold !')
    
    def run(self, nmpi = 1): #*args, **kwargs):
        #let XML generator class create the XML file
        #needs an XML top-level group name, here 'ConfigSurfacer' because we want sth specific for surfacer tool
        #and a dictionary with the settings
        self.xml = paraprobe_xml( 'Fourier', self.simid, 'ConfigFourier', self.config, self.help, candidates = self.candidates )
        #sh = 'paraprobe_bash()'
        
        #create the specific bash command to run this analysis
        if nmpi >= 1:
            sid = str(np.uint32(self.simid))
            prefix = 'PARAPROBE.Fourier.SimID.' + sid
            execnm = 'paraprobe_fourier'
            #sh = 'mpiexec -n ' + str(np.uint32(nmpi)) + ' ' + execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            sh = execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            return sh
        else:
            raise ValueError('We need to use at least one process when working with the Message Passing Interface !')     

# =============================================================================
# exit
# #example usage
# #take your reconstruction and computed distances to characterize where crystallographic signal 
#for a particular crystal structure candidate is at all strongcompute spatial statistics
#task = paraprobe_fourier( 1, 'PARAPROBE.Transcoder.Results.SimID.1.apth5', 'PARAPROBE.Surfacer.Results.SimID.1.h5' )
#use a 10 random ROIs with 2.0nm and 64 voxel to sample reciprocal space on [-pi,+pi]
#task.set_roi_random( 10 )
#task.set_roi_rmax( 2.0 )
#task.set_hklgrid_resolution( 64 )
#characterize crystallinity
#task.set_characterize_crystallinity( 0.0 )
# ##MK::implement check for spatial resolution of inter-plane distances based on roi_rmax and m
#assume ideal fcc Al lattice as candidate crystal structure
#task.add_candidate( 'Al', 0.405 )
#create XML file to be used for executing paraprobe-fourier
#cmd1a = task.run()
# cmd1b = task.run( 1 )
# cmd1c = task.run( nmpi = 1 )
# #alternatively, run with two process
# cmd2 = task.run( 2 )
# =============================================================================
