# -*- coding: utf-8 -*-
"""
created 2020/05/26, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for creating XML and shell config files for paraprobe-surfacer
"""
import sys
import glob
from pathlib import Path
import numpy as np

from PARAPROBE_XML import *
#import PARAPROBE_Shell.py

config = { 'Healthy': True,
           'InputfilePSE': 'PARAPROBE.PeriodicTableOfElements.xml',
           'InputfileReconstruction': '', 
           'SurfacingMode': 1,
           'AdvIonPruneBinWidthMin': 1.0,
           'AdvIonPruneBinWidthIncr': 1.0,
           'AdvIonPruneBinWidthMax': 1.0,
           'AlphaShapeAlphaValue': 0,
           'DistancingMode': 2,
           'AdvDistanceBinWidthMin':  1.0,
           'AdvDistanceBinWidthIncr': 1.0,
           'AdvDistanceBinWidthMax':  1.0,
           'DistancingRadiusMax': 5.0,
           'RequeryThreshold': 0.7  }

#we should inform on particular config keys, does not need inform for every config key
info = { 'SurfacingMode': '0 nothing, 1 alpha shape', 
         'AlphaShapeAlphaValue': '0 smallest solid, 1 cgal considered optimal',
         'DistancingMode': '0 nothing, 1 complete, 2 skin within DistancingRadiusMax'  }

class paraprobe_surfacer():

    def __init__(self, simid, recon, *args, **kwargs):
        self.simid = simid
        self.toolname = 'Surfacer'
        self.config = config
        self.help = info
        
        if not recon.endswith('.h5'): #'.apth5'):
            raise ValueError('The filename pointing to the reconstruction is not an H5 file !')
        else:
            self.config['InputfileReconstruction'] = recon

    def set_roi_rmax(self, val):
        if val >= 0.5:
            self.config['DistancingRadiusMax'] = np.around(np.float32(val), decimals=3)
        else:
            raise ValueError('DistancingRadiusMax needs to be >= 0.5 !')

    def set_distancing_nothing(self):
        self.config['DistancingMode'] = 0

    def set_distancing_complete(self):
        self.config['DistancingMode'] = 1
        
    def set_distancing_skin(self, dskin ):
        if dskin >= 1.0:
            self.config['DistancingMode'] = 2
            self.config['DistancingRadiusMax'] = np.around(np.float32(dskin), decimals=3)
        else:
            raise ValueError('dskin needs to be at least 1.0nm !')        
        
    def get_hull_filename(self):
        return 'PARAPROBE.Surfacer.Results.SimID.' + str(np.uint32(self.simid)) + '.h5' #MK::paraprobe-surfacer renames the files automatically
   
    def run(self, nmpi = 1): #*args, **kwargs):
        #let XML generator class create the XML file
        #needs an XML top-level group name, here 'ConfigSurfacer' because we want sth specific for surfacer tool
        #and a dictionary with the settings
        self.xml = paraprobe_xml( 'Surfacer', self.simid, 'ConfigSurfacer', self.config, self.help )
        #sh = 'paraprobe_bash()'
        #return xml, sh

        #create the specific bash command to run this analysis
        if nmpi >= 1:
            sid = str(np.uint32(self.simid))
            prefix = 'PARAPROBE.Surfacer.SimID.' + sid
            execnm = 'paraprobe_surfacer' #'./paraprobe_surfacer'
            #sh = 'mpiexec -n ' + str(np.uint32(nmpi)) + ' ' + execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            sh = execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            return sh
        else:
            raise ValueError('We need to use at least one process when working with the Message Passing Interface !')

# =============================================================================
# exit
# #example usage 
# #take your reconstruction build an alpha shape from it and compute for all ions the distances to the alpha shape
# task = paraprobe_surfacer( 'PARAPROBE.Transcoder.Results.SimID.1.apth5' )
# task.set_roi_rmax( 10.0 )
# task.set_distancing_complete()
# xml = task.configure( 1 )
# cmd = task.run( 1 )
# =============================================================================
