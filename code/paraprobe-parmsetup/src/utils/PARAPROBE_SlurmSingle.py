# -*- coding: utf-8 -*-
"""
created 2020/05/26, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for creating Slurm scripts for computer cluster like TALOS
"""

import glob
from pathlib import Path
import numpy as np

class paraprobe_slurm_single():
    
    def __init__(self, usernm, simid, machine, nmpi, taskdict, compiler, *args, **kwargs ):
        #print(str(nmpi))
        if nmpi < 1:
            raise ValueError('NumberOfProcesses needs to be >= 1 !')
        #only specific for TALOS
        if machine == 'TALOS':
            if nmpi > 80:
                raise ValueError('NumberOfProcesses maximal on TALOS is ' + str(np.uint32(nmpi)) + ' !')            
        self.simid = str(np.uint32(simid))
        self.filename = 'SLURM.PARAPROBE.Workflow.SimID.' + self.simid + '.sh'
        self.cmp = ''
        self.xml = ''
        self.stdout = 'SLURM.PARAPROBE.Workflow.SimID.' + self.simid + '.STDOUT.txt'
        self.stderr = 'SLURM.PARAPROBE.Workflow.SimID.' + self.simid + '.STDERR.txt'
        self.wkdir = './'
        self.jbnm = usernm #self.simid + 'paraprobe' #'paraprobe' + self.simid
        
        ##this example is specific for TALOS only
        #################################################
        #queing settings, how many cores, how many processes and threads, how much time, which queue?
        self.partition = '--partition=p.talos'
        self.nodes = '--nodes=' + str(np.uint32(nmpi))
        self.ntasks = '--ntasks-per-node=1' #how many mpi processes, 1 for normal jobs, 2 for GPU jobs to delegate work per V100
        self.ncpus = '--cpus-per-task=40' #TALOS how many threads per process, no hyperthreading!
        #self.ngpus = '--gres=gpu:2' #do not use GPUs
        #self.mail = '--mail-user=m.kuehbach@mpie.de'
        self.time = '--time=24:00:00' #set a time limit
        
        #which module environment to use?
        self.modls = ''
        if machine == 'MPI30':
            if compiler == 'ITL':
                self.modls += 'module load cmake' + '\n'
                self.modls += 'module load intel' + '\n'
                self.modls += 'module load impi' + '\n'
                #self.modls += 'module load mkl' + '\n'
                #self.modls += 'module load boost' + '\n'
            if compiler == 'GNU':
                self.modls += 'module load cmake' + '\n'
                self.modls += 'module load gcc' + '\n'
                self.modls += 'module load mpich' + '\n'
                #self.modls += 'module load mkl' + '\n'
                #self.modls += 'module load boost' + '\n'
            if compiler == 'PGI':
                self.modls += 'module load cmake/3.10' + '\n'
                self.modls += 'module load cuda/10.0' + '\n'
                self.modls += 'module load pgi/19' + '\n'
                self.modls += 'module load impi/2018.4' + '\n'
        if machine == 'TALOS':
            if compiler == 'ITL':
                self.modls += 'module load cmake' + '\n'
                self.modls += 'module load intel' + '\n'
                self.modls += 'module load impi' + '\n'
                #self.modls += 'module load mkl' + '\n'
                #self.modls += 'module load boost' + '\n'
            if compiler == 'GNU':
                self.modls += 'module load cmake' + '\n'
                self.modls += 'module load gcc' + '\n'
                self.modls += 'module load impi' + '\n'
                #self.modls += 'module load mkl' + '\n'
                #self.modls += 'module load boost' + '\n'
            #if compiler == 'PGI':
            #    ########MK check for TALOS
            #    ########
        self.modls += 'module list' + '\n'
        self.modls += 'if [ ! -z $SLURM_CPUS_PER_TASK ] ; then' + '\n'
        self.modls += '	export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK' + '\n'
        self.modls += 'else' + '\n'
        self.modls += '	export OMP_NUM_THREADS=1' + '\n'
        self.modls += 'fi' + '\n'
        self.modls += 'export OMP_PLACES=cores' + '\n'
        #self.modls += 'export OMP_NESTED=true'
        #self.modls += 'export MKL_NUM_THREADS=1'
        self.modls += 'echo "OpenMP NUM THREADS env option"' + '\n'
        self.modls += 'echo $OMP_NUM_THREADS' + '\n'
        #self.modls += 'echo "OpenMP NESTED env option"'
        #self.modls += 'echo $OMP_NESTED'
        self.modls += 'echo "OpenMP PLACES env option"' + '\n'
        self.modls += 'echo $OMP_PLACES' + '\n'
        if compiler == 'PGI':
            self.modls += 'export CUDA_VISIBLE_DEVICES=0,1' + '\n'
            self.modls += 'echo $CUDA_VISIBLE_DEVICES' + '\n'
            self.modls += 'echo $PGI_CURR_CUDA_HOME' + '\n'
            self.modls += 'echo $PGI' + '\n'
        
        #echo "MKL NUM THREADS env option"
        #echo $MKL_NUM_THREADS + '\n'
        #export LIBRARY_PATH=/mpcdf/soft/SLE_15/packages/x86_64/intel_parallel_studio/2018.4/mkl/lib/intel64_lin/
        #echo $LIBRARY_PATH
        #echo "CUDA VISIBLE DEVICES env option"
        #export CUDA_VISIBLE_DEVICES=0,1
        #echo $CUDA_VISIBLE_DEVICES
        #echo "PGI CURR CUDA env option"
        ####for pgi/18
        ##export PGI_CURR_CUDA_HOME=/talos/u/system/soft/SLE_15/packages/skylake/pgi/18.10/linux86-64/2018/cuda/10.0
        ##echo $PGI_CURR_CUDA_HOME
        ##echo "PGI home folder"
        ##export PGI=/talos/u/system/soft/SLE_15/packages/skylake/pgi/18.10
        ##echo $PGI
        ##for pgi/19
        ##export PGI_CURR_CUDA_HOME=/talos/u/system/soft/SLE_15/packages/skylake/pgi/linux86-64-llvm/2019/cuda
        ##echo $PGI_CURR_CUDA_HOME
        ##echo "PGI home folder"
        ##export PGI=/talos/u/system/soft/SLE_15/packages/skylake/pgi/19.3
        ##echo $PGI
        self.modls += 'ulimit -s unlimited' + '\n'
        self.modls += 'echo "ulimit env option"' + '\n'
        self.modls += 'ulimit' + '\n'
        #list allocated TALOS resources nodes
        self.modls += 'echo $SLURM_JOB_NODELIST' + '\n'
        ##echo $SLURM_NODELIST
        self.modls += 'echo $SLURM_JOB_NUM_NODES' + '\n'
        ##echo $SLURM_NNODES
        self.modls += 'echo "job $SLURM_JOB_NAME with job id $SLURM_JOB_ID is running on $SLURM_JOB_NUM_NODES node(s): $SLURM_JOB_NODELIST"' + '\n'
        ##end of TALOS specific stuff
        
        ##describe the workflow, i.e. which commands to execute one after another?
        self.what = ''
        for key in taskdict.keys():
            self.what += 'srun' + ' ' + taskdict[key] + '\n'
            #print('__' + str(key) + '__' + taskdict[key] + '__') #+ '\n')

    def set_simid(self, val):
        self.simid = val
        
    def set_timeplan(self, val):
        self.time = val;
    
    def set_mpiranks(self, val):
        self.ntasks = '--ntasks-per-node=' + str(val)
        
    def set_ompthreads(self, val):
        self.ncpus = '--cpus-per-task=' + str(val) #TALOS how many threads per process
    
    def set_gpus(self, val):
        #for TALOS specific
        if val == 0:
            self.ngpus = ''
        else:
            if val >= 1 and val <= 2:
                self.ngpus = '--gres=gpu:' + str(np.uint32(val))
            else:
                raise ValueError('There are at most two V100 on TALOS !')
                
    def write_slurm_script(self):
        #compose the slurm script, each slurm scripts needs a magic first line (shebang)
        self.sh = '#!/bin/bash -l' + '\n'
        #parameter and settings for the Slurm batch system need to come first!
        self.sh += '#SBATCH -o ' + self.wkdir + self.stdout + '.%j' + '\n'
        self.sh += '#SBATCH -e ' + self.wkdir + self.stderr + '.%j' + '\n'
        self.sh += '#SBATCH -D ' + self.wkdir + '\n'
        self.sh += '#SBATCH -J ' + self.jbnm + '\n'
        self.sh += '#SBATCH ' + self.partition + '\n'
        self.sh += '#SBATCH ' + self.nodes + '\n'
        self.sh += '#SBATCH ' + self.ntasks + '\n'
        self.sh += '#SBATCH ' + self.ncpus + '\n'
        #self.sh += '#SBATCH ' + self.ngpus + '\n'
        #self.sh += '#SBATCH ' + self.mail + '\n'
        self.sh += '#SBATCH ' + self.time + '\n'
        
        self.sh += '\n'
        #next we have the compiler specific modifications to load the correct environment
        self.sh += self.modls + '\n'
        
        self.sh += '\n'
        #finally the actual paraprobe jobs
        self.sh += '### ' + ' paraprobe tool workflow SimID ' + str(self.simid) + '\n'
        self.sh += '\n'
        self.sh += self.what + '\n'

        with open(self.filename,'w') as f:
             f.write(self.sh)
             
    def get_slurm_script_filename(self):
        return self.filename
             
#usage example
#slm = paraprobe_slurm_single( 'HuanZhaoScriptMat2018', 1, 80, { '1_Transcoder': 'mpiexec -n 80 paraprobe_transcoder 1 Test.xml 1>STDOUT 2>STDERR.txt' }, 'GCC' )
#slm.write_slurm_script()
#print(slm.get_slurm_script_filename())
