# -*- coding: utf-8 -*-
"""
created 2020/05/26, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for writing specifically configured two-level XML files for paraprobe tools
"""

import glob
from pathlib import Path
import numpy as np

#from PARAPROBE_Shell.py import *

class motif():
    def __init__(self, elementsymbol, u1, u2, u3, chargestate, *args, **kwargs):
        self.symbol = elementsymbol
        #u1
        if u1 < 0.0:
            self.u1 = u1 + 1.0
        elif u1 <= 1.0:
            self.u1 = u1
        else:
            self.u1 = u1 - 1.0
        #u2
        if u2 < 0.0:
            self.u2 = u2 + 1.0
        elif u2 <= 1.0:
            self.u2 = u2
        else:
            self.u2 = u2 - 1.0
        #u3
        if u3 < 0.0:
            self.u3 = u3 + 1.0
        elif u3 <= 1.0:
            self.u3 = u3
        else:
            self.u3 = u3 - 1.0
        #self.u1 = u1
        #self.u2 = u2
        #self.u3 = u3
        self.charge = chargestate

class crystalstructure():
    def __init__(self, spcgrp, a, b, c, motifarr, *args, **kwargs):
        self.spcgrp = spcgrp
        self.a = a
        self.b = b
        self.c = c
        self.motif = motifarr

class paraprobe_xml():
    
    def __init__(self, toolnm, simid, grpnm, dic, hlp, *args, **kwargs):
        self.filename = 'PARAPROBE.' + toolnm + '.SimID.' + str(np.uint32(simid)) + '.xml'
        #create the XML formatted configuration file, using the dictionary ensures all elements exist
        self.xml = '<?xml version="1.0" encoding="utf-8"?>' + '\n'
        self.xml += '<' + grpnm + '>' + '\n' 
        
        #handle settings and parameter
        for key, val in dic.items():
            self.add_xml_entry(key, val)
            #check if for this key(word) there is help like and explanation for the setting/parameter available
            if key in hlp.keys():
                self.add_xml_info( hlp[key] )
                
        #are there optional target-only ion type combinations
        trgs = kwargs.get('targets', None)
        if not trgs == None: #or not trgs == []:
            if not trgs == []:
                #print(len(trgs))
                self.xml += '\t' + '<IontypeCombinations>' + '\n'
                for val in trgs:
                    #for every combination of a trg
                    #print('__' + str(val) + '__') 
                    self.xml += '\t\t' + '<entry targets="' + str(val) + '"/>' + '\n'
                self.xml += '\t' + '</IontypeCombinations>' + '\n'
            else:
                raise ValueError('We need at least one code for target ions ! It seems you dont want to do something right now.')
        #else:
        #    raise ValueError('targets is empty!')
        
        #are there optional matrix lattice?
        mucell = kwargs.get('matrixunitcell', None)
        if not mucell == None:
            #print('mucell')
            #print(np.shape(mucell))
            self.xml += '\t' + '<MatrixLatticeUnitCell>' + '\n'
            #for val in mucell:
            self.xml += '\t' + '\t' + '<prototype spacegroup="' + str(mucell.spcgrp) + '"/>' + '\n'
            self.xml += '\t' + '\t' + '<parameter a="' + str(mucell.a) + '" b="' + str(mucell.b) + '" c="' + str(mucell.c) + '"/>' + '\n'
            for atom in mucell.motif:
                self.xml += '\t' + '\t' + '<basis symbol="' + str(atom.symbol) + '" u1="' + str(np.around(np.float32(atom.u1), decimals=4)) + '" u2="' + str(np.around(np.float32(atom.u2), decimals=4)) + '" u3="' + str(np.around(np.float32(atom.u3), decimals=4)) + '" chargestate="' + str(atom.charge) + '"/>' + '\n'
            self.xml += '\t' + '</MatrixLatticeUnitCell>' + '\n'
        
        #are there optional particle lattice?
        pucell = kwargs.get('particlesunitcell', None)
        if not pucell == None:
            #print('pucell')
            #print(np.shape(pucell))
            self.xml += '\t' + '<PrecipitateLatticeUnitCell>' + '\n'
            #for val in pucell:
            self.xml += '\t' + '\t' + '<prototype spacegroup="' + str(pucell.spcgrp) + '"/>' + '\n'
            self.xml += '\t' + '\t' + '<parameter a="' + str(pucell.a) + '" b="' + str(pucell.b) + '" c="' + str(pucell.c) + '"/>' + '\n'
            for atom in pucell.motif:
                self.xml += '\t' + '\t' + '<basis symbol="' + str(atom.symbol) + '" u1="' + str(np.around(np.float32(atom.u1), decimals=4)) + '" u2="' + str(np.around(np.float32(atom.u2), decimals=4)) + '" u3="' + str(np.around(np.float32(atom.u3), decimals=4)) + '" chargestate="' + str(atom.charge) + '"/>' + '\n'
            self.xml += '\t' + '</PrecipitateLatticeUnitCell>' + '\n'
        #are there optional ion type combinations
        icmb = kwargs.get('icombis', None)
        if not icmb == None: #or not icmb == []:
            if not icmb == []:
                #print(len(icmb))
                self.xml += '\t' + '<IontypeCombinations>' + '\n'
                for val in icmb:
                    #for every combination of a trg and a nbr
                    trg_nbr = val.split(';')
                    if len(trg_nbr) == 2:
                        #print('__' + str(val) + '__') 
                        self.xml += '\t\t' + '<entry targets="' + str(trg_nbr[0]) + '" neighbors="' + str(trg_nbr[1]) + '"/>' + '\n'
                    else:
                        raise ValueError('We need a clear code for target ions and neighbor ions !')
                self.xml += '\t' + '</IontypeCombinations>' + '\n'
            else:
                raise ValueError('We need at least one code for target ions and neighbor ions ! It seems you dont want to do something right now.')
        #else:
        #    raise ValueError('icombis is empty!')

        #are there optional mass-to-charge intervals
        iv = kwargs.get('ivals', None)
        if not iv == None:
            if not iv == []:
                #print(len(iv))
                self.xml += '\t' + '<PeakSearchFilters>' + '\n'
                for bounds in iv:
                    #for every combination of a lo and hi mass-to-charge interval bound
                    if len(bounds) == 3:
                        #print('__' + str(val) + '__') 
                        self.xml += '\t\t' + '<entry min="' + str(np.around(np.float32(bounds[0]), decimals=4)) + '" max="' + str(np.around(np.float32(bounds[1]), decimals=4)) + '" targets="' + bounds[2] + '"/>' + '\n'
                    else:
                        raise ValueError('We valid left and right mass-to-charge bounds and a list of element name targets (or at least an empty string) to define an interval !')
                self.xml += '\t' + '</PeakSearchFilters>' + '\n'
            #else:
            #    raise ValueError('We need at least one mass-to-charge interval !')
        #else:
        #    raise ValueError('ivals is empty!')

        #are there optional crystal structure candidates? (paraprobe-araullo and -fourier)
        ccmb = kwargs.get('candidates', None)
        if not ccmb == None:
            if not ccmb == []:
                self.xml += '\t' + '<LatticeIontypeCombinations>' + '\n'
                for val in ccmb:
                    self.xml += '\t\t' + val + '\n'
                self.xml += '\t' + '</LatticeIontypeCombinations>' + '\n'
            else:
                raise ValueError('ccmb is empty!')
        #else:
        #    raise ValueError('candidates is empty!')

        #are there optional reference images? (paraprobe-indexer)
        ccmb = kwargs.get('refimages', None)
        if not ccmb == None:
            if not ccmb == []:
                self.xml += '\t' + '<InputfileRefImages>' + '\n'
                for val in ccmb:
                    self.xml += '\t\t' + val + '\n'
                self.xml += '\t' + '</InputfileRefImages>' + '\n'
            else:
                raise ValueError('ccmb is empty!')
        #else:
        #    raise ValueError('refimages is empty!')

        self.xml += '</' + grpnm + '>' + '\n'

        #write the file
        with open(self.filename, 'w') as f:
             f.write(self.xml)
    
    def add_xml_entry(self, tag, val): 
        self.xml += '\t<' + tag + '>' + str(val) + '</' + tag + '>' + '\n'
        
    def add_xml_info(self, val):
        self.xml += '\t<!--' + val + '-->' + '\n'
    
#self.xml += '\t' + '<LatticeIontypeCombinations>' + '\n'
#for ic in range(0,len(self.icombis[:])):
#    self.xml += '\t' + '  ' + self.icombis[ic] + '\n'
#self.xml += '\t' + '</LatticeIontypeCombinations>' + '\n'
