# -*- coding: utf-8 -*-
"""
created 2020/05/26, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for creating XML and shell config files for paraprobe-transcoder
"""

import glob
from pathlib import Path
import numpy as np

import PARAPROBE_XML.py

class paraprobe_xml():
    
    def __init__(self, simid, grpnm, dic, *args, **kwargs):
        self.filename = 'PARAPROBE.Transcoder.SimID.' + int(simid) + '.xml'
        #create the XML formatted configuration file, using the dictionary ensures all elements exist
        self.xml = '<?xml version="1.0" encoding="utf-8"?>' + '\n'
        self.xml += '<' + grpnm + '>' + '\n'        
        for key, val in dic.items():
            self.add_xml_subelement(key, val)
        self.xml += '</' + grpnm + '>' + '\n'
        #write the file
        with open(self.filename, 'w') as f:
             f.write(self.xml)
    
#self.xml += '\t' + '<LatticeIontypeCombinations>' + '\n'
#for ic in range(0,len(self.icombis[:])):
#    self.xml += '\t' + '  ' + self.icombis[ic] + '\n'
#self.xml += '\t' + '</LatticeIontypeCombinations>' + '\n'     
    def __init__(self, compiler): 
        #*args, **kwargs):
        self.cmp = compiler
        self.xml = ""
        self.stdout = 'PARAPROBE.STDOUT.txt'
        self.stderr = 'PARAPROBE.STDERR.txt'
        self.wkdir = './'
        self.jbnm = 'paraprobe'
        
        self.partition = '--partition=p.talos'
        self.nodes = '--nodes=1'
        self.ntasks = '--ntasks-per-node=1' #how many mpi processes
        self.ncpus = '--cpus-per-task=40' #TALOS how many threads per process
        self.ngpus = '--gres=gpu:2'
        self.mail = '--mail-user=m.kuehbach@mpie.de'
        self.time = '24:00:00'
        
        self.modls = ''
        if compiler == 'ITL':
            self.modls += 'module load cmake' + '\n'
            self.modls += 'module load intel' + '\n'
            self.modls += 'module load impi' + '\n'
            self.modls += 'module load mkl' + '\n'
            self.modls += 'module load boost' + '\n'
        if compiler == 'PGI':
            self.modls += 'module load cmake/3.10' + '\n'
            self.modls += 'module load cuda/10.0' + '\n'
            self.modls += 'module load pgi/19' + '\n'
            self.modls += 'module load impi/2018.4' + '\n'
        self.modls += 'module list' + '\n'
        self.modls += 'if [ ! -z $SLURM_CPUS_PER_TASK ] ; then' + '\n'
        self.modls += '	export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK' + '\n'
        self.modls += 'else' + '\n'
        self.modls += '	export OMP_NUM_THREADS=1' + '\n'
        self.modls += 'fi' + '\n'
        self.modls += 'export OMP_PLACES=cores' + '\n'
        #self.modls += 'export OMP_NESTED=true'
        #self.modls += 'export MKL_NUM_THREADS=1'
        self.modls += 'echo "OpenMP NUM THREADS env option"' + '\n'
        self.modls += 'echo $OMP_NUM_THREADS' + '\n'
        #self.modls += 'echo "OpenMP NESTED env option"'
        #self.modls += 'echo $OMP_NESTED'
        self.modls += 'echo "OpenMP PLACES env option"' + '\n'
        self.modls += 'echo $OMP_PLACES' + '\n'
        if compiler == 'PGI':
            self.modls += 'export CUDA_VISIBLE_DEVICES=0,1' + '\n'
            self.modls += 'echo $CUDA_VISIBLE_DEVICES' + '\n'
            self.modls += 'echo $PGI_CURR_CUDA_HOME' + '\n'
            self.modls += 'echo $PGI' + '\n'
        
        #echo "MKL NUM THREADS env option"
        #echo $MKL_NUM_THREADS + '\n'
        #export LIBRARY_PATH=/mpcdf/soft/SLE_15/packages/x86_64/intel_parallel_studio/2018.4/mkl/lib/intel64_lin/
        #echo $LIBRARY_PATH
        #echo "CUDA VISIBLE DEVICES env option"
        #export CUDA_VISIBLE_DEVICES=0,1
        #echo $CUDA_VISIBLE_DEVICES
        #echo "PGI CURR CUDA env option"
        ####for pgi/18
        ##export PGI_CURR_CUDA_HOME=/talos/u/system/soft/SLE_15/packages/skylake/pgi/18.10/linux86-64/2018/cuda/10.0
        ##echo $PGI_CURR_CUDA_HOME
        ##echo "PGI home folder"
        ##export PGI=/talos/u/system/soft/SLE_15/packages/skylake/pgi/18.10
        ##echo $PGI
        ##for pgi/19
        ##export PGI_CURR_CUDA_HOME=/talos/u/system/soft/SLE_15/packages/skylake/pgi/linux86-64-llvm/2019/cuda
        ##echo $PGI_CURR_CUDA_HOME
        ##echo "PGI home folder"
        ##export PGI=/talos/u/system/soft/SLE_15/packages/skylake/pgi/19.3
        ##echo $PGI
        self.modls += 'ulimit -s unlimited' + '\n'
        self.modls += 'echo "ulimit env option"' + '\n'
        self.modls += 'ulimit' + '\n'
        #list allocated TALOS resources nodes
        self.modls += 'echo $SLURM_JOB_NODELIST' + '\n'
        ##echo $SLURM_NODELIST
        self.modls += 'echo $SLURM_JOB_NUM_NODES' + '\n'
        ##echo $SLURM_NNODES
        self.modls += 'echo "job $SLURM_JOB_NAME with job id $SLURM_JOB_ID is running on $SLURM_JOB_NUM_NODES node(s): $SLURM_JOB_NODELIST"' + '\n'
        
        self.what = ''

    def set_simid(self, val):
        self.simid = val
        
    def set_timeplan(self, val):
        self.time = val;
    
    def set_mpiranks(self, val):
        self.ntasks = '--ntasks-per-node=' + str(val)
        
    def set_ompthreads(self, val):
        self.ncpus = '--cpus-per-task=' + str(val) #TALOS how many threads per process
    
    def set_gpus(self, val):
        self.ngpus = '--gres=gpu:' + str(val)
        
    def set_what_to_do(self, val):
        self.what = val;
        
    def to_bash(self, jobname): #, root: ET.Element):
        self.sh = '#!/bin/bash -l' + '\n'
        self.sh += '#SBATCH -o ./PARAPROBE.Toolchain.SimID.' + str(self.simid) + '.STDOUT.%j' + '\n'
        self.sh += '#SBATCH -e ./PARAPROBE.Toolchain.SimID.' + str(self.simid) + '.STDERR.%j' + '\n'
        self.sh += '#SBATCH -D ./' + '\n'
        self.sh += '#SBATCH -J ' + jobname + '\n'
        self.sh += '#SBATCH ' + self.partition + '\n'
        self.sh += '#SBATCH ' + self.nodes + '\n'
        self.sh += '#SBATCH ' + self.ntasks + '\n'
        self.sh += '#SBATCH ' + self.ncpus + '\n'
        if self.cmp == 'PGI':
            self.sh += '#SBATCH ' + self.ngpus + '\n'
        self.sh += '#SBATCH ' + self.mail + '\n'
        self.sh += '#SBATCH ' + self.time + '\n'
        self.sh += '#submits to p.talos make jobs currently by default to run exclusively on resources' + '\n'
        self.sh += self.modls + '\n'
        self.sh += '\n'
        self.sh += '#this is the toolchain' + '\n'
        self.sh += self.what + '\n'        
        
    def write(self, file_name: str):
         #file_path = Path(file_name)
         #file_path.write_text(self.to_utf8())
         with open(file_name,'w') as f:
             f.write(self.sh)
         

    #def to_utf8(self):
    #    """
    #    Beautifies/Structures the xml output
    #    :return: str
    #    """
    #    return minidom.parseString(ET.tostring(self.xml)).toprettyxml(encoding="utf-8").decode('ascii')

