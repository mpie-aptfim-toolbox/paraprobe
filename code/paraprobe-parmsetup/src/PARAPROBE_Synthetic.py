# -*- coding: utf-8 -*-
"""
created 2020/05/26, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for creating XML and shell config files for paraprobe-synthetic
"""
import sys
import glob
from pathlib import Path
import numpy as np

from PARAPROBE_XML import *
#import PARAPROBE_Shell.py


config = { 'Healthy': True,
           'PeriodicTableOfElements': 'PARAPROBE.PeriodicTableOfElements.xml',
           'SynthesizingMode': 1,
           'SimNumberOfAtoms': 2.0e6,
           'SimRelBottomRadius': 0.50,
           'SimRelTopRadius': 0.50, 
           'SimRelBottomCapHeight': 0.00,
           'SimRelTopCapHeight': 0.00,
           'SimDetectionEfficiency': 1.00, 
           'SimFiniteSpatResolutionX': 0.00,
           'SimFiniteSpatResolutionY': 0.00,
           'SimFiniteSpatResolutionZ': 0.00,
           'DebugPhaseID': 0,
           'SimMatrixOrientation': '0.0;0.0;0.0',
           'a1': 0.405,
           'b1': 0.405,
           'c1': 0.405,
           'alpha1': 90.0,
           'beta1': 90.0,
           'gamma1': 90.0,
           'SimNumberOfCluster': 0,
           'SimPrecipitateOrientation': '0.0;0.0;0.0',
           'SimClusterRadiusMean': 5.0, 
           'SimClusterRadiusSigmaSqr': 0.1, 
           'a2': 0.410,
           'b2': 0.410,
           'c2': 0.410,
           'alpha2': 90.0,
           'beta2': 90.0,
           'gamma2': 90.0,         
           'CrystalloVoroMeanRadius': 5.0  }

#we should inform on particular config keys, does not need inform for every config key
info = { 'SynthesizingMode': '0 none, 1 single-crystal, 2 polycrystal', 
         'DebugPhaseID': '0 Al, 1 Al3Sc, 2 W, 3 Al3Li, 4 user-defined specific space groups' }

implemented_spcgrps = np.array([62, 141, 194, 221, 225, 229])

class paraprobe_synthetic():

    def __init__(self, simid, *args, **kwargs):
        self.simid = simid
        self.toolname = 'Synthetic'
        self.recon_filename = 'PARAPROBE.Synthetic.Results.SimID.' + str(np.uint32(self.simid)) + '.h5' #'.apth5'
        self.config = config
        self.help = info
        self.matrixunitcell = None
        self.particlesunitcell = None
        #check if fnm is a supported format

    def get_recon_filename(self):
        return self.recon_filename   
        
    def set_tip_geom_natoms(self, val):
        if np.uint32(val) >= np.uint32(1):
            self.config['SimNumberOfAtoms'] = val
        else:
            raise ValueError('Number of atoms needs to be [1, ' + str(2**32-1) + ') !')
    
    def set_tip_geom_radius_top(self, val):
        if val >= 0.0:
            self.config['SimRelTopRadius'] = np.around(np.float32(val), decimals=3)
        else:
            raise ValueError('SimRelTopRadius needs to be >= 0 !')
 
    def set_tip_geom_radius_bottom(self, val):
        if val >= 0.0:
            self.config['SimRelBottomRadius'] = np.around(np.float32(val), decimals=3)
        else:
            raise ValueError('SimRelBottomRadius needs to be >= 0 !')

    def set_tip_noise_deteff(self, val):
        if val >= 0.0 and val <= 1.0:
            self.config['SimDetectionEfficiency'] = np.around(np.float32(val), decimals=3)
        else:
            raise ValueError('SimDetectionEfficiency needs to be in [0.0, 1.0] !')
        
    def set_tip_noise_gaussianx(self, val):
        if val >= 0.0:
            self.config['SimFiniteSpatResolutionX'] = np.around(np.float32(val), decimals=3)
        else:
            raise ValueError('SimFiniteSpatResolutionX needs to be >= 0 !')
  
    def set_tip_noise_gaussiany(self, val):
        if val >= 0.0:
            self.config['SimFiniteSpatResolutionY'] = np.around(np.float32(val), decimals=3)
        else:
            raise ValueError('SimFiniteSpatResolutionY needs to be >= 0 !')
    
    def set_tip_noise_gaussianz(self, val):
        if val >= 0.0:
            self.config['SimFiniteSpatResolutionZ'] = np.around(np.float32(val), decimals=3)
        else:
            raise ValueError('SimFiniteSpatResolutionZ needs to be >= 0 !')

    def set_tip_matrix_phase_al(self):
        self.config['DebugPhaseID'] = 0
        
    def set_tip_matrix_phase_al3sc(self):
        self.config['DebugPhaseID'] = 1
        
    def set_tip_matrix_phase_w(self):
        self.config['DebugPhaseID'] = 2
    
    def set_tip_matrix_phase_al3li(self):
        self.config['DebugPhaseID'] = 3
        
    def set_tip_matrix_lattice_spacegroup(self, spcgrp, a, b, c, motifarr ):
        valid = False
        for spc in implemented_spcgrps:
            if spcgrp != spc:
                continue
            else:
                valid = True
                break
        if valid == False:
            raise ValueError('Spacegroup ' + str(spcgrp) + ' is not supported !')
        if a >= 0.0 and b >= 0.0 and c >= 0.0 and len(motifarr) >= 1:
            self.config['SynthesizingMode'] = 1
            self.config['DebugPhaseID'] = 4
            self.matrixunitcell = crystalstructure( spcgrp, np.around(np.float32(a), decimals=3), np.around(np.float32(b), decimals=3), np.around(np.float32(c), decimals=3), motifarr )
        else:
             raise ValueError('Values chosen as lattice constants a,b,c for matrix make no sense or no motifs !')

    def set_tip_matrix_lattice_abc(self, a, b, c):
        if a >= 0.0 and b >= 0.0 and c >= 0.0:
            self.a1 = np.around(np.float32(a), decimals=3)
            self.b1 = np.around(np.float32(b), decimals=3)
            self.c1 = np.around(np.float32(c), decimals=3)
        else:
            raise ValueError('Values chosen as lattice constants a,b,c for matrix make no sense !')

    def set_tip_matrix_ori(self, phi1, Phi, phi2):
        if phi1 >= 0.0 and phi1 <= 360.0 and Phi >= 0.0 and Phi <= 360.0 and phi2 >= 0.0 and phi2 <= 360:
            self.config['SimMatrixOrientation'] = str(np.around(np.float32(phi1), decimals=2)) + ';' + str(np.around(np.float32(Phi), decimals=2)) + ';' + str(np.around(np.float32(phi2), decimals=2))
        else:
            raise ValueError('Values chosen as Bunge-Euler angles for single-crystalline matrix make no sense !')   

    def set_tip_particles_number(self, N):
        if N >= 0:
            self.config['SimNumberOfCluster'] = np.uint32(N)
        else:
            raise ValueError('Number of particles needs to be >= 0 !')

    def set_tip_particles_radius(self, R):
        if R >= 1.0:
            self.config['SimClusterRadiusMean'] = np.around(np.float32(R), decimals=3)
        else:
            raise ValueError('Radius of particles needs to be at least 1.0 nm !')

    def set_tip_particles_sigma(self, sigma):
        if sigma >= 0.1:
            self.config['SimClusterRadiusSigmaSqr'] = np.around(np.float32(sigma), decimals=3)
        else:
            raise ValueError('Sigma of particles for size distribution to be at least 0.1 nm !')

    def set_tip_particles_lattice_spacegroup(self, spcgrp, a, b, c, motifarr ):
        if self.config['SynthesizingMode'] != 1 or self.config['DebugPhaseID'] != 4:
            raise ValueError('Combination of particles with lattice space group only when using single-crystalline matrix with lattice space groups!')
        valid = False
        for spc in implemented_spcgrps:
            if spcgrp != spc:
                continue
            else:
                valid = True
                break
        if valid == False:
            raise ValueError('Spacegroup ' + str(spcgrp) + ' is not supported !')
        if a >= 0.0 and b >= 0.0 and c >= 0.0 and len(motifarr) >= 1:
            self.particlesunitcell = crystalstructure( spcgrp, np.around(np.float32(a), decimals=3), np.around(np.float32(b), decimals=3), np.around(np.float32(c), decimals=3), motifarr )
        else:
             raise ValueError('Values chosen as lattice constants a,b,c for particles make no sense or no motifs !')

    def set_tip_particles_lattice_abc(self, a, b, c):
        if a >= 0.0 and b >= 0.0 and c >= 0.0:
            self.a2 = np.around(np.float32(a), decimals=3)
            self.b2 = np.around(np.float32(b), decimals=3)
            self.c2 = np.around(np.float32(c), decimals=3)
        else:
            raise ValueError('Values chosen as lattice constants a,b,c for particles make no sense !')

    def set_tip_particles_ori(self, phi1, Phi, phi2):
        if phi1 >= 0.0 and phi1 <= 360.0 and Phi >= 0.0 and Phi <= 360.0 and phi2 >= 0.0 and phi2 <= 360:
            self.config['SimPrecipitateOrientation'] = str(np.around(np.float32(phi1), decimals=2)) + ';' + str(np.around(np.float32(Phi), decimals=2)) + ';' + str(np.around(np.float32(phi2), decimals=2))
        else:
            raise ValueError('Values chosen as Bunge-Euler angles for particles make no sense !')

    ###MK::fully separate particles and matrix instead of the debug keys use XML description of unit cell e.g. COD
   
    #place here the more higher-level functions for end users e.g.
    def set_tip_cylinder(self, radius2height, natoms):
        if natoms >= 1.0e3:
            self.set_tip_geom_natoms( natoms )
        else:
            raise ValueError('Number of atoms must be at least 1.0e3 !')
        if radius2height >= 0.1 and radius2height <= 1.0:
            self.set_tip_geom_radius_top( radius2height )
            self.set_tip_geom_radius_bottom( radius2height )
        else:
            raise ValueError('Radius2Height needs to be on interval [0.1, 1.0] !')
            
    def set_tip_noise_none(self):
        self.set_tip_noise_deteff( 1.0 )
        self.set_tip_noise_gaussianx( 0.0 )
        self.set_tip_noise_gaussiany( 0.0 )
        self.set_tip_noise_gaussianz( 0.0 )     
        
    def set_tip_matrix_fcc(self, symbol, a, chargestate):
        ###MK::add check for the element for convenience
        if not symbol == '' and a >= 0.1:
            base = np.array([ motif( symbol, 0.0, 0.0, 0.0, chargestate) ])
            self.set_tip_matrix_lattice_spacegroup( 225, a, a, a, base )
        else:
            raise ValueError('We need an element symbol and a lattice constant a >= 0.1 nanometer !')
            
    def set_tip_matrix_bcc(self, symbol, a, chargestate):
        ###MK::add check for the element for convenience
        if not symbol == '' and a >= 0.1:
            base = np.array([ motif( symbol, 0.0, 0.0, 0.0, chargestate) ])
            self.set_tip_matrix_lattice_spacegroup( 229, a, a, a, base )
        else:
            raise ValueError('We need an element symbol and a lattice constant a >= 0.1 nanometer !')
            
    def set_tip_matrix_hcp(self, symbol, a, c, chargestate):
        ###MK::add check for the element for convenience
        if not symbol == '' and a >= 0.1 and c >= 0.1:
            base = np.array([ motif( symbol, 1.0/3.0, 2.0/3.0, 1.0/4.0, chargestate),
                              motif( symbol, 2.0/3.0, 1.0/3.0, 3.0/4.0, chargestate) ])
            self.set_tip_matrix_lattice_spacegroup( 194, a, a, c, base )
        else:
            raise ValueError('We need an element symbol and a lattice constant a >= 0.1 nanometer !')

    #and finally some examples for specific crystal structures, more complicated using the AFLOW crystal structure library
    #in conjunction with paraprobe's space group handling capabilities
    #and here Zircon at 1atm y := y3 = 0.0660 and z := z3 = 0.1951            
    def set_tip_matrix_al3sc(self):
        base = np.array([ motif( 'Sc', 0.0, 0.0, 0.0, 1 ),
                          motif( 'Al', 0.0, 0.5, 0.5, 2 ),
                          motif( 'Al', 0.5, 0.0, 0.5, 2 ),
                          motif( 'Al', 0.5, 0.5, 0.0, 2 ) ])
        self.set_tip_matrix_lattice_spacegroup( 221, 0.410, 0.410, 0.410, base )

    def set_tip_matrix_zircon(self):
        #http://aflowlib.org/CrystalDatabase/A4BC_tI24_141_h_b_a.html
        aa = 6.604/10.0 #angstrom
        cc = 5.980/10.0 #angstrom in aflow but we need nm !
        y3 = 0.0660 #fraction value
        z3 = 0.1951 #fractional value
        base = np.array([ motif( 'Zr', 7.0/8.0, 1.0/8.0, 3.0/4.0, 2 ),
                          motif( 'Zr', 1.0/8.0, 7.0/8.0, 1.0/4.0, 2 ),
                          motif( 'Si', 5.0/8.0, 3.0/8.0, 1.0/4.0, 2 ),
                          motif( 'Si', 3.0/8.0, 5.0/8.0, 3.0/4.0, 2 ),
                          motif( 'O:', (y3+z3), z3, y3, 1 ),
                          motif( 'O:', (1.0/2.0-y3+z3), z3, (1.0/2.0-y3), 1),
                          motif( 'O:', z3, (1.0/2.0-y3+z3), -y3, 1 ),
                          motif( 'O:', z3, (y3+z3), (1.0/2.0+y3), 1),
                          motif( 'O:', (1.0/2.0+y3-z3), -z3, (1.0/2.0+y3), 1),
                          motif( 'O:' -(y3+z3), -z3, -y3, 1),
                          motif( 'O:', -z3, (1.0/2.0+y3-z3), y3, 1),
                          motif( 'O:', -z3, -(y3+z3), (1.0/2.0-y3), 1) ])
        self.set_tip_matrix_lattice_spacegroup( 141, aa, aa, cc, base )

    def run(self, nmpi = 1): #*args, **kwargs):
        #let XML generator class create the XML file
        #needs an XML top-level group name, here 'ConfigSynthetic' because we want sth specific for synthetic tool
        #and a dictionary with the settings
        #self.simid = simid
        #del self.config['Healthy']
        #del self.config['PeriodicTableOfElements']
        if self.config['DebugPhaseID'] != 4:
            self.xml = paraprobe_xml( 'Synthetic', self.simid, 'ConfigSynthetic', self.config, self.help )
        else:
            if self.config['SynthesizingMode'] != 1:
                raise ValueError('DebugPhaseID currently supported only for SynthesizingMode == 1, single crystal !')
            #clear out unnecessary parameters in the config file for this mode
            #del self.config['a1']
            #del self.config['b1']
            #del self.config['c1']
            #del self.config['alpha1']
            #del self.config['beta1']
            #del self.config['gamma1']
            #del self.config['a2']
            #del self.config['b2']
            #del self.config['c2']
            #del self.config['alpha2']
            #del self.config['beta2']
            #del self.config['gamma2']
            #del self.config['SimPrecipitateOrientation']
            #del self.config['SimClusterRadiusMean']
            #del self.config['SimClusterRadiusSigmaSqr']
            #del self.config['CrystalloVoroMeanRadius']
            self.xml = paraprobe_xml( 'Synthetic', self.simid, 'ConfigSynthetic', self.config, self.help, matrixunitcell = self.matrixunitcell, particlesunitcell = self.particlesunitcell )
        #sh = 'paraprobe_bash()'
        #return xml, sh

        #create the specific bash command to run this analysis
        if nmpi >= 1:
            sid = str(np.uint32(self.simid))
            prefix = 'PARAPROBE.Synthetic.SimID.' + sid
            execnm = 'paraprobe_synthetic' #'./paraprobe_synthetic'
            #sh = 'mpiexec -n ' + str(np.uint32(nmpi)) + ' ' + execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            sh = execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            return sh
        else:
            raise ValueError('We need to use at least one process when working with the Message Passing Interface !')

# =============================================================================
# exit
# #example usage
# #build a cylindrical APT dataset with 25 mio atoms and radius-to-height 0.5:1 single-crystal Al
#tip = paraprobe_synthetic( 1 )
#tip.set_tip_geom_natoms( 25.0e6 )
#tip.set_tip_geom_radius_top( 0.5 )
#tip.set_tip_geom_radius_bottom( 0.5 )
#base = np.array([ motif('Sc', 0.0, 0.0, 0.0, 1), 
#                  motif('Al', 0.0, 0.5, 0.5, 1),
#                  motif('Al', 0.5, 0.0, 0.5, 1),
#                  motif('Al', 0.5, 0.5, 0.0, 1) ])
#tip.set_tip_matrix_lattice_spacegroup( 221, 0.410, 0.410, 0.410, base )

# xml = tip.configure( 1 )
#cmd = tip.run( 1 )
# =============================================================================
