# -*- coding: utf-8 -*-
"""
created 2020/05/26, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for creating XML and shell config files for paraprobe-spatstat
"""
import sys
import glob
from pathlib import Path
import numpy as np

from PARAPROBE_XML import *
#import PARAPROBE_Shell.py

config = { 'Healthy': True,
           'InputfilePSE': 'PARAPROBE.PeriodicTableOfElements.xml',
           'InputfileReconstruction': '', 
           'InputfileHullAndDistances': '',
           'AnalyzeRDF': 0,
           'AnalyzeKNN': 0,
           'AnalyzeSDM': 0,
           'ROIRadiusRDFMin': 0.000,
           'ROIRadiusRDFIncr': 0.001,
           'ROIRadiusRDFMax': 5.000,
           'ROIRadiusKNNMin': 0.000,
           'ROIRadiusKNNIncr': 0.001,
           'ROIRadiusKNNMax': 5.000,
           'ROIRadiusSDMMin': 0.00,
           'ROIRadiusSDMIncr': 0.10,
           'ROIRadiusSDMMax': 5.00,
           'ROIVolumeInsideOnly': 1,
           'KOrderForKNN': 1,
           'KOrderForSDM': 1  }

#we should inform on particular config keys, does not need inform for every config key
info = { 'SurfacingMode': '0 nothing, 1 alpha shape', 
         'AlphaShapeAlphaValue': '0 smallest solid, 1 cgal considered optimal',
         'DistancingMode': '0 nothing, 1 complete, 2 skin within DistancingRadiusMax'  }

class paraprobe_spatstat():

    def __init__(self, simid, recon, hull, *args, **kwargs):
        self.simid = simid
        self.toolname = 'Spatstat'
        self.config = config
        self.help = info
        self.icombis = []
        
        if not recon.endswith('.h5'): #'apth5'):
            raise ValueError('The filename pointing to the reconstruction is not an H5 file !')
        else:
            self.config['InputfileReconstruction'] = recon
        if not hull.endswith('.h5'):
            raise ValueError('The filename pointing to the dataset edge triangulation and distances is not an HDF5 file !')
        else:
            self.config['InputfileHullAndDistances'] = hull
    
    def set_rdf(self, incr, rmax):
        if incr >= 0.001 and rmax >= incr and rmax >= 0.5:
            self.config['AnalyzeRDF'] = 1
            self.config['ROIRadiusRDFMin'] = np.around(np.float32(0.000), decimals=3)
            self.config['ROIRadiusRDFIncr'] = np.around(np.float32(incr), decimals=3)
            self.config['ROIRadiusRDFMax'] = np.around(np.float32(rmax), decimals=3)
        else:
            raise ValueError('For computing radial distribution function we need incr >= 0.001 and rmax >= 0.5 >= 0.001 !')

    def set_knn(self, incr, rmax, kthnparr ):
        if incr >= 0.001 and rmax >= incr and rmax >= 0.5:
            self.config['AnalyzeKNN'] = 1
            self.config['ROIRadiusKNNMin'] = np.around(np.float32(0.000), decimals=3)
            self.config['ROIRadiusKNNIncr'] = np.around(np.float32(incr), decimals=3)
            self.config['ROIRadiusKNNMax'] = np.around(np.float32(rmax), decimals=3)
            kthnparr = np.sort( kthnparr )
            N = len(kthnparr)
            tmp = ''
            for i in np.arange(0,N-1):
                #check if these are uint32 and sort
                tmp += str(kthnparr[i]) + ';'
            tmp += str(kthnparr[N-1])
            self.config['KOrderForKNN'] = tmp
        else:
            raise ValueError('For computing k nearest neighbors function we need incr >= 0.001 and rmax >= 0.5 >= 0.001 !')
  
    def set_sdm(self, incr, rmax, kthnparr):
        if incr >= 0.01 and rmax >= incr and rmax >= 0.5 and (rmax / incr) < 100:
            self.config['AnalyzeSDM'] = 1
            self.config['ROIRadiusSDMMin'] = np.around(np.float32(0.000), decimals=2)
            self.config['ROIRadiusSDMIncr'] = np.around(np.float32(incr), decimals=2)
            self.config['ROIRadiusSDMMax'] = np.around(np.float32(rmax), decimals=2)
            kthnparr = np.sort( kthnparr )
            N = len(kthnparr)
            tmp = ''
            for i in np.arange(0,N-1):
                #check if these are uint32 and sort
                tmp += str(kthnparr[i]) + ';'
            tmp += str(kthnparr[N-1])
            self.config['KOrderForSDM'] = tmp
        else:
            raise ValueError('For computing 3D spatial distribution maps we need incr >= 0.01, rmax >= 0.5 >= 0.01 and rmax/incr < 100 !')
   
    def add_iontype_combi(self, trg, nbr ):
        if not trg == '' and not nbr == '':
            #add more checks to it using Python periodic table
            icmb = trg + ';' + nbr
            #print(str(icmb))
            self.icombis.append( icmb )
        else:
            raise ValueError('Either the target or neighbor iontype keys are incorrect !')
    
    def run(self, nmpi = 1): #*args, **kwargs):
        #let XML generator class create the XML file
        #needs an XML top-level group name, here 'ConfigSurfacer' because we want sth specific for surfacer tool
        #and a dictionary with the settings
        self.xml = paraprobe_xml( 'Spatstat', self.simid, 'ConfigSpatstat', self.config, self.help, icombis = self.icombis )
        #sh = 'paraprobe_bash()'
        
        #create the specific bash command to run this analysis
        if nmpi >= 1:
            sid = str(np.uint32(self.simid))
            prefix = 'PARAPROBE.Spatstat.SimID.' + sid
            execnm = 'paraprobe_spatstat' #'./paraprobe_spatstat'
            #sh = 'mpiexec -n ' + str(np.uint32(nmpi)) + ' ' + execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            sh = execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            return sh
        else:
            raise ValueError('We need to use at least one process when working with the Message Passing Interface !')     

# =============================================================================
# exit
# #example usage
# #take your reconstruction and computed distances to compute spatial statistics
# #for different combinations of ions, using original labels and randomized labels, using different radii, using different kth
# task = paraprobe_spatstat( 1, 'PARAPROBE.Transcoder.Results.SimID.1.apth5', 'PARAPROBE.Surfacer.Results.SimID.1.h5' )
# 
# #these ion combinations e.g. for an Al-Li-Mg-Ag alloy maybe
# task.add_iontype_combi( 'Al', 'Al' )
# task.add_iontype_combi( 'Al', 'Li' )
# task.add_iontype_combi( 'Al', 'Mg' )
# task.add_iontype_combi( 'Al', 'Ag' )
# task.add_iontype_combi( 'Li', 'Al' )
# task.add_iontype_combi( 'Li', 'Li' )
# task.add_iontype_combi( 'Li', 'Mg' )
# task.add_iontype_combi( 'Li', 'Ag' )
# task.add_iontype_combi( 'Mg', 'Al' )
# task.add_iontype_combi( 'Mg', 'Li' )
# task.add_iontype_combi( 'Mg', 'Mg' )
# task.add_iontype_combi( 'Mg', 'Ag' )
# task.add_iontype_combi( 'Ag', 'Al' )
# task.add_iontype_combi( 'Ag', 'Li' )
# task.add_iontype_combi( 'Ag', 'Mg' )
# task.add_iontype_combi( 'Ag', 'Ag' )
# 
# #ahh, wait maybe I also want to check for spatial correlations if H
# task.add_iontype_combi( 'H:', 'Al')
# task.add_iontype_combi( 'H:', 'Li')
# task.add_iontype_combi( 'H:', 'Mg')
# task.add_iontype_combi( 'H:', 'Ag')
# 
# #or multi target and multi neighbors?
# #e.g. either Al or Li as targets against all Mg neighbors? easy...
# task.add_iontype_combi( 'Al,Li', 'Mg')
# 
# #okay, lets do some spatial statistics
# 
# #e.g. radial distribution function
# task.set_rdf( 0.001, 5.000 )
# 
# #e.g. kth nearest neighbors for all combinations 
# task.set_knn( 0.001, 5.000, np.array([1, 10, 100]) )
# #yes a single line gives you all combinations, easy peasy
# 
# #e.g. why not also three-dimensional environment i.e. directional spatial statistics?
# #3D spatial distribution maps
# task.set_sdm( 0.1, 5.0, np.array([1, 10]) )
# 
#and create the XML configuration file and the command line entry for running spatstat using mpi and openmp
# ##MK::give dependencies to identify what the file needs to build workflows automatically, for now manual scripting
# #three equivalent version to instruct running with one process
# cmd1a = task.run()
# cmd1b = task.run( 1 )
# cmd1c = task.run( nmpi = 1 )
# 
# #alternatively, run with two process
# cmd2 = task.run( 2 )
# =============================================================================
