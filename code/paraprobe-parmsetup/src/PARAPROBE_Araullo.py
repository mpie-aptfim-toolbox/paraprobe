# -*- coding: utf-8 -*-
"""
created 2020/05/29, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for creating XML and shell config files for paraprobe-araullo
"""

import glob
from pathlib import Path
import numpy as np

from PARAPROBE_XML import *
#import PARAPROBE_Shell.py

config = { 'Healthy': True,
           'InputfilePSE': 'PARAPROBE.PeriodicTableOfElements.xml',
           'InputfileElevAzimGrid': 'PARAPROBE.Araullo.GeodesicSphere.40962.h5',
           'InputfileReconstruction': '', 
           'InputfileHullAndDistances': '',
           'VolumeSamplingMethod': 3,
           'ROIRadiusMax': 2.0,
           'SamplingGridBinWidthX': 4.0,
           'SamplingGridBinWidthY': 4.0,
           'SamplingGridBinWidthZ': 4.0,
           'InputfileUserROIPositions': '',
           'RefineGridOldBinWidthX': 4.0,
           'RefineGridOldBinWidthY': 4.0,
           'RefineGridOldBinWidthZ': 4.0,
           'RefineGridNewSubdivisionX': 1,
           'RefineGridNewSubdivisionY': 1,
           'RefineGridNewSubdivisionZ': 1,
           'SamplingPosition': '0.0;0.0;0.0',
           'FixedNumberOfROIs': 10,
           'RemoveROIsProtrudingOutside': 1,
           'SDMBinExponent': 8,
           'SDMNormalize': 1,
           'WindowingMethod': 0,
           'KaiserAlpha': 8.0,
           'IOStoreSpecificPeaks': 1,
           'IODBScanOnSpecificPeaks': 0,
           'DBScanIntensityThreshold': 0.1,
           'DBScanSearchRadius': 0.02,
           'DBScanMinPksForReflector': 1,
           'IOStoreKeyQuants': 1,
           'IOStoreThreeStrongestPeaks': 0,
           'IOStoreHistoCnts': 0,
           'IOStoreHistoFFTMagn': 0,
           'KeyQuants': '0.000;0.0001;0.001;0.0100;0.1000;0.5000;0.9000;0.9900;0.9990;0.9999;1.0000',
           'MaxMemoryPerNode': 180,
           'ProcessesPerMPITeam': 1,
           'GPUsPerComputingNode': 1,
           'GPUPerProcess': 1,
           'GPUWorkloadFactor': 1  } #0.02 works well for 40952 geodesic sphere FE mesh

#we should inform on particular config keys, does not need inform for every config key
info = { 'VolumeSamplingMethod': '0 3D regular ROI grid, 1 single ROI at user-defined position, 2 single ROI in dataset center, 3 random ROIs',
         'WindowingMethod': '0 rectangular window, 1 Kaiser window (not implemented)',
         'RefineGridOldBinWidthX': 'has to be the SamplingGridBinWidthX, Y, Z from the previous run',
         'RefineGridNewSubdivisionX': 'into how many new voxels to sub-divide the ROI grid locally in X, Y, Z',
         'IOStoreHistoCnts': 'all IO options are active when 1, else inactive',
         'IOStoreSpecificPeaks': 'required for indexing, for initial assessments of the file do KeyQuantsPerROI first',
         'IODBScanOnSpecificPeaks': 'if IOStoreSpecificPeaks is 1, compresses peaks and currently stores only the compressed peaks not the complete signatures',
         'IODBScanOnSpecificPeaks': 'execute a DBScan on the specific peaks to compress the signatures',
         'DBScanIntensityThreshold': 'which FE mesh vertices to disregard before applying a DBScan',
         'DBScanSearchRadius': 'which Cartesian distance to use to group neighbouring FE mesh vertices during DBScan',
         'DBScanMinPksForReflector': 'how many FE mesh vertices we need at least to define a peak',
         'IOStoreKeyQuants': 'quantiles of image intensities for the ROI from specific amplitude peaks',
         'MaxMemoryPerNode': 'set to maximum mount of memory available on the node but leave about 5 GB for operating system',
         'GPUsPerComputingNode': 'araullo currently does not use GPUs, only expert settings for software developers' }

class paraprobe_araullo():

    def __init__(self, simid, recon, hull, *args, **kwargs):
        self.simid = simid
        self.toolname = 'Araullo'
        self.config = config
        self.help = info
        self.candidates = []

        if not recon.endswith('.h5'): #'.apth5'):
            raise ValueError('The filename pointing to the reconstruction is not an H5 file !')
        else:
            self.config['InputfileReconstruction'] = recon
        if not hull.endswith('.h5'):
            raise ValueError('The filename pointing to the dataset edge triangulation and distances is not an HDF5 file !')
        else:
            self.config['InputfileHullAndDistances'] = hull

    def set_roi_rmax(self, rmax):
        if rmax >= 0.5:
            self.config['ROIRadiusMax'] = np.around(np.float32(rmax), decimals=3)
        else:
            raise ValueError('The radius of the ROIs should be at least 0.5nm to include some ions !')

    def set_roi_3dgrid(self, dx, dy, dz):
        self.config['VolumeSamplingMethod'] = 0
        if dx >= 0.1 and dy >= 0.1 and dz >= 0.1:
            self.config['SamplingGridBinWidthX'] = dx
            self.config['SamplingGridBinWidthY'] = dy
            self.config['SamplingGridBinWidthZ'] = dz
        else:
            raise ValueError('All grid spacings need to be positive and at least 0.1nm !')            

    def set_roi_single_here(self, x, y, z):
        self.config['VolumeSamplingMethod'] = 1
        self.config['SamplingPosition'] = str(np.around(np.float32(x), decimals=3)) + ';' + str(np.around(np.float32(y), decimals=3)) + ';' + str(np.around(np.float32(z), decimals=3))

    def set_roi_single_center(self):
        self.config['VolumeSamplingMethod'] = 2
   
    def set_roi_user_refinement(self, roifn, di, ni):
        self.config['VolumeSamplingMethod'] = 4
        self.config['InputfileUserROIPositions'] = roifn
        if di > 0.1 and ni >= 1:
            self.config['RefineGridOldBinWidthX'] = di
            self.config['RefineGridOldBinWidthY'] = di
            self.config['RefineGridOldBinWidthZ'] = di
            self.config['RefineGridNewSubdivisionX'] = ni
            self.config['RefineGridNewSubdivisionY'] = ni
            self.config['RefineGridNewSubdivisionZ'] = ni
        else:
            raise ValueError('RefineGridOldBinWidthX needs to be at least 0.1nm and RefineGridNewSubdivision at least 1 !')

    def set_roi_random(self, N):
        if N >= 1 and N < 2**32-1:
            self.config['VolumeSamplingMethod'] = 3
            self.config['FixedNumberOfROIs'] = np.uint32(N)
        else:
            raise ValueError('For random sampling of ROIs we the number of ROIs needs to be at least 1 and at most ' + str(2*32-1) + ' !')

    def set_sdm_binning(self, m):
        if m >= 8 and m <= 12:
            self.config['SDMBinExponent'] = np.uint32(m)
        else:
            raise ValueError('The binning exponent m for the amplitude spectra frequency resolution needs to an integer on >= 8 and <= 12 !')

    def set_report_keyquantiles_yes(self):
        self.config['IOStoreKeyQuantsPerROI'] = 1

    def set_report_keyquantiles_no(self):
        self.config['IOStoreKeyQuantsPerROI'] = 0

    def set_report_histograms_yes(self):
        self.config['IOStoreHistoCnts'] = 1

    def set_report_histograms_no(self):
        self.config['IOStoreHistoCnts'] = 0

    def set_report_fftspectra_yes(self):
        self.config['IOStoreFFTMagn'] = 1

    def set_report_fftspectra_no(self):
        self.config['IOStoreFFTMagn'] = 0
        
    def set_characterize_initial(self):
        #we to characterize only how strong individual specific peaks on the signature are wrt to background, 
        #so no need to store the signatures only a lean summary of quantiles of signature intensities
        self.config['IOStoreKeyQuants'] = 1
        self.config['IOStoreSpecificPeaks'] = 0
        self.config['IOStoreThreeStrongestPeaks'] = 0
        self.config['IOStoreHistoCnts'] = 0
        self.config['IOStoreHistoFFTMagn'] = 0

    def set_characterize_crystallinity(self):
        #we need to collect the signatures to index them
        ###MK::TODO will replace set_characterize_initial at some point
        self.config['IOStoreKeyQuants'] = 1
        self.config['IOStoreSpecificPeaks'] = 1
        self.config['IODBScanOnSpecificPeaks'] = 0
        self.config['IOStoreThreeStrongestPeaks'] = 0
        self.config['IOStoreHistoCnts'] = 0
        self.config['IOStoreHistoFFTMagn'] = 0
        
    def set_characterize_crystallinity_compressed(self, thrs):
        #we need to collect the signatures and then compress them
        self.config['IOStoreKeyQuants'] = 1
        self.config['IOStoreSpecificPeaks'] = 1
        self.config['IODBScanOnSpecificPeaks'] = 1
        if thrs <= 1.0 and thrs >= 0.1:
            self.config['DBScanIntensityThreshold'] = np.around(thrs, decimals=3)
        else:
            raise ValueError('The specific peak intensity threshold should be on the interval [0.1, 1.0]')
        #use defaults for minpts and epsilon for the DBScan
        self.config['IOStoreThreeStrongestPeaks'] = 0
        self.config['IOStoreHistoCnts'] = 0
        self.config['IOStoreHistoFFTMagn'] = 0       

    def set_prepare_indexing(self):
        #we need to collect the signatures to index them
        self.config['IOStoreSpecificPeaks'] = 1
        self.config['IOStoreKeyQuants'] = 1
        self.config['IOStoreThreeStrongestPeaks'] = 0
        self.config['IOStoreHistoCnts'] = 0
        self.config['IOStoreHistoFFTMagn'] = 0

    def get_which_bin(self, h, k, l, a, b, c, alpha, beta, gamma, m, R):
        #http://duffy.princeton.edu/sites/default/files/pdfs/links/xtalgeometry.pdf
        #convenience function telling us in which bin a certain dhkl reflects is expected
        ###MK::test 1/dhkl**2 solution existence
        alp = alpha / 180.0 * np.pi
        bet = beta / 180.0 * np.pi
        gam = gamma / 180.0 * np.pi
        S11 = (b**2)*(c**2)*(np.sin(alp)**2)
        S22 = (a**2)*(c**2)*(np.sin(bet)**2)
        S33 = (a**2)*(b**2)*(np.sin(gam)**2)
        S12 = a*b*(c**2)*(np.cos(alp)*np.cos(bet)-np.cos(gam))
        S23 = (a**2)*b*c*(np.cos(bet)*np.cos(gam)-np.cos(alp))
        S13 = a*(b**2)*c*(np.cos(gam)*np.cos(alp)-np.cos(bet))
        #unit cell volume squared
        V2 = (a**2)*(b**2)*(c**2)*(1.0 - (np.cos(alp)**2) - (np.cos(bet)**2) - (np.cos(gam)**2) + 2.0*np.cos(alp)*np.cos(bet)*np.cos(gam))
        c2 =  +  + (2.0*(a**2)*b*c*(np.cos(bet)*np.cos(gam)-np.cos(alp))*k*l)
        dhkl = (((S11*(h**2) + S22*(k**2) + S33*(l**2) + 2.0*S12*h*k + 2.0*S23*k*l + 2.0*S13*h*l) / V2)**(-1))**0.5
        L = 2.0**m
        dR = 2.0*R/(L-2.0)
        fs = L/(2.0*(R+dR))
        f = 1.0/dhkl
        binID = np.floor(f/fs*L)
        print('hkl = {' + str(h) + ' ' + str(k) + ' ' + str(l) + '} with m = ' + str(m) + ' and R = ' + str(R) + ' nm')
        print('dhkl = ' + str(dhkl))
        print('binID = ' + str(binID))        
        
    def add_candidate(self, trg, rmi, rmx):
        if not trg == '' and rmi < rmx and rmi > 0.0:
            #add more checks to it using Python periodic table
            ccmb = '<entry targets="' + trg + '" realspace="' + str(np.around(0.5*(rmi+rmx), decimals=4)) + '" realspacemin="' + str(np.around(rmi, decimals=4)) + '" realspacemax="' + str(np.around(rmx, decimals=4)) + '"/>'
            self.candidates.append( ccmb )
        else:
            raise ValueError('Either the target is empty, or 0.0 < rmi < rmx does not hold !')
    
    def run(self, nmpi = 1): #*args, **kwargs):
        #let XML generator class create the XML file
        #needs an XML top-level group name, here 'ConfigSurfacer' because we want sth specific for surfacer tool
        #and a dictionary with the settings
        self.xml = paraprobe_xml( 'Araullo', self.simid, 'ConfigAraullo', self.config, self.help, candidates = self.candidates )
        #sh = 'paraprobe_bash()'
        
        #create the specific bash command to run this analysis
        if nmpi >= 1:
            sid = str(np.uint32(self.simid))
            prefix = 'PARAPROBE.Araullo.SimID.' + sid
            execnm = 'paraprobe_araullo'
            #sh = 'mpiexec -n ' + str(np.uint32(nmpi)) + ' ' + execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            sh = execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            return sh
        else:
            raise ValueError('We need to use at least one process when working with the Message Passing Interface !')     

# =============================================================================
# exit
# #example usage
# #take your reconstruction and computed distances to characterize where crystallographic signal 
#for a particular crystal structure candidate is at all strongcompute spatial statistics
#task = paraprobe_araullo( 1, 'PARAPROBE.Transcoder.Results.SimID.1.apth5', 'PARAPROBE.Surfacer.Results.SimID.1.h5' )
#use a 10 random ROIs with 2.0nm and 2**(8-1) bin resolved amplitude spectra
#task.set_roi_random( 10 )
#task.set_roi_rmax( 2.0 )
#task.set_sdm_binning( 8 )
#characterize crystallinity
#task.set_characterize_crystallinity()
# ##MK::implement check for spatial resolution of inter-plane distances based on roi_rmax and m
#assume ideal fcc Al lattice as candidate crystal structure
#task.add_candidate( 'Al', 0.18225, 0.22275 )
#create XML configuration file to be used for executing paraprobe-araullo
#cmd1a = task.run()
# cmd1b = task.run( 1 )
# cmd1c = task.run( nmpi = 1 )
# 
# #alternatively, run with two process
# cmd2 = task.run( 2 )
# =============================================================================
