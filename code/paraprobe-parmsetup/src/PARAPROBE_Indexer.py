# -*- coding: utf-8 -*-
"""
created 2020/05/29, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for creating XML and shell config files for paraprobe-araullo
"""
import os, sys
import glob
from pathlib import Path
import numpy as np

from PARAPROBE_XML import *
#import PARAPROBE_Shell.py

config = { 'Healthy': True,
           'InputfileTestOris': 'PARAPROBE.Indexer.CubicDeg1SO3Grid.h5',
           'InputfileAraullo': '',
           'AnalysisMode': 3,
           'IndexSoManyHighestPeaks': 1000,
           'IOUpToKthClosest': 1,
           'IOSolutionQuality': 1,
           'IOSolutionDisori': 0,
           'IODisoriMatrices': 0,
           'IOSymmReduction': 0,
           'IOSymmRedDisoriAngle': 0.5,
           'CachePerIndexingEpoch': 16,
           'GPUsPerNode': 0,
           'GPUWorkload': 1   }

#we should inform on particular config keys, does not need inform for every config key
info = { 'IndexSoManyHighestPeaks': 'how many of the nodes with strongest signal of a signature should be used',
         'AnalysisMode': 'currently only 3 for end users, other options for this parameter are beta stage expert options only for developers',
         'IOSolutionDisori': 'use 0, other options only to be used for paraprobe-intersector for developers',
         'IODisoriMatrices': 'use 0, like IOSolutionDisori, other options also for developers only',
         'IOSymmReduction': 'use 0',
         'IOSymmRedDisoriAngle': 'use 0.5',
         'CachePerIndexingEpoch': 'in gigabytes, choose at most maximum amount of main memory minus 4 gb for operating system and temporaries',
         'GPUsPerNode': 'leave 0, paraprobe-indexer does not use GPUs at the moment',
         'GPUWorkload': 'leave 1, paraprobe-indexer does not use GPUs at the moment'  }

class paraprobe_indexer():

    def __init__(self, simid, ara, *args, **kwargs):
        self.simid = simid
        self.toolname = 'Indexer'
        self.config = config
        self.help = info
        self.refimages = []
        
        if not ara.endswith('.h5'):
            raise ValueError('The filename pointing to the paraprobe-araullo results is not an HDF5 file !')
        else:
            self.config['InputfileAraullo'] = ara

    def set_number_of_pks(self, N):
        if N >= 1000:
            self.config['IndexSoManyHighestPeaks'] = np.uint32(N)
        else:
            if N > 1:
                print('WARNING: Using few peaks from the signature will make the indexing ambiguous, if not impossible !')
                self.config['IndexSoManyHighestPeaks'] = np.uint32(N)
            else:
                raise ValueError('IndexSoManyHighestPeaks via N should be at least positive and finite  use e.g. 1000 !')
                
    def set_number_of_solutions(self, N):
        if N > 10:
            print('WARNING: Solutions are listed in increasing image difference between signature and candidate')
            print('WARNING: Choosing a large number of solutions does not make the best more significant, fewer are equally good and process more efficiently')
            self.config['IOUpToKthClosest'] = np.uint32(N)
        else:
            if N >= 1:
                print('Solutions are listed in increasing image difference between signature and candidate')
                self.config['IOUpToKthClosest'] = np.uint32(N)
            else:
                raise ValueError('IOUpToKthClosest via N needs to be at least 1 !')

    def set_cache_size(self, gb):
        if gb >= 4:
            self.config['CachePerIndexingEpoch'] = np.around(np.float32(gb), decimals=3)
        else:
            if gb >= 1:
                self.config['CachePerIndexingEpoch'] = np.around(np.float32(gb), decimals=3)
            else:
                raise ValueError('CachePerIndexingEpoch via gb should be at least 1 GB !')    
        
    def add_candidate(self, refnm, imgnm, phnm, refori, trgs):
        if not refnm == '' and not imgnm == '' and not phnm == '' and not refori == '' and not trgs == '':
            #add more checks to it using Python periodic table
            ccmb = '<entry refname="' + refnm + '" imagename="' + imgnm + '" usetoindex="' + phnm + '" refori="' + refori + '" targets="' + trgs + '"/>'
            self.refimages.append( ccmb )
        else:
            raise ValueError('At least one of the arguments is empty !')
    
    def run(self, nmpi = 1): #*args, **kwargs):
        #let XML generator class create the XML file
        #needs an XML top-level group name, here 'ConfigSurfacer' because we want sth specific for surfacer tool
        #and a dictionary with the settings
        self.xml = paraprobe_xml( 'Indexer', self.simid, 'ConfigIndexer', self.config, self.help, refimages = self.refimages )
        #sh = 'paraprobe_bash()'
        
        #create the specific bash command to run this analysis
        if nmpi >= 1:
            sid = str(np.uint32(self.simid))
            prefix = 'PARAPROBE.Indexer.SimID.' + sid
            execnm = 'paraprobe_indexer'
            #sh = 'mpiexec -n ' + str(np.uint32(nmpi)) + ' ' + execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            sh = execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            return sh
        else:
            raise ValueError('We need to use at least one process when working with the Message Passing Interface !')

## =============================================================================
## exit
## #example usage
##take the result from paraprobe-araullo and use it to index the references 
##for a particular crystal structure candidate is at all strongcompute spatial statistics
##task = paraprobe_indexer( 1, 'PARAPROBE.Araullo.Results.SimID.1.h5' )
##we have the following PRECOMPUTED candidate crystal structures and need to specify references to these files now
##lets assume we have only one candidate, perfect fcc Al single crystal in 8.0, 8.0, 8.0 Bunge-Euler orientation and would like to use 
##it then we say
##task.add_candidate('PARAPROBE.Araullo.Results.SimID.210001.h5', '/AraulloPeters/Results/SpecificPeak/PH0/0', 'PH0', '8.0;8.0;8.0', 'Al' )
##first argument tells the program in which file it can find the signature of a candidate
##second argument tells the program where in the file (dataset) the signature for the candidate is
##third argument tells for which Phase of the araullo results this candidate should be used
##fourth argument tells the rotation used for computing the signature of the condidate, necessary to create the correctly rotated candidates
##fifth argument tells which ion type label

##add more candidates if desired here ...

##task.set_number_of_pks( 1000 )
##task.set_number_of_solutions( 1 )
##choose cache memory, this is memory to is used at runtime to cache read araullo results and index them
##internally we first read araullo results (the raw data for the indexing) for a portion of ROIs and then index these ROIs
##next we discard these araullo raw data and load the next batch of raw data, cache size says how much memory we give to store raw data
##task.set_cache_size( 16 )

#cmd1a = task.run()
## cmd1b = task.run( 1 )
## cmd1c = task.run( nmpi = 1 )
## 
## #alternatively, run with two process
## cmd2 = task.run( 2 )
## =============================================================================
