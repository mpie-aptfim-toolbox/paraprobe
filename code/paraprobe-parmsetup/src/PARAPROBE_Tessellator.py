# -*- coding: utf-8 -*-
"""
created 2020/05/29, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for creating XML and shell config files for paraprobe-tessellator
"""
import sys
import glob
from pathlib import Path
import numpy as np

from PARAPROBE_XML import *


config = { 'Healthy': True,
           'InputfilePSE': 'PARAPROBE.PeriodicTableOfElements.xml',
           'InputfileReconstruction': '', 
           'InputfileDistances': '',
           'SpatialSplittingModel': 0,
           'CellErosionDistance': 0.25, 
           'IOCellVolume': 1,
           'IOCellNeighbors': 0,
           'IOCellShape': 0,
           'IOCellProfiling': 1  }

#we should inform on particular config keys, does not need inform for every config key
info = { 'SpatialSplittingModel': '0 aim for near equal ion count per thread, 1 aim for near distance weights per thread (not implemented)',
         'CellErosionDistance': 'in nm, remove Voronoi cells so close to the dataset edge',
         'IOCellVolume': 'all IO options are active when 1, else inactive',
         'IOCellNeighbors': '(not implemented, contact m.kuehbach at mpie.de if of interest)',
         'IOCellShape': '(not implemented, contact m.kuehbach at mpie.de if of interest)',
         'IOCellProfiling': 'elapsed time taken to compute the cell in s' }

class paraprobe_tessellator():

    def __init__(self, simid, recon, hull, *args, **kwargs):
        self.simid = simid
        self.toolname = 'Tessellator'
        self.config = config
        self.help = info
        #self.icombis = []
        
        if not recon.endswith('.h5'):
            raise ValueError('The filename pointing to the reconstruction is not an H5 file !')
        else:
            self.config['InputfileReconstruction'] = recon
        if not hull.endswith('.h5'):
            raise ValueError('The filename pointing to the ion-to-edge distances is not an HDF5 file !')
        else:
            self.config['InputfileDistances'] = hull
    
    def set_cell_erosion(self, d):
        if d >= 0.0:
            self.config['CellErosionDistance'] = np.around(np.float32(d), decimals=3)
        else:
            raise ValueError('The cell erosion distance has needs to be positive !')
   
    def run(self, nmpi = 1): #*args, **kwargs):
        #let XML generator class create the XML file
        #needs an XML top-level group name, here 'ConfigSurfacer' because we want sth specific for surfacer tool
        #and a dictionary with the settings
        self.xml = paraprobe_xml( 'Tessellator', self.simid, 'ConfigTessellator', self.config, self.help )
        #sh = 'paraprobe_bash()'
        
        #create the specific bash command to run this analysis
        if nmpi >= 1:
            sid = str(np.uint32(self.simid))
            prefix = 'PARAPROBE.Tessellator.SimID.' + sid
            execnm = 'paraprobe_tessellator' #'./paraprobe_tessellator'
            #sh = 'mpiexec -n ' + str(np.uint32(nmpi)) + ' ' + execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            sh = execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            return sh
        else:
            raise ValueError('We need to use at least one process when working with the Message Passing Interface !')     

# =============================================================================
#example usage
# compute a Poisson-Voronoi tessellation of the datasets and report the cell volume and 
# computing time for all cells at least 2.0nm from the dataset edge
#task = paraprobe_tessellator( 1, 'R6754.Transcoder.Results.apth5', 'R6754.Surfacer.Results.h5' )
#task.set_cell_erosion( 2.0 )
#and create the XML configuration file and the command line entry for running spatstat using mpi and openmp
# ##MK::give dependencies to identify what the file needs to build workflows automatically, for now manual scripting
# #three equivalent version to instruct running with one process
#cmd1a = task.run()
# cmd1b = task.run( 1 )
# cmd1c = task.run( nmpi = 1 )
# 
# #alternatively, run with two process
# cmd2 = task.run( 2 )
# =============================================================================
