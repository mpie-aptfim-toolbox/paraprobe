# -*- coding: utf-8 -*-
"""
created 2020/05/28, Markus K\"uhbach, m.kuehbach@mpie.de
definitions for HDF5 groups and datasets for results of the paraprobe-surfacer tool
"""

PARAPROBE_RANGER='/Ranging'
#PARAPROBE_RANGER_HRDWR='/Ranging/HardwareDetails'
#PARAPROBE_RANGER_HRDWR_META='/Ranging/HardwareDetails/Metadata'
#PARAPROBE_RANGER_HRDWR_META_KEYS='/Ranging/HardwareDetails/Metadata/Keywords'
#PARAPROBE_RANGER_SFTWR='/Ranging/SoftwareDetails'
#PARAPROBE_RANGER_SFTWR_META='/Ranging/SoftwareDetails/Metadata'
#PARAPROBE_RANGER_SFTWR_META_KEYS='/Ranging/SoftwareDetails/Metadata/Keywords'
PARAPROBE_RANGER_META='/Ranging/Metadata'
PARAPROBE_RANGER_META_TYPID_DICT='/Ranging/Metadata/Iontypes'					
PARAPROBE_RANGER_META_TYPID_DICT_ID='/Ranging/Metadata/Iontypes/IDs'
PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX=1
PARAPROBE_RANGER_META_TYPID_DICT_WHAT='/Ranging/Metadata/Iontypes/What'
PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX=8
PARAPROBE_RANGER_META_TYPID_DICT_IVAL='/Ranging/Metadata/Iontypes/MQIVal'
PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX=2
PARAPROBE_RANGER_META_TYPID_DICT_ASSN='/Ranging/Metadata/Iontypes/MQ2IDs'
#//to which Iontypes/IDs do the range belong, each type can have multiple ranges!
PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX=1

#PARAPROBE_RANGER_RES='/Ranging/Results'
#PARAPROBE_RANGER_RES_TYPID='/Ranging/Results/IontypeIDs'
#PARAPROBE_RANGER_RES_TYPID_NCMAX=1
