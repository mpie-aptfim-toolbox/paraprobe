# -*- coding: utf-8 -*-
"""
created 2020/05/26, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for showing how to run complex workflows of individual analysis with multiple tools
using PARAPROBE. Here, we create a synthetic dataset (single crystal single phase), a 3D grid of spherical regions-of-interest (ROI)
and characterize the crystallographic signal locally within this specimen using the paraprobe-araullo tool
"""

import glob
from pathlib import Path
import numpy as np

#import the paraprobe tools we need
from PARAPROBE_BashBatch import *
from PARAPROBE_SlurmSingle import *
from PARAPROBE_XML import *
#from PARAPROBE_Transcoder import *
from PARAPROBE_Synthetic import *
#from PARAPROBE_RNG2RRNG import *
from PARAPROBE_Ranger import *
from PARAPROBE_Surfacer import *
#from PARAPROBE_Spatstat import *
from PARAPROBE_Araullo import *
#from PARAPROBE_DBScan import *
#from PARAPROBE_CharParticles import *
#from PARAPROBE_Isosurfacer import *


N = np.uint32(1)
AnalysisJobs = {}
for i in np.arange(1,N+1):
    #define a simulation ID, unique for each workflow to
    i = 1
    SimulationID = i
    ProcessesToUse = 1 #use one MPI process to execute, multithreading is on by default#use one MPI process to execute, multithreading is on by default
    
    #define a dictionary of what we need to do for the workflow
    WhatToDo = {}
    
    #first, we create the synthetic dataset
    task = paraprobe_synthetic( SimulationID )
    task.set_tip_geom_natoms( 25.0e6 ) #25 million atoms
    task.set_tip_geom_radius_bottom( 0.5 )
    task.set_tip_geom_radius_top( 0.5 ) #cylindrical dataset
    
    tza
    
    
    
    #first, we transcode IVAS POS to paraprobe HDF5
    task = paraprobe_transcoder( SimulationID, dataset + '.pos' ) #this creates the XML settings file
    ###MK::make changes if desired to the internal XML settings file
    #the numbers in front of the keys words for the WhatToDo dictionaries are necessary!!
    #they assure that the commands in the slurm scripts (slm variable below) end up in order!!
    WhatToDo['1_Transcode'] = task.run( ProcessesToUse )
    #will give me a reconstruction in HDF5 format
    recon_fnm = task.get_recon_filename()
    
    #second, we range using our range files
    range_fnm = dataset + '.rrng'
    task = paraprobe_ranger( SimulationID, recon_fnm, range_fnm )
    WhatToDo['2_Ranger'] = task.run( ProcessesToUse )
    
    #third, we compute an alpha shape to the specimen
    task = paraprobe_surfacer( SimulationID, recon_fnm )
    #make changes to the default settings ##MK::implement getters for the default settings
    task.set_roi_rmax( 5.0 )
    task.set_distancing_complete()
    WhatToDo['3_Surfacer'] = task.run( ProcessesToUse )
    hull_fnm = task.get_hull_filename()
    
    #fourth, we characterize spatial statistics using the alpha shape and computed distances from the hull file
    task = paraprobe_spatstat( SimulationID, recon_fnm, hull_fnm )
    #these ion combinations e.g. for an Al-Li-Mg-Ag alloy maybe
    task.add_iontype_combi( 'Al', 'Al' )
    task.add_iontype_combi( 'Al', 'Zn' )
    task.add_iontype_combi( 'Al', 'Mg' )
    task.add_iontype_combi( 'Al', 'Cu' )
    task.add_iontype_combi( 'Zn', 'Al' )
    task.add_iontype_combi( 'Zn', 'Zn' )
    task.add_iontype_combi( 'Zn', 'Mg' )
    task.add_iontype_combi( 'Zn', 'Cu' )
    task.add_iontype_combi( 'Mg', 'Al' )
    task.add_iontype_combi( 'Mg', 'Zn' )
    task.add_iontype_combi( 'Mg', 'Mg' )
    task.add_iontype_combi( 'Mg', 'Cu' )
    task.add_iontype_combi( 'Cu', 'Al' )
    task.add_iontype_combi( 'Cu', 'Zn' )
    task.add_iontype_combi( 'Cu', 'Mg' )
    task.add_iontype_combi( 'Cu', 'Cu' )
    #okay, lets do some spatial statistics
    #e.g. radial distribution function
    task.set_rdf( 0.001, 5.000 )    
    #e.g. kth nearest neighbors for all combinations 
    task.set_knn( 0.001, 5.000, np.array([1]) ) #, 10, 100]) )
    #yes a single line gives you all combinations, easy peasy
    #e.g. why not also three-dimensional environment i.e. directional spatial statistics, i.e. 3D spatial distribution maps
    #task.set_sdm( 0.1, 5.0, np.array([1, 10]) )
    WhatToDo['4_Spatstat'] = task.run( ProcessesToUse )
    
    #you could add more analysis steps here, only your imagination is the limit... for example ...
    
# =============================================================================
#     #fifth, we compile a graphical report from the results to assist us in scanning the results of all these analyses, easy peasy
#         
#     #sixth, we run a DBScan maximum separation clustering analysis on the datasets
#     
#     #seventh, we use the results from the maximum separation analysis and computed hull to characterize which particles are cut by the dataset edge
#     #and some other characterization of the size distributions corrected for bias by edge and others
#     
#     #eigth, we run a few isosurface analyses on this (and compute the distances of the ions to the isosurfaces)
#     
#     #for paper20 on HDF5 file format 
#     #ninth, we run clustering methods for other folks in the community for instance Iman Ghamarian and Emmanuel Marquis' Hierarchical DBScan
#     #the benefit of our suggestion to use a common format for presenting results complementary to how individual researchers report their results
#     #is the following:
#     #lets run again our characterize particles tool and compare heads up the two methods
#     
#     #see it has never been so easy to make cross-method comparisons using state-of-the-art scientific computing
#     #we only ask for a minimal amount of agreement how to exchange data and ask people to write a few lines of code
#     #that will boost their professional career, APT data analysis made FAIR and simple!
# =============================================================================

    #now fuse all these steps into a workflow, write a slurm script (to be submitted to workstation or super computer) and 
    slm = paraprobe_slurm_single( dataset[0:9], SimulationID, 'TALOS', ProcessesToUse, WhatToDo, 'ITL' ) #use the GCC compiler, ##MK::specific for TALOS
    slm.write_slurm_script()
    
    #remember that we want to submit this job
    AnalysisJobs[N] = slm.get_slurm_script_filename()
    #increment SimulationID
    N = N + np.uint32(1)

#this last thing here makes the submission to cluster even simpler, fuse all these shell scripts into one to submit to cluster at once

sh = paraprobe_bash_batch( 'HuanZhaoScriptaMat2018', AnalysisJobs )
#sh.write_bash_script()

#so now just source HuanZhaoScriptaMat2018.sh and let the cluster do its work, 100% FAIR

#alternatively, we could also submit the scripts individually
#i.e. sbatch SLURM.PARAPROBE.Workflow.HuanZhaoScriptMat2018.SimID.*.sh
#now t