# -*- coding: utf-8 -*-
"""
created 2020/05/26, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for showing how to run complex workflows of individual analysis with multiple tools
using PARAPROBE. We use both trivial and non-trivial parallelism on a cluster computer to process a large number of reconstructed 
atom probe datasets with existent reconstruction (POS file from IVAS) and ranging (RNG from IVAS) the data here are from
H. Zhao et al. Scripta Mat, 2018, dx.doi.org/10.1016/j.scriptamat.2018.05.024
"""

import glob
from pathlib import Path
import numpy as np

#import the paraprobe tools we need
from PARAPROBE_BashBatch import *
from PARAPROBE_SlurmSingle import *
from PARAPROBE_BashSingle import *
from PARAPROBE_XML import *
from PARAPROBE_Transcoder import *
#from PARAPROBE_Synthetic import *
from PARAPROBE_RNG2RRNG import *
from PARAPROBE_Ranger import *
from PARAPROBE_Surfacer import *
from PARAPROBE_Spatstat import *
from PARAPROBE_Tessellator import *
from PARAPROBE_DBScan import *
#from PARAPROBE_CharParticles import *
#from PARAPROBE_Isosurfacer import *

Compiler = 'GCC'  #last argument is compiler either 'ITL' or 'GCC'

#define the datasets (reconstructed and with ranging data)
ds = np.array([  'R76-25946-2h', 
                 'R76-26865-PA',
                 'R76-26737-PA',
                 'R76-26110-30min',
                 'R76-25139-2h',
                 'R76-24909-30min',
                 'R76-26120-OA'    ])
ds = np.array([  'R76-26120-OA'    ])
#you could add much more datasets here, only your computational resources define the limit...

#transcode existent RNG range file into RRNG format
for dataset in ds:
    print('Transcoding RNG range file format to RRNG range file format...')
    print(str(dataset) + '.rng')
    task = paraprobe_rng2rrng( dataset + '.rng' )
    task.write_rrng()
    rrngfilename = task.get_rrng_filename()

#now set up an ensemble of paraprobe analyses, one analysis, i.e. workflow for each above dataset
#MK::better one workflow per dataset instead of all datasets into the same workflow
#because such we can run all datasets at the same time and in parallel
#so combination of trivial and non-trivial parallelism used the easy way
#this is the same as if we have seven students and give each one dataset and an IVAS license
#and instruct all students to start working. There is a better alternative to this: 

N = np.uint32(1)
AnalysisJobs = {}
for dataset in ds:
    #define a simulation ID, unique for each workflow to
    SimulationID = N
    ProcessesToUse = 1 #use one MPI process to execute, multithreading is on by default#use one MPI process to execute, multithreading is on by default
    
    #define a dictionary of what we need to do for the workflow
    WhatToDo = {}
    
    #first, we transcode IVAS POS to paraprobe HDF5
    task = paraprobe_transcoder( SimulationID, dataset + '.pos' ) #this creates the XML settings file
    ###MK::make changes if desired to the internal XML settings file
    #the numbers in front of the keys words for the WhatToDo dictionaries are necessary!!
    #they assure that the commands in the slurm scripts (slm variable below) end up in order!!
    WhatToDo['1_Transcode'] = task.run( ProcessesToUse )
    #will give me a reconstruction in HDF5 format
    recon_fnm = task.get_recon_filename()
    del task
    
    #second, we range using our range files
    range_fnm = dataset + '.rrng'
    task = paraprobe_ranger( SimulationID, recon_fnm, range_fnm )
    WhatToDo['2_Ranger'] = task.run( ProcessesToUse )
    
    #third, we compute an alpha shape to the specimen
    task = paraprobe_surfacer( SimulationID, recon_fnm )
    #make changes to the default settings ##MK::implement getters for the default settings
    #task.set_roi_rmax( 1.0 )
    task.set_distancing_skin( 1.0 )
    #task.set_distancing_complete()
    WhatToDo['3_Surfacer'] = task.run( ProcessesToUse )
    hull_fnm = task.get_hull_filename()
    del task
    
    #fourth, we characterize spatial statistics using the alpha shape and computed distances from the hull file
    task = paraprobe_spatstat( SimulationID, recon_fnm, hull_fnm )
    #these ion combinations e.g. for an Al-Li-Mg-Ag alloy maybe
    task.add_iontype_combi( 'Al', 'Al' )
    task.add_iontype_combi( 'Al', 'Zn' )
    task.add_iontype_combi( 'Al', 'Mg' )
    task.add_iontype_combi( 'Al', 'Cu' )
    task.add_iontype_combi( 'Zn', 'Al' )
    task.add_iontype_combi( 'Zn', 'Zn' )
    task.add_iontype_combi( 'Zn', 'Mg' )
    task.add_iontype_combi( 'Zn', 'Cu' )
    task.add_iontype_combi( 'Mg', 'Al' )
    task.add_iontype_combi( 'Mg', 'Zn' )
    task.add_iontype_combi( 'Mg', 'Mg' )
    task.add_iontype_combi( 'Mg', 'Cu' )
    task.add_iontype_combi( 'Cu', 'Al' )
    task.add_iontype_combi( 'Cu', 'Zn' )
    task.add_iontype_combi( 'Cu', 'Mg' )
    task.add_iontype_combi( 'Cu', 'Cu' )
    #okay, lets do some spatial statistics
    #e.g. radial distribution function
    task.set_rdf( 0.001, 1.000 )    
    #e.g. kth nearest neighbors for all combinations 
    task.set_knn( 0.001, 1.000, np.array([1]) ) #, 10, 100]) )
    #yes a single line gives you all combinations, easy peasy
    #e.g. why not also three-dimensional environment i.e. directional spatial statistics, i.e. 3D spatial distribution maps
    #task.set_sdm( 0.1, 5.0, np.array([1, 10]) )
    WhatToDo['4_Spatstat'] = task.run( ProcessesToUse )
    del task
    
    #lets also build a tessellation of the dataset, that will be useful for the next step...
    task = paraprobe_tessellator( SimulationID, recon_fnm, hull_fnm )
    task.set_cell_erosion( 1.0 )
    WhatToDo['5_Tessellator'] = task.run( ProcessesToUse )
    del task
    
    #sixth, ...because we can now a maximum separation clustering analysis and use
    #the tessellation for marking all cluster with ions closer than here 1.0 nm to the dataset edge as truncated
    #and thereby analyse clusters in the interior or all
    task = paraprobe_dbscan( SimulationID, recon_fnm, hull_fnm )
    task.set_high_throughput_maximum_separation_method( [0.25, 0.25, 1.00], [5, 10, 5], 1.0 )
    #what is possibly clustering, lets see its a Al-Mg-Zn-Cu alloy, so Mg, Zn, Cu form clusters
    task.add_targets( 'Mg' )
    task.add_targets( 'Zn' )
    task.add_targets( 'Cu' )
    #the clou here is that above maximum separation runs will be done for all these...
    WhatToDo['6_DBScan'] = task.run( ProcessesToUse )
    del task
    
    #you could add more analysis steps here, only your imagination is the limit... for example ...
    
# =============================================================================
#     #fifth, we compile a graphical report from the results to assist us in scanning the results of all these analyses, easy peasy
#     #eigth, we run a few isosurface analyses on this (and compute the distances of the ions to the isosurfaces)
#     
#     #for paper20 on HDF5 file format 
#     #ninth, we run clustering methods for other folks in the community for instance Iman Ghamarian and Emmanuel Marquis' Hierarchical DBScan
#     #the benefit of our suggestion to use a common format for presenting results complementary to how individual researchers report their results
#     #is the following:
#     #lets run again our characterize particles tool and compare heads up the two methods
#     
#     #see it has never been so easy to make cross-method comparisons using state-of-the-art scientific computing
#     #we only ask for a minimal amount of agreement how to exchange data and ask people to write a few lines of code
#     #that will boost their professional career, APT data analysis made FAIR and simple!
# =============================================================================

    #now fuse all these steps into a workflow, write a slurm script (to be submitted to workstation or super computer) 
    slm = paraprobe_slurm_single( dataset[0:9], SimulationID, 'TALOS', ProcessesToUse, WhatToDo, Compiler )
    slm.write_slurm_script()
    #but also write a bash script (to be submitted to a local workstation without super computer)
    bsh = paraprobe_bash_single( dataset[0:9], SimulationID, 'MPI30', ProcessesToUse, WhatToDo, Compiler )
    bsh.write_bash_script()
    
    #remember that we want to submit this job
    AnalysisJobs[N] = slm.get_slurm_script_filename()
    #increment SimulationID
    N = N + np.uint32(1)

#this last line here makes the submission to the cluster even simpler
#it fuses all above slurm shell scripts into one to submit to cluster at once
sh = paraprobe_bash_batch( 'HuanZhaoScrMat2018', AnalysisJobs )
#sh.write_bash_script()

#so now just source HuanZhaoScrMat2018.sh and let the cluster do its work, 100% FAIR

#alternatively, we could also submit the scripts individually
#i.e. sbatch SLURM.PARAPROBE.Workflow.HuanZhaoScrMat2018.SimID.*.sh
#now t