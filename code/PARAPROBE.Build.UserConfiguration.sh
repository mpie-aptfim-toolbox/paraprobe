#!bin/bash

#which tools to compile?
build_utils=1
build_transcoder=1
build_synthetic=1
build_reconstruct=0
build_ranger=1
build_surfacer=1
build_tessellator=1
build_nanochem=0
build_spatstat=1
build_dbscan=1
build_araullo=1
build_fourier=1
build_indexer=1
build_intersector=0

#always clear the build directory prior configurating and compilating?
clean_build=1
#collect executables after successful build of a tool?
collect_executables=1
#what are the target folders where are libraries?
target_home="/home/markus/ParaprobeVideoTutorials/paraprobe/code/"
#hdf5_home="/home/markus/ParaprobeVideoTutorials/paraprobe/code/thirdparty/HDF5"
#cgal_home="/home/markus/ParaprobeVideoTutorials/paraprobe/code/thirdparty/CGAL/CGAL-4.11.3/"
hdf5_home="/home/m.kuehbach/GITHUB/MPIE_APTFIM_TOOLBOX/paraprobe/code/thirdparty/HDF5/CMake-hdf5-1.10.7/HDF5-1.10.7-Linux/HDF_Group/HDF5/1.10.7"
cgal_home="/home/m.kuehbach/GITHUB/MPIE_APTFIM_TOOLBOX/paraprobe/code/thirdparty/CGAL/CGAL-5.1.1"
	
MYHDF5HOME=$hdf5_home
MYCGALHOME=$cgal_home
#check code version integrate GitSHA?
check_codeversion=1
#working with environment modules?
modules_used=0
#which compiler to use (gnu, itel, pgi)?
compiler_which="gnu"
echo "Preparing environment"
#define alias names for the compilers and prep them
MYWHO_COMPILER=GNU
MYCCC_COMPILER=gcc
MYCXX_COMPILER=g++
echo "Compiler choice $MYWHO_COMPILER"
echo "C compiler $MYCCC_COMPILER"
echo "C++ compiler $MYCXX_COMPILER"
echo "PARAPROBE HOME /home/markus/ParaprobeVideoTutorials/paraprobe/code/"
echo "HDF5 HOME /home/markus/ParaprobeVideoTutorials/paraprobe/code/thirdparty/HDF5"
echo "CGAL HOME /home/markus/ParaprobeVideoTutorials/paraprobe/code/thirdparty/CGAL/CGAL-4.11.3/"

echo "Identifying the paraprobe version"
MYGITSHA="$(git describe --abbrev=50 --dirty --broken --always --tags)"
echo $MYGITSHA

echo "Compiling paraprobe-utils..."
mkdir -p paraprobe-utils/build
cd paraprobe-utils/build
if [ "$clean_build" == 1 ]; then
	echo "Cleaning $PWD"
	rm -rf *
fi
echo "Configuring paraprobe-utils..."
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER .. 1>PARAPROBE.Utils.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Utils.CMake.$MYWHO_COMPILER.STDERR.txt
echo "Compiling paraprobe-utils..."
make 1>PARAPROBE.Utils.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Utils.Make.$MYWHO_COMPILER.STDERR.txt
#DO NOT DELETE THESE GENERATED FILES below TOOLS WILL INCLUDE SOME OF THEM
cd ../../
if [ "$build_transcoder" == 1 ]; then
	echo "Compiling paraprobe-transcoder..."
	mkdir -p paraprobe-transcoder/build
	cd paraprobe-transcoder/build
	if [ "$clean_build" == 1 ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-transcoder..."
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Transcoder.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Transcoder.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compiling paraprobe-transcoder..."
	make 1>PARAPROBE.Transcoder.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Transcoder.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_transcoder
	if [ "$collect_executables" == 1 ]; then
		cp paraprobe_transcoder $target_home/paraprobe_transcoder
	fi
	cd ../../
fi
if [ "$build_synthetic" == 1 ]; then
	echo "Compiling paraprobe-synthetic..."
	mkdir -p paraprobe-synthetic/build
	cd paraprobe-synthetic/build
	if [ "$clean_build" == 1 ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-synthetic..."
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Synthetic.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Synthetic.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compiling paraprobe-synthetic..."
	make 1>PARAPROBE.Synthetic.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Synthetic.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_synthetic
	if [ "$collect_executables" == 1 ]; then
		cp paraprobe_synthetic $target_home/paraprobe_synthetic
	fi
	cd ../../
fi
if [ "$build_reconstruct" == 1 ]; then
	echo "Compiling paraprobe-reconstruct..."
	mkdir -p paraprobe-reconstruct/build
	cd paraprobe-reconstruct/build
	if [ "$clean_build" == 1 ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-reconstruct..."
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Reconstruct.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Reconstruct.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compiling paraprobe-reconstruct..."
	make 1>PARAPROBE.Reconstruct.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Reconstruct.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_reconstruct
	if [ "$collect_executables" == 1 ]; then
		cp paraprobe_reconstruct $target_home/paraprobe_reconstruct
	fi
	cd ../../
fi
if [ "$build_ranger" == 1 ]; then
	echo "Compiling paraprobe-ranger..."
	mkdir -p paraprobe-ranger/build
	cd paraprobe-ranger/build
	if [ "$clean_build" == 1 ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-ranger..."
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Ranger.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Ranger.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compiling paraprobe-ranger..."
	make 1>PARAPROBE.Ranger.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Ranger.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_ranger
	if [ "$collect_executables" == 1 ]; then
		cp paraprobe_ranger $target_home/paraprobe_ranger
	fi
	cd ../../
fi
if [ "$build_surfacer" == 1 ]; then
	echo "Compiling paraprobe-surfacer..."
	mkdir -p paraprobe-surfacer/build
	cd paraprobe-surfacer/build
	if [ "$clean_build" == 1 ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-surfacer..."
	cmake -DCMAKE_BUILD_TYPE=Release -DCGAL_DIR=$MYCGALHOME -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Surfacer.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Surfacer.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compiling paraprobe-surfacer..."
	make 1>PARAPROBE.Surfacer.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Surfacer.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_surfacer
	if [ "$collect_executables" == 1 ]; then
		cp paraprobe_surfacer $target_home/paraprobe_surfacer
	fi
	cd ../../
fi
if [ "$build_tessellator" == 1 ]; then
	echo "Compiling paraprobe-tessellator..."
	mkdir -p paraprobe-tessellator/build
	cd paraprobe-tessellator/build
	if [ "$clean_build" == 1 ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-tessellator..."
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Tessellator.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Tessellator.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compiling paraprobe-tessellator..."
	make 1>PARAPROBE.Tessellator.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Tessellator.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_tessellator
	if [ "$collect_executables" == 1 ]; then
		cp paraprobe_tessellator $target_home/paraprobe_tessellator
	fi
	cd ../../
fi
if [ "$build_nanochem" == 1 ]; then
	echo "Compiling paraprobe-nanochem..."
	mkdir -p paraprobe-nanochem/build
	cd paraprobe-nanochem/build
	if [ "$clean_build" == 1 ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-nanochem..."
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Nanochem.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Nanochem.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compiling paraprobe-nanochem..."
	make 1>PARAPROBE.Nanochem.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Nanochem.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_nanochem
	if [ "$collect_executables" == 1 ]; then
		cp paraprobe_nanochem $target_home/paraprobe_nanochem
	fi
	cd ../../
fi
if [ "$build_spatstat" == 1 ]; then
	echo "Compiling paraprobe-spatstat..."
	mkdir -p paraprobe-spatstat/build
	cd paraprobe-spatstat/build
	if [ "$clean_build" == 1 ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-spatstat..."
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Spatstat.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Spatstat.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compiling paraprobe-spatstat..."
	make 1>PARAPROBE.Spatstat.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Spatstat.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_spatstat
	if [ "$collect_executables" == 1 ]; then
		cp paraprobe_spatstat $target_home/paraprobe_spatstat
	fi
	cd ../../
fi
if [ "$build_dbscan" == 1 ]; then
	echo "Compiling paraprobe-dbscan..."
	mkdir -p paraprobe-dbscan/build
	cd paraprobe-dbscan/build
	if [ "$clean_build" == 1 ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-dbscan..."
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Dbscan.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Dbscan.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compiling paraprobe-dbscan..."
	make 1>PARAPROBE.Dbscan.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Dbscan.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_dbscan
	if [ "$collect_executables" == 1 ]; then
		cp paraprobe_dbscan $target_home/paraprobe_dbscan
	fi
	cd ../../
fi
if [ "$build_araullo" == 1 ]; then
	echo "Compiling paraprobe-araullo..."
	mkdir -p paraprobe-araullo/build
	cd paraprobe-araullo/build
	if [ "$clean_build" == 1 ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-araullo..."
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Araullo.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Araullo.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compiling paraprobe-araullo..."
	make 1>PARAPROBE.Araullo.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Araullo.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_araullo
	if [ "$collect_executables" == 1 ]; then
		cp paraprobe_araullo $target_home/paraprobe_araullo
	fi
	cd ../../
fi
if [ "$build_fourier" == 1 ]; then
	echo "Compiling paraprobe-fourier..."
	mkdir -p paraprobe-fourier/build
	cd paraprobe-fourier/build
	if [ "$clean_build" == 1 ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-fourier..."
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Fourier.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Fourier.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compiling paraprobe-fourier..."
	make 1>PARAPROBE.Fourier.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Fourier.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_fourier
	if [ "$collect_executables" == 1 ]; then
		cp paraprobe_fourier $target_home/paraprobe_fourier
	fi
	cd ../../
fi
if [ "$build_indexer" == 1 ]; then
	echo "Compiling paraprobe-indexer..."
	mkdir -p paraprobe-indexer/build
	cd paraprobe-indexer/build
	if [ "$clean_build" == 1 ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	echo "Configuring paraprobe-indexer..."
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Indexer.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Indexer.CMake.$MYWHO_COMPILER.STDERR.txt
	echo "Compiling paraprobe-indexer..."
	make 1>PARAPROBE.Indexer.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Indexer.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_indexer
	if [ "$collect_executables" == 1 ]; then
		cp paraprobe_indexer $target_home/paraprobe_indexer
	fi
	cd ../../
fi
if [ "$build_intersector" == 1 ]; then
	echo "Compiling TetGen library..."
	cd paraprobe-intersector/src/thirdparty/TetGen/tetgen1.5.1
	rm *.o *.a
	make CC=$MYCCC_COMPILER CXX=$MYCXX_COMPILER tetlib 1>TetGen.Make.$MYWHO_COMPILER.STDOUT.txt 2>TetGen.Make.$MYWHO_COMPILER.STDERR.txt
	cd ../../../../../
	echo "Compiling paraprobe-intersector..."
	mkdir -p paraprobe-intersector/build
	cd paraprobe-intersector/build
	if [ "$clean_build" = true ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>PARAPROBE.Intersector.CMake.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Intersector.CMake.$MYWHO_COMPILER.STDERR.txt
	make 1>PARAPROBE.Intersector.Make.$MYWHO_COMPILER.STDOUT.txt 2>PARAPROBE.Intersector.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x paraprobe_intersector
	if [ "$collect_executables" = true ]; then
		cp paraprobe_intersector $target_home/paraprobe_intersector
	fi
	cd ../../
fi

#add additional tools
