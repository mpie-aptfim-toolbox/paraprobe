//##MK::GPLV3

#ifndef __PARAPROBE_SYNTHETIC_SOLUTEMODEL_H__
#define __PARAPROBE_SYNTHETIC_SOLUTEMODEL_H__

#include "PARAPROBE_GrainModel.h"

struct speci
{
	apt_real c;			//target global composition at %
	apt_real m2q;		//dummy mass to charge 
	//##MK::I know, can be multiple values because of isotopes, 
	//which is here for simplicity not conisdered thoghnot considered yet...
	unsigned char ityp;

	speci() : c(0.f), m2q(0.f), ityp(UNKNOWN_IONTYPE) {}
	speci(const apt_real _c, const apt_real _m2q, const unsigned char _ityp) :
		c(_c), m2q(_m2q), ityp(_ityp) {}
};

ostream& operator<<(ostream& in, speci const & val);


bool SortSpeciAscComposition( speci & first, speci & second );


#define DEFAULT_SPECI speci( 0.0, 0.001, static_cast<unsigned char>(UNKNOWN_IONTYPE) );


class soluteModel
{
public:
	soluteModel();
	//soluteModel( vector<speci> const & composition );
	~soluteModel();

	void initialize( long customseed );
	speci get_random_speci();

private:
	vector<speci> composition;
	mt19937 urng;			//a random number generator to sample from distribution curve types
};

#endif

