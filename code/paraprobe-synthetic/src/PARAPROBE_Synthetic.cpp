//##MK::CODESPLIT

#include "PARAPROBE_SyntheticHdl.h"


//define what is passed upon execution
#define SIMID									1
#define CONTROLFILE								2

#ifndef GITSHA									//otherwise we could not compile without adding -DGITSHA="d123" a dummy GitSha
	#define GITSHA								0
#endif
//stringification of strings passed from cmake at compile time
//https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html
#define xstr(s)									str(s)
#define str(s) 									#s


void hello( void )
{
	cout << "paraprobe-synthetic" << "\n";
	cout << xstr(GITSHA) << "\n";
	cout << "Collecting " << CiteMeSynthetic::cite() << " publications for the tool to cite:" << "\n";
	for( auto it = CiteMeSynthetic::Citations.begin(); it != CiteMeSynthetic::Citations.end(); it++ ) {
		cout << it->author << "\n";
		cout << it->title << "\n";
		cout << it->journal << ", " << it->year << ", " << it->vol << ", pp" << it->pages << "\n";
		cout << it->doi << "\n";
	}
}


bool init(  int pargc, char** pargv )
{
	ConfigShared::SimID = stoul( pargv[SIMID] );
	try {
		ConfigSynthetic::readXML(pargv[CONTROLFILE]);
	}
	catch (exception& e) {
		cerr << "\n" << "Unable to parse control file! Details:" << "\n" << e.what() << "\n"; return false;
	}
	if ( ConfigSynthetic::checkUserInput() == false ) {
		cerr << "\n" << "Control file settings failed the validity check!" << "\n"; return false;
	}
	else {
		cout << endl << "\t\tInput is valid under SimulationID = " << "SimID." <<  ConfigShared::SimID << endl;
	}
	cout << endl;
	return true;
}


void synthesize_specimens()
{
	//allocate sequential transcoder instance
	double tic = omp_get_wtime();

	syntheticHdl* synthetic = NULL;
	try {
		synthetic = new syntheticHdl;
	}
	catch (bad_alloc &exc) {
		cerr << "Unable to allocate a sequential syntheticHdl instance" << "\n";
		delete synthetic; synthetic = NULL; return;
	}

	if ( synthetic->init_target_file() == true ) {
		cout << "Rank " << MASTER << " results file initialized!" << "\n";
	}
	else {
		cerr << "Rank " << MASTER << " results file creation failed!" << "\n";
		delete synthetic; synthetic = NULL; return;
	}
	if ( synthetic->write_environment_and_settings() == true ) {
		cout << "Rank " << MASTER << " environment and settings written!" << "\n";
	}
	else {
		cerr << "Rank " << MASTER << " writing environment and settings failed!" << "\n";
		delete synthetic; synthetic = NULL; return;
	}

	double toc = omp_get_wtime();
	memsnapshot mm = memsnapshot();
	synthetic->synthetic_tictoc.prof_elpsdtime_and_mem( "InitResultsFile", APT_XX, APT_IS_SEQ, mm, tic, toc );

	//build first the single/polycrystalline atomic geometry
	switch (ConfigSynthetic::SynthesizingMode)
	{
		case SYNTHESIS_DEVELOPMENT_SX:
			if ( ConfigSynthetic::DebugPhaseID != SPACEGROUP ) {
				if( synthetic->generate_synthetic_sx_mmm_omp() == true ) {
					cout << "Synthetic single-phase, single-crystalline specimen successfully generated" << "\n";
				}
				else {
					cerr << "Unable to synthesize a specimen" << "\n";
					delete synthetic; synthetic = NULL; return;
				}
			}
			else { //== SPACEGROUP
				if( synthetic->generate_synthetic_sx_spcgrp_omp() == true ) {
					cout << "Synthetic single-phase, single-crystalline specimen successfully generated" << "\n";
				}
				else {
					cerr << "Unable to synthesize a specimen" << "\n";
					delete synthetic; synthetic = NULL; return;
				}
			}
			break;
		case SYNTHESIS_DEVELOPMENT_PX:
			if( synthetic->generate_synthetic_px_mmm_omp() == true ) {
				cout << "Synthetic single-phase, polycrystalline specimen with Voronoi successfully generated" << "\n";
			}
			else {
				cerr << "Unable to synthesize a specimen" << "\n";
				delete synthetic; synthetic = NULL; return;
			}
			break;
		default:
			cerr << "Unable to synthesize a specimen" << "\n";
			delete synthetic; synthetic = NULL; return;
	}

	//optionally add precipitates
	if (ConfigSynthetic::NumberOfCluster > 0 ) {
		if( synthetic->generate_synthetic_precipitates_debug() == true ) {
			cout << "Adding of precipitates was successful" << "\n";
		}
		else {
			cerr << "Unable to add precipitates" << "\n";
			delete synthetic; synthetic = NULL; return;
		}
	}

	tic = omp_get_wtime();

	synthetic->get_total_ion_count();

	//report results
	synthetic->specimen_to_apth5();
	synthetic->grains_to_apth5();
	synthetic->precipitates_to_apth5();

	toc = omp_get_wtime();
	mm = synthetic->synthetic_tictoc.get_memoryconsumption();
	synthetic->synthetic_tictoc.prof_elpsdtime_and_mem( "WriteSpecimenToAPTH5andXDMF", APT_XX, APT_IS_SEQ, mm, tic, toc);
cout << "Writing specimen to APTH5 and XDMF took " << (toc-tic) << " seconds" << "\n";
	tic = omp_get_wtime();

	if ( ConfigSynthetic::SynthesizingMode == SYNTHESIS_DEVELOPMENT_PX ) {
		synthetic->tessellation_to_apth5();
	}

	toc = omp_get_wtime();
	mm = synthetic->synthetic_tictoc.get_memoryconsumption();
	synthetic->synthetic_tictoc.prof_elpsdtime_and_mem( "WriteTessellationToAPTH5andXDMF", APT_XX, APT_IS_SEQ, mm, tic, toc);
cout << "Writing specimen to APTH5 and XDMF took " << (toc-tic) << " seconds" << "\n";

	synthetic->synthetic_tictoc.spit_profiling( "Synthetic", ConfigShared::SimID, MASTER );

	//release resources
	delete synthetic; synthetic = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();

	hello();
//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	//helloworld( argc, argv );

	if ( init( argc, argv ) == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	//##MK::n/a

//EXECUTE SPECIFIC TASKS
	synthesize_specimens();

//DESTROY MPI
	//##MK::n/a

	double toc = omp_get_wtime();
	cout << "paraprobe-synthetic took " << (toc-tic) << " seconds wall-clock time in total" << endl;

	return 0;
}
