//##MK::CODESPLIT

#include "PARAPROBE_SyntheticHdl.h"


syntheticHdl::syntheticHdl()
{
	nevt = 0;
}


syntheticHdl::~syntheticHdl()
{
}


bool syntheticHdl::generate_synthetic_sx_mmm_omp()
{
	//a simple, specifically oriented single-phase, single-crystalline specimen with orthorhombic lattice
	//the specimens z axis runs parallel to the c axis of mmm lattice initially
	double tic = omp_get_wtime();

	if ( ConfigSynthetic::DebugPhaseID == ALUMINIUM || ConfigSynthetic::DebugPhaseID == WOLFRAM ) {
		latticeA = unitcellparms( ConfigSynthetic::LatticeA.a, ConfigSynthetic::LatticeA.b, ConfigSynthetic::LatticeA.c,
				ConfigSynthetic::LatticeA.alpha, ConfigSynthetic::LatticeA.beta, ConfigSynthetic::LatticeA.gamma );
	}
	else { //==AL3SC or AL3LI
		latticeB = unitcellparms( ConfigSynthetic::LatticeB.a, ConfigSynthetic::LatticeB.b, ConfigSynthetic::LatticeB.c,
				ConfigSynthetic::LatticeB.alpha, ConfigSynthetic::LatticeB.beta, ConfigSynthetic::LatticeB.gamma );
	}

	//get planned tip geometry
	size_t AtomsPerUnitcell = 0;
	switch(ConfigSynthetic::DebugPhaseID)
	{
		case AL3SC:
			AtomsPerUnitcell = 4; break;
		case AL3LI:
			AtomsPerUnitcell = 4; break;
		case WOLFRAM:
			AtomsPerUnitcell = 2; break;
		default:
			AtomsPerUnitcell = 4;
	}
	tipgeometry = geomodel( ConfigSynthetic::RelBottomRadius, ConfigSynthetic::RelTopRadius,
							ConfigSynthetic::RelBottomCapHeight, ConfigSynthetic::RelTopCapHeight,
							CUBE(ConfigSynthetic::LatticeA.a), ConfigSynthetic::NumberOfAtoms,
							AtomsPerUnitcell ); //ATOMS_PER_UNITCELL_FCC); //##MK::here fcc, so take a from mmm

	//knowing the tip geometry we can now allocate a O(n) time access structure
	rawdata_alf.initcontainer( tipgeometry.mybox );

	//cout << "rawdata_alf/imi/imx/xsz/nx\t\t" << rawdata_alf.mdat.box.xmi << "\t\t" << rawdata_alf.mdat.box.xmx << "\t\t" << rawdata_alf.mdat.box.xsz << "\t\t" << rawdata_alf.mdat.nx << endl;
	//cout << "rawdata_alf/imi/imx/ysz/ny\t\t" << rawdata_alf.mdat.box.ymi << "\t\t" << rawdata_alf.mdat.box.ymx << "\t\t" << rawdata_alf.mdat.box.ysz << "\t\t" << rawdata_alf.mdat.ny << endl;
	//cout << "rawdata_alf/imi/imx/zsz/nz\t\t" << rawdata_alf.mdat.box.zmi << "\t\t" << rawdata_alf.mdat.box.zmx << "\t\t" << rawdata_alf.mdat.box.zsz << "\t\t" << rawdata_alf.mdat.nz << endl;

	//##MK::nothing in solid solution

	//define a ball of atoms about the tipgeometry.mybox
	p3d boxcenter = tipgeometry.mybox.center();
	apt_real R = tipgeometry.mybox.circumscribed_sphere();
	aabb3d ballbox = aabb3d(	boxcenter.x-R, boxcenter.x+R,
								boxcenter.y-R, boxcenter.y+R,
								boxcenter.z-R, boxcenter.z+R   );
	ballbox.scale();
	p3d ballboxcenter = ballbox.center();

	//get uvw lattice plane limits to fully enclose tip AABB, ##MK::exemplarily fcc crystal lattice only here
	unitcellaggr rve = unitcellaggr();

	if ( ConfigSynthetic::DebugPhaseID == ALUMINIUM || ConfigSynthetic::DebugPhaseID == WOLFRAM ) {
		rve = unitcellaggr( latticeA, ballbox, ConfigSynthetic::DebugPhaseID );
	}
	else { //==AL3SC, AL3LI
		rve = unitcellaggr( latticeB, ballbox, ConfigSynthetic::DebugPhaseID );
	}

	//read orientation of the single-crystal
	bunge euler = bunge( ConfigSynthetic::SimMatrixOrientation );
	squat quat = euler.eu2qu();
	t3x3 local_ori = euler.eu2om();

	cout << "Bunge-Euler orientation " << euler << "\n";
	cout << "Quaternion orientation " << quat << "\n";
	cout << "Crystal orientation is " << local_ori << "\n";

	polycrystal.grains.push_back( grain( 0, quat ) );

	//MK::on the fly addition of ions from crystallographic motif to prevent building unnecessary copies
	//size_t processed = 0;

	#pragma omp parallel
	{
		double mytic = omp_get_wtime();

		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();

		//use PRNG to account for limited detection efficiency and finite spatial resolution
		//define thread-local PRNG which must be deterministic to get reproducible tips MK::but only when running with the same number of threads!
		size_t myseed = -11111111 * (mt + 1);
		#pragma omp critical
		{
			cout << "Parallelized building of the specimen atom by atom thread " << mt << " seed " << myseed << "\n";
		}

		mt19937 mydice;
		mydice.seed( myseed );
		mydice.discard( ConfigShared::RndDescrStatsPRNGDiscard ); //##MK::use ConfigSynthetic::PRNGWarmup
		uniform_real_distribution<apt_real> unifrnd(0.f, 1.f);

		//get normally distributed values for scatter about ideal lattice position
		normal_distribution<apt_real> gaussian_x(0.f, ConfigSynthetic::SpatResolutionSigmaX ); //mean is zero, stdev is user-defined
		normal_distribution<apt_real> gaussian_y(0.f, ConfigSynthetic::SpatResolutionSigmaY );
		normal_distribution<apt_real> gaussian_z(0.f, ConfigSynthetic::SpatResolutionSigmaZ );

		//warm up MersenneTwister
		//for (unsigned int i = 0; i < ConfigShared::RndDescrStatsPRNGDiscard; ++i)
		//	apt_real discard = unifrnd(mydice);

		//solute model, quasi deterministic becomes number of atoms per thread is large, hence each sub point cloud though its potential spatial confinement in a particular region of the tip is quasi CSR
		soluteModel solutes = soluteModel();
		solutes.initialize( myseed );

		//thread-local buffering of pos values
		vector<pos> mybuffer;

		//work gets distributed along z
		#pragma omp for schedule(dynamic,1) nowait
		for(int w = rve.wmin; w <= rve.wmax; ++w) {

			//nested loops all in parallel
			for(size_t b = 0; b < rve.base.size(); ++b) {
				for(int v = rve.vmin; v <= rve.vmax; ++v) {
					for(int u = rve.umin; u <= rve.umax; ++u) {
						p3d ap = rve.get_atom_pos(b, u, v, w);

						//relocate to boxcenter
						ap.x -= ballboxcenter.x;
						ap.y -= ballboxcenter.y;
						ap.z -= ballboxcenter.z;

						//rotate the point with respect to grain local coordinate system fixed at x,y,z
						p3d rot_ap = ap.active_rotation_relocate( local_ori );

						rot_ap.x += ballboxcenter.x;
						rot_ap.y += ballboxcenter.y;
						rot_ap.z += ballboxcenter.z;

						//account for finite and anisotropic spatial resolution
						ap.x = rot_ap.x + gaussian_x(mydice); //+= gaussian_x(dice); //scatter about equilibrium position 0.0
						ap.y = rot_ap.y + gaussian_y(mydice); //+= gaussian_y(dice);
						ap.z = rot_ap.z + gaussian_z(mydice); //+= gaussian_z(dice);

						if ( tipgeometry.mybox.is_inside( ap ) == false ) { //most points are as sphere volume is much larger than tipgeometry.mybox
							continue;
						}

						if ( tipgeometry.is_inside( ap ) == true ) {
							//account for limited detection efficiency
							if ( unifrnd(mydice) > ConfigSynthetic::SimDetEfficiency ) //every eta-th gets through so every 1-eta not, if eta = 0.0, almost every dice result > 0.0 discard, if eta --> 1 almost no dice result is > 1-small number
								continue;

							//##MK::developmental lattice start with single phase pure crystals, add intermetallics later

							////speci Al27 = speci( 1.000, 26.8, 1 );
							//apt_real mass2charge = 26.8;
							////apt_real mass2charge = 0.f; //##MK::dummy value to indicate unknown range

							apt_real mass2charge = 0.0;
							if ( ConfigSynthetic::DebugPhaseID == ALUMINIUM ) {
								//Paper18 pure Aluminium lattice
								//speci Al27 = speci( 0.00, 26.8, 1 ); //##MK::used for all others
								speci Al27 = speci( 0.00, 26.982 / 1.0, 1 ); //##MK::used for VerifyAlLiMg
								mass2charge = Al27.m2q;
							}
							else if ( ConfigSynthetic::DebugPhaseID == AL3SC ) {
								//Paper18 pure Al3Sc lattice
								speci Al27 = speci( 0.00, 26.982 / 1.0, 1 ); //+1
								speci Sc45 = speci( 0.00, 44.955 / 2.0, 4 ); //assume +2
								mass2charge = (b != 0) ? Al27.m2q : Sc45.m2q;
							}
							else if ( ConfigSynthetic::DebugPhaseID == WOLFRAM ) {
								speci W3XX = speci( 0.00, 183.84 / 3.0, 5 ); //+3
								mass2charge = W3XX.m2q;
							}
							else if ( ConfigSynthetic::DebugPhaseID == AL3LI ) {
								speci Li7 = speci( 0.00, 6.941 / 1.0 , 1 ); //assume +1
								speci Al27 = speci( 0.00, 26.982 / 1.0, 1 ); //+1
								mass2charge = ( b != 0 ) ? Al27.m2q : Li7.m2q;
							}
							else {
								//normal ##MK::we need a more sophisticated way to inject structures but not in a proof of concept implementation
								speci coin = solutes.get_random_speci();
								mass2charge = coin.m2q;
							}

							//buffer thread-local ion in tip first
							mybuffer.push_back( pos( ap.x, ap.y, ap.z, mass2charge) );
							//change to adding into a constant time query container
							//rawdata_alf.add_atom( pos(ap.x, ap.y, ap.z, mass2charge) );
						}

						/*processed++;
						if ( myprocessed % 1000000 != 0 )
							continue;
						else
							cout << processed << endl;*/
					} //iterate u
				} //iterate v
			} //iterate base
		} //iterate w and eventually complete parallel nested for loop

		double mytoc = omp_get_wtime();
		#pragma omp critical
		{
			cout << "Thread " << mt << " completed his construction process in " << (mytoc-mytic) << " seconds" << "\n";
		}

		//all done, a barrier is necessary to assure that rawdata_alf gets populated deterministically
		#pragma omp barrier

		//summary phase
		for( int thr = 0; thr < nt; thr++ ) { //walk deterministically and sequentially through thread-local results
			if ( thr == mt ) {
				for( auto it = mybuffer.begin(); it != mybuffer.end(); ++it ) {
					//change to adding into a constant time query container
					rawdata_alf.add_atom( *it );
				}
				if ( mybuffer.size() < static_cast<size_t>(UINT32MX) ) {
					polycrystal.grains.at(0).natoms += static_cast<unsigned int>(mybuffer.size());
				}
				else {
					cerr << "Polycrystal.grains.at(0).natoms is faulty due to datatype overflow!" << "\n";
				}
				//clear memory for the now obsolete thread-local buffer by overloading with new aka empty vector
				mybuffer = vector<pos>();
			}
			//all wait for thr to complete to enforce sequentiality, ##MK::is this sequentiality necessary?
			#pragma omp barrier
		} //accept data from next thread

	} //end of parallel region

	cout << "All tip atoms were placed" << "\n";

	//##MK  potentially deleting the data? rawdata_alf.write_occupancy_raw();
	//all more advanced specimen synthesis stuff comes as post-processing

	double toc = omp_get_wtime();
	memsnapshot mm = synthetic_tictoc.get_memoryconsumption();
	synthetic_tictoc.prof_elpsdtime_and_mem( "GenerateSXMMMTip", APT_XX, APT_IS_PAR, mm, tic, toc);

cout << "Specimen synthesis took " << (toc-tic) << " seconds" << "\n";
	return true;
}


bool syntheticHdl::generate_synthetic_sx_spcgrp_omp()
{
	//a simple, specifically oriented single-phase, single-crystal of a particular phase and general spacegroup
	//which atoms are in the tip geometry and assign them a unique ID, later sorting after IDs so that the
	//result does not differ for different numbers of threads
	//the specimens z axis runs parallel to the [001] axis of the Cartesian coordinate system, we build stacks of primitive cells
	double tic = omp_get_wtime();

	if ( ConfigSynthetic::DebugPhaseID != SPACEGROUP ) {
		return false;
	}

	//get planned tip geometry
	size_t AtomsPerUnitcell = ConfigSynthetic::SimMatrixStructure.motif.size();
	apt_real VolumePerUnitcell = ConfigSynthetic::SimMatrixStructure.unitcell_volume();
cout << "AtomsPerUnitCell = " << AtomsPerUnitcell << "\n";
cout << "VolumeUnitCell = " << VolumePerUnitcell << "\n";

	tipgeometry = geomodel( ConfigSynthetic::RelBottomRadius, ConfigSynthetic::RelTopRadius,
							ConfigSynthetic::RelBottomCapHeight, ConfigSynthetic::RelTopCapHeight,
							VolumePerUnitcell, ConfigSynthetic::NumberOfAtoms, AtomsPerUnitcell );

	//knowing the tip geometry we can now allocate a O(n) time access structure
	rawdata_alf.initcontainer( tipgeometry.mybox );

	//cout << "rawdata_alf/imi/imx/xsz/nx\t\t" << rawdata_alf.mdat.box.xmi << "\t\t" << rawdata_alf.mdat.box.xmx << "\t\t" << rawdata_alf.mdat.box.xsz << "\t\t" << rawdata_alf.mdat.nx << endl;
	//cout << "rawdata_alf/imi/imx/ysz/ny\t\t" << rawdata_alf.mdat.box.ymi << "\t\t" << rawdata_alf.mdat.box.ymx << "\t\t" << rawdata_alf.mdat.box.ysz << "\t\t" << rawdata_alf.mdat.ny << endl;
	//cout << "rawdata_alf/imi/imx/zsz/nz\t\t" << rawdata_alf.mdat.box.zmi << "\t\t" << rawdata_alf.mdat.box.zmx << "\t\t" << rawdata_alf.mdat.box.zsz << "\t\t" << rawdata_alf.mdat.nz << endl;

	//##MK::nothing in solid solution

	//define a ball of atoms about the tipgeometry.mybox
	p3d boxcenter = tipgeometry.mybox.center();
	cout << "Boxcenter " << boxcenter << "\n";
	apt_real R = tipgeometry.mybox.circumscribed_sphere();
	aabb3d ballbox = aabb3d(	boxcenter.x-R, boxcenter.x+R,
								boxcenter.y-R, boxcenter.y+R,
								boxcenter.z-R, boxcenter.z+R   );
	cout << "Ballbox " << ballbox << "\n";
	ballbox.scale();
	p3d ballboxcenter = ballbox.center();

	//get uvw lattice plane limits to fully enclose tip AABB, ##MK::exemplarily fcc crystal lattice only here
	//the unit cell is a parallelepiped so we get the bounding box by checking the extremal positions of its 8 vertices
	vector<p3d> verts;
	verts.push_back( p3d(	0.0, 0.0, 0.0) ); //origin, or rebase
	verts.push_back( p3d( 	0.0 + ConfigSynthetic::SimMatrixStructure.a1.x,
							0.0 + ConfigSynthetic::SimMatrixStructure.a1.y,
							0.0 + ConfigSynthetic::SimMatrixStructure.a1.z) ); //a0a1
	verts.push_back( p3d( 	0.0 + ConfigSynthetic::SimMatrixStructure.a2.x,
							0.0 + ConfigSynthetic::SimMatrixStructure.a2.y,
							0.0 + ConfigSynthetic::SimMatrixStructure.a2.z) ); //a0a2
	verts.push_back( p3d( 	0.0 + ConfigSynthetic::SimMatrixStructure.a3.x,
							0.0 + ConfigSynthetic::SimMatrixStructure.a3.y,
							0.0 + ConfigSynthetic::SimMatrixStructure.a3.z) ); //a0a3
	verts.push_back( p3d( 	0.0 + ConfigSynthetic::SimMatrixStructure.a1.x + ConfigSynthetic::SimMatrixStructure.a2.x,
							0.0 + ConfigSynthetic::SimMatrixStructure.a1.y + ConfigSynthetic::SimMatrixStructure.a2.y,
							0.0 + ConfigSynthetic::SimMatrixStructure.a1.z + ConfigSynthetic::SimMatrixStructure.a2.z) ); //a0a1a2
	verts.push_back( p3d( 	0.0 + ConfigSynthetic::SimMatrixStructure.a3.x + ConfigSynthetic::SimMatrixStructure.a2.x,
							0.0 + ConfigSynthetic::SimMatrixStructure.a3.y + ConfigSynthetic::SimMatrixStructure.a2.y,
							0.0 + ConfigSynthetic::SimMatrixStructure.a3.z + ConfigSynthetic::SimMatrixStructure.a2.z) ); //a0a3a2
	verts.push_back( p3d(	0.0 + ConfigSynthetic::SimMatrixStructure.a1.x + ConfigSynthetic::SimMatrixStructure.a3.x,
							0.0 + ConfigSynthetic::SimMatrixStructure.a1.y + ConfigSynthetic::SimMatrixStructure.a3.y,
							0.0 + ConfigSynthetic::SimMatrixStructure.a1.z + ConfigSynthetic::SimMatrixStructure.a3.z) ); //a0a1a3
	verts.push_back( p3d( 	0.0 + ConfigSynthetic::SimMatrixStructure.a1.x + ConfigSynthetic::SimMatrixStructure.a2.x + ConfigSynthetic::SimMatrixStructure.a3.x,
							0.0 + ConfigSynthetic::SimMatrixStructure.a1.y + ConfigSynthetic::SimMatrixStructure.a2.y + ConfigSynthetic::SimMatrixStructure.a3.y,
							0.0 + ConfigSynthetic::SimMatrixStructure.a1.z + ConfigSynthetic::SimMatrixStructure.a2.z + ConfigSynthetic::SimMatrixStructure.a3.z ) ); //a0a1a2a3
	apt_real xmi = F32MX;
	apt_real xmx = F32MI;
	apt_real ymi = F32MX;
	apt_real ymx = F32MI;
	apt_real zmi = F32MX;
	apt_real zmx = F32MI;
	for ( auto it = verts.begin(); it != verts.end(); it++ ) {
		if ( it->x <= xmi )
			xmi = it->x;
		if ( it->x >= xmx )
			xmx = it->x;
		if ( it->y <= ymi )
			ymi = it->y;
		if ( it->y >= ymx )
			ymx = it->y;
		if ( it->z <= zmi )
			zmi = it->z;
		if ( it->z >= zmx )
			zmx = it->z;
	}
	cout << "xmi " << xmi << "\n";
	cout << "xmx " << xmx << "\n";
	cout << "ymi " << ymi << "\n";
	cout << "ymx " << ymx << "\n";
	cout << "zmi " << zmi << "\n";
	cout << "zmx " << zmx << "\n";

	/*
	//rebase is 0.0, 0.0, 0.0, i.e. in origin here
	int umin = static_cast<int>(floor((ballbox.xmi - ballboxcenter.x) / (xmx - xmi)) ) - 1;
	int umax = static_cast<int>(ceil((ballbox.xmx - ballboxcenter.x) / (xmx - xmi)) ) + 1;
	int vmin = static_cast<int>(floor((ballbox.ymi - ballboxcenter.y) / (ymx - ymi)) ) - 1;
	int vmax = static_cast<int>(ceil((ballbox.ymx - ballboxcenter.y) / (ymx - ymi)) ) + 1;
	int wmin = static_cast<int>(floor((ballbox.zmi - ballboxcenter.z) / (zmx - zmi)) ) - 1;
	int wmax = static_cast<int>(ceil((ballbox.zmx - ballboxcenter.z) / (zmx - zmi)) ) + 1;
	if (( umax - umin + 1) >= 1024 || (vmax - vmin + 1) >= 1024 || (wmax - wmin + 1) >= 1024 ) {
		cerr << "Resulting unit cell aggregate is too large and therefore not supported by the current implementation !" << "\n";
		return false;
	}
	*/

	//use shortest unit cell base vector to scale sphere ##MK::find a more efficient way
	apt_real a1len = SQR(ConfigSynthetic::SimMatrixStructure.a1.x - 0.0) + SQR(ConfigSynthetic::SimMatrixStructure.a1.y - 0.0) + SQR(ConfigSynthetic::SimMatrixStructure.a1.z - 0.0);
	apt_real a2len = SQR(ConfigSynthetic::SimMatrixStructure.a2.x - 0.0) + SQR(ConfigSynthetic::SimMatrixStructure.a2.y - 0.0) + SQR(ConfigSynthetic::SimMatrixStructure.a2.z - 0.0);
	apt_real a3len = SQR(ConfigSynthetic::SimMatrixStructure.a3.x - 0.0) + SQR(ConfigSynthetic::SimMatrixStructure.a3.y - 0.0) + SQR(ConfigSynthetic::SimMatrixStructure.a3.z - 0.0);
	apt_real len = a1len;
	if ( a2len <= len )
		len = a2len;
	if ( a3len <= len )
		len = a3len;
	len = sqrt(len);

	apt_real imin = min(ballbox.xmi, min(ballbox.ymi, ballbox.zmi));
	apt_real imax = max(ballbox.xmx, max(ballbox.ymx, ballbox.zmx));
	int umin = static_cast<int>(floor(( imin - ballboxcenter.x) / len )) - 1; //-1;
	int umax = static_cast<int>(ceil(( imax - ballboxcenter.x) / len )) + 1; //+1;
	int vmin = static_cast<int>(floor(( imin - ballboxcenter.y) / len )) - 1; //-1;
	int vmax = static_cast<int>(ceil(( imax - ballboxcenter.y) / len )) + 1; //+1;
	int wmin = static_cast<int>(floor(( imin - ballboxcenter.z) / len )) - 1; //-1;
	int wmax = static_cast<int>(ceil(( imax - ballboxcenter.z) / len )) + 1; //+1;

	cout << "umin " << umin << "\n";
	cout << "umax " << umax << "\n";
	cout << "vmin " << vmin << "\n";
	cout << "vmax " << vmax << "\n";
	cout << "wmin " << wmin << "\n";
	cout << "wmax " << wmax << "\n";

	//read orientation of the single-crystal
	bunge euler = bunge( ConfigSynthetic::SimMatrixOrientation );
	squat quat = euler.eu2qu();
	t3x3 local_ori = euler.eu2om();

	cout << "Bunge-Euler orientation " << euler << "\n";
	cout << "Quaternion orientation " << quat << "\n";
	cout << "Crystal orientation is " << local_ori << "\n";

	polycrystal.grains.push_back( grain( 0, quat ) );

	/*
	//MK::on the fly addition of ions from crystallographic motif to prevent building unnecessary copies
	//size_t processed = 0;
	crystalstructure matrixstructure = crystalstructure(
			ConfigSynthetic::SimMatrixStructure.a, ConfigSynthetic::SimMatrixStructure.b, ConfigSynthetic::SimMatrixStructure.c,
			ConfigSynthetic::SimMatrixStructure.alpha, ConfigSynthetic::SimMatrixStructure.beta, ConfigSynthetic::SimMatrixStructure.gamma,
			ConfigSynthetic::SimMatrixStructure.spacegroup, ConfigSynthetic::SimMatrixStructure.motif );
	//##MK::BEGIN DEBUG
	for( auto it = matrixstructure.motif.begin(); it != matrixstructure.motif.end(); it++ ) {
		cout << *it << "\n";
	}
	//##MK::END DEBUG
	*/

	#pragma omp parallel
	{
		double mytic = omp_get_wtime();

		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();

		//use PRNG to account for limited detection efficiency and finite spatial resolution
		//define thread-local PRNG which must be deterministic to get reproducible tips MK::but only when running with the same number of threads!
		size_t myseed = -11111111 * (mt + 1);
		#pragma omp critical
		{
			cout << "Parallelized building of the specimen atom by atom thread " << mt << " seed " << myseed << "\n";
		}

		mt19937 mydice;
		mydice.seed( myseed );
		mydice.discard( ConfigShared::RndDescrStatsPRNGDiscard ); //##MK::use ConfigSynthetic::PRNGWarmup
		uniform_real_distribution<apt_real> unifrnd(0.f, 1.f);

		//get normally distributed values for scatter about ideal lattice position
		normal_distribution<apt_real> gaussian_x(0.f, ConfigSynthetic::SpatResolutionSigmaX ); //mean is zero, stdev is user-defined
		normal_distribution<apt_real> gaussian_y(0.f, ConfigSynthetic::SpatResolutionSigmaY );
		normal_distribution<apt_real> gaussian_z(0.f, ConfigSynthetic::SpatResolutionSigmaZ );

		//warm up MersenneTwister
		//for (unsigned int i = 0; i < ConfigShared::RndDescrStatsPRNGDiscard; ++i)
		//	apt_real discard = unifrnd(mydice);

		//##MK::here, no solute model, quasi deterministic becomes number of atoms per thread is large, hence each sub point cloud though its potential spatial confinement in a particular region of the tip is quasi CSR
		//soluteModel solutes = soluteModel();
		//solutes.initialize( myseed );

		//thread-local buffering of pos values
		vector<pos> mybuffer;

		//work gets distributed along z
		#pragma omp for schedule(dynamic,1) nowait
		for(int w = wmin; w <= wmax; ++w) {
			//nested loops all in parallel
			for(int v = vmin; v <= vmax; ++v) {
				for(int u = umin; u <= umax; ++u) {
					for( size_t b = 0; b < ConfigSynthetic::SimMatrixStructure.motif.size(); b++ ) {

						p3d ap = ConfigSynthetic::SimMatrixStructure.get_atom_pos(b, u, v, w); //about origin 0,0,0
						unsigned char Z = ConfigSynthetic::SimMatrixStructure.get_element_Z( b );
						unsigned char Charge = ConfigSynthetic::SimMatrixStructure.get_chargestate( b );

						/*
						//relocate to boxcenter
						ap.x -= ballboxcenter.x;
						ap.y -= ballboxcenter.y;
						ap.z -= ballboxcenter.z;
						*/

						//rotate the point with respect to grain local coordinate system fixed at x,y,z
						p3d rot_ap = ap.active_rotation_relocate( local_ori );

						//relocate to ballboxcenter
						rot_ap.x += ballboxcenter.x;
						rot_ap.y += ballboxcenter.y;
						rot_ap.z += ballboxcenter.z;

						//account for finite and anisotropic spatial resolution
						ap.x = rot_ap.x + gaussian_x(mydice); //scatter about equilibrium position 0.0
						ap.y = rot_ap.y + gaussian_y(mydice);
						ap.z = rot_ap.z + gaussian_z(mydice);

						if ( tipgeometry.mybox.is_inside( ap ) == false ) { //most points are as sphere volume is much larger than tipgeometry.mybox
							continue;
						}

						if ( tipgeometry.is_inside( ap ) == true ) {
							//account for limited detection efficiency
							if ( unifrnd(mydice) > ConfigSynthetic::SimDetEfficiency ) //every eta-th gets through so every 1-eta not, if eta = 0.0, almost every dice result > 0.0 discard, if eta --> 1 almost no dice result is > 1-small number
								continue;

							map<unsigned char, elementinfo>::iterator thisone = ConfigShared::MyElementsZ.find(Z);
							if ( thisone != ConfigShared::MyElementsZ.end() ) {
								apt_real mass2charge = thisone->second.mass / static_cast<apt_real>(static_cast<int>(Charge));

								//buffer thread-local ion in tip first
								//size_t hash = ( u - umin + 1) + ( v - vmin + 1) * 1024 + ( w - wmax + 1) * 1024*1024) + ((bt - ConfigSynthetic::SimMatrixStructure.motif.begin()) * 4000000000);

								/*
								#pragma omp critical
								{
									cout << "Placing x,y,z,mq\t\t" << ap.x << "\t\t" << ap.y << "\t\t" << ap.z << "\t\t" << mass2charge << "\n";
								}
								*/

								mybuffer.push_back( pos( ap.x, ap.y, ap.z, mass2charge ) );

							}
							else {
								cerr << "Thread " << mt << " did not found elementinfo to Z " << Z << "\n";
								continue;
							}
						}
						/*processed++;
						if ( myprocessed % 1000000 != 0 )
							continue;
						else
							cout << processed << endl;*/
					} //iterate base
				} //iterate u
			} //iterate v
		} //iterate w and eventually complete parallel nested for loop

		double mytoc = omp_get_wtime();
		#pragma omp critical
		{
			cout << "Thread " << mt << " completed his construction process in " << (mytoc-mytic) << " seconds" << "\n";
		}

		//all done, a barrier is necessary to assure that rawdata_alf gets populated deterministically
		#pragma omp barrier

		//summary phase
		for( int thr = 0; thr < nt; thr++ ) { //walk deterministically and sequentially through thread-local results
			if ( thr == mt ) {
				for( auto it = mybuffer.begin(); it != mybuffer.end(); ++it ) {
					//change to adding into a constant time query container
					rawdata_alf.add_atom( *it );
				}
				if ( mybuffer.size() < static_cast<size_t>(UINT32MX) ) {
					polycrystal.grains.at(0).natoms += static_cast<unsigned int>(mybuffer.size());
				}
				else {
					cerr << "Polycrystal.grains.at(0).natoms is faulty due to datatype overflow!" << "\n";
				}
				//clear memory for the now obsolete thread-local buffer by overloading with new aka empty vector
				mybuffer = vector<pos>();
			}
			//all wait for thr to complete to enforce sequentiality, ##MK::is this sequentiality necessary?
			#pragma omp barrier
		} //accept data from next thread

	} //end of parallel region

	cout << "All tip atoms were placed" << "\n";

	//##MK  potentially deleting the data? rawdata_alf.write_occupancy_raw();
	//all more advanced specimen synthesis stuff comes as post-processing

	double toc = omp_get_wtime();
	memsnapshot mm = synthetic_tictoc.get_memoryconsumption();
	synthetic_tictoc.prof_elpsdtime_and_mem( "GenerateSXUserSpacegroupTip", APT_XX, APT_IS_PAR, mm, tic, toc);

cout << "Specimen synthesis took " << (toc-tic) << " seconds" << "\n";
	return true;
}


bool syntheticHdl::generate_synthetic_px_mmm_omp()
{
	//##MK::a developmental/testing functionality to built orthorhombic (nano-)polycrystalline specimen
	double tic = omp_get_wtime();

	if ( ConfigSynthetic::DebugPhaseID == ALUMINIUM || ConfigSynthetic::DebugPhaseID == WOLFRAM ) {
		latticeA = unitcellparms( ConfigSynthetic::LatticeA.a, ConfigSynthetic::LatticeA.b, ConfigSynthetic::LatticeA.c,
				ConfigSynthetic::LatticeA.alpha, ConfigSynthetic::LatticeA.beta, ConfigSynthetic::LatticeA.gamma );
	}
	else { //==AL3SC, AL3LI
		latticeB = unitcellparms( ConfigSynthetic::LatticeB.a, ConfigSynthetic::LatticeB.b, ConfigSynthetic::LatticeB.c,
				ConfigSynthetic::LatticeB.alpha, ConfigSynthetic::LatticeB.beta, ConfigSynthetic::LatticeB.gamma );
	}

	//get planned tip geometry
	size_t AtomsPerUnitcell = 0;
	switch(ConfigSynthetic::DebugPhaseID)
	{
		case AL3SC:
			AtomsPerUnitcell = 4; break;
		case WOLFRAM:
			AtomsPerUnitcell = 2; break;
		case AL3LI:
			AtomsPerUnitcell = 4; break;
		default:
			AtomsPerUnitcell = 4;
	}

	//get planned tip geometry
	tipgeometry = geomodel( ConfigSynthetic::RelBottomRadius, ConfigSynthetic::RelTopRadius,
							ConfigSynthetic::RelBottomCapHeight, ConfigSynthetic::RelTopCapHeight,
							CUBE(ConfigSynthetic::LatticeA.a), ConfigSynthetic::NumberOfAtoms,
							AtomsPerUnitcell ); //ATOMS_PER_UNITCELL_FCC); //##MK::here fcc, so take a from mmm

	//knowing the tip geometry we can now allocate a O(n) time access structure
	rawdata_alf.initcontainer( tipgeometry.mybox );

	//Voronoi-tessellate this axis-aligned bounding box about the tip and use the Voronoi cells as a simple
	//model for nano-scale grains, ##MK::initializing microstructures from Poisson-Voronoi tessellations does
	//not account for curvature and spatial/geometrical correlations of the grain boundary network
	//##MK::in this work we use it to introduce grain boundary as interfaces at all to be able to study
	//what is the effect of introducing GB in the specimen when performing APT crystallography methods
	//##MK::in the future replace with milling specimen from relaxed MD configuration or Dream3D

	//how many Voronoi cells to get approximately grains of VoroMeanRadius?
	apt_real Vaabb = tipgeometry.mybox.volume();
	int NumberOfGrains = static_cast<int>(ceil(Vaabb / (4.f/3.f * MYPI * CUBE(ConfigSynthetic::CrystalloVoroMeanRadius))));
cout << "TipAABB volume " << Vaabb << " allows for " << NumberOfGrains << " grains with mean radius " << ConfigSynthetic::CrystalloVoroMeanRadius << "\n";

	p3i blocks = tipgeometry.mybox.blockpartitioning( static_cast<size_t>(ConfigSynthetic::NumberOfAtoms),
			ConfigSynthetic::TessellationPointsPerBlock );

	bool periodicbox = false; //APT data are not periodic, reserve for allocating one grain per box
	container con(	tipgeometry.mybox.xmi - AABBINCLUSION_EPSILON, tipgeometry.mybox.xmx + AABBINCLUSION_EPSILON,
					tipgeometry.mybox.ymi - AABBINCLUSION_EPSILON, tipgeometry.mybox.ymx + AABBINCLUSION_EPSILON,
					tipgeometry.mybox.zmi - AABBINCLUSION_EPSILON, tipgeometry.mybox.zmx + AABBINCLUSION_EPSILON,
					blocks.x, blocks.y, blocks.z,
					periodicbox, periodicbox, periodicbox, 1);

	//double mytic = omp_get_wtime();
	//int mt = MASTER; //omp_get_thread_num();
	//int nt = SINGLETHREADED; //omp_get_num_threads();

	//use PRNG to account for limited detection efficiency and finite spatial resolution
	//define thread-local PRNG which must be deterministic to get reproducible tips MK::but only when running with the same number of threads!
	//size_t myseed = -11111111 * (mt + 1);
	long myseed = ConfigSynthetic::PRNGWorldSeed;
	/*#pragma omp critical
	{
		cout << "Parallelized building of the specimen atom by atom thread " << mt << " seed " << myseed << "\n";
	}*/
	mt19937 mydice_geo;
	//mydice_geo.seed( ConfigSynthetic::PRNGWorldSeed );
	mydice_geo.seed( myseed );
	mydice_geo.discard( ConfigSynthetic::PRNGWarmup ); //##MK::use ConfigSynthetic::PRNGWarmup

	mt19937 mydice_ori;
	mydice_ori.seed( myseed );
	mydice_ori.discard( ConfigSynthetic::PRNGWarmup );

	//get uniform random numbers to thin out randomly point cloud
	uniform_real_distribution<apt_real> unifrnd(0.f, 1.f);
	//get normally distributed values for scatter about ideal lattice position
	normal_distribution<apt_real> gaussian_x(0.f, ConfigSynthetic::SpatResolutionSigmaX ); //mean is zero, stdev is user-defined
	normal_distribution<apt_real> gaussian_y(0.f, ConfigSynthetic::SpatResolutionSigmaY );
	normal_distribution<apt_real> gaussian_z(0.f, ConfigSynthetic::SpatResolutionSigmaZ );

	//put NumberOfGrains many spatially uncorrelatedly i.e. randomly placed points into tipgeometry.mybox and get Voronoi cells about them
	//see P. Hoffrogge, L. A. Barrales-Mora Atomcook
	polycrystal.grains = vector<grain>( NumberOfGrains, grain() );

	vector<grain>::iterator gr = polycrystal.grains.begin();
	for ( int gid = 0; gid < NumberOfGrains; gid++, gr++ ) {
		gr->cellcenter = p3d(
				tipgeometry.mybox.xmi + unifrnd(mydice_geo)*tipgeometry.mybox.xsz,
				tipgeometry.mybox.ymi + unifrnd(mydice_geo)*tipgeometry.mybox.ysz,
				tipgeometry.mybox.zmi + unifrnd(mydice_geo)*tipgeometry.mybox.zsz     );
		gr->id = static_cast<unsigned int>(gid);

		//place grain in the Voro container
		con.put( gid, gr->cellcenter.x, gr->cellcenter.y, gr->cellcenter.z ); //single-precision accurate only!

		//MK::define a random unit quaternion and interpret it as a Bunge passive orientation unit matrix
		gr->ori = squat( unifrnd(mydice_ori), unifrnd(mydice_ori), unifrnd(mydice_ori) );

		//these values get filled in and accumulated respectively later
		gr->cellvolume = 0.0;
		gr->natoms = 0;
		/*cout << "q = " << q << endl;
						t3x3 local_ori = q.qu2om();
		cout << "localori = " << local_ori << endl;
		cout << "det(loc) = " << local_ori.det() << endl;*/
	}

	//solute model, quasi deterministic becomes number of atoms per thread is large, hence each sub point cloud though its potential spatial confinement in a particular region of the tip is quasi CSR
	soluteModel solutes = soluteModel();
	solutes.initialize( myseed );

	//get temporaries to store the geometry of the Voronoi cells
	voronoicell_neighbor c;
	c_loop_all cl(con);
	if (cl.start()) { //this is an incremental computing of the tessellation which holds at no point the entire tessellation
		do {
			if ( con.compute_cell(c, cl) == true ) {
				int stats[4] = {0,0,0,0};
				//0 placed in this current cell
				//1 discarded because not in Voronoi cell bounding box
				//2 discarded because not closest to cell
				//3 discarded because not in tip
				int clpid = cl.pid(); //gather information about the cell/grain, specifically its geometry and aabb3d
				//if ( clpid != 106 )
				//	continue;

				//compute the Voronoi cell geometry
				double x, y, z;	//center
				cl.pos(x, y, z);
				vector<int> neigh; //neighbor cells
				c.neighbors(neigh);
				vector<int> fvert; //vertex indices
				c.face_vertices(fvert);
				vector<double> verts; //vertex coordinate values, not coordinate triplets!
				c.vertices(x, y, z, verts);

				//see documentation in VoroXX.cpp cell store heavydata
				map<int,int> old2new;
				vector<int> fvert_reindexed;
				vector<p3d> vtriplets_unique;

				//get number of unique float triplets
				//http://math.lbl.gov/voro++/examples/polygons/
				int ii = 0; int j = 0;
				for( size_t i = 0; i < neigh.size(); i++ ) { //O(N) processing
					for( int k = 0; k < fvert[j]; k++) { //generating 3d points with implicit indices 0, 1, 2, ....
						int l = 3*fvert[j+k+1];
						int implicitkey = l + (l+1)*1024 + (l+2)*1024*1024; //##MK::
						auto it = old2new.find( implicitkey );
						if ( it != old2new.end() ) { //integer triplet does exists already so reuse
							fvert_reindexed.push_back( it->second );
						}
						else { //integer triplet does not yet exist so create
							old2new.insert( make_pair( implicitkey, ii ) );
							fvert_reindexed.push_back( ii );
							vtriplets_unique.push_back( p3d(verts[l], verts[l+1], verts[l+2]) );
							ii++;
						}
					}
					j += fvert[j] + 1;
				}

				//pass heavy data to buffer
				ii = 0; j = 0;
				for( size_t i = 0; i < neigh.size(); i++ ) {
					tess.io_topo.push_back( static_cast<unsigned int>(3) ); //XDMF keyword to visualize an n-polygon
					tess.io_topo.push_back( static_cast<unsigned int>(fvert[j]) ); //how many edges on the polygon? promotion uint32 to size_t no problem
					for( int k = 0; k < fvert[j]; k++, ii++ ) {
						tess.io_topo.push_back( static_cast<unsigned int>(fvert_reindexed.at(ii)) );
					}
					j += fvert[j] + 1;
					tess.io_halo.push_back( clpid );
				}

				for( auto it = vtriplets_unique.begin(); it != vtriplets_unique.end(); it++ ) {
					tess.io_geom.push_back( it->x );
					tess.io_geom.push_back( it->y );
					tess.io_geom.push_back( it->z );
				}

				tess.io_info.push_back( voro_io_info(
						static_cast<size_t>(clpid), neigh.size(),
						fvert_reindexed.size() + 2*neigh.size(),
						vtriplets_unique.size()) ); //for ngeom report how many triplets not IDs!

				//find bounding box about this Voronoi cell
				aabb3d cbox = aabb3d();
				for ( auto pt = vtriplets_unique.begin(); pt != vtriplets_unique.end(); ++pt ) {
					if ( pt->x <= cbox.xmi )	cbox.xmi = pt->x;
					if ( pt->x >= cbox.xmx )	cbox.xmx = pt->x;
					if ( pt->y <= cbox.ymi )	cbox.ymi = pt->y;
					if ( pt->y >= cbox.ymx )	cbox.ymx = pt->y;
					if ( pt->z <= cbox.zmi )	cbox.zmi = pt->z;
					if ( pt->z >= cbox.zmx )	cbox.zmx = pt->z;
				}
				cbox.scale();

				//define spherical ball at x,y,z which encloses the cbox completely
				p3d cbox_center = cbox.center();
				apt_real RSQR = 0.f;	apt_real DistSQR = 0.f; //use distance squared for finding radius once found compute sqrt only once
				DistSQR = SQR(cbox.xmi-cbox_center.x)+SQR(cbox.ymi-cbox_center.y)+SQR(cbox.zmi-cbox_center.z);
				if ( DistSQR >= RSQR )	RSQR = DistSQR;
				DistSQR = SQR(cbox.xmx-cbox_center.x)+SQR(cbox.ymi-cbox_center.y)+SQR(cbox.zmi-cbox_center.z);
				if ( DistSQR >= RSQR )	RSQR = DistSQR;
				DistSQR = SQR(cbox.xmi-cbox_center.x)+SQR(cbox.ymx-cbox_center.y)+SQR(cbox.zmi-cbox_center.z);
				if ( DistSQR >= RSQR )	RSQR = DistSQR;
				DistSQR = SQR(cbox.xmx-cbox_center.x)+SQR(cbox.ymx-cbox_center.y)+SQR(cbox.zmi-cbox_center.z);
				if ( DistSQR >= RSQR )	RSQR = DistSQR;
				DistSQR = SQR(cbox.xmi-cbox_center.x)+SQR(cbox.ymi-cbox_center.y)+SQR(cbox.zmx-cbox_center.z);
				if ( DistSQR >= RSQR )	RSQR = DistSQR;
				DistSQR = SQR(cbox.xmx-cbox_center.x)+SQR(cbox.ymi-cbox_center.y)+SQR(cbox.zmx-cbox_center.z);
				if ( DistSQR >= RSQR )	RSQR = DistSQR;
				DistSQR = SQR(cbox.xmi-cbox_center.x)+SQR(cbox.ymx-cbox_center.y)+SQR(cbox.zmx-cbox_center.z);
				if ( DistSQR >= RSQR )	RSQR = DistSQR;
				DistSQR = SQR(cbox.xmx-cbox_center.x)+SQR(cbox.ymx-cbox_center.y)+SQR(cbox.zmx-cbox_center.z);
				if ( DistSQR >= RSQR )	RSQR = DistSQR;
				apt_real R = sqrt(RSQR);

				//define unit cell aggregate inside the balls AABB and
				aabb3d ballbox = aabb3d( 	cbox_center.x-R, cbox_center.x+R,
											cbox_center.y-R, cbox_center.y+R,
											cbox_center.z-R, cbox_center.z+R     );
				ballbox.scale();
cout << "Ballbox " << ballbox << "\n";
				p3d ballbox_center = ballbox.center();

				//test if any corner of cbox protrudes out of sphere

				unitcellaggr local_lattice = unitcellaggr( latticeA, ballbox, ballbox_center, ConfigSynthetic::DebugPhaseID );
cout << "Grain local_lattice umi/umx\t\t" << local_lattice.umin << "\t\t" << local_lattice.umax << "\n";
cout << "Grain local_lattice vmi/vmx\t\t" << local_lattice.vmin << "\t\t" << local_lattice.vmax << "\n";
cout << "Grain local_lattice wmi/wmx\t\t" << local_lattice.wmin << "\t\t" << local_lattice.wmax << "\n";

				//MK::define a random unit quaternion and interpret it as a Bunge passive orientation unit matrix
				//##MK::check carefully active passive convention!
				squat q = polycrystal.grains.at(clpid).ori;
cout << "q = " << q << "\n";
				t3x3 local_ori = q.qu2om();
cout << "localori = " << local_ori << "\n";
cout << "det(loc) = " << local_ori.det() << "\n";

				/*bunge bg = q.qu2eu();
cout << "Bunge " << bg << "\n";
				local_ori = bg.eu2om();
cout << "eu2om = " << local_ori << "\n";
cout << "det(loc) = " << local_ori.det() << "\n";*/

cout << "Vorocell cbox " << cbox << "\n";
cout << "Grain clpid/gid\t\t" << clpid << "\n";
cout << "Grain center " << cbox_center << "\n";
cout << "Grain ballbox " << ballbox << "\n";
cout << "Grain squat " << q << "\n";

				polycrystal.grains.at(clpid).cellvolume = c.volume();

				//determine all lattice points that are inside the Voronoi cell volume of the grain
				for(int w = local_lattice.wmin; w <= local_lattice.wmax; ++w) {
					for(size_t b = 0; b < local_lattice.base.size(); ++b) {
						for(int v = local_lattice.vmin; v <= local_lattice.vmax; ++v) {
							for(int u = local_lattice.umin; u <= local_lattice.umax; ++u) {

								p3d ap_initial = local_lattice.get_atom_pos(b, u, v, w);

								//rotate the point with respect to grain local coordinate system fixed at x,y,z
								p3d ap = ap_initial.active_rotation_relocate( local_ori );

								//unset world coordinate system relocation to get rotated initial configuration
								ap.x = ap.x + ballbox_center.x;
								ap.y = ap.y + ballbox_center.y;
								ap.z = ap.z + ballbox_center.z;

								//account for finite and anisotropic spatial resolution
								//MK::do so in the rotated configuration, because the scatter is not along the unit cell axis
								//but the reference global coordinate system because the tomograph cannot resolve well in xy plane or z
								ap.x = ap.x + gaussian_x(mydice_geo); //scatter about equilibrium position
								ap.y = ap.y + gaussian_y(mydice_geo);
								ap.z = ap.z + gaussian_z(mydice_geo);

								//inclusion test to Voronoi polyhedron is more costly than inclusion test against hexahedron i.e. AABB
								//so prune candidate points first via testing for exclusion in cbox

								if ( cbox.is_inside( ap ) == false ) { //most atoms are outside because box to ball is larger than ball than cbox inside ball
									stats[1]++;
									/*if ( clpid == 122 ) {
										cout << ap.x << ";" << ap.y << ";" << ap.z << " clpid122 rejected not in cbox" << "\n";
									}*/
									continue;
								}

								//ion survived all pruning tests, so is inside cbox now its worth to do the more costly test
								apt_real DistTestSQR = F32MX;
								int clpid_ap_isclosest = INT32MI;
								int jjj = 0;
								//improve performance here
								for( auto iit = polycrystal.grains.begin(); iit != polycrystal.grains.end(); iit++, jjj++ ) {
									apt_real distsqr = SQR(iit->cellcenter.x-ap.x)+SQR(iit->cellcenter.y-ap.y)+SQR(iit->cellcenter.z-ap.z);
									if ( distsqr > DistTestSQR )
										continue;
									else {
										DistTestSQR = distsqr;
										clpid_ap_isclosest = jjj;
									}
								}

								/*//this inclusion test results for unknown reasons in incorrect behavior!
								double ix = ap.x;
								double iy = ap.y;
								double iz = ap.z;
								double rx, ry, rz;
								int clpid_ap_isclosest;

								bool vorostatus = con.find_voronoi_cell( ix, iy, iz, rx, ry, rz, clpid_ap_isclosest );
								if ( clpid == 122 ) {
									if ( vorostatus == false && tipgeometry.is_inside( ap ) == true ) {
										cout << ap.x << ";" << ap.y << ";" << ap.z << " clpid122 no cell found but should be in tip!" << "\n";
									}
								}*/

								if ( clpid_ap_isclosest == clpid ) {
/*if ( clpid == 106 ) {
cout << "106PartOfCell;" << ap.x << ";" << ap.y << ";" << ap.z << "\n";
}*/
									//the current point ap belongs to the current Voronoi cell
									if ( tipgeometry.is_inside( ap ) == true ) { //because tess defined on an AABB larger than the tip
										//now only we account for limited detection efficiency because otherwise the tip surface would be rough
										if ( unifrnd(mydice_geo) <= ConfigSynthetic::SimDetEfficiency ) { //every eta-th gets through so every 1-eta not, if eta = 0.0, almost every dice result > 0.0 discard, if eta --> 1 almost no dice result is > 1-small number

											//##MK::developmental lattice start with single phase pure crystals, add intermetallics later
											//speci Al27 = speci( 1.000, 26.8, 1 );
											//apt_real mass2charge = 26.8;
											apt_real mass2charge = 0.0;
											if ( ConfigSynthetic::DebugPhaseID == ALUMINIUM ) {
												//Paper18 pure Aluminium lattice
												speci Al27 = speci( 0.00, 26.8, 1 );
												mass2charge = Al27.m2q;
											}
											else if ( ConfigSynthetic::DebugPhaseID == AL3SC ) {
												//Paper18 pure Al3Sc lattice
												speci Al27 = speci( 0.00, 26.982 / 1.0, 1 ); //+1
												speci Sc45 = speci( 0.00, 44.955 / 2.0, 4 ); //assume +2
												mass2charge = (b != 0) ? Al27.m2q : Sc45.m2q;
											}
											else if ( ConfigSynthetic::DebugPhaseID == WOLFRAM ) {
												speci W3XX = speci( 0.00, 183.84 / 3.0, 5 ); //+3
												mass2charge = W3XX.m2q;
											}
											else if ( ConfigSynthetic::DebugPhaseID == AL3LI ) {
												speci Li7 = speci( 0.00, 6.941 / 1.0 , 1 ); //assume +1
												speci Al27 = speci( 0.00, 26.982 / 1.0, 1 ); //+1
												mass2charge = ( b != 0 ) ? Al27.m2q : Li7.m2q;
											}
											else {
												//normal ##MK::we need a more sophisticated way to inject structures but not in a proof of concept implementation
												speci coin = solutes.get_random_speci();
												mass2charge = coin.m2q;
											}

#ifndef SPACEBUCKETING_ACTIVATE_ACCOUNTING
											//change to adding into a constant time query container
											rawdata_alf.add_atom( pos(ap.x, ap.y, ap.z, mass2charge) );
#else
											unsigned short shgid = ( clpid < UINT16MX ) ? static_cast<unsigned short>(clpid) : UINT16MX;
											rawdata_alf.add_atom_info( pos(ap.x, ap.y, ap.z, mass2charge ), shgid );
#endif
											stats[0]++;
										} //accept ion
									}
									else {
										stats[3]++;
										/*//if ( clpid == 8 )
										//	rawdata_alf.add_atom_info( pos(ap.x, ap.y, ap.z, 0.0 ), 3 );
											//cout << 3 << "\t\t" << ap.x << ";" << ap.y << ";" << ap.z << endl;
										//if ( clpid == 8 )
										//	cout << "ap kicked because not in tip" << endl;
										if ( clpid == 122 ) {
											cout << ap.x << ";" << ap.y << ";" << " clpid122 rejected not in tip" << "\n";
										}*/
									}
								} //done processing an ion of grain clpid
								else {
									stats[2]++;
									/*//if ( clpid == 8 )
									//	rawdata_alf.add_atom_info( pos(ap.x, ap.y, ap.z, 0.0 ), 2 );
										//cout << 2 << "\t\t" << ap.x << ";" << ap.y << ";" << ap.z << endl;
									//	cout << "ap kicked because current is not closest" << endl;
									if ( clpid == 122 ) {
										cout << ap.x << ";" << ap.y << ";" << " clpid122 rejected not closest" << "\n";
									}*/
								}
							} //done with u, an candidate ion from the local lattice
						} //done with v
					} //done with base
				} //done with w

				polycrystal.grains.at(clpid).natoms += static_cast<unsigned int>(stats[0]);

cout << "Grain " << clpid << " was constructed successfully" << "\n";
cout << "Inside/NotInCBox/NotFindVoroClosest/NotInTip\t\t" << stats[0] << ";" << stats[1] << ";" << stats[2] << ";" << stats[3] << "\n";
			} //done synthezising all ions from the current, a valid, Voronoi cell
			else { //reporting an unconstructable Voronoi cell
cerr << "A grain was inconstructible" << "\n";
			}
			//##MK::need to improve parallelization concept
		} while (cl.inc());
	} //done with processing the Voronoi tessellation

	double toc = omp_get_wtime();
	memsnapshot mm = synthetic_tictoc.get_memoryconsumption();
	synthetic_tictoc.prof_elpsdtime_and_mem( "GeneratePXMMMTip", APT_XX, APT_IS_PAR, mm, tic, toc);

cout << "Specimen synthesis took " << (toc-tic) << " seconds" << "\n";

	return true;
}


bool syntheticHdl::generate_synthetic_precipitates_debug()
{
	double tic = omp_get_wtime();
	memsnapshot mm = memsnapshot();

	//##MK::i.e. carve out from the matrix spherical volumina and place spherical precipitates there
	//##MK::add precipitates of a second phase, for now spatially uncorrelatedly distributed spheres which do not overlap
	if ( ConfigSynthetic::SynthesizingMode != SYNTHESIS_DEVELOPMENT_SX ) {
		cerr << "Currently only in SynthesizingMode == 1 we can place precipitates!" << "\n";
		return true;
	}

	if ( ConfigSynthetic::NumberOfCluster > 0 ) {
		//read orientation of the precipitates
		//unitcell_geometry Al3Sc_UnitCell = ConfigSynthetic::LatticeB;

		bunge euler_prec = bunge( ConfigSynthetic::SimPrecipitateOrientation );
cout << "Euler_prec " << euler_prec << "\n";
		squat local_quat_prec = euler_prec.eu2qu();
		t3x3 local_ori_prec = euler_prec.eu2om();
cout << "Local_ori_prec " << local_ori_prec << "\n";

		//define a population of geometric primitives which represent the precipitates ##MK::here spheres for now
		prec = precipitateModel();
		prec.initialize( tipgeometry, ConfigSynthetic::NumberOfCluster, ConfigSynthetic::ClusterRadiusMean, ConfigSynthetic::ClusterRadiusSigmaSqr );

		mt19937 mydice;
		mydice.seed( ConfigSynthetic::PRNGWorldSeed );
		mydice.discard( ConfigShared::RndDescrStatsPRNGDiscard ); //##MK::use ConfigSynthetic::PRNGWarmup
		uniform_real_distribution<apt_real> unifrnd(0.f, 1.f);
		normal_distribution<apt_real> gaussian_x(0.f, ConfigSynthetic::SpatResolutionSigmaX ); //mean is zero, stdev is user-defined
		normal_distribution<apt_real> gaussian_y(0.f, ConfigSynthetic::SpatResolutionSigmaY );
		normal_distribution<apt_real> gaussian_z(0.f, ConfigSynthetic::SpatResolutionSigmaZ );

		double toc = omp_get_wtime();
		mm = memsnapshot();
		synthetic_tictoc.prof_elpsdtime_and_mem( "DefinedPrecipitateLocationsInTip", APT_XX, APT_IS_SEQ, mm, tic, toc);
cout << "Precipitate location identification took " << (toc-tic) << " seconds" << "\n";

		tic = omp_get_wtime();

		vector<pos> newatoms;
		size_t ii = 0;
		for( auto it = prec.particles.begin(); it != prec.particles.end(); it++, ii++ ) {
			//erode all host atoms falling inside the spherical ball at it->center within it->radius

			//##MK::we do not relax the structure via MD here!
			p3d ballcenter = it->center;
			apt_real radius = it->radius;

	//cout << "Ballcenter/Radius\t\t" << ballcenter.x << ";" << ballcenter.y << ";" << ballcenter.z << "\t\t" << radius << endl;

			erase_log status = rawdata_alf.erase_rball( ballcenter, radius );

			//place instead the atoms within the unit cell aggregate of the spherical cluster
			//sample orientations from quasi-random texture
			/*squat ori_rnd = squat( unifrnd(mydice), unifrnd(mydice), unifrnd(mydice) );
			t3x3 ori_prec = ori_rnd.qu2om();*/
			//assign every precipitate the same a-priori defined orientation
			t3x3 ori_prec = local_ori_prec;

			newatoms = vector<pos>();
			//it->get_atoms( Al3Sc_UnitCell, ori_prec, newatoms ); //the precipitate knows its size
			it->get_atoms( ConfigSynthetic::SimPrecipitateStructure, ori_prec, newatoms );

			prec.orientation.at(ii) = local_quat_prec;

			size_t added = 0;
			for( size_t j = 0; j < newatoms.size(); j++) {
				p3d cand = p3d( newatoms.at(j).x, newatoms[j].y, newatoms[j].z );
				if ( tipgeometry.is_inside( cand ) == true ) { //also in reality cluster/precipitates are usually cut by tip boundary
					added++;
					if ( unifrnd(mydice) > ConfigSynthetic::SimDetEfficiency ) //every eta-th gets through so every 1-eta not, if eta = 0.0, almost every dice result > 0.0 discard, if eta --> 1 almost no dice result is > 1-small number
						continue;

					//spatial detection inaccuracy
					pos ap = newatoms.at(j);

					//##MK::another assumption here strictly speaking as there is no MD relaxation term here as the atoms might potentially overlap,
					//as is possible however also in a poor reconstruction model for APT data...!
					ap.x += gaussian_x(mydice);
					ap.y += gaussian_y(mydice);
					ap.z += gaussian_z(mydice);

					rawdata_alf.add_atom( ap );

				} //placement attempt of included atom in current cluster
			} //next atom of current cluster

	cout << "Cluster placed cleared/kept/newatoms planned/new atoms placed\t\t" << status.ncleared << ";" << status.nkept << ";" << newatoms.size() << ";" << added << "\n";
		} //next precipitate
	} //done with precipitates

	//##MK::all more advanced tip synthesis stuff comes as post-processing

	double toc = omp_get_wtime();
	mm = synthetic_tictoc.get_memoryconsumption();
	synthetic_tictoc.prof_elpsdtime_and_mem( "ReplacedMatrixAtomsWithParticleAtoms", APT_XX, APT_IS_SEQ, mm, tic, toc);

cout << "Atom replacement via addition of precipitates took " << (toc-tic) << " seconds" << "\n";
	return true;
}


void syntheticHdl::get_total_ion_count()
{
	//how many ions in total?
	nevt = 0;
	for (size_t b = 0; b < rawdata_alf.buckets.size(); ++b) {
		if ( rawdata_alf.buckets.at(b) != NULL ) {
			nevt = nevt + static_cast<unsigned int>(rawdata_alf.buckets.at(b)->size()); //##MK::increase size
		}
	}
	cout << "Rank " << MASTER << " there are " << nevt << " ions in total!" << "\n";
}


bool syntheticHdl::init_target_file()
{
	//##MK::generate group hierarchy of an open source IFES APTTC APTH5 HDF5 file
	string h5fn_out = "PARAPROBE.Synthetic.Results.SimID." + to_string(ConfigShared::SimID) + ".h5"; //".apth5";
cout << "Initializing target file " << h5fn_out << "\n";

	if ( debugh5Hdl.create_synthetic_apth5( h5fn_out ) == WRAPPED_HDF5_SUCCESS ) {
		return true;
	}
	else {
		return false;
	}
}


bool syntheticHdl::write_environment_and_settings()
{
	vector<pparm> hardware = synthetic_tictoc.report_machine();
	string prfx = PARAPROBE_SYNTH_VOLRECON_META_HRDWR;
	for( auto it = hardware.begin(); it != hardware.end(); it++ ) {
		cout << it->keyword << "__" << it->value << "__" << it->unit << "__" << it->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *it ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing hardware setting " << it->keyword << " failed!" << "\n";
			return false;
		}
	}
	cout << "Rank " << MASTER << " hardware settings written to H5" << "\n";

	vector<pparm> software;
	ConfigSynthetic::reportSettings( software );
	prfx = PARAPROBE_SYNTH_VOLRECON_META_SFTWR;
	for( auto jt = software.begin(); jt != software.end(); jt++ ) {
		cout << jt->keyword << "__" << jt->value << "__" << jt->unit << "__" << jt->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *jt ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing software tool setting " << jt->keyword << " failed!" << "\n";
			return false;
		}
	}
	cout << "Rank " << MASTER << " software settings written to H5" << "\n";

	return true;
}


void syntheticHdl::specimen_to_apth5()
{
	int status = WRAPPED_HDF5_SUCCESS;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	vector<float> f32;
	vector<unsigned int> u32;

	//##MK::populate for now in one go, later replace by successive adding to file
	for (size_t b = 0; b < rawdata_alf.buckets.size(); ++b) {
		if ( rawdata_alf.buckets.at(b) != NULL ) {
			for( auto it = rawdata_alf.buckets.at(b)->begin(); it != rawdata_alf.buckets.at(b)->end(); it++ ) {
				f32.push_back( it->x );
				f32.push_back( it->y );
				f32.push_back( it->z );
			}
		}
	}
	size_t nions = f32.size()/PARAPROBE_SYNTH_VOLRECON_RES_XYZ_NCMAX;

	//domain specific HDF5 keywords and data fields
	ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_RES_XYZ,
			f32.size()/PARAPROBE_SYNTH_VOLRECON_RES_XYZ_NCMAX, PARAPROBE_SYNTH_VOLRECON_RES_XYZ_NCMAX);
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SYNTH_VOLRECON_RES_XYZ create failed!" << "\n"; return;
	}
cout << "PARAPROBE_SYNTH_VOLRECON_RES_XYZ create " << status << "\n";
	offs = h5offsets( 0, f32.size()/PARAPROBE_SYNTH_VOLRECON_RES_XYZ_NCMAX,
			0, PARAPROBE_SYNTH_VOLRECON_RES_XYZ_NCMAX,
				f32.size()/PARAPROBE_SYNTH_VOLRECON_RES_XYZ_NCMAX, PARAPROBE_SYNTH_VOLRECON_RES_XYZ_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SYNTH_VOLRECON_RES_XYZ write failed!" << "\n"; return;
	}
cout << "PARAPROBE_SYNTH_VOLRECON_RES_XYZ write " << status << "\n";
	f32 = vector<float>();

	//write mass2charge
	for (size_t b = 0; b < rawdata_alf.buckets.size(); ++b) {
		if ( rawdata_alf.buckets.at(b) != NULL ) {
			for( auto it = rawdata_alf.buckets.at(b)->begin(); it != rawdata_alf.buckets.at(b)->end(); it++ ) {
				f32.push_back( it->mq );
			}
		}
	}

	//string dummy = "";
	//##MK::vector<pair<string,string>> attrbuf;
	//attrbuf.clear()
	//attrbuf.push_back( make_pair("SIUnit_DetX","mm") );
	//attrbuf.push_back( make_pair("SIUnit_DetY","mm") );
	//status = debugh5Hdl.write_attribute_string( APTH5_ACQ_02_DETECTOR_SIZE, attrbuf );

	ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_RES_MQ, f32.size()/PARAPROBE_SYNTH_VOLRECON_RES_MQ_NCMAX,
			PARAPROBE_SYNTH_VOLRECON_RES_MQ_NCMAX);
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SYNTH_VOLRECON_RES_MQ create failed!" << "\n"; return;
	}
cout << "PARAPROBE_SYNTH_VOLRECON_RES_MQ create " << status << "\n";
	offs = h5offsets( 0, f32.size()/PARAPROBE_SYNTH_VOLRECON_RES_MQ_NCMAX,
			0, PARAPROBE_SYNTH_VOLRECON_RES_MQ_NCMAX,
				f32.size()/PARAPROBE_SYNTH_VOLRECON_RES_MQ_NCMAX, PARAPROBE_SYNTH_VOLRECON_RES_MQ_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SYNTH_VOLRECON_RES_MQ write failed!" << "\n"; return;
	}
cout << "PARAPROBE_SYNTH_VOLRECON_RES_MQ write " << status << "\n";
	f32 = vector<float>();

	//##MK::we add XDMF Polyvertex topology support for visualization using XDMF Paraview and VisIt
	u32.reserve( 3*nevt );
	unsigned int ionID = 0;
	for (size_t b = 0; b < rawdata_alf.buckets.size(); ++b) {
		if ( rawdata_alf.buckets.at(b) != NULL ) {
			for( auto it = rawdata_alf.buckets.at(b)->begin(); it != rawdata_alf.buckets.at(b)->end(); it++ ) {
				u32.push_back( 1 ); //XDMF vertex type
				u32.push_back( 1 ); //XDMF count
				u32.push_back( ionID );
				++ionID;
			}
		}
	}
	//therewith we can filter ions based on attribute data within Paraview
	ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_RES_TOPO, u32.size()/PARAPROBE_SYNTH_VOLRECON_RES_TOPO_NCMAX, PARAPROBE_SYNTH_VOLRECON_RES_TOPO_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SYNTH_VOLRECON_RES_TOPO create failed!" << "\n"; return;
	}
cout << "PARAPROBE_SYNTH_VOLRECON_RES_TOPO create " << status << "\n";
	offs = h5offsets( 0, u32.size()/PARAPROBE_SYNTH_VOLRECON_RES_TOPO_NCMAX,
			0, PARAPROBE_SYNTH_VOLRECON_RES_TOPO_NCMAX,
				u32.size()/PARAPROBE_SYNTH_VOLRECON_RES_TOPO_NCMAX, PARAPROBE_SYNTH_VOLRECON_RES_TOPO_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SYNTH_VOLRECON_RES_TOPO write failed!" << "\n"; return;
	}
cout << "PARAPROBE_SYNTH_VOLRECON_RES_TOPO write " << status << "\n";
	u32 = vector<unsigned int>();

	string xdmffn = "PARAPROBE.Synthetic.Results.SimID." + to_string(ConfigShared::SimID) + ".Recon.xdmf";
	debugxdmf.create_volrecon_file( xdmffn, nions, debugh5Hdl.h5resultsfn );
}


void syntheticHdl::grains_to_apth5()
{
	int status = WRAPPED_HDF5_SUCCESS;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	vector<float> f32;
	vector<double> f64;
	vector<unsigned int> u32;

	//report grainAggregate
	if ( polycrystal.grains.size() > 0 ) {
		string grpnm = PARAPROBE_SYNTH_VOLRECON_META_GRNS;
		status = debugh5Hdl.create_group( grpnm );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_GRNS group creation failed! " << status << "\n"; return;
		}

		//grain orientation
		f32.reserve( PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI_NCMAX*polycrystal.grains.size() );
		for( auto it = polycrystal.grains.begin(); it != polycrystal.grains.end(); it++ ) {
			f32.push_back( it->ori.q0 );
			f32.push_back( it->ori.q1 );
			f32.push_back( it->ori.q2 );
			f32.push_back( it->ori.q3 );
		}
		ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI, f32.size()/PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI_NCMAX,
				PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI create failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI create " << status << "\n";
		offs = h5offsets( 0, f32.size()/PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI_NCMAX, 0, PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI_NCMAX,
				f32.size()/PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI_NCMAX, PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI write failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI write " << status << "\n";
		f32 = vector<float>();

		//grain IDs
		u32.reserve( polycrystal.grains.size()*PARAPROBE_SYNTH_VOLRECON_META_GRNS_ID_NCMAX );
		for( auto it = polycrystal.grains.begin(); it != polycrystal.grains.end(); it++ ) {
			u32.push_back( it->id );
		}
		ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_META_GRNS_ID, u32.size()/PARAPROBE_SYNTH_VOLRECON_META_GRNS_ID_NCMAX, PARAPROBE_SYNTH_VOLRECON_META_GRNS_ID_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_ID create failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_ID create " << status << "\n";
		offs = h5offsets( 0, u32.size()/PARAPROBE_SYNTH_VOLRECON_META_GRNS_ID_NCMAX, 0, PARAPROBE_SYNTH_VOLRECON_META_GRNS_ID_NCMAX,
				u32.size()/PARAPROBE_SYNTH_VOLRECON_META_GRNS_ID_NCMAX, PARAPROBE_SYNTH_VOLRECON_META_GRNS_ID_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_ID write failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_ID write " << status << "\n";
		u32 = vector<unsigned int>();

		//grain number of atoms
		u32.reserve( polycrystal.grains.size()*PARAPROBE_SYNTH_VOLRECON_META_GRNS_NATMS_NCMAX );
		for( auto it = polycrystal.grains.begin(); it != polycrystal.grains.end(); it++ ) {
			u32.push_back( it->natoms );
		}
		ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_META_GRNS_NATMS, u32.size()/PARAPROBE_SYNTH_VOLRECON_META_GRNS_NATMS_NCMAX, PARAPROBE_SYNTH_VOLRECON_META_GRNS_NATMS_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_NATMS create failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_NATMS create " << status << "\n";
		offs = h5offsets( 0, u32.size()/PARAPROBE_SYNTH_VOLRECON_META_GRNS_NATMS_NCMAX, 0, PARAPROBE_SYNTH_VOLRECON_META_GRNS_NATMS_NCMAX,
				u32.size()/PARAPROBE_SYNTH_VOLRECON_META_GRNS_NATMS_NCMAX, PARAPROBE_SYNTH_VOLRECON_META_GRNS_NATMS_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_NATMS write failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_NATMS write " << status << "\n";
		u32 = vector<unsigned int>();

		//grain volume, ##MK::if measured e.g. through voro++ then including volume outside the container
		f64.reserve( polycrystal.grains.size()*PARAPROBE_SYNTH_VOLRECON_META_GRNS_VOL_NCMAX );
		for( auto it = polycrystal.grains.begin(); it != polycrystal.grains.end(); it++ ) {
			f64.push_back( it->cellvolume );
		}
		ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_META_GRNS_VOL, f64.size()/PARAPROBE_SYNTH_VOLRECON_META_GRNS_VOL_NCMAX, PARAPROBE_SYNTH_VOLRECON_META_GRNS_VOL_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f64le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_VOL create failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_VOL create " << status << "\n";
		offs = h5offsets( 0, f64.size()/PARAPROBE_SYNTH_VOLRECON_META_GRNS_VOL_NCMAX, 0, PARAPROBE_SYNTH_VOLRECON_META_GRNS_VOL_NCMAX,
				f64.size()/PARAPROBE_SYNTH_VOLRECON_META_GRNS_VOL_NCMAX, PARAPROBE_SYNTH_VOLRECON_META_GRNS_VOL_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_f64le_hyperslab( ifo, offs, f64 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_VOL write failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_GRNS_VOL write " << status << "\n";
		f64 = vector<double>();
	}
}


void syntheticHdl::precipitates_to_apth5()
{
	int status = WRAPPED_HDF5_SUCCESS;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	vector<float> f32;
	vector<unsigned int> u32;

	//in case of precipitates added, report this also
	if ( prec.particles.size() > 0 ) {
		string grpnm = PARAPROBE_SYNTH_VOLRECON_META_PREC;
		status = debugh5Hdl.create_group( grpnm );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_PREC group creation failed! " << status << "\n"; return;
		}

		//particle centers xyz
		f32.reserve( PARAPROBE_SYNTH_VOLRECON_META_PREC_XYZ_NCMAX*prec.particles.size() );
		for( auto it = prec.particles.begin(); it != prec.particles.end(); it++ ) {
			f32.push_back( it->center.x );
			f32.push_back( it->center.y );
			f32.push_back( it->center.z );
		}
		ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_META_PREC_XYZ, f32.size()/PARAPROBE_SYNTH_VOLRECON_META_PREC_XYZ_NCMAX, PARAPROBE_SYNTH_VOLRECON_META_PREC_XYZ_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_PREC_XYZ create failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_PREC_XYZ create " << status << "\n";
		offs = h5offsets( 0, f32.size()/PARAPROBE_SYNTH_VOLRECON_META_PREC_XYZ_NCMAX, 0, PARAPROBE_SYNTH_VOLRECON_META_PREC_XYZ_NCMAX,
				f32.size()/PARAPROBE_SYNTH_VOLRECON_META_PREC_XYZ_NCMAX, PARAPROBE_SYNTH_VOLRECON_META_PREC_XYZ_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_PREC_XYZ write failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_PREC_XYZ write " << status << "\n";
		f32 = vector<float>();

		//XDMF topology
		u32 = vector<unsigned int>( prec.particles.size()*3, 1 ); //XDMF type key for point, then one can easily add glyph of radius SYNTH_VOLRECON_META_PREC_R as attribute to visualize the particles
		for( size_t i = 0; i < prec.particles.size(); i++ ) {
			u32.at(3*i+2) = i;
		}
		ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_META_PREC_TOPO, u32.size()/PARAPROBE_SYNTH_VOLRECON_META_PREC_TOPO_NCMAX, PARAPROBE_SYNTH_VOLRECON_META_PREC_TOPO_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_PREC_TOPO create failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_PREC_TOPO create " << status << "\n";
		offs = h5offsets( 0, u32.size()/PARAPROBE_SYNTH_VOLRECON_META_PREC_TOPO_NCMAX, 0, PARAPROBE_SYNTH_VOLRECON_META_PREC_TOPO_NCMAX,
				u32.size()/PARAPROBE_SYNTH_VOLRECON_META_PREC_TOPO_NCMAX, PARAPROBE_SYNTH_VOLRECON_META_PREC_TOPO_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_PREC_TOPO write failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_PREC_TOPO write " << status << "\n";
		u32 = vector<unsigned int>();

		//particle radii
		f32.reserve( prec.particles.size() );
		for( auto it = prec.particles.begin(); it != prec.particles.end(); it++ ) {
			f32.push_back( it->radius );
		}
		ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_META_PREC_R, f32.size()/PARAPROBE_SYNTH_VOLRECON_META_PREC_R_NCMAX, PARAPROBE_SYNTH_VOLRECON_META_PREC_R_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_PREC_R create failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_PREC_R create " << status << "\n";
		offs = h5offsets( 0, f32.size()/PARAPROBE_SYNTH_VOLRECON_META_PREC_R_NCMAX, 0, PARAPROBE_SYNTH_VOLRECON_META_PREC_R_NCMAX,
				f32.size()/PARAPROBE_SYNTH_VOLRECON_META_PREC_R_NCMAX, PARAPROBE_SYNTH_VOLRECON_META_PREC_R_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_PREC_R write failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_PREC_R write " << status << "\n";
		f32 = vector<float>();

		if ( prec.particles.size() != prec.orientation.size() ) {
			cerr << "prec.particles and prec.orientation are inconsistent wrt to their size!" << "\n"; return;
		}

		f32.reserve( 4*prec.particles.size() );
		for( auto it = prec.orientation.begin(); it != prec.orientation.end(); it++ ) {
			f32.push_back( it->q0 );
			f32.push_back( it->q1 );
			f32.push_back( it->q2 );
			f32.push_back( it->q3 );
		}
		ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_META_PREC_ROT, f32.size()/PARAPROBE_SYNTH_VOLRECON_META_PREC_ROT_NCMAX, PARAPROBE_SYNTH_VOLRECON_META_PREC_ROT_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_PREC_ROT create failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_PREC_ROT create " << status << "\n";
		offs = h5offsets( 0, f32.size()/PARAPROBE_SYNTH_VOLRECON_META_PREC_ROT_NCMAX, 0, PARAPROBE_SYNTH_VOLRECON_META_PREC_ROT_NCMAX,
				f32.size()/PARAPROBE_SYNTH_VOLRECON_META_PREC_ROT_NCMAX, PARAPROBE_SYNTH_VOLRECON_META_PREC_ROT_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_PREC_ROT write failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_PREC_ROT write " << status << "\n";
		f32 = vector<float>();

		string xdmffn = "PARAPROBE.Synthetic.Results.SimID." + to_string(ConfigShared::SimID) + ".Particles.xdmf";
		debugxdmf.create_precipitate_file( xdmffn, prec.particles.size(), debugh5Hdl.h5resultsfn );
	}
}


void syntheticHdl::tessellation_to_apth5()
{
	if ( ConfigSynthetic::SynthesizingMode == SYNTHESIS_DEVELOPMENT_PX ) {
		//report the Voronoi tessellation
		//first of all translate all local IDs into global IDs
		//##MK::relabeling of cell topology required because vertex indices are per cell
		size_t VertexOffset = 0;
		size_t CurrentCellID = 0;
		for( size_t i = 1; i < tess.io_topo.size();   ) { //first value tells XDMF topology typ index of first local cell ignore this value
			size_t CurrentCellNumberOfFaces = tess.io_info.at(CurrentCellID).nfacets;
			for( size_t f = 0; f < CurrentCellNumberOfFaces; f++ ) {
				size_t CurrentFacetNumberOfIndices = tess.io_topo.at(i);
				i++;
				for( size_t k = 0; k < CurrentFacetNumberOfIndices; k++ ) {
					tess.io_topo.at(i+k) = tess.io_topo.at(i+k) + VertexOffset;
				}
				i = i + CurrentFacetNumberOfIndices + 1; //skip updated values skip XDMF type key
			}
			//cout << "CurrentCellNumberOfFaces--->" << CurrentCellNumberOfFaces << "\t\t\t" << VertexOffset << endl;
			VertexOffset = VertexOffset + tess.io_info.at(CurrentCellID).ngeom;
			CurrentCellID++;
		}

		cout << "XDMF Voronoi cell topology" << "\n";
		size_t NumberOfElements = 0;
		for( auto it = tess.io_info.begin(); it != tess.io_info.end(); ++it ) {
			NumberOfElements += it->nfacets;
		}
		cout << "Number of geom. elements " << NumberOfElements << "\n";
		cout << "Number of topology values " << tess.io_topo.size() << "\n";
		cout << "Number of geometry values " << tess.io_geom.size() << "\n";
		cout << "Number of scalar cell attribute values " << tess.io_halo.size() << "\n";

		string xmlfn = "PARAPROBE.Synthetic.Results.SimID." + to_string(ConfigShared::SimID) + ".VoroTess.xdmf";
		debugxdmf.create_voronoicrystal_file( xmlfn, NumberOfElements, tess.io_topo.size()/1,
					tess.io_geom.size()/3, tess.io_halo.size()/1, debugh5Hdl.h5resultsfn);

		//hdf5 writing
		int status = WRAPPED_HDF5_SUCCESS;
		h5iometa ifo = h5iometa();
		h5offsets offs = h5offsets();

		string grpnm = PARAPROBE_SYNTH_VOLRECON_META_TESS;
		status = debugh5Hdl.create_group( grpnm );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_TESS group creation failed! " << status << "\n"; return;
		}

		//voronoi cell facet topology
		ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO, tess.io_topo.size()/PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO_NCMAX,
				PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO create failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO create " << status << "\n";
		offs = h5offsets( 0, tess.io_topo.size()/PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO_NCMAX,
				0, PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO_NCMAX,
				tess.io_topo.size()/PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO_NCMAX, PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, tess.io_topo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO write failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO write " << status << "\n";
		tess.io_topo = vector<unsigned int>();

		//voronoi cell facet edge vertices xyz
		ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ,
					tess.io_geom.size()/PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ_NCMAX,
						PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ create failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ create " << status << "\n";
		offs = h5offsets( 0, tess.io_geom.size()/PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ_NCMAX,
				0, PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ_NCMAX,
				tess.io_geom.size()/PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ_NCMAX, PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, tess.io_geom );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ write failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ write " << status << "\n";
		tess.io_geom = vector<float>();

		//halo info
		ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO, tess.io_halo.size()/PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO_NCMAX,
						PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_i32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO create failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO create " << status << "\n";
		offs = h5offsets( 0, tess.io_halo.size()/PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO_NCMAX,
				0, PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO_NCMAX,
				tess.io_geom.size()/PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO_NCMAX, PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_i32le_hyperslab( ifo, offs, tess.io_halo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO write failed! " << status << "\n"; return;
		}
		cout << "PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO write " << status << "\n";
		tess.io_halo = vector<int>();
	}
}
