//##MK::GPLV3

#ifndef __PARAPROBE_SYNTHETIC_TESSELLATION_H__
#define __PARAPROBE_SYNTHETIC_TESSELLATION_H__

#include "PARAPROBE_VoroXXInterface.h"


class tessHdl
{
public:
	tessHdl();
	~tessHdl();

	//void initialize( geomodel const & geom, const apt_real rmean );
	vector<float> io_geom;
	vector<unsigned int> io_topo;
	vector<int> io_halo;
	vector<voro_io_info> io_info;
};


#endif

