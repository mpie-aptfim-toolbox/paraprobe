//##MK::GPLV3

#ifndef __PARAPROBE_CONFIG_SYNTHETIC_H__
#define __PARAPROBE_CONFIG_SYNTHETIC_H__

//#include "../../paraprobe-utils/src/CONFIG_Shared.h"
#include "../../paraprobe-utils/src/PARAPROBE_FFT.h"



//move to Crystallography or Datatypes
struct unitcell_geometry
{
	apt_real a;
	apt_real b;
	apt_real c;
	apt_real alpha;
	apt_real beta;
	apt_real gamma;
	//string spacegroup;
	unitcell_geometry() : a(1.0), b(1.0), c(1.0), alpha(DEGREE2RADIANT(90.0)), beta(DEGREE2RADIANT(90.0)), gamma(DEGREE2RADIANT(90.0)) {}
	unitcell_geometry( const apt_real _a, const apt_real _b, const apt_real _c, const apt_real _alpha, const apt_real _beta, const apt_real _gamma ) :
		a(_a), b(_b), c(_c), alpha(DEGREE2RADIANT(_alpha)), beta(DEGREE2RADIANT(_beta)), gamma(DEGREE2RADIANT(_gamma)) {}
};

enum ORIGIN_OF_DATASET {
	EXPERIMENT,
	SYNTHETIC
};


enum SYNTHESIS_MODE {
	SYNTHESIS_NOTHING,						//nothing
	SYNTHESIS_DEVELOPMENT_SX,				//orthorhombic lattice singlecrystal
	SYNTHESIS_DEVELOPMENT_PX				//orthorhombic lattice polycrystal
};


class ConfigSynthetic
{
public:
	
	static ORIGIN_OF_DATASET DatasetOrigin;
	static SYNTHESIS_MODE SynthesizingMode;
	//static string InputfilePSE;

	static unsigned int DebugPhaseID;
	static crystalstructure SimMatrixStructure;
	static crystalstructure SimPrecipitateStructure;

	static string SimMatrixOrientation;
	static string SimPrecipitateOrientation;

	static size_t NumberOfAtoms;
	static apt_real RelBottomRadius;
	static apt_real RelTopRadius;
	static apt_real RelBottomCapHeight;
	static apt_real RelTopCapHeight;
	static apt_real SimDetEfficiency;			//to account for non-ideal detection efficiency
	static apt_real SpatResolutionSigmaX;
	static apt_real SpatResolutionSigmaY;
	static apt_real SpatResolutionSigmaZ;		//to account for finite APT resolution

	static size_t NumberOfCluster;
	static apt_real ClusterRadiusMean;
	static apt_real ClusterRadiusSigmaSqr;

	static unitcell_geometry LatticeA;
	static unitcell_geometry LatticeB;

	static apt_real CrystalloVoroMeanRadius;

	static string PRNGType;
	static size_t PRNGWarmup;
	static long PRNGWorldSeed;
	static size_t SimAnnealingIterMax;
	static size_t TessellationPointsPerBlock;

	//performance compromise spatial querying Voro++ needs to be at least 1.f + EPSILON will be interpreted into integer


	static bool readXML( string filename = "" );
	static bool checkUserInput();
	static void reportSettings( vector<pparm> & res );
};

#endif
