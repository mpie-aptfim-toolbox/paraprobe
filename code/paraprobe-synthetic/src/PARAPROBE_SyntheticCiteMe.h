//##MK::GPLV3


#ifndef __PARAPROBE_SYNTHETIC_CITEME_H__
#define __PARAPROBE_SYNTHETIC_CITEME_H__

#include "CONFIG_Synthetic.h"


class CiteMeSynthetic
{
public:
	
	static vector<bibtex> Citations;
	
	static size_t cite();
};

#endif

