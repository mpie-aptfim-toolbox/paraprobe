//##MK::GPLV3

#ifndef __PARAPROBE_SYNTHETIC_GRAINMODEL_H__
#define __PARAPROBE_SYNTHETIC_GRAINMODEL_H__

#include "PARAPROBE_TessellationHdl.h"

struct grain
{
	unsigned int id;
	unsigned int natoms;
	squat ori;

	p3d cellcenter;
	double cellvolume;

	grain() : id(0), natoms(0), ori(squat()), cellcenter(p3d()), cellvolume(0.0) {}
	grain( const unsigned int _id, squat const & in ) : id(_id), natoms(0),
		ori(squat(in.q0, in.q1, in.q2, in.q3)), cellcenter(p3d()), cellvolume(0.0) {}
	grain( const unsigned int _id, const unsigned int _natms, squat const & in, p3d const & _cellc, const double _cellvol ) :
		id(_id), natoms(_natms), ori(squat(in.q0, in.q1, in.q2, in.q3)),
		cellcenter(p3d(_cellc.x, _cellc.y, _cellc.z)), cellvolume(_cellvol) {}
};

ostream& operator<<(ostream& in, grain const & val);


class grainAggregate
{
public:
	grainAggregate();
	~grainAggregate();

	vector<grain> grains;
};

#endif

