//##MK::GPLV3

#include "CONFIG_Synthetic.h"

ORIGIN_OF_DATASET ConfigSynthetic::DatasetOrigin = SYNTHETIC;
SYNTHESIS_MODE ConfigSynthetic::SynthesizingMode = SYNTHESIS_NOTHING;
//string ConfigSynthetic::InputfilePSE = "PARAPROBE.PeriodicTableOfElements.xml";

unsigned int ConfigSynthetic::DebugPhaseID = ALUMINIUM;
crystalstructure ConfigSynthetic::SimMatrixStructure = crystalstructure();
crystalstructure ConfigSynthetic::SimPrecipitateStructure = crystalstructure();

string ConfigSynthetic::SimMatrixOrientation = "0.0;0.0;0.0";
string ConfigSynthetic::SimPrecipitateOrientation = "0.0;0.0;0.0";

size_t ConfigSynthetic::NumberOfAtoms = 0;
apt_real ConfigSynthetic::RelBottomRadius = 0.10;
apt_real ConfigSynthetic::RelTopRadius = 0.05;
apt_real ConfigSynthetic::RelBottomCapHeight = 0.05;
apt_real ConfigSynthetic::RelTopCapHeight = 0.05;
apt_real ConfigSynthetic::SimDetEfficiency = 1.0;
apt_real ConfigSynthetic::SpatResolutionSigmaX = 0.f; //nm
apt_real ConfigSynthetic::SpatResolutionSigmaY = 0.f; //nm
apt_real ConfigSynthetic::SpatResolutionSigmaZ = 0.f; //nm

apt_real ConfigSynthetic::CrystalloVoroMeanRadius = 5.f;

size_t ConfigSynthetic::NumberOfCluster = 0;
apt_real ConfigSynthetic::ClusterRadiusMean = EPSILON; //nm
apt_real ConfigSynthetic::ClusterRadiusSigmaSqr = EPSILON; //nm

unitcell_geometry ConfigSynthetic::LatticeA = unitcell_geometry( 0.404, 0.404, 0.404, 90.0, 90.0, 90.0 );
unitcell_geometry ConfigSynthetic::LatticeB = unitcell_geometry( 0.410, 0.410, 0.410, 90.0, 90.0, 90.0 );

string ConfigSynthetic::PRNGType = "MT19937";
size_t ConfigSynthetic::PRNGWarmup = 700000;
long ConfigSynthetic::PRNGWorldSeed = -12345678;
size_t ConfigSynthetic::SimAnnealingIterMax = 1000000;
size_t ConfigSynthetic::TessellationPointsPerBlock = 5;


bool ConfigSynthetic::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.Input.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigSynthetic")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;

	//WHAT TYPE OF ANALYSIS TO DO
	//InputfilePSE = read_xml_attribute_string( rootNode, "InputfilePSE" );

	mode = read_xml_attribute_uint32( rootNode, "SynthesizingMode" );
	switch (mode)
	{
		case SYNTHESIS_DEVELOPMENT_SX:
			SynthesizingMode = SYNTHESIS_DEVELOPMENT_SX; break;
		case SYNTHESIS_DEVELOPMENT_PX:
			SynthesizingMode = SYNTHESIS_DEVELOPMENT_PX; break;
		default:
			SynthesizingMode = SYNTHESIS_NOTHING;
	}
	//approximate specimen geometry
	NumberOfAtoms = read_xml_attribute_float( rootNode, "SimNumberOfAtoms" );
	RelBottomRadius = read_xml_attribute_float( rootNode, "SimRelBottomRadius" );
	RelTopRadius = read_xml_attribute_float( rootNode, "SimRelTopRadius" );
	RelBottomCapHeight = read_xml_attribute_float( rootNode, "SimRelBottomCapHeight" );
	RelTopCapHeight = read_xml_attribute_float( rootNode, "SimRelTopCapHeight" );

	//simple noise models
	SimDetEfficiency = read_xml_attribute_float( rootNode, "SimDetectionEfficiency" );
	SpatResolutionSigmaX = read_xml_attribute_float( rootNode, "SimFiniteSpatResolutionX" );
	SpatResolutionSigmaY = read_xml_attribute_float( rootNode, "SimFiniteSpatResolutionY" );
	SpatResolutionSigmaZ = read_xml_attribute_float( rootNode, "SimFiniteSpatResolutionZ" );

	//crystal structures to build
	if ( SynthesizingMode == SYNTHESIS_DEVELOPMENT_SX ) {
		mode = read_xml_attribute_uint32( rootNode, "DebugPhaseID" );
		switch(mode) {
			case AL3SC:
				DebugPhaseID = AL3SC; break;
			case WOLFRAM:
				DebugPhaseID = WOLFRAM; break;
			case AL3LI:
				DebugPhaseID = AL3LI; break;
			case SPACEGROUP:
				DebugPhaseID = SPACEGROUP; break;
			default:
				DebugPhaseID = ALUMINIUM;
		}
	}
	else { //all other modes are currently for developers only
		DebugPhaseID = ALUMINIUM;
	}

	if ( DebugPhaseID == SPACEGROUP ) {
		//##MK::the allowed space groups
		//vector<size_t> implemented_spcgrps = { 62, 194, 221, 225, 229 };

		//parse out matrix lattice unit cell
		xml_node<>* matrix_node = rootNode->first_node("MatrixLatticeUnitCell");
		string spcgrp = "";
		for (xml_node<> * proto_node = matrix_node->first_node("prototype"); proto_node; proto_node = proto_node->next_sibling() ) {
			spcgrp = proto_node->first_attribute("spacegroup")->value();
			break;
		}
		size_t spc = stoul( spcgrp );
		if ( spc > 230 ) {
			cerr << "For MatrixLatticeUnitCell an unphysical space group > 230 was given !" << "\n"; return false;
		}

		bool valid = false;
		for( auto it = ConfigShared::MySpacegroups.begin(); it != ConfigShared::MySpacegroups.end(); it++ ) {
			if ( *it != spc ) {
				continue;
			}
			else {
				valid = true;
				break;
			}
		}

		if ( valid == false ) { //spc != spc != 194 && spc != 221 && spc != 225 && spc != 229 ) {
			cerr << "For MatrixLatticeUnitCell an unknown space group (" << spc << ") was given, currently not all space groups supported !" << "\n"; return false;
		}

		string aa = "";
		string bb = "";
		string cc = "";
		for( xml_node<> * parm_node = matrix_node->first_node("parameter"); parm_node; parm_node = parm_node->next_sibling() ) {
			aa = parm_node->first_attribute("a")->value();
			bb = parm_node->first_attribute("b")->value();
			cc = parm_node->first_attribute("c")->value();
			break;
		}
		apt_real aaa = str2real( aa );
		apt_real bbb = str2real( bb );
		apt_real ccc = str2real( cc );
		if ( aaa <= EPSILON ) {
			cerr << "For MatrixLatticeUnitCell a needs to be positive and finite !" << "\n"; return false;
		}
		if ( bbb <= EPSILON ) {
			cerr << "For MatrixLatticeUnitCell b needs to be positive and finite !" << "\n"; return false;
		}
		if ( ccc <= EPSILON ) {
			cerr << "For MatrixLatticeUnitCell c needs to be positive and finite !" << "\n"; return false;
		}

		vector<motifunit> themotif;
		for( xml_node<> * basis_node = matrix_node->first_node("basis"); basis_node; basis_node = basis_node->next_sibling() ) {
			string symb = basis_node->first_attribute("symbol")->value();
			string u1 = basis_node->first_attribute("u1")->value();
			string u2 = basis_node->first_attribute("u2")->value();
			string u3 = basis_node->first_attribute("u3")->value();
			string chg = basis_node->first_attribute("chargestate")->value();

			motifunit tmp = motifunit();
			map<string, elementinfo>::iterator thisone = ConfigShared::MyElementsSymbol.find( symb );
			if ( thisone == ConfigShared::MyElementsSymbol.end() ) {
				cerr << "For MatrixLatticeUnitCell detected you seem to parse an element symbol that is not in the periodic table !" << "\n"; return false;
			}
			else {
				tmp.elementZ = thisone->second.Z;
			}
			unsigned int chrg = stoul( chg );
			if ( chrg == 0 || chrg > 255 ) {
				cerr << "For MatrixLatticeUnitCell detected you seem to parse a charge state that is either 0 or >255, this is invalid !" << "\n"; return false;
			}
			else {
				tmp.chargestate = static_cast<unsigned char>(chrg);
			}
			/*
			apt_real uu1 = str2real(u1);
			if ( uu1 <= (-1.0 - EPSILON) || uu1 >= (1.0 + EPSILON) ) {
				cerr << "For MatrixLatticeUnitCell fractional coordinate u1 is outside reasonable interval [-1.0, 1.0] !" << "\n"; return false;
			}
			else {
				tmp.u1 = uu1;
			}
			apt_real uu2 = str2real(u2);
			if ( uu2 <= (-1.0 - EPSILON) || uu2 >= (1.0 + EPSILON) ) {
				cerr << "For MatrixLatticeUnitCell fractional coordinate u2 is outside reasonable interval [-1.0, 1.0] !" << "\n"; return false;
			}
			else {
				tmp.u2 = uu2;
			}
			apt_real uu3 = str2real(u3);
			if ( uu3 <= (-1.0 - EPSILON) || uu3 >= (1.0 + EPSILON) ) {
				cerr << "For MatrixLatticeUnitCell fractional coordinate u3 is outside reasonable interval [-1.0, 1.0] !" << "\n"; return false;
			}
			else {
				tmp.u3 = uu3;
			}
			*/
			tmp.u1 = str2real(u1);
			tmp.u2 = str2real(u2);
			tmp.u3 = str2real(u3);
			
			cout << "For MatrixLatticeUnitCell accepting a motifunit " << tmp << "\n";
			themotif.push_back( tmp );
			cout << "MatrixLatticeUnitCell themotif.size() " << themotif.size() << "\n";
		}
		if ( themotif.size() < 1 ) {
			cerr << "For MatrixLatticeUnitCell not enough motifs !" << "\n"; return false;
		}
		else {
			SimMatrixStructure = crystalstructure( aaa, bbb, ccc, 90.0, 90.0, 90.0, spc, themotif ); //##MK::90.0, 90.0, 90.0 are dummy values
			cout << "SimMatrixStructure" << SimMatrixStructure << "\n";
		}
	}
	else {
		LatticeA = unitcell_geometry( 	read_xml_attribute_float( rootNode, "a1" ),
										read_xml_attribute_float( rootNode, "b1" ),
										read_xml_attribute_float( rootNode, "c1" ),
										read_xml_attribute_float( rootNode, "alpha1" ),
										read_xml_attribute_float( rootNode, "beta1" ),
										read_xml_attribute_float( rootNode, "gamma1" )    );

		LatticeB = unitcell_geometry( 	read_xml_attribute_float( rootNode, "a2" ),
										read_xml_attribute_float( rootNode, "b2" ),
										read_xml_attribute_float( rootNode, "c2" ),
										read_xml_attribute_float( rootNode, "alpha2" ),
										read_xml_attribute_float( rootNode, "beta2" ),
										read_xml_attribute_float( rootNode, "gamma2" )    );
	}

	//orientations
	SimMatrixOrientation = read_xml_attribute_string( rootNode, "SimMatrixOrientation" );


	//spherical cluster/precipitates in the dataset of a second phase
	NumberOfCluster = read_xml_attribute_uint32( rootNode, "SimNumberOfCluster" );
	if ( NumberOfCluster > 0 ) {
		//precipitate crystal structure
		//parse out precipitate lattice unit cell
		xml_node<>* cluster_node = rootNode->first_node("PrecipitateLatticeUnitCell");
		string spcgrp = "";
		for (xml_node<> * proto_node = cluster_node->first_node("prototype"); proto_node; proto_node = proto_node->next_sibling() ) {
			spcgrp = proto_node->first_attribute("spacegroup")->value();
			break;
		}
		size_t spc = stoul( spcgrp );
		if ( spc > 230 ) {
			cerr << "For PrecipitateLatticeUnitCell an unphysical space group > 230 was given !" << "\n"; return false;
		}

		bool valid = false;
		for( auto it = ConfigShared::MySpacegroups.begin(); it != ConfigShared::MySpacegroups.end(); it++ ) {
			if ( *it != spc ) {
				continue;
			}
			else {
				valid = true;
				break;
			}
		}

		if ( valid == false ) {
			cerr << "For PrecipitateLatticeUnitCell an unknown space group (" << spc << ") was given, currently not all space groups supported !" << "\n"; return false;
		}

		string aa = "";
		string bb = "";
		string cc = "";
		for( xml_node<> * parm_node = cluster_node->first_node("parameter"); parm_node; parm_node = parm_node->next_sibling() ) {
			aa = parm_node->first_attribute("a")->value();
			bb = parm_node->first_attribute("b")->value();
			cc = parm_node->first_attribute("c")->value();
			break;
		}
		apt_real aaa = str2real( aa );
		apt_real bbb = str2real( bb );
		apt_real ccc = str2real( cc );
		if ( aaa <= EPSILON ) {
			cerr << "For PrecipitateLatticeUnitCell a needs to be positive and finite !" << "\n"; return false;
		}
		if ( bbb <= EPSILON ) {
			cerr << "For PrecipitateLatticeUnitCell b needs to be positive and finite !" << "\n"; return false;
		}
		if ( ccc <= EPSILON ) {
			cerr << "For PrecipitateLatticeUnitCell c needs to be positive and finite !" << "\n"; return false;
		}

		vector<motifunit> themotif = vector<motifunit>();
		for( xml_node<> * basis_node = cluster_node->first_node("basis"); basis_node; basis_node = basis_node->next_sibling() ) {
			string symb = basis_node->first_attribute("symbol")->value();
			string u1 = basis_node->first_attribute("u1")->value();
			string u2 = basis_node->first_attribute("u2")->value();
			string u3 = basis_node->first_attribute("u3")->value();
			string chg = basis_node->first_attribute("chargestate")->value();

			motifunit tmp = motifunit();
			map<string, elementinfo>::iterator thisone = ConfigShared::MyElementsSymbol.find( symb );
			if ( thisone == ConfigShared::MyElementsSymbol.end() ) {
				cerr << "For PrecipitateLatticeUnitCell detected you seem to parse an element symbol that is not in the periodic table !" << "\n"; return false;
			}
			else {
				tmp.elementZ = thisone->second.Z;
			}
			unsigned int chrg = stoul( chg );
			if ( chrg == 0 || chrg > 255 ) {
				cerr << "For PrecipitateLatticeUnitCell detected you seem to parse a charge state that is either 0 or >255, this is invalid !" << "\n"; return false;
			}
			else {
				tmp.chargestate = static_cast<unsigned char>(chrg);
			}
			/*
			apt_real uu1 = str2real(u1);
			if ( uu1 <= (-1.0 - EPSILON) || uu1 >= (1.0 + EPSILON) ) {
				cerr << "For PrecipitateLatticeUnitCell fractional coordinate u1 is outside reasonable interval [-1.0, 1.0] !" << "\n"; return false;
			}
			else {
				tmp.u1 = uu1;
			}
			apt_real uu2 = str2real(u2);
			if ( uu2 <= (-1.0 - EPSILON) || uu2 >= (1.0 + EPSILON) ) {
				cerr << "For PrecipitateLatticeUnitCell fractional coordinate u2 is outside reasonable interval [-1.0, 1.0] !" << "\n"; return false;
			}
			else {
				tmp.u2 = uu2;
			}
			apt_real uu3 = str2real(u3);
			if ( uu3 <= (-1.0 - EPSILON) || uu3 >= (1.0 + EPSILON) ) {
				cerr << "For PrecipitateLatticeUnitCell fractional coordinate u3 is outside reasonable interval [-1.0, 1.0] !" << "\n"; return false;
			}
			else {
				tmp.u3 = uu3;
			}
			*/
			tmp.u1 = str2real(u1);
			tmp.u2 = str2real(u2);
			tmp.u3 = str2real(u3);			
			cout << "For PrecipitateLatticeUnitCell accepting a motifunit " << tmp << "\n";
			themotif.push_back( tmp );
			cout << "PrecipitateLatticeUnitCell themotif.size() " << themotif.size() << "\n";
		}
		if ( themotif.size() < 1 ) {
			cerr << "For PrecipitateLatticeUnitCell not enough motifs !" << "\n"; return false;
		}
		else {
			SimPrecipitateStructure = crystalstructure( aaa, bbb, ccc, 90.0, 90.0, 90.0, spc, themotif );
			cout << "SimPrecipitateStructure" << SimPrecipitateStructure << "\n";
		}

		SimPrecipitateOrientation = read_xml_attribute_string( rootNode, "SimPrecipitateOrientation" );
		ClusterRadiusMean = read_xml_attribute_float( rootNode, "SimClusterRadiusMean" );
		ClusterRadiusSigmaSqr = read_xml_attribute_float( rootNode, "SimClusterRadiusSigmaSqr" );
	}

	if ( SynthesizingMode == SYNTHESIS_DEVELOPMENT_PX ) {
		CrystalloVoroMeanRadius = read_xml_attribute_float( rootNode, "CrystalloVoroMeanRadius" );
	}

	return true;
}


bool ConfigSynthetic::checkUserInput()
{
	cout << "ConfigSynthetic::" << "\n";
	//cout << "InputfilePSE " << InputfilePSE << "\n";
	switch (SynthesizingMode)
	{
		case SYNTHESIS_DEVELOPMENT_SX:
			cout << "Building single-phase, single-crystalline virtual specimen of phase and orientation as specified" << "\n"; break;
		case SYNTHESIS_DEVELOPMENT_PX:
			cout << "Building single-phase, polycrystalline Al fcc virtual specimen from Voronoi-Tessellation with random texture" << "\n"; break;
		default:
			cerr << "No synthesis chosen about to quit" << "\n"; return false;
	}
	if ( NumberOfAtoms < 1 ) {
		cerr << "The tip must contain at least one atom!" << "\n"; return false;
	}
	cout << "Number of atoms " << NumberOfAtoms << "\n";
	if ( RelBottomRadius < EPSILON ) {
		cerr << "RelBottomRadius must be positive and non-zero!" << "\n"; return false;
	}
	cout << "RelBottomRadius " << RelBottomRadius << "\n";
	if ( RelTopRadius < EPSILON || RelTopRadius > RelBottomRadius ) {
		cerr << "RelTopRadius must not be larger than bottom, positive and non-zero!" << "\n"; return false;
	}
	cout << "RelTopRadius " << RelTopRadius << "\n";
	if ( RelBottomCapHeight < 0.f || RelBottomCapHeight > RelBottomRadius ) { //##MK::carve out not more than a halfsphere from bottom of the frustum
		cerr << "RelBottomCapHeight must be positive and not larger than RelBottomRadius!" << "\n"; return false;
	}
	cout << "RelBottomCapHeight " << RelBottomCapHeight << "\n";
	if ( RelTopCapHeight < 0.f || RelTopCapHeight > RelTopRadius ) {
		cerr << "RelTopCapHeight must be positive and not larger than RelTopRadius!" << "\n"; return false; //##MK::add further constraints on cap heights
	}
	cout << "RelTopCapHeight " << RelTopCapHeight << "\n";

	if ( SimDetEfficiency < EPSILON || SimDetEfficiency > 1.f ) {
		cerr << "The simulated detection efficiency needs to on interval (0,1]!" << "\n"; return false;
	}
	cout << "DetectionEfficiency " << SimDetEfficiency << "\n";
	if ( SpatResolutionSigmaX < 0.f || SpatResolutionSigmaY < 0.f || SpatResolutionSigmaZ < 0.f ) {
		cerr << "The simulated finite spatial resolutions must be positive if not zero!" << "\n"; return false;
	}
	cout << "FiniteSpatResSigmaX " << SpatResolutionSigmaX << "\n";
	cout << "FiniteSpatResSigmaY " << SpatResolutionSigmaY << "\n";
	cout << "FiniteSpatResSigmaZ " << SpatResolutionSigmaZ << "\n";

	switch(DebugPhaseID)
	{
		case ALUMINIUM:
			cout << "Debugging with phase pure Al" << "\n"; break;
		case AL3SC:
			cout << "Debugging with phase pure Al3Sc" << "\n"; break;
		case WOLFRAM:
			cout << "Debugging with phase pure W:" << "\n"; break;
		case AL3LI:
			cout << "Debugging with phase pure Al3Li" << "\n"; break;
		case SPACEGROUP:
			cout << "Debugging with specific space group candidate" << "\n"; break;
		default:
			cerr << "Unknown DebugPhaseID !" << "\n"; return false;
	}

	if ( DebugPhaseID != SPACEGROUP ) {
		if ( LatticeA.a < EPSILON || LatticeA.b < EPSILON || LatticeA.c < EPSILON ) {
			cerr << "LatticeConstantsA a1, b1, and c1 must be positive and non-zero!" << "\n"; return false;
		}
		cout << "a1 " << LatticeA.a << "\n";
		cout << "b1 " << LatticeA.b << "\n";
		cout << "c1 " << LatticeA.c << "\n";
		if ( LatticeA.alpha < EPSILON || LatticeA.alpha > DEGREE2RADIANT(180.f) ||
				LatticeA.beta < EPSILON || LatticeA.beta > DEGREE2RADIANT(180.f) ||
					LatticeA.gamma < EPSILON || LatticeA.gamma > DEGREE2RADIANT(180.f) ) {
			cerr << "LatticeConstantsA alpha1, beta1, gamma1 must be positive and less than 180deg " << "\n"; return false;
		}
		cout << "LatticeA.alpha " << LatticeA.alpha << "\n";
		cout << "LatticeA.beta " << LatticeA.beta << "\n";
		cout << "LatticeA.gamma " << LatticeA.gamma << "\n";

		if ( LatticeB.a < EPSILON || LatticeB.b < EPSILON || LatticeB.c < EPSILON ) {
			cerr << "LatticeConstantsB a2, b2, and c2 must be positive and non-zero!" << "\n"; return false;
		}
		cout << "a2 " << LatticeB.a << "\n";
		cout << "b2 " << LatticeB.b << "\n";
		cout << "c2 " << LatticeB.c << "\n";
		if ( LatticeB.alpha < EPSILON || LatticeB.alpha > DEGREE2RADIANT(180.f) ||
				LatticeB.beta < EPSILON || LatticeB.beta > DEGREE2RADIANT(180.f) ||
					LatticeB.gamma < EPSILON || LatticeB.gamma > DEGREE2RADIANT(180.f) ) {
			cerr << "LatticeConstantsB alpha2, beta2, gamma2 must be positive and less than 180deg " << "\n"; return false;
		}
		cout << "LatticeB.alpha " << LatticeB.alpha << "\n";
		cout << "LatticeB.beta " << LatticeB.beta << "\n";
		cout << "LatticeB.gamma " << LatticeB.gamma << "\n";
	}
	//else, nothing to do, special treatments for general space groups have already been performed

	cout << "Orientation " << SimMatrixOrientation << "\n";

	if ( NumberOfCluster < 0 ) { //MK::choose 0 to generate no cluster
		cerr << "The number of cluster must be positive and finite or zero!" << "\n"; return false;
	}
	else {
		cout << "Number of cluster " << NumberOfCluster << "\n";
		if ( NumberOfCluster > 0 ) {
			//##MK::at the moment using distribution mu and sigma^2 instead of mean and variance interpreting values in nanometer
			if ( ClusterRadiusMean < 0.f ) {
				cerr << "The mean of the lognormal cluster size distribution must not be negative or non-zero!" << "\n"; return false;
			}
			cout << "ClusterRadiusMean " << ClusterRadiusMean << "\n";
			if ( ClusterRadiusSigmaSqr < EPSILON ) {
				cout << "The sigma(variance) of the lognormal cluster size distribution must not be negative or non-zero!" << "\n"; return false;
			}
			cout << "ClusterRadiusVar " << ClusterRadiusSigmaSqr << "\n";

			cout << "SimPrecipitateOrientation " << SimPrecipitateOrientation << "\n";
		}
	}

	if ( SynthesizingMode == SYNTHESIS_DEVELOPMENT_PX ) {
		if ( CrystalloVoroMeanRadius < EPSILON ) {
			cerr << "CrystalloVoroMeanRadius must be positive !" << "\n"; return false;
		}
		cout << "CrystalloVoroMeanRadius " << CrystalloVoroMeanRadius << "\n";
		cout << "TessellationPointsPerBlock " << TessellationPointsPerBlock << "\n";
	}

	cout << "PRNGType " << PRNGType << "\n";
    cout << "PRNGWarmup " << PRNGWarmup << "\n";
	cout << "PRNGWorldSeed " << PRNGWorldSeed << "\n";

	/*if ( SynthesizingMode == SYNTHESIS_DEVELOPMENT_SX ) {
		cout << "DebugPhaseID " << DebugPhaseID << "\n";
	}*/

	cout << "\n";
	return true;
}


void ConfigSynthetic::reportSettings( vector<pparm> & res)
{
	//res.push_back( pparm( "InputfilePSE", InputfilePSE, "", "" ) );
	res.push_back( pparm( "DatasetOrigin", sizet2str(DatasetOrigin), "", "is this a synthetic dataset or from an experiment" ) );
	res.push_back( pparm( "SynthesizingMode", sizet2str(SynthesizingMode), "", "which type of microstructure to create for the dataset" ) );

	res.push_back( pparm( "NumberOfAtoms", sizet2str(NumberOfAtoms), "", "how many atoms to include in the dataset approximately" ) );
	res.push_back( pparm( "RelBottomRadius", real2str(RelBottomRadius), "1.0", "radius of hemispherical cap carved out from bottom of the dataset" ) );
	res.push_back( pparm( "RelTopRadius", real2str(RelTopRadius), "1.0", "radius of hemispherical cap placed on top of the conical frustrum while building the dataset shape" ) );
	res.push_back( pparm( "RelBottomCapHeight", real2str(RelBottomCapHeight), "1.0", "height of the hemispherical cap that is carved out from the bottom of the dataset" ) );
	res.push_back( pparm( "RelTopCapHeight", real2str(RelTopCapHeight), "1.0", "height of the hemispherical cap that is placed on top of the conical frustrum" ) );

	res.push_back( pparm( "SimDetEfficiency", real2str(SimDetEfficiency), "1.0", "fraction of atoms to keep" ) );
	res.push_back( pparm( "SpatResolutionSigmaX", real2str(SpatResolutionSigmaX), "nm", "standard deviation of Gaussian position smearing kernel along x" ) );
	res.push_back( pparm( "SpatResolutionSigmaY", real2str(SpatResolutionSigmaY), "nm", "standard deviation of Gaussian position smearing kernel along y" ) );
	res.push_back( pparm( "SpatResolutionSigmaZ", real2str(SpatResolutionSigmaZ), "nm", "standard deviation of Gaussian position smearing kernel along z" ) );

	res.push_back( pparm( "DebugPhaseID", uint2str(DebugPhaseID), "", "internal ID which characterizes how crystal structures are created internally" ) );

	if ( DebugPhaseID != SPACEGROUP ) {
		res.push_back( pparm( "LatticeA_a", real2str(LatticeA.a), "nm", "for matrix, lattice parameter a" ) );
		res.push_back( pparm( "LatticeA_b", real2str(LatticeA.b), "nm", "for matrix, lattice parameter b" ) );
		res.push_back( pparm( "LatticeA_c", real2str(LatticeA.c), "nm", "for matrix, lattice parameter c" ) );
		res.push_back( pparm( "LatticeA_alpha", real2str(LatticeA.alpha), "degree", "for matrix, lattice parameter alpha" ) );
		res.push_back( pparm( "LatticeA_beta", real2str(LatticeA.beta), "degree", "for matrix, lattice parameter beta" ) );
		res.push_back( pparm( "LatticeA_gamma", real2str(LatticeA.gamma), "degree", "for matrix, lattice parameter gamma" ) );

		res.push_back( pparm( "LatticeB_a", real2str(LatticeB.a), "nm", "for precipitate, lattice parameter a" ) );
		res.push_back( pparm( "LatticeB_b", real2str(LatticeB.b), "nm", "for precipitate, lattice parameter b" ) );
		res.push_back( pparm( "LatticeB_c", real2str(LatticeB.c), "nm", "for precipitate, lattice parameter c" ) );
		res.push_back( pparm( "LatticeB_alpha", real2str(LatticeB.alpha), "degree", "for precipitate, lattice parameter alpha" ) );
		res.push_back( pparm( "LatticeB_beta", real2str(LatticeB.beta), "degree", "for precipitate, lattice parameter beta" ) );
		res.push_back( pparm( "LatticeB_gamma", real2str(LatticeB.gamma), "degree", "for precipitate, lattice parameter gamma" ) );
	}
	else {
		res.push_back( pparm( "MatrixSpacegroup", sizet2str(SimMatrixStructure.spacegroup), "", "for matrix, space group" ) );
		res.push_back( pparm( "MatrixParameterA", sizet2str(SimMatrixStructure.a), "nm", "for matrix, lattice parameter a" ) );
		res.push_back( pparm( "MatrixParameterB", sizet2str(SimMatrixStructure.b), "nm", "for matrix, lattice parameter b" ) );
		res.push_back( pparm( "MatrixParameterC", sizet2str(SimMatrixStructure.c), "nm", "for matrix, lattice parameter c" ) );
		for( auto it = SimMatrixStructure.motif.begin(); it != SimMatrixStructure.motif.end(); it++ ) {
			size_t nmotif = it - SimMatrixStructure.motif.begin() + 1;
			//string prefix = "MatrixMotif" + to_string(it - SimMatrixStructure.motif.begin() + 1);
			string prefix = "MatrixMotif"; //four digits
			if ( nmotif > 999 )
				prefix += to_string(nmotif);
			else if ( nmotif > 99 )
				prefix += "0" + to_string(nmotif);
			else if ( nmotif > 9 )
				prefix += "00" + to_string(nmotif);
			else
				prefix += "000" + to_string(nmotif);
			res.push_back( pparm( prefix + "ElementZ", sizet2str((int) it->elementZ), "", "for matrix, atomic number") );
			res.push_back( pparm( prefix + "ChargeState", sizet2str((int) it->chargestate), "e", "for matrix, charge of the ion, molecular ion, ionic complex") );
			res.push_back( pparm( prefix + "FracCoorU1", real2str(it->u1), "1.0", "for matrix, fractional lattice coordinate along u1") );
			res.push_back( pparm( prefix + "FracCoorU2", real2str(it->u2), "1.0", "for matrix, fractional lattice coordinate along u2") );
			res.push_back( pparm( prefix + "FracCoorU3", real2str(it->u3), "1.0", "for matrix, fractional lattice coordinate along u3") );
		}
		if ( NumberOfCluster > 0 ) {
			res.push_back( pparm( "PrecipitateSpacegroup", sizet2str(SimPrecipitateStructure.spacegroup), "", "for precipitate, space group" ) );
			res.push_back( pparm( "PrecipitateParameterA", sizet2str(SimPrecipitateStructure.a), "nm", "for precipitate, lattice parameter a" ) );
			res.push_back( pparm( "PrecipitateParameterB", sizet2str(SimPrecipitateStructure.b), "nm", "for precipitate, lattice parameter b" ) );
			res.push_back( pparm( "PrecipitateParameterC", sizet2str(SimPrecipitateStructure.c), "nm", "for precipitate, lattice parameter c" ) );
			for( auto it = SimPrecipitateStructure.motif.begin(); it != SimPrecipitateStructure.motif.end(); it++ ) {
				size_t nmotif = it - SimPrecipitateStructure.motif.begin() + 1;
				string prefix = "PrecipitateMotif"; //four digits
				if ( nmotif > 999 )
					prefix += to_string(nmotif);
				else if ( nmotif > 99 )
					prefix += "0" + to_string(nmotif);
				else if ( nmotif > 9 )
					prefix += "00" + to_string(nmotif);
				else
					prefix += "000" + to_string(nmotif);
				res.push_back( pparm( prefix + "ElementZ", sizet2str((int) it->elementZ), "", "for precipitate, atomic number") );
				res.push_back( pparm( prefix + "ChargeState", sizet2str((int) it->chargestate), "e", "for precipitate, charge of the ion, molecular ion, ionic complex") );
				res.push_back( pparm( prefix + "FracCoorU1", real2str(it->u1), "1.0", "for precipitate, fractional lattice coordinate along u1") );
				res.push_back( pparm( prefix + "FracCoorU2", real2str(it->u2), "1.0", "for precipitate, fractional lattice coordinate along u2") );
				res.push_back( pparm( prefix + "FracCoorU3", real2str(it->u3), "1.0", "for precipitate, fractional lattice coordinate along u3") );
			}
		}
	}

	res.push_back( pparm( "SimMatrixOrientation", SimMatrixOrientation, "degree", "for matrix, Bunge-Euler parameterization phi1, Phi, phi2" ) );
	res.push_back( pparm( "SimPrecipitateOrientation", SimPrecipitateOrientation, "degree", "for precipitate, Bunge-Euler parameterization phi1, Phi, phi2" ) );

	res.push_back( pparm( "NumberOfCluster", sizet2str(NumberOfCluster), "1.0", "for precipitates, how many to place in the bounding-box of to dataset" ) );
	res.push_back( pparm( "ClusterRadiusMean", real2str(ClusterRadiusMean), "nm", "for precipitates, population mean radius" ) );
	res.push_back( pparm( "ClusterRadiusSigmaSqr", real2str(ClusterRadiusSigmaSqr), "nm", "for precipitates, population spread" ) );

	res.push_back( pparm( "CrystalloVoroMeanRadius", real2str(CrystalloVoroMeanRadius), "nm", "for matrix, population mean of equivalent spherical radius to aim for in polycrystalline matrix") );

	res.push_back( pparm( "PRNGType", PRNGType, "", "which pseudo-random number generator" ) );
	res.push_back( pparm( "PRNGWarmup", sizet2str(PRNGWarmup), "1", "how many random numbers to discard in the warm-up stage" ) );
	res.push_back( pparm( "PRNGWorldSeed", sizet2str(PRNGWorldSeed), "", "which seed to achieve deterministic execution" ) );
	res.push_back( pparm( "SimAnnealingIterMax", sizet2str(SimAnnealingIterMax), "1", "maximum number of repetitions in loop to e.g. place objects via simulated annealing" ) );
	res.push_back( pparm( "TessellationPointsPerBlock", sizet2str(TessellationPointsPerBlock), "1", "how many points/ions/atoms to aim for in one bin of the regular spatial binning for Voro++" ) );
}

