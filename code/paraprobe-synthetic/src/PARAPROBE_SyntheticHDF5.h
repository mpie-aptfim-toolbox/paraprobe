//##MK::CODE

#ifndef __PARAPROBE_SYNTHETICHDF_H__
#define __PARAPROBE_SYNTHETICHDF_H__

//shared headers
#include "../../paraprobe-utils/src/PARAPROBE_APTH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_UtilsMetadataDefsH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_SyntheticMetadataDefsH5.h"
/*
#include "../../paraprobe-utils/src/metadata/PARAPROBE_RangerMetadataDefsH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_SurfacerMetadataDefsH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_SpatstatMetadataDefsH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_FourierMetadataDefsH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_AraulloMetadataDefsH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_IndexerMetadataDefsH5.h"
*/

#include "../../paraprobe-utils/src/PARAPROBE_XDMF.h"

//tool-specific headers
//#include "CONFIG_Synthetic.h"
#include "PARAPROBE_SyntheticCiteMe.h"

/*
#include "METADATA_SyntheticDefsHDF5.h"
*/

class synthetic_h5 : public h5Hdl
{
	//tool-specific sub-class of a HDF5 inheriting all methods of the base class h5Hdl
	//coordinating instance handling all (sequential) writing to HDF5 file wrapping HDF5 low-level C library calls

public:
	synthetic_h5();
	~synthetic_h5();

	int create_synthetic_apth5( const string h5fn );

//private:
};

#endif
