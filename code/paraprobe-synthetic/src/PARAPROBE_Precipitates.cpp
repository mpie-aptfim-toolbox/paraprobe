//##MK::GPLV3

#include "PARAPROBE_Precipitates.h"


bool SortRadiiDesc( apt_real & first, apt_real & second )
{
	return first > second;
}


void spherical_particle::get_atoms( unitcell_geometry const & ez, t3x3 const & ori, vector<pos> & out )
{
	//##MK::specifically rotated Al3Sc ideal unit cell centered at sphere center
	//https://materials.springer.com/isp/crystallographic/docs/sd_1922024

	speci Al27 = speci( 0.00, 26.8, 1 );
	speci Sc45 = speci( 0.00, 44.955, 4 );
	apt_real Al3ScLatticeConstant = ez.a; //nm

	apt_real OriginShift = 0.5 * Al3ScLatticeConstant; //MK::base unit cell center is defined at sphere center

	p3d c = this->center;
	apt_real R = this->radius;
	apt_real SQRR = SQR(R);

//cout << "center/c.x/c.y/c.z/R\t\t" << c.x << ";" << c.y << ";" << c.z << "\t\t" << R << "\n";

	//aabb3d window = aabb3d( c.x-R, c.x+R, c.y-R, c.y+R, c.z-R, c.z+R );
	aabb3d window = aabb3d( -1.f*R, +1.f*R, -1.f*R, +1.f*R, -1.f*R, +1.f*R );

	unitcellparms Al3ScLattice = unitcellparms( ez.a, ez.b, ez.c, ez.alpha, ez.beta, ez.gamma );
	unitcellaggr al3sclatt = unitcellaggr( Al3ScLattice, window, AL3SC );

	//cout << "latt/a\t\t\t" << al3sclatt.a << endl;
	//cout << "latt/cx/umi/umx\t\t" << c.x << "\t\t" << al3sclatt.umin << "\t\t" << al3sclatt.umax << endl;
	//cout << "latt/cy/vmi/vmx\t\t" << c.y << "\t\t" << al3sclatt.vmin << "\t\t" << al3sclatt.vmax << endl;
	//cout << "latt/cz/wmi/wmx\t\t" << c.z << "\t\t" << al3sclatt.wmin << "\t\t" << al3sclatt.wmax << endl;

	for(size_t b = 0; b < al3sclatt.base.size(); ++b) {
		//unsigned int mark = (b == 0) ? 4 : 1; //##MK::first defined is Scandium rest Al, see definition in unitcellaggr constructor
		apt_real mass2charge = (b == 0) ? Sc45.m2q : Al27.m2q;

		for(int w = al3sclatt.wmin; w <= al3sclatt.wmax; ++w) {
			for(int v = al3sclatt.vmin; v <= al3sclatt.vmax; ++v) {
				for(int u = al3sclatt.umin; u <= al3sclatt.umax; ++u) {

					p3d ap_ref = al3sclatt.get_atom_pos(b, u, v, w); //##MK::applying temporary shift of origin

					p3d ap_ref_shft = p3d( ap_ref.x-OriginShift, ap_ref.y - OriginShift, ap_ref.z - OriginShift );

					p3d ap_rot = ap_ref_shft.active_rotation_relocate( ori ); //rotates about the originn of the lattice, ##MK::which is here the center of the lattice

					if ( (SQR(ap_rot.x)+SQR(ap_rot.y)+SQR(ap_rot.z)) <= SQRR ) {
						out.push_back( pos( ap_rot.x + c.x, ap_rot.y + c.y, ap_rot.z + c.z, mass2charge) );
					} //atom is inside defined sphere
				}
			}
		}
	} //next base
}


void spherical_particle::get_atoms( crystalstructure const & ez, t3x3 const & ori, vector<pos> & out )
{
	p3d c = this->center; //MK::lays somewhere in the tip, not necessarily at 0.0, 0.0, 0.0 !
	apt_real R = this->radius;
	apt_real SQRR = SQR(R);

cout << "center/c.x/c.y/c.z/R\t\t" << c.x << ";" << c.y << ";" << c.z << "\t\t" << R << "\n";

	aabb3d ballbox = aabb3d(	c.x-R, c.x+R,
								c.y-R, c.y+R,
								c.z-R, c.z+R   );
cout << "Ballbox " << ballbox << "\n";
	ballbox.scale();
	p3d ballboxcenter = ballbox.center(); //MK::essentially c, so a position somewhere in the tip, not necessarily at 0.0, 0.0, 0.0 !

	cout << "xmi " << ConfigSynthetic::SimPrecipitateStructure.box_xmi << "\n";
	cout << "xmx " << ConfigSynthetic::SimPrecipitateStructure.box_xmx << "\n";
	cout << "ymi " << ConfigSynthetic::SimPrecipitateStructure.box_ymi << "\n";
	cout << "ymx " << ConfigSynthetic::SimPrecipitateStructure.box_ymx << "\n";
	cout << "zmi " << ConfigSynthetic::SimPrecipitateStructure.box_zmi << "\n";
	cout << "zmx " << ConfigSynthetic::SimPrecipitateStructure.box_zmx << "\n";
	cout << "len " << ConfigSynthetic::SimPrecipitateStructure.box_len << "\n";

	//given a parallelepiped unit cell how many multiples of the unit cell to go in every direction do we need to fill the sphere
	apt_real imin = min(ballbox.xmi, min(ballbox.ymi, ballbox.zmi));
	apt_real imax = max(ballbox.xmx, max(ballbox.ymx, ballbox.zmx));
	int umin = static_cast<int>(floor(( imin - ballboxcenter.x) / ConfigSynthetic::SimPrecipitateStructure.box_len )) - 1;
	int umax = static_cast<int>(ceil(( imax - ballboxcenter.x) / ConfigSynthetic::SimPrecipitateStructure.box_len )) + 1;
	int vmin = static_cast<int>(floor(( imin - ballboxcenter.y) / ConfigSynthetic::SimPrecipitateStructure.box_len )) - 1;
	int vmax = static_cast<int>(ceil(( imax - ballboxcenter.y) / ConfigSynthetic::SimPrecipitateStructure.box_len )) + 1;
	int wmin = static_cast<int>(floor(( imin - ballboxcenter.z) / ConfigSynthetic::SimPrecipitateStructure.box_len )) - 1;
	int wmax = static_cast<int>(ceil(( imax - ballboxcenter.z) / ConfigSynthetic::SimPrecipitateStructure.box_len )) + 1;

	cout << "umin " << umin << "\n";
	cout << "umax " << umax << "\n";
	cout << "vmin " << vmin << "\n";
	cout << "vmax " << vmax << "\n";
	cout << "wmin " << wmin << "\n";
	cout << "wmax " << wmax << "\n";

	for(int w = wmin; w <= wmax; ++w) {
		for(int v = vmin; v <= vmax; ++v) {
			for(int u = umin; u <= umax; ++u) {
				for( size_t b = 0; b < ConfigSynthetic::SimPrecipitateStructure.motif.size(); b++ ) {

					p3d ap_ref = ConfigSynthetic::SimPrecipitateStructure.get_atom_pos(b, u, v, w); //about origin 0,0,0
					unsigned char Z = ConfigSynthetic::SimPrecipitateStructure.get_element_Z( b );
					unsigned char Charge = ConfigSynthetic::SimPrecipitateStructure.get_chargestate( b );

					//p3d ap_ref_shft = p3d( ap_ref.x, ap_ref.y, ap_ref.z );

					//p3d ap_rot = ap_ref_shft.active_rotation_relocate( ori ); //rotates about the originn of the lattice, ##MK::which is here the center of the lattice
					p3d ap_rot = ap_ref.active_rotation_relocate( ori );

					if ( (SQR(ap_rot.x)+SQR(ap_rot.y)+SQR(ap_rot.z)) <= SQRR ) { //atom is inside defined sphere

						map<unsigned char, elementinfo>::iterator thisone = ConfigShared::MyElementsZ.find(Z);
						if ( thisone != ConfigShared::MyElementsZ.end() ) {
							apt_real mass2charge = thisone->second.mass / static_cast<apt_real>(static_cast<int>(Charge));

							//buffer thread-local ion in tip first
							//size_t hash = ( u - umin + 1) + ( v - vmin + 1) * 1024 + ( w - wmax + 1) * 1024*1024) + ((bt - ConfigSynthetic::SimMatrixStructure.motif.begin()) * 4000000000);
							out.push_back( pos( ap_rot.x + ballboxcenter.x, ap_rot.y + ballboxcenter.y, ap_rot.z + ballboxcenter.z, mass2charge) );
							//plus ballboxcenter to go from somewhere around 0.0, 0.0, 0.0 to somewhere around c where the precipitate is
						}
						else {
							cerr << "Did not found elementinfo to Z " << Z << "\n";
							continue;
						}
					}
				}
			}
		}
	}
}


precipitateModel::precipitateModel()
{
	urng.seed( ConfigSynthetic::PRNGWorldSeed );
	urng.discard( ConfigSynthetic::PRNGWarmup );
}


precipitateModel::~precipitateModel()
{
}


void precipitateModel::initialize( geomodel const & geom, const size_t N, const apt_real rmean, const apt_real rvar )
{
	//##MK::rmean and rvar are in nanometer however we require the distribution parameter mu and sigma
cout << "WARNING currently all precipitates are generated of the same radius!" << "\n";
	//##MK::implement lognormal_distribution<apt_real> lognrm( rm, rvar ); //##MK::is in nanometer!
	vector<apt_real> radii;
	radii.reserve( N );
	for( size_t i = 0; i < N; i++) {
		//fixed size radius value
		radii.push_back( rmean );

		//distributed radius value
		//radii.push_back( lognrm(urng) );
	}

	//sorting by descending radius improves packing process speed
	sort( radii.begin(), radii.end(), SortRadiiDesc );

//##MK::DEBUG#############for( size_t i = 0; i < radii.size(); ++i)  { cout << radii.at(i) << endl; } cout << endl << endl;

	uniform_real_distribution<apt_real> unifrnd(0.f,1.f);
	size_t itermax = ConfigSynthetic::SimAnnealingIterMax; //at most so many attempts to place a particle
	size_t iter = 0;
	size_t pid = 0;

	while( particles.size() < N && iter < itermax ) { //attempt placement, itermax prevents endless loop
		//pick a place at random
		p3d rnd = p3d(
				geom.mybox.xmi + (unifrnd(urng) * geom.mybox.xsz),
				geom.mybox.ymi + (unifrnd(urng) * geom.mybox.ysz),
				geom.mybox.zmi + (unifrnd(urng) * geom.mybox.zsz)  );

//cout << "particlesSize/N/iter/itermax\t\t" << particles.size() << ";" << N << ";" << iter << ";" << itermax << "\t\t" << rnd.x << ";" << rnd.y << ";" << rnd.z << endl;

		//MK::we assume here in this simplistic second-phase insertion model that
		//precipitates are perfect spheres, they may protrude out of mybox but they may not numerically overlap with any other precipitate
		//##MK::such model is useful for principal well-known ground truth verification of algorithms
		//##MK::they are however a potentially too strong simplification because
		//1.) in reality precipitates are not necessarily spheres
		//2.) the geometry of a precipitate typically spatially correlated with the size of its neighboring precipitates and the local chemo-mechanical field
		//##MK::in the future such model may be improved by loading either phase-field simulation results or MD results

		bool contact = false;
		//MK::test until intrusion or touching particle found if any
		for( auto pt = particles.begin(); pt != particles.end(); pt++ ) {
			//apt_real distnow = sqrt( SQR(rnd.x - pt->center.x) + SQR(rnd.y - pt->center.y) + SQR(rnd.z - pt->center.z) ); //##MK::sqrt can be avoided here work with SQR values instead
			//apt_real distcrt = radii.at(pid) + pt->radius;

			apt_real distnow = SQR(rnd.x - pt->center.x) + SQR(rnd.y - pt->center.y) + SQR(rnd.z - pt->center.z);
			apt_real distcrt = SQR(radii.at(pid) + pt->radius);

//cout << "\t\t\t" << distnow << "\t\t" << distcrt << endl;
			if ( distnow <= distcrt ) { //will become the more likely case the more particles have already been placed
				contact = true;
				break; //MK::we can break if we found at least one overlap
			}
			//we need to test overlap against potentially all
		}

		//decision
		if ( contact == true ) { //pick a new place, will be come the ore likely case the more particles have already been placed
			iter++;
		}
		else { //place the particle
			particles.push_back( spherical_particle( rnd, radii.at(pid) ));
//cout << "rnd/pid/iter\t\t" << rnd.x << ";" << rnd.y << ";" << rnd.z << "\t\t" << pid << "\t\t" << iter << "\n";
			pid++;
			iter = 0; //reset counter because next particle placed will again be allowed only maximum
		}
	} //fill 3d box geom.mybox with N randomly placed particles of mean size rm and variance rvar

cout << "A total of " << particles.size() << " were placed" << "\n";

	//initialize their orientations to identity orientation
	orientation = vector<squat>( particles.size(), squat() );
}

