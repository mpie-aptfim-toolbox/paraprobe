//##MK::GPLV3

#include "PARAPROBE_GrainModel.h"


ostream& operator<<(ostream& in, grain const & val)
{
	in << "GrainID " << val.id << " GrainOrientation " << val.ori.q0 << ";" << val.ori.q1 << ";" << val.ori.q2 << ";" << val.ori.q3 << "\n";
	in << "GrainCenter " << val.cellcenter.x << ";" << val.cellcenter.y << ";" << val.cellcenter.z << ";" << " Cellvolume " << val.cellvolume << " NAtoms " << val.natoms << "\n";
	return in;
}


grainAggregate::grainAggregate()
{
}

grainAggregate::~grainAggregate()
{
}

