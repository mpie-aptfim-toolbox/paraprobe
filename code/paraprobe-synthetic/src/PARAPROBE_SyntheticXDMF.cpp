#include "PARAPROBE_SyntheticXDMF.h"


synthetic_xdmf::synthetic_xdmf()
{
	
}
	
synthetic_xdmf::~synthetic_xdmf()
{
	
}

	
int synthetic_xdmf::create_volrecon_file( const string xmlfn, const size_t nions, const string h5ref )
{
	xdmfout.open( xmlfn.c_str() );
	if ( xdmfout.is_open() == true ) {
		xdmfout << XDMF_HEADER_LINE1 << "\n";
		xdmfout << XDMF_HEADER_LINE2 << "\n";
		xdmfout << XDMF_HEADER_LINE3 << "\n";

		xdmfout << "  <Domain>" << "\n";
		xdmfout << "    <Grid Name=\"volrecon\" GridType=\"Uniform\">" << "\n";
		xdmfout << "      <Topology TopologyType=\"Mixed\" NumberOfElements=\"" << nions << "\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << 3*nions << "\" NumberType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5ref << ":" << PARAPROBE_SYNTH_VOLRECON_RES_TOPO << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Topology>" << "\n";
		xdmfout << "      <Geometry GeometryType=\"XYZ\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nions << " 3\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5ref << ":" << PARAPROBE_SYNTH_VOLRECON_RES_XYZ << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Geometry>" << "\n";

		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"Mass2Charge\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nions << " 1\" DataType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5ref << ":" << PARAPROBE_SYNTH_VOLRECON_RES_MQ << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";

		/*
		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"Iontype\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nions << " 1\" DataType=\"UInt\" Precision=\"1\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5ref << ":" << PARAPROBE_RANGER_RES_TYPID << "\n";
		xdmfout << "       </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";
		*/

		/*if ( Settings::IOIonTipSurfDists == true ) {
			xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"SqrdDist\">" << "\n";
			xdmfout << "	    <DataItem Dimensions=\"" << nions << " 1\" DataType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
			xdmfout << "          " << h5ref << ":" << PARAPROBE_VOLRECON_SURFDISTSQR << "\n";
			xdmfout << "        </DataItem>" << "\n";
			xdmfout << "      </Attribute>" << "\n";
		}*/
 		xdmfout << "    </Grid>" << "\n";
		xdmfout << "  </Domain>" << "\n";
		xdmfout << "</Xdmf>" << "\n";

		xdmfout.flush();
		xdmfout.close();

		return WRAPPED_XDMF_SUCCESS;
	}
	else {
		return WRAPPED_XDMF_IOFAILED;
	}
}


int synthetic_xdmf::create_precipitate_file( const string xmlfn, const size_t nprec, const string h5ref )
{
	xdmfout.open( xmlfn.c_str() );
	if ( xdmfout.is_open() == true ) {
		xdmfout << XDMF_HEADER_LINE1 << "\n";
		xdmfout << XDMF_HEADER_LINE2 << "\n";
		xdmfout << XDMF_HEADER_LINE3 << "\n";

		xdmfout << "  <Domain>" << "\n";
		xdmfout << "    <Grid Name=\"particles\" GridType=\"Uniform\">" << "\n";
		xdmfout << "      <Topology TopologyType=\"Mixed\" NumberOfElements=\"" << nprec << "\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << 3*nprec << "\" NumberType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5ref << ":" << PARAPROBE_SYNTH_VOLRECON_META_PREC_TOPO << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Topology>" << "\n";
		xdmfout << "      <Geometry GeometryType=\"XYZ\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nprec << " 3\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5ref << ":" << PARAPROBE_SYNTH_VOLRECON_META_PREC_XYZ << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Geometry>" << "\n";

		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"Radius\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nprec << " 1\" DataType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5ref << ":" << PARAPROBE_SYNTH_VOLRECON_META_PREC_R << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";

		//##MK::quaternions
		/*
		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"Radius\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nprec << " 1\" DataType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5ref << ":" << PARAPROBE_SYNTH_VOLRECON_META_PREC_R << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";
		*/

 		xdmfout << "    </Grid>" << "\n";
		xdmfout << "  </Domain>" << "\n";
		xdmfout << "</Xdmf>" << "\n";

		xdmfout.flush();
		xdmfout.close();

		return WRAPPED_XDMF_SUCCESS;
	}
	else {
		return WRAPPED_XDMF_IOFAILED;
	}
}


int synthetic_xdmf::create_voronoicrystal_file( const string xmlfn, const size_t nelem, const size_t ntopo,
		const size_t ngeom, const size_t nhalo, const string h5ref )
{
	xdmfout.open( xmlfn.c_str() );
	if ( xdmfout.is_open() == true ) {
		xdmfout << XDMF_HEADER_LINE1 << "\n";
		xdmfout << XDMF_HEADER_LINE2 << "\n";
		xdmfout << XDMF_HEADER_LINE3 << "\n";

		xdmfout << "  <Domain>" << "\n";
		xdmfout << "    <Grid Name=\"vorongrains\" GridType=\"Uniform\">" << "\n";
		xdmfout << "      <Topology TopologyType=\"Mixed\" NumberOfElements=\"" << nelem << "\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << ntopo << "\" NumberType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5ref << ":" << PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Topology>" << "\n";
		xdmfout << "      <Geometry GeometryType=\"XYZ\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << ngeom << " 3\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5ref << ":" << PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Geometry>" << "\n";

		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Cell\" Name=\"CellGrainID\">" << "\n"; //cell-based attributes, not Node based!
		xdmfout << "        <DataItem Dimensions=\"" << nhalo << " 1\" DataType=\"Int\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5ref << ":" << PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";

		xdmfout << "    </Grid>" << "\n";
		xdmfout << "  </Domain>" << "\n";
		xdmfout << "</Xdmf>" << "\n";

		xdmfout.flush();
		xdmfout.close();

		return WRAPPED_XDMF_SUCCESS;
	}
	else {
		return WRAPPED_XDMF_IOFAILED;
	}
}


