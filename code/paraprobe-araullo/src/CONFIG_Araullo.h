//##MK::CODE

#ifndef __PARAPROBE_CONFIG_ARAULLO_H__
#define __PARAPROBE_CONFIG_ARAULLO_H__

//#include "../../paraprobe-utils/src/CONFIG_Shared.h"
//#include "../../paraprobe-utils/src/PARAPROBE_VolumeSampler.h"
//#include "../../paraprobe-utils/src/PARAPROBE_CPUGPUWorkloadStructs.h"
#include "../../paraprobe-utils/src/PARAPROBE_FFT.h"

enum ANALYSIS_VOLUME {
	ENTIRE_DATASET,
	CUBOIDAL_REGION
};


enum VOLUME_SAMPLING_METHOD {
	CUBOIDAL_GRID,
	SINGLE_POINT_USER,
	SINGLE_POINT_CENTER,
	RANDOM_POINTS_FIXED,
	USERDEFINED_POINTS
};


enum WINDOWING_METHOD {
	RECTANGULAR,
	KAISER
};


class ConfigAraullo
{
public:
	
	//static string InputfilePSE;
	static string InputfileReconstruction;
	static string InputfileHullAndDistances;
	//static string Outputfile;

	static ANALYSIS_VOLUME AnalysisVolume;
	static aabb3d AnalysisVolumeAABB;
	static VOLUME_SAMPLING_METHOD ROISamplingMethod;
	static string InputfileUserROIPositions;
	static WINDOWING_METHOD SDMWindowingMethod;
	
	static apt_real ROIRadiusMax;
	static apt_real SamplingGridBinWidthX;
	static apt_real SamplingGridBinWidthY;
	static apt_real SamplingGridBinWidthZ;
	static apt_real RefineGridOldBinWidthX; //defining OldBinWidthi
	static apt_real RefineGridOldBinWidthY;
	static apt_real RefineGridOldBinWidthZ;
	static unsigned int RefineGridNewSubdivisionX; //into how many new voxels to split OldBinWidthi/2?
	static unsigned int RefineGridNewSubdivisionY;
	static unsigned int RefineGridNewSubdivisionZ;
	static string SamplingPosition;
	static unsigned int FixedNumberOfROIs;
	static unsigned int SDMBinExponent;
	static apt_real KaiserAlpha;
	static apt_real DBScanThreshold;
	static apt_real DBScanEpsilon;
	static unsigned int DBScanMinPts;

	static bool SamplingGridRemoveBndPoints;
	static bool SDMNormalize;
	
	static bool IOSpecificPeaks;
	static bool IODBScanOnSpecificPeaks;
	static bool IOKeyQuants;
	static bool IOStrongPeaks;
	static bool IOHistoCnts;
	static bool IOHistoFFTMagn;
	static string KeyQuantsParseMe;
	static vector<apt_real> KeyQuantiles;
	
	//static string LatticeDistancesToProbe;
	//static string IontypesToProbe;
	
	static string InputfileElevAzimGrid;
	static unsigned long MaxMemoryPerNode;
	static unsigned int ProcessesPerMPITeam;
	static unsigned int GPUsPerNode;
	static unsigned int GPUPerProcess;
	static unsigned int GPUWorkloadFactor;
	static apt_real ROI2ROIMinDistance;
	
	//internals
	//static vector<pair<unsigned int,unsigned int>> ProbeThisBinThisType;
	//static unsigned int NumberOfFFTMagnImages;
	static unsigned int NumberOfSO2Directions;
	

	static bool readXML( string filename = "" );
	static bool checkUserInput();
	static void reportSettings( vector<pparm> & res );
};

#endif
