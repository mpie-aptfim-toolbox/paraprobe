//##MK::GPLV3

#ifndef __PARAPROBE_ARAULLO_CITEME_H__
#define __PARAPROBE_ARAULLO_CITEME_H__

#include "CONFIG_Araullo.h"


class CiteMeAraullo
{
public:
	
	static vector<bibtex> Citations;
	
	static size_t cite();
};

#endif

