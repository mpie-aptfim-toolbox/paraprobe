//##MK::GPLV3

#ifndef __PARAPROBE_ARAULLO_HDF_H__
#define __PARAPROBE_ARAULLO_HDF_H__

//shared headers
//#include "../../paraprobe-utils/src/PARAPROBE_APTH5.h"
//#include "../../paraprobe-utils/src/PARAPROBE_XDMF.h"
//##already included through utils/src/PARAPROBE_VolumeSampler.h

#include "../../paraprobe-utils/src/PARAPROBE_APTH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_UtilsMetadataDefsH5.h"
#include "../../paraprobe-utils/src/metadata/PARAPROBE_AraulloMetadataDefsH5.h"

#include "../../paraprobe-utils/src/PARAPROBE_PeriodicTable.h"

//tool-specific headers
//#include "CONFIG_Araullo.h"
#include "PARAPROBE_AraulloCiteMe.h"
/*
#include "METADATA_AraulloDefsHDF5.h"
*/


class araullo_h5 : public h5Hdl
{
	//tool-specific sub-class of a HDF5 inheriting all methods of the base class h5Hdl but adds tool specific read/write functions
	//coordinating instance handling all (sequential) writing to HDF5 file wrapping HDF5 low-level C library calls

public:
	araullo_h5();
	~araullo_h5();


	int create_araullo_apth5( const string h5fn );

//private:
};


#endif
