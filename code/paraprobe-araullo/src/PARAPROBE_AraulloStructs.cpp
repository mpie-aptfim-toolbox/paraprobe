//##MK::GPLV3

#include "PARAPROBE_AraulloStructs.h"

ostream& operator << (ostream& in, elaz const & val) {
	in << val.elevation << ";" << val.azimuth << "\n";
	return in;
}


ostream& operator << (ostream& in, iontype_ival const & val) {
	in << "[ " << val.incl_left << ", " << val.excl_right << ") m = " << val.m << "\n";
	return in;
}


ostream& operator << (ostream& in, binning_info const & val) {
	in << "ROIRadius " << val.R << "\n";
	in << "Histogram dR " << val.dR << "\n";
	in << "Histogram mult " << val.binner << "\n";
	in << "Histogram/2 mult " << val.binner2 << "\n";
	in << "Histogram RdR " << val.RdR << "\n";
	in << "Histogram pad " << val.Rpadding << "\n";
	in << "Histogram nbins " << val.NumberOfBins << "\n";
	in << "FFT nbins half " << val.NumberOfBinsHalf << "\n";
	return in;
}


ostream& operator << (ostream& in, specpeakcluster const & val) {
	in << "SUMIntensity " << val.SUMIntensity << " MAXIntensity " << val.MAXIntensity << " PksCnts " << val.PksCnts << "\n";
	in << "Max peak location x, y, z " << val.xmax << ";" << val.ymax << ";" << val.zmax << " (not reprojected on unit sphere)" << "\n";
	in << "Naive equally-weighted mean peak location x, y, z " << val.xmean << ";" << val.ymean << ";" << val.zmean << " (not reprojected on unit sphere)" << "\n";
	return in;
}


ostream& operator << (ostream& in, threepeaks const & val) {
	in << val.max1.first << ";" << val.max1.second << "\n";
	in << val.max2.first << ";" << val.max2.second << "\n";
	in << val.max3.first << ";" << val.max3.second << "\n";
	return in;
}
