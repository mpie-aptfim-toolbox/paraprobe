//##MK::CODESPLIT

#include "PARAPROBE_AraulloHdl.h"


araulloHdl::araulloHdl()
{
	myrank = MASTER;
	nranks = 1;
	maximum_iontype_uc = 0x00;
}


araulloHdl::~araulloHdl()
{
	for( size_t i = 0; i < workers.size(); i++ ) {
		if ( workers.at(i) != NULL ) {
			delete workers.at(i);
			workers.at(i) = NULL;
		}
	}
}


bool araulloHdl::read_periodictable()
{
	//all read PeriodicTable
	/*
	if ( rng.nuclides.load_isotope_library( ConfigAraullo::InputfilePSE ) == false ) {
		cerr << "Rank " << get_myrank() << " loading of PeriodicTableOfElements failed!" << "\n"; return false;
	}
	*/
	if ( rng.nuclides.load_isotope_library() == false ) {
		cerr << "Rank " << get_myrank() << " loading of Config_SHARED PeriodicTableOfElements failed!" << "\n"; return false;
	}
	cout << "Rank " << get_myrank() << " loaded rng.nuclides.isotopes.size() " << rng.nuclides.isotopes.size() << " isotopes" << "\n";
	return true;
}


bool araulloHdl::read_iontype_combinations( string fn )
{
	//all read IontypeCombinations
	if ( itsk.load_iontype_combinations( fn ) == false ) {
		cerr << "Rank " << get_myrank() << " loading of IontypeCombinations failed!" << "\n"; return false;
	}
	cout << "Rank " << get_myrank() << " itsk.icombis.size() " << itsk.icombis.size() << " icombis " << "\n";
	return true;
}


bool araulloHdl::read_reconxyz_from_apth5()
{
	double tic = MPI_Wtime();

	//load xyz positions
	if ( inputReconH5Hdl.read_xyz_from_apth5( ConfigAraullo::InputfileReconstruction, xyz ) == true ) {
		cout << ConfigAraullo::InputfileReconstruction << " read positions successfully" << "\n";
	}
	else {
		cerr << ConfigAraullo::InputfileReconstruction << " read positions failed!" << "\n"; return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "ReadReconXYZFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

    return true;
}


bool araulloHdl::read_ranging_from_apth5()
{
	double tic = MPI_Wtime();

	//load ranging information
	if ( inputReconH5Hdl.read_iontypes_from_apth5( ConfigAraullo::InputfileReconstruction, rng.iontypes ) == true ) {
		cout << ConfigAraullo::InputfileReconstruction << " read ranging information successfully" << "\n";

		//create iontype_dictionary
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
			size_t id = static_cast<size_t>(it->id);
			size_t hashval = rng.hash_molecular_ion( it->strct.Z1, it->strct.Z2, it->strct.Z3 );
	cout << id << "\t\t" << hashval << "\t\t" << (int) it->strct.Z1 << ";" << (int) it->strct.Z2 << ";" << (int) it->strct.Z3 << "\n";
			if ( id != 0 && hashval != 0 ) { //skip adding existent UNKNOWN_IONTYPE
				auto jt = rng.iontypes_dict.find( hashval );
				if ( jt == rng.iontypes_dict.end() ) { //really a new iontype for which not yet mqival were defined
	//cout << "--->Adding new type " << id << "\t\t" << hashval << "\n";
					//add iontype in dictionary
					rng.iontypes_dict.insert( pair<size_t,size_t>( hashval, id ) );
				}
				else {
	cerr << "--->Detected an iontype where the triplet Z1, Z2, Z3 is a duplicate, which should not happen as iontypes have to be mutually exclusive!" << "\n";
					return false;
	//cout << "--->Adding to an existent type " << id << "\t\t" << hashval << "\t\t" << jt->second << "\n";
					//match to an already existent type
					//rng.iontypes_dict.insert( pair<size_t,size_t>( hashval, id ) ); //jt->second ) );
					//##MK::this fix makes it de facto a charge state specific 8;0,0;0;1 and 8;0;0;0;0;0;0;1 and 8;0;0;0;0;0;0;2 map to different types
					//return false; //##MK::switched of for FAU APT School
				}
			}
			else {
	cout << "--->Me skipping the UNKNOWN_IONTYPE" << "\n";
			}
		}

		//##MK::BEGIN DEBUG, make sure we use have correct ranging data
		cout << "--->Debugging ranging data..." << "\n";
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
			size_t id = static_cast<size_t>(it->id);
			size_t hashval = rng.hash_molecular_ion( it->strct.Z1, it->strct.Z2, it->strct.Z3 );
			cout << id << "\t\t" << hashval << "\t\t" << (int) it->strct.Z1 << ";" << (int) it->strct.Z2 << ";" << (int) it->strct.Z3 << "\n";
			for( auto jt = it->rng.begin(); jt != it->rng.end(); jt++ ) {
				cout << "\t\t\t" << jt->lo << "\t\t" << jt->hi << "\n";
			}
		}
		cout << "--->Debugging ion dictionary..."  << "\n";
		for( auto kt = rng.iontypes_dict.begin(); kt != rng.iontypes_dict.end(); kt++ ) {
			cout << kt->first << "\t\t" << kt->second << "\n";
		}
		//##MK::END DEBUG
	}
	else {
		cerr << ConfigAraullo::InputfileReconstruction << " read ranging information failed!" << "\n"; return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "ReadRangingFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool araulloHdl::read_itypes_from_apth5()
{
	double tic = MPI_Wtime();

	//load iontype per xyz position
	if ( inputReconH5Hdl.read_ityp_from_apth5( ConfigAraullo::InputfileReconstruction, ityp_org ) == true ) {
		cout << ConfigAraullo::InputfileReconstruction << " read itypes successfully" << "\n";
	}
	else {
		cerr << ConfigAraullo::InputfileReconstruction << " read itypes failed!" << "\n"; return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "ReadIontypesFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

    return true;
}


bool araulloHdl::read_dist2hull_from_apth5()
{
cerr << "araulloHdl::read_dist2hull_from_apth5 currently not implemented!" << "\n";
	//see paraprobe-spatstat
	return true;
}


/*
bool araulloHdl::read_reconstruction_from_apth5()
{
	double tic = MPI_Wtime();

	if ( inputReconH5Hdl.read_reconstruction_from_apth5( ConfigAraullo::InputfileReconstruction, reconstruction ) == true ) {
		cout << ConfigAraullo::InputfileReconstruction << " read successfully" << "\n";
	}
	else {
		cerr << ConfigAraullo::InputfileReconstruction << " read failed!" << "\n";
		return false;
	}
	
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "ReadReconstructionFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}
*/


bool araulloHdl::read_directions_from_apth5()
{
	double tic = MPI_Wtime();

	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	if ( ConfigAraullo::InputfileElevAzimGrid.substr(ConfigAraullo::InputfileElevAzimGrid.length()-3) != ".h5" ) {
		cerr << "The InputfileElevAzimGrid " << ConfigAraullo::InputfileElevAzimGrid << " has not the proper apth5 file extension!" << "\n";
		return false;
	}

	//probe existence of xyz coordinates array in the input file and get dimensions
	inputElevAzimH5Hdl.h5resultsfn = ConfigAraullo::InputfileElevAzimGrid;
	if ( inputElevAzimH5Hdl.query_contiguous_matrix_dims( PARAPROBE_ARAULLO_META_DIR_ELAZ, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the PARAPROBE_ARAULLO_META_DIR_ELAZ in the InputfileElevAzimGrid!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_ARAULLO_META_DIR_ELAZ is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}
	
	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX ) {
		cerr << "PARAPROBE_ARAULLO_META_DIR_ELAZ is either empty and/or has unexpected size!" << "\n";
		return false;
	}
	
	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_ARAULLO_META_DIR_ELAZ, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = inputElevAzimH5Hdl.read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_ARAULLO_META_DIR_ELAZ on InputfileElevAzimGrid failed!" << "\n";
		return false;
	}
cout << "PARAPROBE_ARAULLO_META_DIR_ELAZ read " << status << "\n";

	try {
		directions = vector<elaz>();
		directions.reserve( offs.nrows() );
//MK::here point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading PARAPROBE_ARAULLO_ELEVAZIM!" << "\n"; return false;
	}
	
	for( size_t i = 0; i < f32.size(); i += 2 ) {
		directions.push_back( elaz( f32.at(i+0), f32.at(i+1) ) );
	}
	f32 = vector<float>();
	
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "ReadElevAzimGridFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	
cout << ConfigAraullo::InputfileElevAzimGrid << " read successfully" << "\n";
	return true;
}


bool araulloHdl::read_trianglehull_from_apth5()
{
	double tic = MPI_Wtime();

	if ( ConfigAraullo::InputfileHullAndDistances.substr(ConfigAraullo::InputfileHullAndDistances.length()-3) != ".h5" ) {
		cerr << "The InputfileHullAndDistances " << ConfigAraullo::InputfileHullAndDistances << " has not the proper h5 file extension!" << "\n";
		return false;
	}

	inputTriHullH5Hdl.h5resultsfn = ConfigAraullo::InputfileHullAndDistances;
	if ( inputTriHullH5Hdl.read_trianglehull_from_apth5( ConfigAraullo::InputfileHullAndDistances, trianglehull ) == true ) {
		cout << ConfigAraullo::InputfileHullAndDistances << " triangle hull read successfully" << "\n";
	}
	else {
		cerr << ConfigAraullo::InputfileHullAndDistances << " triangle hull read failed!" << "\n";
		return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "ReadTriangleHullFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool araulloHdl::read_voxelized_from_apth5()
{
	double tic = MPI_Wtime();

	if ( ConfigAraullo::InputfileHullAndDistances.substr(ConfigAraullo::InputfileHullAndDistances.length()-3) != ".h5" ) {
		cerr << "The InputfileHullAndDistances " << ConfigAraullo::InputfileHullAndDistances << " has not the proper h5 file extension!" << "\n";
		return false;
	}

	if ( inputTriHullH5Hdl.read_voxelized_from_apth5( ConfigAraullo::InputfileHullAndDistances, ca ) == true ) {
		cout << ConfigAraullo::InputfileHullAndDistances << " voxelized read successfully" << "\n";
	}
	else {
		cerr << ConfigAraullo::InputfileHullAndDistances << " voxelized read failed!" << "\n";
		return false;
	}
	
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "ReadVoxelizationFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool araulloHdl::read_userdefined_rois_from_apth5()
{
	double tic = MPI_Wtime();

	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	if ( ConfigAraullo::InputfileUserROIPositions.substr(ConfigAraullo::InputfileUserROIPositions.length()-3) != ".h5" ) {
		cerr << "The InputfileUserROIPositions " << ConfigAraullo::InputfileUserROIPositions << " has not the proper apth5 file extension!" << "\n";
		return false;
	}

	//probe existence of xyz coordinates array in the input file and get dimensions
	inputUserROIsH5Hdl.h5resultsfn = ConfigAraullo::InputfileUserROIPositions;
	if ( inputUserROIsH5Hdl.query_contiguous_matrix_dims( PARAPROBE_ARAULLO_META_MP_XYZ, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the PARAPROBE_ARAULLO_META_MP_XYZ in the InputfileUserROIPositions!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_ARAULLO_META_MP_XYZ is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX ) {
		cerr << "PARAPROBE_ARAULLO_META_MP_XYZ is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_ARAULLO_META_MP_XYZ, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = inputUserROIsH5Hdl.read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_ARAULLO_META_MP_XYZ on InputfileUserROIPositions failed!" << "\n";
		return false;
	}
cout << "PARAPROBE_ARAULLO_META_MP_XYZ read " << status << "\n";

	try {
		userrois = vector<p3d>();
		userrois.reserve( offs.nrows() );
//MK::here point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading PARAPROBE_ARAULLO_META_MP_XYZ transfer array!" << "\n"; return false;
	}

	for( size_t i = 0; i < f32.size(); i += 3 ) {
		userrois.push_back( p3d( f32.at(i+0), f32.at(i+1), f32.at(i+2) ) );
	}
	f32 = vector<float>();

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "ReadUserROIPositionsFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

cout << ConfigAraullo::InputfileUserROIPositions << " read successfully" << "\n";
	return true;
}

//##MK::try to move these generic master to all broadcast functionalities into an own class


bool araulloHdl::broadcast_reconxyz()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
		return true;
	}

	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( xyz.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " ion reconstructions!" << "\n"; localhealth = 0;
		}
		if ( xyz.size() == 0 ) {
			cerr << "Ion positions is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the MASTER's xyz size and state!" << "\n"; return false;
	}

	//communicate size of the master's dataset
	int nevt = 0;
	if ( get_myrank() == MASTER ) {
		nevt = static_cast<int>(xyz.size());
	}
	MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	MPI_Synth_XYZ* buf = NULL;
	try {
		buf = new MPI_Synth_XYZ[nevt];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for MPI_Synth_XYZ buffer!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL;
		return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < xyz.size(); i++ ) {
			buf[i] = MPI_Synth_XYZ( xyz.at(i) );
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, nevt, MPI_Synth_XYZ_Type, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			xyz.reserve ( static_cast<size_t>(nevt) );
			for( int i = 0; i < nevt; i++ ) {
				xyz.push_back( p3d( buf[i].x, buf[i].y, buf[i].z ) );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout to instantiate xyz!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";	return false;
	}

//cout << "Rank " << get_myrank() << " listened to MASTER reconstruction.size() " << reconstruction.size() << "\n";
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "BroadcastReconXYZ", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


bool araulloHdl::broadcast_ranging()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
		return true;
	}

	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( rng.iontypes.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " iontypesinfo!" << "\n"; localhealth = 0;
		}
		if ( rng.iontypes.size() == 0 ) {
			cerr << "MASTER's rng.iontypes is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
		if ( ityp_org.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " ityp_org per ion!" << "\n"; localhealth = 0;
		}
		if ( ityp_org.size() == 0 ) {
			cerr << "MASTER's ityp_org is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the MASTER's ranging size and state!" << "\n"; return false;
	}

	//communicate metadata first
	int nrng_ivl_evt_dict[4] = {0, 0, 0, 0};
	if ( get_myrank() == MASTER ) {
		nrng_ivl_evt_dict[0] = static_cast<int>(rng.iontypes.size());
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
			nrng_ivl_evt_dict[1] += static_cast<int>(it->rng.size());
		}
		nrng_ivl_evt_dict[2] = static_cast<int>(ityp_org.size());
		nrng_ivl_evt_dict[3] = static_cast<int>(rng.iontypes_dict.size());
	}
	MPI_Bcast( nrng_ivl_evt_dict, 4, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	MPI_Ranger_Iontype* buf1 = NULL;
	MPI_Ranger_MQIval* buf2 = NULL;
	unsigned char* buf3 = NULL;
	MPI_Ranger_DictKeyVal* buf4 = NULL;
	try {
		buf1 = new MPI_Ranger_Iontype[nrng_ivl_evt_dict[0]];
		buf2 = new MPI_Ranger_MQIval[nrng_ivl_evt_dict[1]];
		buf3 = new unsigned char[nrng_ivl_evt_dict[2]];
		buf4 = new MPI_Ranger_DictKeyVal[nrng_ivl_evt_dict[3]];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for buf1, buf2, buf3, and buf4!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf1; buf1 = NULL;
		delete [] buf2; buf2 = NULL;
		delete [] buf3; buf3 = NULL;
		delete [] buf4; buf4 = NULL;
		return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		size_t iv = 0;
		size_t id = 0;
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++, id++ ) {
			unsigned char typid = static_cast<unsigned char>(it->id);
			buf1[id] = MPI_Ranger_Iontype( typid, it->strct.Z1, it->strct.N1, it->strct.Z2, it->strct.N2,
												it->strct.Z3, it->strct.N3, it->strct.sign, it->strct.charge );
			for( auto jt = it->rng.begin(); jt != it->rng.end(); jt++, iv++ ) {
				buf2[iv] = MPI_Ranger_MQIval( jt->lo, jt->hi, typid );
			}
		}
		for( size_t i = 0; i < ityp_org.size(); i++ ) {
			buf3[i] = ityp_org.at(i);
		}
		size_t ii = 0;
		for( auto kvt = rng.iontypes_dict.begin(); kvt != rng.iontypes_dict.end(); kvt++, ii++ ) {
			buf4[ii] = MPI_Ranger_DictKeyVal( static_cast<unsigned long long>(kvt->first), static_cast<unsigned long long>(kvt->second) );
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf1, nrng_ivl_evt_dict[0], MPI_Ranger_Iontype_Type, MASTER, MPI_COMM_WORLD );
	MPI_Bcast( buf2, nrng_ivl_evt_dict[1], MPI_Ranger_MQIval_Type, MASTER, MPI_COMM_WORLD);
	MPI_Bcast( buf3, nrng_ivl_evt_dict[2], MPI_UNSIGNED_CHAR, MASTER, MPI_COMM_WORLD );
	MPI_Bcast( buf4, nrng_ivl_evt_dict[3], MPI_Ranger_DictKeyVal_Type, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			rng.iontypes.reserve( static_cast<size_t>(nrng_ivl_evt_dict[0]) );
			for( int i = 0; i < nrng_ivl_evt_dict[0]; i++ ) {
				rng.iontypes.push_back( rangedIontype() );
				rng.iontypes.back().id = static_cast<unsigned int>( buf1[i].id );
				rng.iontypes.back().strct = evapion3( buf1[i].Z1, buf1[i].N1, buf1[i].Z2, buf1[i].N2, buf1[i].Z3, buf1[i].N3, buf1[i].sign, buf1[i].charge );
				for( int j = 0; j < nrng_ivl_evt_dict[1]; j++ ) { //was [2], ##MK:avoid O(N^2) search, but should not be a problem because N << 256
					if ( static_cast<unsigned int>(buf2[j].id) != rng.iontypes.back().id ) {
						continue;
					}
					else {
						rng.iontypes.back().rng.push_back( mqival( buf2[j].low, buf2[j].high ) );
					}
				}
			}
			ityp_org.reserve( static_cast<size_t>(nrng_ivl_evt_dict[2]) );
			for( int k = 0; k < nrng_ivl_evt_dict[2]; k++ ) {
				ityp_org.push_back( buf3[k] );
			}

			for ( int kv = 0; kv < nrng_ivl_evt_dict[3]; kv++ ) {
				rng.iontypes_dict.insert( pair<size_t,size_t>(
						static_cast<size_t>(buf4[kv].key), static_cast<size_t>(buf4[kv].val) ) );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout to instantiate rng.iontypes and ityp_org!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf1; buf1 = NULL;
	delete [] buf2; buf2 = NULL;
	delete [] buf3; buf3 = NULL;
	delete [] buf4; buf4 = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";	return false;
	}

//cout << "Rank " << get_myrank() << " listened to MASTER reconstruction.size() " << reconstruction.size() << "\n";
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "BroadcastRanging", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


bool araulloHdl::broadcast_distances()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
		return true;
	}

	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( dist2hull.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " ion dist2hull info!" << "\n"; localhealth = 0;
		}
		if ( dist2hull.size() == 0 ) {
			cerr << "MASTER's dist2hull is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the MASTER's dist2hull size and state!" << "\n"; return false;
	}

	//communicate size of the master's dataset
	int nevt = 0;
	if ( get_myrank() == MASTER ) {
		nevt = static_cast<int>(dist2hull.size());
	}
	MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	float* buf = NULL;
	try {
		buf = new float[nevt];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for float buffer!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL;
		return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < dist2hull.size(); i++ ) {
			buf[i] = dist2hull.at(i);
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, nevt, MPI_FLOAT, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			dist2hull.reserve ( static_cast<size_t>(nevt) );
			for( int i = 0; i < nevt; i++ ) {
				dist2hull.push_back( buf[i] );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout to instantiate dist2hull!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";	return false;
	}

//cout << "Rank " << get_myrank() << " listened to MASTER reconstruction.size() " << reconstruction.size() << "\n";
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "BroadcastDist2Hull", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


bool araulloHdl::broadcast_directions()
{
	if ( get_nranks() < 2 ) {
		//memorize number of SO2 directions
		ConfigAraullo::NumberOfSO2Directions = directions.size(); //modifies as value in the process-local instance of ConfigAraullo class

		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
		return true;
	}

	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( directions.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " directions!" << "\n"; localhealth = 0;
		}
		if ( directions.size() == 0 ) {
			cerr << "Directions is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the size and state of the datasets!" << "\n";
		return false;
	}
	
	//communicate size of the master's dataset
	int nevt = 0;
	if ( get_myrank() == MASTER ) {
		nevt = static_cast<int>(directions.size());
	}
	MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	MPI_ElevAzim* buf = NULL;
	try {
		buf = new MPI_ElevAzim[nevt];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for MPI_ElevAzim buffer!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL; return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < directions.size(); ++i ) {
			buf[i] = MPI_ElevAzim( directions.at(i).elevation, directions[i].azimuth );
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, nevt, MPI_ElevAzim_Type, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			directions.reserve( static_cast<size_t>(nevt) );
			for( int i = 0; i < nevt; i++ ) {
				directions.push_back( elaz( buf[i].elevation, buf[i].azimuth ) );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf1 readout!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;
		
	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";
		return false;
	}

	//memorize number of SO2 directions
	ConfigAraullo::NumberOfSO2Directions = directions.size(); //modifies as value in the process-local instance of ConfigAraullo class

//cout << "Rank " << get_myrank() << " listened to MASTER recon_positions.size() " << directions.size() << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "BroadcastDirectionsElevAzim", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


/*
bool araulloHdl::broadcast_reconstruction()
{
	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( reconstruction.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " ion reconstructions!" << "\n"; localhealth = 0;
		}
		if ( reconstruction.size() == 0 ) {
			cerr << "Ion positions is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the size and state of the datasets!" << "\n";
		return false;
	}
	
	if ( get_nranks() > 1 ) { //shortcut if only a single MPI process across MPI_COMM_WORLD

		//define MPI datatype which assists us to logically structure the communicated data packages
		//##MK::use MPI_Ion_Type; user compound type MPI_FLOAT, MPI_FLOAT, MPI_FLOAT, MPI_UNSIGNED

		//communicate size of the master's dataset
		int nevt = 0;
		if ( get_myrank() == MASTER ) {
			nevt = static_cast<int>(reconstruction.size());
		}
		MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

		//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
		localhealth = 1;
		MPI_Ion* buf = NULL;
		try {
			buf = new MPI_Ion[nevt];
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error for MPI_Ion buffer!" << "\n"; localhealth = 0;
		}
		
		globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != get_nranks() ) {
			cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
			delete [] buf; buf = NULL;
			return false;
		}

		//master populates communication buffer
		if ( get_myrank() == MASTER ) {
			for( size_t i = 0; i < reconstruction.size(); ++i ) {
				buf[i] = MPI_Ion( reconstruction.at(i).x, reconstruction.at(i).y, reconstruction.at(i).z, reconstruction.at(i).m );
			}
		}

		//collective call MPI_Bcast, MASTER broadcasts, slaves listen
		MPI_Bcast( buf, nevt, MPI_Ion_Type, MASTER, MPI_COMM_WORLD );

		//slave nodes pull from buffer
		if ( get_myrank() != MASTER ) {
			try {
				reconstruction.reserve( static_cast<size_t>(nevt) );
				for( int i = 0; i < nevt; i++ ) {
					reconstruction.push_back( p3dm1( buf[i].x, buf[i].y, buf[i].z, buf[i].m ) );
				}
			}
			catch(bad_alloc &mecroak) {
				cerr << "Rank " << get_myrank() << " allocation error during buf1 readout!" << "\n"; localhealth = 0;
			}
		}

		//all release the buffer, delete on NULL pointer gets compiled as no op
		delete [] buf; buf = NULL;
		
		//final check whether all were successful so far
		globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != get_nranks() ) {
			cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";
			return false;
		}
	}
	else {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
	}

//cout << "Rank " << get_myrank() << " listened to MASTER reconstruction.size() " << reconstruction.size() << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "BroadcastReconstructionAcrossMPIProcesses", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}
*/


bool araulloHdl::broadcast_triangles()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
		return true;
	}

	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( trianglehull.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " triangles!" << "\n"; localhealth = 0;
		}
		if ( trianglehull.size() == 0 ) {
			cerr << "Trianglehull is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the size and state of the trianglehull datasets!" << "\n";
		return false;
	}

	//communicate size of the master's dataset
	int nevt = 0;
	if ( get_myrank() == MASTER ) {
		nevt = static_cast<int>(trianglehull.size());
	}
	MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	MPI_Triangle3D* buf = NULL;
	try {
		buf = new MPI_Triangle3D[nevt];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for MPI_Triangle3D buffer!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL; return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < trianglehull.size(); i++ ) {
			buf[i] = MPI_Triangle3D( trianglehull.at(i) );
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, nevt, MPI_Triangle3D_Type, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			trianglehull.reserve( static_cast<size_t>(nevt) );
			for( int i = 0; i < nevt; i++ ) {
				trianglehull.push_back( tri3d( 	buf[i].x1, buf[i].y1, buf[i].z1,
												buf[i].x2, buf[i].y2, buf[i].z2,
												buf[i].x3, buf[i].y3, buf[i].z3  ) );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf1 readout!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";
		return false;
	}

//cout << "Rank " << get_myrank() << " listened to MASTER trianglehull.size() " << trianglehull.size() << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "BroadcastTriangleHull", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


bool araulloHdl::broadcast_vxlized()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
		return true;
	}

	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( ca.vxlgrid.nxyz >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " voxel!" << "\n"; localhealth = 0;
		}
		if ( ca.vxlgrid.nxyz == 0 ) {
			cerr << "Voxelgrid is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the size and state of the voxelgrid datasets!" << "\n";
		return false;
	}

	//communicate size of the master's dataset
	MPI_VxlizationInfo ifo = MPI_VxlizationInfo();
	if ( get_myrank() == MASTER ) {
		ifo = MPI_VxlizationInfo( ca.vxlgrid.box, ca.vxlgrid.width, ca.vxlgrid.width, ca.vxlgrid.width, ca.vxlgrid.nx, ca.vxlgrid.ny, ca.vxlgrid.nz );
	}
	MPI_Bcast( &ifo, 1, MPI_VxlizationInfo_Type, MASTER, MPI_COMM_WORLD );

	if ( get_myrank() != MASTER ) {
		ca.vxlgrid.nx = ifo.nx;
		ca.vxlgrid.ny = ifo.ny;
		ca.vxlgrid.nz = ifo.nz;
		ca.vxlgrid.nxy = ifo.nx*ifo.ny;
		ca.vxlgrid.nxyz = ifo.nx*ifo.ny*ifo.nz;
		ca.vxlgrid.width = ifo.dx;
		ca.vxlgrid.box = aabb3d( ifo.xmi, ifo.xmx, ifo.ymi, ifo.ymx, ifo.zmi, ifo.zmx );
		ca.vxlgrid.box.scale();
	}

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	unsigned char* buf = NULL;
	try {
		buf = new unsigned char[ca.vxlgrid.nxyz];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for unsigned char buffer!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL; return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < ca.vxlgrid.nxyz; i++ ) {
			buf[i] = ( ca.IsInside[i] == true ) ? IS_INSIDE : IS_VACUUM;
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, static_cast<int>(ca.vxlgrid.nxyz), MPI_UNSIGNED_CHAR, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		if ( ca.IsInside == NULL ) {
			try {
				ca.IsInside = new bool[ca.vxlgrid.nxyz];
				for( size_t i = 0; i < ca.vxlgrid.nxyz; i++ ) {
					ca.IsInside[i] = (buf[i] == IS_INSIDE ) ? true : false;
				}
			}
			catch(bad_alloc &mecroak) {
				cerr << "Rank " << get_myrank() << " allocation error during buf1 readout!" << "\n"; localhealth = 0;
			}
		}
		else {
			cerr << "Rank " << get_myrank() << " ca.IsInside was already allocated!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";
		return false;
	}

//cout << "Rank " << get_myrank() << " listened to MASTER ca.vxlgrid.nxyz " << ca.vxlgrid.nxyz << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "BroadcastVoxelgrid", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


bool araulloHdl::broadcast_userdefined_rois()
{
	//##MK::needs implementation
	//##################
	//##################
	return true;
}


bool araulloHdl::define_phase_candidates()
{
	//all create the internal spatial statistics and two-point statistics (SDM) task list,
	//i.e. which iontype combinations are probed
	double tic = MPI_Wtime();

	size_t max_memory_per_node = araullo_tictoc.get_memory_max_on_node();
	size_t max_memory_for_buf = 0.1 * max_memory_per_node;

	cout << "Rank " << get_myrank() << " max_memory_per_node " << max_memory_per_node << " B" << "\n";
	cout << "Rank " << get_myrank() << " max_memory_for_buf " << max_memory_for_buf << " B" << "\n";

/*
 * ################################################################CAN BE DELETED NEEDS COMMENT#############
	//paraprobe-spatstat buffers for each thread and each task 2x histograms and/or 2x SDMs respectively, to
	//allow for thread-memory-local parallelized writing back of results without global sync
	//this may create situations where the computing node provides not enough memory to allocate all the buffers
	//consequently, we want to catch this as earliest as possible to warn the user and advise less workpackages per run
	//max_memory_per_node in Byte, approximately MK::just a not too naive mechanism to avoid
	//that unexperienced user issue instruct e.g. 1000^3 large SDM with 100-th of combinations
*/

	//firstly, we transform the human-readable icombis targ/nbor strings pairs into evapion3 objects
	vector<Evapion3Combi> tmp;
	for( auto it = itsk.icombis.begin(); it != itsk.icombis.end(); it++ ) {
		tmp.push_back( Evapion3Combi() );

		rng.add_evapion3( it->targets, tmp.back().targets );
/*
#ifdef EMPLOY_SINGLEPRECISION
		apt_real parse_realspace = stof( it->realspacestr );
#else
		apt_real parse_realspace = stod( it->realspacestr );
#endif
		if ( parse_realspace < EPSILON || parse_realspace > ConfigAraullo::ROIRadiusMax ) {
			cerr << "Rank " << get_myrank() << " attempt to use an invalid realspace value in a an evapion3 ityp combi, combi will be ignored!" << "\n";
		}
		else {
			tmp.back().realspace = parse_realspace;
		}
*/
		tmp.back().realspace = 0.0;
#ifdef EMPLOY_SINGLEPRECISION
		apt_real parse_rmi = stof( it->realspacemin );
#else
		apt_real parse_rmi = stod( it->realspacemin );
#endif
		if ( parse_rmi < EPSILON || parse_rmi > ConfigAraullo::ROIRadiusMax ) {
			cerr << "Rank " << get_myrank() << " attempt to use an invalid realspacemin value in a an evapion3 ityp combi!" << "\n"; return false;
		}
		else {
			tmp.back().realspacemin = parse_rmi;
		}
#ifdef EMPLOY_SINGLEPRECISION
		apt_real parse_rmx = stof( it->realspacemax );
#else
		apt_real parse_rmx = stod( it->realspacemax );
#endif
		if ( parse_rmx < EPSILON || parse_rmx > ConfigAraullo::ROIRadiusMax || parse_rmi > parse_rmx ) {
			cerr << "Rank " << get_myrank() << " attempt to use an invalid realspacemax value in a an evapion3 ityp combi!" << "\n"; return false;
		}
		else {
			tmp.back().realspacemax = parse_rmx;
		}

		/*
		//##BEGIN DEBUG
		cout << "Rank " << get_myrank() << " targets __" << it->targets << "__ ---->" << "\n";
		for( auto jt = tmp.back().targets.begin(); jt != tmp.back().targets.end(); jt++ ) {
			cout << (int) jt->Z1 << ";" << (int) jt->Z2 << ";" << (int) jt->Z3  << "\n";
		}
		cout << "Rank " << get_myrank() << " realspace __" << it->realspacestr << "__ ---->" << "\n";
		cout << tmp.back().realspace << ";" << tmp.back().realspacemin << ";" << tmp.back().realspacemax << "\n";
		//##END DEBUG
		*/
	}

	//secondly, we have to understand that these user-combinations may not at all be present in the ranging data, i.e.
	//are possibly not all evapion3 types that we identified in the analyzed dataset as ranging data,
	//sure, in most cases user will make correct input but in every case we need to make sure
	map<size_t,size_t>::iterator does_not_exist = rng.iontypes_dict.end();
	for( auto jt = tmp.begin(); jt != tmp.end(); jt++ ) {
		UC8Combi valid;
		for( auto tg = jt->targets.begin(); tg != jt->targets.end(); tg++ ) {
			size_t hashval = rng.hash_molecular_ion( tg->Z1, tg->Z2, tg->Z3 );
			//MK::it is important to understand here that the user is not enforced to write the molecular ions
			//within the XML input file in order, because above preprocessing assures that combinations such as AlH: or H:Al will be sorted
			//into a unique representation namely Al, H, UNOCCUPIED as it holds also for the ranging
			//Consequently, internal ion type handling is handling commutativity to please the user
			map<size_t,size_t>::iterator vt = rng.iontypes_dict.find( hashval );
			if ( vt != does_not_exist ) { //kt is an ion type which exists within the ranging data
//cout << " Adding an identified ion from the ranging process as target\t\t" << vt->first << "\t\t" << vt->second << "\n";
				valid.targets.push_back( vt->second );
			}
		}
		valid.realspace = jt->realspace;
		valid.realspacemin = jt->realspacemin;
		valid.realspacemax = jt->realspacemax;

		itsk.combinations.push_back( UC8Combi() );
		for( auto tgs = valid.targets.begin(); tgs != valid.targets.end(); tgs++ ) { //explicit deep copy
			itsk.combinations.back().targets.push_back( *tgs );
		}
		itsk.combinations.back().realspace = valid.realspace;
		itsk.combinations.back().realspacemin = valid.realspacemin;
		itsk.combinations.back().realspacemax = valid.realspacemax;
	}

	//get rid of temporaries
	tmp = vector<Evapion3Combi>();

	//thirdly, and now having the IontypeCombinations we plan the actual PhaseCandidates or tasks
	//the key idea here is that we conduct eventually multiple Araullo Peters analyses per ROI
	//think like indexing in Electron Backscatter Diffraction (EBSD): we need to know crystal unit cells of possible candidates
	//with which to run the analysis a priori. for araullo it is the same, we probe the local spatial arrangement of
	//specific iontypes and for each such ityp candidate we probe intensity of a specific Fourier spectrum magnitude bin
	//as we know that for a pristine crystal of this phase and sub-lattice we would expect lattice planes in the unrotated
	//configuration, we can work in the unrotated configuration because we probe along different direction
	//one direction will be strong and aligned with the crystal [uvw] regardless the orientation if the grid of SDMs is
	//probed accurately


	unsigned int tskid = 0; //scientific analysis taskIDs start at 0
	size_t max_memory_expected = 0; //in bytes

	if ( itsk.combinations.size() < 1 ) {
		cerr << "Rank " << get_myrank() << " combinations were specified but not parsable after having evaluated InputfileReconstruction ranging data and settings!" << "\n";
		return false;
	}

	for( auto it = itsk.combinations.begin(); it != itsk.combinations.end(); it++ ) {
		if ( it->targets.size() <= TARGET_LIST_SIZE ) {

			itsk.iphcands.push_back( PhaseCandidate() );
			size_t i = 0;
			for(       ; i < it->targets.size(); i++ ) {
				itsk.iphcands.back().targets[i] = ( static_cast<int>(it->targets.at(i)) < static_cast<int>(UCHARMX) ) ?
						it->targets.at(i) : 0xFF;
			}
			for(       ; i < TARGET_LIST_SIZE; i++ ) {
				itsk.iphcands.back().targets[i] = 0xFF;
			}
			itsk.iphcands.back().realspace = it->realspace;
			itsk.iphcands.back().realspacemin = it->realspacemin;
			itsk.iphcands.back().realspacemax = it->realspacemax;
			itsk.iphcands.back().candid = static_cast<unsigned int>(it - itsk.combinations.begin() );

			//memory consumption per iph
			//max_memory_expected += 0;
		}
		else {
			cerr << "Rank " << get_myrank() << " skipping a PhaseCandidate with too many target iontypes maximum is " << TARGET_LIST_SIZE << "\n";
		}
	}

	if ( itsk.iphcands.size() < 1 ) {
		cerr << "Rank " << get_myrank() << " no valid PhaseCandidate(s) were identifiable!" << "\n";
		return false;
	}

	/*
	//final check would all these buffers fit in memory?
	if ( ( max_memory_expected * static_cast<size_t>(1 + omp_get_max_threads()) ) >= max_memory_per_node ) { //+1 because there is the process-local copy to decouple critical regions of non-master threads and master thread
		cerr << "With " << max_memory_expected << " * " << omp_get_max_threads() << " threads we would exceed the max_memory_per_node!" << "\n"; return false;
	}
	else {
		cout << "Rank expected memory demands for buffering intermediate results to be " << max_memory_expected << " B" << "\n";
	}
	*/

	//##BEGIN DEBUG
	for( int rk = MASTER; rk < get_nranks(); rk++ ) {
		if ( rk == get_myrank() ) {
		cout << "Rank " << rk << " prepared all tasks now verifying successful deep copies of values..." << "\n";
			for( auto ic = itsk.icombis.begin(); ic != itsk.icombis.end(); ic++ ) {
				cout << ic->targets << "\t\t" << ic->realspacestr << "\t\t" << ic->realspacemin << ";" << ic->realspacemax << "\n";
			}
			for( auto jt = itsk.combinations.begin(); jt != itsk.combinations.end(); jt++ ) {
				cout << "Rank " << rk << " itsk.combinations " << jt - itsk.combinations.begin() << "\n";
				for( auto tg = jt->targets.begin(); tg != jt->targets.end(); tg++ ) {
					cout << "Rank " << rk << " tg = " << (int) *tg << "\n";
				}
				cout << "Rank " << rk << " rsp = " << jt->realspace << "\t\t" << jt->realspacemin << ";"<< jt->realspacemax << "\n";
			}
			for( auto tsk = itsk.iphcands.begin(); tsk != itsk.iphcands.end(); tsk++ ) {
				cout << "Rank " << rk << " isk.iphcands" << "\n";

				for( unsigned int uc = 0; uc < TARGET_LIST_SIZE; uc++ ) {
					cout << "Target\t\t" << (int) tsk->targets[uc] << "\n";
				}
				cout << "realspc = " << tsk->realspace << "\n";
				cout << "realmin = " << tsk->realspacemin << "\n";
				cout << "realmax = " << tsk->realspacemax << "\n";
				cout << "candid = " << tsk->candid << "\n";
			}
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
	//##END DEBUG

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "DefinePhaseCandidates", APT_XX, APT_IS_PAR, mm, tic, toc );

	return true;
}

/*
void araulloHdl::spatial_decomposition()
{
	double tic, toc;
	tic = MPI_Wtime();

	sp.tip_aabb_get_extrema( reconstruction );

	toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "SpecimenAABB3DGetExtrema", APT_XX, APT_IS_SEQ, mm, tic, toc);

	tic = MPI_Wtime();

	sp.loadpartitioning( reconstruction );

	toc = MPI_Wtime();
	mm = araullo_tictoc.get_memoryconsumption();
	araullo_tictoc.prof_elpsdtime_and_mem( "AABB3DLoadPartitioning", APT_XX, APT_IS_PAR, mm, tic, toc);
}
*/


void araulloHdl::itype_sensitive_spatial_decomposition()
{
	double tic = MPI_Wtime();

	sp.tip_aabb_get_extrema( xyz );

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "SpecimenAABB3DGetExtrema", APT_XX, APT_IS_SEQ, mm, tic, toc);

	tic = MPI_Wtime();

	//get maximum iontype
	unsigned int maximum_ityp = 0;
	for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
		if ( it->id < static_cast<unsigned char>(UCHARMX) ) {
			if ( it->id > maximum_ityp ) {
				maximum_ityp = it->id;
			}
		}
		else {
			cerr << "Detected an invalid iontype during itype_sensitive_spatial_decomposition!" << "\n"; return;
		}
	}

	maximum_iontype_uc = static_cast<unsigned char>(maximum_ityp); //MK::not the number of disjoint types but the maximum ityp value!
cout << "Rank " << get_myrank() << " maximum itype identified is " << (int) maximum_iontype_uc << "\n";

	sp.loadpartitioning_subkdtree_per_ityp( xyz, dist2hull, ityp_org, ityp_org, maximum_iontype_uc ); //random labels will not be used!

	//first point were ityp_org and ityp_rnd are no longer necessary and could in principle be deleted

	toc = MPI_Wtime();
	mm = araullo_tictoc.get_memoryconsumption();
	araullo_tictoc.prof_elpsdtime_and_mem( "AABB3DLoadPartitioning", APT_XX, APT_IS_PAR, mm, tic, toc);
}


void araulloHdl::define_matpoint_volume_grid()
{
	double tic = MPI_Wtime();

	//##MK::to begin with use the entire tip as our reference window to analyze
	mp.define_probe_volume( sp.tip );

	if (ConfigAraullo::ROISamplingMethod == CUBOIDAL_GRID ) {
		/*mp.define_regular_grid( ConfigAraullo::SamplingGridBinWidthX,
								ConfigAraullo::SamplingGridBinWidthY,
								ConfigAraullo::SamplingGridBinWidthZ,
								ConfigAraullo::ROIRadiusMax );*/

		mp.define_regular_grid( ConfigAraullo::SamplingGridBinWidthX,
										ConfigAraullo::SamplingGridBinWidthY,
										ConfigAraullo::SamplingGridBinWidthZ,
										ConfigAraullo::ROIRadiusMax,
										ca.vxlgrid, ca.IsInside, trianglehull );
	}
	else if ( ConfigAraullo::ROISamplingMethod == SINGLE_POINT_USER ) {
		p3d here = p3d();
		stringstream parsethis;
		parsethis << ConfigAraullo::SamplingPosition;
		string datapiece;
		getline( parsethis, datapiece, ';');	here.x = stof( datapiece );
		getline( parsethis, datapiece, ';');	here.y = stof( datapiece );
		getline( parsethis, datapiece, ';');	here.z = stof( datapiece );
cout << "SINGLE_POINT_USER " << here.x << ";" << here.y << ";" << here.z << "\n";

		mp.define_single_point( here, ConfigAraullo::ROIRadiusMax );
	}
	else if ( ConfigAraullo::ROISamplingMethod == SINGLE_POINT_CENTER ) {
		p3d here = sp.tip.center();
cout << "SINGLE_POINT_CENTER " << here.x << ";" << here.y << ";" << here.z << "\n";

		mp.define_single_point( here, ConfigAraullo::ROIRadiusMax );
	}
	else if ( ConfigAraullo::ROISamplingMethod == RANDOM_POINTS_FIXED ) {
		/*
		mp.define_random_cloud_fixed( ConfigAraullo::FixedNumberOfROIs, ConfigAraullo::ROIRadiusMax );
		*/
		mp.define_random_cloud_fixed( ConfigAraullo::FixedNumberOfROIs, ConfigAraullo::ROIRadiusMax,
										ca.vxlgrid, ca.IsInside, trianglehull );
	}
	else if ( ConfigAraullo::ROISamplingMethod == USERDEFINED_POINTS ) {

		mp.define_locally_refined_grid( userrois, ConfigAraullo::ROIRadiusMax,
										ConfigAraullo::RefineGridOldBinWidthX, ConfigAraullo::RefineGridNewSubdivisionX,
										ca.vxlgrid, ca.IsInside, trianglehull );
	}
	else {
		return;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "DefineMaterialPointEnsemble", APT_XX, APT_IS_SEQ, mm, tic, toc);
}


void araulloHdl::distribute_matpoints_on_processes_roundrobin()
{
	//all processes have the same matpoints and agree on the same global round-robin work partitioning
	//##MK::error handling
	if ( mp.matpoints.size() >= static_cast<size_t>(INT32MX-1) ) {
		cerr << "More material sampling points were defined than current implementation can handle!" << "\n"; return;
	}
	if ( mp.matpoints.size() == 0 ) {
		cerr << "No material points to process!" << "\n"; return;
	}

	double tic = MPI_Wtime();

	mp2rk.reserve( mp.matpoints.size() );
	//globally orchestrated round-robin partitioning
	//MK::block partitioning is expected to result in strong load-imbalance because the implicit order on the ion positions
	//in xyz maps the specimen geometry, whether positions earlier or to the end of the array xyz are likely closer to
	//the specimen boundary and therefore processes taking such block on average have fewer ions per region and therefore
	//will finish earlier, even though the sequence of ion hits on the detector xy plane i.e. the xy plane is somewhat random
	//the order in z is not!
	int nmp = static_cast<int>(mp.matpoints.size());
	int nr = get_nranks();
	int mr = get_myrank();
	for( int mpid = 0; mpid < nmp; mpid++ ) {
		int thisrank = mpid % nr;
		mp2rk.push_back( thisrank );
		if ( thisrank != mr ) {
			continue;
		}
		else {
			mp2me.push_back( mpid ); //global mat point IDs!
//cout << "Rank " << thisrank << " takes care of material point/ROI " << mpid << "\n";
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = araullo_tictoc.get_memoryconsumption();
	araullo_tictoc.prof_elpsdtime_and_mem( "DistributeMatPointsOnMPIProcesses", APT_XX, APT_IS_PAR, mm, tic, toc);

	cout << "Rank " << get_myrank() << " will process " << mp2me.size() << " of a total of " << mp2rk.size() << " sampling points" << "\n";
}


/*
gpu_cpu_max_workload fourierHdl::plan_max_per_epoch( const int ngpu, const int nthr, const int nmp )
{
	gpu_cpu_max_workload res = gpu_cpu_max_workload();
	if ( ngpu > 0 ) {
		if ( nthr > 1 ) { //master delegates work to GPUs, only if additional threads CPU also participates in FT
			if ( nmp <= nthr-1 ) { //##MK::small workload only on the GPU
				res.chunk_max_cpu = 0; //##MK::could be improved, but insignificant
				res.chunk_max_gpu = nmp;
			}
			else { //large workload distribute on GPUs and CPUs
				res.chunk_max_cpu = nthr-1;
				res.chunk_max_gpu = ngpu * ConfigFourier::GPUWorkload; //##MK::needs benchmarking, empirical!, per GPU!, assume multiple of the same accelerator e.g. 2x NVIDIA V100 as on TALOS
			}
		}
		else { //only a master which delegates work to GPUs, no work scheduled to CPUs, master doesnot solve own fourier
			res.chunk_max_cpu = 0; //##MK::could be improved, but insignificant
			res.chunk_max_gpu = ngpu * ConfigFourier::GPUWorkload;
		}
	}
	else { //no GPU
		res.chunk_max_gpu = 0;
		res.chunk_max_cpu = nthr; //every thread gets an own FT to work on to maximize independent workload as such minimize sync
		//##MK::might be problematic for large NN, as memory consumption is at least nt*(NN^3)*3*8B, <=256 should no problem even for nt=40 for TALOS
	}

	return res;
}


gpu_cpu_now_workload fourierHdl::plan_now_per_epoch(
		const int ngpu, const int nthr, const int nmp, const int mp, const gpu_cpu_max_workload mxl )
{
	gpu_cpu_now_workload res = gpu_cpu_now_workload();
	int rem = nmp - mp;

	if ( ngpu > 0 ) { //if we can use accelerators
		if ( nthr > 1 ) { //master delegates work to GPUs surplus additional threads CPUs also computing FTs
			if ( rem >= (mxl.chunk_max_gpu + mxl.chunk_max_cpu) ) { //still enough work for GPUs and CPUs
				res.chunk_now_gpu = mxl.chunk_max_gpu;
				res.chunk_now_cpu = mxl.chunk_max_cpu;
			}
			else if ( rem >= mxl.chunk_max_cpu ) { //more work than CPUs should take off
				res.chunk_now_gpu = rem - mxl.chunk_max_cpu; //GPUs assist
				res.chunk_now_cpu = mxl.chunk_max_cpu; //CPUs remainder
			}
			else {
				res.chunk_now_gpu = 0; //##MK::can be improved...
				res.chunk_now_cpu = rem;
			}
		}
		else { //only master
			if ( rem >= mxl.chunk_max_gpu ) {
				res.chunk_now_gpu = mxl.chunk_max_gpu;
				res.chunk_now_cpu = 0;
			}
			else {
				res.chunk_now_gpu = rem;
				res.chunk_now_cpu = 0;
			}
		}
	}
	else { //we have no accelerators
		res.chunk_now_gpu = 0;
		if ( rem >= mxl.chunk_max_cpu )
			res.chunk_now_cpu = mxl.chunk_max_cpu;
		else
			res.chunk_now_cpu = rem;
	}

	return res;
}


void fourierHdl::generate_debug_roi( mt19937 & mydice, vector<dft_real> * out )
{
	//generate a pseudo lattice workload for testing purposes of the algorithm
	uniform_real_distribution<dft_real> unifrnd(0.f, 1.f); //no MT warmup
	int imi = -5;
	int imx = +5;
	dft_real a = 0.404; //lattice constant
	dft_real da = 0.10 * a; //some simple squared scatter about ideal lattice position
	for( int z = imi; z <= imx; z++ ) {
		for( int y = imi; y <= imx; y++ ) {
			for( int x = imi; x <= imx; x++ ) {
				//create dummy sc lattice
				dft_real xx = static_cast<dft_real>(x) * a * (2*unifrnd(mydice)-1.0) * 1.0 * da;
				dft_real yy = static_cast<dft_real>(y) * a * (2*unifrnd(mydice)-1.0) * 1.0 * da;
				dft_real zz = static_cast<dft_real>(z) * a * (2*unifrnd(mydice)-1.0) * 0.5 * da;
				//if ( unifrnd(mydice) < 1.0 ) { //random thinning
				out->push_back( xx );
				out->push_back( yy );
				out->push_back( zz );
				//}
			}
		}
	}
	//##MK::BEGIN OF DEBUGGING
	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " generated a dummy dataset with " << out->size() << " coordinates" << "\n";
	}
	//##MK::END OF DEBUGGING
}


void fourierHdl::query_ions_within_roi( mp3d const & roicenter, vector<dft_real> * out )
{
	//CALLED FROM WITHIN PARALLEL REGION
	apt_xyz SearchRadiusSQR = SQR(roicenter.R);
	p3dm1 probesite = p3dm1( roicenter.x, roicenter.y, roicenter.z, DEFAULTTYPE );

	vector<p3dm1> neighbors3dm1;
	for( size_t thr = MASTER; thr < sp.db.size(); thr++ ) { //scan all regions O(lgN) to find neighboring candidates
		threadmemory* thisregion = sp.db.at(thr);
		if ( thisregion != NULL ) {
			vector<p3dm1> const & theseions = thisregion->ionpp3_kdtree;
			kd_tree* curr_kauri = thisregion->threadtree;
			if ( curr_kauri != NULL ) {
				curr_kauri->range_rball_noclear_nosort_p3d( probesite, theseions, SearchRadiusSQR, neighbors3dm1 );
			}
		}
	}

	for( auto it = neighbors3dm1.begin(); it != neighbors3dm1.end(); it++ ) { //un-pack and layout into de-interleaved array of xyz distance differences
		out->push_back( it->x - probesite.x );
		out->push_back( it->y - probesite.y );
		out->push_back( it->z - probesite.z );
	}
}


void fourierHdl::debug_epoch_profiling( vector<epoch_log> const & in, vector<epoch_prof> const & timing )
{
	//generate table with epochs as rows and columns detailing what the threads were doing
	//demands that number of threads did not change
	if ( in.size() < 1 ) {
		cerr << "Epoch diary is empty!" << "\n"; return;
	}
	size_t expected_nt = static_cast<size_t>(in.at(0).nthreads);
	if ( (in.size() % expected_nt) != 0 ) {
		cerr << in.size() << "\n";
		cerr << expected_nt << "\n";
		cerr << (in.size() % expected_nt) << "\n";
		cerr << "Total number of logs is not an integer multiple of the expected number of threads so data are faulty or data are missing!" << "\n"; return;
	}
	for( auto it = in.begin(); it != in.end(); it++ ) {
		if ( static_cast<size_t>(it->nthreads) == expected_nt ) {
			continue;
		}
		else {
			cerr << "The total number of threads was not the same for all epochs" << "\n"; return;
		}
	}
	size_t nepochs = in.size() / expected_nt;
	if ( timing.size() != nepochs ) {
		cerr << "The number of epoch_prof timing steps is inconsistent with the thread profiling data!" << "\n"; return;
	}

	//##MK::suboptimal... one file per rank
	string fn = "PARAPROBE.Fourier.SimID." + to_string(ConfigShared::SimID) + ".Rank." + to_string(get_myrank()) + ".EpochThreadProfiling.csv";

	ofstream csvlog;
	csvlog.open(fn.c_str(), ofstream::out | ofstream::trunc);
	if (csvlog.is_open() == true) {
		//header
		csvlog << "Epoch;MPITotalWallclock";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "OMPWallClock";
		//csvlog << ";";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "OMPIonTotal";
		//csvlog << ";";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "OMPChunkTotal";
		csvlog << "\n";
		csvlog << "Epoch;MPITotalWallClock";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "Thread" << thr;
		//csvlog << ";";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "Thread" << thr;
		//csvlog << ";";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "Thread" << thr;
		csvlog << "\n";

		for( size_t i = 0; i < in.size(); i += expected_nt ) {
			csvlog << in.at(i).epoch << ";" << timing.at(i/expected_nt).dt;
			for( size_t thr = MASTER; thr < expected_nt; thr++ )
				csvlog << ";" << in.at(i+thr).dt;
			//csvlog << ";";
			for( size_t thr = MASTER; thr < expected_nt; thr++ )
				csvlog << ";" << in.at(i+thr).ionsum;
			//csvlog << ";";
			for( size_t thr = MASTER; thr < expected_nt; thr++ )
				csvlog << ";" << in.at(i+thr).nchunks;
			csvlog << "\n";
		}

		csvlog.flush();
		csvlog.close();
		cout << "Rank " << get_myrank() << " successful generation of epoch profiling protocol file" << "\n";
	}
	else {
		cerr << "Unable to write process-local epoch profiling protocol file" << "\n";
	}
}
*/


/*
void araulloHdl::execute_local_workpackage()
{
	double tic = MPI_Wtime();

	if ( mp2me.size() == 0) {
		cout << "WARNING:: Rank " << get_myrank() << " has no work to do!" << "\n"; return;
	}

	//identify which ion types we need to probe at all as they are targets in a PhaseCandidate
	//all other ions are immediately ignored thereby improving on ityp_specific ion querying
	//important is that material point positions in contrast to ions in paraprobe-spatstat mostly, except
	//for some seldom by chance events lay exactly at the positions of ions, therefore we only need to query
	//externally wrt to the kdtrees

	#pragma omp parallel
	{
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();
		//mkl_set_num_threads_local(1);

		#pragma omp master
		{
			workers = vector<acquisor*>( nt, NULL );
		}
		//necessary as otherwise write back conflict on workers from threads
		#pragma omp barrier

		//setup thread-local worker and use thread-local memory
		acquisor* thrhdl = NULL;
		try {
			thrhdl = new acquisor;
		}
		catch (bad_alloc &mecroak) {
			#pragma omp critical
			{
				cerr << "Rank " << get_myrank() << " thread " << mt << " unable to allocate an acquisor instance!" << "\n";
			}
		}

		//now workers backreference holders can safely be modified by current thread
		//MK::pointer will point to thread-local memory
		#pragma omp critical
		{
			workers.at(mt) = thrhdl;
		}

		thrhdl->configure();

		apt_real R = thrhdl->cfg.R; //could be in the future modified to have different radii per material point
		apt_real RSQR = SQR(R);
		//##MK::how is it assured that workers is always !NULL ?

		//make thread-local memory copies of the phases to reduce cache traffic
		vector<PhaseCandidate> mytsk;
		for( auto tsk = itsk.iphcands.begin(); tsk != itsk.iphcands.end(); tsk++ ) {
			mytsk.push_back( PhaseCandidate() );
			for( unsigned int tg = 0; tg < TARGET_LIST_SIZE; tg++ ) {
				mytsk.back().targets[tg] = tsk->targets[tg];
			}
			mytsk.back().realspace = tsk->realspace;
			mytsk.back().realspacemin = tsk->realspacemin;
			mytsk.back().realspacemax = tsk->realspacemax;
			mytsk.back().candid = tsk->candid;
		}

		//identify which iontype kd-subtrees we should at all attempt to probe as targets across all regions
		//we need to probe only the _org trees
		set<int> relevant_trees;
		for( int thr = MASTER; thr < nt; thr++ ) {
			threadmemory* thisone = sp.db.at(thr);
			int nitypes = thisone->ionpp1_kdtree_org.size(); //int suffices, only at most 256 itypes!
			for( int ii = 0; ii < nitypes; ii++ ) {
				if ( thisone->ionpp1_kdtree_org.at(ii) != NULL ) { //for a sub-tree to be relevant it first of all needs to contain ions
					//but also it should handle an iontype which is part of an analysis task
					for( auto mtsk = mytsk.begin(); mtsk != mytsk.end(); mtsk++ ) {
						for( int jj = 0; jj < TARGET_LIST_SIZE; jj++ ) {
							unsigned char current_ityp = mtsk->targets[jj];
							if ( current_ityp != 0xFF ) {
								if ( static_cast<int>(current_ityp) != ii ) {
									continue;
								}
								relevant_trees.insert( ii );
								break; //we break because we scan for ityp ii only!
							}
						} //next target to test if of ityp ii
					} //next phase candidate to test delivering potentially different itypes to query for
				}
			} //next type to test
		} //next thread region to test

		//actual ion scanning
		size_t nivals = 1+static_cast<size_t>((int) maximum_iontype_uc); //MK::do forget that we have also the zero-th type so we need 1+nival accessors for subkdtrees

//##MK::DEBUG
//#pragma omp critical
//{
//	cout << "Rank " << get_myrank() << " thread " << mt << " nivals " << nivals << "\n";
//}

		//barrier here is necessary because all threads have to be initialized and their settings configured
		#pragma omp barrier

		//now execute cooperative processing of material points by thread team
		#pragma omp for schedule(dynamic,1) nowait
		for( size_t i = 0; i < mp2me.size(); i++ ) { //threads distribute materials point dynamically
			int mpid = mp2me.at(i); //i is a process-local material point ID, mpid is a world-global material point ID !
			mp3d thisone = mp.matpoints.at(mpid);
			apt_real rlargestsqr = SQR(thisone.R);

			p3d here = p3d( thisone.x, thisone.y, thisone.z ); //a material point, i.e. location in the volume, NOT an ion!
			//p3dm1 probesite = p3dm1( thisone.x, thisone.y, thisone.z, 0 );

//threaded-searches for ions within spherical ROI and iontypes referred to in relevant_trees
			vector<p3d> ions_xyz;
			vector<iontype_ival> ions_ityp_block = vector<iontype_ival>( nivals, iontype_ival(0, 0) ); //build an array of neighbors within largest ROI
			size_t prev = 0;
			size_t next = 0;

			for( auto tt = relevant_trees.begin(); tt != relevant_trees.end(); tt++ ) { //its a set so implicitly sorted in ascending order!
//cout << "\t\t\t\t\ttt " << *tt << "\n";
				//scan all the relevant sub-trees
				prev = ions_xyz.size();

				//query all ions of ityp tt within a ROI at position here using ityp-subtree O(lgN_ityp)
				for( int thr = MASTER; thr < nt; thr++ ) {
					threadmemory* currentregion = sp.db.at(thr);
					if ( currentregion != NULL ) {
						kd_tree* thistree = currentregion->ityp_kdtree_org.at(*tt);
						//always external queries because matpoint against ions, not ions against other ions like in paraprobe-spatstat
						if ( thistree != NULL ) {
							vector<p3d> const & ppp = *(currentregion->ionpp1_kdtree_org.at(*tt));
							thistree->range_rball_append_external( here, ppp, rlargestsqr, ions_xyz );
						}
					}
				} //next threadregion

				next = ions_xyz.size();
				ions_ityp_block.at(*tt) = iontype_ival( prev, next );
			} //query next relevant itype, leave all others ityp_block values at 0, 0 indicating nothing to read from ions_xyz for these ityps

//thread-local processing of neighbors
			//walk through all PhaseCandidates with the querying results
			for( auto mtsk = mytsk.begin(); mtsk != mytsk.end(); mtsk++ ) {
				//do we have within the current ROI at all ions of at least one target ityp for the task?
				vector<p3d> myneighbors;
				for( int tg = 0; tg < TARGET_LIST_SIZE; tg++ ) {
					if ( mtsk->targets[tg] != 0xFF ) {
						int curr_ityp = static_cast<int>(mtsk->targets[tg]);
						for( size_t jj = ions_ityp_block.at(curr_ityp).incl_left; jj < ions_ityp_block.at(curr_ityp).excl_right; jj++ ) {
							myneighbors.push_back( ions_xyz.at(jj) );
						}
					}
				}
				if ( myneighbors.size() > 0 ) { //allocate a thread-local buffer for the temporaries and work on current matpoint
					araullo_res* hdl = NULL;
					hdl = new araullo_res;
					if ( hdl != NULL ) {
						hdl->owner = thrhdl; //MK::pointer to thrhdl not pointer to this class instance!!
						hdl->mp = here;

						hdl->project_cpu( myneighbors, directions );
						//#pragma omp critical
						//{
						//	cout << "Rank " << get_myrank() << " thread " << mt << " mpid " << mpid << " hdl->SDMU32.size() " << hdl->SDMHistoTblU32.size() << "\n";
						//}
						hdl->fft_cpu();
						//#pragma omp critical
						//{
						//	cout << "Rank " << get_myrank() << " thread " << mt << " mpid " << mpid << " hdl->SDMF32.size() " << hdl->SDMHistoTblF32.size() << "\n";
						//}
						hdl->findpks_cpu( mtsk->realspace );
						//#pragma omp critical
						//{
						//	cout << "Rank " << get_myrank() << " thread " << mt << " mpid " << mpid << " hdl->ThreePeaks.size() " << hdl->ThreePeaksTbl.size() << "\n";
						//}

						thrhdl->res.push_back( araullo_res_node() );
						thrhdl->res.back().dat = hdl;
						thrhdl->res.back().mpid = mpid;
						thrhdl->res.back().phcandid = mtsk->candid;
					}
					else {
						#pragma omp critical
						{
							cerr << "Rank " << get_myrank() << " thread " << mt << " mpid " << mpid << " was unable to allocate araullo_res object, hdl == NULL!" << "\n";
						}
						thrhdl->res.push_back( araullo_res_node() );
						thrhdl->res.back().dat = NULL;
						thrhdl->res.back().mpid = mpid;
						thrhdl->res.back().phcandid = mtsk->candid;
					}
				}
				else {
					thrhdl->res.push_back( araullo_res_node() );
					thrhdl->res.back().dat = NULL;
					thrhdl->res.back().mpid = mpid;
					thrhdl->res.back().phcandid = mtsk->candid;
				}
			} //next task at the point
		} //next sampling point

		//#pragma omp barrier
		//#pragma omp critical
		//{
		//	cout << "Rank " << myrk << " Thread " << mt << " finished atom probe crystallography took " << (mytoc-mytic) << " seconds" << endl;
		//}
	} //end of parallel region

	double toc = MPI_Wtime();
	memsnapshot mm = araullo_tictoc.get_memoryconsumption();
	araullo_tictoc.prof_elpsdtime_and_mem( "ExtractCrystallographyAraullo", APT_XX, APT_IS_PAR, mm, tic, toc);
	cout << "Conducted multithreaded AtomProbeCrystallography analysis in " << (toc-tic) << " seconds" << "\n";
}
*/


void araulloHdl::execute_local_workpackage2()
{
	double tic = MPI_Wtime();

	if ( mp2me.size() == 0) {
		cout << "WARNING:: Rank " << get_myrank() << " has no work to do!" << "\n"; return;
	}

	//initialize one GPU, ##MK::for TALOS the two NVIDIA V100 GPUs per node which I, as get_myrank() am currently running
	int n_gpus = 0; //0 - no gpus, 1 or 2 gpus
	int n_mygpus = 0; //either 0, or 1, because a rank either has an assisting gpu and can use it or uses a multi-threaded fallback
	int devId = -1;
	int devCount = 0;

#ifdef UTILIZE_GPUS
	if ( ConfigAraullo::GPUsPerNode > 0 ) {
		devId = -1;
		devCount = acc_get_num_devices(acc_device_nvidia);
		if ( devCount > 0  ) {
			n_gpus = devCount;
			n_mygpus = 1;
			cout << "Rank " << get_myrank() << " NVIDIA accelerator GPU initialization devCount " << devCount << "\n";
		}
		else {
			n_mygpus = 0;
			cout << "Rank " << get_myrank() << " no accelerators existent " << devCount << "\n";
		}
	}
	//##MK::not necessarily portable, here system-specific accelerator initialization exemplified
	//for the MPCDF system TALOS, specifically its computing nodes with 2x20core + 2xNVIDIA V100 accelerators/GPGPUs/"GPUs"
	if ( n_gpus > 0 ) {
		//MK::assume user has instructed model with two processes per node each getting one GPUs
		devId = get_myrank() % ConfigAraullo::GPUsPerNode;
		//cout << "Rank " << get_myrank() << " my GPU devID is " << devId << "\n";
		// creates a context on the GPUs
		//excludes initialization time from computations
		acc_set_device_num(devId, acc_device_nvidia);
		acc_init(acc_device_nvidia);
cout << "Rank " << get_myrank() << " running on GPU with device ID " << devId << "\n";
	}
	else {
		cerr << "Rank " << get_myrank() << " will use multi-threaded CPU fallback n_gpus " << n_gpus << " n_mygpus " << n_mygpus << "\n";
	}
#endif
	//##cout <<  report use fallback multithreaded CPU only
	cout << "Rank " << get_myrank() << " GPU and CPU initialized now processing... n_gpus " << n_gpus << " n_mygpus " << n_mygpus << "\n";


	//identify which ion types we need to probe at all as they are targets in a PhaseCandidate
	//all other ions are immediately ignored thereby improving on ityp_specific ion querying
	//important is that material point positions in contrast to ions in paraprobe-spatstat mostly, except
	//for some seldom by chance events lay exactly at the positions of ions, therefore we only need to query
	//externally wrt to the kdtrees

	#pragma omp parallel shared(n_gpus,n_mygpus,devId)
	{
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();
		//mkl_set_num_threads_local(1);

		#pragma omp master
		{
			workers = vector<acquisor*>( nt, NULL );
		}
		//necessary as otherwise write back conflict on workers from threads
		#pragma omp barrier

		//setup thread-local worker and use thread-local memory
		acquisor* thrhdl = NULL;
		try {
			thrhdl = new acquisor;
		}
		catch (bad_alloc &mecroak) {
			#pragma omp critical
			{
				cerr << "Rank " << get_myrank() << " thread " << mt << " unable to allocate an acquisor instance!" << "\n";
			}
		}

		//now workers backreference holders can safely be modified by current thread
		//MK::pointer will point to thread-local memory
		#pragma omp critical
		{
			workers.at(mt) = thrhdl;
		}

		thrhdl->configure();

		apt_real R = thrhdl->cfg.R; //could be in the future modified to have different radii per material point
		apt_real RSQR = SQR(R);
		//##MK::how is it assured that workers is always !NULL ?

		//make thread-local memory copies of the phases to reduce cache traffic
		vector<PhaseCandidate> mytsk;
		for( auto tsk = itsk.iphcands.begin(); tsk != itsk.iphcands.end(); tsk++ ) {
			mytsk.push_back( PhaseCandidate() );
			for( unsigned int tg = 0; tg < TARGET_LIST_SIZE; tg++ ) {
				mytsk.back().targets[tg] = tsk->targets[tg];
			}
			mytsk.back().realspace = tsk->realspace;
			mytsk.back().realspacemin = tsk->realspacemin;
			mytsk.back().realspacemax = tsk->realspacemax;
			mytsk.back().candid = tsk->candid;
		}

		//identify which iontype kd-subtrees we should at all attempt to probe as targets across all regions
		//we need to probe only the _org trees
		set<int> relevant_trees;
		for( int thr = MASTER; thr < nt; thr++ ) {
			threadmemory* thisone = sp.db.at(thr);
			int nitypes = thisone->ionpp1_kdtree_org.size(); //int suffices, only at most 256 itypes!
			for( int ii = 0; ii < nitypes; ii++ ) {
				if ( thisone->ionpp1_kdtree_org.at(ii) != NULL ) { //for a sub-tree to be relevant it first of all needs to contain ions
					//but also it should handle an iontype which is part of an analysis task
					for( auto mtsk = mytsk.begin(); mtsk != mytsk.end(); mtsk++ ) {
						for( int jj = 0; jj < TARGET_LIST_SIZE; jj++ ) {
							unsigned char current_ityp = mtsk->targets[jj];
							if ( current_ityp != 0xFF ) {
								if ( static_cast<int>(current_ityp) != ii ) {
									continue;
								}
								relevant_trees.insert( ii );
								break; //we break because we scan for ityp ii only!
							}
						} //next target to test if of ityp ii
					} //next phase candidate to test delivering potentially different itypes to query for
				}
			} //next type to test
		} //next thread region to test

		//actual ion scanning
		size_t nivals = 1+static_cast<size_t>((int) maximum_iontype_uc); //MK::do forget that we have also the zero-th type so we need 1+nival accessors for subkdtrees

//##MK::DEBUG
/*
#pragma omp critical
{
	cout << "Rank " << get_myrank() << " thread " << mt << " nivals " << nivals << "\n";
}
*/

		//barrier here is necessary because all threads have to be initialized and their settings configured
		#pragma omp barrier

		//now execute cooperative processing of material points by thread team
		#pragma omp for schedule(dynamic,1) nowait
		for( size_t i = 0; i < mp2me.size(); i++ ) { //threads distribute materials point dynamically
			int mpid = mp2me.at(i); //i is a process-local material point ID, mpid is a world-global material point ID !
			mp3d thisone = mp.matpoints.at(mpid);
			apt_real rlargestsqr = SQR(thisone.R);

			p3d here = p3d( thisone.x, thisone.y, thisone.z ); //a material point, i.e. location in the volume, NOT an ion!
			//p3dm1 probesite = p3dm1( thisone.x, thisone.y, thisone.z, 0 );

//threaded-searches for ions within spherical ROI and iontypes referred to in relevant_trees
			vector<p3d> ions_xyz;
			vector<iontype_ival> ions_ityp_block = vector<iontype_ival>( nivals, iontype_ival(0, 0) ); //build an array of neighbors within largest ROI
			size_t prev = 0;
			size_t next = 0;

			for( auto tt = relevant_trees.begin(); tt != relevant_trees.end(); tt++ ) { //its a set so implicitly sorted in ascending order!
//cout << "\t\t\t\t\ttt " << *tt << "\n";
				//scan all the relevant sub-trees
				prev = ions_xyz.size();

				//query all ions of ityp tt within a ROI at position here using ityp-subtree O(lgN_ityp)
				for( int thr = MASTER; thr < nt; thr++ ) {
					threadmemory* currentregion = sp.db.at(thr);
					if ( currentregion != NULL ) {
						kd_tree* thistree = currentregion->ityp_kdtree_org.at(*tt);
						//always external queries because matpoint against ions, not ions against other ions like in paraprobe-spatstat
						if ( thistree != NULL ) {
							vector<p3d> const & ppp = *(currentregion->ionpp1_kdtree_org.at(*tt));
							thistree->range_rball_append_external( here, ppp, rlargestsqr, ions_xyz );
						}
					}
				} //next threadregion

				next = ions_xyz.size();
				ions_ityp_block.at(*tt) = iontype_ival( prev, next );
			} //query next relevant itype, leave all others ityp_block values at 0, 0 indicating nothing to read from ions_xyz for these ityps

//thread-local processing of neighbors
			//walk through all PhaseCandidates with the querying results
			for( auto mtsk = mytsk.begin(); mtsk != mytsk.end(); mtsk++ ) {
				//do we have within the current ROI at all ions of at least one target ityp for the task?

				vector<p3d> myneighbors;
				for( int tg = 0; tg < TARGET_LIST_SIZE; tg++ ) {
					if ( mtsk->targets[tg] != 0xFF ) {
						int curr_ityp = static_cast<int>(mtsk->targets[tg]);
						for( size_t jj = ions_ityp_block.at(curr_ityp).incl_left; jj < ions_ityp_block.at(curr_ityp).excl_right; jj++ ) {
							myneighbors.push_back( ions_xyz.at(jj) );
						}
					}
				}
				if ( myneighbors.size() > 0 ) { //allocate a thread-local buffer for the temporaries and work on current matpoint
					araullo_res* hdl = NULL;
					hdl = new araullo_res;
					if ( hdl != NULL ) {
						hdl->owner = thrhdl; //MK::pointer to thrhdl not pointer to this class instance!!
						hdl->mp = here;
#ifdef UTILIZE_GPUS
						if ( (n_mygpus == 1 && mt != MASTER) || (n_mygpus == 0) ) {
							//in-case there is a GPU but I am slave thread, I do regular processing
							//or in case there are no thread and therefore use a multi-thread fallback

							hdl->project_cpu( myneighbors, directions );

							hdl->fft_cpu();

							hdl->findpks_cpu( mtsk->realspacemin, mtsk->realspacemax ); //mtsk->realspace );
						}
						else { //will only be entered if n_mygpus != 0 ##MK::and provided above setting of n_mygpus to 1 when mt == MASTER

							hdl->project_gpu2( myneighbors, directions );

							hdl->fft_gpu(); //##MK::parallelize and switch to fft_gpu

							hdl->findpks_cpu( mtsk->realspacemin, mtsk->realspacemax ); //mtsk->realspace ); //##MK::parallelize GPU and switch to findpks_gpu
						}
#else
						hdl->tictoc[TICTOC_IONSINROI] = static_cast<double>(myneighbors.size());
						hdl->tictoc[TICTOC_THREADID] = static_cast<double>(mt);

						hdl->project_cpu( myneighbors, directions );

						hdl->fft_cpu();

						hdl->findpks_cpu( mtsk->realspacemin, mtsk->realspacemax ); //mtsk->realspace );

						hdl->dbscan_cpu( directions );
#endif

						thrhdl->res.push_back( araullo_res_node() );
						thrhdl->res.back().dat = hdl;
						thrhdl->res.back().mpid = mpid;
						thrhdl->res.back().phcandid = mtsk->candid;
					}
					else {
						#pragma omp critical
						{
							cerr << "Rank " << get_myrank() << " thread " << mt << " mpid " << mpid << " was unable to allocate araullo_res object, hdl == NULL!" << "\n";
						}
						/*
						thrhdl->res.push_back( araullo_res_node() );
						thrhdl->res.back().dat = NULL;
						thrhdl->res.back().mpid = mpid; //world-global material point ID
						thrhdl->res.back().phcandid = mtsk->candid; //it is possible that for certain material points results are not available for each phase
						*/
					}
				}
				/*
				else {
					thrhdl->res.push_back( araullo_res_node() );
					thrhdl->res.back().dat = NULL;
					thrhdl->res.back().mpid = mpid;
					thrhdl->res.back().phcandid = mtsk->candid;
				}*/ //no results are stored for unallocateable guys
			} //next task at the point
		} //next sampling point

		//#pragma omp barrier
		/*#pragma omp critical
		{
			cout << "Rank " << myrk << " Thread " << mt << " finished atom probe crystallography took " << (mytoc-mytic) << " seconds" << endl;
		}*/
	} //end of parallel region

	double toc = MPI_Wtime();
	memsnapshot mm = araullo_tictoc.get_memoryconsumption();
	araullo_tictoc.prof_elpsdtime_and_mem( "ExtractCrystallographyAraullo", APT_XX, APT_IS_PAR, mm, tic, toc);

cout << "Rank " << get_myrank() << " multi-threaded AtomProbeCrystallography analysis in " << (toc-tic) << " seconds" << "\n";
}


bool araulloHdl::init_target_file()
{
	//##MK::generate group hierarchy of an open source IFES APTTC APTH5 HDF5 file
	double tic = MPI_Wtime();

	string h5fn_out = "PARAPROBE.Araullo.Results.SimID." + to_string(ConfigShared::SimID) + ".h5";

	debugh5Hdl.h5resultsfn = h5fn_out;

	//only master instantiates the file
	if ( get_myrank() == MASTER ) {
cout << "Rank " << MASTER << " initializing target file " << h5fn_out << "\n";

		if ( debugh5Hdl.create_araullo_apth5( h5fn_out ) == WRAPPED_HDF5_SUCCESS ) {
			//instantiate containers for PhaseCandidate specific results
			string fwslash = "/";
			string grpnm = "";
			int status = WRAPPED_HDF5_SUCCESS;
			if ( ConfigAraullo::IOSpecificPeaks == true ) {
				for( auto mtsk = itsk.iphcands.begin(); mtsk != itsk.iphcands.end(); mtsk++ ) {
					if ( ConfigAraullo::IODBScanOnSpecificPeaks == false ) { //##MK::currently an either or solution
						grpnm = PARAPROBE_ARAULLO_RES_SPECPEAK + fwslash + "PH" + to_string(mtsk->candid);
						status = debugh5Hdl.create_group( grpnm );
						if ( status != WRAPPED_HDF5_SUCCESS ) { cerr << grpnm << " create failed! " << status << "\n"; return false; }
						cout << grpnm << " create " << status << "\n";
					}
					else {
					//if ( ConfigAraullo::IODBScanOnSpecificPeaks == true ) {
						grpnm = PARAPROBE_ARAULLO_RES_DBSCAN + fwslash + "PH" + to_string(mtsk->candid);
						status = debugh5Hdl.create_group( grpnm );
						if ( status != WRAPPED_HDF5_SUCCESS ) { cerr << grpnm << " create failed! " << status << "\n"; return false; }
						cout << grpnm << " create " << status << "\n";
					}
				}
			}
			if ( ConfigAraullo::IOKeyQuants == true ) {
				for( auto mtsk = itsk.iphcands.begin(); mtsk != itsk.iphcands.end(); mtsk++ ) {
					grpnm = PARAPROBE_ARAULLO_RES_KEYQUANT + fwslash + "PH" + to_string(mtsk->candid);
					status = debugh5Hdl.create_group( grpnm );
					if ( status != WRAPPED_HDF5_SUCCESS ) { cerr << grpnm << " create failed! " << status << "\n"; return false; }
					cout << grpnm << " create " << status << "\n";
				}
			}
			if ( ConfigAraullo::IOStrongPeaks == true ) {
				for( auto mtsk = itsk.iphcands.begin(); mtsk != itsk.iphcands.end(); mtsk++ ) {
					grpnm = PARAPROBE_ARAULLO_RES_THREEPEAK + fwslash + "PH" + to_string(mtsk->candid);
					status = debugh5Hdl.create_group( grpnm );
					if ( status != WRAPPED_HDF5_SUCCESS ) { cerr << grpnm << " create failed! " << status << "\n"; return false; }
					cout << grpnm << " create " << status << "\n";
				}
			}
			if ( ConfigAraullo::IOHistoCnts == true ) {
				for( auto mtsk = itsk.iphcands.begin(); mtsk != itsk.iphcands.end(); mtsk++ ) {
					grpnm = PARAPROBE_ARAULLO_RES_HISTO_CNTS + fwslash + "PH" + to_string(mtsk->candid);
					status = debugh5Hdl.create_group( grpnm );
					if ( status != WRAPPED_HDF5_SUCCESS ) { cerr << grpnm << " create failed! " << status << "\n"; return false; }
					cout << grpnm << " create " << status << "\n";
				}
			}
			if ( ConfigAraullo::IOHistoFFTMagn == true ) {
				for( auto mtsk = itsk.iphcands.begin(); mtsk != itsk.iphcands.end(); mtsk++ ) {
					grpnm = PARAPROBE_ARAULLO_RES_HISTO_MAGN + fwslash + "PH" + to_string(mtsk->candid);
					status = debugh5Hdl.create_group( grpnm );
					if ( status != WRAPPED_HDF5_SUCCESS ) { cerr << grpnm << " create failed! " << status << "\n"; return false; }
					cout << grpnm << " create " << status << "\n";
				}
			}
			//for the overview list for which material point and we have results for phase ph
			grpnm = PARAPROBE_ARAULLO_RES_MP_IOID;
			status = debugh5Hdl.create_group( grpnm );
			if ( status != WRAPPED_HDF5_SUCCESS ) { cerr << grpnm << " create failed! " << status << "\n"; return false; }
			cout << grpnm << " create " << status << "\n";

			double toc = MPI_Wtime();
			memsnapshot mm = memsnapshot();
			araullo_tictoc.prof_elpsdtime_and_mem( "InitTargetResultsH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
			return true;
		}
		else {
			return false;
		}
	}
	else { //slaves just fill the names
		double toc = MPI_Wtime();
		memsnapshot mm = memsnapshot();
		araullo_tictoc.prof_elpsdtime_and_mem( "InitTargetResultsH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
		return true;
	}
}


bool araulloHdl::write_environment_and_settings()
{
	vector<pparm> hardware = araullo_tictoc.report_machine();
	string prfx = PARAPROBE_ARAULLO_META_HRDWR; //PARAPROBE_UTILS_HRDWR_META_KEYS;
	for( auto it = hardware.begin(); it != hardware.end(); it++ ) {
		cout << it->keyword << "__" << it->value << "__" << it->unit << "__" << it->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *it ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing hardware setting " << it->keyword << " failed!" << "\n";
			return false;
		}
	}
	cout << "Rank " << MASTER << " hardware settings written to H5" << "\n";

	vector<pparm> software;
	ConfigAraullo::reportSettings( software );
	prfx = PARAPROBE_ARAULLO_META_SFTWR; //PARAPROBE_UTILS_SFTWR_META_KEYS;
	for( auto jt = software.begin(); jt != software.end(); jt++ ) {
		cout << jt->keyword << "__" << jt->value << "__" << jt->unit << "__" << jt->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *jt ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing software tool setting " << jt->keyword << " failed!" << "\n";
			return false;
		}
	}
	cout << "Rank " << MASTER << " software settings written to H5" << "\n";

	//add LatticeIontypeCombinations using the same prfx
	for( auto kt = itsk.iifo.begin(); kt != itsk.iifo.end(); kt++ ) {
		cout << kt->keyword << "__" << kt->value << "__" << kt->unit << "__" << kt->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *kt ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing software tool setting " << kt->keyword << " failed!" << "\n";
			return false;
		}
	}
	return true;
}


bool araulloHdl::write_materialpoints_to_apth5()
{
	double tic = MPI_Wtime();

	if ( workers.size() == 0 ) {
		cerr << "Rank " << get_myrank() << " workers is empty!" << "\n"; return false;
	}
	if ( workers.at(0) == NULL ) {
		cerr << "Rank " << get_myrank() << " MASTER worker does not exist!" << "\n"; return false;
	}

	string fwslash = "/";

	size_t nso2 = ConfigAraullo::NumberOfSO2Directions;
	if ( directions.size() != nso2 ) {
		cerr << "Rank " << get_myrank() << " directions.size() is unexpectedly different!" << "\n"; return false;
	}

	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";

	//elevation, azimuth
	dsnm = PARAPROBE_ARAULLO_META_DIR_ELAZ;
	vector<float> f32 = vector<float>( nso2 * PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX, 0.f );
	for( size_t ea = 0; ea < nso2; ea++ ) {
		f32[ea*PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX+0] = directions[ea].elevation;
		f32[ea*PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX+1] = directions[ea].azimuth;
	}
	ifo = h5iometa( dsnm, f32.size()/PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX, PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create DIR_ELAZ metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, f32.size()/PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX, 0, PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX,
			f32.size()/PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX, PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store DIR_ELAZ metadata! " << status << "\n"; return false;
	}

	//x,y,z
	f32 = vector<float>( nso2 * PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX, 0.f );
	dsnm = PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ; //direction vectors for visualization
	for( size_t ea = 0; ea < nso2; ea++ ) {
		//new:geographic definition active rotation about azimuth counter-clockwise with az = 0 beginning at x axis, right handed CS
		//new:elevation from xy plane upwards positive
		float e = directions[ea].elevation;
		float a = directions[ea].azimuth;
		f32[ea*PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX+0] = cos(a) * cos(e);
		f32[ea*PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX+1] = sin(a) * cos(e);
		f32[ea*PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX+2] = sin(e);
	}
	ifo = h5iometa( dsnm, f32.size()/PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX, PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create DIR_XYZ metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, f32.size()/PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX, 0, PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX,
			f32.size()/PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX, PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store DIR_XYZ metadata! " << status << "\n"; return false;
	}
	f32 = vector<float>();

	//XDMF topo
	vector<unsigned int> u32 = vector<unsigned int>( 3*directions.size(), 1 ); //MK::we need to store a triplet of XDMF topology type count and vertex ID
	dsnm = PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO;
	for ( size_t i = 0; i < directions.size(); i++ ) {  //XDMF type key 1, how many 1 because vertices
		u32.at(i*3+2) = i;
	}
	ifo = h5iometa( dsnm, u32.size()/PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO_NCMAX, PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create DIR_TOPO metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, u32.size()/PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO_NCMAX, 0, PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO_NCMAX,
			u32.size()/PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO_NCMAX, PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store DIR_TOPO metadata! " << status << "\n"; return false;
	}
	u32 = vector<unsigned int>();

	//material point grid xyz
	f32 = vector<float>( PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX*mp.matpoints.size(), 0.f ); //MK::we need to store a triplet of XDMF topology type count and vertex ID
	dsnm = PARAPROBE_ARAULLO_META_MP_XYZ;
	for ( size_t i = 0; i < mp.matpoints.size(); i++ ) {
		f32.at(i*PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX+0) = mp.matpoints[i].x;
		f32.at(i*PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX+1) = mp.matpoints[i].y;
		f32.at(i*PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX+2) = mp.matpoints[i].z;
	}
	ifo = h5iometa( dsnm, f32.size()/PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX, PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create MP_XYZ metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, f32.size()/PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX, 0, PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX,
			f32.size()/PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX, PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX );
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store MP_XYZ metadata! " << status << "\n"; return false;
	}
	f32 = vector<float>( PARAPROBE_ARAULLO_META_MP_R_NCMAX*mp.matpoints.size(), 0.f ); //MK::we need to store a triplet of XDMF topology type count and vertex ID
	dsnm = PARAPROBE_ARAULLO_META_MP_R;
	for ( size_t i = 0; i < mp.matpoints.size(); i++ ) {
		f32.at(i*PARAPROBE_ARAULLO_META_MP_R_NCMAX+0) = mp.matpoints[i].R;
	}
	ifo = h5iometa( dsnm, f32.size()/PARAPROBE_ARAULLO_META_MP_R_NCMAX, PARAPROBE_ARAULLO_META_MP_R_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create MP_R metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, f32.size()/PARAPROBE_ARAULLO_META_MP_R_NCMAX, 0, PARAPROBE_ARAULLO_META_MP_R_NCMAX,
			f32.size()/PARAPROBE_ARAULLO_META_MP_R_NCMAX, PARAPROBE_ARAULLO_META_MP_R_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store MP_R metadata! " << status << "\n"; return false;
	}
	f32 = vector<float>();

	//material point ID
	u32 = vector<unsigned int>( PARAPROBE_ARAULLO_META_MP_ID_NCMAX*mp.matpoints.size(), 0 );
	dsnm = PARAPROBE_ARAULLO_META_MP_ID;
	for ( size_t i = 0; i < mp.matpoints.size(); i++ ) {
		u32.at(i*PARAPROBE_ARAULLO_META_MP_ID_NCMAX+0) = mp.matpoints[i].mpid;
	}
	ifo = h5iometa( dsnm, u32.size()/PARAPROBE_ARAULLO_META_MP_ID_NCMAX, PARAPROBE_ARAULLO_META_MP_ID_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create MP_ID metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, u32.size()/PARAPROBE_ARAULLO_META_MP_ID_NCMAX, 0, PARAPROBE_ARAULLO_META_MP_ID_NCMAX,
			u32.size()/PARAPROBE_ARAULLO_META_MP_ID_NCMAX, PARAPROBE_ARAULLO_META_MP_ID_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store MP_ID metadata! " << status << "\n"; return false;
	}

	//key quantiles
	if ( ConfigAraullo::IOKeyQuants == true ) {
		f32 = vector<float>( ConfigAraullo::KeyQuantiles.size()*PARAPROBE_ARAULLO_META_KEYQUANT_QNT_NCMAX, 0.f ); //##MK::the same for all phases
		dsnm = PARAPROBE_ARAULLO_META_KEYQUANT_QNT;
		for ( size_t i = 0; i < ConfigAraullo::KeyQuantiles.size(); i++ ) {
			f32.at(i) = ConfigAraullo::KeyQuantiles.at(i);
		}
		ifo = h5iometa( dsnm, f32.size()/PARAPROBE_ARAULLO_META_KEYQUANT_QNT_NCMAX, PARAPROBE_ARAULLO_META_KEYQUANT_QNT_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " failed to create PARAPROBE_ARAULLO_META_KEYQUANT_QNT_NCMAX metadata! " << status << "\n"; return false;
		}
		offs = h5offsets( 0, f32.size()/PARAPROBE_ARAULLO_META_KEYQUANT_QNT_NCMAX, 0, PARAPROBE_ARAULLO_META_KEYQUANT_QNT_NCMAX,
				f32.size()/PARAPROBE_ARAULLO_META_KEYQUANT_QNT_NCMAX, PARAPROBE_ARAULLO_META_KEYQUANT_QNT_NCMAX );
		status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " failed to store PARAPROBE_ARAULLO_META_KEYQUANT_QNT_NCMAX metadata! " << status << "\n"; return false;
		}
		f32 = vector<float>();
	}

	//XDMF topo
	u32 = vector<unsigned int>( 3*mp.matpoints.size(), 1 ); //MK::we need to store a triplet of XDMF topology type count and vertex ID
	dsnm = PARAPROBE_ARAULLO_META_MP_TOPO;
	for ( size_t i = 0; i < mp.matpoints.size(); i++ ) {
		u32.at(3*i+2) = i;
	}
	ifo = h5iometa( dsnm, u32.size()/PARAPROBE_ARAULLO_META_MP_TOPO_NCMAX, PARAPROBE_ARAULLO_META_MP_TOPO_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create MP_TOPO metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, u32.size()/PARAPROBE_ARAULLO_META_MP_TOPO_NCMAX, 0, PARAPROBE_ARAULLO_META_MP_TOPO_NCMAX,
			u32.size()/PARAPROBE_ARAULLO_META_MP_TOPO_NCMAX, PARAPROBE_ARAULLO_META_MP_TOPO_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store MP_TOPO metadata! " << status << "\n"; return false;
	}
	u32 = vector<unsigned int>();

	//phases
	u32.push_back( itsk.iphcands.size() );
	dsnm = PARAPROBE_ARAULLO_META_PHCAND_N;
	ifo = h5iometa( dsnm, u32.size()/PARAPROBE_ARAULLO_META_PHCAND_N_NCMAX, PARAPROBE_ARAULLO_META_PHCAND_N_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " PARAPROBE_ARAULLO_META_PHCAND_N create failed! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, u32.size()/PARAPROBE_ARAULLO_META_PHCAND_N_NCMAX, 0, PARAPROBE_ARAULLO_META_PHCAND_N_NCMAX,
				u32.size()/PARAPROBE_ARAULLO_META_PHCAND_N_NCMAX, PARAPROBE_ARAULLO_META_PHCAND_N_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " PARAPROBE_ARAULLO_META_PHCAND_N write failed! " << status << "\n"; return false;
	}
	u32 = vector<unsigned int>();

	for( auto mtsk = itsk.iphcands.begin(); mtsk != itsk.iphcands.end(); mtsk++ ) {
		string grpnm = PARAPROBE_ARAULLO_META_PHCAND + fwslash + "PH" + to_string(mtsk->candid);
		status = debugh5Hdl.create_group( grpnm );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "Rank " << get_myrank() << " grpnm " << grpnm << " create failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " grpnm " << grpnm << " create " << status << "\n";

		vector<unsigned char> u8_id;
		vector<unsigned char> u8_wh;
		for( int tg = 0; tg < TARGET_LIST_SIZE; tg++ ) {
			unsigned char current_ityp = mtsk->targets[tg];
			if ( current_ityp != 0xFF ) {
				u8_id.push_back( current_ityp );
				evapion3 tmp = rng.iontypes.at(static_cast<int>(current_ityp)).strct;
				u8_wh.push_back( tmp.Z1 );
				u8_wh.push_back( tmp.N1 );
				u8_wh.push_back( tmp.Z2 );
				u8_wh.push_back( tmp.N2 );
				u8_wh.push_back( tmp.Z3 );
				u8_wh.push_back( tmp.N3 );
				u8_wh.push_back( tmp.sign );
				u8_wh.push_back( tmp.charge );
			}
		}
		vector<float> f32_rsp;
		f32_rsp.push_back( mtsk->realspace );
		f32_rsp.push_back( mtsk->realspacemin );
		f32_rsp.push_back( mtsk->realspacemax );

		dsnm = grpnm + fwslash + PARAPROBE_ARAULLO_META_PHCAND_ID;
		ifo = h5iometa( dsnm, u8_id.size()/PARAPROBE_ARAULLO_META_PHCAND_ID_NCMAX, PARAPROBE_ARAULLO_META_PHCAND_ID_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " create failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " create " << status << "\n";
		offs = h5offsets( 0, u8_id.size()/PARAPROBE_ARAULLO_META_PHCAND_ID_NCMAX, 0, PARAPROBE_ARAULLO_META_PHCAND_ID_NCMAX,
						u8_id.size()/PARAPROBE_ARAULLO_META_PHCAND_ID_NCMAX, PARAPROBE_ARAULLO_META_PHCAND_ID_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, u8_id );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " write failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " write " << status << "\n";
		u8_id = vector<unsigned char>();

		dsnm = grpnm + fwslash + PARAPROBE_ARAULLO_META_PHCAND_WHAT;
		ifo = h5iometa( dsnm, u8_wh.size()/PARAPROBE_ARAULLO_META_PHCAND_WHAT_NCMAX, PARAPROBE_ARAULLO_META_PHCAND_WHAT_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " create failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " create " << status << "\n";
		offs = h5offsets( 0, u8_wh.size()/PARAPROBE_ARAULLO_META_PHCAND_WHAT_NCMAX, 0, PARAPROBE_ARAULLO_META_PHCAND_WHAT_NCMAX,
						u8_wh.size()/PARAPROBE_ARAULLO_META_PHCAND_WHAT_NCMAX, PARAPROBE_ARAULLO_META_PHCAND_WHAT_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, u8_wh );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " write failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " write " << status << "\n";
		u8_wh = vector<unsigned char>();

		dsnm = grpnm + fwslash + PARAPROBE_ARAULLO_META_PHCAND_RSP;
		ifo = h5iometa( dsnm, f32_rsp.size()/PARAPROBE_ARAULLO_META_PHCAND_RSP_NCMAX, PARAPROBE_ARAULLO_META_PHCAND_RSP_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " create failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " create " << status << "\n";
		offs = h5offsets( 0, f32_rsp.size()/PARAPROBE_ARAULLO_META_PHCAND_RSP_NCMAX, 0, PARAPROBE_ARAULLO_META_PHCAND_RSP_NCMAX,
						f32_rsp.size()/PARAPROBE_ARAULLO_META_PHCAND_RSP_NCMAX, PARAPROBE_ARAULLO_META_PHCAND_RSP_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32_rsp );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " write failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " write " << status << "\n";
		f32_rsp = vector<float>();

		//instantiate groups for phase-specific results were instantiated at init_targetfile
	}

	string xdmffn = "PARAPROBE.Araullo.Results.SimID." + to_string(ConfigShared::SimID) + ".MatPoints.xdmf";
	debugxdmf.create_materialpoint_file( xdmffn, mp.matpoints.size(), debugh5Hdl.h5resultsfn );

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "WriteMatPointsResultsH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool araulloHdl::write_results_to_apth5()
{
	double tic = MPI_Wtime();

	if ( workers.size() == 0 ) {
		cerr << "Rank " << get_myrank() << " workers is empty!" << "\n"; return true;
	}
	if ( workers.at(0) == NULL ) {
		cerr << "Rank " << get_myrank() << " MASTER worker does not exist!" << "\n"; return true;
	}

	string fwslash = "/";
	size_t nso2 = ConfigAraullo::NumberOfSO2Directions;
	size_t nbins = workers.at(0)->cfg.NumberOfBins;

	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";
	size_t roffset = 0;

	//##MK::DEBUG simple OpenMP profiling per material point
	string tictoc_fn = "PARAPROBE.Araullo.SimID." + to_string(ConfigShared::SimID) + ".Rank." + to_string(get_myrank()) + ".DetailedProfiling.csv";
	ofstream csvlog;
	csvlog.open(tictoc_fn.c_str(), ofstream::out | ofstream::trunc);
	if (csvlog.is_open() == true) {
		//header
		csvlog << "MPID;PHCANDID;IonsInROI;ThreadID;Project;FFT;FindPks;DBScan\n";
		csvlog << ";;;;s;s;s;s\n";
		csvlog << "MPID;PHCANDID;IonsInROI;ThreadID;Project;FFT;FindPks;DBScan\n";
	}
	else {
		cerr << "Unable to write process-local profiling files" << "\n";
	}
	//##MK::DEBUG

	for( size_t thr = 0; thr < workers.size(); thr++ ) {
		acquisor* thrhdl = workers.at(thr);
		if ( thrhdl != NULL ) {
			for( auto it = thrhdl->res.begin(); it != thrhdl->res.end(); it++ ) {

				int mpid = it->mpid;
				int phcid = it->phcandid;
				araullo_res* thisone = it->dat;

//cout << "Rank " << get_myrank() << " mpid/phcid/thisone " << mpid << ";" << phcid << ";" << thisone << "\n";

				//to separate or not the results of different images or use aggregated hyperslabs?
				//++ separation is much more intuitive to read and check for implementation errors, in particular for non experts
				//++ allows for explicit visualization of single material point results
				//-- is less performant because more datasets to create, in particular when doing so using PHDF5
				//##MK::here I opted to use the separated approach
				if ( thisone != NULL ) {

					if ( ConfigAraullo::IOSpecificPeaks == true ) {
//cout << "Rank " << get_myrank() << " IOSpecificPeaks mpid/phcid/thisone " << mpid << ";" << phcid << ";" << thisone << "\t\t" << thisone->SpecificPeaksTbl.size() << "\n";
						if ( ConfigAraullo::IODBScanOnSpecificPeaks == false ) { //##MK::currently an either or solution
							//either we output specific peak signatures completely or only the compressed result from DBScan
							dsnm = PARAPROBE_ARAULLO_RES_SPECPEAK + fwslash + "PH" + to_string(phcid) + fwslash + to_string(mpid);
							//in-place writing of thisone->SpecificPeaksTbl
							ifo = h5iometa( dsnm, thisone->SpecificPeaksTbl.size()/PARAPROBE_ARAULLO_RES_SPECPEAK_NCMAX, PARAPROBE_ARAULLO_RES_SPECPEAK_NCMAX );
							status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
							if ( status == WRAPPED_HDF5_SUCCESS ) {
								offs = h5offsets( 0, thisone->SpecificPeaksTbl.size()/PARAPROBE_ARAULLO_RES_SPECPEAK_NCMAX,
										0, PARAPROBE_ARAULLO_RES_SPECPEAK_NCMAX,
											thisone->SpecificPeaksTbl.size()/PARAPROBE_ARAULLO_RES_SPECPEAK_NCMAX, PARAPROBE_ARAULLO_RES_SPECPEAK_NCMAX);
								status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, thisone->SpecificPeaksTbl );
								if ( status == WRAPPED_HDF5_SUCCESS ) {
	//cout << "Rank " << get_myrank() << " wrote SpecificPeak " << dsnm << " status " << status << "\n";
									thisone->SpecificPeaksTbl = vector<float>();
								}
								else {
									cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to store " << dsnm << " status " << status << "\n"; return false;
								}
							}
							else {
								cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to create " << dsnm << " status " << status << "\n"; return false;
							}
						}
						else {
						//if ( ConfigAraullo::IODBScanOnSpecificPeaks == true ) {
							if ( thisone->DBScanTbl.size() > 0 ) { //report only if at least one reflector was found with DBScan in the signature
								dsnm = PARAPROBE_ARAULLO_RES_DBSCAN + fwslash + "PH" + to_string(phcid) + fwslash + to_string(mpid);
								vector<float> f32 = vector<float>( thisone->DBScanTbl.size()*PARAPROBE_ARAULLO_RES_DBSCAN_NCMAX, 0.f );
								size_t clid = 0;
								for( auto kt = thisone->DBScanTbl.begin(); kt != thisone->DBScanTbl.end(); kt++ ) {
									f32[clid*PARAPROBE_ARAULLO_RES_DBSCAN_NCMAX+0] = kt->xmax;
									f32[clid*PARAPROBE_ARAULLO_RES_DBSCAN_NCMAX+1] = kt->ymax;
									f32[clid*PARAPROBE_ARAULLO_RES_DBSCAN_NCMAX+2] = kt->zmax;
									f32[clid*PARAPROBE_ARAULLO_RES_DBSCAN_NCMAX+3] = kt->xmean;
									f32[clid*PARAPROBE_ARAULLO_RES_DBSCAN_NCMAX+4] = kt->ymean;
									f32[clid*PARAPROBE_ARAULLO_RES_DBSCAN_NCMAX+5] = kt->zmean;
									f32[clid*PARAPROBE_ARAULLO_RES_DBSCAN_NCMAX+6] = kt->MAXIntensity;
									f32[clid*PARAPROBE_ARAULLO_RES_DBSCAN_NCMAX+7] = kt->SUMIntensity;
									f32[clid*PARAPROBE_ARAULLO_RES_DBSCAN_NCMAX+8] = kt->PksCnts;
									clid++;
								}
								ifo = h5iometa( dsnm, f32.size()/PARAPROBE_ARAULLO_RES_DBSCAN_NCMAX, PARAPROBE_ARAULLO_RES_DBSCAN_NCMAX );
								status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
								if ( status == WRAPPED_HDF5_SUCCESS ) {
									offs = h5offsets( 0, f32.size()/PARAPROBE_ARAULLO_RES_DBSCAN_NCMAX,
											0, PARAPROBE_ARAULLO_RES_DBSCAN_NCMAX,
												f32.size()/PARAPROBE_ARAULLO_RES_DBSCAN_NCMAX, PARAPROBE_ARAULLO_RES_DBSCAN_NCMAX);
									status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
									if ( status == WRAPPED_HDF5_SUCCESS ) {
		//cout << "Rank " << get_myrank() << " wrote SpecificPeak " << dsnm << " status " << status << "\n";
										thisone->DBScanTbl = vector<specpeakcluster>();
									}
									else {
										cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to store " << dsnm << " status " << status << "\n"; return false;
									}
								}
								else {
									cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to create " << dsnm << " status " << status << "\n"; return false;
								}
							}
						} //done with DBScan results for specific peaks
					}

					if ( ConfigAraullo::IOKeyQuants == true ) {
						dsnm = PARAPROBE_ARAULLO_RES_KEYQUANT + fwslash + "PH" + to_string(phcid) + fwslash + to_string(mpid);
						//in-place writing of thisone->SpecificPeaksTbl
						ifo = h5iometa( dsnm, thisone->KeyQuantilesTbl.size()/PARAPROBE_ARAULLO_RES_KEYQUANT_NCMAX, PARAPROBE_ARAULLO_RES_KEYQUANT_NCMAX );
						status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
						if ( status == WRAPPED_HDF5_SUCCESS ) {
							offs = h5offsets( 0, thisone->KeyQuantilesTbl.size()/PARAPROBE_ARAULLO_RES_KEYQUANT_NCMAX,
									0, PARAPROBE_ARAULLO_RES_KEYQUANT_NCMAX,
										thisone->KeyQuantilesTbl.size()/PARAPROBE_ARAULLO_RES_KEYQUANT_NCMAX, PARAPROBE_ARAULLO_RES_KEYQUANT_NCMAX );
							status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, thisone->KeyQuantilesTbl );
							if ( status == WRAPPED_HDF5_SUCCESS ) {
//cout << "Rank " << get_myrank() << " wrote SpecificPeak " << dsnm << " status " << status << "\n";
								thisone->KeyQuantilesTbl = vector<float>();
							}
							else {
								cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to store keyquants " << dsnm << " status " << status << "\n"; return false;
							}
						}
						else {
							cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to create keyquants" << dsnm << " status " << status << "\n"; return false;
						}
					}

					if ( ConfigAraullo::IOStrongPeaks == true ) {
//cout << "Rank " << get_myrank() << " IOStrongPeaks mpid/phcid/thisone " << mpid << ";" << phcid << ";" << thisone << "\t\t" << thisone->ThreePeaksTbl.size() << "\n";

						dsnm = PARAPROBE_ARAULLO_RES_THREEPEAK + fwslash + "PH" + to_string(phcid) + fwslash + to_string(mpid);
						//in-place writing of thisone->ThreePeaksTbl
						ifo = h5iometa( dsnm, thisone->ThreePeaksTbl.size()/PARAPROBE_ARAULLO_RES_THREEPEAK_NCMAX, PARAPROBE_ARAULLO_RES_THREEPEAK_NCMAX);
						status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
						if ( status == WRAPPED_HDF5_SUCCESS ) {
							offs = h5offsets( 0, thisone->ThreePeaksTbl.size()/PARAPROBE_ARAULLO_RES_THREEPEAK_NCMAX,
										0, PARAPROBE_ARAULLO_RES_THREEPEAK_NCMAX,
										thisone->ThreePeaksTbl.size()/PARAPROBE_ARAULLO_RES_THREEPEAK_NCMAX, PARAPROBE_ARAULLO_RES_THREEPEAK_NCMAX);
							status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, thisone->ThreePeaksTbl );
							if ( status == WRAPPED_HDF5_SUCCESS ) {
//cout << "Rank " << get_myrank() << " wrote ThreePeaksTbl " << dsnm << " status " << status << "\n";
								thisone->ThreePeaksTbl = vector<float>();
							}
							else {
								cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to store " << dsnm << " status " << status << "\n"; return false;
							}
						}
						else {
							cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to create " << dsnm << " status " << status << "\n"; return false;
						}
					}

					if ( ConfigAraullo::IOHistoCnts == true ) {
//cout << "Rank " << get_myrank() << " IOHistoCnts mpid/phcid/thisone " << mpid << ";" << phcid << ";" << thisone << "\t\t" << thisone->SDMHistoTblU32.size() <<  "\n";

						dsnm = PARAPROBE_ARAULLO_RES_HISTO_CNTS + fwslash + "PH" + to_string(phcid) + fwslash + to_string(mpid);
						//only one PhaseCandidate per result so use SDMHistoTblU32 in-place , histogram count u32
						ifo = h5iometa( dsnm, thisone->SDMHistoTblU32.size()/nbins, nbins);
						status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
						if ( status == WRAPPED_HDF5_SUCCESS ) {
							offs = h5offsets( 0, thisone->SDMHistoTblU32.size()/nbins, 0, nbins,
									thisone->SDMHistoTblU32.size()/nbins, nbins);
							status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, thisone->SDMHistoTblU32 );
							if ( status == WRAPPED_HDF5_SUCCESS ) {
//cout << "Rank " << get_myrank() << " wrote SDMU32 " << dsnm << " status " << status << "\n";
								thisone->SDMHistoTblU32 = vector<unsigned int>();
							}
							else {
								cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to store " << dsnm << " status " << status << "\n"; return false;
							}
						}
						else {
							cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to create " << dsnm << " status " << status << "\n"; return false;
						}
					}
					if ( ConfigAraullo::IOHistoFFTMagn == true ) {
//cout << "Rank " << get_myrank() << " IOHistoFFTMagn mpid/phcid/thisone " << mpid << ";" << phcid << ";" << thisone << "\t\t" << thisone->SDMHistoTblF32.size() << "\n";

						dsnm = PARAPROBE_ARAULLO_RES_HISTO_MAGN + fwslash + "PH" + to_string(phcid) + fwslash + to_string(mpid);
						//only one PhaseCandidate per result so use SDMHistoTblU32 in-place , histogram count f32 containing the fft on the first [0, nbins/2
						ifo = h5iometa( dsnm, thisone->SDMHistoTblF32.size()/nbins, nbins);
						status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
						if ( status == WRAPPED_HDF5_SUCCESS ) {
							offs = h5offsets( 0, thisone->SDMHistoTblF32.size()/nbins, 0, nbins,
							thisone->SDMHistoTblF32.size()/nbins, nbins);
							status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, thisone->SDMHistoTblF32 );
							if ( status == WRAPPED_HDF5_SUCCESS ) {
//cout << "Rank " << get_myrank() << " wrote SDMF32 " << dsnm << " status " << status << "\n";
								thisone->SDMHistoTblF32 = vector<float>();
							}
							else {
								cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to store " << dsnm << " status " << status << "\n"; return false;
							}
						}
						else {
							cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to create " << dsnm << " status " << status << "\n"; return false;
						}
					}

					if ( csvlog.is_open() == true ) {
						csvlog << mpid << ";" << phcid << ";" << thisone->tictoc[TICTOC_IONSINROI] << ";" <<
								thisone->tictoc[TICTOC_THREADID] << ";" << thisone->tictoc[TICTOC_PROJECT] << ";" <<
									thisone->tictoc[TICTOC_FFT] << ";" << thisone->tictoc[TICTOC_FINDPKS] << ";" <<
										thisone->tictoc[TICTOC_DBSCAN] << "\n";
					}
					//MK::it->dat remains != NULL to indicate we have results for this one
				} //done writing all desired and populated fields for a specific material point and phase
				//no else because if no results then there were no results achieved for the material point maybe because there were no or not sufficient ions to support the analysis
			} //next result from this thread
		}
	} //next thread of this rank

	if ( csvlog.is_open() == true ) {
		csvlog.flush();
		csvlog.close();
	}

	//strict I/O policy, all matpoints with their results need to end up in file, otherwise we already went out with a false
	double toc = MPI_Wtime();
	memsnapshot mm = araullo_tictoc.get_memoryconsumption();
	araullo_tictoc.prof_elpsdtime_and_mem( "WriteResultsH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;

	//collect XDMF results
	/*
	if ( get_nranks() == SINGLEPROCESS && get_myrank() == MASTER ) { //##MK::implement communication across processes to identify which results exist
		for( auto mtsk = itsk.iphcands.begin(); mtsk != itsk.iphcands.end(); mtsk++ ) {
			vector<unsigned int> attrnm;
			int current_phcid = mtsk->candid;

			for( size_t thr = 0; thr < workers.size(); thr++ ) {
				acquisor* thrhdl = workers.at(thr);
				if ( thrhdl != NULL ) {
					for( auto it = thrhdl->res.begin(); it != thrhdl->res.end(); it++ ) {
						int mpid = it->mpid;
						int phcid = it->phcandid;
						araullo_res* thisone = it->dat;
						if ( phcid != current_phcid || thisone == NULL ) {
							continue;
						}
						else {
							attrnm.push_back( mpid );
						}
					} //test if results for next guy are there
				} //only valid threadregions
			} //next threadregion

			string xmlfn = "PARAPROBE.Araullo.Results.SimID." + to_string(ConfigShared::SimID) + ".PH" + to_string(current_phcid) + ".xdmf";
			debugxdmf.create_phaseresults_file( xmlfn, ConfigAraullo::NumberOfSO2Directions, debugh5Hdl.h5resultsfn, current_phcid, attrnm );
		} //next candidate
	}
	*/
}


bool araulloHdl::write_shortlist_matpoints_withresults_to_apth5()
{
	double tic = MPI_Wtime();

	//paraprobe-indexer requires that all for each material point all phases are populate
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string fwslash = "/";

	int localhealth = 1;
	if ( workers.size() == 0 ) {
		cerr << "Rank " << get_myrank() << " workers is empty!" << "\n"; localhealth = 0;
	}
	if ( workers.at(0) == NULL ) {
		cerr << "Rank " << get_myrank() << " MASTER worker does not exist!" << "\n"; localhealth = 0;
	}

	multimap<int,int> available_res;
	for( size_t thr = 0; thr < workers.size(); thr++ ) {
		acquisor* thrhdl = workers.at(thr);
		if ( thrhdl != NULL ) {
			for( auto it = thrhdl->res.begin(); it != thrhdl->res.end(); it++ ) {
				int mpid = it->mpid;
				int phcid = it->phcandid;
				araullo_res* thisone = it->dat;
				if ( thisone != NULL ) {
//cout << "Writing shortlist phcid/mpid " << phcid << ";" << mpid << "\n";
					available_res.insert( pair<int,int>( phcid, mpid ) ); //enables accessing all matpoint for specific phcid at once see below
				}
//				else {
//cout << "Missing shortlist phcid/mpid " << phcid << ";" << mpid << "\n";
//				}
			}
		}
	}

	for( auto mtsk = itsk.iphcands.begin(); mtsk != itsk.iphcands.end(); mtsk++ ) {
		int curr_phcandid = mtsk->candid;

cout << "Processing shortlist curr_phcandid " << curr_phcandid << "\n";

		//use multimap to get directly all keys
		vector<int> cand;

		pair<multimap<int, int>::iterator, multimap<int, int>::iterator> these;
		these = available_res.equal_range( curr_phcandid ); //returns a pair representing the range of elements with key equal to curr_phcandid
		for ( auto it = these.first; it != these.second; it++) {
			cand.push_back( it->second );
		}
		//total Elements in the range int count = distance(these.first, these.second);

		if ( cand.size() >= static_cast<size_t>(INT32MX) ) {
			cerr << "Rank " << get_myrank() << " more unique_mpid than current implementation can handle (" << INT32MX - 1 << ") !" << "\n"; localhealth = 0;
		}

		int globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != get_nranks() ) { //either all or none goes out...
			cerr << "Rank " << get_myrank() << " has recognized that not all processes have collected unique_mpid!" << "\n";
			return false;
		}

		int localn = static_cast<int>(cand.size());
		//MPI_Gatherv because each process might have different value for localn

		vector<int> i32; //final I/O buffer

		if ( get_nranks() > SINGLEPROCESS ) { //we need to collect results from slaves
			//MK::processes never share material points!
			//therefore collect just all unique_mpid on MASTER and write to H5 file
			int* rk2n = NULL;
			//how many to receive per process
			rk2n = new int[get_nranks()];
			for( int rk = MASTER; rk < get_nranks(); rk++ ) {
				rk2n[rk] = 0;
			}
			//identify first on MASTER how many results per process
			MPI_Allgather(&localn, 1, MPI_INT, rk2n, 1, MPI_INT, MPI_COMM_WORLD );

			//master dumps its results first without MPI communication
			if ( get_myrank() == MASTER ) {
				for( auto it = cand.begin(); it != cand.end(); it++ ) {
					i32.push_back( *it );
				}
			}
			MPI_Barrier( MPI_COMM_WORLD );
			//then query results from neighbors for this phase
			for( int rk = MASTER+1; rk < get_nranks(); rk++ ) {
				if ( rk2n[rk] > 0 ) { //everybody is informed...
					if ( get_myrank() == MASTER ) {
						int* rcv = NULL;
						rcv = new int[rk2n[rk]];
						for( int ii = 0; ii < rk2n[rk]; ii++ ) { rcv[ii] = INT32MX; } //hyperphobic

						MPI_Recv( rcv, rk2n[rk], MPI_INT, rk, rk, MPI_COMM_WORLD, MPI_STATUS_IGNORE );

						for( int ii = 0; ii < rk2n[rk]; ii++ ) {
							if ( rcv[ii] != INT32MX ) { //test for UINT32MX
								i32.push_back( rcv[ii] );
							}
							else {
								cerr << "Rank " << MASTER << " detect receival of an invalid mat point ID in phase " << curr_phcandid << "\n";
							}
						}
						delete [] rcv; rcv = NULL;
					}
					else {
						if ( get_myrank() == rk ) {
							int* snd = NULL;
							snd = new int[rk2n[rk]];
							for( int ii = 0; ii < cand.size(); ii++ ) {
								snd[ii] = cand[ii];
							}
							for( int ii = cand.size(); ii < rk2n[rk]; ii++ ) { //hyperphobic should never be executed
								snd[ii] = INT32MX;
							}
							MPI_Send( snd, rk2n[rk], MPI_INT, MASTER, rk, MPI_COMM_WORLD );
							delete [] snd; snd = NULL;
						}
					}
					MPI_Barrier( MPI_COMM_WORLD );
				} //done pulling existent results
			} //pull results from next rank

			/*
			//##MK::Gatherv when localn is zero?
			int* disl = NULL;
			if ( get_myrank() == MASTER ) {
				disl = new int[get_nranks()];
				disl[0] = 0;
				for( int rk = MASTER+1; rk < get_nranks(); rk++ ) {
					disl[rk] = disl[rk-1] + rk2n[rk-1];
				}
			}

			//generate send buffer
			int* snd = NULL;
			snd = new int[localn];
			size_t i = 0;
			for( auto it = unique_mpid.begin(); it != unique_mpid.end(); it++, i++ ) {
				snd[i] = *it;
			}
			unique_mpid = unordered_set<int>();

			//allocate receive buffer, which is only signifcant at root
			int* rcv = NULL;
			if ( get_myrank() == MASTER ) {
				rcv = new int[disl[get_nranks()-1]];
				for( int i = 0; i < disl[get_nranks()-1]; i++ ) { //hyperphobic
					rcv[i] = INT32MX;
				}
			}
			//do not create a datatype here
			MPI_Gatherv( snd, localn, MPI_INT, rcv, rk2n, disl, MPI_INT, MASTER, MPI_COMM_WORLD ); //##MK::use in-place

			if ( get_myrank() == MASTER ) {
				//for( int i = 0; i < disl[get_nranks()-1]; i++ ) {
					i32.push_back( rcv[i] );
				}
				delete rcv; rcv = NULL;
				delete rk2n; rk2n = NULL;
				delete disl; disl = NULL;
			}
			delete snd; snd = NULL;
			*/
		}
		else { //only MASTER
			for( auto it = cand.begin(); it != cand.end(); it++ ) { //implies no order
				i32.push_back( *it );
			}
		}

		//now that I/O buffer is filled, write it to file
		if ( get_myrank() == MASTER ) {

			//sort the world-global material point IDs with results for this phase
			sort( i32.begin(), i32.end() ); //sorts by default in ascending order

			string dsnm = PARAPROBE_ARAULLO_RES_MP_IOID + fwslash + "PH" + to_string(curr_phcandid);
			ifo = h5iometa( dsnm, i32.size()/PARAPROBE_ARAULLO_RES_MP_IOID_NCMAX, PARAPROBE_ARAULLO_RES_MP_IOID_NCMAX );
			status = debugh5Hdl.create_contiguous_matrix_i32le( ifo );
			if ( status == WRAPPED_HDF5_SUCCESS ) {
				offs = h5offsets( 0, i32.size()/PARAPROBE_ARAULLO_RES_MP_IOID_NCMAX, 0, PARAPROBE_ARAULLO_RES_MP_IOID_NCMAX,
							i32.size()/PARAPROBE_ARAULLO_RES_MP_IOID_NCMAX, PARAPROBE_ARAULLO_RES_MP_IOID_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_i32le_hyperslab( ifo, offs, i32 );
				if ( status == WRAPPED_HDF5_SUCCESS ) {

					//now we can use this information also to write a supplementary XDMF file to visualize the results in Paraview
					if ( ConfigAraullo::IOSpecificPeaks == true ) {
						string xmlfn = "PARAPROBE.Araullo.Results.SimID." + to_string(ConfigShared::SimID) + ".PH" + to_string(curr_phcandid) + ".xdmf";
						debugxdmf.create_phaseresults_file( xmlfn, ConfigAraullo::NumberOfSO2Directions, debugh5Hdl.h5resultsfn, curr_phcandid, i32 );
					}

					i32 = vector<int>();
				}
				else {
					cerr << "Rank " << MASTER << " failed to store " << dsnm << " status " << status << "\n"; localhealth = 0;
				}
			}
			else {
				cerr << "Rank " << MASTER << " failed to create " << dsnm << " status " << status << "\n"; localhealth = 0;
			}
		}

		MPI_Barrier( MPI_COMM_WORLD );

		globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != get_nranks() ) { //either all or none goes out...
			cerr << "Rank " << get_myrank() << " has recognized that not collecting matpoint IDs with results for phase " << curr_phcandid << " failed!" << "\n";
			return false;
		}

	} //next phase

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	araullo_tictoc.prof_elpsdtime_and_mem( "WriteShortlistsResultsH5", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


int araulloHdl::get_myrank()
{
	return this->myrank;
}


int araulloHdl::get_nranks()
{
	return this->nranks;
}


void araulloHdl::set_myrank( const int rr )
{
	this->myrank = rr;
}


void araulloHdl::set_nranks( const int nn )
{
	this->nranks = nn;
}


void araulloHdl::init_mpidatatypes()
{
	MPI_Type_contiguous(2, MPI_UNSIGNED_LONG_LONG, &MPI_Ranger_DictKeyVal_Type);
	MPI_Type_commit(&MPI_Ranger_DictKeyVal_Type);

	MPI_Type_contiguous(3, MPI_FLOAT, &MPI_Synth_XYZ_Type);
	MPI_Type_commit(&MPI_Synth_XYZ_Type);

	MPI_Type_contiguous(9, MPI_UNSIGNED_CHAR, &MPI_Ranger_Iontype_Type);
	MPI_Type_commit(&MPI_Ranger_Iontype_Type);

	MPI_Type_contiguous(2, MPI_FLOAT, &MPI_ElevAzim_Type);
	MPI_Type_commit(&MPI_ElevAzim_Type);

	MPI_Type_contiguous(9, MPI_FLOAT, &MPI_Triangle3D_Type);
	MPI_Type_commit(&MPI_Triangle3D_Type);

	/*int elementCounts[2] = {3, 1};
	MPI_Aint displacements[2] = {0, 3 * MPIIO_OFFSET_INCR_F32};
	MPI_Datatype oldTypes[2] = {MPI_FLOAT, MPI_UNSIGNED};
	MPI_Type_create_struct(2, elementCounts, displacements, oldTypes, &MPI_Ion_Type);
	MPI_Type_commit(&MPI_Ion_Type);*/

	int elementCounts[2] = {2, 4};
	MPI_Aint displacements[2] = {0, 2 * MPIIO_OFFSET_INCR_F32};
	MPI_Datatype oldTypes[2] = {MPI_FLOAT, MPI_UNSIGNED_CHAR};
	MPI_Type_create_struct(2, elementCounts, displacements, oldTypes, &MPI_Ranger_MQIval_Type);
	MPI_Type_commit(&MPI_Ranger_MQIval_Type);

	int elementCounts2[2] = {6+3, 3};
	MPI_Aint displacements2[2] = {0, (6+3)* MPIIO_OFFSET_INCR_F32};
	MPI_Datatype oldTypes2[2] = {MPI_FLOAT, MPI_UNSIGNED};
	MPI_Type_create_struct(2, elementCounts2, displacements2, oldTypes2, &MPI_VxlizationInfo_Type);
	MPI_Type_commit(&MPI_VxlizationInfo_Type);


	/*
	int elementCounts3[3] = {3, 6, 4};
	MPI_Aint displacements3[3] = {0, 3 * MPIIO_OFFSET_INCR_F32, (3 * MPIIO_OFFSET_INCR_F32 + 6 * MPIIO_OFFSET_INCR_UINT32) };
	MPI_Datatype oldTypes3[3] = {MPI_FLOAT, MPI_UNSIGNED, MPI_BYTE};
	MPI_Type_create_struct(3, elementCounts3, displacements3, oldTypes3, &MPI_Spatstat_Task_Type);
	MPI_Type_commit(&MPI_Spatstat_Task_Type);
	*/
}
