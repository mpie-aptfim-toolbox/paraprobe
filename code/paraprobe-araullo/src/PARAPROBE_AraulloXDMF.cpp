//##MK::GPLV3

#include "PARAPROBE_AraulloXDMF.h"


araullo_xdmf::araullo_xdmf()
{
	
}
	
araullo_xdmf::~araullo_xdmf()
{
	
}


int araullo_xdmf::create_materialpoint_file( const string xmlfn, const size_t nmp, const string h5ref )
{
	xdmfout.open( xmlfn.c_str() );
	if ( xdmfout.is_open() == true ) {
		xdmfout << XDMF_HEADER_LINE1 << "\n";
		xdmfout << XDMF_HEADER_LINE2 << "\n";
		xdmfout << XDMF_HEADER_LINE3 << "\n";

		xdmfout << "  <Domain>" << "\n";
		xdmfout << "    <Grid Name=\"matpoints\" GridType=\"Uniform\">" << "\n";
		xdmfout << "      <Topology TopologyType=\"Mixed\" NumberOfElements=\"" << nmp << "\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << 3*nmp << "\" NumberType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5ref << ":" << PARAPROBE_ARAULLO_META_MP_TOPO << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Topology>" << "\n";

		xdmfout << "      <Geometry GeometryType=\"XYZ\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nmp << " 3\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5ref << ":" << PARAPROBE_ARAULLO_META_MP_XYZ << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Geometry>" << "\n";

		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"MPID\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nmp << " 1\" DataType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5ref << ":" << PARAPROBE_ARAULLO_META_MP_ID << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";
/*
		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"NIons\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nmp << " 1\" DataType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5ref << ":" << PARAPROBE_araullo_MATPOINT_NIONS << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";
*/

		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"ROIRadius\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nmp << " 1\" DataType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5ref << ":" << PARAPROBE_ARAULLO_META_MP_R << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";

 		xdmfout << "    </Grid>" << "\n";
		xdmfout << "  </Domain>" << "\n";
		xdmfout << "</Xdmf>" << "\n";

		xdmfout.flush();
		xdmfout.close();

		return WRAPPED_XDMF_SUCCESS;
	}
	else {
		return WRAPPED_XDMF_IOFAILED;
	}
}


int araullo_xdmf::create_phaseresults_file( const string xmlfn, const size_t ndir, const string h5ref,
		const unsigned int phcandid, vector<int> const & mpids )
{
	xdmfout.open( xmlfn.c_str() );
	if ( xdmfout.is_open() == true ) {
		xdmfout << XDMF_HEADER_LINE1 << "\n";
		xdmfout << XDMF_HEADER_LINE2 << "\n";
		xdmfout << XDMF_HEADER_LINE3 << "\n";

		xdmfout << "  <Domain>" << "\n";
		xdmfout << "    <Grid Name=\"directions\" GridType=\"Uniform\">" << "\n";
		xdmfout << "      <Topology TopologyType=\"Mixed\" NumberOfElements=\"" << ndir << "\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << 3*ndir << "\" NumberType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5ref << ":" << PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Topology>" << "\n";

		xdmfout << "      <Geometry GeometryType=\"XYZ\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << ndir << " 3\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5ref << ":" << PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Geometry>" << "\n";

		string fwslash = "/";
		string prefix = PARAPROBE_ARAULLO_RES_SPECPEAK + fwslash + "PH" + to_string(phcandid) + fwslash;
		for( auto it = mpids.begin(); it != mpids.end(); it++ ) {
			xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"mpid" << *it << "\">" << "\n";
			xdmfout << "        <DataItem Dimensions=\"" << ndir << " 1\" DataType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
			xdmfout << "           " << h5ref << ":" << prefix << *it << "\n";
			xdmfout << "        </DataItem>" << "\n";
			xdmfout << "      </Attribute>" << "\n";
		}

 		xdmfout << "    </Grid>" << "\n";
		xdmfout << "  </Domain>" << "\n";
		xdmfout << "</Xdmf>" << "\n";

		xdmfout.flush();
		xdmfout.close();

		return WRAPPED_XDMF_SUCCESS;
	}
	else {
		return WRAPPED_XDMF_IOFAILED;
	}
}

