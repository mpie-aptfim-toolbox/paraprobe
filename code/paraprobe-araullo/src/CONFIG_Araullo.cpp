//##MK::GPLV3efineGridOldBinWidthX

#include "CONFIG_Araullo.h"

//string ConfigAraullo::InputfilePSE = "";
string ConfigAraullo::InputfileReconstruction = "";
string ConfigAraullo::InputfileHullAndDistances = "";
//string ConfigAraullo::Outputfile = "";
	
ANALYSIS_VOLUME ConfigAraullo::AnalysisVolume = ENTIRE_DATASET;
aabb3d ConfigAraullo::AnalysisVolumeAABB = aabb3d();
VOLUME_SAMPLING_METHOD ConfigAraullo::ROISamplingMethod = CUBOIDAL_GRID;
string ConfigAraullo::InputfileUserROIPositions = "";
WINDOWING_METHOD ConfigAraullo::SDMWindowingMethod = RECTANGULAR;

apt_real ConfigAraullo::ROIRadiusMax = 0.0;
apt_real ConfigAraullo::SamplingGridBinWidthX = 0.0;
apt_real ConfigAraullo::SamplingGridBinWidthY = 0.0;
apt_real ConfigAraullo::SamplingGridBinWidthZ = 0.0;
apt_real ConfigAraullo::RefineGridOldBinWidthX = 0.0;
apt_real ConfigAraullo::RefineGridOldBinWidthY = 0.0;
apt_real ConfigAraullo::RefineGridOldBinWidthZ = 0.0;
unsigned int ConfigAraullo::RefineGridNewSubdivisionX = 1;
unsigned int ConfigAraullo::RefineGridNewSubdivisionY = 1;
unsigned int ConfigAraullo::RefineGridNewSubdivisionZ = 1;
string ConfigAraullo::SamplingPosition = "";
unsigned int ConfigAraullo::FixedNumberOfROIs = 0;
unsigned int ConfigAraullo::SDMBinExponent = 8;
apt_real ConfigAraullo::KaiserAlpha = 0.0;
apt_real ConfigAraullo::DBScanThreshold = 0.1;
apt_real ConfigAraullo::DBScanEpsilon = 0.01; //Eucledian distance between vertices on unit sphere surface
unsigned int ConfigAraullo::DBScanMinPts = 1;

	
bool ConfigAraullo::SamplingGridRemoveBndPoints = false;
bool ConfigAraullo::SDMNormalize = true;

bool ConfigAraullo::IOHistoCnts = false;
bool ConfigAraullo::IOHistoFFTMagn = false;
bool ConfigAraullo::IOSpecificPeaks = false;
bool ConfigAraullo::IODBScanOnSpecificPeaks = false;
bool ConfigAraullo::IOStrongPeaks = false;
bool ConfigAraullo::IOKeyQuants = false;
string ConfigAraullo::KeyQuantsParseMe = "";
vector<apt_real> ConfigAraullo::KeyQuantiles = vector<apt_real>();

//string ConfigAraullo::LatticeDistancesToProbe = "";
//string ConfigAraullo::IontypesToProbe = "";
string ConfigAraullo::InputfileElevAzimGrid = "PARAPROBE.Araullo.GeodesicSphere40962.dat";
unsigned long ConfigAraullo::MaxMemoryPerNode = 0;
unsigned int ConfigAraullo::ProcessesPerMPITeam = 1;
unsigned int ConfigAraullo::GPUsPerNode = 0;
unsigned int ConfigAraullo::GPUPerProcess = 0;
unsigned int ConfigAraullo::GPUWorkloadFactor = 1;
apt_real ConfigAraullo::ROI2ROIMinDistance = 0.01; //nm
//vector<pair<unsigned int, unsigned int>> ConfigAraullo::ProbeThisBinThisType = vector<pair<unsigned int, unsigned int>>();

//unsigned int ConfigAraullo::NumberOfFFTMagnImages = 0;
unsigned int ConfigAraullo::NumberOfSO2Directions = 0;


bool ConfigAraullo::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.Araullo.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigAraullo")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;

	/*
	//WHICH REGION OF THE DATASET to ANALYZE
	mode = read_xml_attribute_uint32( rootNode, "AnalysisVolume" );
	switch (mode)
	{
		case CUBOIDAL_REGION:
			AnalysisVolume = CUBOIDAL_REGION;
			AnalysisVolumeAABB = aabb3d( 	read_xml_attribute_float( rootNode, "AnalysisVolumeXMin" ),
											read_xml_attribute_float( rootNode, "AnalysisVolumeXMax" ),
											read_xml_attribute_float( rootNode, "AnalysisVolumeYMin" ),
											read_xml_attribute_float( rootNode, "AnalysisVolumeYMax" ),
											read_xml_attribute_float( rootNode, "AnalysisVolumeZMin" ),
											read_xml_attribute_float( rootNode, "AnalysisVolumeZMax" )  );
			break;
		default:
			AnalysisVolume = ENTIRE_DATASET;
	}
	*/

	//WHAT TYPE OF ANALYSIS TO DO
	mode = read_xml_attribute_uint32( rootNode, "VolumeSamplingMethod" );
	switch (mode)
	{
		case CUBOIDAL_GRID:
			ROISamplingMethod = CUBOIDAL_GRID; break;
		case SINGLE_POINT_USER:
			ROISamplingMethod = SINGLE_POINT_USER; break;
		case SINGLE_POINT_CENTER:
			ROISamplingMethod = SINGLE_POINT_CENTER; break;
		case RANDOM_POINTS_FIXED:
			ROISamplingMethod = RANDOM_POINTS_FIXED; break;
		case USERDEFINED_POINTS:
			ROISamplingMethod = USERDEFINED_POINTS; break;
		default:
			cerr << "Unknown VolumeSamplingMethod!" << "\n"; 
			return false;
	}
	
	//InputfilePSE = read_xml_attribute_string( rootNode, "InputfilePSE"); //PeriodicTableOfElements" );
	InputfileReconstruction = read_xml_attribute_string( rootNode, "InputfileReconstruction" );
	InputfileHullAndDistances = read_xml_attribute_string( rootNode, "InputfileHullAndDistances" );
	//Outputfile = read_xml_attribute_string( rootNode, "Outputfile" );
	
	ROIRadiusMax = read_xml_attribute_float( rootNode, "ROIRadiusMax" );
	switch (ROISamplingMethod) {
		case CUBOIDAL_GRID:
			SamplingGridBinWidthX = read_xml_attribute_float( rootNode, "SamplingGridBinWidthX" );
			SamplingGridBinWidthY = read_xml_attribute_float( rootNode, "SamplingGridBinWidthY" );
			SamplingGridBinWidthZ = read_xml_attribute_float( rootNode, "SamplingGridBinWidthZ" );
			break;
		case SINGLE_POINT_USER:
			SamplingPosition = read_xml_attribute_string( rootNode, "SamplingPosition" );
			break;
		case SINGLE_POINT_CENTER:
			break;
		case RANDOM_POINTS_FIXED:
			FixedNumberOfROIs = read_xml_attribute_uint32( rootNode, "FixedNumberOfROIs" );
			break;
		case USERDEFINED_POINTS:
			InputfileUserROIPositions = read_xml_attribute_string( rootNode, "InputfileUserROIPositions" );
			RefineGridOldBinWidthX = read_xml_attribute_float( rootNode, "RefineGridOldBinWidthX" );
			RefineGridOldBinWidthY = read_xml_attribute_float( rootNode, "RefineGridOldBinWidthY" );
			RefineGridOldBinWidthZ = read_xml_attribute_float( rootNode, "RefineGridOldBinWidthZ" );
			RefineGridNewSubdivisionX = read_xml_attribute_uint32( rootNode, "RefineGridNewSubdivisionX" );
			RefineGridNewSubdivisionY = read_xml_attribute_uint32( rootNode, "RefineGridNewSubdivisionY" );
			RefineGridNewSubdivisionZ = read_xml_attribute_uint32( rootNode, "RefineGridNewSubdivisionZ" );
			break;
		default:
			return false;
	}
	
	SamplingGridRemoveBndPoints = read_xml_attribute_bool( rootNode, "RemoveROIsProtrudingOutside" );

	SDMBinExponent = read_xml_attribute_uint32( rootNode, "SDMBinExponent" );
	SDMNormalize = read_xml_attribute_bool( rootNode, "SDMNormalize" );
	
	mode = read_xml_attribute_uint32( rootNode, "WindowingMethod" );
	switch (mode)
	{
		case RECTANGULAR:
			SDMWindowingMethod = RECTANGULAR; break;
		case KAISER:
			SDMWindowingMethod = KAISER; break;
		default:
			SDMWindowingMethod = RECTANGULAR;
			cout << "WARNING: An invalid windowing method was specified we use the default, rectangular windowing!" << "\n";
	}
	
	if ( SDMWindowingMethod == KAISER ) {
		KaiserAlpha = read_xml_attribute_float( rootNode, "KaiserAlpha" );
	}
	
	IOSpecificPeaks = read_xml_attribute_bool( rootNode, "IOStoreSpecificPeaks" );
	if ( IOSpecificPeaks == true ) { //optional compression of the peaks
		IODBScanOnSpecificPeaks = read_xml_attribute_bool( rootNode, "IODBScanOnSpecificPeaks" );
		if ( IODBScanOnSpecificPeaks == true ) {
			DBScanThreshold = read_xml_attribute_float( rootNode, "DBScanIntensityThreshold" );
			DBScanEpsilon = read_xml_attribute_float( rootNode, "DBScanSearchRadius" );
			DBScanMinPts = read_xml_attribute_uint32( rootNode, "DBScanMinPksForReflector" );
		}
	}
	IOKeyQuants = read_xml_attribute_bool( rootNode, "IOStoreKeyQuants" );
	if ( IOKeyQuants == true ) {
		KeyQuantsParseMe = read_xml_attribute_string( rootNode, "KeyQuants" );
	}
	//##MK::these are mainly for developers
	IOStrongPeaks = read_xml_attribute_bool( rootNode, "IOStoreThreeStrongestPeaks" );
	IOHistoCnts = read_xml_attribute_bool( rootNode, "IOStoreHistoCnts" );
	IOHistoFFTMagn = read_xml_attribute_bool( rootNode, "IOStoreHistoFFTMagn" );


	//LatticeDistancesToProbe = read_xml_attribute_string( rootNode, "LatticeDistancesToProbe" );
	//IontypesToProbe = read_xml_attribute_string( rootNode, "IontypesToProbe" );
	
	InputfileElevAzimGrid = read_xml_attribute_string( rootNode, "InputfileElevAzimGrid" );
	MaxMemoryPerNode = read_xml_attribute_uint64( rootNode, "MaxMemoryPerNode" );
	//##MK::replace by system call at some point
	//https://stackoverflow.com/questions/349889/how-do-you-determine-the-amount-of-linux-system-ram-in-c
	//https://stackoverflow.com/questions/5553665/get-ram-system-size
	ProcessesPerMPITeam = read_xml_attribute_uint32( rootNode, "ProcessesPerMPITeam" );
	GPUsPerNode = read_xml_attribute_int32( rootNode, "GPUsPerComputingNode" );
	GPUPerProcess = read_xml_attribute_uint32( rootNode, "GPUPerProcess" );
	GPUWorkloadFactor = read_xml_attribute_uint32( rootNode, "GPUWorkloadFactor" );
	
	return true;
}


bool ConfigAraullo::checkUserInput()
{
	cout << "ConfigAraullo::" << "\n";
	switch(AnalysisVolume)
	{
		case CUBOIDAL_REGION:
			cout << "Only a cuboidal region from the dataset will be analyzed" << "\n";
			cout << AnalysisVolumeAABB << "\n";
			break;
		default:
			cout << "The entire dataset will be analyzed" << "\n";
	}

	switch (ROISamplingMethod)
	{
		case CUBOIDAL_GRID:
			cout << "Building voxel volume grid at positions displaced from the specimen main axis" << "\n"; break;
		case SINGLE_POINT_USER:
			cout << "Building a single point user-specified at " << SamplingPosition << "\n"; break;
		case SINGLE_POINT_CENTER:
			cout << "Building a single point in the center of a specimen bounding box " << "\n"; break;
		case RANDOM_POINTS_FIXED:
			cout << "Building a cloud of randomly distribute ROI in the specimen bounding box " << "\n"; break;
		case USERDEFINED_POINTS:
			cout << "Working with ROIs placed at positions explicitly defined by the user " << "\n";
			cout << "Duplicate points within " << ROI2ROIMinDistance << " nm will be removed " << "\n"; break;
		default:
			cerr << "Unknown Volume sampling method chosen" << "\n";
			return false;
	}
	
	if ( ROIRadiusMax < EPSILON ) {
		cerr << "ROIRadiusMax must be positive and finite!" << "\n"; return false;
	}
	cout << "ROIRadiusMax " << ROIRadiusMax << "\n";
	if ( ROISamplingMethod == CUBOIDAL_GRID ) {
		if ( SamplingGridBinWidthX < EPSILON ) {
			cerr << "SamplingGridBinWidthX needs to be positive and finite!" << "\n"; return false;
		}
		cout << "SamplingGridBinWidthX " << SamplingGridBinWidthX << "\n";
		if ( SamplingGridBinWidthY < EPSILON ) {
			cerr << "SamplingGridBinWidthY needs to be positive and finite!" << "\n"; return false;
		}
		cout << "SamplingGridBinWidthY " << SamplingGridBinWidthY << "\n";
		if ( SamplingGridBinWidthZ < EPSILON ) {
			cerr << "SamplingGridBinWidthY needs to be positive and finite!" << "\n"; return false;
		}
		cout << "SamplingGridBinWidthZ " << SamplingGridBinWidthZ << "\n";
	}
	if ( ROISamplingMethod == SINGLE_POINT_USER ) {
		stringstream parsethis;
		parsethis << SamplingPosition;
		string datapiece;
		size_t nsemicolon = count( SamplingPosition.begin(), SamplingPosition.end(), ';' );
		if ( nsemicolon != 2 ) {
			cerr << "SamplingPosition has invalid format must be x;y;z!" << "\n"; return false;
		}
		cout << "SamplingPosition " << SamplingPosition << "\n";
	}
	if ( ROISamplingMethod == RANDOM_POINTS_FIXED ) {
		if ( FixedNumberOfROIs < 1 ) {
			cerr << "FixedNumberOfROIs must be positive and finite!" << "\n"; return false;
		}
		cout << "FixedNumberOfROIs " << FixedNumberOfROIs << "\n";
	}
	if ( ROISamplingMethod == USERDEFINED_POINTS ) {
		if ( RefineGridOldBinWidthX < EPSILON ) {
			cerr << "RefineGridOldBinWidthX needs to be positive and finite!" << "\n"; return false;
		}
		cout << "RefineGridOldBinWidthX " << RefineGridOldBinWidthX << "\n";
		if ( RefineGridOldBinWidthY < EPSILON ) {
			cerr << "RefineGridOldBinWidthY needs to be positive and finite!" << "\n"; return false;
		}
		cout << "RefineGridOldBinWidthY " << RefineGridOldBinWidthY << "\n";
		if ( RefineGridOldBinWidthZ < EPSILON ) {
			cerr << "RefineGridOldBinWidthZ needs to be positive and finite!" << "\n"; return false;
		}
		cout << "RefineGridOldBinWidthZ " << RefineGridOldBinWidthZ << "\n";
		if ( RefineGridNewSubdivisionX < 1 ) { //|| ConfigAraullo::RefineGridNewSubdivisionX > ) {
			cerr << "RefineGridNewSubdivisionX needs to be at least 1 !" <<  "\n"; return false;
		}
		cout << "RefineGridNewSubdivisionX " << RefineGridNewSubdivisionX << "\n";
		if ( RefineGridNewSubdivisionY < 1 ) {
			cerr << "RefineGridNewSubdivisionY needs to be at least 1 !" <<  "\n"; return false;
		}
		cout << "RefineGridNewSubdivisionY " << RefineGridNewSubdivisionY << "\n";
		if ( RefineGridNewSubdivisionZ < 1 ) {
			cerr << "RefineGridNewSubdivisionZ needs to be at least 1 !" <<  "\n"; return false;
		}
		cout << "RefineGridNewSubdivisionZ " << RefineGridNewSubdivisionZ << "\n";

		cout << "User-defined ROI positions will be parsed from " << InputfileUserROIPositions << "\n";
	}
		
	if ( SDMBinExponent < 8 || SDMBinExponent > 12 ) {
		cerr << "SDMBinExponent should be integer in-between [8, 12]!" << "\n"; return false;
	}
	cout << "SDMBinExponent " << SDMBinExponent << "\n";
	cout << "SDMNormalize " << SDMNormalize << "\n";

	switch (SDMWindowingMethod)
	{
		case RECTANGULAR:
			cout << "Rectangular windowing will be used" << "\n"; break;
		case KAISER:
			cout << "Kaiser windowing will be used" << "\n"; break;
		default:
			cerr << "Unknown windowing method !" << "\n"; return false;
	}
	if ( SDMWindowingMethod == KAISER && KaiserAlpha < EPSILON ) {
		cerr << "KaiserAlpha constant must be positive and finite !" << "\n"; return false;
	}
	
	cout << "IOSpecificPeak " << IOSpecificPeaks << "\n";
	if ( IOSpecificPeaks == true ) {
		cout << "IODBScanOnSpecificPeaks " << IODBScanOnSpecificPeaks << "\n";
		if ( IODBScanOnSpecificPeaks == true ) {
			if ( DBScanThreshold > EPSILON ) {
				cout << "DBScanThreshold " << DBScanThreshold << "\n";
			}
			else {
				cerr << "DBScanThreshold needs to be > " << EPSILON << "\n"; return false;
			}
			if ( DBScanEpsilon > EPSILON ) {
				cout << "DBScanEpsilon " << DBScanEpsilon << "\n";
			}
			else {
				cerr << "DBScanEpsilon needs to be > " << EPSILON << "\n"; return false;
			}
			if ( DBScanMinPts >= 1 ) {
				cout << "DBScanMinPts " << DBScanMinPts << "\n";
			}
			else {
				cerr << "DBScanMinPts needs to be at least 1 !" << "\n"; return false;
			}
		}
	}
	cout << "IOKeyQuants " << IOKeyQuants << "\n";
	if ( IOKeyQuants == true ) {
		cout << "KeyQuantsParseMe " << KeyQuantsParseMe << "\n";
		size_t nsemicola = count(KeyQuantsParseMe.begin(), KeyQuantsParseMe.end(), ';');
cout << "KeyQuantsParseMe contains " << nsemicola << " semicola" << "\n";
		stringstream parsethis;
		string datapiece;
		parsethis << KeyQuantsParseMe;
		for( size_t i = 0; i <= nsemicola; i++ ) { //<= to parse at least one
			getline( parsethis, datapiece, ';');
cout << "__" << datapiece.c_str() << "__" << "\n";
			apt_real val = stof(datapiece);
			if ( val < static_cast<apt_real>(val) < 0.0 || val > static_cast<apt_real>(1.0) ) {
				cerr << "Parsed a quantile value __" << val << "__ outside the allowed range [0.0, 1.0] !" << "\n"; return false;
			}
			else {
				KeyQuantiles.push_back( val );
			}
		}

		if ( KeyQuantiles.size() == 0 ) {
			cerr << "KeyQuantiles should be computed but not quantile values were parseable!" << "\n"; return false;
		}
		cout << "The following key quantiles were identified:" << "\n";
		sort( KeyQuantiles.begin(), KeyQuantiles.end() ); //sort for numerical efficiency reasons
		for( auto it = KeyQuantiles.begin(); it != KeyQuantiles.end(); it++ ) {
			cout << *it << "\n";
		}
	}
	cout << "IOStrongPeaks " << IOStrongPeaks << "\n";
	cout << "IOHistoCnts " << IOHistoCnts << "\n";
	cout << "IOHistoFFTMagn " << IOHistoFFTMagn << "\n";
	
	//cout << "InputfilePSE read from " << InputfilePSE << "\n";
	cout << "InputfileReconstruction read from " << InputfileReconstruction << "\n";
	cout << "InputHullsAndDistances read from " << InputfileHullAndDistances << "\n";
	//cout << "Output written to " << Outputfile << "\n";
	
	cout << "InputElevAzimGrid read from " << InputfileElevAzimGrid << "\n";
	if ( MaxMemoryPerNode > 0 ) {
		cout << "MaxMemoryPerNode " << MaxMemoryPerNode << "\n"; 
	}
	else {
		cerr << "MaxMemoryPerNode is needs to be finite" << "\n"; return false;
	}
	cout << "ProcessesPerMPITeam " << ProcessesPerMPITeam << "\n";
	cout << "GPUsPerNode " << GPUsPerNode << "\n";
	cout << "GPUPerProcess " << GPUPerProcess << "\n";
	cout << "GPUWorkloadFactor " << GPUWorkloadFactor << "\n";
	cout << "ROI2ROIMinDistance " << ROI2ROIMinDistance << "\n";
	
	if ( SamplingGridRemoveBndPoints == true ) {
		cout << "Material points outside the inside voxel or within closer than ROIRadiusMax to the triangle hull are discarded!" << "\n";
	}
	else {
		cout << "WARNING finite dataset boundary affects are not accounted for so some sampling points will have insignificant ion support!" << "\n";
	}
	
	cout << "\n";
	return true;
}


void ConfigAraullo::reportSettings( vector<pparm> & res)
{
	//res.push_back( pparm( "InputfilePSE", InputfilePSE, "", "" ) );
	res.push_back( pparm( "InputfileReconstruction", InputfileReconstruction, "", "pre-computed reconstruction space x,y,z, and atom type" ) );
	res.push_back( pparm( "InputfileHullAndDistances", InputfileHullAndDistances, "", "pre-computed distances of ions to edge of dataset" ) );

	/*
	res.push_back( pparm( "AnalysisVolume", sizet2str(AnalysisVolume), "", "space portion of the dataset to work with" ) );
	res.push_back( pparm( "AnalysisVolumeAABBXmin", real2str(AnalysisVolumeAABB.xmi), "nm", "xmin boundary of dataset point cloud" ) );
	res.push_back( pparm( "AnalysisVolumeAABBXmax", real2str(AnalysisVolumeAABB.xmx), "nm", "xmax boundary of dataset point cloud" ) );
	res.push_back( pparm( "AnalysisVolumeAABBYmin", real2str(AnalysisVolumeAABB.ymi), "nm", "ymin boundary of dataset point cloud" ) );
	res.push_back( pparm( "AnalysisVolumeAABBYmax", real2str(AnalysisVolumeAABB.ymx), "nm", "ymax boundary of dataset point cloud" ) );
	res.push_back( pparm( "AnalysisVolumeAABBZmin", real2str(AnalysisVolumeAABB.zmi), "nm", "zmin boundary of dataset point cloud" ) );
	res.push_back( pparm( "AnalysisVolumeAABBZmax", real2str(AnalysisVolumeAABB.zmx), "nm", "zmax boundary of dataset point cloud" ) );
	*/

	res.push_back( pparm( "VolumeSamplingMethod", sizet2str(ROISamplingMethod), "", "how to define where to place ROIs" ) );
	switch(ROISamplingMethod)
	{
		case CUBOIDAL_GRID:
			res.push_back( pparm( "SamplingGridBinWidthX", real2str(SamplingGridBinWidthX), "nm", "for cuboidal ROI grid spacing between ROIs in x" ) );
			res.push_back( pparm( "SamplingGridBinWidthY", real2str(SamplingGridBinWidthY), "nm", "for cuboidal ROI grid spacing between ROIs in y" ) );
			res.push_back( pparm( "SamplingGridBinWidthZ", real2str(SamplingGridBinWidthZ), "nm", "for cuboidal ROI grid spacing between ROIs in z" ) );
			break;
		case SINGLE_POINT_USER:
			res.push_back( pparm( "SamplingPosition", SamplingPosition, "", "for single ROI, where to place in x, y, z" ) );
			break;
		case SINGLE_POINT_CENTER:
			break;
		case RANDOM_POINTS_FIXED:
			res.push_back( pparm( "FixedNumberOfROIs", uint2str(FixedNumberOfROIs), "", "for fixed number of ROIs, how many to place" ) );
			break;
		case USERDEFINED_POINTS:
			res.push_back( pparm( "InputfileUserROIPositions", InputfileUserROIPositions, "", "pre-computed x,y,z position of ROIs" ) );
			res.push_back( pparm( "RefineGridOldBinWidthX", real2str(RefineGridOldBinWidthX), "nm", "the grid resolution used in a previous run of paraprobe-araullo to define how strongly to refine along X" ) );
			res.push_back( pparm( "RefineGridOldBinWidthY", real2str(RefineGridOldBinWidthY), "nm", "the grid resolution used in a previous run of paraprobe-araullo to define how strongly to refine along Y" ) );
			res.push_back( pparm( "RefineGridOldBinWidthZ", real2str(RefineGridOldBinWidthZ), "nm", "the grid resolution used in a previous run of paraprobe-araullo to define how strongly to refine along Z" ) );
			res.push_back( pparm( "RefineGridNewSubdivisionX", uint2str(RefineGridNewSubdivisionX), "1", "the number of sub-divisions where to place new ROIs on the interval RefineGridOldBinWidthi/2 about the old ROI along X" ) );
			res.push_back( pparm( "RefineGridNewSubdivisionY", uint2str(RefineGridNewSubdivisionY), "1", "the number of sub-divisions where to place new ROIs on the interval RefineGridOldBinWidthi/2 about the old ROI along Y" ) );
			res.push_back( pparm( "RefineGridNewSubdivisionZ", uint2str(RefineGridNewSubdivisionZ), "1", "the number of sub-divisions where to place new ROIs on the interval RefineGridOldBinWidthi/2 about the old ROI along Z" ) );
			break;
		default:
			break;
	}

	res.push_back( pparm( "WindowingMethod", sizet2str(SDMWindowingMethod), "", "prior to fast-Fourier-transforming to control side lobes and frequency resolution" ) );
	switch(SDMWindowingMethod)
	{
		case KAISER:
			res.push_back( pparm( "KaiserAlpha", real2str(KaiserAlpha), "", "parameter to scale the Kaiser windowing kernel" ) );
			break;
		default:
			break;
	}

	res.push_back( pparm( "ROIRadiusMax", real2str(ROIRadiusMax), "nm", "radius of the ROI" ) );
	res.push_back( pparm( "SDMBinExponent", uint2str(SDMBinExponent), "", "how many bins used for one SDM, one bin padding on either side" ) );
	res.push_back( pparm( "SamplingGridRemoveBndPoints", sizet2str(SamplingGridRemoveBndPoints), "", "to take or not to take ROIs at the dataset edge" ) );
	res.push_back( pparm( "SDMNormalize", sizet2str(SDMNormalize), "", "to normalize or not the FFT-ed amplitude spectrum intensities" ) );

	res.push_back( pparm( "IOSpecificPeaks", sizet2str(IOSpecificPeaks), "", "to report or not the specific signatures composed from a particular bin" ) );
	if ( IODBScanOnSpecificPeaks == true ) {
		res.push_back( pparm( "IODBScanOnSpecificPeaks", sizet2str(IODBScanOnSpecificPeaks), "", "to run a DBScan on the specific peaks and return only these results" ) );
		res.push_back( pparm( "DBScanIntensityThreshold", real2str(DBScanThreshold), "", "threshold above which to consider signal at a FE vertex significant" ) );
		res.push_back( pparm( "DBScanSearchRadius", real2str(DBScanEpsilon), "", "normalized Cartesian distance between FE vertices to group during DBScan" ) );
		res.push_back( pparm( "DBScanMinPksForReflector", uint2str(DBScanMinPts), "", "how many FE vertices to have at least to consider peak significant" ) );
	}
	res.push_back( pparm( "IOKeyQuants", sizet2str(IOKeyQuants), "", "to report or not key quantiles of the signature image intensities" ) );
	res.push_back( pparm( "KeyQuants", KeyQuantsParseMe, "", "which quantiles to evaluate" ) );
	res.push_back( pparm( "IOStrongPeaks", sizet2str(IOStrongPeaks), "", "deprecated" ) );
	res.push_back( pparm( "IOHistoCnts", sizet2str(IOHistoCnts), "", "to report or not the SDM counts" ) );
	res.push_back( pparm( "IOHistoFFTMagn", sizet2str(IOHistoFFTMagn), "", "to report or not the amplitude spectra" ) );
	res.push_back( pparm( "InputfileElevAzimGrid", InputfileElevAzimGrid, "", "pre-computed list of elevation/azimuth pair polar coordinates along which to compute SDMs" ) );
	res.push_back( pparm( "MaxMemoryPerNode", sizet2str(MaxMemoryPerNode), "", "currently not implemented") ); //how much memory to reserve for caching results" ) ); //##MK::per process or node?
	res.push_back( pparm( "ProcessesPerMPITeam", uint2str(ProcessesPerMPITeam), "", "currently not used" ) );
	res.push_back( pparm( "GPUsPerNode", uint2str(GPUsPerNode), "1", "how many GPUs on the node, make sure to have one MPI process per GPU" ) );
	res.push_back( pparm( "GPUPerProcess", uint2str(GPUPerProcess), "1", "currently needs to be 1" ) );
	res.push_back( pparm( "GPUWorkloadFactor", uint2str(GPUWorkloadFactor), "1", "currently not used" ) );
	/* ##MK::duplicate removal currently not implemented
	res.push_back( pparm( "ROI2ROIMinDistance", real2str(ROI2ROIMinDistance), "nm", "below which distance to remove ROIs to avoid wasting time in computing very close by ROI positions" ) );
	*/
}
