//##MK::GPLV3

#ifndef __PARAPROBE_ARAULLO_ACQUISOR_H__
#define __PARAPROBE_ARAULLO_ACQUISOR_H__

//#include "PARAPROBE_AraulloStructs.h"
#include "PARAPROBE_AraulloPhaseCandHdl.h"

//#define SINGLEGPUWORKLOAD					10
//#define NORESULTS_FOR_THIS_MATERIALPOINT	INT32MX
//#define WAIT_UNTIL_GLOBAL_EXTREMUM_FOUND	INT32MX

class acquisor;

class araullo_res;

struct araullo_res_node
{
	araullo_res* dat;
	int mpid;
	int phcandid;
	araullo_res_node() : dat(NULL), mpid(INT32MX), phcandid(INT32MX) {}
	araullo_res_node( const int _mpid, const int _phcid ) :
		 mpid(_mpid), phcandid(_phcid), dat(NULL) {}
};


//storing of timing data
#define TICTOC_NCOLS		6
#define TICTOC_IONSINROI	0
#define TICTOC_THREADID		1
#define TICTOC_PROJECT		2
#define TICTOC_FFT			3
#define TICTOC_FINDPKS		4
#define TICTOC_DBSCAN		5

class araullo_res
{
public:
	araullo_res();
	~araullo_res();
	
	//p3dm1 mp;
	p3d mp;

	void project_cpu( vector<p3d> const & cand, vector<elaz> const & dir );
	void fft_cpu();
	void findpks_cpu( const float spec_peak_rangeleft, const float spec_peak_rangeright ); //const float spec_peak_position );
	void dbscan_cpu( vector<elaz> const & dir );

#ifdef UTILIZE_GPUS
	void project_gpu1( vector<p3d> const & cand, vector<elaz> const & dir );
	void project_gpu2( vector<p3d> const & cand, vector<elaz> const & dir );
	void fft_gpu();
	void findpks_gpu( const float spec_peak_position );
#endif

	cfftn ft;
	acquisor* owner;

	//implicit results arrays per material point
	vector<float> SpecificPeaksTbl;			//nrows = 1 * NumberOfSO2Directions, ncols = 1
	vector<float> ThreePeaksTbl;			//nrows = NumberOfSO2Directions * PARAPROBE_ARAULLO_RES_THREEPEAK_NCMAX
	vector<float> KeyQuantilesTbl;			//nrows = ConfigAraullo::KeyQuantiles.size() * PARAPROBE_ARAULLO_RES_KEYQUANT_NCMAX
	vector<specpeakcluster> DBScanTbl;		//as many reflectors as DBScan-clusterable specific peaks above threshold

	//temporaries
	//vector<iontype_ival> IV; 				//size = 1 first value left nrow bound, second value right nrow bound
	//vector<float> FFTBinsTbl;				//nrows = 1 * NumberOfSO2Directions, ncols = 2^(m-1))

	//CPU buffers
	vector<p3d> DiffVecNeighbors;			//size = number of neighbor ions within ROI found, the array is structured into blocks of ions of the same type
	vector<unsigned int> SDMHistoTblU32;	//nrows = 1 * NumberOfSO2Directions, ncols = 2^m
	vector<float> SDMHistoTblF32;			//float version of above U32

	double tictoc[TICTOC_NCOLS];
};


class acquisor
{
public:
	acquisor();
	~acquisor();

	void configure();

	binning_info cfg;
	vector<float> window_coeff;
	vector<float> reciprocal_pos;
	//vector<pair<int,araullo_res*>> res;
	vector<araullo_res_node> res;
};

#endif
