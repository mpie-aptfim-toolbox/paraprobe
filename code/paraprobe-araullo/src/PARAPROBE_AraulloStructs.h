//##MK::GPLV3

#ifndef __PARAPROBE_ARAULLO_STRUCTS_H__
#define __PARAPROBE_ARAULLO_STRUCTS_H__

//here is the level where we loop in all tool-specific utils
//#include "PARAPROBE_AraulloHDF5.h"
//#include "PARAPROBE_AraulloXDMF.h"
#include "PARAPROBE_AraulloHdlGPU.h"

struct elaz
{
	apt_real elevation;
	apt_real azimuth;
	elaz() : elevation(0.0), azimuth(0.0) {}
	elaz( const apt_real _el, const apt_real _az ) : elevation(_el), azimuth(_az) {}
};

ostream& operator << (ostream& in, elaz const & val);


struct iontype_ival
{
	size_t incl_left;
	size_t excl_right;
	size_t m;
	iontype_ival() : incl_left(0), excl_right(0), m(0) {}
	iontype_ival( const size_t _left, const size_t _right ) :
		incl_left(_left), excl_right(_right), m(0) {}
	iontype_ival( const size_t _left, const size_t _right, const size_t _m ) :
		incl_left(_left), excl_right(_right), m(_m) {}
};

ostream& operator << (ostream& in, iontype_ival const & val);


struct binning_info
{
	apt_real R;
	apt_real dR;
	apt_real binner;			//multiplier for NumberOfBins
	apt_real binner2;			//multiplier for NumberOfBins2
	apt_real RdR;				//intermediate
	unsigned int Rpadding; 		//per side
	unsigned int NumberOfBins;
	unsigned int NumberOfBinsHalf;
	binning_info() : R(0.f), dR(0.f), binner(0.f), binner2(0.f), RdR(0.f), Rpadding(0), NumberOfBins(0), NumberOfBinsHalf(0) {}
	binning_info( const apt_real _R, const apt_real _dR, const apt_real _binner, const apt_real _binner2, const apt_real _RdR,
			const unsigned int _rpad, const unsigned int _nbins, const unsigned int _nbins2 ) :
				R(_R), dR(_dR), binner(_binner), binner2(_binner2), RdR(_RdR), Rpadding(_rpad), NumberOfBins(_nbins), NumberOfBinsHalf(_nbins2) {}
};

ostream& operator << (ostream& in, binning_info const & val);


struct specpeakcluster
{
	apt_real SUMIntensity;
	apt_real MAXIntensity;
	apt_real PksCnts;
	apt_real xmax;				//location of the node with maximum intensity
	apt_real ymax;
	apt_real zmax;
	apt_real xmean;					//location of the equally-weighted average
	apt_real ymean;
	apt_real zmean;

	specpeakcluster() : SUMIntensity(0.0), MAXIntensity(0.0), PksCnts(0.0),
			xmax(0.0), ymax(0.0), zmax(0.0), xmean(0.0), ymean(0.0), zmean(0.0) {}
	specpeakcluster( const apt_real _sum, const apt_real _max, const apt_real _fcnts,
			const apt_real _xmx, const apt_real _ymx, const apt_real _zmx,
				const apt_real _xmn, const apt_real _ymn, const apt_real _zmn ) :
		SUMIntensity(_sum), MAXIntensity(_max), PksCnts(_fcnts),
			xmax(_xmx), ymax(_ymx), zmax(_zmx),
				xmean(_xmn), ymean(_ymn), zmean(_zmn) {}
};

ostream& operator << (ostream& in, specpeakcluster const & val);


struct dbsc3d
{
	apt_real val;				//intensity (of specific peak)
	apt_real x;					//reprojected position on unit sphere surface
	apt_real y;
	apt_real z;
	unsigned int clbl;			//cluster id (UINT32MX is noise)
	bool visited;				//temporaries during DBScanning
	bool pad1;
	bool pad2;
	bool pad3;
	dbsc3d() : val(0.0), x(0.f), y(0.f), z(0.f), clbl(UINT32MX), visited(false),
			pad1(false), pad2(false), pad3(false) {}
	dbsc3d( const apt_real _val, const apt_real _x, const apt_real _y, const apt_real _z ) :
		val(_val), x(_x), y(_y), z(_z), clbl(UINT32MX), visited(false), pad1(false), pad2(false), pad3(false) {}
};


struct threepeaks
{
	//apt_real elevation;
	//apt_real azimuth;
	pair<float,float> max1; //bin, val
	pair<float,float> max2;
	pair<float,float> max3;
	threepeaks() : 	max1(pair<float,float>(0.f,0.f)),
					max2(pair<float,float>(0.f,0.f)),
					max3(pair<float,float>(0.f,0.f)) {}
	threepeaks( pair<float,float> const & _m1, pair<float,float> const & _m2, pair<float,float> const & _m3 ) :
			max1(_m1), max2(_m2), max3(_m3) {}
};

ostream& operator << (ostream& in, threepeaks const & val);

#endif
