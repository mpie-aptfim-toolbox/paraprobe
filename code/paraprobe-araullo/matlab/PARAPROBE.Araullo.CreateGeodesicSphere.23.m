% PARAPROBE.Araullo.CreateGeodesicSphere.26.m
% Markus K\"uhbach, 2019
% crystallographic directions for verifcation purposes
tic
    clear;
    clc;
    format long;
    
% load dat file from Matthew and translate into H5 format
    PARAPROBE_ARAULLO_META_DIR_ELAZ = '/AraulloPeters/Metadata/Directions/ElevAzim';
    %h5fn = 'Y:\GITHUB\MPIE_APTFIM_TOOLBOX\paraprobe\code\paraprobe-surfacer\run\PARAPROBE.Surfacer.SimID.0.apth5';
    %h5read(h5fn, dsnm, DAT);
    
    XYZ26 = zeros(26,3);
    XYZ26[1,:] = [+1,0,0];
    XYZ26[2,:] = [-1,0,0];
    XYZ26[3,:] = [0,+1,0];
    XYZ26[4,:] = [0,-1,0];
    XYZ26[5,:] = [0,0,+1];
    XYZ26[6,:] = [0,0,-1];
    
    XYZ26
    
    filename = 'GeodesicSphere40962.dat';
    delimiter = ';';
    startRow = 3;
    formatSpec = '%f%f%[^\n\r]';
    fileID = fopen(filename,'r');
    dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string', 'EmptyValue', NaN, 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
    fclose(fileID);
    GeodesicSphere40962 = single([dataArray{1:end-1}]);
    clearvars filename delimiter startRow formatSpec fileID dataArray ans;
    
%% transpose from Fortran to Cstyle order
    [nr, nc] = size(GeodesicSphere40962);
    for c=1:1:nc
        DAT(c,1:nr) = reshape( GeodesicSphere40962(:,c), [1, nr]);
    end
    
%% safe Matlab object and H5 file
    save('PARAPROBE.Araullo.GeodesicSphere.40962.mat','-v7.3');
    %h5fn = 'H:\BIGMAX_RELATED\MPIE-APTFIM-TOOLBOX\paraprobe\code\paraprobe-araullo\matlab\PARAPROBE.Araullo.GeodesicSphere40962.h5';
    %dsnm = '/ElevationAzimuth';
    h5fn = 'PARAPROBE.Araullo.GeodesicSphere.40962.h5';
    dsnm = PARAPROBE_ARAULLO_META_DIR_ELAZ;
    h5create(h5fn, dsnm, size(DAT));
    h5write(h5fn, dsnm, DAT);
toc
