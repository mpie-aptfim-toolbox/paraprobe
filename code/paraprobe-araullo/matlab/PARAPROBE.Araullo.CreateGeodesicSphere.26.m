% PARAPROBE.Araullo.CreateGeodesicSphere.26.m
% Markus K\"uhbach, 2019
% crystallographic directions for verifcation purposes
tic
    clear;
    clc;
    format long;
    
% load dat file from Matthew and translate into H5 format
    PARAPROBE_ARAULLO_META_DIR_ELAZ = '/AraulloPeters/Metadata/Directions/ElevAzim';
    %h5fn = 'Y:\GITHUB\MPIE_APTFIM_TOOLBOX\paraprobe\code\paraprobe-surfacer\run\PARAPROBE.Surfacer.SimID.0.apth5';
    %h5read(h5fn, dsnm, DAT);
    
    XYZ26 = zeros(26,3);
    XYZ26( 1,:) = [+1,0,0];
    XYZ26( 2,:) = [-1,0,0];
    XYZ26( 3,:) = [0,+1,0];
    XYZ26( 4,:) = [0,-1,0];
    XYZ26( 5,:) = [0,0,+1];
    XYZ26( 6,:) = [0,0,-1];
    
    XYZ26( 7,:) = [+1,+1,0];
    XYZ26( 8,:) = [+1,-1,0];
    XYZ26( 9,:) = [-1,+1,0];
    XYZ26(10,:) = [-1,-1,0];    
    XYZ26(11,:) = [0,+1,+1];
    XYZ26(12,:) = [0,+1,-1];
    XYZ26(13,:) = [0,-1,+1];
    XYZ26(14,:) = [0,-1,-1];    
    XYZ26(15,:) = [+1,0,+1];
    XYZ26(16,:) = [+1,0,-1];
    XYZ26(17,:) = [-1,0,+1];
    XYZ26(18,:) = [-1,0,-1];
    
    XYZ26(19,:) = [+1,+1,+1];
    XYZ26(20,:) = [+1,+1,-1];
    XYZ26(21,:) = [+1,-1,+1];
    XYZ26(22,:) = [+1,-1,-1];
    XYZ26(23,:) = [-1,+1,+1];
    XYZ26(24,:) = [-1,+1,-1];
    XYZ26(25,:) = [-1,-1,+1];
    XYZ26(26,:) = [-1,-1,-1];
    % normalize
    for i=1:1:26
        nrm = 1.0 / sqrt(XYZ26(i,1)^2 + XYZ26(i,2)^2 + XYZ26(i,3)^2); 
        XYZ26(i,:) = XYZ26(i,:) * nrm;
    end
    %https://www.wolframalpha.com/input/?i=x+%3D+cos%28a%29*c+and+y+%3D+sin%28a%29*c+solve+for+a  
    GeodesicSphere26 = zeros(26,2);
    for i = 1:1:26
        % normalize
        clearvars el az S;
        syms el az;
        S = solve( cos(az)*cos(el) == XYZ26(i,1), ...
                   sin(az)*cos(el) == XYZ26(i,2), ...
                   sin(el) == XYZ26(i,3));
        GeodesicSphere26(i,:) = [S.el(1), S.az(1)];
        GeodesicSphere26(i,:) = [mod(S.el(1),2*pi), mod(S.az(1),2*pi)];
        display(num2str(i));
    end
    % verify reverse computation
    TMP = zeros(26,3);
    for i = 1:1:26
        TMP(i,:) = [ cos(GeodesicSphere26(i,2))*cos(GeodesicSphere26(i,1)), ...
                     sin(GeodesicSphere26(i,2))*cos(GeodesicSphere26(i,1)), ...
                     sin(GeodesicSphere26(i,1)) ];
    end
    %kill numeric noise
    TMP(abs(TMP) < eps) = 0.0;
    DIFF = zeros(26,3);
    for i = 1:1:26 
        DIFF(i,:) = XYZ26(i,:) - TMP(i,:);
    end
    DIFF(abs(DIFF) < 2.0*eps) = 0.0;
        
    %filename = 'GeodesicSphere40962.dat';
    %delimiter = ';';
    %startRow = 3;
    %formatSpec = '%f%f%[^\n\r]';
    %fileID = fopen(filename,'r');
    %dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string', 'EmptyValue', NaN, 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
    %fclose(fileID);
    %GeodesicSphere40962 = single([dataArray{1:end-1}]);
    %clearvars filename delimiter startRow formatSpec fileID dataArray ans;
    
%% transpose from Fortran to Cstyle order
    [nr, nc] = size(GeodesicSphere26);
    for c=1:1:nc
        DAT(c,1:nr) = reshape( GeodesicSphere26(:,c), [1, nr]);
    end
    clearvars c i nrm S;
    
%% safe Matlab object and H5 file
    save('PARAPROBE.Araullo.GeodesicSphere.26.mat','-v7.3');
    %h5fn = 'H:\BIGMAX_RELATED\MPIE-APTFIM-TOOLBOX\paraprobe\code\paraprobe-araullo\matlab\PARAPROBE.Araullo.GeodesicSphere40962.h5';
    %dsnm = '/ElevationAzimuth';
    h5fn = 'PARAPROBE.Araullo.GeodesicSphere.26.h5';
    dsnm = PARAPROBE_ARAULLO_META_DIR_ELAZ;
    h5create(h5fn, dsnm, size(DAT));
    h5write(h5fn, dsnm, DAT);
toc
