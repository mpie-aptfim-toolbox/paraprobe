//##MK::GPLV3

#include "CONFIG_Ranger.h"


RANGING_METHOD ConfigRanger::RangingMethod = USE_EXISTENT_RANGEFILE;

//string ConfigRanger::InputfileComposition = "";
//string ConfigRanger::InputfileContaminants = "";
string ConfigRanger::InputfileReconstruction = "";
string ConfigRanger::InputfileRangingData = "";

int ConfigRanger::PeakSearchMaximumComponents = 3;
unsigned short int ConfigRanger::PeakSearchMaximumCharge = 7;
vector<peaksearch_filter> ConfigRanger::PeakSearchFilters = vector<peaksearch_filter>();
//apt_real ConfigRanger::PeakSearchM2QMin = 0.0;
//apt_real ConfigRanger::PeakSearchM2QMax = 0.0;

lival ConfigRanger::AMUBinning = lival();

unsigned int ConfigRanger::SNIPIterations = 20;
unsigned int ConfigRanger::SavitzkyGolayM = 0;
unsigned int ConfigRanger::PeaksWindowingHalfwidth = 10;

apt_real ConfigRanger::PeaksSignalToNoiseRatio = 2.0;
apt_real ConfigRanger::RangingFWHM = 0.9;
apt_real ConfigRanger::RangingFlankBumpTol = 1.0;

apt_real ConfigRanger::PracticalMassToChargeResolution = 1.0 / 2000.0; //Da


bool ConfigRanger::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.Ranger.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigRanger")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;
  
	mode = read_xml_attribute_uint32( rootNode, "RangingMode" );
	switch (mode)
	{
		case USE_EXISTENT_RANGEFILE:
			RangingMethod = USE_EXISTENT_RANGEFILE; break;
		case PEAK_SEARCH_VALUES:
			RangingMethod = PEAK_SEARCH_VALUES; break;
		case PEAK_SEARCH_SPECTRUM:
			RangingMethod = PEAK_SEARCH_SPECTRUM; break;
		default:
			RangingMethod = USE_EXISTENT_RANGEFILE;
			break;
	}
	if ( RangingMethod == PEAK_SEARCH_SPECTRUM ) {
		cerr << "RangingMethod PEAK_SEARCH_SPECTRUM is currently not implemented !" << "\n";
		return false;
	}
	
	if ( RangingMethod != PEAK_SEARCH_VALUES ) {
		InputfileReconstruction = read_xml_attribute_string( rootNode, "InputfileReconstruction" );
	
		if ( RangingMethod == USE_EXISTENT_RANGEFILE ) {
			InputfileRangingData = read_xml_attribute_string( rootNode, "InputfileRangingData" );
		}

		if ( RangingMethod == PEAK_SEARCH_SPECTRUM ) {
			//InputfileComposition = read_xml_attribute_string( rootNode, "InputfileComposition" );
			//InputfileContaminants = read_xml_attribute_string( rootNode, "InputfileContaminants" );
		
			AMUBinning = lival( 0.0,
								read_xml_attribute_float( rootNode, "AMUBinningWidth" ),
								read_xml_attribute_float( rootNode, "AMUBinningMax" )    );
        
			SNIPIterations = read_xml_attribute_uint32( rootNode, "SNIPIterations" );
			SavitzkyGolayM = read_xml_attribute_uint32( rootNode, "SavitzkyGolayM" );
			PeaksWindowingHalfwidth = read_xml_attribute_uint32( rootNode, "PeakDetectionWindowingHalfwidth" );
			PeaksSignalToNoiseRatio = read_xml_attribute_float( rootNode, "PeakDetectionSignalToNoise" );
			RangingFWHM = read_xml_attribute_float( rootNode, "RangingFWHM" );
			RangingFlankBumpTol = read_xml_attribute_float( rootNode, "RangingFlankBumpTol" );
		}
	}
	else { //PEAK_SEARCH_VALUES
		PeakSearchMaximumComponents = read_xml_attribute_int32( rootNode, "PeakSearchMaximumNumberOfComponents" );
		PeakSearchMaximumCharge = read_xml_attribute_uint16( rootNode, "PeakSearchMaximumCharge" );

		xml_node<>* comb_node = rootNode->first_node("PeakSearchFilters");
		PeakSearchFilters = vector<peaksearch_filter>();
		for (xml_node<> * entry_node = comb_node->first_node("entry"); entry_node; entry_node = entry_node->next_sibling() ) {
			apt_real mqmin = 0.0;
			apt_real mqmax = 0.0;
			string targ = "";
#ifdef EMPLOY_SINGLEPRECISION
			mqmin = stof(entry_node->first_attribute("min")->value());
			mqmax = stof(entry_node->first_attribute("max")->value());
#else
			mqmin = stod(entry_node->first_attribute("min")->value());
			mqmax = stod(entry_node->first_attribute("max")->value());
#endif

			targ = entry_node->first_attribute("targets")->value();

			if ( mqmin > EPSILON && (mqmax - mqmin) >= PracticalMassToChargeResolution  ) {
				PeakSearchFilters.push_back( peaksearch_filter( mqmin, mqmax, targ ) );
			}
			else {
				cerr << "PeakSearchFilters entry " << mqmin << ";" << mqmax << ";" << targ << " is invalid input, mqmin >= EPSILON, and mqmax - mqmin >= " << PracticalMassToChargeResolution << " !" << "\n";
				return false;
			}
		}
	}
    
	return true;
}


bool ConfigRanger::checkUserInput()
{
	cout << "ConfigRanger::" << "\n";
	switch (RangingMethod)
	{
		case USE_EXISTENT_RANGEFILE:
			cout << "Using external ranging data from e.g. USE_EXISTENT_RANGEFILE" << "\n";
			break;
		case PEAK_SEARCH_VALUES:
			cout << "Finding candidates for a specific peak" << "\n";
			break;
		case PEAK_SEARCH_SPECTRUM:
			cerr << "Find peaks in a spectrum and find candidates for all these peaks (currently not implemented !)" << "\n";
			return false;
		default:
			cerr << "Unknown ranging mode !" << "\n";
			return false;
	}

    if ( RangingMethod != PEAK_SEARCH_VALUES ) {
       	if ( InputfileReconstruction.substr(InputfileReconstruction.length()-3) != ".h5" ) {
    		cerr << "InputfileReconstruction refers to an invalid file format ending, only .h5 is accepted !" << "\n"; return false;
    	}
    	cout << "InputfileReconstruction read from " << InputfileReconstruction << "\n";

    	if ( RangingMethod == USE_EXISTENT_RANGEFILE ) {
			if ( InputfileRangingData.substr(InputfileRangingData.length()-3) != ".h5" ) {
				 cerr << "InputfileRangingData contains invalid file format ending, only .h5 is accepted !" << "\n"; return false;
			}
			cout << "InputfileRangingData read from " << InputfileRangingData << "\n";
    	}

    	if ( RangingMethod == PEAK_SEARCH_SPECTRUM ) {
			//cout << "Reading composition from " << InputfileComposition << "\n";
			//cout << "Reading contaminants from " << InputfileContaminants << "\n";
			if ( AMUBinning.incr < EPSILON ) {
				cerr << "AMUBinningIncr must be positive and finite!" << "\n"; return false;
			}
			if ( AMUBinning.max < EPSILON ) {
				cerr << "AMUBinningMax must be positive and finite!" << "\n"; return false;
			}
			if ( AMUBinning.max < AMUBinning.min ) {
				cerr << "AMUBinningMax must be at least as large as AMUBinningMin!" << "\n"; return false;
			}
			cout << "AMUBinningMin " << AMUBinning.min << " amu" << "\n";
			cout << "AMUBinningIncr " << AMUBinning.incr << " amu" << "\n";
			cout << "AMUBinningMax " << AMUBinning.max << " amu" << "\n";
			//accept SNIPIterations == 0 will not perform SNIP
			cout << "SNIPIterations " << SNIPIterations << "\n";
			//accept SavitzkyGolay == 0 will not do Savitzky Golay Smoothing
			if ( SavitzkyGolayM > 13 ) {
				cerr << "SavitzkyGolayM smoothing currently implemented only for m<=13!" << "\n"; return false;
			}
			cout << "SavitzkyGolayM " << SavitzkyGolayM << "\n";
			if ( PeaksWindowingHalfwidth < 1 ) {
				cerr << "PeaksWindowingHalfwidth must be at least 1!" << "\n"; return false;
			}
			cout << "PeaksWindowingHalfwidth " << PeaksWindowingHalfwidth << "\n";
			if ( PeaksSignalToNoiseRatio <= (1.0 - EPSILON) ) {
				cerr << "PeaksSignalToNoiseRatio too small, peaks have to be at least the noise level!" << "\n"; return false;
			}
			cout << "PeaksSignalToNoiseRatio " << PeaksSignalToNoiseRatio << "\n";
			if ( RangingFWHM < 0.1 ) {
				cerr << "RangingWidth should be at least 0.1!" << "\n"; return false;
			}
			cout << "RangingFWHM " << RangingFWHM << "\n";
			if ( RangingFlankBumpTol < EPSILON ) {
				cerr << "RangingFlankBumpTolerance must be positive and finite!" << "\n"; return false;
			}
			cout << "RangingFlankBumpTol " << RangingFlankBumpTol << "\n";
		}
    }
    else { //PEAK_SEARCH_VALUES
    	if ( PeakSearchMaximumComponents < 1 || PeakSearchMaximumComponents > APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS	 ) {
    		cerr << "PeakSearchMaximumComponents needs to be an integer from the interval [1, " << APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS << "] !" << "\n"; return false;
    	}
    	cout << "PeakSearchMaximumComponents " << PeakSearchMaximumComponents << "\n";
    	if ( PeakSearchMaximumCharge < 1 || PeakSearchMaximumCharge > APTMOLECULARION_MAX_CHARGE ) {
    		cerr << "PeakSearchMaximumCharge needs to be an integer from the interval [1, " << APTMOLECULARION_MAX_CHARGE << "] !" << "\n"; return false;
    	}
    	cout << "PeakSearchMaximumCharge " << PeakSearchMaximumCharge << "\n";
    	for ( auto it = PeakSearchFilters.begin(); it != PeakSearchFilters.end(); it++ ) {
    		if ( it->iv.lo < 0.0 ) {
    			cerr << "PeakSearchFiltersMassToChargeMin must be positive and finite!" << "\n"; return false;
    		}
    		if ( it->iv.hi < 0.0 ) {
    			cerr << "PeakSearchFiltersMassToChargeMax must be positive and finite!" << "\n"; return false;
    		}
    		if ( (it->iv.hi - it->iv.lo) < PracticalMassToChargeResolution ) { //Da, practical resolution limit APT
    			cerr << "PeakSearchFiltersMassToChargeMax needs to be at least " << PracticalMassToChargeResolution << " Da larger than PeakSearchFiltersMassToChargeMin !" << "\n"; return false;
    	    }
    		cout << "PeakSearchFilters entry " << it->iv.lo << ";" << it->iv.hi << ";" << it->targets << "\n";
    	}
    }

    cout << "PracticalMassToChargeResolution " << ConfigRanger::PracticalMassToChargeResolution << "\n";

	cout << "\n";
	return true;
}


void ConfigRanger::reportSettings( vector<pparm> & res)
{
	res.push_back( pparm( "RangingMethod", sizet2str(RangingMethod), "", "which internal method to execute the ranging" ) );
	if ( RangingMethod != PEAK_SEARCH_VALUES ) {
		res.push_back( pparm( "InputfileReconstruction", InputfileReconstruction, "", "pre-computed reconstruction space x,y,z" ) );

		if ( RangingMethod == USE_EXISTENT_RANGEFILE ) {
			res.push_back( pparm( "InputfileRangingData", "InputfileRangingData", "", "pre-computed information how to map mass-to-charge-state ratios to atom types" ) );
		}
		if ( RangingMethod == PEAK_SEARCH_SPECTRUM ) {
			//##MK::report quantities
		}
	}
	else {
		res.push_back( pparm( "PeakSearchMaximumAllowedNumberOfComponents", uint2str(PeakSearchMaximumComponents), "", "what is the maximum number of (not necessarily disjoint) nuclids building the (molecular) ions we are searching" ) );
		res.push_back( pparm( "PeakSearchMaximumSupportedNumberOfComponents", uint2str(APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS), "", "what is the maximum number of (not necessarily disjoint) nuclids which the implementation supports" ) );
		res.push_back( pparm( "PeakSearchMaximumAllowedCharge", uint2str(PeakSearchMaximumCharge), "e", "what is the maximum charge of the (molecular) ions we are searching" ) );
		res.push_back( pparm( "PeakSearchMaximumSupportedCharge", uint2str(APTMOLECULARION_MAX_CHARGE), "e", "what is the maximum charge of the (molecular) ions which the implementation supports" ) );
		res.push_back( pparm( "PracticalMassToChargeResolutionLimit", real2str(PracticalMassToChargeResolution), "Da", "what is practically the lowest mass-to-charge difference identifiable" ) );

		/*
		for ( auto it = PeakSearchFilters.begin(); it != PeakSearchFilters.end(); it++ ) {
			size_t i = it - PeakSearchFilters.begin();
			//if ( i > 1000 )
			string key = "PeakSearchFilters" + to_string(i);
			res.push_back( pparm( key + "Min", real2str(it->lo), "Da", "left-bound of mass-to-charge spectrum interval where to find candidates in") );
			res.push_back( pparm( key + "Max", real2str(it->hi), "Da", "right-bound of mass-to-charge spectrum interval where to find candidates in") );
		}
		*/
	}

	//res.push_back( pparm( "InputfileComposition", InputfileComposition, "", "pre-computed composition table" ) );
	//res.push_back( pparm( "InputfileContaminants", InputfileContaminants, "", "pre-computed information about which elements are considered contaminants" ) );
}
