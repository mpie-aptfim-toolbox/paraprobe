//##MK::GPLV3

#include "PARAPROBE_RangerXDMF.h"


ranger_xdmf::ranger_xdmf()
{
	
}
	
ranger_xdmf::~ranger_xdmf()
{
	
}


/*
int ranger_xdmf::create_materialpoint_file( const string xmlfn, const size_t nmp, const string h5ref )
{
	xdmfout.open( xmlfn.c_str() );
	if ( xdmfout.is_open() == true ) {
		xdmfout << XDMF_HEADER_LINE1 << "\n";
		xdmfout << XDMF_HEADER_LINE2 << "\n";
		xdmfout << XDMF_HEADER_LINE3 << "\n";

		xdmfout << "  <Domain>" << "\n";
		xdmfout << "    <Grid Name=\"matpoints\" GridType=\"Uniform\">" << "\n";
		xdmfout << "      <Topology TopologyType=\"Mixed\" NumberOfElements=\"" << nmp << "\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << 3*nmp << "\" NumberType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5ref << ":" << PARAPROBE_ARAULLO_META_MATPNT_TOPO << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Topology>" << "\n";

		xdmfout << "      <Geometry GeometryType=\"XYZ\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nmp << " 3\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5ref << ":" << PARAPROBE_ARAULLO_META_MATPNT_XYZ << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Geometry>" << "\n";

		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"MPID\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nmp << " 1\" DataType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5ref << ":" << PARAPROBE_ARAULLO_META_MATPNT_IDS << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";

		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"ROIRadius\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nmp << " 1\" DataType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5ref << ":" << PARAPROBE_ARAULLO_META_MATPNT_R << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";

 		xdmfout << "    </Grid>" << "\n";
		xdmfout << "  </Domain>" << "\n";
		xdmfout << "</Xdmf>" << "\n";

		xdmfout.flush();
		xdmfout.close();

		return WRAPPED_XDMF_SUCCESS;
	}
	else {
		return WRAPPED_XDMF_IOFAILED;
	}
}
*/

int ranger_xdmf::create_volrecon_file( const string xmlfn, const size_t nions, const string h5ref )
{
	xdmfout.open( xmlfn.c_str() );
	if ( xdmfout.is_open() == true ) {
		xdmfout << XDMF_HEADER_LINE1 << "\n";
		xdmfout << XDMF_HEADER_LINE2 << "\n";
		xdmfout << XDMF_HEADER_LINE3 << "\n";

		xdmfout << "  <Domain>" << "\n";
		xdmfout << "    <Grid Name=\"volrecon\" GridType=\"Uniform\">" << "\n";
		xdmfout << "      <Topology TopologyType=\"Mixed\" NumberOfElements=\"" << nions << "\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << 3*nions << "\" NumberType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5ref << ":" << PARAPROBE_SYNTH_VOLRECON_RES_TOPO << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Topology>" << "\n";
		xdmfout << "      <Geometry GeometryType=\"XYZ\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nions << " 3\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5ref << ":" << PARAPROBE_SYNTH_VOLRECON_RES_XYZ << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Geometry>" << "\n";

		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"Iontype\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nions << " 1\" DataType=\"UInt\" Precision=\"1\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5ref << ":" << PARAPROBE_RANGER_RES_TYPID << "\n";
		xdmfout << "       </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";

		/*
		if ( Settings::IOIonTipSurfDists == true ) {
			xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"SqrdDist\">" << "\n";
			xdmfout << "	    <DataItem Dimensions=\"" << nions << " 1\" DataType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
			xdmfout << "          " << h5ref << ":" << PARAPROBE_VOLRECON_SURFDISTSQR << "\n";
			xdmfout << "        </DataItem>" << "\n";
			xdmfout << "      </Attribute>" << "\n";
		}
		*/
 		xdmfout << "    </Grid>" << "\n";
		xdmfout << "  </Domain>" << "\n";
		xdmfout << "</Xdmf>" << "\n";

		xdmfout.flush();
		xdmfout.close();

		return WRAPPED_XDMF_SUCCESS;
	}
	else {
		return WRAPPED_XDMF_IOFAILED;
	}
}


