//##MK::GPLV3

#include "PARAPROBE_RangerHdl.h"


//define what is passed upon execution
#define SIMID									1
#define CONTROLFILE								2

//##MK::for querying system maximum physical memory see
//https://stackoverflow.com/questions/2513505/how-to-get-available-memory-c-g

#ifndef GITSHA									//otherwise we could not compile without adding -DGITSHA="d123" a dummy GitSha
	#define GITSHA								0
#endif
//stringification of strings passed from cmake at compile time
//https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html
#define xstr(s)									str(s)
#define str(s) 									#s


void hello( void )
{
	cout << "paraprobe-ranger" << "\n";
	cout << xstr(GITSHA) << "\n";
	cout << "Collecting " << CiteMeRanger::cite() << " publications for the tool to cite:" << "\n";
	for( auto it = CiteMeRanger::Citations.begin(); it != CiteMeRanger::Citations.end(); it++ ) {
		cout << it->author << "\n";
		cout << it->title << "\n";
		cout << it->journal << ", " << it->year << ", " << it->vol << ", pp" << it->pages << "\n";
		cout << it->doi << "\n";
	}
}


bool init(  int pargc, char** pargv )
{
    ConfigShared::SimID = stoul( pargv[SIMID] );
	try {
		ConfigRanger::readXML(pargv[CONTROLFILE]);
	}
	catch (exception& e) {
		cerr << "\n" << "Unable to parse control file! Details:" << "\n" << e.what() << "\n"; return false;
	}
	if ( ConfigRanger::checkUserInput() == false ) {
		cerr << "\n" << "Control file settings failed the validity check!" << "\n"; return false;
	}
	else {
		cout << "\n" << "\t\tInput is valid under SimulationID = " << "SimID." <<  ConfigShared::SimID << "\n";
	}
	
	cout << endl;
	return true;
}


void ranging( const int r, const int nr )
{
	//allocate process-level instance of a rangeHdl. The instance handles all process-internal
	//processing, the instances synchronize across and communicate with each other during execution
	rangerHdl* rngr = NULL;

	int localhealth = 1;
	int globalhealth = nr;
	try {
		rngr = new rangerHdl;
		rngr->set_myrank(r);
		rngr->set_nranks(nr);
		//rngr->init_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Unable to allocate an rangerHdl class object instance" << "\n"; localhealth = 0;
	}

	//load periodic table of elements and isotope to prepare ranging
	/*
	if ( rngr->rng.nuclides.load_isotope_library( ConfigRanger::InputfilePSE ) == false ) {
		cerr << "Rank " << MASTER << " loading of PeriodicTableOfElements failed!" << "\n";
		delete rngr; rngr = NULL; return;
	}
	*/
	if ( rngr->rng.nuclides.load_isotope_library() == false ) {
		cerr << "Rank " << r << " loading of Config_SHARED PeriodicTableOfElements failed!" << "\n";
		delete rngr; rngr = NULL; return;
	}

	//do we have all processes on board?
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete rngr; rngr = NULL; return;
	}
	
	//we have all on board, read reconstruction
	//MK::we could have all processes loading from the same file, or have only MASTER do so and broadcast
	//we opt for the latter strategy
	//##MK::should not be a problem because initialization is orders of magnitude cheaper than actual calculations...
	if ( ConfigRanger::RangingMethod != PEAK_SEARCH_VALUES ) {
		if ( rngr->get_myrank() == MASTER ) {
			if ( rngr->read_reconxyz_from_apth5() == true ) {
				cout << "MASTER read successfully all ion coordinates of the specimen in reconstruction space" << "\n";
			}
			else {
				cerr << "MASTER was unable to read a specimen in reconstruction space!" << "\n"; localhealth = 0;
			}
			if ( rngr->read_mass2charge_from_apth5() == true ) {
				cout << "MASTER read successfully all mass-to-charge values of the specimen" << "\n";
			}
			else {
				cerr << "MASTER was unable to read mass-to-charge values!" << "\n"; localhealth = 0;
			}
			if ( rngr->read_ranging_from_apth5() == true ) {
				cout << "MASTER read successfully all ranging data" << "\n";
			}
			else {
				cerr << "MASTER was unable to read ranging data!" << "\n"; localhealth = 0;
			}

			if ( rngr->xyz.size() != rngr->m2q.size() ) {
				cerr << "Not every ion seems to have a mass-to-charge value!" << "\n"; localhealth = 0;
			}
		}
		//else {} ##no slaves
		//##MK::strictly speaking not necessary, but second order issue for about 80 processes as on TALOS...?
		//MPI_Barrier( MPI_COMM_WORLD );

		globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != nr ) {
			cerr << "Rank " << r << " has recognized that all data have arrived!" << "\n";
			delete rngr; rngr = NULL;
			return;
		}

		if ( ConfigRanger::RangingMethod == USE_EXISTENT_RANGEFILE ) {
			rngr->apply_existent_ranging();
		}

		//MK::ranging result can be cached, so target file created after the applying of existent ranging
		if ( rngr->init_target_file() == true ) {
			cout << "Rank MASTER successfully initialized APTH5 results file" << "\n";
		}
		else {
			cerr << "Rank MASTER unable to initialize results APTH5 file" << "\n"; localhealth = 0;
		}

		if ( rngr->write_environment_and_settings() == true ) {
			cout << "Rank " << MASTER << " environment and settings written!" << "\n";
		}
		else {
			cerr << "Rank " << MASTER << " writing environment and settings failed!" << "\n"; localhealth = 0;
		}

		//MK::no initialization of a new file
		//InputfileReconstruction is supplemented!

		if ( rngr->write_metadata_to_apth5() == true ) { //does not require MPI communication all results already on the master
			cout << "Rank MASTER successfully wrote all materialpoints to an APTH5 file" << "\n";
		}
		else {
			cerr << "Rank MASTER unable to write all materialpoints to the APTH5 file" << "\n"; localhealth = 0;
		}

		cout << "Rank " << r << " has completed its workpackage" << "\n";
		//no barrier required SINGLE_PROCESS only MPI_Barrier(MPI_COMM_WORLD);

		//no sync of processes required when SINGLE_PROCESS
		//MPI_Barrier( MPI_COMM_WORLD );

		if ( rngr->write_ranging_to_apth5() == true ) {
			cout << "Rank MASTER successfully wrote all materialpoints to an APTH5 file" << "\n";
		}
		else {
			cerr << "Rank MASTER unable to write all materialpoints to the APTH5 file" << "\n"; localhealth = 0;
		}
	}
	else { //PEAK_SEARCH_VALUES
		if ( rngr->get_myrank() == MASTER ) {

			//MK::for identifying molecular ions it might be possible that the results are not cacheable, due to memory restrictions
			//so better create the results file before the main computations, also the results file is different here as no ranging
			//is applied to a specific dataset
			if ( rngr->init_target_file() == true ) {
				cout << "Rank MASTER successfully initialized APTH5 results file" << "\n";
			}
			else {
				cerr << "Rank MASTER unable to initialize results APTH5 file" << "\n"; localhealth = 0;
			}

			if ( rngr->write_environment_and_settings() == true ) {
				cout << "Rank " << MASTER << " environment and settings written!" << "\n";
			}
			else {
				cerr << "Rank " << MASTER << " writing environment and settings failed!" << "\n"; localhealth = 0;
			}

			//main computations
			if ( rngr->identify_molecularions() == true ) {
				cout << "Rank MASTER successfully wrote possible molecularion candidates on mass-to-charge intervals to a H5 file" << "\n";
				//[" << ConfigRanger::PeakSearchM2QMin << ", " << ConfigRanger::PeakSearchM2QMax << "]" << " Da to a H5 file" << "\n";
			}
			else {
				cerr << "Rank MASTER unable to write molecularion candidates to APTH5 file" << "\n"; localhealth = 0;
			}
		}
	}

	//MPI_Barrier( MPI_COMM_WORLD );
    rngr->ranger_tictoc.spit_profiling( "Ranger", ConfigShared::SimID, rngr->get_myrank() );

	//release resources
	delete rngr; rngr = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();

	hello();
//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	//helloworld( argc, argv );

	if ( init( argc, argv ) == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks!" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else { 
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}
	
	//##MK::currently using MPI to be prepared for the future but only one process is required
	if ( nr == SINGLEPROCESS ) {
        
        cout << r << "-th MPI process initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";
    
//EXECUTE SPECIFIC TASK
        ranging( r, nr );
    }
    else {
        cerr << "Rank " << r << " currently paraprobe-ranger is implemented for a single process only!" << "\n";
    }
    
//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	cout << "Rank " << r << " left MPI_COMM_WORLD deconstructed" << endl;

	double toc = omp_get_wtime();
	cout << "paraprobe-ranger took " << (toc-tic) << " seconds wall-clock time in total" << endl;
	
	return 0;
}
