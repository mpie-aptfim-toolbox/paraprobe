//##MK::GPLV3

#include "PARAPROBE_RangerHDF5.h"
//https://www.learncpp.com/cpp-tutorial/114-constructors-and-initialization-of-derived-classes/


ranger_h5::ranger_h5()
{
}


ranger_h5::~ranger_h5()
{
}


int ranger_h5::create_ranger_apth5( const string h5fn )
{
	cout << "Creating new results file " << h5fn << "\n";
	h5resultsfn = h5fn;

	fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );
	if ( fileid < 0 ) {
		cerr << "Ranger apth5 file opening failed! " << fileid << "\n"; return WRAPPED_HDF5_FAILED;
	}

	//domain specific HDF5 keywords and data fields
	vector<string> grp_nms = { 	PARAPROBE_RANGER, PARAPROBE_RANGER_META,
								PARAPROBE_RANGER_META_HRDWR, PARAPROBE_RANGER_META_SFTWR,
								PARAPROBE_RANGER_RES  };
	for( auto it = grp_nms.begin(); it != grp_nms.end(); it++ ) {
		groupid = H5Gcreate2(fileid, it->c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group " << it->c_str() << " failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Closing group " << it->c_str() << " failed!" << "\n";
		}
	}

	//##MK::PARAPROBE_RANGER_META_TYPID_DICT
	if ( ConfigRanger::RangingMethod == PEAK_SEARCH_VALUES ) {
		groupid = H5Gcreate2(fileid, PARAPROBE_RANGER_META_PEAKFIND, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group " << PARAPROBE_RANGER_META_PEAKFIND << " failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Closing group " << PARAPROBE_RANGER_META_PEAKFIND << " failed!" << "\n";
		}

		groupid = H5Gcreate2(fileid, PARAPROBE_RANGER_RES_PEAKFIND, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group " << PARAPROBE_RANGER_RES_PEAKFIND << " failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Closing group " << PARAPROBE_RANGER_RES_PEAKFIND << " failed!" << "\n";
		}

		string grpnm = "";
		string fwslash = "/";
		for( auto it = ConfigRanger::PeakSearchFilters.begin(); it != ConfigRanger::PeakSearchFilters.end(); it++ ) {
			grpnm = PARAPROBE_RANGER_META_PEAKFIND + fwslash + to_string(it - ConfigRanger::PeakSearchFilters.begin());
			groupid = H5Gcreate2(fileid, grpnm.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			if ( groupid < 0 ) {
				cerr << "Create group " << grpnm.c_str() << " failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
			}
			status = H5Gclose(groupid);
			if ( status < 0 ) {
				cerr << "Closing group " << grpnm.c_str() << " failed!" << "\n";
			}

			grpnm = PARAPROBE_RANGER_RES_PEAKFIND + fwslash + to_string(it - ConfigRanger::PeakSearchFilters.begin());
			groupid = H5Gcreate2(fileid, grpnm.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			if ( groupid < 0 ) {
				cerr << "Create group " << grpnm.c_str() << " failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
			}
			status = H5Gclose(groupid);
			if ( status < 0 ) {
				cerr << "Closing group " << grpnm.c_str() << " failed!" << "\n";
			}
		}

	}

	status = H5Fclose(fileid);
	if ( status < 0 ) {
		cerr << "Close file " << h5resultsfn << " failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	return WRAPPED_HDF5_SUCCESS;
}


int ranger_h5::append_ranger_apth5( const string h5fn )
{
	cout << "Trying to append new data to " << h5fn << "\n";
	h5resultsfn = h5fn;
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
	if ( fileid < 0 ) {
		cerr << "Ranger apth5 file opening failed! " << fileid << "\n"; return WRAPPED_HDF5_FAILED;
	}

	//domain specific HDF5 keywords and data fields
	groupid = H5Gcreate2(fileid, PARAPROBE_RANGER, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_RANGER failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
    
    groupid = H5Gcreate2(fileid, PARAPROBE_RANGER_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_RANGER_META failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);

	groupid = H5Gcreate2(fileid, PARAPROBE_RANGER_META_HRDWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_RANGER_META_HRDWR failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);

	groupid = H5Gcreate2(fileid, PARAPROBE_RANGER_META_SFTWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_RANGER_META_SFTWR failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
    
    groupid = H5Gcreate2(fileid, PARAPROBE_RANGER_META_TYPID_DICT, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_RANGER_META_DICT failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
    
    groupid = H5Gcreate2(fileid, PARAPROBE_RANGER_RES, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_RANGER_RES failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);

/*
	//add environment and tool specific settings
	groupid = H5Gcreate2(fileid,  PARAPROBE_RANGER_HRDWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << " PARAPROBE_RANGER_HRDWR " << status << "\n";

	groupid = H5Gcreate2(fileid,  PARAPROBE_RANGER_HRDWR_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << " PARAPROBE_RANGER_HRDWR_META " << status << "\n";

	groupid = H5Gcreate2(fileid,  PARAPROBE_RANGER_HRDWR_META_KEYS, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << " PARAPROBE_RANGER_HRDWR_META_KEYS " << status << "\n";

	groupid = H5Gcreate2(fileid,  PARAPROBE_RANGER_SFTWR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << " PARAPROBE_RANGER_SFTWR " << status << "\n";

	groupid = H5Gcreate2(fileid,  PARAPROBE_RANGER_SFTWR_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << " PARAPROBE_RANGER_SFTWR_META " << status << "\n";

	groupid = H5Gcreate2(fileid,  PARAPROBE_RANGER_SFTWR_META_KEYS, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << " PARAPROBE_RANGER_SFTWR_META_KEYS " << status << "\n";
*/
    
	//close file
	status = H5Fclose(fileid);
	if ( status < 0 ) {
		cerr << "Close file " << h5resultsfn << " failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	return WRAPPED_HDF5_SUCCESS;
}
