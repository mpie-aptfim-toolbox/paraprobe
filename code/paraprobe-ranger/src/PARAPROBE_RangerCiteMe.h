//##MK::GPLV3


#ifndef __PARAPROBE_RANGER_CITEME_H__
#define __PARAPROBE_RANGER_CITEME_H__

#include "CONFIG_Ranger.h"


class CiteMeRanger
{
public:
	
	static vector<bibtex> Citations;
	
	static size_t cite();
};

#endif

