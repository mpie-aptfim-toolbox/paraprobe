//##MK::GPLV3


#ifndef __PARAPROBE_CONFIG_RANGER_H__
#define __PARAPROBE_CONFIG_RANGER_H__

#include "../../paraprobe-utils/src/PARAPROBE_Histogram.h"
#include "../../paraprobe-utils/src/CONFIG_Shared.h"
//#include "../../paraprobe-utils/src/PARAPROBE_FFT.h"


#define APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS		22
//El-Zoka et al. reported even peaks from D_{X}H_{15-X}O_{7}}^+ with X=15,14,...,0 hydrated fragments (frozen water)
//however playing this for all combinations of nuclids would end up with approximately 3000-ish ^ 22 / 0.5 different variants if
//including radioactive or at least 254^22, too many combinations
//therefore internally we suggest to reduce the search space using specific nuclids only and allow for
//substantially fewer nuclids (nuclids do not need to be disjoint !) in the molecular ions, e.g. 3 the larger the
//used portion of the nuclid map

#define APTMOLECULARION_MAX_CHARGE					7


enum RANGING_METHOD
{
	USE_EXISTENT_RANGEFILE,		//external ranging information, ranging was performed with APSuite/IVAS or manually
	PEAK_SEARCH_VALUES,			//identify which possible peaks on specified mass-to-charge interval
	PEAK_SEARCH_SPECTRUM		//identify all peaks on specified mass-to-charge interval
};


struct peaksearch_filter
{
	//filter criteria
	mqival iv;
	string targets;
	peaksearch_filter() : iv(mqival(0.0, 1000.0)), targets("") {}
	peaksearch_filter( const apt_real _lo, const apt_real _hi, const string _tg ) :
		iv(mqival(_lo, _hi)), targets(_tg) {}
};


class ConfigRanger
{
public:
	
	static RANGING_METHOD RangingMethod;
	//static string InputfileComposition;
	//static string InputfileContaminants;
	static string InputfileReconstruction;
	static string InputfileRangingData;
	
	static int PeakSearchMaximumComponents;
	static unsigned short int PeakSearchMaximumCharge;
	static vector<peaksearch_filter> PeakSearchFilters;
	static apt_real PeakSearchM2QMin;
	static apt_real PeakSearchM2QMax;


	static lival AMUBinning;

	//SNIP
	static unsigned int SNIPIterations;
	
	//Smoothing	
	static unsigned int SavitzkyGolayM;

	//Peak detection
	static unsigned int PeaksWindowingHalfwidth;
	static apt_real PeaksSignalToNoiseRatio;
	
	//ranging
	static apt_real RangingFWHM;
	static apt_real RangingFlankBumpTol;
	
	//sensible defaults
	static apt_real PracticalMassToChargeResolution;

	static bool readXML( string filename = "" );
	static bool checkUserInput();
	static void reportSettings( vector<pparm> & res);
};

#endif

