//##MK::GPLV3


#ifndef __PARAPROBE_RANGER_HDL_H__
#define __PARAPROBE_RANGER_HDL_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_RangerIonBuilder.h"


class rangerHdl
{
	//process-level class which implements the worker instance which executes ranging of ions using OpenMP
	//specimens result are written to a specifically-formatted HDF5 file

public:
	rangerHdl();
	~rangerHdl();

	bool read_reconxyz_from_apth5();
	bool read_mass2charge_from_apth5();
	bool read_ranging_from_apth5();
	
	bool write_molecularion_candidates_to_apth5( const vector<peaksearch_filter>::iterator searchtaskID, vector<apt_molecular_ion> const & cand );

	void apply_existent_ranging();
	bool identify_molecularions();

	bool init_target_file();
	bool write_environment_and_settings();

	bool write_metadata_to_apth5();
	bool write_ranging_to_apth5();

	int get_myrank();
	int get_nranks();
	void set_myrank( const int rr );
	void set_nranks( const int nn );
	void init_mpidatatypes();
	
	rangeTable rng;

	vector<p3d> xyz;			//reconstructed coordinates x,y,z
	vector<apt_real> m2q;			//mass2charge x,y,z
	//vector<evapion3> itype;		//to which type where the ions ranged?
	vector<unsigned char> itype;	

	h5Hdl inputReconH5Hdl;
	h5Hdl rangeH5Hdl;
	ranger_h5 debugh5Hdl;
	ranger_xdmf debugxdmf;

	profiler ranger_tictoc;

private:
	//MPI related
	int myrank;							//my MPI ID in the MPI_COMM_WORLD
	int nranks;
	//MPI_Datatype MPI_Ion_Type;
};


#endif

