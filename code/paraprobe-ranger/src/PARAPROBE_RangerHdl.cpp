//##MK::GPLV3

#include "PARAPROBE_RangerHdl.h"


rangerHdl::rangerHdl()
{
	myrank = MASTER;
	nranks = 1;
}


rangerHdl::~rangerHdl()
{
    xyz = vector<p3d>();
    m2q = vector<apt_real>();
    itype = vector<unsigned char>();
}


bool rangerHdl::read_reconxyz_from_apth5()
{
    double tic = MPI_Wtime();

	if ( inputReconH5Hdl.read_xyz_from_apth5( 
                ConfigRanger::InputfileReconstruction, xyz ) == true ) {
		cout << ConfigRanger::InputfileReconstruction << " read successfully" << "\n";
	}
	else {
		cerr << ConfigRanger::InputfileReconstruction << " read failed!" << "\n";
		return false;
	}
	
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	ranger_tictoc.prof_elpsdtime_and_mem( "ReadReconstructedXYZFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
    return true;
}


bool rangerHdl::read_mass2charge_from_apth5()
{
    double tic = MPI_Wtime();

	if ( inputReconH5Hdl.read_m2q_from_apth5( 
                ConfigRanger::InputfileReconstruction, m2q ) == true ) {
		cout << ConfigRanger::InputfileReconstruction << " read successfully" << "\n";
	}
	else {
		cerr << ConfigRanger::InputfileReconstruction << " read failed!" << "\n";
		return false;
	}
	
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	ranger_tictoc.prof_elpsdtime_and_mem( "ReadMassToChargeFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
    return true;
}


bool rangerHdl::read_ranging_from_apth5()
{
	double tic = MPI_Wtime();

	if ( rangeH5Hdl.read_iontypes_from_apth5( ConfigRanger::InputfileRangingData, rng.iontypes ) == true ) {
		cout << ConfigRanger::InputfileRangingData << " read successfully" << "\n";

		//create iontype_dictionary
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
			size_t id = static_cast<size_t>(it->id);
			size_t hashval = rng.hash_molecular_ion( it->strct.Z1, it->strct.Z2, it->strct.Z3 );
	cout << id << "\t\t" << hashval << "\t\t" << (int) it->strct.Z1 << ";" << (int) it->strct.Z2 << ";" << (int) it->strct.Z3 << "\n";
			if ( id != 0 && hashval != 0 ) { //skip adding existent UNKNOWN_IONTYPE
				auto jt = rng.iontypes_dict.find( hashval );
				if ( jt == rng.iontypes_dict.end() ) { //really a new iontype for which not yet mqival were defined
	//cout << "--->Adding new type " << id << "\t\t" << hashval << "\n";
					//add iontype in dictionary
					rng.iontypes_dict.insert( pair<size_t,size_t>( hashval, id ) );
				}
				else {
	cerr << "--->Detected an iontype where the triplet Z1, Z2, Z3 is a duplicate, which should not happen as iontypes have to be mutually exclusive!" << "\n";
					return false;
	//cout << "--->Adding to an existent type " << id << "\t\t" << hashval << "\t\t" << jt->second << "\n";
					//match to an already existent type
					//rng.iontypes_dict.insert( pair<size_t,size_t>( hashval, id ) ); //jt->second ) );
					//##MK::this fix makes it de facto a charge state specific 8;0,0;0;1 and 8;0;0;0;0;0;0;1 and 8;0;0;0;0;0;0;2 map to different types
					//return false; //##MK::switched of for FAU APT School
				}
			}
			else {
	cout << "--->Me skipping the UNKNOWN_IONTYPE" << "\n";
			}
		}

		//##MK::BEGIN DEBUG, make sure we use have correct ranging data
		cout << "--->Debugging ranging data..." << "\n";
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
			size_t id = static_cast<size_t>(it->id);
			size_t hashval = rng.hash_molecular_ion( it->strct.Z1, it->strct.Z2, it->strct.Z3 );
			cout << id << "\t\t" << hashval << "\t\t" << (int) it->strct.Z1 << ";" << (int) it->strct.Z2 << ";" << (int) it->strct.Z3 << "\n";
			for( auto jt = it->rng.begin(); jt != it->rng.end(); jt++ ) {
				cout << "\t\t\t" << jt->lo << "\t\t" << jt->hi << "\n";
			}
		}
		cout << "--->Debugging ion dictionary..."  << "\n";
		for( auto kt = rng.iontypes_dict.begin(); kt != rng.iontypes_dict.end(); kt++ ) {
			cout << kt->first << "\t\t" << kt->second << "\n";
		}
		//##MK::END DEBUG
	}
	else {
		cerr << ConfigRanger::InputfileRangingData << " read failed!" << "\n";
		return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	ranger_tictoc.prof_elpsdtime_and_mem( "ReadRangingDataFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


void rangerHdl::apply_existent_ranging()
{
    double tic = MPI_Wtime();
    
    //assign every ion first the default type
    try {
        //itype = vector<evapion3>( xyz.size(), evapion3() );
        itype = vector<unsigned char>( xyz.size(), static_cast<unsigned char>(UNKNOWN_IONTYPE) );
    }
    catch( bad_alloc &croak) {
        cerr << "Allocation failed for itype!" << "\n";
        itype = vector<unsigned char>(); return;
    }
    
    //assign physical meaningful iontypes based on models, ##MK::could be OpenMP parallelized in the future
    if ( ConfigRanger::RangingMethod == USE_EXISTENT_RANGEFILE ) {
    	//##MK::the old way, with fixed rrng files
        //if ( rng.read_rrng_file( ConfigRanger::InputfileRangingData ) == true ) {
        //    if ( rng.validity_check() == true ) {
    	//##MK::the new way, clean H5 range files

            	//##MK::parallelize this part
                for( size_t i = 0; i < m2q.size(); i++ ) {
                    apt_real mq = m2q.at(i);
                    unsigned int solution = UNKNOWN_IONTYPE;
                    for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
                        //ranges are non-overlapping so only one solution if any
                        for( auto jt = it->rng.begin(); jt != it->rng.end(); jt++ ) {
                            if ( jt->inrange( mq ) == false ) { //most likely case is not in the range
                                continue;
                            }
                            else {
                                solution = it->id;
                            }
                        } //next range same iontype
                    } //next iontype
                    
                    if ( solution < static_cast<unsigned int>(UCHARMX) ) {
                        itype.at(i) = static_cast<unsigned char>(solution);
                    }
                    else {
                        cerr << "Impossible to match any range, should not be encountered!" << "\n";
                        itype = vector<unsigned char>(); return;
                    }
                } //next ion

                cout << "Rank " << get_myrank() << " completed local workpackage!" << "\n";
            //}
            //else {
            //    cerr << "Rank " << MASTER << " the range file has invalid ranges (possible empty ranges or overlap)!" << "\n";
            //    itype = vector<unsigned char>(); return;
            //}
        //}
        //else {
        //    cerr << "Rank " << MASTER << " could not open the RRNG file so default labels were assigned" << "\n";
        //    itype = vector<unsigned char>(); return;
        //}
    }
    
    double toc = MPI_Wtime();
    memsnapshot mm = memsnapshot();
    ranger_tictoc.prof_elpsdtime_and_mem( "ApplyingExistentRanging", APT_XX, APT_IS_SEQ, mm, tic, toc);
}


bool rangerHdl::identify_molecularions()
{
	//double tic = MPI_Wtime();
	double prof[2] = { 0.0, 0.0 };

	for ( auto it = ConfigRanger::PeakSearchFilters.begin(); it != ConfigRanger::PeakSearchFilters.end(); it++ ) {
		double mycomptic = MPI_Wtime();

		molecular_ion_builder crawler;

		if ( it->targets == "" ) { //no restrictions on the targets
			crawler.build_nuclidtable_default();
		}
		else {
			crawler.build_nuclidtable_reduced( it->targets );
		}

		crawler.find_molecularions( it->iv );

		//##MK::BEGIN DEBUG
		cout << "Crawler.candidates.size() = " << crawler.candidates.size() << "\n";
		/*
		for( auto jt = crawler.candidates.begin(); jt != crawler.candidates.end(); jt++ ) {
			for ( int j = 0; j < ConfigRanger::PeakSearchMaximumComponents; j++ ) { //APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS; j++ ) {
				pair<unsigned char, unsigned char> unhashed = unhash_zn( jt->composition[j] );
				//cout << jt->composition[j] << "\t";
				cout << "(" << ((int) unhashed.first + (int) unhashed.second) << ";" << (int) unhashed.first << ";" << (int) unhashed.second << ")" << "\t";
			}
			cout << jt->charge << "\t" << jt->mass / (apt_real) jt->charge << "\n";
		}
		*/
		//##MK:END DEBUG

		double mycomptoc = MPI_Wtime();
		prof[0] = prof[0] + (mycomptoc - mycomptic);
		double myiotic = MPI_Wtime();

		if ( crawler.candidates.size() > 0 ) {
			//write results to HDF5
			if ( write_molecularion_candidates_to_apth5( it, crawler.candidates ) == true ) {
				cout << "Rank " << get_myrank() << " wrote results for peak search "
						<< it - ConfigRanger::PeakSearchFilters.begin() << " written to H5 file" << "\n";
			}
			else {
				cerr << "Rank " << get_myrank() << " failed to write results for peak search "
						<< it - ConfigRanger::PeakSearchFilters.begin() << " to H5 file" << "\n";
				return false;
			}
		}

		double myiotoc = MPI_Wtime();
		prof[1] = prof[1] + (myiotoc - myiotic);
	}

	//double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	ranger_tictoc.prof_elpsdtime_and_mem( "FindMolecularIons", APT_XX, APT_IS_SEQ, mm, 0.0, prof[0] );
	ranger_tictoc.prof_elpsdtime_and_mem( "WriteMolecularIons", APT_XX, APT_IS_SEQ, mm, 0.0, prof[1] );
	return true;
}


bool rangerHdl::init_target_file()
{
	//MK::append results to existent dataset
    //##MK::ID tagging
	if ( ConfigRanger::RangingMethod == USE_EXISTENT_RANGEFILE ) {
		if ( debugh5Hdl.append_ranger_apth5( ConfigRanger::InputfileReconstruction ) == WRAPPED_HDF5_SUCCESS ) {
			return true;
		}
	}
	else {
		string h5fnm = "PARAPROBE.Ranger.Results.SimID." + to_string(ConfigShared::SimID) + ".h5";
		if ( debugh5Hdl.create_ranger_apth5( h5fnm ) == WRAPPED_HDF5_SUCCESS ) {
			return true;
		}
	}

	return false;
}


bool rangerHdl::write_environment_and_settings()
{
	vector<pparm> hardware = ranger_tictoc.report_machine();
	string prfx = PARAPROBE_RANGER_META_HRDWR; //PARAPROBE_RANGER_HRDWR_META_KEYS;
	for( auto it = hardware.begin(); it != hardware.end(); it++ ) {
		cout << it->keyword << "__" << it->value << "__" << it->unit << "__" << it->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *it ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing hardware setting " << it->keyword << " failed!" << "\n";
			return false;
		}
	}
	cout << "Rank " << MASTER << " hardware settings written to H5" << "\n";

	vector<pparm> software;
	ConfigRanger::reportSettings( software );
	prfx = PARAPROBE_RANGER_META_SFTWR; //PARAPROBE_RANGER_SFTWR_META_KEYS;
	for( auto jt = software.begin(); jt != software.end(); jt++ ) {
		cout << jt->keyword << "__" << jt->value << "__" << jt->unit << "__" << jt->info << "\n";
		if ( debugh5Hdl.write_settings_entry( prfx, *jt ) == WRAPPED_HDF5_SUCCESS ) {
			continue;
		}
		else {
			cerr << "Writing software tool setting " << jt->keyword << " failed!" << "\n";
			return false;
		}
	}
	cout << "Rank " << MASTER << " software settings written to H5" << "\n";

	return true;
}


bool rangerHdl::write_metadata_to_apth5()
{
	double tic = MPI_Wtime();

	if ( xyz.size() != itype.size() ) {
		cerr << "Rank " << MASTER << " detected that not every ion was ranged!" << "\n"; return false;
	}

	string fwslash = "/";
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";

	//iontype dictionary
	vector<unsigned char> uc8_id;
    vector<unsigned char> uc8_wh;
    vector<float> f32;
    vector<unsigned char> uc8_mq;
    for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
        if ( it->id < static_cast<unsigned int>(UCHARMX ) ) {
        	unsigned char thisid = static_cast<unsigned char>(it->id);
            uc8_id.push_back( thisid );
            uc8_wh.push_back( it->strct.Z1 );
            uc8_wh.push_back( it->strct.N1 );
            uc8_wh.push_back( it->strct.Z2 );
            uc8_wh.push_back( it->strct.N2 );
            uc8_wh.push_back( it->strct.Z3 );
            uc8_wh.push_back( it->strct.N3 );
            uc8_wh.push_back( it->strct.sign );
            uc8_wh.push_back( it->strct.charge );
            for( auto jt = it->rng.begin(); jt != it->rng.end(); jt++ ) {
                f32.push_back( jt->lo );
                f32.push_back( jt->hi );
                uc8_mq.push_back( thisid );
            }
        }
        else {
            cerr << "Rank " << MASTER << " failure reporting iontype " << it->id << "\n"; return false;
        }
    }
    
	dsnm = PARAPROBE_RANGER_META_TYPID_DICT_ID;
    ifo = h5iometa( dsnm, uc8_id.size()/PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX, PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to create PARAPROBE_RANGER_META_TYPID_DICT_ID metadata!" << "\n"; return false;
	}
	offs = h5offsets( 0, uc8_id.size()/PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX, 0, PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX,
			uc8_id.size()/PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX, PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_id );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to store PARAPROBE_RANGER_META_TYPID_DICT_ID metadata!" << "\n"; return false;
	}
	uc8_id = vector<unsigned char>();

	dsnm = PARAPROBE_RANGER_META_TYPID_DICT_WHAT;
    ifo = h5iometa( dsnm, uc8_wh.size()/PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX, PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to create PARAPROBE_RANGER_META_TYPID_DICT_WHAT metadata!" << "\n"; return false;
	}
	offs = h5offsets( 0, uc8_wh.size()/PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX, 0, PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX,
			uc8_wh.size()/PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX, PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_wh );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to store PARAPROBE_RANGER_META_TYPID_DICT_WHAT metadata!" << "\n"; return false;
	}
	uc8_wh = vector<unsigned char>();
    
    dsnm = PARAPROBE_RANGER_META_TYPID_DICT_IVAL;
    ifo = h5iometa( dsnm, f32.size()/PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX, PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to create PARAPROBE_RANGER_META_TYPID_DICT_IVAL metadata!" << "\n"; return false;
	}
	offs = h5offsets( 0, f32.size()/PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX, 0, PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX,
			f32.size()/PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX, PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to store PARAPROBE_RANGER_META_TYPID_DICT_IVAL metadata!" << "\n"; return false;
	}
	f32 = vector<float>();

    dsnm = PARAPROBE_RANGER_META_TYPID_DICT_ASSN;
    ifo = h5iometa( dsnm, uc8_mq.size()/PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX, PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to create PARAPROBE_RANGER_META_TYPID_DICT_ASSN metadata!" << "\n"; return false;
	}
	offs = h5offsets( 0, uc8_mq.size()/PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX, 0, PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX,
			uc8_mq.size()/PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX, PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_mq );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to store PARAPROBE_RANGER_META_TYPID_DICT_ASSN metadata!" << "\n"; return false;
	}
	uc8_mq = vector<unsigned char>();

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	ranger_tictoc.prof_elpsdtime_and_mem( "WriteRangingMetadataAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool rangerHdl::write_molecularion_candidates_to_apth5( const vector<peaksearch_filter>::iterator searchtaskID, vector<apt_molecular_ion> const & cand )
{
	string fwslash = "/";
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	//log metadata
	string grpnm = PARAPROBE_RANGER_META_PEAKFIND + fwslash + to_string(searchtaskID - ConfigRanger::PeakSearchFilters.begin());
	string dsnm = grpnm + fwslash + PARAPROBE_RANGER_META_PEAKFIND_IVAL;
	vector<float> f32;
	f32.push_back( searchtaskID->iv.lo );
	f32.push_back( searchtaskID->iv.hi );
	ifo = h5iometa( dsnm, f32.size()/PARAPROBE_RANGER_META_PEAKFIND_IVAL_NCMAX, PARAPROBE_RANGER_META_PEAKFIND_IVAL_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to create " << dsnm << " !" << "\n"; return false;
	}
	offs = h5offsets( 0, f32.size()/PARAPROBE_RANGER_META_PEAKFIND_IVAL_NCMAX,
				0, PARAPROBE_RANGER_META_PEAKFIND_IVAL_NCMAX, f32.size()/PARAPROBE_RANGER_META_PEAKFIND_IVAL_NCMAX,
				PARAPROBE_RANGER_META_PEAKFIND_IVAL_NCMAX );
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to store " << dsnm << " !" << "\n"; return false;
	}
	f32 = vector<float>();

	//results
	//molecular ion architecture
	size_t adaptive_nr = cand.size();
	size_t adaptive_nc = ConfigRanger::PeakSearchMaximumComponents * 2 + 1; //uchar pairs of Nproton,Nneutron, decoded from uint16 hash, and uchar for charge, one row per molecular ion

	grpnm = PARAPROBE_RANGER_RES_PEAKFIND + fwslash + to_string(searchtaskID - ConfigRanger::PeakSearchFilters.begin());
	dsnm = grpnm + fwslash + PARAPROBE_RANGER_RES_PEAKFIND_WHAT;
	vector<unsigned char> u8 = vector<unsigned char>( adaptive_nr * adaptive_nc, 0x00 );
	size_t i = 0;
	for( auto jt = cand.begin(); jt != cand.end(); jt++, i++ ) {
		for( int c = 0; c < ConfigRanger::PeakSearchMaximumComponents; c++ ) {
			pair<unsigned char, unsigned char> unhashed = unhash_zn( jt->composition[c] );
			u8[i*adaptive_nc+c*2+0] = unhashed.first; //nprotons
			u8[i*adaptive_nc+c*2+1] = unhashed.second; //nneutrons
			//cout << "i/c/hash/first/second " << i << "\t" << c << "\t" << jt->composition[c] << "\t" << (int) unhashed.first << "\t" << (int) unhashed.second << "\n";
		}
		u8[i*adaptive_nc+ConfigRanger::PeakSearchMaximumComponents*2+0] = jt->charge;
	}
	ifo = h5iometa( dsnm, u8.size()/adaptive_nc, adaptive_nc );
	status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to create " << dsnm << " !" << "\n"; return false;
	}
	offs = h5offsets( 0, u8.size()/adaptive_nc, 0, adaptive_nc, u8.size()/adaptive_nc, adaptive_nc );
	status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, u8 );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to store " << dsnm << " !" << "\n"; return false;
	}
	u8 = vector<unsigned char>();

	//no filtering out of ions with mass to charge less than 1.0 Da
	//mass-to-charge of the ions, ##MK::could be fused with the above loop to become faster with paying for memory
	grpnm = PARAPROBE_RANGER_RES_PEAKFIND + fwslash + to_string(searchtaskID - ConfigRanger::PeakSearchFilters.begin());
	dsnm = grpnm + fwslash + PARAPROBE_RANGER_RES_PEAKFIND_MQ;
	f32 = vector<float>( adaptive_nr * PARAPROBE_RANGER_RES_PEAKFIND_MQ_NCMAX, 0.0 );
	i = 0;
	for( auto jt = cand.begin(); jt != cand.end(); jt++, i++ ) {
		f32[i] = jt->mass / (apt_real) jt->charge;
	}
	ifo = h5iometa( dsnm, f32.size()/PARAPROBE_RANGER_RES_PEAKFIND_MQ_NCMAX, PARAPROBE_RANGER_RES_PEAKFIND_MQ_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to create " << dsnm << " !" << "\n"; return false;
	}
	offs = h5offsets( 0, f32.size()/PARAPROBE_RANGER_RES_PEAKFIND_MQ_NCMAX, 0, PARAPROBE_RANGER_RES_PEAKFIND_MQ_NCMAX,
			f32.size()/PARAPROBE_RANGER_RES_PEAKFIND_MQ_NCMAX, PARAPROBE_RANGER_RES_PEAKFIND_MQ_NCMAX );
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to store " << dsnm << " !" << "\n"; return false;
	}
	f32 = vector<float>();

	//abundance score
	grpnm = PARAPROBE_RANGER_RES_PEAKFIND + fwslash + to_string(searchtaskID - ConfigRanger::PeakSearchFilters.begin());
	dsnm = grpnm + fwslash + PARAPROBE_RANGER_RES_PEAKFIND_ABUN;
	vector<double> f64 = vector<double>( adaptive_nr * PARAPROBE_RANGER_RES_PEAKFIND_ABUN_NCMAX, 0.0 );
	i = 0;
	for( auto jt = cand.begin(); jt != cand.end(); jt++, i++ ) {
		f64[i] = jt->abun;
	}
	ifo = h5iometa( dsnm, f64.size()/PARAPROBE_RANGER_RES_PEAKFIND_ABUN_NCMAX, PARAPROBE_RANGER_RES_PEAKFIND_ABUN_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f64le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to create " << dsnm << " !" << "\n"; return false;
	}
	offs = h5offsets( 0, f64.size()/PARAPROBE_RANGER_RES_PEAKFIND_ABUN_NCMAX, 0, PARAPROBE_RANGER_RES_PEAKFIND_ABUN_NCMAX,
			f64.size()/PARAPROBE_RANGER_RES_PEAKFIND_ABUN_NCMAX, PARAPROBE_RANGER_RES_PEAKFIND_ABUN_NCMAX );
	status = debugh5Hdl.write_contiguous_matrix_f64le_hyperslab( ifo, offs, f64 );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to store " << dsnm << " !" << "\n"; return false;
	}
	f64 = vector<double>();

	return true;
}


bool rangerHdl::write_ranging_to_apth5()
{
	double tic = MPI_Wtime();

	if ( xyz.size() != itype.size() ) {
		cerr << "Rank " << MASTER << " detected that not every ion was ranged!" << "\n"; return false;
	}

	string fwslash = "/";
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";
    
    //iontype IDs
    //MK::use itype in-place, works because PARAPROBE_RANGER_RES_TYPID_NCMAX == 1 and we are on MASTER rank only

    dsnm = PARAPROBE_RANGER_RES_TYPID;
	ifo = h5iometa( dsnm, itype.size()/1, 1 );
    status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
	if ( status < 0 ) {
        cerr << "Rank " << MASTER << " failed to create PARAPROBE_RANGER_RES_TYPID results!" << "\n"; return false;
    }
    offs = h5offsets( 0, itype.size()/PARAPROBE_RANGER_RES_TYPID_NCMAX,
    			0, PARAPROBE_RANGER_RES_TYPID_NCMAX, itype.size()/PARAPROBE_RANGER_RES_TYPID_NCMAX, PARAPROBE_RANGER_RES_TYPID_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, itype ); //used in-place
    if ( status < 0 ) {
        cerr << "Rank " << MASTER << " failed to store PARAPROBE_RANGER_RES_TYPID results!" << "\n"; return false;
    }

    string xdmffn = ConfigRanger::InputfileReconstruction + ".Ranging.xdmf";
    debugxdmf.create_volrecon_file( xdmffn, itype.size()/PARAPROBE_RANGER_RES_TYPID_NCMAX, debugh5Hdl.h5resultsfn );

    //earliest point in time when itype is no longer required
    itype = vector<unsigned char>();

    double toc = MPI_Wtime();
    memsnapshot mm = memsnapshot();
    ranger_tictoc.prof_elpsdtime_and_mem( "WriteResultsToAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

    return true;
}


int rangerHdl::get_myrank()
{
	return this->myrank;
}


int rangerHdl::get_nranks()
{
	return this->nranks;
}


void rangerHdl::set_myrank( const int rr )
{
	this->myrank = rr;
}


void rangerHdl::set_nranks( const int nn )
{
	this->nranks = nn;
}


void rangerHdl::init_mpidatatypes()
{
	/*
	int elementCounts[2] = {3, 1};
	MPI_Aint displacements[2] = {0, 3 * MPIIO_OFFSET_INCR_F32};
	MPI_Datatype oldTypes[2] = {MPI_FLOAT, MPI_UNSIGNED};
	MPI_Type_create_struct(2, elementCounts, displacements, oldTypes, &MPI_Ion_Type);
	MPI_Type_commit(&MPI_Ion_Type);
	*/
}
