//##MK::GPLV3

#include "PARAPROBE_RangerIonBuilder.h"

apt_nuclid zn_hash( const unsigned char Z, const unsigned char N )
{
	//Z number of protons, N total nucleons, not neutrons!
	apt_nuclid hash = 0;
	unsigned short int nprotons = (unsigned short int) Z;
	unsigned short int totalnucleons = (unsigned short int) N;
	unsigned short int nneutrons = totalnucleons - nprotons;
	hash = (nneutrons * static_cast<unsigned short int>(256)) + nprotons;
	//so ^{1}H is hashed to 1*256+1 = 257, ^{238}U is hashed to 146*256 + 92
	//cout << "Hashing hash " << hash << " Z/N " << (int) Z << ";" << (int) N << " nprotons = " << nprotons << " totalnucleons = " << totalnucleons << " nneutrons = " << nneutrons << "\n";
	return hash;
}


pair<unsigned char, unsigned char> unhash_zn( const unsigned short int hash )
{
	//nprotons first change fastest, nneutrons second
	unsigned short int n = hash / static_cast<unsigned short int>(256); //nneutrons
	unsigned short int z = hash - (n*static_cast<unsigned short int>(256)); //nprotons
	//cout << "Unhashing hash " << hash << " z = " << z << " n = " << n << "\n";
	if ( z < static_cast<unsigned short int>(256) && n < static_cast<unsigned short int>(256) ) {
		return pair<unsigned char, unsigned char>( z, n );
	}
	else {
		cerr << "Narrowing conversion from unsigned short int to unsigned char overflows for hash/z/n " << hash << ";" << z << ";" << n << " !" << "\n";
		return pair<unsigned char, unsigned char>( 0x00, 0x00 );
	}
}


apt_molecular_ion::apt_molecular_ion()
{
	for( int i = 0; i < APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS; i++ ) {
		this->composition[i] = 0;
	}
	this->charge = 0;
	this->mass = 0.0;
	this->abun = 1.0;
}


apt_molecular_ion::apt_molecular_ion( apt_molecular_ion const & in )
{
	for( int i = 0; i < APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS; i++ ) {
		this->composition[i] = in.composition[i];
	}
	this->charge = in.charge;
	this->mass = in.mass;
	this->abun = in.abun;
}


bool apt_molecular_ion::is_equal( apt_molecular_ion const & test )
{
	//in most cases molecular ions differ
	for( int i = 0; i < APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS; i++ ) {
		if ( this->composition[i] != test.composition[i] ) {
			return false;
		}
		//optionally
		//if ( this->composition[i] == 0 ) { //in most cases users allow only for a low number of nuclids to build the molecular ion
		//	break;
		//}
	}
	//if not returned yet, the entire nuclid building code was the same, so checking for the total charge equality remains
	if ( this->charge != test.charge ) {
		return false;
	}

	//##MK::mass and abun are not checked
	return true;
}



molecular_ion_builder::molecular_ion_builder()
{
	for ( auto iso_it = ConfigShared::MyIsotopes.begin(); iso_it != ConfigShared::MyIsotopes.end(); iso_it++ ) {
		apt_nuclid hash = zn_hash( iso_it->Z, iso_it->N );
		nuclid_mass[hash] = iso_it->mass;
		nuclid_abun[hash] = iso_it->abundance;
		//cout << "Initializing molecular_ion_builder Z/N/mass " << (int) iso_it->Z << "\t\t" << (int) iso_it->N << "\t\t" << nuclid_mass[hash] << "\t\t" << nuclid_abun[hash] << "\n";
	}
}


molecular_ion_builder::~molecular_ion_builder()
{
}


apt_real molecular_ion_builder::get_mass( apt_molecular_ion const & test )
{
	//evaluate the mass
	apt_real m = 0.0;
	for( int i = 0; i < APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS; ++i ) {
		unsigned short int hash = test.composition[i];
		map<apt_nuclid, apt_real>::iterator thisone = nuclid_mass.find(hash);
		if ( thisone != nuclid_mass.end() ) {
			m += thisone->second;
		}
		else { //##MK::possibility for premature break out because we build molecular ions strictly in ascending order on a zero-value initialized composition hash table
			//cerr << "molecular_ion_builder::get_mass unable to get mass for hash " << (int) hash << "\n";
			break;
		}
	}
	return m;
}


double molecular_ion_builder::get_abundance_score( apt_molecular_ion const & test )
{
	//evaluate the abundance score
	double abunscore = 1.0;
	for( int i = 0; i < APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS; ++i ) {
		unsigned short int hash = test.composition[i];
		map<apt_nuclid, double>::iterator thisone = nuclid_abun.find(hash);
		if ( thisone != nuclid_abun.end() ) {
			abunscore *= thisone->second;
		}
		else { //##MK::possibility for premature break out because we build molecular ions strictly in ascending order on a zero-value initialized composition hash table
			//cerr << "molecular_ion_builder::get_mass unable to get mass for hash " << (int) hash << "\n";
			break;
		}
	}
	return abunscore;
}


void molecular_ion_builder::build_nuclidtable_default()
{
	nuclids = vector<apt_nuclid>();
	for( auto iso_it = ConfigShared::MyIsotopes.begin(); iso_it != ConfigShared::MyIsotopes.end(); iso_it++ ) {
		//check that isotopes are unique
		apt_nuclid hash = zn_hash( iso_it->Z, iso_it->N );
		nuclids.push_back( hash );
		//cout << "molecular_ion_builder creating default nuclid table " << nuclids.back() << "\n";
	}

	//nuclids need to be sorted strictly in descending order
	sort(nuclids.rbegin(), nuclids.rend());

	/*
	//##MK::BEGIN DEBUG
	cout << "Nuclidtable " << "\n";
	for( auto it = nuclids.begin(); it != nuclids.end(); it++ ) {
		cout << "\t\t" << *it << "\n";
	}
	//##MK::END DEBUG
	*/
}


void molecular_ion_builder::build_nuclidtable_reduced( string const & tg )
{
	nuclids = vector<apt_nuclid>();

	vector<unsigned char> theseZ; // = { static_cast<unsigned char>(1), static_cast<unsigned char>(8) };

	//parse out element names from tg white list, comma-separated list, e.g. "Al,H:,O:"
	vector<string> tokens;
#ifdef UTILIZE_BOOST
	boost::split( tokens, tg, boost::is_any_of(",")); //can handle multiple delimiter different than getline
#else
	string input{tg};
	regex re("[,]+");
	//sregex_token_iterator first{input.begin(), input.end(), re, -1};
	sregex_token_iterator it(input.begin(), input.end(), re, -1);
	sregex_token_iterator last; //the '-1' is what makes the regex split (-1 := what was not matched)
	for( ; it != last; it++) {
cout << "regex __" << it->str() << "__" << "\n";
		tokens.push_back( it->str() );
	}
#endif
	for( auto tk = tokens.begin(); tk != tokens.end(); tk++ ) {
		string thisone = *tk;
		if ( thisone.length() == 2 ) {
			//find named element specified by the token via scanning the PSE
			for( auto Zt = ConfigShared::MyElementsSymbol.begin(); Zt != ConfigShared::MyElementsSymbol.end(); Zt++ ) { //scan PSE
				string elementname = Zt->first;
				//does an element with this name exist in the token?
	//cout << "elementname " << "__" << elementname << "__" << " thisone " << "__" << thisone << "__" << "\n";
				//repetitive scan for elementnames, i.e. 'H:H:H:' needs to come three times
				//get the first occurrence
				size_t position = thisone.find( elementname );
				if ( position != string::npos ) {
					theseZ.push_back( Zt->second.Z );
					break;
				}
			} //probe next possible element
		}
	}

	//##BEGIN DEBUG
	for( auto kt = theseZ.begin(); kt != theseZ.end(); kt++ ) {
		cout << "Building reduced nuclid table with element " << (int) *kt << "\n";
	}
	//##END DEBUG

	//knowing the elements that should be considered, now collect all their isotopes
	for( auto iso_targ_it = theseZ.begin(); iso_targ_it != theseZ.end(); iso_targ_it++ ) {
		//naive search for isotopes
		for( auto iso_it = ConfigShared::MyIsotopes.begin(); iso_it != ConfigShared::MyIsotopes.end(); iso_it++ ) {
			if ( iso_it->Z != *iso_targ_it ) {
				continue;
			}
			else {
				apt_nuclid hash = zn_hash( iso_it->Z, iso_it->N );
				nuclids.push_back( hash );
				cout << "Molecular_ion_builder creating reduced nuclid table " << nuclids.back() << "\n";
			}
		}
	}

	//nuclids need to be sorted strictly in descending order
	sort(nuclids.rbegin(), nuclids.rend());

	/*
	//##MK::BEGIN DEBUG
	cout << "Nuclidtable " << "\n";
	for( auto it = nuclids.begin(); it != nuclids.end(); it++ ) {
		cout << "\t\t" << *it << "\n";
	}
	//##MK::END DEBUG
	*/
}


void molecular_ion_builder::find_molecularions( mqival const & interval )
{
	apt_molecular_ion tmp = apt_molecular_ion();
	build_molecular_ion( nuclids.begin(), tmp, 0, interval );
}


void molecular_ion_builder::build_molecular_ion( vector<apt_nuclid>::iterator itstart, apt_molecular_ion & in, const int i, mqival const & iv )
{
	if ( i < ConfigRanger::PeakSearchMaximumComponents ) { //APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS ) {
		for( vector<apt_nuclid>::iterator it = itstart; it != nuclids.end(); it++ ) { //recursively add ions with the same or lower ID
			apt_molecular_ion new_in = apt_molecular_ion( in );
			new_in.composition[i] = *it;


			//##MK::BEGIN DEBUG
			/*
			bool healthy = true;
			for ( int j = 0; j < APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS; j++ ) {
				if ( new_in.composition[j] == 324 ) {
					healthy = false;
				}
			}
			if ( healthy == false ) {
			*/
			/*
				cout << "Recursing new_in i = " << i << "\n";
				for ( int j = 0; j < APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS-1; j++ ) {
					cout << new_in.composition[j] << ";";
				}
				cout << new_in.composition[APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS-1] << "\n";
				cout << "charge " << new_in.charge << " mass " << new_in.mass << " abun " << new_in.abun << "\n";
			*/
			/*
			}
			*/
			//##MK::END DEBUG


			apt_real new_mass = get_mass( new_in );
			double new_abun_score = get_abundance_score( new_in );
			//cout << "new_mass " << new_mass << "\n";
			for( unsigned short int chrg = 1; chrg <= ConfigRanger::PeakSearchMaximumCharge; chrg++ ) { //APTMOLECULARION_MAX_CHARGE
				apt_real mass_to_charge = new_mass / (apt_real) chrg;
				if ( mass_to_charge < iv.lo ) {
					break;
					//MK::one can break the entire charge state generation here already instead of continue
					//because already the current mq is left out of interval [mqmin, mqmax]
					//and all mq in the next iterations will result in even lower mass-to-charge you walk to the left
					//increasing your distance to the left bound
				}
				if ( mass_to_charge > iv.hi ) { //##MK::can be optimized and broken out of earlier if testing first chrg == 1 and then chrg == APTMOLECULARION_MAX_CHANGE
					continue;
					//MK::must not be break! because with adding more charge you might walk from right to left so possibly into your interval!
				}
				//molecular ion is within user-specified bounds
				new_in.charge = chrg;
				new_in.mass = new_mass;
				new_in.abun = new_abun_score;

				//cout << "Recursing i = " << i << " charge " << chrg << " added" << "\n";
				candidates.push_back( new_in );


				//##MK::BEGIN DEBUG
				/*
				bool healthy = true;
				for ( int j = 0; j < APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS; j++ ) {
					if ( candidates.back().composition[j] == 324 ) {
						healthy = false;
					}
				}
				if ( healthy == false ) {
				*/
				/*
					cout << "Recursing cand added i = " << i << "\n";
					for ( int j = 0; j < APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS-1; j++ ) {
						cout << candidates.back().composition[j] << ";";
					}
					cout << candidates.back().composition[APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS-1] << "\n";
					cout << "charge " << candidates.back().charge << " mass " << candidates.back().mass << " abun " << candidates.back().abun << "\n";
				*/
				/*
				}
				*/
				//##MK::END DEBUG
			}
			//recursively add the next ion
			build_molecular_ion( it, new_in, i+1, iv );
		}
	}
	else { //break the recursion
		return;
	}
}
