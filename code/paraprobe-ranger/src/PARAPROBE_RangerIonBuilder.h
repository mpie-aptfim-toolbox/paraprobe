//##MK::GPLV3

#ifndef __PARAPROBE_RANGER_IONBUILDER_H__
#define __PARAPROBE_RANGER_IONBUILDER_H__

#include "PARAPROBE_RangerXDMF.h"

//we hash nuclids to unsigned short int, because this covers (at the cost of approx factor two overhead)
// the entire nuclid table and transform equality checks for molecular ions into a series of equality checks for numbers
typedef unsigned short int apt_nuclid;

apt_nuclid zn_hash( const unsigned char Z, const unsigned char N );

/*
apt_nuclid zn_hash( const int nprotons, const int nneutrons )
{
	apt_nuclid hashval = 0;
	if ( nprotons < 256 && nneutrons < 256 ) {
		//think about the nuclid table with nprotons on the y and nneutrons on the x axis
		//we build the hash like if a snail walks through the nuclid map, visiting every possible combination
		//you start at nprotons = 0 and walk increasing number of neutrons, i.e. along a row
		//once finished with the row you got to nprotons 1 and repeat until covered
		hashval = nneutrons * 256 + nprotons; //so ^{1}H is hashed to 1*256+1 = 257, ^{238}U is hashed to 146*256 + 92
	}
	return hashval;
}
*/


pair<unsigned char, unsigned char> unhash_zn( const unsigned short int hash );


struct apt_molecular_ion
{
	apt_nuclid composition[APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS]; 	//initialized to zero by default
	unsigned short int charge;
	apt_real mass;
	double abun;													//product of natural abundance of nuclids

	apt_molecular_ion();
	apt_molecular_ion( apt_molecular_ion const & in );

	/*
	apt_real get_mass()
	{
		//evaluate the mass
		apt_real m = 0.0;
		for( int i = 0; i < APTMOLECULARION_MAX_NUMBER_OF_NUCLIDS; ++i ) {
			unsigned short int hash = this->composition[i];
			if ( hash != 0 ) {
				m += ConfigRanger::Mass[hash];
			}
			else { //##MK::possibility for premature break out because we build molecular ions strictly in ascending order on a zero-value initialized composition hash table
				break;
			}
		}
		return m;
	}
	*/
	bool is_equal( apt_molecular_ion const & test );
};


//map<apt_nuclid,apt_real> mass;
//##MK::build the nuclid table
//##MK::for El-Zoka for instance add only H, O, resulting in limiting mass-to-charge
//##MK::for all other cases run o ver isotope table

//begin with
//apt_molecular_ion tmp = apt_molecular_ion();
//build_molecular_ion( nuclids.begin(), 0, tmp, 44.0, 46.0 );

class molecular_ion_builder
{
public:
	molecular_ion_builder();
	~molecular_ion_builder();

	apt_real get_mass( apt_molecular_ion const & test );
	double get_abundance_score( apt_molecular_ion const & test );


	void build_nuclidtable_default();
	void build_nuclidtable_reduced( string const & tg );
	void find_molecularions( mqival const & interval );
	void build_molecular_ion( vector<apt_nuclid>::iterator itstart, apt_molecular_ion & in, const int i, mqival const & iv );

	//unordered_set<apt_molecular_ion> candidates;
	vector<apt_molecular_ion> candidates;

private:
	vector<apt_nuclid> nuclids; //MK::this list MUST be descending order!
	map<apt_nuclid, apt_real> nuclid_mass;
	map<apt_nuclid, double> nuclid_abun;
};


#endif
