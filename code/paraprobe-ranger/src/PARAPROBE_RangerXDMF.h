//##MK::GPLV3

#ifndef __PARAPROBE_RANGER_XDMF_H__
#define __PARAPROBE_RANGER_XDMF_H__

#include "PARAPROBE_RangerHDF5.h"

class ranger_xdmf : public xdmfHdl
{
	//tool-specific sub-class of a XDMF inheriting all methods of the base class xdmfHdl
	//coordinating instance handling all (sequential) writing to XDMF text file to supplement visualization of HDF5 content

public:
	ranger_xdmf();
	~ranger_xdmf();

	//int create_materialpoint_file( const string xmlfn, const size_t nmp, const string h5ref );
	int create_volrecon_file( const string xmlfn, const size_t nions, const string h5ref );

//private:
};

#endif
