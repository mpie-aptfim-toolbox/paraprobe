map<unsigned char, map<unsigned char, isotopeinfo> ConfigShared::MyIsotopes = {
{ static_cast<unsigned char>(1), {
	{ static_cast<unsigned char>(1), isotopeinfo( 1.0078250322, 0.99972, 1, 1, false)},
	{ static_cast<unsigned char>(1), isotopeinfo( 2.0141017781, 0.00028, 2, 1, false)}
}},
{ static_cast<unsigned char>(2), {
	{ static_cast<unsigned char>(2), isotopeinfo( 3.01602932, 2e-06, 3, 2, false)},
	{ static_cast<unsigned char>(2), isotopeinfo( 4.0026032541, 0.9999979999999999, 4, 2, false)}
}},
{ static_cast<unsigned char>(3), {
	{ static_cast<unsigned char>(3), isotopeinfo( 6.015122887, 0.078, 6, 3, false)},
	{ static_cast<unsigned char>(3), isotopeinfo( 7.01600344, 0.922, 7, 3, false)}
}},
{ static_cast<unsigned char>(4), {
	{ static_cast<unsigned char>(4), isotopeinfo( 9.0121831, 1.0, 9, 4, false)}
}},
{ static_cast<unsigned char>(5), {
	{ static_cast<unsigned char>(5), isotopeinfo( 10.012937, 0.204, 10, 5, false)},
	{ static_cast<unsigned char>(5), isotopeinfo( 11.009305, 0.7959999999999999, 11, 5, false)}
}},
{ static_cast<unsigned char>(6), {
	{ static_cast<unsigned char>(6), isotopeinfo( 12.0, 0.9884, 12, 6, false)},
	{ static_cast<unsigned char>(6), isotopeinfo( 13.003354835, 0.0116, 13, 6, false)}
}},
{ static_cast<unsigned char>(7), {
	{ static_cast<unsigned char>(7), isotopeinfo( 14.003074004, 0.9957799999999999, 14, 7, false)},
	{ static_cast<unsigned char>(7), isotopeinfo( 15.000108899, 0.004220000000000001, 15, 7, false)}
}},
{ static_cast<unsigned char>(8), {
	{ static_cast<unsigned char>(8), isotopeinfo( 15.99491462, 0.9973799999999999, 16, 8, false)},
	{ static_cast<unsigned char>(8), isotopeinfo( 16.999131757, 0.0004, 17, 8, false)},
	{ static_cast<unsigned char>(8), isotopeinfo( 17.999159613, 0.0022199999999999998, 18, 8, false)}
}},
{ static_cast<unsigned char>(9), {
	{ static_cast<unsigned char>(9), isotopeinfo( 18.998403163, 1.0, 19, 9, false)}
}},
{ static_cast<unsigned char>(10), {
	{ static_cast<unsigned char>(10), isotopeinfo( 19.99244018, 0.9048, 20, 10, false)},
	{ static_cast<unsigned char>(10), isotopeinfo( 20.9938467, 0.0027, 21, 10, false)},
	{ static_cast<unsigned char>(10), isotopeinfo( 21.9913851, 0.0925, 22, 10, false)}
}},
{ static_cast<unsigned char>(11), {
	{ static_cast<unsigned char>(11), isotopeinfo( 22.98976928, 1.0, 23, 11, false)}
}},
{ static_cast<unsigned char>(12), {
	{ static_cast<unsigned char>(12), isotopeinfo( 23.9850417, 0.7888, 24, 12, false)},
	{ static_cast<unsigned char>(12), isotopeinfo( 24.985837, 0.10034, 25, 12, false)},
	{ static_cast<unsigned char>(12), isotopeinfo( 25.982593, 0.1109, 26, 12, false)}
}},
{ static_cast<unsigned char>(13), {
	{ static_cast<unsigned char>(13), isotopeinfo( 26.9815385, 1.0, 27, 13, false)}
}},
{ static_cast<unsigned char>(14), {
	{ static_cast<unsigned char>(14), isotopeinfo( 27.976926535, 0.92191, 28, 14, false)},
	{ static_cast<unsigned char>(14), isotopeinfo( 28.976494665, 0.04699, 29, 14, false)},
	{ static_cast<unsigned char>(14), isotopeinfo( 29.97377001, 0.0311, 30, 14, false)}
}},
{ static_cast<unsigned char>(15), {
	{ static_cast<unsigned char>(15), isotopeinfo( 30.973761998, 1.0, 31, 15, false)}
}},
{ static_cast<unsigned char>(16), {
	{ static_cast<unsigned char>(16), isotopeinfo( 31.972071174, 0.9441, 32, 16, false)},
	{ static_cast<unsigned char>(16), isotopeinfo( 32.97145891, 0.00797, 33, 16, false)},
	{ static_cast<unsigned char>(16), isotopeinfo( 33.967867, 0.0477, 34, 16, false)},
	{ static_cast<unsigned char>(16), isotopeinfo( 35.967081, 0.00018700000000000002, 36, 16, false)}
}},
{ static_cast<unsigned char>(17), {
	{ static_cast<unsigned char>(17), isotopeinfo( 34.9688527, 0.755, 35, 17, false)},
	{ static_cast<unsigned char>(17), isotopeinfo( 36.9659026, 0.239, 37, 17, false)}
}},
{ static_cast<unsigned char>(18), {
	{ static_cast<unsigned char>(18), isotopeinfo( 35.9675451, 0.003336, 36, 18, false)},
	{ static_cast<unsigned char>(18), isotopeinfo( 37.962732, 0.000629, 38, 18, false)},
	{ static_cast<unsigned char>(18), isotopeinfo( 39.96238312, 0.996035, 40, 18, false)}
}},
{ static_cast<unsigned char>(19), {
	{ static_cast<unsigned char>(19), isotopeinfo( 38.96370649, 0.932581, 39, 19, false)},
	{ static_cast<unsigned char>(19), isotopeinfo( 39.9639982, 0.000117, 40, 19, true)},
	{ static_cast<unsigned char>(19), isotopeinfo( 40.96182526, 0.067302, 41, 19, false)}
}},
{ static_cast<unsigned char>(20), {
	{ static_cast<unsigned char>(20), isotopeinfo( 39.9625909, 0.96941, 40, 20, false)},
	{ static_cast<unsigned char>(20), isotopeinfo( 41.958618, 0.00647, 42, 20, false)},
	{ static_cast<unsigned char>(20), isotopeinfo( 42.958766, 0.00135, 43, 20, false)},
	{ static_cast<unsigned char>(20), isotopeinfo( 43.955482, 0.02086, 44, 20, false)},
	{ static_cast<unsigned char>(20), isotopeinfo( 45.95369, 4e-05, 46, 20, false)},
	{ static_cast<unsigned char>(20), isotopeinfo( 47.9525228, 0.00187, 48, 20, true)}
}},
{ static_cast<unsigned char>(21), {
	{ static_cast<unsigned char>(21), isotopeinfo( 44.955908, 1.0, 45, 21, false)}
}},
{ static_cast<unsigned char>(22), {
	{ static_cast<unsigned char>(22), isotopeinfo( 45.952628, 0.0825, 46, 22, false)},
	{ static_cast<unsigned char>(22), isotopeinfo( 46.951759, 0.0744, 47, 22, false)},
	{ static_cast<unsigned char>(22), isotopeinfo( 47.947942, 0.7372, 48, 22, false)},
	{ static_cast<unsigned char>(22), isotopeinfo( 48.947866, 0.0541, 49, 22, false)},
	{ static_cast<unsigned char>(22), isotopeinfo( 49.944787, 0.0518, 50, 22, false)}
}},
{ static_cast<unsigned char>(23), {
	{ static_cast<unsigned char>(23), isotopeinfo( 49.947156, 0.0025, 50, 23, true)},
	{ static_cast<unsigned char>(23), isotopeinfo( 50.943957, 0.9975, 51, 23, false)}
}},
{ static_cast<unsigned char>(24), {
	{ static_cast<unsigned char>(24), isotopeinfo( 49.946042, 0.04345, 50, 24, false)},
	{ static_cast<unsigned char>(24), isotopeinfo( 51.940506, 0.8378899999999999, 52, 24, false)},
	{ static_cast<unsigned char>(24), isotopeinfo( 52.940648, 0.09501, 53, 24, false)},
	{ static_cast<unsigned char>(24), isotopeinfo( 53.938879, 0.02365, 54, 24, false)}
}},
{ static_cast<unsigned char>(25), {
	{ static_cast<unsigned char>(25), isotopeinfo( 54.938044, 1.0, 55, 25, false)}
}},
{	static_cast<unsigned char>(26), {
	{ static_cast<unsigned char>(26), isotopeinfo( 53.939609, 0.05845, 54, 26, false)},
	{ static_cast<unsigned char>(26), isotopeinfo( 55.934936, 0.9175399999999999, 56, 26, false)},
	{ static_cast<unsigned char>(26), isotopeinfo( 56.935393, 0.02119, 57, 26, false)},
	{ static_cast<unsigned char>(26), isotopeinfo( 57.933274, 0.00282, 58, 26, false)}
}},
{ static_cast<unsigned char>(27), {
	{ static_cast<unsigned char>(27), isotopeinfo( 58.933194, 1.0, 59, 27, false)}
}},
{ static_cast<unsigned char>(28), {
	{ static_cast<unsigned char>(28), isotopeinfo( 57.935342, 0.680769, 58, 28, false)},
	{ static_cast<unsigned char>(28), isotopeinfo( 59.930786, 0.262231, 60, 28, false)},
	{ static_cast<unsigned char>(28), isotopeinfo( 60.931056, 0.011399, 61, 28, false)},
	{ static_cast<unsigned char>(28), isotopeinfo( 61.928345, 0.036345, 62, 28, false)},
	{ static_cast<unsigned char>(28), isotopeinfo( 63.927967, 0.009256, 64, 28, false)}
}},
{ static_cast<unsigned char>(29), {
	{ static_cast<unsigned char>(29), isotopeinfo( 62.929598, 0.6915, 63, 29, false)},
	{ static_cast<unsigned char>(29), isotopeinfo( 64.92779, 0.3085, 65, 29, false)}
}},
{ static_cast<unsigned char>(30), {
	{ static_cast<unsigned char>(30), isotopeinfo( 63.929142, 0.4917, 64, 30, false)},
	{ static_cast<unsigned char>(30), isotopeinfo( 65.926034, 0.2773, 66, 30, false)},
	{ static_cast<unsigned char>(30), isotopeinfo( 66.927128, 0.0404, 67, 30, false)},
	{ static_cast<unsigned char>(30), isotopeinfo( 67.924845, 0.1845, 68, 30, false)},
	{ static_cast<unsigned char>(30), isotopeinfo( 69.92532, 0.0061, 70, 30, false)}
}},
{ static_cast<unsigned char>(31), {
	{ static_cast<unsigned char>(31), isotopeinfo( 68.925574, 0.6010800000000001, 69, 31, false)},
	{ static_cast<unsigned char>(31), isotopeinfo( 70.924703, 0.39892, 71, 31, false)}
}},
{ static_cast<unsigned char>(32), {
	{ static_cast<unsigned char>(32), isotopeinfo( 69.924249, 0.2052, 70, 32, false)},
	{ static_cast<unsigned char>(32), isotopeinfo( 71.9220758, 0.2745, 72, 32, false)},
	{ static_cast<unsigned char>(32), isotopeinfo( 72.923459, 0.0776, 73, 32, false)},
	{ static_cast<unsigned char>(32), isotopeinfo( 73.92117776, 0.3652, 74, 32, false)},
	{ static_cast<unsigned char>(32), isotopeinfo( 75.9214027, 0.0775, 76, 32, true)}
}},
{ static_cast<unsigned char>(33), {
	{ static_cast<unsigned char>(33), isotopeinfo( 74.921595, 1.0, 75, 33, false)}
}},
{ static_cast<unsigned char>(34), {
	{ static_cast<unsigned char>(34), isotopeinfo( 73.9224759, 0.0086, 74, 34, false)},
	{ static_cast<unsigned char>(34), isotopeinfo( 75.9192137, 0.0923, 76, 34, false)},
	{ static_cast<unsigned char>(34), isotopeinfo( 76.9199142, 0.076, 77, 34, false)},
	{ static_cast<unsigned char>(34), isotopeinfo( 77.917309, 0.2369, 78, 34, false)},
	{ static_cast<unsigned char>(34), isotopeinfo( 79.916522, 0.498, 80, 34, false)},
	{ static_cast<unsigned char>(34), isotopeinfo( 81.9167, 0.0882, 82, 34, true)}
}},
{ static_cast<unsigned char>(35), {
	{ static_cast<unsigned char>(35), isotopeinfo( 78.918338, 0.505, 79, 35, false)},
	{ static_cast<unsigned char>(35), isotopeinfo( 80.91629, 0.49200000000000005, 81, 35, false)}
}},
{ static_cast<unsigned char>(36), {
	{ static_cast<unsigned char>(36), isotopeinfo( 77.920365, 0.00355, 78, 36, true)},
	{ static_cast<unsigned char>(36), isotopeinfo( 79.916378, 0.02286, 80, 36, false)},
	{ static_cast<unsigned char>(36), isotopeinfo( 81.913483, 0.11592999999999999, 82, 36, false)},
	{ static_cast<unsigned char>(36), isotopeinfo( 82.914127, 0.115, 83, 36, false)},
	{ static_cast<unsigned char>(36), isotopeinfo( 83.91149773, 0.56987, 84, 36, false)},
	{ static_cast<unsigned char>(36), isotopeinfo( 85.91061063, 0.17279, 86, 36, false)}
}},
{ static_cast<unsigned char>(37), {
	{ static_cast<unsigned char>(37), isotopeinfo( 84.91178974, 0.7217, 85, 37, false)},
	{ static_cast<unsigned char>(37), isotopeinfo( 86.90918053, 0.2783, 87, 37, true)}
}},
{ static_cast<unsigned char>(38), {
	{ static_cast<unsigned char>(38), isotopeinfo( 83.913419, 0.0056, 84, 38, false)},
	{ static_cast<unsigned char>(38), isotopeinfo( 85.909261, 0.0986, 86, 38, false)},
	{ static_cast<unsigned char>(38), isotopeinfo( 86.908878, 0.07, 87, 38, false)},
	{ static_cast<unsigned char>(38), isotopeinfo( 87.905613, 0.8258, 88, 38, false)}
}},
{ static_cast<unsigned char>(39), {
	{ static_cast<unsigned char>(39), isotopeinfo( 88.90584, 1.0, 89, 39, false)}
}},
{ static_cast<unsigned char>(40), {
	{ static_cast<unsigned char>(40), isotopeinfo( 89.9047, 0.5145, 90, 40, false)},
	{ static_cast<unsigned char>(40), isotopeinfo( 90.90564, 0.1122, 91, 40, false)},
	{ static_cast<unsigned char>(40), isotopeinfo( 91.90503, 0.1715, 92, 40, false)},
	{ static_cast<unsigned char>(40), isotopeinfo( 93.90631, 0.1738, 94, 40, false)},
	{ static_cast<unsigned char>(40), isotopeinfo( 95.90827, 0.028, 96, 40, true)}
}},
{ static_cast<unsigned char>(41), {
	{ static_cast<unsigned char>(41), isotopeinfo( 92.90637, 1.0, 93, 41, false)}
}},
{ static_cast<unsigned char>(42), {
	{ static_cast<unsigned char>(42), isotopeinfo( 91.906808, 0.14649, 92, 42, false)},
	{ static_cast<unsigned char>(42), isotopeinfo( 93.905085, 0.09187000000000001, 94, 42, false)},
	{ static_cast<unsigned char>(42), isotopeinfo( 94.905839, 0.15872999999999998, 95, 42, false)},
	{ static_cast<unsigned char>(42), isotopeinfo( 95.904676, 0.16673, 96, 42, false)},
	{ static_cast<unsigned char>(42), isotopeinfo( 96.906018, 0.09582, 97, 42, false)},
	{ static_cast<unsigned char>(42), isotopeinfo( 97.905405, 0.24291999999999997, 98, 42, false)},
	{ static_cast<unsigned char>(42), isotopeinfo( 99.907472, 0.09744, 100, 42, true)}
}},
{ static_cast<unsigned char>(44), {
	{ static_cast<unsigned char>(44), isotopeinfo( 95.90759, 0.0554, 96, 44, false)},
	{ static_cast<unsigned char>(44), isotopeinfo( 97.90529, 0.0187, 98, 44, false)},
	{ static_cast<unsigned char>(44), isotopeinfo( 98.905934, 0.1276, 99, 44, false)},
	{ static_cast<unsigned char>(44), isotopeinfo( 99.904214, 0.126, 100, 44, false)},
	{ static_cast<unsigned char>(44), isotopeinfo( 100.905577, 0.1706, 101, 44, false)},
	{ static_cast<unsigned char>(44), isotopeinfo( 101.904344, 0.3155, 102, 44, false)},
	{ static_cast<unsigned char>(44), isotopeinfo( 103.90543, 0.1862, 104, 44, false)}
}},
{ static_cast<unsigned char>(45), {
	{ static_cast<unsigned char>(45), isotopeinfo( 102.9055, 1.0, 103, 45, false)}
}},
{ static_cast<unsigned char>(46), {
	{ static_cast<unsigned char>(46), isotopeinfo( 101.9056, 0.0102, 102, 46, false)},
	{ static_cast<unsigned char>(46), isotopeinfo( 103.904031, 0.1114, 104, 46, false)},
	{ static_cast<unsigned char>(46), isotopeinfo( 104.90508, 0.2233, 105, 46, false)},
	{ static_cast<unsigned char>(46), isotopeinfo( 105.90348, 0.2733, 106, 46, false)},
	{ static_cast<unsigned char>(46), isotopeinfo( 107.903892, 0.2646, 108, 46, false)},
	{ static_cast<unsigned char>(46), isotopeinfo( 109.905172, 0.1172, 110, 46, false)}
}},
{ static_cast<unsigned char>(47), {
	{ static_cast<unsigned char>(47), isotopeinfo( 106.90509, 0.51839, 107, 47, false)},
	{ static_cast<unsigned char>(47), isotopeinfo( 108.904755, 0.48161000000000004, 109, 47, false)}
}},
{ static_cast<unsigned char>(48), {
	{ static_cast<unsigned char>(48), isotopeinfo( 105.90646, 0.01245, 106, 48, false)},
	{ static_cast<unsigned char>(48), isotopeinfo( 107.904183, 0.008879999999999999, 108, 48, false)},
	{ static_cast<unsigned char>(48), isotopeinfo( 109.903007, 0.1247, 110, 48, false)},
	{ static_cast<unsigned char>(48), isotopeinfo( 110.904183, 0.12795, 111, 48, false)},
	{ static_cast<unsigned char>(48), isotopeinfo( 111.902763, 0.24109, 112, 48, false)},
	{ static_cast<unsigned char>(48), isotopeinfo( 112.904408, 0.12227, 113, 48, true)},
	{ static_cast<unsigned char>(48), isotopeinfo( 113.903365, 0.28754, 114, 48, false)},
	{ static_cast<unsigned char>(48), isotopeinfo( 115.904763, 0.07512, 116, 48, true)}
}},
{ static_cast<unsigned char>(49), {
	{ static_cast<unsigned char>(49), isotopeinfo( 112.904062, 0.04281, 113, 49, false)},
	{ static_cast<unsigned char>(49), isotopeinfo( 114.90387878, 0.95719, 115, 49, true)}
}},
{ static_cast<unsigned char>(50), {
	{ static_cast<unsigned char>(50), isotopeinfo( 111.904824, 0.0097, 112, 50, false)},
	{ static_cast<unsigned char>(50), isotopeinfo( 113.902783, 0.0066, 114, 50, false)},
	{ static_cast<unsigned char>(50), isotopeinfo( 114.9033447, 0.0034, 115, 50, false)},
	{ static_cast<unsigned char>(50), isotopeinfo( 115.901743, 0.1454, 116, 50, false)},
	{ static_cast<unsigned char>(50), isotopeinfo( 116.902954, 0.0768, 117, 50, false)},
	{ static_cast<unsigned char>(50), isotopeinfo( 117.901607, 0.2422, 118, 50, false)},
	{ static_cast<unsigned char>(50), isotopeinfo( 118.903311, 0.0859, 119, 50, false)},
	{ static_cast<unsigned char>(50), isotopeinfo( 119.902202, 0.3258, 120, 50, false)},
	{ static_cast<unsigned char>(50), isotopeinfo( 121.90344, 0.0463, 122, 50, false)},
	{ static_cast<unsigned char>(50), isotopeinfo( 123.905277, 0.0579, 124, 50, false)}
}},
{ static_cast<unsigned char>(51), {
	{ static_cast<unsigned char>(51), isotopeinfo( 120.90381, 0.5721, 121, 51, false)},
	{ static_cast<unsigned char>(51), isotopeinfo( 122.90421, 0.4279, 123, 51, false)}
}},
{ static_cast<unsigned char>(52), {
	{ static_cast<unsigned char>(52), isotopeinfo( 119.90406, 0.0009, 120, 52, false)},
	{ static_cast<unsigned char>(52), isotopeinfo( 121.90304, 0.0255, 122, 52, false)},
	{ static_cast<unsigned char>(52), isotopeinfo( 122.90427, 0.0089, 123, 52, false)},
	{ static_cast<unsigned char>(52), isotopeinfo( 123.90282, 0.0474, 124, 52, false)},
	{ static_cast<unsigned char>(52), isotopeinfo( 124.90443, 0.0707, 125, 52, false)},
	{ static_cast<unsigned char>(52), isotopeinfo( 125.90331, 0.1884, 126, 52, false)},
	{ static_cast<unsigned char>(52), isotopeinfo( 127.904461, 0.3174, 128, 52, true)},
	{ static_cast<unsigned char>(52), isotopeinfo( 129.90622275, 0.3408, 130, 52, true)}
}},
{ static_cast<unsigned char>(53), {
	{ static_cast<unsigned char>(53), isotopeinfo( 126.90447, 1.0, 127, 53, false)}
{ static_cast<unsigned char>(54), {
	{ static_cast<unsigned char>(54), isotopeinfo( 123.90589, 0.00095, 124, 54, false)},
	{ static_cast<unsigned char>(54), isotopeinfo( 125.9043, 0.0008900000000000001, 126, 54, false)},
	{ static_cast<unsigned char>(54), isotopeinfo( 127.903531, 0.0191, 128, 54, false)},
	{ static_cast<unsigned char>(54), isotopeinfo( 128.90478086, 0.26400999999999997, 129, 54, false)},
	{ static_cast<unsigned char>(54), isotopeinfo( 129.9035094, 0.04071, 130, 54, false)},
	{ static_cast<unsigned char>(54), isotopeinfo( 130.905084, 0.21231999999999998, 131, 54, false)},
	{ static_cast<unsigned char>(54), isotopeinfo( 131.90415509, 0.26909, 132, 54, false)},
	{ static_cast<unsigned char>(54), isotopeinfo( 133.905395, 0.10436, 134, 54, false)},
	{ static_cast<unsigned char>(54), isotopeinfo( 135.90721448, 0.08857000000000001, 136, 54, true)}
}},
{ static_cast<unsigned char>(55), {
	{ static_cast<unsigned char>(55), isotopeinfo( 132.90545196, 1.0, 133, 55, false)}
{ static_cast<unsigned char>(56), {
	{ static_cast<unsigned char>(56), isotopeinfo( 129.90632, 0.0011, 130, 56, true)},
	{ static_cast<unsigned char>(56), isotopeinfo( 131.905061, 0.001, 132, 56, false)},
	{ static_cast<unsigned char>(56), isotopeinfo( 133.904508, 0.0242, 134, 56, false)},
	{ static_cast<unsigned char>(56), isotopeinfo( 134.905688, 0.0659, 135, 56, false)},
	{ static_cast<unsigned char>(56), isotopeinfo( 135.904576, 0.0785, 136, 56, false)},
	{ static_cast<unsigned char>(56), isotopeinfo( 136.905827, 0.1123, 137, 56, false)},
	{ static_cast<unsigned char>(56), isotopeinfo( 137.905247, 0.717, 138, 56, false)}
}},
{ static_cast<unsigned char>(57), {
	{ static_cast<unsigned char>(57), isotopeinfo( 137.90712, 0.0008881000000000001, 138, 57, true)},
	{ static_cast<unsigned char>(57), isotopeinfo( 138.90636, 0.9991119, 139, 57, false)},
}},
{ static_cast<unsigned char>(58), {
	{ static_cast<unsigned char>(58), isotopeinfo( 135.907129, 0.00186, 136, 58, false)},
	{ static_cast<unsigned char>(58), isotopeinfo( 137.90599, 0.00251, 138, 58, false)},
	{ static_cast<unsigned char>(58), isotopeinfo( 139.90544, 0.88449, 140, 58, false)},
	{ static_cast<unsigned char>(58), isotopeinfo( 141.90925, 0.11114, 142, 58, false)}
}},
{ static_cast<unsigned char>(59), {
	{ static_cast<unsigned char>(59), isotopeinfo( 140.90766, 1.0, 141, 59, false)}
}},
{ static_cast<unsigned char>(60), {
	{ static_cast<unsigned char>(60), isotopeinfo( 141.90773, 0.27153, 142, 60, false)},
	{ static_cast<unsigned char>(60), isotopeinfo( 142.90982, 0.12172999999999999, 143, 60, false)},
	{ static_cast<unsigned char>(60), isotopeinfo( 143.91009, 0.23798000000000002, 144, 60, true)},
	{ static_cast<unsigned char>(60), isotopeinfo( 144.91258, 0.08292999999999999, 145, 60, false)},
	{ static_cast<unsigned char>(60), isotopeinfo( 145.91312, 0.17189000000000002, 146, 60, false)},
	{ static_cast<unsigned char>(60), isotopeinfo( 147.9169, 0.05756, 148, 60, false)},
	{ static_cast<unsigned char>(60), isotopeinfo( 149.9209, 0.05637999999999999, 150, 60, true)}
}},
{ static_cast<unsigned char>(62), {
	{ static_cast<unsigned char>(62), isotopeinfo( 143.91201, 0.0308, 144, 62, false)},
	{ static_cast<unsigned char>(62), isotopeinfo( 146.9149, 0.15, 147, 62, true)},
	{ static_cast<unsigned char>(62), isotopeinfo( 147.91483, 0.1125, 148, 62, true)},
	{ static_cast<unsigned char>(62), isotopeinfo( 148.91719, 0.1382, 149, 62, false)},
	{ static_cast<unsigned char>(62), isotopeinfo( 149.91728, 0.0737, 150, 62, false)},
	{ static_cast<unsigned char>(62), isotopeinfo( 151.91974, 0.2674, 152, 62, false)},
	{ static_cast<unsigned char>(62), isotopeinfo( 153.92222, 0.2274, 154, 62, false)}
}},
{ static_cast<unsigned char>(63), {
	{ static_cast<unsigned char>(63), isotopeinfo( 150.91986, 0.4781, 151, 63, true)},
	{ static_cast<unsigned char>(63), isotopeinfo( 152.92124, 0.5219, 153, 63, false)}
}},
{ static_cast<unsigned char>(64), {
	{ static_cast<unsigned char>(64), isotopeinfo( 151.9198, 0.002, 152, 64, false)},
	{ static_cast<unsigned char>(64), isotopeinfo( 153.92087, 0.0218, 154, 64, false)},
	{ static_cast<unsigned char>(64), isotopeinfo( 154.92263, 0.148, 155, 64, false)},
	{ static_cast<unsigned char>(64), isotopeinfo( 155.92213, 0.2047, 156, 64, false)},
	{ static_cast<unsigned char>(64), isotopeinfo( 156.92397, 0.1565, 157, 64, false)},
	{ static_cast<unsigned char>(64), isotopeinfo( 157.92411, 0.2484, 158, 64, false)},
	{ static_cast<unsigned char>(64), isotopeinfo( 159.92706, 0.2186, 160, 64, false)}
}},
{ static_cast<unsigned char>(65), {
	{ static_cast<unsigned char>(65), isotopeinfo( 158.92535, 1.0, 159, 65, false)}
}},
{ static_cast<unsigned char>(66), {
	{ static_cast<unsigned char>(66), isotopeinfo( 155.92428, 0.00056, 156, 66, false)},
	{ static_cast<unsigned char>(66), isotopeinfo( 157.92442, 0.00095, 158, 66, false)},
	{ static_cast<unsigned char>(66), isotopeinfo( 159.9252, 0.02329, 160, 66, false)},
	{ static_cast<unsigned char>(66), isotopeinfo( 160.92694, 0.18889, 161, 66, false)},
	{ static_cast<unsigned char>(66), isotopeinfo( 161.92681, 0.25475, 162, 66, false)},
	{ static_cast<unsigned char>(66), isotopeinfo( 162.92874, 0.24896, 163, 66, false)},
	{ static_cast<unsigned char>(66), isotopeinfo( 163.92918, 0.2826, 164, 66, false)}
}},
{ static_cast<unsigned char>(67), {
	{ static_cast<unsigned char>(67), isotopeinfo( 164.93033, 1.0, 165, 67, false)}
}},
{ static_cast<unsigned char>(68), {
	{ static_cast<unsigned char>(68), isotopeinfo( 161.92879, 0.00139, 162, 68, false)},
	{ static_cast<unsigned char>(68), isotopeinfo( 163.92921, 0.01601, 164, 68, false)},
	{ static_cast<unsigned char>(68), isotopeinfo( 165.9303, 0.33503, 166, 68, false)},
	{ static_cast<unsigned char>(68), isotopeinfo( 166.93205, 0.22869, 167, 68, false)},
	{ static_cast<unsigned char>(68), isotopeinfo( 167.93238, 0.26978, 168, 68, false)},
	{ static_cast<unsigned char>(68), isotopeinfo( 169.93547, 0.1491, 170, 68, false)}
}},
{ static_cast<unsigned char>(69), {
	{ static_cast<unsigned char>(69), isotopeinfo( 168.93422, 1.0, 169, 69, false)}
}},
{ static_cast<unsigned char>(70), {
	{ static_cast<unsigned char>(70), isotopeinfo( 167.93389, 0.00126, 168, 70, false)},
	{ static_cast<unsigned char>(70), isotopeinfo( 169.93477, 0.03023, 170, 70, false)},
	{ static_cast<unsigned char>(70), isotopeinfo( 170.93633, 0.14215999999999998, 171, 70, false)},
	{ static_cast<unsigned char>(70), isotopeinfo( 171.93639, 0.21754, 172, 70, false)},
	{ static_cast<unsigned char>(70), isotopeinfo( 172.93822, 0.16097999999999998, 173, 70, false)},
	{ static_cast<unsigned char>(70), isotopeinfo( 173.93887, 0.31895999999999997, 174, 70, false)},
	{ static_cast<unsigned char>(70), isotopeinfo( 175.94258, 0.12887, 176, 70, false)}
}},
{ static_cast<unsigned char>(71), {
	{ static_cast<unsigned char>(71), isotopeinfo( 174.94078, 0.97401, 175, 71, false)},
	{ static_cast<unsigned char>(71), isotopeinfo( 175.94269, 0.02599, 176, 71, true)}
}},
{ static_cast<unsigned char>(72), {
	{ static_cast<unsigned char>(72), isotopeinfo( 173.94005, 0.0016, 174, 72, true)},
	{ static_cast<unsigned char>(72), isotopeinfo( 175.94141, 0.0526, 176, 72, false)},
	{ static_cast<unsigned char>(72), isotopeinfo( 176.94323, 0.186, 177, 72, false)},
	{ static_cast<unsigned char>(72), isotopeinfo( 177.94371, 0.2728, 178, 72, false)},
	{ static_cast<unsigned char>(72), isotopeinfo( 178.94582, 0.1362, 179, 72, false)},
	{ static_cast<unsigned char>(72), isotopeinfo( 179.94656, 0.3508, 180, 72, false)}
}},
{ static_cast<unsigned char>(73), {
	{ static_cast<unsigned char>(73), isotopeinfo( 179.94746, 0.00012009999999999998, 180, 73, true)},
	{ static_cast<unsigned char>(73), isotopeinfo( 180.948, 0.9998799, 181, 73, false)}
}},
{ static_cast<unsigned char>(74), {
	{ static_cast<unsigned char>(74), isotopeinfo( 179.94671, 0.0012, 180, 74, true)},
	{ static_cast<unsigned char>(74), isotopeinfo( 181.948204, 0.265, 182, 74, false)},
	{ static_cast<unsigned char>(74), isotopeinfo( 182.950223, 0.1431, 183, 74, false)},
	{ static_cast<unsigned char>(74), isotopeinfo( 183.950931, 0.3064, 184, 74, false)},
	{ static_cast<unsigned char>(74), isotopeinfo( 185.95436, 0.2843, 186, 74, false)}
}},
{ static_cast<unsigned char>(75), {
	{ static_cast<unsigned char>(75), isotopeinfo( 184.952955, 0.374, 185, 75, false)},
	{ static_cast<unsigned char>(75), isotopeinfo( 186.95575, 0.626, 187, 75, true)}
}},
{ static_cast<unsigned char>(76), {
	{ static_cast<unsigned char>(76), isotopeinfo( 183.952489, 0.0002, 184, 76, true)},
	{ static_cast<unsigned char>(76), isotopeinfo( 185.95384, 0.0159, 186, 76, true)},
	{ static_cast<unsigned char>(76), isotopeinfo( 186.95575, 0.0196, 187, 76, false)},
	{ static_cast<unsigned char>(76), isotopeinfo( 187.95584, 0.1324, 188, 76, false)},
	{ static_cast<unsigned char>(76), isotopeinfo( 188.95814, 0.1615, 189, 76, false)},
	{ static_cast<unsigned char>(76), isotopeinfo( 189.95844, 0.2626, 190, 76, false)},
	{ static_cast<unsigned char>(76), isotopeinfo( 191.96148, 0.4078, 192, 76, false)}
}},
{ static_cast<unsigned char>(77), {
	{ static_cast<unsigned char>(77), isotopeinfo( 190.96059, 0.373, 191, 77, false)},
	{ static_cast<unsigned char>(77), isotopeinfo( 192.96292, 0.627, 193, 77, false)}
}},
{ static_cast<unsigned char>(78), {
	{ static_cast<unsigned char>(78), isotopeinfo( 189.95993, 0.00011999999999999999, 190, 78, true)},
	{ static_cast<unsigned char>(78), isotopeinfo( 191.96104, 0.00782, 192, 78, false)},
	{ static_cast<unsigned char>(78), isotopeinfo( 193.962681, 0.32864, 194, 78, false)},
	{ static_cast<unsigned char>(78), isotopeinfo( 194.964792, 0.33775, 195, 78, false)},
	{ static_cast<unsigned char>(78), isotopeinfo( 195.964952, 0.25211, 196, 78, false)},
	{ static_cast<unsigned char>(78), isotopeinfo( 197.96789, 0.07356, 198, 78, false)}
}},
{ static_cast<unsigned char>(79), {
	{ static_cast<unsigned char>(79), isotopeinfo( 196.966569, 1.0, 197, 79, false)}
}},
{ static_cast<unsigned char>(80), {
	{ static_cast<unsigned char>(80), isotopeinfo( 195.96583, 0.0015, 196, 80, false)},
	{ static_cast<unsigned char>(80), isotopeinfo( 197.966769, 0.1004, 198, 80, false)},
	{ static_cast<unsigned char>(80), isotopeinfo( 198.968281, 0.1694, 199, 80, false)},
	{ static_cast<unsigned char>(80), isotopeinfo( 199.968327, 0.2314, 200, 80, false)},
	{ static_cast<unsigned char>(80), isotopeinfo( 200.970303, 0.1317, 201, 80, false)},
	{ static_cast<unsigned char>(80), isotopeinfo( 201.970643, 0.2974, 202, 80, false)},
	{ static_cast<unsigned char>(80), isotopeinfo( 203.973494, 0.0682, 204, 80, false)}
}},
{ static_cast<unsigned char>(81), {
	{ static_cast<unsigned char>(81), isotopeinfo( 202.972345, 0.2944, 203, 81, false)},
	{ static_cast<unsigned char>(81), isotopeinfo( 204.974428, 0.7041, 205, 81, false)}
}},
{ static_cast<unsigned char>(82), {
	{ static_cast<unsigned char>(82), isotopeinfo( 203.973044, 0.013999999999999999, 204, 82, false)},
	{ static_cast<unsigned char>(82), isotopeinfo( 205.974466, 0.24100000000000002, 206, 82, false)},
	{ static_cast<unsigned char>(82), isotopeinfo( 206.975897, 0.221, 207, 82, false)},
	{ static_cast<unsigned char>(82), isotopeinfo( 207.976653, 0.524, 208, 82, false)}
}},
{ static_cast<unsigned char>(83), {
	{ static_cast<unsigned char>(83), isotopeinfo( 208.9804, 1.0, 209, 83, true)}
}},
{ static_cast<unsigned char>(90), {
	{ static_cast<unsigned char>(90), isotopeinfo( 230.03313, 0.0002, 230, 90, true)},
	{ static_cast<unsigned char>(90), isotopeinfo( 232.03806, 0.9998, 232, 90, true)}
}},
{ static_cast<unsigned char>(92), {
	{ static_cast<unsigned char>(92), isotopeinfo( 234.04095, 5.4000000000000005e-05, 234, 92, true)},
	{ static_cast<unsigned char>(92), isotopeinfo( 235.04393, 0.007204, 235, 92, true)},
	{ static_cast<unsigned char>(92), isotopeinfo( 238.05079, 0.992742, 238, 92, true)}
}},
};

//no isotopes for artificial elements 91, 89-84, 61, 43
