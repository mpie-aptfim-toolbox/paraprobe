# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np
from mendeleev import *

#generate C++ source code for all elements for ConfigShared::MyElements
cxx = 'map<string, elementinfo> ConfigShared::MyElements = {' + '\n'
for Z in np.arange(1,93):
    el = element(int(Z))
    symb = el.symbol
    if len(symb) == 1:
        symb += ':'
    if el.abundance_crust != None and el.abundance_sea != None:
        cxx += '{"' + symb + '", elementinfo( ' + str(el.mass) + ', ' + str(el.atomic_number) + ')},' + '\n'
cxx += '};'
with open('MyElementsParsedFromMendeleev.cpp','w') as f:
    f.write(cxx) #.replace('"','\''))

#generate C++ source code for all isotopes for ConfigShared::MyIsotopes
#MyIsotopes[static_cast<unsigned char>(' + str(Z) + ')] = 
cxx = 'map<unsigned char, map<unsigned char, isotopeinfo> ConfigShared::MyIsotopes = {' + '\n'
for Z in np.arange(1,93):
    el = element(int(Z))
    #symb = el.symbol
    #if len(symb) == 1:
    #    symb += ':'
    #cxx += '{"' + symb + '", elementinfo( ' + str(el.mass) + ', ' + str(el.atomic_number) + ')},' + '\n'
    cxx += '{ static_cast<unsigned char>(' + str(Z) + '), {' + '\n'
    for iso in el.isotopes:
        radio = 'false'
        if iso.is_radioactive == True: #we keep only the stable naturally occurring isotopes
            radio = 'true'
        if iso.abundance != None:
            cxx += '\t' + '{ static_cast<unsigned char>(' + str(Z) + '), isotopeinfo( ' + str(iso.mass) + ', ' + str(iso.abundance) + ', ' + str(iso.mass_number) + ', ' + str(iso.atomic_number) + ', ' + radio + ')},' + '\n'
            #cxx += 'ConfigShared::MyIsotopes[static_cast<unsigned char>(' + str(Z) + ')][static_cast<unsigned char>(' + str(iso.mass_number) + ')] = isotopeinfo( ' + str(iso.mass) + ', ' + str(iso.abundance) + ', ' + str(iso.mass_number) + ', ' + str(iso.atomic_number) + ', ' + radio + ');' + '\n'
        #print(str(iso.atomic_number) + ' ' + str(iso.mass_number) + ' ' + str(iso.mass))
    #print(el.symbol)
with open('MyIsotopesParsedFromMendeleev.cpp','w') as f:
    f.write(cxx) #.replace('"','\''))
#cxx += '};'


 
