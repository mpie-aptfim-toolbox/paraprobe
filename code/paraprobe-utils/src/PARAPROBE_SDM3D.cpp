//##MK::GPLV3

#include "PARAPROBE_SDM3D.h"

//implements functionality to compute two-point correlation functions in spherical environment about ion location supporting
//by 3d binning with binwidth _width to identify directionality dependent features and perform methods as developed by Kalidindi et al.


ostream& operator<<(ostream& in, d3dd const & val)
{
	in << "DiffVec = " << val.dx << ";" << val.dy << ";" << val.dz << " Distance " << val.dSQR << "\n";
	return in;
}

/*
bool SortDiffForAscDistance(const d3dd & a, const d3dd & b)
{
	return a.dSQR < b.dSQR;
}
*/


ostream& operator<<(ostream& in, vxlcube const & val)
{
	in << "NX/NXY/NXYZ = " << val.NX << ";" << val.NXY << ";" << val.NXYZ << "\n";
	in << "XMI/XMX = " << val.xmi << ";" << val.xmx << "\n";
	in << "YMI/YMX = " << val.ymi << ";" << val.ymx << "\n";
	in << "ZMI/ZMX = " << val.zmi << ";" << val.zmx << "\n";
	return in;
}


sdm3d::sdm3d()
{
	box = vxlcube();
	SQRRmax = 0.0;
	Offset = 0;
	dump = 0;
}


sdm3d::sdm3d( const apt_real rmax, const apt_real width, const int nmax )
{
	//probe first of all expected number of bins, bin aggregate is a cube with odd number of bins along edge, cubic bins of width _width
	//MK::uses right-handed coordinate system with origin centered at ((support.ni - 1) + 0.5)*support.width	
	if ( width < EPSILON ) {
		cerr << "SDM3D initialization _width must be positive and finite!" << "\n"; return;
	}
	apt_real halfedgelen = ceil(rmax / width ) + 1.0; //MK: +1 for one layer guardzone
	int Nhalf = static_cast<int>(halfedgelen);
	int N = 1 + 2 * Nhalf; //two halfedgelen plus center bin
	
	size_t N3 = CUBE(static_cast<size_t>(N));
	if ( N3 >= static_cast<size_t>(INT32MX) ) {
		cerr << "SDM3D initialization discretization is with N = " << N << " out of range INT32MX of current implementation!" << "\n"; return;
	}
	if ( N*N*N > CUBE(nmax) ) {
		cerr << "SDM3D initialization discretization is with N = " << N << " too fine!" << "\n"; return;
	}
	/*else {
		cout << "Computing two point statistics / SDM3D with N = " << N << "\n";
	}*/
	
	//MK:: local continuum coordinates relative to center (0,0,0) about target/central ions
	apt_real Ld = 1.0 * (halfedgelen + 0.5) * width;
	box = vxlcube(	N, width, -Ld, +Ld, -Ld, +Ld, -Ld, +Ld ); //cubic bin grid about central bin
	
	SQRRmax = SQR(rmax); //maximum length of feature vector
	Offset = Nhalf;
	dump = 0;

//cout << "halfedgelen " <<  halfedgelen << " Nhalf " << Nhalf << " N " << N << " Ld " << Ld << " box.NXYZ " << box.NXYZ << "\n";

	try {
		cnts = vector<unsigned int>( box.NXYZ, 0 );
	}
	catch (bad_alloc &croak) {
		cerr << "SDM3D initialization allocation error for cnts!" << "\n"; return;
	}
}


sdm3d::~sdm3d()
{
	//MK::vector clears after itself
}


void sdm3d::add( d3dd const & where )
{
	//MK::add only if within maximum length of feature vector
	//MK::where uvw coordinates can be negative translation into local bin coordinate system necessary
	if ( where.dSQR <= SQRRmax ) {

		int NX = box.NX;
		int NXY = box.NXY;
		apt_real _width = 1.f / box.width;
//cout << "NX/NXY/_width/cnts.size() " << NX << ";" << NXY << ";" << _width << ";" << cnts.size() << "\n";

		//MK::cast issues http://jkorpela.fi/round.html, https://www.cs.cmu.edu/~rbd/papers/cmj-float-to-int.html
		//##MK::binning like so int discretelyhere = there, we get concentration at the 0 component values symmetrical to coordinates undesired
		int targetbin = 0;
		apt_real there = where.dx * _width;
		int here = (there >= 0.f) ? (int)(there+0.5) : (int)(there-0.5);
		targetbin = targetbin + (here + Offset)*1;
//cout << where.dx << "\t\t" << there << "\t\t" << here << "\t\t" << targetbin << "\n";

		there = where.dy * _width;
		here = (there >= 0.f) ? (int)(there+0.5) : (int)(there-0.5);
		targetbin = targetbin + (here + Offset)*NX;
//cout << where.dy << "\t\t" << there << "\t\t" << here << "\t\t" << targetbin << "\n";

		there = where.dz * _width;
		here = (there >= 0.f) ? (int)(there+0.5) : (int)(there-0.5);
		targetbin = targetbin + (here + Offset)*NXY;

//cout << where.dz << "\t\t" << there << "\t\t" << here << "\t\t" << targetbin << "\n";

//cout << where.w << "\t\t" << there << "\t\t" << targetbin << endl;
		cnts.at(targetbin)++;
	}
	else {
		dump++;
	}
}


/*
void npc3d::normalize()
{
	apt_real sum = 0.f;
	for( size_t b = 0; b < cnts.size(); ++b )
		sum += cnts[b];

	if ( sum >= 1.f ) { //at least one count
		for( size_t b = 0; b < cnts.size(); ++b )
			cnts[b] /= sum;
	}
}

sqb npc3d::get_support()
{
	return support;
}
*/


p3d sdm3d::get_bincenter( const unsigned int ix, const unsigned int iy, const unsigned int iz )
{
	//the npc3d histogram bins belong to an 2n+1 aggregate where the center cell
	//(i.e. cell position n,n,n) contains the origin of a 3d Euclidean coordinate system
	//if ( ix < this->support.nx && iy < this->support.ny && iz < this->support.nz ) {
	return p3d(
			(static_cast<apt_real>(ix) - static_cast<apt_real>((this->box.NX-1)/2)) * this->box.width,
			(static_cast<apt_real>(iy) - static_cast<apt_real>((this->box.NX-1)/2)) * this->box.width,
			(static_cast<apt_real>(iz) - static_cast<apt_real>((this->box.NX-1)/2)) * this->box.width     );
}


unsigned int sdm3d::get_nx()
{
	return this->box.NX;
}


unsigned int sdm3d::get_nxyz()
{
	return this->box.NXYZ;
}
