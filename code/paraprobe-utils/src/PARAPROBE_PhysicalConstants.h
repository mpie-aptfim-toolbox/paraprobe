//##MK::GPLV3

#ifndef __PARAPROBE_ABINITIO_H__
#define __PARAPROBE_ABINITIO_H__

//natural constants
#define MYPI							(3.141592653589793238462643383279502884197169399375105820974)
#define ONE_PI							((1.0)*(MYPI))
#define TWO_PI							((2.0)*(MYPI))
#define kboltzmann						(1.3806488e-23)		//J/Kmol
#define echarge							(1.602176565e-19)	//Coulomb
#define Navogadro						(6.022140857e+23)	//1/mol
#define RGAS							(8.31446154) 		//(Navogadro)*(kboltzmann)) //J/K/mol

//natural beauty
#define SQR(a)							((a)*(a))
#define CUBE(a)							((a)*(a)*(a))
#define MIN(X,Y)						(((X) < (Y)) ? (X) : (Y))
#define MAX(X,Y)						(((X) < (Y)) ? (Y) : (X))
#define CLAMP(x,lo,hi)					(MIN(MAX((x), (lo)), (hi)))


//MK::coordinate system is right-handed x,y,z
#define PARAPROBE_XAXIS					0
#define PARAPROBE_YAXIS					1
#define PARAPROBE_ZAXIS					2

//logical
#define NO								0x00
#define YES								0x01


//crystal lattice
#define ATOMS_PER_UNITCELL_FCC			(4)

#define FCC								(0)
#define ALUMINIUM						(0)
#define AL3SC							(1)
#define WOLFRAM							(2)
#define AL3LI							(3)
#define SPACEGROUP						(4)

#define MMM								(2)


#endif
