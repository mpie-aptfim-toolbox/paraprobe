//##MK::GPLV3


#ifndef __PARAPROBE_UTILS_CRYSTALLOGRAPHY_H__
#define __PARAPROBE_UTILS_CRYSTALLOGRAPHY_H__

//#include "PARAPROBE_MPIDatatypes.h"
#include "CONFIG_Shared.h"

struct motifunit
{
	apt_real u1;					//fractional coordinates
	apt_real u2;
	apt_real u3;
	unsigned char elementZ;			//element number
	unsigned char chargestate;		//assumed charge state, ##MK::here used to decode mass-to-charge
	unsigned char pad1;
	unsigned char pad2;
	motifunit() : u1(0.0), u2(0.0), u3(0.0), elementZ(0x00), chargestate(0x00), pad1(0x00), pad2(0x00) {}
	motifunit( const apt_real _u1, const apt_real _u2, const apt_real _u3, const unsigned char _elemZ, const unsigned char _charge ) :
		u1(_u1), u2(_u2), u3(_u3), elementZ(_elemZ), chargestate(_charge), pad1(0x00), pad2(0x00) {}
};

ostream& operator<<(ostream& in, motifunit const & val);


struct crystalstructure
{
	apt_real a;
	apt_real b;
	apt_real c;
	apt_real alpha;
	apt_real beta;
	apt_real gamma;
	t3x3 mtrx;
	p3d a1;
	p3d a2;
	p3d a3;
	unsigned short spacegroup;
	unsigned short pad;
	//utility, an axis-aligned bounding box about the given primitive cell in lab coordinate system
	apt_real box_xmi;
	apt_real box_xmx;
	apt_real box_ymi;
	apt_real box_ymx;
	apt_real box_zmi;
	apt_real box_zmx;
	apt_real box_len;

	vector<motifunit> motif;
	crystalstructure() : a(0.0), b(0.0), c(0.0), alpha(0.0), beta(0.0), gamma(0.0), mtrx(t3x3()),
			a1(p3d(1.0, 0.0, 0.0)), a2(p3d(0.0, 1.0, 0.0)), a3(p3d(0.0, 0.0, 1.0)), spacegroup(0), pad(0),
				box_xmi(F32MX), box_xmx(F32MI), box_ymi(F32MX), box_ymx(F32MI), box_zmi(F32MX), box_zmx(F32MI), box_len(0.0),
					motif(vector<motifunit>()) {}
	crystalstructure( const apt_real _a, const apt_real _b, const apt_real _c, const apt_real _alpha, const apt_real _beta, const apt_real _gamma,
			const unsigned short _spacegrp, vector<motifunit> const & in )
	{
		this->a = _a;
		this->b = _b;
		this->c = _c;
		this->alpha = DEGREE2RADIANT(_alpha);
		this->beta = DEGREE2RADIANT(_beta);
		this->gamma = DEGREE2RADIANT(_gamma);
		this->mtrx = t3x3();
		this->a1 = p3d(1.0, 0.0, 0.0);
		this->a2 = p3d(0.0, 1.0, 0.0);
		this->a3 = p3d(0.0, 0.0, 1.0);
		this->spacegroup = _spacegrp;
		this->pad = 0;
		//size_t nmotif = 0;

		//make spacegroup-specific definitions, we use primitive cells
		switch(this->spacegroup)
		{
			//##MK::list further space group members
			//first, the constraints on the lattice base vectors
			//second, the transformation matrix mtrx
			//case XX:
			//	constraints on a,b,c
			//	mtrx

			case 62: //Pnma, e.g. CaTiO3
				this->mtrx = t3x3( 	this->a,			0.0,				0.0,
									0.0,				this->b, 			0.0,
									0.0,				0.0,				this->c );
				break;
			case 141: //I4_1/amd, e.g. Zircon, body-centered tetragonal primitive
				this->b = _a;
				this->mtrx = t3x3(	-0.5*this->a,		+0.5*this->a,		+0.5*this->c,
									+0.5+this->a,		-0.5*this->a,		+0.5*this->c,
									+0.5*this->a,		+0.5*this->a,		-0.5*this->c  );
				break;
			case 194: //hcp, e.g. Mg
				this->b = _a;
				this->mtrx = t3x3( 	+0.5*this->a,		-0.5*sqrt(3.0)*this->a,			0.0,
									+0.5*this->a,		+0.5*sqrt(3.0)*this->a,			0.0,
									0.0,				0.0,							this->c );
				break;
			case 221: //L12, e.g. Cu3Al
				//set specific specialization of the tricline case
				this->b = _a;
				this->c = _a;
				//this->alpha = DEGREE2RADIANT(90.0);
				//this->beta = DEGREE2RADIANT(90.0);
				//this->gamma = DEGREE2RADIANT(90.0);
				this->mtrx = t3x3( 	this->a,			0.0,				0.0,
									0.0,				this->a, 			0.0,
									0.0,				0.0,				this->a );
				break;
			case 225: //fcc, e.g. Cu
				this->b = _a;
				this->c = _a;
				//this->alpha = DEGREE2RADIANT(60.0);
				//this->beta = DEGREE2RADIANT(60.0);
				//this->gamma = DEGREE2RADIANT(60.0);
				this->mtrx = t3x3( 	0.0,				+0.5*this->a,		+0.5*this->a,
									+0.5*this->a,		0.0,				+0.5*this->a,
									+0.5*this->a,		+0.5*this->a,		0.0 );
				break;
			case 229: //bcc, e.g. W
				this->b = _a;
				this->c = _a;
				/*this->alpha = DEGREE2RADIANT(60.0);
				this->beta = DEGREE2RADIANT(60.0);
				this->gamma = DEGREE2RADIANT(60.0);*/
				this->mtrx = t3x3( 	-0.5*this->a,		+0.5*this->a,		+0.5*this->a,
									+0.5*this->a,		-0.5*this->a,		+0.5*this->a,
									+0.5*this->a,		+0.5*this->a,		-0.5*this->a );
				break;
			default:
				cerr << "The space group is not supported, implement it use e.g. the aflow.org/CrystalDatabase !" << "\n"; break;
		}
		//compute lattice vectors in Cartesian space mtrx leftmultiplied on Cartesian coordinate system base vectors
		p3d xbar = p3d( 1.0, 0.0, 0.0 );
		p3d ybar = p3d( 0.0, 1.0, 0.0 );
		p3d zbar = p3d( 0.0, 0.0, 1.0 );
		this->a1 = p3d( this->mtrx.a11 * xbar.x + this->mtrx.a12 * ybar.x + this->mtrx.a13 * zbar.x,
						this->mtrx.a11 * xbar.y + this->mtrx.a12 * ybar.y + this->mtrx.a13 * zbar.y,
						this->mtrx.a11 * xbar.z + this->mtrx.a12 * ybar.z + this->mtrx.a13 * zbar.z   );

		this->a2 = p3d( this->mtrx.a21 * xbar.x + this->mtrx.a22 * ybar.x + this->mtrx.a23 * zbar.x,
						this->mtrx.a21 * xbar.y + this->mtrx.a22 * ybar.y + this->mtrx.a23 * zbar.y,
						this->mtrx.a21 * xbar.z + this->mtrx.a22 * ybar.z + this->mtrx.a23 * zbar.z   );

		this->a3 = p3d( this->mtrx.a31 * xbar.x + this->mtrx.a32 * ybar.x + this->mtrx.a33 * zbar.x,
						this->mtrx.a31 * xbar.y + this->mtrx.a32 * ybar.y + this->mtrx.a33 * zbar.y,
						this->mtrx.a31 * xbar.z + this->mtrx.a32 * ybar.z + this->mtrx.a33 * zbar.z   );
		//copy in the motif, ##MK::no copy-in check here
		this->motif = vector<motifunit>();
		for( auto it = in.begin(); it != in.end(); it++ ) {
			this->motif.push_back( *it );
		}

		//utility tasks
		//get uvw lattice plane limits to fully enclose tip AABB, ##MK::exemplarily fcc crystal lattice only here
		//the unit cell is a parallelepiped so we get the bounding box by checking the extremal positions of its 8 vertices
		vector<p3d> verts;
		verts.push_back( p3d(	0.0, 0.0, 0.0) ); //origin, or rebase
		verts.push_back( p3d( 	0.0 + this->a1.x,
								0.0 + this->a1.y,
								0.0 + this->a1.z) ); //a0a1
		verts.push_back( p3d( 	0.0 + this->a2.x,
								0.0 + this->a2.y,
								0.0 + this->a2.z) ); //a0a2
		verts.push_back( p3d( 	0.0 + this->a3.x,
								0.0 + this->a3.y,
								0.0 + this->a3.z) ); //a0a3
		verts.push_back( p3d( 	0.0 + this->a1.x + this->a2.x,
								0.0 + this->a1.y + this->a2.y,
								0.0 + this->a1.z + this->a2.z) ); //a0a1a2
		verts.push_back( p3d( 	0.0 + this->a3.x + this->a2.x,
								0.0 + this->a3.y + this->a2.y,
								0.0 + this->a3.z + this->a2.z) ); //a0a3a2
		verts.push_back( p3d(	0.0 + this->a1.x + this->a3.x,
								0.0 + this->a1.y + this->a3.y,
								0.0 + this->a1.z + this->a3.z) ); //a0a1a3
		verts.push_back( p3d( 	0.0 + this->a1.x + this->a2.x + this->a3.x,
								0.0 + this->a1.y + this->a2.y + this->a3.y,
								0.0 + this->a1.z + this->a2.z + this->a3.z ) ); //a0a1a2a3
		this->box_xmi = F32MX;
		this->box_xmx = F32MI;
		this->box_ymi = F32MX;
		this->box_ymx = F32MI;
		this->box_zmi = F32MX;
		this->box_zmx = F32MI;
		for ( auto it = verts.begin(); it != verts.end(); it++ ) {
			if ( it->x <= this->box_xmi )
				this->box_xmi = it->x;
			if ( it->x >= this->box_xmx )
				this->box_xmx = it->x;
			if ( it->y <= this->box_ymi )
				this->box_ymi = it->y;
			if ( it->y >= this->box_ymx )
				this->box_ymx = it->y;
			if ( it->z <= this->box_zmi )
				this->box_zmi = it->z;
			if ( it->z >= this->box_zmx )
				this->box_zmx = it->z;
		}
		//use shortest unit cell base vector to scale sphere ##MK::find a more efficient way
		apt_real a1len = SQR(this->a1.x - 0.0) + SQR(this->a1.y - 0.0) + SQR(this->a1.z - 0.0);
		apt_real a2len = SQR(this->a2.x - 0.0) + SQR(this->a2.y - 0.0) + SQR(this->a2.z - 0.0);
		apt_real a3len = SQR(this->a3.x - 0.0) + SQR(this->a3.y - 0.0) + SQR(this->a3.z - 0.0);
		this->box_len = a1len;
		if ( a2len <= this->box_len )
			this->box_len = a2len;
		if ( a3len <= this->box_len )
			this->box_len = a3len;
		this->box_len = sqrt(this->box_len);

		/*cout << "xmi " << this->box_xmi << "\n";
		cout << "xmx " << this->box_xmx << "\n";
		cout << "ymi " << this->box_ymi << "\n";
		cout << "ymx " << this->box_ymx << "\n";
		cout << "zmi " << this->box_zmi << "\n";
		cout << "zmx " << this->box_zmx << "\n";*/
	}

	apt_real unitcell_volume()
	{
		p3d a2a3 = cross( this->a2, this->a3 );
		return fabs( dot( this->a1, a2a3 ) );
	}

	p3d get_atom_pos( const size_t _b, const int _u, const int _v, const int _w )
	{
		motifunit atom = this->motif.at(_b);
		return p3d(
				(atom.u1 + _u)*this->a1.x + (atom.u2 + _v)*this->a2.x + (atom.u3 + _w)*this->a3.x,
				(atom.u1 + _u)*this->a1.y + (atom.u2 + _v)*this->a2.y + (atom.u3 + _w)*this->a3.y,
				(atom.u1 + _u)*this->a1.z + (atom.u2 + _v)*this->a2.z + (atom.u3 + _w)*this->a3.z  );
	}

	unsigned char get_element_Z( const size_t b )
	{
		return this->motif.at(b).elementZ;
	}

	unsigned char get_chargestate( const size_t b )
	{
		return this->motif.at(b).chargestate;
	}
};

ostream& operator<<(ostream& in, crystalstructure const & val);


#endif
