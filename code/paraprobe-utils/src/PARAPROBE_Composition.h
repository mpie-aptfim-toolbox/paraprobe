//##MK::GPLV3

#ifndef __PARAPROBE_UTILS_COMPOSITION_H__
#define __PARAPROBE_UTILS_COMPOSITION_H__

#include "PARAPROBE_PeriodicTable.h"

class compositionTable
{
public:
	compositionTable();
	~compositionTable();

	bool load_composition_measurement( const string fn );
	bool load_contaminants_measurement( const string fn );

	//composition is a bucket list providing constant time access to all chemical elements for a given Z
	//if composition.at(Z).Z == UNOCCUPIED chemical element was loaded
	vector<chemicalelement> composition;
};


#endif
