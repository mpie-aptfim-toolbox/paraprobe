//##MK::GPLV3

#include "PARAPROBE_Vxlizer.h"

vxlizer::vxlizer()
{
	vxlgrid = sqb();

	IsVacuum = NULL;
	IsSurface = NULL;
	IsInside = NULL;
}


vxlizer::~vxlizer()
{
	delete [] IsVacuum; IsVacuum = NULL;
	delete [] IsSurface; IsSurface = NULL;
	delete [] IsInside; IsInside = NULL;
}
