//##MK::GPLV3

#include "PARAPROBE_Composition.h"


compositionTable::compositionTable()
{
	//init composition table
	for( unsigned char Z = 0; Z < STABLE_ELEMENTS_MAXIMUM; Z++ ) {
		composition.push_back( chemicalelement() );
	}
}


compositionTable::~compositionTable()
{
}


bool compositionTable::load_composition_measurement( const string fn )
{
	cout << "Parsing user-defined nominal composition..." << "\n";
	xml_document<> doc;
	xml_node<> * root_node;

	//read the xml file into a vector
	ifstream theFile( fn );
	vector<char> buffer((istreambuf_iterator<char>(theFile)), istreambuf_iterator<char>());
	buffer.push_back('\0');
	//parse the buffer using the xml file parsing library into doc
	doc.parse<0>(&buffer[0]);
	//find our root node
	root_node = doc.first_node("nominal-composition-table");

	//iterate over the elements
	for (xml_node<> * element_node = root_node->first_node("entry"); element_node; element_node = element_node->next_sibling() ) {
		string elementnm = element_node->first_attribute("symbol")->value();
		string elementZ = element_node->first_attribute("atomic-number")->value();
		size_t Z = (stoi(elementZ) <= static_cast<size_t>(UCHARMX)) ?
				static_cast<size_t>(stoi(elementZ)) : static_cast<size_t>(UCHARMX);

		string conc = element_node->first_attribute("atpercent")->value();
		string err = element_node->first_attribute("error")->value();

cout << "\t___" << elementnm << "___" << Z << "___" << conc << "___" << err << "\n";

		chemicalelement elm = chemicalelement();
		if ( elementnm.size() >= 2 ) {
			elm.nm1 = elementnm.at(0);
			elm.nm2 = elementnm.at(1);
		}
		else if ( elementnm.size() == 1 ) {
			elm.nm1 = elementnm.at(0);
			elm.nm2 = SPACECHARACTER;
		}
		else {
			cerr << "Unable to decipher name of the chemical element" << "\n";
			return false;
		}

		elm.Z = static_cast<unsigned char>(Z);
		elm.atpercent = stof(conc);
		elm.comp_error = stof(err);

		if ( composition.at(Z).Z == UNOCCUPIED ) { //fill in only once!
			composition.at(Z) = elm;
		}
		else {
			cerr << "Attempting to store duplicate chemical element, the composition XML is faulty, quitting now" << "\n";
			return false;
		}
	}

	cout << "Reporting composition..." << "\n";
	for( auto it = composition.begin(); it != composition.end(); ++it ) {
		if ( it->Z != UNOCCUPIED ) {
			cout << "Element " << (int) it->Z << " has been loaded with a composition of " << it->atpercent << "\n";
		}
	}

	//cout << "In total there are " << composition.size() << " elements " << endl;
	return true;
}
