//##MK::GPLV3

#ifndef __PARAPROBE_UTILS_XDMFHDL_H__
#define __PARAPROBE_UTILS_XDMFHDL_H__

#include "PARAPROBE_HDF5.h"

#define XDMF_HEADER_LINE1				"<?xml version=\"1.0\" ?>"
#define XDMF_HEADER_LINE2				"<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>"
#define XDMF_HEADER_LINE3				"<Xdmf xmlns:xi=\"http://www.w3.org/2003/XInclude\" Version=\"2.2\">"

#define WRAPPED_XDMF_SUCCESS				+1
#define WRAPPED_XDMF_IOFAILED				-1


class xdmfHdl
{
	//coordinating instance handling all (sequential) writing of XDMF metafiles detailing HDF5 additional metadata
	//for visualization for Paraview or VisIt
public:
	xdmfHdl();
	~xdmfHdl();

	/*
	//file generation and closing
	int create_crystalloxyz_file( const string xmlfn, const size_t npoints, const string h5ref );
	int create_debugsynthesis_file( const string xmlfn, const size_t nions, const string h5ref );
	//int create_iondistance_file( const string xmlfn, const size_t nions, const string h5ref );
	int create_tipsurface_file( const string xmlfn, const size_t topo_nelements,
			const size_t topo_dims, const size_t geom_dims, const string h5ref );
	int create_voronoicell_vis_file( const string xmlfn, const size_t topo_nelements,
			const size_t topo_dims, const size_t geom_dims, const size_t attr_dims, const string h5ref );
	int create_voronoicell_vol_file( const string xmlfn, const size_t ncells, const string h5ref );

	int create_voronoicell_debug_file( const string xmlfn, const size_t topo_nelements,
			const size_t topo_dims, const size_t geom_dims, const size_t attr_dims, const string h5ref );

	int create_tapsim_recon_vs_synth_file( const string xmlfn, const size_t nions, const string h5ref );
	*/

//private:
	ofstream xdmfout;
};

#endif
