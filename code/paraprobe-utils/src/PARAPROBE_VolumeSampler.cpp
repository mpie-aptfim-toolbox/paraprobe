//##MK::GPLV3

#include "PARAPROBE_VolumeSampler.h"

ostream& operator << (ostream& in, mp3d const & val) {
	in << val.mpid << ";" << val.x << ";" << val.y << ";" << val.z << val.R << ";" << val.nions << "\n";
	return in;
}

volsampler::volsampler()
{
	window = aabb3d();
}


volsampler::~volsampler()
{
}


void volsampler::define_probe_volume( aabb3d const & wdw )
{
	window = wdw;
	//##MK::implement cuboidal sectioning of the volume here
}


void volsampler::define_regular_grid( const apt_real dx, const apt_real dy, const apt_real dz,
				const apt_real R, sqb & bx, const bool* inside, vector<tri3d> const & trihull )
{
	//build an AABBtree for the triangles
	Tree* kauri = NULL;
	try {
		kauri = new class Tree( trihull.size() );
	}
	catch (bad_alloc &croak) {
		cerr << "Unable to allocate memory for the tree of the triangle BVH!" << "\n"; return;
	}

	apt_real xmi = F32MX;
	apt_real xmx = F32MI;
	apt_real ymi = F32MX;
	apt_real ymx = F32MI;
	apt_real zmi = F32MX;
	apt_real zmx = F32MI;
	unsigned int triid = 0;
	for( auto it = trihull.begin(); it != trihull.end(); it++ ) {
		xmi = fmin( fmin(it->x1, it->x2), it->x3 );
		xmx = fmax( fmax(it->x1, it->x2), it->x3 );
		ymi = fmin( fmin(it->y1, it->y2), it->y3 );
		ymx = fmax( fmax(it->y1, it->y2), it->y3 );
		zmi = fmin( fmin(it->z1, it->z2), it->z3 );
		zmx = fmax( fmax(it->z1, it->z2), it->z3 );
		//https://github.com/lohedges/aabbcc
		//Here index is a key that is used to create a map between particles and nodes in the AABB tree.
		//The key should be unique to the particle and can take any value between 0 and std::numeric_limits<unsigned int>::max() - 1
		kauri->insertParticle( triid, trpl(xmi, ymi, zmi), trpl(xmx, ymx, zmx) );
		triid++;
	}

	//define a grid with respect to window
	//MK::grid is defined symmetric about center of the window
	cout << "Window is " << window << "\n";

	cuboidgrid3d w = cuboidgrid3d();
	w.ve = window;
	w.ve.scale();

	w.binwidths = p3d( dx, dy, dz );
	w.gridcells = p6i(
			static_cast<int>(floor((w.ve.xmi - window.center().x) / w.binwidths.x)),
			static_cast<int>(ceil((w.ve.xmx - window.center().x) / w.binwidths.x)),
			static_cast<int>(floor((w.ve.ymi - window.center().y) / w.binwidths.y)),
			static_cast<int>(ceil((w.ve.ymx - window.center().y) / w.binwidths.y)),
			static_cast<int>(floor((w.ve.zmi - window.center().z) / w.binwidths.z)),
			static_cast<int>(ceil((w.ve.zmx - window.center().z) / w.binwidths.z))     );

	cout << "The suggested sampling point grid is " << w.gridcells << "\n";
	cout << "The chosen ve is " << w.ve << "\n";
	cout << "The chosen binwidths are " << w.binwidths << "\n";

	if ( w.ngridcells() >= static_cast<size_t>(UINT32MX-1) ) {
		cerr << "The proposed grid will have too many points to be handled with the current implementation!" << "\n"; return;
	}
	unsigned int GlobalMatPointID = 0; //MK::global, i.e. the same state for every process
	for( int z = w.gridcells.zmi; z <= w.gridcells.zmx; ++z) {
		for( int y = w.gridcells.ymi; y <= w.gridcells.ymx; ++y) {
			for( int x = w.gridcells.xmi; x <= w.gridcells.xmx; ++x) {
				//define the center coordinate of the cubic voxel of the grid
				p3d p = w.where( x, y, z);

				//is this coordinate an acceptable one?
				if ( p.x < (F32MX-EPSILON) && p.y < (F32MX-EPSILON) && p.z < (F32MX-EPSILON) ) {
					if ( p.x >= w.ve.xmi && p.x <= w.ve.xmx && p.y >= w.ve.ymi && p.y <= w.ve.ymx && p.z >= w.ve.zmi && p.z <= w.ve.zmx ) {
						//##MK::here handle relative location of ROI center to specimen and possibly discard points, aka boundary correction

						//apply spatial constraints, p should be in an inside voxel
						p3dm1 test = p3dm1( p.x, p.y, p.z, 0 );
						size_t xyz = bx.where( test );
						if ( xyz != -1 ) {
							//check if this is a position in the interior
							if ( inside[xyz] == true ) {
								//consider further if sufficiently distant from all triangles

								//before requerying step one:: prune most triangles of the surface by utilizing bounded volume hierarchy (BVH) Rtree
								hedgesAABB e_aabb( trpl(p.x-R, p.y-R, p.z-R), trpl(p.x+R, p.y+R, p.z+R) );
								vector<unsigned int> mycand;
								mycand = kauri->query( e_aabb );

								bool protruding_hull = false;
								if ( mycand.size() == 0 ) { //likely most are in the interior
									protruding_hull = false;
								}
								else { //mycand.size() > 0 do we find any candidate closer than R to the triangle hull?
									p3d ptest = p3d( p.x, p.y, p.z );
									apt_real RSQR = SQR(R);
									for( auto tt = mycand.begin(); tt != mycand.end(); tt++ ) {
										apt_real CurrentDistance = closestPointOnTriangle( trihull.at(*tt), ptest );
										if ( CurrentDistance > RSQR ) {
											continue;
										}
										else {
											protruding_hull = true;
											break;
										}
									} //test next triangle whether it is close enough to ROI
								}
								if ( protruding_hull == false ) { //accept ROI with center in inside voxel and sufficient distance to the triangle hull
									matpoints.push_back( mp3d( p.x, p.y, p.z, R, GlobalMatPointID, 0 ) ); //MK::now different inspection radii are possible
									GlobalMatPointID++;
//cout << matpoints.back() << "\n";
								}
							} //done on the inside
						} //done with position check relative to voxel grid
						else {
							cerr << "bx.where( test ) returning -1 is unexpected!" << "\n";
						}
					}
				}
			} //next x grid point
		} //next y grid point
	} //next z grid point

	delete kauri; kauri = NULL;

	cout << "An ensemble of material points for mining was defined " << matpoints.size() << "\n";
}


void volsampler::define_regular_grid( const apt_real dx, const apt_real dy, const apt_real dz, const apt_real R )
{
	//define a grid with respect to window
	//MK::grid is defined symmetric about center of the window
	cout << "Window is " << window << "\n";

	cuboidgrid3d w = cuboidgrid3d();
	w.ve = window;
	w.ve.scale();

	w.binwidths = p3d( dx, dy, dz );
	w.gridcells = p6i(
			static_cast<int>(floor((w.ve.xmi - window.center().x) / w.binwidths.x)),
			static_cast<int>(ceil((w.ve.xmx - window.center().x) / w.binwidths.x)),
			static_cast<int>(floor((w.ve.ymi - window.center().y) / w.binwidths.y)),
			static_cast<int>(ceil((w.ve.ymx - window.center().y) / w.binwidths.y)),
			static_cast<int>(floor((w.ve.zmi - window.center().z) / w.binwidths.z)),
			static_cast<int>(ceil((w.ve.zmx - window.center().z) / w.binwidths.z))     );

	cout << "The suggested sampling point grid is " << w.gridcells << "\n";
	cout << "The chosen ve is " << w.ve << "\n";
	cout << "The chosen binwidths are " << w.binwidths << "\n";

	if ( w.ngridcells() >= static_cast<size_t>(UINT32MX-1) ) {
		cerr << "The proposed grid will have too many points to be handled with the current implementation!" << "\n"; return;
	}
	unsigned int GlobalMatPointID = 0; //MK::global, i.e. the same state for every process
	for( int z = w.gridcells.zmi; z <= w.gridcells.zmx; ++z) {
		for( int y = w.gridcells.ymi; y <= w.gridcells.ymx; ++y) {
			for( int x = w.gridcells.xmi; x <= w.gridcells.xmx; ++x) {
				//define the center coordinate of the cubic voxel of the grid
				p3d p = w.where( x, y, z);

				//is this coordinate an acceptable one?
				if ( p.x < (F32MX-EPSILON) && p.y < (F32MX-EPSILON) && p.z < (F32MX-EPSILON) ) {
					if ( p.x >= w.ve.xmi && p.x <= w.ve.xmx && p.y >= w.ve.ymi && p.y <= w.ve.ymx && p.z >= w.ve.zmi && p.z <= w.ve.zmx ) {
						//##MK::here handle relative location of ROI center to specimen and possibly discard points, aka boundary correction
						matpoints.push_back( mp3d( p.x, p.y, p.z, R, GlobalMatPointID, 0 ) ); //MK::now different inspection radii are possible
						GlobalMatPointID++;
//cout << matpoints.back() << "\n";
					}
				}
			} //next x grid point
		}
	}
	cout << "An ensemble of material points for mining was defined " << matpoints.size() << "\n";
}


void volsampler::define_single_point( p3d const & p, const apt_real R )
{
	cout << "Window is " << window << "\n";
	
	unsigned int GlobalMatPointID = 0;
	if ( p.x < (F32MX-EPSILON) && p.y < (F32MX-EPSILON) && p.z < (F32MX-EPSILON) ) {
		if ( p.x >= window.xmi && p.x <= window.xmx && p.y >= window.ymi && p.y <= window.ymx && p.z >= window.zmi && p.z <= window.zmx ) {
			//##MK::here handle relative location of ROI center to specimen and possibly discard points, aka boundary correction
			matpoints.push_back( mp3d( p.x, p.y, p.z, R, GlobalMatPointID, 0 ) ); //MK::now different inspection radii are possible
		}
	}
	cout << "An ensemble of material points for mining was defined " << matpoints.size() << "\n";
}


void volsampler::define_random_cloud_fixed( const unsigned int nrois, const apt_real R,
		sqb & bx, const bool* inside, vector<tri3d> const & trihull )
{
	//build an AABBtree for the triangles
	Tree* kauri = NULL;
	try {
		kauri = new class Tree( trihull.size() );
	}
	catch (bad_alloc &croak) {
		cerr << "Unable to allocate memory for the tree of the triangle BVH!" << "\n"; return;
	}

	apt_real xmi = F32MX;
	apt_real xmx = F32MI;
	apt_real ymi = F32MX;
	apt_real ymx = F32MI;
	apt_real zmi = F32MX;
	apt_real zmx = F32MI;
	unsigned int triid = 0;
	for( auto it = trihull.begin(); it != trihull.end(); it++ ) {
		xmi = fmin( fmin(it->x1, it->x2), it->x3 );
		xmx = fmax( fmax(it->x1, it->x2), it->x3 );
		ymi = fmin( fmin(it->y1, it->y2), it->y3 );
		ymx = fmax( fmax(it->y1, it->y2), it->y3 );
		zmi = fmin( fmin(it->z1, it->z2), it->z3 );
		zmx = fmax( fmax(it->z1, it->z2), it->z3 );
		//https://github.com/lohedges/aabbcc
		//Here index is a key that is used to create a map between particles and nodes in the AABB tree.
		//The key should be unique to the particle and can take any value between 0 and std::numeric_limits<unsigned int>::max() - 1
		kauri->insertParticle( triid, trpl(xmi, ymi, zmi), trpl(xmx, ymx, zmx) );
		triid++;
	}

	//get random number generator
	mt19937 dice;
	dice.seed( ConfigShared::RndDescrStatsPRNGSeed ); //deterministic seed that is for all processes the same
	//##MK::needs to be otherwise matpoint grids would diverge across the MPI process team!

	uniform_real_distribution<apt_real> unifrnd(0.f,1.f);
	for (unsigned int i = 0; i < ConfigShared::RndDescrStatsPRNGDiscard; ++i) { //warm up MersenneTwister
		apt_real discard = unifrnd(dice);
	}

	//define how many points to sample inside
	//apt_real NumberOfPoints = 1.f;
	//if ( Settings::SamplingGridType == E_AABB_RANDOM_POINTS_DENSITY )
	//	NumberOfPoints = owner->binner->metadata_geometry.volume_inside / ( (4.f*MYPI/3.f)*CUBE(Settings::SamplingGridPointDensity) );
	//else
	//	NumberOfPoints = Settings::SamplingGridPointSoMany;
	//size_t Np = (static_cast<size_t>(NumberOfPoints) >= 1) ? static_cast<size_t>(NumberOfPoints) : 1;
	//size_t Nplaced = 0;
    //size_t iter = 0;

	window.scale();
	for (unsigned int GlobalMatPointID = 0; GlobalMatPointID < nrois;    ) { //sample points until desired number of such interior points without triangle contact is found

		p3d p = p3d( 	window.xmi + unifrnd(dice) * window.xsz,
						window.ymi + unifrnd(dice) * window.ysz,
						window.zmi + unifrnd(dice) * window.zsz    );

		//apply spatial constraints, p should be in an inside voxel
		p3dm1 test = p3dm1( p.x, p.y, p.z, 0 );
		size_t xyz = bx.where( test );
//cout << "-->" << p.x << ";" << p.y << ";" << p.z << "\n";
		if ( xyz != -1 ) {
			//check if this is a position in the interior
			if ( inside[xyz] == true ) {
//cout << "-->inside" << "\n";
				//consider further if sufficiently distant from all triangles

				//before requerying step one:: prune most triangles of the surface by utilizing bounded volume hierarchy (BVH) Rtree
				hedgesAABB e_aabb( trpl(p.x-R, p.y-R, p.z-R), trpl(p.x+R, p.y+R, p.z+R) );
				vector<unsigned int> mycand;
				mycand = kauri->query( e_aabb );

//cout << "mycand.size() " << mycand.size() << "\n";

				bool protruding_hull = false;
				if ( mycand.size() == 0 ) { //likely most are in the interior
					protruding_hull = false;
				}
				else { //mycand.size() > 0 do we find any candidate closer than R to the triangle hull?
					p3d ptest = p3d( p.x, p.y, p.z );
					apt_real RSQR = SQR(R);
					for( auto tt = mycand.begin(); tt != mycand.end(); tt++ ) {
						apt_real CurrentDistance = closestPointOnTriangle( trihull.at(*tt), ptest );
						if ( CurrentDistance > RSQR ) {
							continue;
						}
						else {
							protruding_hull = true;
							break;
						}
					} //test next triangle whether it is close enough to ROI
				}
				if ( protruding_hull == false ) { //accept ROI with center in inside voxel and sufficient distance to the triangle hull
//cout << "protruding_hull == false accepting point" << "\n";
					matpoints.push_back( mp3d( p.x, p.y, p.z, R, GlobalMatPointID, 0 ) ); //MK::now different inspection radii are possible
					GlobalMatPointID++;
//cout << matpoints.back() << "\n";
				}
			} //done on the inside
		} //done with position check relative to voxel grid
	}
	cout << "An ensemble of material points for mining was defined " << matpoints.size() << "\n";

	delete kauri; kauri = NULL;
}


void volsampler::define_random_cloud_fixed( const unsigned int nrois, const apt_real R )
{
	//get random number generator
	mt19937 dice;
	dice.seed( ConfigShared::RndDescrStatsPRNGSeed ); //deterministic seed that is for all processes the same
	//##MK::needs to be otherwise matpoint grids would diverge across the MPI process team!

	uniform_real_distribution<apt_real> unifrnd(0.f,1.f);
	for (unsigned int i = 0; i < ConfigShared::RndDescrStatsPRNGDiscard; ++i) { //warm up MersenneTwister
		apt_real discard = unifrnd(dice);
	}

	//define how many points to sample inside
	//apt_real NumberOfPoints = 1.f;
	//if ( Settings::SamplingGridType == E_AABB_RANDOM_POINTS_DENSITY )
	//	NumberOfPoints = owner->binner->metadata_geometry.volume_inside / ( (4.f*MYPI/3.f)*CUBE(Settings::SamplingGridPointDensity) );
	//else
	//	NumberOfPoints = Settings::SamplingGridPointSoMany;
	//size_t Np = (static_cast<size_t>(NumberOfPoints) >= 1) ? static_cast<size_t>(NumberOfPoints) : 1;
	//size_t Nplaced = 0;
    //size_t iter = 0;

	window.scale();
	for (unsigned int GlobalMatPointID = 0; GlobalMatPointID < nrois; GlobalMatPointID++ ) { //sample points until desired number of such interior points without triangle contact is found

		p3d p = p3d( 	window.xmi + unifrnd(dice) * window.xsz,
						window.ymi + unifrnd(dice) * window.ysz,
						window.zmi + unifrnd(dice) * window.zsz    );
		matpoints.push_back( mp3d( p.x, p.y, p.z, R, GlobalMatPointID, 0 ) );
	}
	cout << "An ensemble of material points for mining was defined " << matpoints.size() << "\n";
}


void volsampler::define_locally_refined_grid( vector<p3d> const & barycenter, const apt_real R,
		const apt_real old_binwidth_i, const unsigned int new_subdiv_i,
		sqb & bx, const bool* inside, vector<tri3d> const & trihull )
{

	//define how many points to sample inside
	//apt_real NumberOfPoints = 1.f;
	//if ( Settings::SamplingGridType == E_AABB_RANDOM_POINTS_DENSITY )
	//	NumberOfPoints = owner->binner->metadata_geometry.volume_inside / ( (4.f*MYPI/3.f)*CUBE(Settings::SamplingGridPointDensity) );
	//else
	//	NumberOfPoints = Settings::SamplingGridPointSoMany;
	//size_t Np = (static_cast<size_t>(NumberOfPoints) >= 1) ? static_cast<size_t>(NumberOfPoints) : 1;
	//size_t Nplaced = 0;
    //size_t iter = 0;

	window.scale();

	//##MK::currently set an extended Moore-type kernel spreading out from center
	int ni = static_cast<int>(new_subdiv_i);
	apt_real di = 0.5*old_binwidth_i / static_cast<apt_real>(ni);

	unsigned int GlobalMatPointID = 0;
	for ( auto it = barycenter.begin(); it != barycenter.end(); it++ ) { //sample points until desired number of such interior points without triangle contact is found
		//place new ROIs at extended kernel locations around *it
		for( int z = -1*ni; z <= +1*ni; z++ ) {
			for( int y = -1*ni; y <= +1*ni; y++ ) {
				for ( int x = -1*ni; x <= +1*ni; x++ ) {

					p3d p = p3d( it->x + static_cast<apt_real>(x) * di,
								 it->y + static_cast<apt_real>(y) * di,
								 it->z + static_cast<apt_real>(z) * di );

					matpoints.push_back( mp3d( p.x, p.y, p.z, R, GlobalMatPointID, 0 ) );
					if ( GlobalMatPointID < UINT32MX ) {
						GlobalMatPointID++; //ROIs will be renamed!
						continue;
					}
					else {
						cerr << "Attempting to place more ROIs than is currently supported !" << "\n";
						matpoints = vector<mp3d>();
						return;
					}
				} //next x
			} //next y
		} //next z
	} //next user-defined ROI

	cout << "An ensemble of material points for mining was defined " << matpoints.size() << "\n";
}
