//##MK::GPLV3

#include "PARAPROBE_Verbose.h"

/*
bool helloworld( int pargc, char** pargv )
{
	cout << "Starting up PARAPROBE v" << VERSION_MAJOR << "." << VERSION_MINOR << "." << VERSION_REVISION;
	if ( VERSION_BETASTAGE == 1 )
		cout << " beta stage" << "\n";
	else
		cout << "\n";

	cout << "ERRORs are terminating in every case..." << "\n";
	if ( pargc < 2 ) {
		std::cout << "\t\tERROR::We need at least a simulation id <unsigned int> and an XML control file <*.xml> before doing something useful!" << endl;
		return false;
	}
	else {
		return true;
	}
}
*/


//genering global functions to report state, warnings and erros
void reporting( const int rank, const string what )
{
	cout << "VERBOSE::" << rank << " " << what << endl;
}

void reporting( const string what )
{
	cout << "VERBOSE::" << what << endl;
}


void complaining( const int rank, const string what )
{
	cout << "WARNING::" << rank << " " << what << endl;
}

void complaining( const string what )
{
	cout << "WARNING::" << what << endl;
}


void stopping( const int rank, const string what )
{
	cerr << "ERROR::" << rank << " " << what << endl;
}

void stopping( const string what )
{
	cerr << "ERROR::" << what << endl;
}


ostream& operator<<(ostream& in, pparm const & val)
{
	in << "Keyword " << val.keyword << "\n";
	in << "Value " << val.value << "\n";
	in << "Unit " << val.unit << "\n";
	in << "Comment " << val.info << "\n";
	return in;
}

