//##MK::GPLV3

#include "PARAPROBE_Crystallography.h"

ostream& operator<<(ostream& in, motifunit const & val)
{
	in << "u1 = " << val.u1 << ", u2 = " << val.u2 << ", u3 = " << val.u3 << ", Z = " << (int) val.elementZ << ", charge = " << (int) val.chargestate << "\n";
	return in;
}


ostream& operator<<(ostream& in, crystalstructure const & val)
{
	in << "Crystal structure candidate" << "\n";
	in << "a = " << val.a << "\n";
	in << "b = " << val.b << "\n";
	in << "c = " << val.c << "\n";
	in << "alpha = " << RADIANT2DEGREE(val.alpha) << "\n";
	in << "beta = " << RADIANT2DEGREE(val.beta) << "\n";
	in << "gamma = " << RADIANT2DEGREE(val.gamma) << "\n";
	in << "mtrx = ( " << val.mtrx.a11 << ", " << val.mtrx.a12 << ", " << val.mtrx.a13 << ", " << "\n";
	in << "         " << val.mtrx.a21 << ", " << val.mtrx.a22 << ", " << val.mtrx.a23 << ", " << "\n";
	in << "         " << val.mtrx.a31 << ", " << val.mtrx.a32 << ", " << val.mtrx.a33 << " ) " << "\n";
	in << "a1 = " << val.a1.x << ";" << val.a1.y << ";" << val.a1.z << "\n";
	in << "a2 = " << val.a2.x << ";" << val.a2.y << ";" << val.a2.z << "\n";
	in << "a3 = " << val.a3.x << ";" << val.a3.y << ";" << val.a3.z << "\n";
	in << "spacegrp = " << val.spacegroup << "\n";
	return in;
}
