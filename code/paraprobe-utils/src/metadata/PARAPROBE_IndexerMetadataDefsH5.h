//##MK::CODE

//specify here which specific results this tool writes into H5 files

#ifndef __PARAPROBE_INDEXER_METADATA_DEFS_H__
#define __PARAPROBE_INDEXER_METADATA_DEFS_H__

#define PARAPROBE_IDXR								"/OriAndPhaseIndexing"

#define PARAPROBE_IDXR_META							"/OriAndPhaseIndexing/Metadata"
#define PARAPROBE_IDXR_META_HRDWR					"/OriAndPhaseIndexing/Metadata/ToolEnvironment"
#define PARAPROBE_IDXR_META_SFTWR					"/OriAndPhaseIndexing/Metadata/ToolConfiguration"


//S2 grid of rotations used for template matching, the same for all phases and material points!
#define PARAPROBE_IDXR_META_ORI						"/OriAndPhaseIndexing/Metadata/S2Grid"
#define PARAPROBE_IDXR_META_ORI_QUAT				"/OriAndPhaseIndexing/Metadata/S2Grid/Quat"				//4x float
#define PARAPROBE_IDXR_META_ORI_QUAT_NCMAX			4
#define PARAPROBE_IDXR_META_ORI_CLOSETBL			"/OriAndPhaseIndexing/Metadata/S2Grid/CloseCand"		//start, end 2x size_t
#define PARAPROBE_IDXR_META_ORI_CLOSETBL_NCMAX		2
#define PARAPROBE_IDXR_META_ORI_CLOSEIDS			"/OriAndPhaseIndexing/Metadata/S2Grid/CloseIDs"			//unsigned int
#define PARAPROBE_IDXR_META_ORI_CLOSEIDS_NCMAX		1
#define PARAPROBE_IDXR_META_ORI_CLOSEDIS			"/OriAndPhaseIndexing/Metadata/S2Grid/CloseDisori"		//float
#define PARAPROBE_IDXR_META_ORI_CLOSEDIS_NCMAX		1

#define PARAPRIBE_IDXR_META_ORI_GRTCRCL				"/OriAndPhaseIndexing/Metadata/S2Grid/MaxGreatCircleAngle"	//float for given rotation and all directions/vertices of S2 grid, what is the maximum great circle distance of the point with the worst vertex overlap
#define PARAPRIBE_IDXR_META_ORI_GRTCRCL_NCMAX		1


#define PARAPROBE_IDXR_RES							"/OriAndPhaseIndexing/Results"
#define PARAPROBE_IDXR_RES_SOL						"/OriAndPhaseIndexing/Results/Solutions"				//2x float
#define PARAPROBE_IDXR_RES_SOL_NCMAX				2
#define PARAPROBE_IDXR_RES_DIS						"/OriAndPhaseIndexing/Results/Disori2Ref"				//4x float
#define PARAPROBE_IDXR_RES_DIS_NCMAX				4
//#####MK

/*
#define PARAPROBE_IDXR_META_DIR						"/OriPhaseIndexing/Metadata/S2Grid"
#define PARAPROBE_IDXR_META_DIR_ELAZ				"/OriPhaseIndexing/Metadata/S2Grid/ElevAzim"					//2x float
#define PARAPROBE_IDXR_META_DIR_ELAZ_NCMAX			2
#define PARAPROBE_IDXR_META_DIR_XYZ					"/OriPhaseIndexing/Metadata/S2Grid/XYZ"							//3x float
#define PARAPROBE_IDXR_META_DIR_XYZ_NCMAX			3
#define PARAPROBE_IDXR_META_DIR_TOPO				"/OriPhaseIndexing/Metadata/S2Grid/Topo"						//1x unsigned int
#define PARAPROBE_IDXR_META_DIR_TOPO_NCMAX			1

#define PARAPROBE_IDXR_META_ORI						"/OriPhaseIndexing/Metadata/S2GridRotations"
#define PARAPROBE_IDXR_META_ORI_QUAT				"/OriPhaseIndexing/Metadata/S2GridRotations/Quat"				//4x float
#define PARAPROBE_IDXR_META_ORI_QUAT_NCMAX			4
#define PARAPROBE_IDXR_META_ORI_BESTVAR				"/OriPhaseIndexing/Metadata/S2GridRotations/CloseQuatIDs"		//for each row the IDs of the n closest quaternions in fundamental zone
#define PARAPROBE_IDXR_META_ORI_BESTDEV				"/OriPhaseIndexing/Metadata/S2GridRotations/CloseQuatDisori"	//for each row the IDs of the n closest quaternions in fundamental zone

//##MK:::??????
#define PARAPROBE_IDXR_META_ORI_ANGDIST				"/OriPhaseIndexing/Metadata/S2GridRotations/MaxAngDeviation"	//for each row, i.e. S2rot 
//for each rotation great circle distance between rotated point and reference

//the templates S2 intensity images per phase used for indexing
//##MK::for every phase and group, so far we use the same set of test rotations of the reference S2Grid for testing all phases

#define PARAPROBE_IDXR_META_TMPL					"/OriPhaseIndexing/Metadata/Templates"
//+per group e.g. "/OriPhaseIndexing/Metadata/Templates/Phase0/UsePeak
#define PARAPROBE_IDXR_META_TMPL_USED				"UsePeak"				//bool, same nrows as PARAPROBE_IDXR_META_DIR_ELAZ tells S2pnts used				
#define PARAPROBE_IDXR_META_TMPL_VAL				"Intensity"				//floats

#define PARAPROBE_IDXR_META_MP_XYZ					"/OriPhaseIndexing/Metadata/MatPoints/XYZ"					//3x float
#define PARAPROBE_IDXR_META_MP_XYZ_NCMAX			3
#define PARAPROBE_IDXR_META_MP_TOPO					"/OriPhaseIndexing/Metadata/MatPoints/Topo"					//1x unsigned int
#define PARAPROBE_IDXR_META_MP_TOPO_NCMAX			1
#define PARAPROBE_IDXR_META_MP_ID					"/OriPhaseIndexing/Metadata/MatPoints/IDs"					//1x unsigned int
#define PARAPROBE_IDXR_META_MP_ID_NCMAX				1
#define PARAPROBE_IDXR_META_MP_R					"/OriPhaseIndexing/Metadata/MatPoints/Radius"				//1x float
#define PARAPROBE_IDXR_META_MP_R_NCMAX				1

#define PARAPROBE_IDXR_RES							"/OriPhaseIndexing/Results"
#define PARAPROBE_IDXR_RES_SOL						"/OriPhaseIndexing/Results/Solutions"
//+per group e.g. "/OriPhaseIndexing/Results/Solutions/Phase0/BestOris"
#define PARAPROBE_IDXR_RES_SOL_BESTORI				"BestOris"
#define PARAPROBE_IDXR_RES_SOL_BESTDEV				"BestDisori"
*/

#endif

