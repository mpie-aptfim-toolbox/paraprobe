//##MK::GPLV3

//specify here which specific results this tool writes into H5 files

#ifndef __PARAPROBE_NANOCHEM_METADATA_DEFS_H__
#define __PARAPROBE_NANOCHEM_METADATA_DEFS_H__

#define PARAPROBE_NANOCHEM								"/Nanochemistry"
#define PARAPROBE_NANOCHEM_META							"/Nanochemistry/Metadata"
#define PARAPROBE_NANOCHEM_META_HRDWR					"/Nanochemistry/Metadata/ToolEnvironment"
#define PARAPROBE_NANOCHEM_META_SFTWR					"/Nanochemistry/Metadata/ToolConfiguration"

#define PARAPROBE_NANOCHEM_META_WDW						"/Nanochemistry/Metadata/AnalysisWindow"
#define PARAPROBE_NANOCHEM_META_ISO						"/Nanochemistry/Metadata/IsoSurfaces"
#define PARAPROBE_NANOCHEM_META_CONC					"/Nanochemistry/Metadata/ConcProfiles"


#define PARAPROBE_NANOCHEM_RES							"/Nanochemistry/Results"
#define PARAPROBE_NANOCHEM_RES_ISO						"/Nanochemistry/Results/IsoSurfaces"
#define PARAPROBE_NANOCHEM_RES_CONC						"/Nanochemistry/Results/ConcProfiles"


//##MK::only for debugging purposes
#define PARAPROBE_NANOCHEM_META_DEBUG_MC				"/Nanochemistry/Metadata/MarchingCubesDebugging"
#define PARAPROBE_NANOCHEM_META_DEBUG_MC_XYZ			"/Nanochemistry/Metadata/MarchingCubesDebugging/ScalarFieldXYZ"
#define PARAPROBE_NANOCHEM_META_DEBUG_MC_XYZ_NCMAX		3 //3x float
#define PARAPROBE_NANOCHEM_META_DEBUG_MC_TOPO			"/Nanochemistry/Metadata/MarchingCubesDebugging/ScalarFieldTopology"
#define PARAPROBE_NANOCHEM_META_DEBUG_MC_TOPO_NCMAX		1 //1x unsigned int
#define PARAPROBE_NANOCHEM_META_DEBUG_MC_VAL			"/Nanochemistry/Metadata/MarchingCubesDebugging/ScalarFieldValue"
#define PARAPROBE_NANOCHEM_META_DEBUG_MC_VAL_NCMAX		1 //1x float

#define PARAPROBE_NANOCHEM_META_DEBUG_TSCL				"/Nanochemistry/Metadata/TriangleSoupClusteringDebugging"
#define PARAPROBE_NANOCHEM_META_DEBUG_TSCL_XYZ			"/Nanochemistry/Metadata/TriangleSoupClusteringDebugging/TrianglesXYZ"
#define PARAPROBE_NANOCHEM_META_DEBUG_TSCL_XYZ_NCMAX	9 //9x float, vertex v1, v2, v3
#define PARAPROBE_NANOCHEM_META_DEBUG_TSCL_TOPO			"/Nanochemistry/Metadata/TriangleSoupClusteringDebugging/TrianglesTopology"
#define PARAPROBE_NANOCHEM_META_DEBUG_TSCL_TOPO_NCMAX	1 //1x unsigned int
#define PARAPROBE_NANOCHEM_META_DEBUG_TSCL_CLID			"/Nanochemistry/Metadata/TriangleSoupClusteringDebugging/TrianglesClusterID"
#define PARAPROBE_NANOCHEM_META_DEBUG_TSCL_CLID_NCMAX	1 //1x unsigned int

#define PARAPROBE_NANOCHEM_META_DEBUG_CGAL				"/Nanochemistry/Metadata/PolyhedronConstructorDebugging"
//one group per (closed oriented 2-manifold) polyhedron found wit these datasets
#define PARAPROBE_NANOCHEM_META_DEBUG_CGAL_XYZ			"/Nanochemistry/Metadata/PolyhedronConstructorDebugging/TrianglesXYZ"
#define PARAPROBE_NANOCHEM_META_DEBUG_CGAL_XYZ_NCMAX	3 //3x float
#define PARAPROBE_NANOCHEM_META_DEBUG_CGAL_TOPO			"/Nanochemistry/Metadata/PolyhedronConstructorDebugging/TrianglesTopo"
#define PARAPROBE_NANOCHEM_META_DEBUG_CGAL_TOPO_NCMAX	1 //1x unsigned int
#define PARAPROBE_NANOCHEM_META_DEBUG_CGAL_OUNRM		"/Nanochemistry/Metadata/PolyhedronConstructorDebugging/TrianglesOuterUnitNormal"
#define PARAPROBE_NANOCHEM_META_DEBUG_CGAL_OUNRM_NCMAX	3 //3x float


#endif
