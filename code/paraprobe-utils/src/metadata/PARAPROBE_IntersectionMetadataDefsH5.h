//##MK::CODE

//specify here which specific results this tool writes into H5 files

#ifndef __PARAPROBE_ISECT_METADATA_DEFS_H__
#define __PARAPROBE_ISECT_METADATA_DEFS_H__

#define PARAPROBE_ISECT								"/OriAndPhaseIndexing"

#define PARAPROBE_ISECT_META						"/OriAndPhaseIndexing/Metadata"

//S2 grid of rotations used for template matching, the same for all phases and material points!
#define PARAPROBE_ISECT_META_ORI					"/OriAndPhaseIndexing/Metadata/S2Grid"

//visualization of tetrahedra decomposition
#define PARAPROBE_ISECT_META_TETS					"/OriAndPhaseIndexing/Metadata/TetrahedralizedPolyhedra"
//for each dataset, one dataset per polyhedron
#define PARAPROBE_ISECT_META_TETS_TOPO				"TetrahedraTopo"
#define PARAPROBE_ISECT_META_TETS_TOPO_NCMAX		4
#define PARAPROBE_ISECT_META_TETS_TIDS				"TetrahedraIDs"	//which tetraeder?
#define PARAPROBE_ISECT_META_TETS_TIDS_NCMAX		1
#define PARAPROBE_ISECT_META_TETS_PIDS				"PolyhedraIDs" //which associated polyhedron
#define PARAPROBE_ISECT_META_TETS_PIDS_NCMAX		1
#define PARAPROBE_ISECT_META_TETS_XYZ				"TetrahedraXYZ"
#define PARAPROBE_ISECT_META_TETS_XYZ_NCMAX			3

//SPHERES_POLYHEDRA_INTERSECTIONS
#define PARAPROBE_ISECT_RES							"/OriAndPhaseIndexing/Results"
#define PARAPROBE_ISECT_RES_DIS						"/OriAndPhaseIndexing/Results/Disori2Ref"				//2x float
#define PARAPROBE_ISECT_RES_DIS_NCMAX				5 // 1+MAX_INTERSECTING_POLYHEDRA

//COMPARE_WITH_TKD_RESULTS
#define PARAPROBE_ISECT_RES_TKD_NCMAX				9 // 1+MAX_TKD_TESTS


#define PARAPROBE_ISECT_RES_OVER					"/OriAndPhaseIndexing/Results/PolyhedraIntersections" 	//roi, roi vol, four strongest 4*2x float
#define PARAPROBE_ISECT_RES_OVER_NCMAX				10




#endif

