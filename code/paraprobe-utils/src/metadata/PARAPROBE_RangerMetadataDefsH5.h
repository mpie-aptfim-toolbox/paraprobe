//##MK::CODE

//specify here which specific results this tool writes into H5 files

#ifndef __PARAPROBE_RANGER_METADATA_DEFS_H__
#define __PARAPROBE_RANGER_METADATA_DEFS_H__

#define PARAPROBE_RANGER								"/Ranging"

/*
#define PARAPROBE_RANGER_HRDWR							"/Ranging/HardwareDetails"
#define PARAPROBE_RANGER_HRDWR_META						"/Ranging/HardwareDetails/Metadata"
#define PARAPROBE_RANGER_HRDWR_META_KEYS				"/Ranging/HardwareDetails/Metadata/Keywords"

#define PARAPROBE_RANGER_SFTWR							"/Ranging/SoftwareDetails"
#define PARAPROBE_RANGER_SFTWR_META						"/Ranging/SoftwareDetails/Metadata"
#define PARAPROBE_RANGER_SFTWR_META_KEYS				"/Ranging/SoftwareDetails/Metadata/Keywords"
 */

#define PARAPROBE_RANGER_META							"/Ranging/Metadata"
#define PARAPROBE_RANGER_META_HRDWR						"/Ranging/Metadata/ToolEnvironment"
#define PARAPROBE_RANGER_META_SFTWR						"/Ranging/Metadata/ToolConfiguration"

#define PARAPROBE_RANGER_META_TYPID_DICT				"/Ranging/Metadata/Iontypes"					
#define PARAPROBE_RANGER_META_TYPID_DICT_ID				"/Ranging/Metadata/Iontypes/IDs"					//1x unsigned char, [0,255] which iontype
#define PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX		1
#define PARAPROBE_RANGER_META_TYPID_DICT_WHAT			"/Ranging/Metadata/Iontypes/What"					//evapion3, i.e. 8x unsigned char
#define PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX		8
#define PARAPROBE_RANGER_META_TYPID_DICT_IVAL			"/Ranging/Metadata/Iontypes/MQIVal"					//2x float
#define PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX		2
#define PARAPROBE_RANGER_META_TYPID_DICT_ASSN			"/Ranging/Metadata/Iontypes/MQ2IDs"					//1x unsigned char
//to which Iontypes/IDs do the range belong, each type can have multiple ranges!
#define PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX		1

#define PARAPROBE_RANGER_META_PEAKFIND					"/Ranging/Metadata/PossibleMolecularIons"
//for every peak search mqival one group
#define PARAPROBE_RANGER_META_PEAKFIND_IVAL				"MQIval"											//2x float
#define PARAPROBE_RANGER_META_PEAKFIND_IVAL_NCMAX		2


#define PARAPROBE_RANGER_RES							"/Ranging/Results"
#define PARAPROBE_RANGER_RES_TYPID						"/Ranging/Results/IontypeIDs"						//unsigned char
#define PARAPROBE_RANGER_RES_TYPID_NCMAX				1

#define PARAPROBE_RANGER_RES_PEAKFIND					"/Ranging/Results/PossibleMolecularIons"
//for every peak search mqival one group
#define PARAPROBE_RANGER_RES_PEAKFIND_WHAT				"What"		//Maximum number of nuclids allowed x 2 x uchar (for pairs of Nprotons and Nneutrons decoded from the uint16 hashes) + 1x uchar for the charge
//#define PARAPROBE_RANGER_RES_PEAKFIND_WHAT_NCMAX		//this is adaptive to safe diskspace
#define PARAPROBE_RANGER_RES_PEAKFIND_MQ				"MassToCharge"										//1x float
#define PARAPROBE_RANGER_RES_PEAKFIND_MQ_NCMAX			1
#define PARAPROBE_RANGER_RES_PEAKFIND_ABUN				"AbundanceScore"									//1x double
#define PARAPROBE_RANGER_RES_PEAKFIND_ABUN_NCMAX		1



#endif
