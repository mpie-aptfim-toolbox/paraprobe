//##MK::GPLV3

#ifndef __PARAPROBE_TRANSCODER_METADATA_DEFS_H__
#define __PARAPROBE_TRANSCODER_METADATA_DEFS_H__

#define PARAPROBE_TRANSCODE_DETSPACE					"/DetectorSpace"
#define PARAPROBE_TRANSCODE_DETSPACE_META				"/DetectorSpace/Metadata"
#define PARAPROBE_TRANSCODE_DETSPACE_META_HRDWR			"/DetectorSpace/Metadata/ToolEnvironment"
#define PARAPROBE_TRANSCODE_DETSPACE_META_SFTWR			"/DetectorSpace/Metadata/ToolConfiguration"

#define PARAPROBE_TRANSCODE_DETSPACE_RES				"/DetectorSpace/Results"
#define PARAPROBE_TRANSCODE_DETSPACE_RES_XY				"/DetectorSpace/Results/XY"		//2x float
#define PARAPROBE_TRANSCODE_DETSPACE_RES_XY_NCMAX		2


#define PARAPROBE_TRANSCODE_VOLTCURVES					"/VoltageCurves"
#define PARAPROBE_TRANSCODE_VOLTCURVES_META				"/VoltageCurves/Metadata"
#define PARAPROBE_TRANSCODE_VOLTCURVES_META_HRDWR		"/VoltageCurves/Metadata/ToolEnvironment"
#define PARAPROBE_TRANSCODE_VOLTCURVES_META_SFTWR		"/VoltageCurves/Metadata/ToolConfiguration"

#define PARAPROBE_TRANSCODE_VOLTCURVES_RES				"/VoltageCurves/Results"
#define PARAPROBE_TRANSCODE_VOLTCURVES_RES_VDC			"/VoltageCurves/Results/VDC"	//##MK:: "standing voltage" //1x float
#define PARAPROBE_TRANSCODE_VOLTCURVES_RES_VDC_NCMAX	1
#define PARAPROBE_TRANSCODE_VOLTCURVES_RES_VPU			"/VoltageCurves/Results/VPU"	//##MK:: "pulse voltage" //1x float
#define PARAPROBE_TRANSCODE_VOLTCURVES_RES_VPU_NCMAX	1


#endif

