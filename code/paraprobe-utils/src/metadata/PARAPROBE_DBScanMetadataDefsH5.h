//##MK::GPLV3

#ifndef __PARAPROBE_DBSCAN_METADATA_DEFS_H__
#define __PARAPROBE_DBSCAN_METADATA_DEFS_H__

#define PARAPROBE_DBSC								"/DBScan"

//one result per ion group and eps, minPts
//string dsnm = PARAPROBE_DBSC_META + to_string(CaseID) + PARAPROBE_DBSC_META_TYPID --> "/DBScan/Metadata/Task1/IontypeIDs"

#define PARAPROBE_DBSC_META							"/DBScan/Metadata"
#define PARAPROBE_DBSC_META_HRDWR					"/DBScan/Metadata/ToolEnvironment"
#define PARAPROBE_DBSC_META_SFTWR					"/DBScan/Metadata/ToolConfiguration"

//+per case
//masks, all or region-of-interest
#define PARAPROBE_DBSC_META_THRS					"Ion2EdgeDistThreshold" //1x float
#define PARAPROBE_DBSC_META_THRS_NCMAX				1
#define PARAPROBE_DBSC_META_EPS						"Epsilon" //1x float
#define PARAPROBE_DBSC_META_EPS_NCMAX				1
#define PARAPROBE_DBSC_META_KTH						"KthNearestNeighbor" //1x unsigned int
#define PARAPROBE_DBSC_META_KTH_NCMAX				1
#define PARAPROBE_DBSC_META_MINPTS					"MinNumberOfIonsForSignificantCluster" //1x uint32
#define PARAPROBE_DBSC_META_MINPTS_NCMAX			1

#define PARAPROBE_DBSC_META_TYPID_TG				"IontypeTargets"				//evapion3 as many rows as combinations, zero-th row is always central ion
#define PARAPROBE_DBSC_META_TYPID_TG_NCMAX			9
#define PARAPROBE_DBSC_META_NIONS					"NumberOfTargetIons"		//1x unsigned int
#define PARAPROBE_DBSC_META_NIONS_NCMAX				1
#define PARAPROBE_DBSC_META_RANK					"Rank"	//1x unsigned int
#define PARAPROBE_DBSC_META_RANK_NCMAX				1
#define PARAPROBE_DBSC_META_PROF					"ProfilingPreRunPost" //3x double
#define PARAPROBE_DBSC_META_PROF_NCMAX				1

#define PARAPROBE_DBSC_RES							"/DBScan/Results"
//+per case
#define PARAPROBE_DBSC_RES_LBL						"ClusterID" //1x long, (0 if noise, >positive if eps connected, <negative if core point
#define PARAPROBE_DBSC_RES_LBL_NCMAX				1
#define PARAPROBE_DBSC_RES_XYZ						"XDMFxyz" //3x float
#define PARAPROBE_DBSC_RES_XYZ_NCMAX				3
#define PARAPROBE_DBSC_RES_TOPO						"XDMFTopology" //1x unsigned int
#define PARAPROBE_DBSC_RES_TOPO_NCMAX				1
#define PARAPROBE_DBSC_RES_IID						"IonID" //1x unsigned int
#define PARAPROBE_DBSC_RES_IID_NCMAX				1

#define PARAPROBE_DBSC_RES_PREC_LBL					"ClusterID" //1x long
#define PARAPROBE_DBSC_RES_PREC_LBL_NCMAX			1
#define PARAPROBE_DBSC_RES_PREC_NIONS				"NumberOfIons" //1x unsigned int
#define PARAPROBE_DBSC_RES_PREC_NIONS_NCMAX			1
#define PARAPROBE_DBSC_RES_PREC_XYZ					"Barycenter" //3x float
#define PARAPROBE_DBSC_RES_PREC_XYZ_NCMAX			3
#define PARAPROBE_DBSC_RES_PREC_BND					"BoundaryContact" //1x bool
#define PARAPROBE_DBSC_RES_PREC_BND_NCMAX			1


#define PARAPROBE_DBSC_RES_CDFRAW					"ECDFEdgeCorrNo" //3x double, no edge correction
#define PARAPROBE_DBSC_RES_CDFRAW_NCMAX				2
#define PARAPROBE_DBSC_RES_CDFCLN					"ECDFEdgeCorrYes" //3x double, with edge correction
#define PARAPROBE_DBSC_RES_CDFCLN_NCMAX				2

/*
#define PARAPROBE_MSMA								"/MaximumSeparationMethod"

//one result per ion group and eps, minPts
//string dsnm = PARAPROBE_MSMA_META + to_string(CaseID) + PARAPROBE_MSMA_META_TYPID --> "/MaximumSeparationMethod/Metadata/Task1/IontypeIDs"
#define PARAPROBE_MSMA_META							"/MaximumSeparationMethod/Metadata"  
//+per case
//masks, all or region-of-interest
#define PARAPROBE_MSMA_META_EPS						"dmax" //1x float
#define PARAPROBE_MSMA_META_EPS_NCMAX				1
#define PARAPROBE_MSMA_META_KTH						"kth" //1x unsigned int
#define PARAPROBE_MSMA_META_KTH_NCMAX				1
//minimum number of points to be significant
#define PARAPROBE_MSMA_META_MINPTS					"minPts" //1x uint32
#define PARAPROBE_MSMA_META_MINPTS_NCMAX			1
//##MK::erosion distance
#define PARAPROBE_MSMA_META_TYPID_TG				"IontypeTargets"				//evapion3 as many rows as combinations, zero-th row is always central ion
#define PARAPROBE_MSMA_META_TYPID_TG_NCMAX			9

#define PARAPROBE_MSMA_RES							"/MaximumSeparationMethod/Results"
//+per case
#define PARAPROBE_MSMA_RES_LBLS						"ClusterID" //1x long, (0 if noise, >positive if eps connected, <negative if core point
#define PARAPROBE_MSMA_RES_LBLS_NCMAX				1
*/

#endif

