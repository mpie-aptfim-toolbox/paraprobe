//##MK::GPLV3

//specify here which specific results this tool writes into H5 files

#ifndef __PARAPROBE_RECONSTRUCT_METADATA_DEFS_H__
#define __PARAPROBE_RECONSTRUCT_METADATA_DEFS_H__

#define PARAPROBE_RECONSTRUCT_DETSPC					"/DetectorSpace"

#define PARAPROBE_RECONSTRUCT_DETSPC_META				"/DetectorSpace/Metadata"
#define PARAPROBE_RECONSTRUCT_DETSPC_META_HRDWR			"/DetectorSpace/Metadata/ToolEnvironment"
#define PARAPROBE_RECONSTRUCT_DETSPC_META_SFTWR			"/DetectorSpace/Metadata/ToolConfiguration"

#define PARAPROBE_RECONSTRUCT_DETSPC_META_GRD			"/DetectorSpace/Metadata/Grid"
#define PARAPROBE_RECONSTRUCT_DETSPC_META_BNDS			"/DetectorSpace/Metadata/Bounds"
#define PARAPROBE_RECONSTRUCT_DETSPC_META_RES			"/DetectorSpace/Metadata/Resolution"
#define PARAPROBE_RECONSTRUCT_DETSPC_META_HMAPSIFO		"/DetectorSpace/Metadata/HitMapInfo"

#define PARAPROBE_RECONSTRUCT_DETSPC_RES				"/DetectorSpace/Results"
#define PARAPROBE_RECONSTRUCT_DETSPC_RES_HITMAPS		"/DetectorSpace/Results/DetectorHitMaps"

//one sub-group for each parameter run

#define PARAPROBE_RECONSTRUCT_RECSPC					"/ReconstructionSpace"
#define PARAPROBE_RECONSTRUCT_RECSPC_META				"/ReconstructionSpace/Metadata"
#define PARAPROBE_RECONSTRUCT_RECSPC_META_HRDWR			"/ReconstructionSpace/Metadata/ToolEnvironment"
#define PARAPROBE_RECONSTRUCT_RECSPC_META_SFTWR			"/ReconstructionSpace/Metadata/ToolConfiguration"
//one sub-group for each reconstruction realization


#endif


