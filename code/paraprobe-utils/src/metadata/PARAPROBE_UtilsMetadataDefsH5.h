//##MK::GPLV3

#ifndef __PARAPROBE_UTILS_METADATA_DEFS_H__
#define __PARAPROBE_UTILS_METADATA_DEFS_H__

#define PARAPROBE_UTILS_HRDWR								"/HardwareDetails"
#define PARAPROBE_UTILS_HRDWR_META							"/HardwareDetails/Metadata"
#define PARAPROBE_UTILS_HRDWR_META_KEYS						"/HardwareDetails/Metadata/Keywords"


#define PARAPROBE_UTILS_SFTWR								"/SoftwareDetails"
#define PARAPROBE_UTILS_SFTWR_META							"/SoftwareDetails/Metadata"
#define PARAPROBE_UTILS_SFTWR_META_KEYS						"/SoftwareDetails/Metadata/Keywords"


#endif

