//##MK::CODE

//specify here which specific results this tool writes into H5 files

#ifndef __PARAPROBE_ARAULLO_METADATA_DEFS_H__
#define __PARAPROBE_ARAULLO_METADATA_DEFS_H__

#define PARAPROBE_ARAULLO								"/AraulloPeters"

#define PARAPROBE_ARAULLO_META							"/AraulloPeters/Metadata"
#define PARAPROBE_ARAULLO_META_HRDWR					"/AraulloPeters/Metadata/ToolEnvironment"
#define PARAPROBE_ARAULLO_META_SFTWR					"/AraulloPeters/Metadata/ToolConfiguration"

#define PARAPROBE_ARAULLO_META_MP						"/AraulloPeters/Metadata/MatPoints"

#define PARAPROBE_ARAULLO_META_MP_XYZ					"/AraulloPeters/Metadata/MatPoints/XYZ"			//3x float
#define PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX				3
#define PARAPROBE_ARAULLO_META_MP_TOPO					"/AraulloPeters/Metadata/MatPoints/Topo"		//1x unsigned int
#define PARAPROBE_ARAULLO_META_MP_TOPO_NCMAX			1
#define PARAPROBE_ARAULLO_META_MP_ID					"/AraulloPeters/Metadata/MatPoints/IDs"			//1x unsigned int
#define PARAPROBE_ARAULLO_META_MP_ID_NCMAX				1
#define PARAPROBE_ARAULLO_META_MP_R						"/AraulloPeters/Metadata/MatPoints/Radius"		//1x float
#define PARAPROBE_ARAULLO_META_MP_R_NCMAX				1

#define PARAPROBE_ARAULLO_META_PHCAND					"/AraulloPeters/Metadata/PhaseCandidates"
#define PARAPROBE_ARAULLO_META_PHCAND_N					"/AraulloPeters/Metadata/PhaseCandidates/NumberOfCandidates"	//1x unsigned int
#define PARAPROBE_ARAULLO_META_PHCAND_N_NCMAX			1
//+per candidate
#define PARAPROBE_ARAULLO_META_PHCAND_ID				"IontypesID"									//1x unsigned char, [0,255] which iontype
#define PARAPROBE_ARAULLO_META_PHCAND_ID_NCMAX			1
#define PARAPROBE_ARAULLO_META_PHCAND_WHAT				"IontypesWhat"									//evapion3, i.e. 8x unsigned char
#define PARAPROBE_ARAULLO_META_PHCAND_WHAT_NCMAX		8
#define PARAPROBE_ARAULLO_META_PHCAND_RSP				"RealspacePosition"								//3x float predefined or min,max range
#define PARAPROBE_ARAULLO_META_PHCAND_RSP_NCMAX			3
#define PARAPROBE_ARAULLO_META_PHCAND_VAL				"RefIntensity"									//1x float
#define PARAPROBE_ARAULLO_META_PHCAND_VAL_NCMAX			1


#define PARAPROBE_ARAULLO_META_DIR						"/AraulloPeters/Metadata/Directions"
#define PARAPROBE_ARAULLO_META_DIR_ELAZ					"/AraulloPeters/Metadata/Directions/ElevAzim"	//2x float
#define PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX			2
#define PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ				"/AraulloPeters/Metadata/Directions/XYZ"		//3x float
#define PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX		3
#define PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO			"/AraulloPeters/Metadata/Directions/Topo"		//1x unsigned int
#define PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO_NCMAX		1

#define PARAPROBE_ARAULLO_META_KEYQUANT					"/AraulloPeters/Metadata/KeyQuantsForMatPoints"
#define PARAPROBE_ARAULLO_META_KEYQUANT_QNT				"/AraulloPeters/Metadata/KeyQuantsForMatPoints/UserDefQuantiles" //as many floats as KeyQuants desired by the user
#define PARAPROBE_ARAULLO_META_KEYQUANT_QNT_NCMAX		1
#define PARAPROBE_ARAULLO_META_MP_NIONS					"/AraulloPeters/Metadata/NumberOfIonsForMatPoints" //one unsigned int per mat point/ROI
#define PARAPROBE_ARAULLO_META_MP_NIONS_NCMAX			1

#define PARAPROBE_ARAULLO_RES							"/AraulloPeters/Results"
#define PARAPROBE_ARAULLO_RES_MP_IOID					"/AraulloPeters/Results/MatPointIDsWithResults"	//1x int
#define PARAPROBE_ARAULLO_RES_MP_IOID_NCMAX				1
//#define PARAPROBE_ARAULLO_RES_SPECMETA					"/AraulloPeters/Results/SpecificMeta"
#define PARAPROBE_ARAULLO_RES_DBSCAN					"/AraulloPeters/Results/SpecificPeakDBScan" //9x floats x,y,z max, x,y,z mean, max int, sum int, pks cnts
#define PARAPROBE_ARAULLO_RES_DBSCAN_NCMAX				9
#define PARAPROBE_ARAULLO_RES_SPECPEAK					"/AraulloPeters/Results/SpecificPeak"
#define PARAPROBE_ARAULLO_RES_SPECPEAK_NCMAX			1
#define PARAPROBE_ARAULLO_RES_THREEPEAK					"/AraulloPeters/Results/ThreeStrongest"
#define PARAPROBE_ARAULLO_RES_THREEPEAK_NCMAX			6
#define PARAPROBE_ARAULLO_RES_KEYQUANT					"/AraulloPeters/Results/KeyQuantsForMatPoints"
//one dataset for each quantile, so "/AraulloPeters/Results/KeyQuantsForMatPoints/Quantile0, .../Quantile1, ... which quantiles these are tells PARAPROBE_ARAULLO_META_KEYQUANT_QNT
#define PARAPROBE_ARAULLO_RES_KEYQUANT_NCMAX			1 //for each ROI a table with as many rows as quantiles and a column for the quants, a column for the intensity, a column for the elevation and one column for the azimuth
//thereby we can answer the following question: for the signature of a ROI computed for a particular specific amplitude bin, how intense are the peaks, how weak is the background in the spherical image, and where are the peaks located?
//MK::alternatively one could run a DBScan on the spherical image to identify the location of peaks

#define PARAPROBE_ARAULLO_RES_HISTO_CNTS				"/AraulloPeters/Results/Histogramm"
#define PARAPROBE_ARAULLO_RES_HISTO_MAGN				"/AraulloPeters/Results/FFTMagnitude"


#endif
