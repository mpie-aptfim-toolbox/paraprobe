//##MK::GPLV3

#ifndef __PARAPROBE_TESSELLATOR_METADATA_DEFS_H__
#define __PARAPROBE_TESSELLATOR_METADATA_DEFS_H__

#define PARAPROBE_TESS								"/VoroTess"
#define PARAPROBE_TESS_META							"/VoroTess/Metadata"
#define PARAPROBE_TESS_META_HRDWR					"/VoroTess/Metadata/ToolEnvironment"
#define PARAPROBE_TESS_META_SFTWR					"/VoroTess/Metadata/ToolConfiguration"

#define PARAPROBE_TESS_META_DERO					"/VoroTess/Metadata/CellErosionDistance" //1x double
#define PARAPROBE_TESS_META_DERO_NCMAX				1
#define PARAPROBE_TESS_META_PROCESSID				"/VoroTess/Metadata/ProcessID"			//1x int32
#define PARAPROBE_TESS_META_PROCESSID_NCMAX			1
#define PARAPROBE_TESS_META_THREADID				"/VoroTess/Metadata/ThreadID"			//1x int32
#define PARAPROBE_TESS_META_THREADID_NCMAX			1
#define PARAPROBE_TESS_META_WORKPART				"/VoroTess/Metadata/Workpartitioning"	//multiple size_t
#define PARAPROBE_TESS_META_WORKPART_NCMAX			1 //##MK::#### multiple size_t


#define PARAPROBE_TESS_RES							"/VoroTess/Results"
#define PARAPROBE_TESS_RES_ID						"/VoroTess/Results/IonIDCellID"			//1x uint32
#define PARAPROBE_TESS_RES_ID_NCMAX					1
#define PARAPROBE_TESS_RES_VOL						"/VoroTess/Results/CellVolume"			//1x double
#define PARAPROBE_TESS_RES_VOL_NCMAX				1


#endif
