//##MK::GPLV3

#include "PARAPROBE_OriMath.h"

squat::squat( const apt_real _q0, const apt_real _q1, const apt_real _q2, const apt_real _q3 )
{
	if ( _q0 < 0.f ) { //reverse sign for consistence
		this->q0 = -_q0;
		this->q1 = -_q1;
		this->q2 = -_q2;
		this->q3 = -_q3;
	}
	else {
		this->q0 = _q0;
		this->q1 = _q1;
		this->q2 = _q2;
		this->q3 = _q3;
	}
}


squat::squat( const apt_real X0, const apt_real X1, const apt_real X2 )
{
	//MK: Quaternion algebra: q = -q define an equivalent rotation. for unit quaternions ||q|| = 1 so the inverse q^-1 = q*/||q||^2 simplifies to = q* with q* the conjugated q0 - (q1,q2,q3)
	//K. Shoemake, Graphic Gems III (editor D. Kirk) CalTech pp124-134, mind quaternion order of Shoemake w  + i*v + j*x + k*z <=> 0 +  1 2 3 with
	apt_real r1 = sqrt(1-X0);
	apt_real r2 = sqrt(X0);
	apt_real theta1 = 2.f * MYPI * X1;
	apt_real theta2 = 2.f * MYPI * X2;

	this->q0 = r1*sin(theta1);
	this->q1 = r1*cos(theta1);
	this->q2 = r2*sin(theta2);
	this->q3 = r2*cos(theta2); //w,v,x,z

	if ( this->q0 < 0.f ) { //reverse sign to Northern hemisphere
		this->q0 = -this->q0;
		this->q1 = -this->q1;
		this->q2 = -this->q2;
		this->q3 = -this->q3;
	}

	//this->normalize();
}


void squat::normalize()
{
	apt_real len = sqrt(SQR(this->q0)+SQR(this->q1)+SQR(this->q2)+SQR(this->q3));
	this->q0 /= len;
	this->q1 /= len;
	this->q2 /= len;
	this->q3 /= len;
}


squat squat::invert()
{
	//for unit quaternion norm = 1.0 so
	return conjugate();
}


squat squat::conjugate()
{
	return squat( this->q0, -this->q1, -this->q2, -this->q3 );
}


squat squat::multiply_quaternion( squat const & q)
{
	//multiply me being p with input squat q ie pq = p*q
	//##MK::possibly a sign change in the vector part Pijk? #######
    return squat( 	this->q0*q.q0 - this->q1*q.q1 - this->q2*q.q2 - this->q3*q.q3,
    		 		this->q0*q.q1 + this->q1*q.q0 + this->q2*q.q3 - this->q3*q.q2,
    				this->q0*q.q2 - this->q1*q.q3 + this->q2*q.q0 + this->q3*q.q1,
					this->q0*q.q3 + this->q1*q.q2 - this->q2*q.q1 + this->q3*q.q0   );  //DAMASK::ok same as in Consistent rotations tutorial, 2015 and Grimmer, 1974
}


p3d squat::Lqr( d3d const & r )
{
	apt_real qbar = SQR(this->q0) - (SQR(this->q1)+SQR(this->q2)+SQR(this->q3));
	apt_real qdot = 2.f*(this->q1*r.u + this->q2*r.v + this->q3*r.w);
	apt_real qpre = 2.f*this->q0;
	
	return p3d(		qbar*r.u + qdot*this->q1 - qpre*(this->q2*r.w - this->q3*r.v),
					qbar*r.v + qdot*this->q2 - qpre*(this->q3*r.u - this->q1*r.w),
					qbar*r.w + qdot*this->q3 - qpre*(this->q1*r.v - this->q2*r.u)    );
}


apt_real squat::disorientation_angle_fcc( squat & q )
{
	//this being p, input squat being q, solving for pq^-1
	//##MK::check MTex again
	//see mentioned Grimmer, 1974 Acta Cryst A reference in declaration for details
	//passive orientation quaternions

	//squat pme = squat( this->q0, this->q1, this->q2, this->q3 );
	squat qconj = q.conjugate(); //q^-1

	squat qcand = multiply_quaternion( qconj ); //misorientation between me, ie this and q using the
	//active definition of a misorientation (##MK::passive?) convention pq^-1

	//fcc fundamental quaternions
	apt_real qcandsymm[24];

	//taking absolute values as we are only interested in disorientation angle not the axis
	qcandsymm[ 0] = fabs(qcand.q0); //Grimmer 7a
	qcandsymm[ 1] = fabs(qcand.q1);
	qcandsymm[ 2] = fabs(qcand.q2);
	qcandsymm[ 3] = fabs(qcand.q3);

	apt_real _sqrt2 = 1.0 / sqrt(static_cast<apt_real>(2.0));
	qcandsymm[ 4] = fabs(_sqrt2*(qcand.q0 + qcand.q1)); //Grimmer 7b
	qcandsymm[ 5] = fabs(_sqrt2*(qcand.q0 - qcand.q1));
	qcandsymm[ 6] = fabs(_sqrt2*(qcand.q2 + qcand.q3));
	qcandsymm[ 7] = fabs(_sqrt2*(qcand.q2 - qcand.q3));

	qcandsymm[ 8] = fabs(_sqrt2*(qcand.q0 + qcand.q2)); //Grimmer 7c
	qcandsymm[ 9] = fabs(_sqrt2*(qcand.q0 - qcand.q2));
	qcandsymm[10] = fabs(_sqrt2*(qcand.q1 + qcand.q3));
	qcandsymm[11] = fabs(_sqrt2*(qcand.q1 - qcand.q3));

	qcandsymm[12] = fabs(_sqrt2*(qcand.q0 + qcand.q3)); //Grimmer 7d
	qcandsymm[13] = fabs(_sqrt2*(qcand.q0 - qcand.q3));
	qcandsymm[14] = fabs(_sqrt2*(qcand.q1 + qcand.q2));
	qcandsymm[15] = fabs(_sqrt2*(qcand.q1 - qcand.q2));

	apt_real half = static_cast<apt_real>(0.5);

	qcandsymm[16] = fabs(half*(qcand.q0 + qcand.q1 + qcand.q2 + qcand.q3)); //Grimmer 7e
	qcandsymm[17] = fabs(half*(qcand.q0 + qcand.q1 - qcand.q2 - qcand.q3));
	qcandsymm[18] = fabs(half*(qcand.q0 - qcand.q1 + qcand.q2 - qcand.q3));
	qcandsymm[19] = fabs(half*(qcand.q0 - qcand.q1 - qcand.q2 + qcand.q3));

	qcandsymm[20] = fabs(half*(qcand.q0 + qcand.q1 + qcand.q2 - qcand.q3)); //Grimmer 7f
	qcandsymm[21] = fabs(half*(qcand.q0 + qcand.q1 - qcand.q2 + qcand.q3));
	qcandsymm[22] = fabs(half*(qcand.q0 - qcand.q1 + qcand.q2 + qcand.q3));
	qcandsymm[23] = fabs(half*(qcand.q0 - qcand.q1 - qcand.q2 - qcand.q3));

	apt_real maximum = 0.f;
	for (unsigned int i = 0; i < 24; ++i) {
		if ( qcandsymm[i] >= maximum) //##MK::potentially case < maximum hit more likely, then better if/else construct to profit from more successful branch prediction
			maximum = qcandsymm[i];
	}

	if ( maximum < (1.f - EPSILON) ) { //p and q are not the same
		return (2.0*acos(maximum)); //angle in radiant
		//cout << "Nan occurring in disori " << maximum << "-->" << p[0] << ";" << p[1] << ";" << p[2] << ";" << p[3] << endl;
		//cout << "Nan occurring in disori " << maximum << "-->" << qconj[0] << ";" << qconj[1] << ";" << qconj[2] << ";" << qconj[3] << endl;
		//cout << "Nan occurring in disori qcand component " << qcand[0] << ";" << qcand[1] << ";" << qcand[2] << ";" << qcand[3] << endl;
	}
	//##MK::p and q likely the same, implicit else
	return EPSILON;
}


t3x3 squat::qu2om()
{
	apt_real qbar = (this->q0*this->q0) -
			((this->q1*this->q1) + (this->q2*this->q2) + (this->q3*this->q3));

	t3x3 res = t3x3(
			qbar + (2.f*this->q1*this->q1),
			2.f*((this->q1*this->q2) - (P_IJK*this->q0*this->q3)),
			2.f*((this->q1*this->q3) + (P_IJK*this->q0*this->q2)),

			2.f*((this->q1*this->q2) + (P_IJK*this->q0*this->q3)),
			qbar + (2.f*this->q2*this->q2),
			2.f*((this->q2*this->q3) - (P_IJK*this->q0*this->q1)),

			2.f*((this->q1*this->q3) - (P_IJK*this->q0*this->q2)),
			2.f*((this->q2*this->q3) + (P_IJK*this->q0*this->q1)),
			qbar + (2.f*this->q3*this->q3)  ); //MK::a23 position was +P_ijk ###!

	if ( fabs(res.det() - 1.f) < 1.0e-5 ) {
		return res;
	}
	else {
		cerr << "qu2om " << this->q0 << ";" << this->q1 << ";" << this->q2 << ";" << this->q3 << " results in improper rotation matrix " << res.det() << "\n";
		return res;
	}
}


bunge squat::qu2eu()
{
	//qu2eu
	//check:: to handle negative cases: https://github.com/BlueQuartzSoftware/DREAM3D/blob/
	//	1ab713bcdc39c19f51892443936ad0b93aee984e/Source/OrientationLib/Core/OrientationTransformation.hpp
	apt_real q03 = SQR(this->q0) + SQR(this->q3); //>= 0.0
	apt_real q12 = SQR(this->q1) + SQR(this->q2); //>= 0.0
	apt_real chi = sqrt(q03*q12); //>= 0.0
	//gimbal lock
	bunge res = bunge();
	if ( chi >= EPSILON ) { //&& q12 >= EPSILON && q12 >= EPSILON ) { //most likely case
		//the document definition in the paper of Rowenhorst does not match above implementation!
		res.phi1 = atan2( (this->q1*this->q3 - P_IJK*this->q0*this->q2) / chi,
				(-P_IJK*this->q0*this->q1 - this->q2*this->q3) / chi );

		//currently implemented

		res.Phi = atan2( 2.f*chi, q03 - q12 );

		res.phi2 = atan2( (P_IJK*this->q0*this->q2 + this->q1*this->q3) / chi,
				(this->q2*this->q3 - P_IJK*this->q0*this->q1) / chi );
	}
	else {
		res.phi1 = 0.f; //error values
		res.Phi = 0.f;
		res.phi2 = 0.f;

		if ( chi < EPSILON && q12 < EPSILON ) {
			res.phi1 = atan2( -2.f*P_IJK*this->q0*this->q3, (SQR(this->q0)-SQR(this->q3)) );
			res.Phi = 0.f;
			res.phi2 = 0.f;
		}
		if ( chi < EPSILON && q03 < EPSILON ) {
			res.phi1 = atan2( +2.f*this->q1*this->q2, (SQR(this->q1)-SQR(this->q2)) );
			res.Phi = MYPI;
			res.phi2 = 0.f;
		}
	}

	//according to above implementation to bring in positive octant
	if ( res.phi1 < 0.0 ) {
		res.phi1 = fmod(res.phi1 + 100.0*MYPI, 2.0*MYPI );
	}
	if ( res.Phi < 0.0 ) {
		res.Phi = fmod(res.Phi + 100.0*MYPI, MYPI );
	}
	if ( res.phi2 < 0.0 ) {
		res.phi2 = fmod(res.phi2 + 100.0*MYPI, 2.0*MYPI );
	}

	return res;
}


ostream& operator<<(ostream& in, squat const & val)
{
	in << "\n" << "\t\t" << val.q0 << ";" << val.q1 << ";" << val.q2 << ";" << val.q3 << "\n";
	return in;
}


bunge::bunge( const string parseme )
{
	this->phi1 = 0.f;
	this->Phi = 0.f;
	this->phi2 = 0.f;

	stringstream parsethis;
	string datapiece;
	parsethis << parseme;

	getline( parsethis, datapiece, ';');
	this->phi1 = DEGREE2RADIANT(stof(datapiece));
	getline( parsethis, datapiece, ';');
	this->Phi = DEGREE2RADIANT(stof(datapiece));
	getline( parsethis, datapiece, ';');
	this->phi2 = DEGREE2RADIANT(stof(datapiece));
}


squat bunge::eu2qu()
{
	apt_real sigma = 0.5*(this->phi1 + this->phi2);
	apt_real delta = 0.5*(this->phi1 - this->phi2);
	apt_real c = cos(this->Phi/2.f);
	apt_real s = sin(this->Phi/2.f);

	apt_real test_northernhemi_q0 = c * cos(sigma);

	if ( test_northernhemi_q0 < 0.0 ) { //reverse sign of entire quaternion to bring to Northern hemisphere
		return squat( c*cos(sigma), +P_IJK*s*cos(delta), +P_IJK*s*sin(delta), +P_IJK*c*sin(sigma) );
	}
	else {
		return squat( +c*cos(sigma), -P_IJK*s*cos(delta), -P_IJK*s*sin(delta), -P_IJK*c*sin(sigma) );
	}
}


t3x3 bunge::eu2om()
{
    apt_real c1 = cos(this->phi1);
    apt_real s1 = sin(this->phi1);
    apt_real c  = cos(this->Phi);
    apt_real s  = sin(this->Phi);
    apt_real c2 = cos(this->phi2);
    apt_real s2 = sin(this->phi2);
    
    return t3x3(    +c1*c2 - s1*c*s2,       +s1*c2 + c1*c*s2,      +s*s2,
                    -c1*s2 - s1*c*c2,       -s1*s2 + c1*c*c2,      +s*c2,
                    +s1*s,                  -c1*s,                 +c  );
}
    

ostream& operator<<(ostream& in, bunge const & val)
{
	in << "\n" << "\t\t" << val.phi1 << ";" << val.Phi << ";" << val.phi2 << "\n";
	return in;
}


apt_real orimatrix::det()
{
	apt_real row1 = +1.f*this->a11 * ((this->a22*this->a33) - (this->a32*this->a23));
	apt_real row2 = +1.f*this->a21 * ((this->a12*this->a33) - (this->a32*this->a13));
	apt_real row3 = +1.f*this->a31 * ((this->a12*this->a23) - (this->a22*this->a13));
	return row1 - row2 + row3;
}


orimatrix orimatrix::inv()
{
	apt_real nrm = 1.f / this->det();
	apt_real a11 = nrm * (this->a22*this->a33 - this->a23*this->a32);
	apt_real a12 = nrm * (this->a13*this->a32 - this->a12*this->a33);
	apt_real a13 = nrm * (this->a12*this->a23 - this->a13*this->a22);

	apt_real a21 = nrm * (this->a23*this->a31 - this->a21*this->a33);
	apt_real a22 = nrm * (this->a11*this->a33 - this->a13*this->a31);
	apt_real a23 = nrm * (this->a13*this->a21 - this->a11*this->a23);

	apt_real a31 = nrm * (this->a21*this->a32 - this->a22*this->a31);
	apt_real a32 = nrm * (this->a12*this->a31 - this->a11*this->a32);
	apt_real a33 = nrm * (this->a11*this->a22 - this->a12*this->a21);

	return orimatrix( 	a11, a12, a13,
						a21, a22, a23,
						a31, a32, a33   );
}


squat orimatrix::om2qu()
{
	apt_real q0 =         0.5 * sqrt(1.f + this->a11 + this->a22 + this->a33);
	apt_real q1 = 0.5 * P_IJK * sqrt(1.f + this->a11 - this->a22 - this->a33);
	apt_real q2 = 0.5 * P_IJK * sqrt(1.f - this->a11 + this->a22 - this->a33);
	apt_real q3 = 0.5 * P_IJK * sqrt(1.f - this->a11 - this->a22 + this->a33);

	if ( this->a32 < this->a23 ) {
		q1 *= -1.0;
	}
	if ( this->a13 < this->a31 ) {
		q2 *= -1.0;
	}
	if ( this->a21 < this->a12 ) {
		q3 *= -1.0;
	}

	apt_real nrm = 1.f / sqrt( SQR(q0)+SQR(q1)+SQR(q2)+SQR(q3) );
	q0 *= nrm;
	q1 *= nrm;
	q2 *= nrm;
	q3 *= nrm;

	return squat( q0, q1, q2, q3 );

	return squat(); //identity
}


ostream& operator << (ostream& in, orimatrix const & val) {
	in << val.a11 << ";" << val.a12 << ";" << val.a13 << "\n";
	in << val.a21 << ";" << val.a22 << ";" << val.a23 << "\n";
	in << val.a31 << ";" << val.a32 << ";" << val.a33 << "\n";
	return in;
}


squat multiply_quaternion( squat const & p, squat const & q)
{
	//pq = p*q
    return squat( 	p.q0*q.q0 - p.q1*q.q1 - p.q2*q.q2 - p.q3*q.q3,
    		 		p.q0*q.q1 + p.q1*q.q0 + p.q2*q.q3 - p.q3*q.q2,
    				p.q0*q.q2 - p.q1*q.q3 + p.q2*q.q0 + p.q3*q.q1,
					p.q0*q.q3 + p.q1*q.q2 - p.q2*q.q1 + p.q3*q.q0   );  //DAMASK::ok same as in Consistent rotations tutorial, 2015 and Grimmer, 1974
}


apt_real disorientation_angle_fcc_grimmer( squat const & qcand )
{
	//fcc fundamental quaternions
	apt_real qcandsymm[24];

	//taking absolute values as we are only interested in disorientation angle not the axis
	qcandsymm[ 0] = fabs(qcand.q0); //Grimmer 7a
	qcandsymm[ 1] = fabs(qcand.q1);
	qcandsymm[ 2] = fabs(qcand.q2);
	qcandsymm[ 3] = fabs(qcand.q3);

	apt_real _sqrt2 = 1.0 / sqrt(static_cast<apt_real>(2.0));
	qcandsymm[ 4] = fabs(_sqrt2*(qcand.q0 + qcand.q1)); //Grimmer 7b
	qcandsymm[ 5] = fabs(_sqrt2*(qcand.q0 - qcand.q1));
	qcandsymm[ 6] = fabs(_sqrt2*(qcand.q2 + qcand.q3));
	qcandsymm[ 7] = fabs(_sqrt2*(qcand.q2 - qcand.q3));

	qcandsymm[ 8] = fabs(_sqrt2*(qcand.q0 + qcand.q2)); //Grimmer 7c
	qcandsymm[ 9] = fabs(_sqrt2*(qcand.q0 - qcand.q2));
	qcandsymm[10] = fabs(_sqrt2*(qcand.q1 + qcand.q3));
	qcandsymm[11] = fabs(_sqrt2*(qcand.q1 - qcand.q3));

	qcandsymm[12] = fabs(_sqrt2*(qcand.q0 + qcand.q3)); //Grimmer 7d
	qcandsymm[13] = fabs(_sqrt2*(qcand.q0 - qcand.q3));
	qcandsymm[14] = fabs(_sqrt2*(qcand.q1 + qcand.q2));
	qcandsymm[15] = fabs(_sqrt2*(qcand.q1 - qcand.q2));

	apt_real half = static_cast<apt_real>(0.5);

	qcandsymm[16] = fabs(half*(qcand.q0 + qcand.q1 + qcand.q2 + qcand.q3)); //Grimmer 7e
	qcandsymm[17] = fabs(half*(qcand.q0 + qcand.q1 - qcand.q2 - qcand.q3));
	qcandsymm[18] = fabs(half*(qcand.q0 - qcand.q1 + qcand.q2 - qcand.q3));
	qcandsymm[19] = fabs(half*(qcand.q0 - qcand.q1 - qcand.q2 + qcand.q3));

	qcandsymm[20] = fabs(half*(qcand.q0 + qcand.q1 + qcand.q2 - qcand.q3)); //Grimmer 7f
	qcandsymm[21] = fabs(half*(qcand.q0 + qcand.q1 - qcand.q2 + qcand.q3));
	qcandsymm[22] = fabs(half*(qcand.q0 - qcand.q1 + qcand.q2 + qcand.q3));
	qcandsymm[23] = fabs(half*(qcand.q0 - qcand.q1 - qcand.q2 - qcand.q3));

	apt_real maximum = 0.f;
	for (unsigned int i = 0; i < 24; ++i) {
		if ( qcandsymm[i] >= maximum) //##MK::potentially case < maximum hit more likely, then better if/else construct to profit from more successful branch prediction
			maximum = qcandsymm[i];
	}

	if ( maximum < (1.f - EPSILON) ) { //p and q are not the same
		return (2.0*acos(maximum)); //angle in radiant
		//cout << "Nan occurring in disori " << maximum << "-->" << p[0] << ";" << p[1] << ";" << p[2] << ";" << p[3] << endl;
		//cout << "Nan occurring in disori " << maximum << "-->" << qconj[0] << ";" << qconj[1] << ";" << qconj[2] << ";" << qconj[3] << endl;
		//cout << "Nan occurring in disori qcand component " << qcand[0] << ";" << qcand[1] << ";" << qcand[2] << ";" << qcand[3] << endl;
	}
	//##MK::p and q likely the same, implicit else
	return EPSILON;
}

void cubic_cubic_operators( vector<squat> & out )
{
	out.clear();
	out.push_back( squat( CUBIC_SYMM_L01_Q0, CUBIC_SYMM_L01_Q1, CUBIC_SYMM_L01_Q2, CUBIC_SYMM_L01_Q3 ) );

	out.push_back( squat( CUBIC_SYMM_L03_Q0, CUBIC_SYMM_L03_Q1, CUBIC_SYMM_L03_Q2, CUBIC_SYMM_L03_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L05_Q0, CUBIC_SYMM_L05_Q1, CUBIC_SYMM_L05_Q2, CUBIC_SYMM_L05_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L06_Q0, CUBIC_SYMM_L06_Q1, CUBIC_SYMM_L06_Q2, CUBIC_SYMM_L06_Q3 ) );

	out.push_back( squat( CUBIC_SYMM_L19_Q0, CUBIC_SYMM_L19_Q1, CUBIC_SYMM_L19_Q2, CUBIC_SYMM_L19_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L26_Q0, CUBIC_SYMM_L26_Q1, CUBIC_SYMM_L26_Q2, CUBIC_SYMM_L26_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L07_Q0, CUBIC_SYMM_L07_Q1, CUBIC_SYMM_L07_Q2, CUBIC_SYMM_L07_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L21_Q0, CUBIC_SYMM_L21_Q1, CUBIC_SYMM_L21_Q2, CUBIC_SYMM_L21_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L27_Q0, CUBIC_SYMM_L27_Q1, CUBIC_SYMM_L27_Q2, CUBIC_SYMM_L27_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L09_Q0, CUBIC_SYMM_L09_Q1, CUBIC_SYMM_L09_Q2, CUBIC_SYMM_L09_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L23_Q0, CUBIC_SYMM_L23_Q1, CUBIC_SYMM_L23_Q2, CUBIC_SYMM_L23_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L25_Q0, CUBIC_SYMM_L25_Q1, CUBIC_SYMM_L25_Q2, CUBIC_SYMM_L25_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L15_Q0, CUBIC_SYMM_L15_Q1, CUBIC_SYMM_L15_Q2, CUBIC_SYMM_L15_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L17_Q0, CUBIC_SYMM_L17_Q1, CUBIC_SYMM_L17_Q2, CUBIC_SYMM_L17_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L11_Q0, CUBIC_SYMM_L11_Q1, CUBIC_SYMM_L11_Q2, CUBIC_SYMM_L11_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L13_Q0, CUBIC_SYMM_L13_Q1, CUBIC_SYMM_L13_Q2, CUBIC_SYMM_L13_Q3 ) );

	out.push_back( squat( CUBIC_SYMM_L31_Q0, CUBIC_SYMM_L31_Q1, CUBIC_SYMM_L31_Q2, CUBIC_SYMM_L31_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L45_Q0, CUBIC_SYMM_L45_Q1, CUBIC_SYMM_L45_Q2, CUBIC_SYMM_L45_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L37_Q0, CUBIC_SYMM_L37_Q1, CUBIC_SYMM_L37_Q2, CUBIC_SYMM_L37_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L41_Q0, CUBIC_SYMM_L41_Q1, CUBIC_SYMM_L41_Q2, CUBIC_SYMM_L41_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L35_Q0, CUBIC_SYMM_L35_Q1, CUBIC_SYMM_L35_Q2, CUBIC_SYMM_L35_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L39_Q0, CUBIC_SYMM_L39_Q1, CUBIC_SYMM_L39_Q2, CUBIC_SYMM_L39_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L43_Q0, CUBIC_SYMM_L43_Q1, CUBIC_SYMM_L43_Q2, CUBIC_SYMM_L43_Q3 ) );
	out.push_back( squat( CUBIC_SYMM_L33_Q0, CUBIC_SYMM_L33_Q1, CUBIC_SYMM_L33_Q2, CUBIC_SYMM_L33_Q3 ) );
}



pair<bool,squat> disoriquaternion_cubic_cubic( squat const & ori_p, squat const & ori_q )
{
	//see H. Grimmer, Acta Cryst. 1974 A30, 685-688
	//see R. Bonnet, Acta Cryst. 1980 A36, 116-122
	//see A. Heinz and P. Neumann, Acta Cryst. 1991, A47, 780-789
	//see above D. Rowenhorst, MSMSE
	//see also http://pajarito.materials.cmu.edu/rollett/27750/L13-Grain_Bndries_RFspace-15Mar16.pdf slide pseudo-code

	//assume that ori_p and ori_q describe passive orientation quaternions of two grains A and B
	vector<squat> O;
	cubic_cubic_operators( O );

	//we want specific GB misorientation quaternion with minimum angle and axis in fundamental zone
	squat pp = ori_p;
	squat qq = ori_q;
	squat q1 = qq.conjugate();
	squat pq1 = pp.multiply_quaternion( q1 ); //pq^-1
	squat p1 = pp.conjugate();
	squat qp1 = qq.multiply_quaternion( p1 ); //qp^-1 because bicrystal symmetry switching symmetry applies pq1 and qp1 are misorientation quaternions not orientation quaternions!

	//get disorientation, i.e. quaternion in fundamental zone including switching symmetry!
	apt_real q0_best = 0.f;
	squat disori = squat(0.f, 0.f, 0.f, 0.f); //not a valid quaternion
	bool valid = false;

	//##MK::readable but slow because many ifs will evaluate to false
	for( auto i = O.begin(); i != O.end(); i++ ) { //24x
		squat Oi = *i;
		squat Oi_pq1 = Oi.multiply_quaternion( pq1 );
		squat Oi_qp1 = Oi.multiply_quaternion( qp1 );
		for ( auto j = O.begin(); j != O.end(); j++ ) { //24x
			squat Oj = *j;
			//##MK::squat Oi_pq1_Oj = Oj.multiply_quaternion( Oi_pq1 );
			//##MK::squat Oi_qp1_Oj = Oj.multiply_quaternion( Oi_qp1 );
			squat Oi_pq1_Oj = Oi_pq1.multiply_quaternion( Oj );
			squat Oi_qp1_Oj = Oi_qp1.multiply_quaternion( Oj );
			//2x
			//check if axis is in the FZ for cubic crystal symmetry most frequently it is not
			if ( Oi_pq1_Oj.q3 >= 0.f ) {
				if ( Oi_pq1_Oj.q1 >= Oi_pq1_Oj.q2 && Oi_pq1_Oj.q2 >= Oi_pq1_Oj.q3 ) { //Oi_pq1_Oj is in fundamental zone
					if ( Oi_pq1_Oj.q0 >= q0_best ) {
						q0_best = Oi_pq1_Oj.q0;
						disori = Oi_pq1_Oj;
						valid = true;
					}
				}
			}
			if ( Oi_qp1_Oj.q3 >= 0.f ) {
				if ( Oi_qp1_Oj.q1 >= Oi_qp1_Oj.q2 && Oi_qp1_Oj.q2 >= Oi_qp1_Oj.q3 ) { //Oi_qp1_Oj is in fundamental zone
					if ( Oi_qp1_Oj.q0 >= q0_best ) {
						q0_best = Oi_qp1_Oj.q0;
						disori = Oi_qp1_Oj;
						valid = true;
					}
				}
			}
		} //next j
	} //next i
	//need to test all hyperphobic...

	return pair<bool,squat>( valid, disori );
}


