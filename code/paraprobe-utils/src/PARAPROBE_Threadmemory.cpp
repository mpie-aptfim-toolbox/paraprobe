//##MK::GPLV3

#include "PARAPROBE_Threadmemory.h"


threadmemory::threadmemory()
{
	//owner = NULL;
	threadtree = NULL;
	//tessmii = p6d64();
	//tesshalowidth = p3d64();
	zmi = F32MX;
	zmx = F32MI;
	//melast = false;
}


threadmemory::~threadmemory()
{
	delete threadtree; threadtree = NULL;

	for( size_t i = 0; i < ityp_kdtree_org.size(); i++ ) {
		delete ityp_kdtree_org.at(i); ityp_kdtree_org.at(i) = NULL;
	}
	ityp_kdtree_org = vector<kd_tree*>();

	for( size_t i = 0; i < ityp_kdtree_rnd.size(); i++ ) {
		delete ityp_kdtree_rnd.at(i); ityp_kdtree_rnd.at(i) = NULL;
	}
	ityp_kdtree_rnd = vector<kd_tree*>();

	for( size_t i = 0; i < ionpp1_kdtree_org.size(); i++ ) {
		delete ionpp1_kdtree_org.at(i); ionpp1_kdtree_org.at(i) = NULL;
	}
	ionpp1_kdtree_org = vector<vector<p3d>*>();
	for( size_t i = 0; i < ionpp1_kdtree_rnd.size(); i++ ) {
		delete ionpp1_kdtree_rnd.at(i); ionpp1_kdtree_rnd.at(i) = NULL;
	}
	ionpp1_kdtree_rnd = vector<vector<p3d>*>();

	for( size_t i = 0; i < ionifo_kdtree_org.size(); i++ ) {
		delete ionifo_kdtree_org.at(i); ionifo_kdtree_org.at(i) = NULL;
	}
	ionifo_kdtree_rnd = vector<vector<p3difo>*>();
	for( size_t i = 0; i < ionifo_kdtree_rnd.size(); i++ ) {
		delete ionifo_kdtree_rnd.at(i); ionifo_kdtree_rnd.at(i) = NULL;
	}
	ionifo_kdtree_rnd = vector<vector<p3difo>*>();


	/*for( size_t i = 0; i < ionpp2_kdtree_org.size(); i++ ) {
		delete ionpp2_kdtree_org.at(i); ionpp2_kdtree_org.at(i) = NULL;
	}
	ionpp2_kdtree_org = vector<vector<p3d>*>();

	for( size_t i = 0; i < ionpp2_kdtree_rnd.size(); i++ ) {
		delete ionpp2_kdtree_rnd.at(i); ionpp2_kdtree_rnd.at(i) = NULL;
	}
	ionpp2_kdtree_rnd = vector<vector<p3d>*>();*/



}


void threadmemory::init_localmemory( p6d64 const & mybounds, p3d64 const & myhalo, const bool mlast )
{
	//MK::CALLED FROM WITHIN PARALLEL REGION
	//tessmii = mybounds;
	//tesshalowidth = myhalo;
	zmi = mybounds.zmi;
	zmx = mybounds.zmx;
	//melast = mlast;
}


/*
void threadmemory::init_localions( vector<p3d> const & p )
{
	//MK::CALLED FROM WITHIN PARALLEL REGION
	//picks from reconstructed volume all positions inside me into local structure

	//bool TessellationYesOrNo = (Settings::VolumeTessellation != E_NOTESS) ? true : false;
	apt_real local_zmi = zmi;
	apt_real local_zmx = zmx;

	//when we need to have a guard zone for the tessellation we have a different strategy
	//if ( TessellationYesOrNo == true ) {} //see implementation in main code

	unsigned int nions = p.size();
	for( unsigned int ionID = 0; ionID < nions; ionID++ ) {
		if ( p[ionID].z < zmi )
			continue;
		if ( p[ionID].z >= zmx )
			continue;

		//not continued ion is within or on boundary of interval [zmi, zmx)
		ionpp1.push_back( p[ionID] );
	}

	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " init_localions ionpp1.size() << " << ionpp1.size() << "\n";
	}
}
*/


void threadmemory::init_localions( vector<p3dm1> const & p )
{
	//MK::CALLED FROM WITHIN PARALLEL REGION
	//picks from reconstructed volume all positions inside me into local structure

	//bool TessellationYesOrNo = (Settings::VolumeTessellation != E_NOTESS) ? true : false;
	apt_real local_zmi = zmi;
	apt_real local_zmx = zmx;

	//when we need to have a guard zone for the tessellation we have a different strategy
	//if ( TessellationYesOrNo == true ) {} //see implementation in main code

	unsigned int nions = p.size();
	for( unsigned int ionID = 0; ionID < nions; ionID++ ) {
		if ( p[ionID].z < zmi )
			continue;
		if ( p[ionID].z >= zmx )
			continue;

		//not continued ion is within or on boundary of interval [zmi, zmx)
		ionpp3.push_back( p3dm3( p[ionID].x, p[ionID].y, p[ionID].z, ionID, VALIDZONE_ION, DEFAULTTYPE ) );
	}

	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " init_localions ionpp3.size() << " << ionpp3.size() << "\n";
	}
}


void threadmemory::init_localions_subkdtree_per_ityp( vector<p3d> const & p, vector<apt_real> const & d,
			vector<unsigned char> const & lorg, vector<unsigned char> const & lrnd )
{
	//MK::CALLED FROM WITHIN PARALLEL REGION
	//picks from reconstructed volume all positions inside me into local structure

	//bool TessellationYesOrNo = (Settings::VolumeTessellation != E_NOTESS) ? true : false;
	apt_real local_zmi = zmi;
	apt_real local_zmx = zmx;

	//when we need to have a guard zone for the tessellation we have a different strategy
	//if ( TessellationYesOrNo == true ) {} //see implementation in main code

	unsigned int nions = p.size();
	for( unsigned int ionID = 0; ionID < nions; ionID++ ) {
		if ( p.at(ionID).z < zmi )
			continue;
		if ( p[ionID].z >= zmx )
			continue;

		//not continued ion is within or on boundary of interval [zmi, zmx)
		//ionpp2.push_back( p3dm2( p[ionID].x, p[ionID].y, p[ionID].z, lorg.at(ionID), lrnd.at(ionID) ) );
		ionpp1.push_back( p3d( p.at(ionID).x, p[ionID].y, p[ionID].z ) );
		ionifo.push_back( p3difo( d.at(ionID), ionID, lorg.at(ionID), lrnd.at(ionID) ) );
	}

	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " init_localions ionpp1/ionifo size << " << ionpp1.size() << ";" << ionifo.size() << "\n";
	}
}


void threadmemory::init_localkdtree()
{
	//CALLED FROM WITHIN PARALLEL REGION

	//MK::there is a principal conflict: the input heavy point cloud data of an APT/FIM experiment are typically stored only approximately in order along increasing depth but near random in xy
	//i.e. negative z direction in effect any interval of data taken from ionpp3 probe almost certainly randomly in reconstruction space
	//in other words, the array is not in any reasonable way organized for memory efficient access
	//for all so far conducted tasks this is irrelevant as processing operations such as ranging, finding bounding boxes are essential linear time trivial array read or write throughs
	//spatial range queries, however, require an as best as possible balanced grouping of ions close to one another in recon space to remain efficient.
	//MK::hence we reorganize the storage of the ionpp3 array, such that afterwards physical close ions of the kdtree leaf end up in a linear sequence in memory
	//MK::we do not store an entire copy of the data in the KDTree during the entire runtime but a one time double buffer allocation to restructure
	//if we were then to release the original ionpp3 all previous functions would loose the implicit mapping the order ionpp3 keeps
	//##MK:: as of now: build tree, restructure and keep two copies, future improvement, build tree already in spatial partitioning phase and implement leaner via for instance vector<vector*>
	//##MK::if sufficient threads are used also unsigned int will be sufficient
	if ( threadtree != NULL ) {
		cerr << "Thread " << omp_get_thread_num() << " a local KDTree of the ions from that region exists already!" << "\n";
		return;
	}

	double tic = omp_get_wtime();

	//allocate temporaries and permutated position array
	vector<p3d> ioninput;
	vector<size_t> permutations;

	try {
		ioninput.reserve(ionpp3.size());
		permutations.reserve(ionpp3.size());
		ionpp3_kdtree.reserve(ionpp3.size());
	}
	catch (bad_alloc &mecroak) {
		cerr << "Thread " << omp_get_thread_num() << " allocation error during building threadlocal KDTree in " << omp_get_thread_num() << "\n";
		return;
	}

	for( size_t i = 0; i < ionpp3.size(); i++ ) {
		ioninput.push_back( p3d( ionpp3[i].x, ionpp3[i].y, ionpp3[i].z ) );
	}

	threadtree = new kd_tree; //this calls the default constructor

	//this really constructs the nodes of the tree, here with 256 objects per leaf
	//MK::number of objects in leafs is a compromise too few in leafs --> more branching,
	//MK::too many in leafs too much testing --> too much testing of close but not close enough candidates
	threadtree->build( ioninput, permutations, 256 );

	#pragma omp critical
	{
		//cout << "Thread-local KDTree on " << to_string(omp_get_thread_num()) << " has [" << threadtree->min.x << ";" << threadtree->min.y << ";" << threadtree->min.z << "----" << threadtree->max.x << "] with " << threadtree->nodes.size() << " nodes in total"  << endl;
		//cout << "Thread-local KDTree on " << to_string(omp_get_thread_num()) << " has [" << threadtree->min << ";" << threadtree->max.x << "] with " << threadtree->nodes.size() << " nodes in total"  << endl;
		cout << "Thread " << omp_get_thread_num() << " KDTree build with " << threadtree->nodes.size() << " nodes in total consuming at least " << threadtree->get_treememory_consumption() << "\n";
	}

	threadtree->pack_p3dm1( permutations, ionpp3, ionpp3_kdtree );

	//permutation array no longer necessary
	ioninput = vector<p3d>();
	permutations = vector<size_t>();

	//ioninput as well, both are temporaries will be freed automatically upon exiting subroutine

	if ( threadtree->verify( ionpp3_kdtree ) == false ) {
		cerr << "Threadlocal KDTree for " << omp_get_thread_num() << " has overlapping indices!" << "\n";
		return;
	}

	double toc = omp_get_wtime();

	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " KDTree build completed in " << (toc-tic) << " seconds" << "\n";
	}
}


void threadmemory::init_localkdtree_subkdtree_per_ityp( const unsigned char ityp_max )
{
	//CALLED FROM WITHIN PARALLEL REGION
	//for details on kdtrees in general see init_localkdtree

	double tic = omp_get_wtime();

	//we build now for every iontype an own KDTree, this way we get more packed in the cache line and
	/*if ( lorg.size() != lrnd.size() ) {
		#pragma omp critical
		{
			cerr << "Thread " << omp_get_thread_num() << " lorg != lrnd!" << "\n";
		}
		return;
	}
	//find maximum label value
	int ityp_min = 0;
	int ityp_max = 0;
	//at this point we may wonder why do not extract the maximum from this->ionpp2 however
	//because the function is CALLED FROM WITHIN PARALLEL REGION so this local region might not have ions of all types
	//as the types were randomized so better we scan the entire original label set
	for( auto ot = lorg.begin(); ot != lorg.end(); ot++ ) {
		int ityp_lt = (int) *ot;
		if ( ityp_lt < ityp_max ) { //most likely not hitting the maximum
			continue;
		}
		ityp_max = ityp_lt;
	}
	//should not be necessary because lrnd contains all labels of lorg but only in different ordered as we just shuffled the labels array!
	for( auto rt = lrnd.begin(); rt != lrnd.end(); rt++ ) {
		int ityp_rt = (int) *rt;
		if ( ityp_rt < ityp_max ) {
			continue;
		}
		ityp_max = ityp_rt;
	}
	*/

	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " maximum ityp_max label is " << (int) ityp_max << "\n";
	}

	//define and allocate subtrees, we need ityp_max + 1 subtrees in total
	size_t nsubtrees = static_cast<size_t>(1+ityp_max);
	if ( nsubtrees >= static_cast<size_t>(UCHARMX) ) {
		#pragma omp critical
		{
			cerr << "More than 256 trees cannot be handled by current implementation!" << "\n";
		}
		return;
	}

	try {
		ityp_kdtree_org = vector<kd_tree*>( nsubtrees, NULL );
		//ionpp2_kdtree_org = vector<vector<p3d>*>( nsubtrees, NULL );
		ionpp1_kdtree_org = vector<vector<p3d>*>( nsubtrees, NULL );
		ionifo_kdtree_org = vector<vector<p3difo>*>( nsubtrees, NULL );

		ityp_kdtree_rnd = vector<kd_tree*>( nsubtrees, NULL );
		//ionpp2_kdtree_rnd = vector<vector<p3d>*>( nsubtrees, NULL );
		ionpp1_kdtree_rnd = vector<vector<p3d>*>( nsubtrees, NULL );
		ionifo_kdtree_rnd = vector<vector<p3difo>*>( nsubtrees, NULL );
	}
	catch (bad_alloc &mecroak) {
		#pragma omp critical
		{
			cerr << "Thread " << omp_get_thread_num() << " ityp coordination hub allocation failed!" << "\n";
		}
		return;
	}


	//decompose ionpp2 dataset based on labels, firstly for original labels
	int currentlabel_org = 0;
	for( size_t tr = 0; tr < static_cast<size_t>(1+ityp_max); tr++, currentlabel_org++ ) {
		unsigned char lbl_org = static_cast<unsigned char>(currentlabel_org);
		vector<p3d> iin_org_ppp;
		vector<p3difo> iin_org_ifo;
		for( size_t i = 0; i < ionpp1.size(); i++ ) {
		//for( auto it = ionpp2.begin(); it != ionpp2.end(); it++ ) { //filter ions of sought after iontype
			//if ( it->i_org != lbl_org )
			if( ionifo.at(i).i_org != lbl_org ) {
				continue;
			}
			iin_org_ppp.push_back( ionpp1.at(i) );
			iin_org_ifo.push_back( ionifo.at(i) );
		}
		if ( iin_org_ppp.size() < 1 && iin_org_ifo.size() < 1 ) { //no ions with this label i this threadregion so respective pointers to kdtree, ionpp1, and ionifo remain NULL
			continue;
		}
		else {
			kd_tree* subtree_org = NULL;
			subtree_org = new kd_tree; //this calls the default constructor
			if ( subtree_org != NULL ) {
				//build incrementally the sub-tree for original and randomized labels for the tr-th iontype
				//copy of ions of this specific tr-th iontype lbl, allocate temporaries and permutated position array
				vector<size_t> iperm_org;
				iperm_org.reserve( iin_org_ppp.size() );

				subtree_org->build( iin_org_ppp, iperm_org, 256 );	//this really constructs the nodes of the tree, here with 256 objects per leaf

				//reorganize the points in memory such that points in the same kdtree bin are contiguous in memory
				vector<p3d>* iout_org_ppp = NULL;
				iout_org_ppp = new vector<p3d>();
				vector<p3difo>* iout_org_ifo = NULL;
				iout_org_ifo = new vector<p3difo>();
				if ( iout_org_ppp != NULL && iout_org_ifo != NULL ) {

					subtree_org->pack( iperm_org, iin_org_ppp, iin_org_ifo, *iout_org_ppp, *iout_org_ifo );

					if ( subtree_org->verify_p3d( *iout_org_ppp ) == false ) {
						//delete subtree_org, iout_org_ppp and iout_org_ifo, however in any case this would be fatal
						#pragma omp critical
						{
							cerr << "Thread " << omp_get_thread_num() << " subtree_org has overlapping indices!" << "\n";
						}
						return;
					}
					else {
						ityp_kdtree_org.at(tr) = subtree_org;
						//ionpp2_kdtree_org.at(tr) = iout_org;
						ionpp1_kdtree_org.at(tr) = iout_org_ppp;
						ionifo_kdtree_org.at(tr) = iout_org_ifo;
						#pragma omp critical
						{
							cout << "Thread " << omp_get_thread_num() << " sub-tree for ityp " << tr << " build has " << subtree_org->nodes.size() << " nodes and "
									<< iout_org_ppp->size() << " ions" << " and " << iout_org_ifo->size() << " infos" << "\n";
						}
					}
				}
				else {
					//delete subtree_org
					#pragma omp critical
					{
						cerr << "Thread " << omp_get_thread_num() << " iout_org_ppp and iout_org_ifo allocation failed!" << "\n";
					}
					return;
				}
				//helper arrays no longer necessary so release...
				iin_org_ppp = vector<p3d>();
				iin_org_ifo = vector<p3difo>();
				iperm_org = vector<size_t>();
			}
			else {
				#pragma omp critical
				{
					cerr << "Thread " << omp_get_thread_num() << " subtree_org tr " << tr << " allocation failed!" << "\n";
				}
				return;
			}
		}
	}

	//and proceed with with the random labels
	int currentlabel_rnd = 0;
	for( size_t tr = 0; tr < static_cast<size_t>(1+ityp_max); tr++, currentlabel_rnd++ ) {
		unsigned char lbl_rnd = static_cast<unsigned char>(currentlabel_rnd);
		vector<p3d> iin_rnd_ppp;
		vector<p3difo> iin_rnd_ifo;
		for( size_t i = 0; i < ionpp1.size(); i++ ) {
		//for( auto it = ionpp2.begin(); it != ionpp2.end(); it++ ) { //filter ions of sought after iontype
			//if ( it->i_rnd != lbl_rnd )
			if( ionifo.at(i).i_rnd != lbl_rnd ) {
				continue;
			}
			iin_rnd_ppp.push_back( ionpp1.at(i) );
			iin_rnd_ifo.push_back( ionifo.at(i) );
		}
		if ( iin_rnd_ppp.size() < 1 && iin_rnd_ifo.size() < 1 ) { //no ions with this label i this threadregion so respective pointers to kdtree, ionpp1, and ionifo remain NULL
			continue;
		}
		else {
			kd_tree* subtree_rnd = NULL;
			subtree_rnd = new kd_tree; //this calls the default constructor
			if ( subtree_rnd != NULL ) {
				//build incrementally the sub-tree for original and randomized labels for the tr-th iontype
				//copy of ions of this specific tr-th iontype lbl, allocate temporaries and permutated position array
				vector<size_t> iperm_rnd;
				iperm_rnd.reserve( iin_rnd_ppp.size() );

				subtree_rnd->build( iin_rnd_ppp, iperm_rnd, 256 );	//this really constructs the nodes of the tree, here with 256 objects per leaf

				//reorganize the points in memory such that points in the same kdtree bin are contiguous in memory
				vector<p3d>* iout_rnd_ppp = NULL;
				iout_rnd_ppp = new vector<p3d>();
				vector<p3difo>* iout_rnd_ifo = NULL;
				iout_rnd_ifo = new vector<p3difo>();
				if ( iout_rnd_ppp != NULL && iout_rnd_ifo != NULL ) {

					subtree_rnd->pack( iperm_rnd, iin_rnd_ppp, iin_rnd_ifo, *iout_rnd_ppp, *iout_rnd_ifo );

					if ( subtree_rnd->verify_p3d( *iout_rnd_ppp ) == false ) {
						#pragma omp critical
						{
							cerr << "Thread " << omp_get_thread_num() << " subtree_rnd has overlapping indices!" << "\n";
						}
						return;
					}
					else {
						ityp_kdtree_rnd.at(tr) = subtree_rnd;
						//ionpp2_kdtree_rnd.at(tr) = iout_rnd;
						ionpp1_kdtree_rnd.at(tr) = iout_rnd_ppp;
						ionifo_kdtree_rnd.at(tr) = iout_rnd_ifo;
						#pragma omp critical
						{
							cout << "Thread " << omp_get_thread_num() << " sub-tree for ityp " << tr << " build has " << subtree_rnd->nodes.size() << " nodes and "
									<< iout_rnd_ppp->size() << " ions" << " and " << iout_rnd_ifo->size() << " infos" << "\n";
						}
					}
				}
				else {
					#pragma omp critical
					{
						cerr << "Thread " << omp_get_thread_num() << " iout_rnd_ppp and iout_rnd_ifo allocation failed!" << "\n";
					}
					return;
				}
				//helper arrays no longer necessary so release...
				iin_rnd_ppp = vector<p3d>();
				iin_rnd_ifo = vector<p3difo>();
				iperm_rnd = vector<size_t>();
			}
			else {
				#pragma omp critical
				{
					cerr << "Thread " << omp_get_thread_num() << " subtree_rnd tr " << tr << " allocation failed!" << "\n";
				}
				return;
			}
		}
	}

	//now we have all points per threadregion sorted and stored organized in categories
	//the categories are the iontype, separately organized for original labels and random labels
	//we have also the ifo object which bundles the distance of the ion to the dataset boundary + the global ID such we always can
	//deterministically organize the data again in the original ion sequence/order which InputfileReconstruction has
	//free unnecessary data duplicates
	ionpp1 = vector<p3d>();
	ionifo = vector<p3difo>();

	double toc = omp_get_wtime();

	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " ityp-specific sub-KDTree field completed in " << (toc-tic) << " seconds" << "\n";
	}
}


apt_real threadmemory::get_zmi()
{
	return this->zmi;
}


apt_real threadmemory::get_zmx()
{
	return this->zmx;
}



threadmemory_guarded::threadmemory_guarded()
{
	halo_zmi = F32MX;
	halo_zmx = F32MI;
	inside_zmi = F32MX;
	inside_zmx = F32MI;
	//melast = false;
}


threadmemory_guarded::~threadmemory_guarded()
{
	for( size_t i = 0; i < ionpp1_org.size(); i++ ) {
		delete ionpp1_org.at(i); ionpp1_org.at(i) = NULL;
	}
	ionpp1_org = vector<vector<p3d>*>();

	for( size_t i = 0; i < ionifo_org.size(); i++ ) {
		delete ionifo_org.at(i); ionifo_org.at(i) = NULL;
	}
	ionifo_org = vector<vector<p3difo>*>();
}


void threadmemory_guarded::init_localmemory( p6d64 const & mybounds, p3d64 const & myhalo,
		const bool melast, const unsigned char ityp_max )
{
	//MK::CALLED FROM WITHIN PARALLEL REGION
	halo_zmi = mybounds.zmi - myhalo.z;
	halo_zmx = mybounds.zmx + myhalo.z;
	inside_zmi = mybounds.zmi;
	inside_zmx = mybounds.zmx;

	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " maximum ityp_max label is " << (int) ityp_max << "\n";
	}

	//define and allocate subtrees, we need ityp_max + 1 subtrees in total
	size_t nsubtrees = static_cast<size_t>(1+ityp_max);
	if ( nsubtrees >= static_cast<size_t>(UCHARMX) ) {
		#pragma omp critical
		{
			cerr << "More than 256 trees cannot be handled by current implementation!" << "\n";
		}
		return;
	}

	try {
		ionpp1_org = vector<vector<p3d>*>( nsubtrees, NULL );
		ionifo_org = vector<vector<p3difo>*>( nsubtrees, NULL );
		for( size_t ityp = 0; ityp < nsubtrees; ityp++ ) {
			ionpp1_org.at(ityp) = new vector<p3d>();
			ionifo_org.at(ityp) = new vector<p3difo>();
		}
	}
	catch (bad_alloc &mecroak) {
		#pragma omp critical
		{
			cerr << "Thread " << omp_get_thread_num() << " array coordination hub allocation failed!" << "\n";
		}
		return;
	}
}


void threadmemory_guarded::init_localmemory_arrays_per_ityp( vector<p3d> const & p,
		vector<apt_real> const & d, vector<unsigned char> const & lorg )
{
	//MK::CALLED FROM WITHIN PARALLEL REGION
	//picks from reconstructed volume all positions inside me into local structure
	unsigned int nions = p.size();
	unsigned int inside_cnt = 0;
	unsigned int guard_cnt = 0;
	for( unsigned int ionID = 0; ionID < nions; ionID++ ) {
		if ( p.at(ionID).z < halo_zmi )
			continue;
		if ( p[ionID].z >= halo_zmx )
			continue;

		//not continued ion is within or on boundary of interval [halo_zmi, halo_zmx)
		unsigned int ityp = static_cast<unsigned int>(lorg.at(ionID));
		if ( p[ionID].z >= inside_zmi && p[ionID].z < inside_zmx ) { //in interior
			ionpp1_org.at(ityp)->push_back( p3d( p[ionID].x, p[ionID].y, p[ionID].z ) );
			ionifo_org.at(ityp)->push_back( p3difo( d.at(ionID), ionID, lorg.at(ionID), 0xFF, 0x00 ) );
			inside_cnt++;
		}
		else { //in halo
			ionpp1_org.at(ityp)->push_back( p3d( p[ionID].x, p[ionID].y, p[ionID].z ) );
			ionifo_org.at(ityp)->push_back( p3difo( d.at(ionID), ionID, lorg.at(ionID), 0x00, 0xFF ) );
			guard_cnt++;
		}
	}

	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " init_localions ionpp1/ionifo size << " << ionpp1_org.size() << ";" << ionifo_org.size() << " inside/guard " << inside_cnt << ";" << guard_cnt << "\n";
	}
}


apt_real threadmemory_guarded::get_halo_zmi()
{
	return this->halo_zmi;
}


apt_real threadmemory_guarded::get_halo_zmx()
{
	return this->halo_zmx;
}


apt_real threadmemory_guarded::get_inside_zmi()
{
	return this->inside_zmi;
}


apt_real threadmemory_guarded::get_inside_zmx()
{
	return this->inside_zmx;
}
