//##MK::GPLV3

#ifndef __PARAPROBE_UTILS_CPUGPUWORKLOAD_H__
#define __PARAPROBE_UTILS_CPUGPUWORKLOAD_H__


#include "PARAPROBE_VolumeSampler.h"


struct epoch_log
{
	unsigned int threadid;
	unsigned int nthreads;
	unsigned int epoch;
	unsigned int nchunks;	//##MK::no differentiation between CPU or GPU necessary, a thread either solves SDM stuff or organizes workload across GPUs
	double dt;				//profiling data should be always double precision
	size_t ionsum;			//how many ions in the ROI across all nchunks as simplest complexity measure

	epoch_log() : threadid(-1), nthreads(-1), epoch(-1), nchunks(0), dt(0.0), ionsum(0) {}
	epoch_log( const unsigned int _mthr, const unsigned int _nthr ) : threadid(_mthr), nthreads(_nthr), epoch(-1), nchunks(0), dt(0.0), ionsum(0) {}
	epoch_log( const unsigned int _mthr, const unsigned int _nthr, const unsigned int _eid, const unsigned int _nchks, const double _dt, const size_t _nsum ) :
		 threadid(_mthr), nthreads(_nthr), epoch(_eid), nchunks(_nchks), dt(_dt), ionsum(_nsum) {}
};

ostream& operator << (ostream& in, epoch_log const & val);

//custom comparator to sort epoch_log with multi valued entries
//https://stackoverflow.com/questions/3574680/sort-based-on-multiple-things-in-c
bool SortEpochLogAsc( const epoch_log & a, const epoch_log & b );


struct epoch_prof
{
	unsigned int epoch;
	double dt;
	epoch_prof() : epoch(-1), dt(0.0) {}
	epoch_prof( const unsigned int _eid, const double _dt) : epoch(_eid), dt(_dt) {}
};


struct gpu_cpu_max_workload
{
	int chunk_max_gpu;
	int chunk_max_cpu;
	gpu_cpu_max_workload() : chunk_max_gpu(0), chunk_max_cpu(0) {}
	gpu_cpu_max_workload( const int _gpu, const int _cpu ) :
		chunk_max_gpu(_gpu), chunk_max_cpu(_cpu) {}
};


struct gpu_cpu_now_workload
{
	int chunk_now_gpu;
	int chunk_now_cpu;
	gpu_cpu_now_workload() : chunk_now_gpu(0), chunk_now_cpu(0) {}
	gpu_cpu_now_workload( const int _gpu, const int _cpu ) :
		chunk_now_gpu(_gpu), chunk_now_cpu(_cpu) {}
};


#endif

