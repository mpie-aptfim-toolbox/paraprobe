/*
	Copyright Max-Planck-Institut f\"ur Eisenforschung, GmbH, D\"sseldorf
	Data structure, code design, parallel implementation:
	Markus K\"uhbach, 2017-2018

	Third-party contributions:
	Andrew Breen - sequential Matlab code snippets for reconstruction and EPOS
	Markus G\"otz et al. - HPDBScan
	Kartik Kukreja - path compressed union/find
	Lester Hedges - AABBTree

	PARAPROBE --- is an MPI/OpenMP/SIMD-parallelized tool for efficient scalable
	processing of Atom Probe Tomography data targeting back-end processing.

	This file is part of PARAPROBE.

	PARAPROBE is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

 	PARAPROBE is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with paraprobe.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_HDF5.h"

//#ifdef UTILIZE_HDF5


ostream& operator<<(ostream& in, h5iometa const & val)
{
	in << val.dsetnm << "__" << val.nr << "__" << val.nc << endl;
	return in;
}

ostream& operator<<(ostream& in, h5offsets const & val)
{
	in << "nr0/nr1__" << val.nr0 << "__" << val.nr1 << "__nc0/nc1__" << val.nc0 << "__" << val.nc1 << "__nrmx/ncrmx__" << val.nrmax << "__" << val.ncmax << endl;
	return in;
}



bool h5offsets::is_within_bounds( const size_t _nr0, const size_t _nr1, const size_t _nc0, const size_t _nc1 )
{
	if ( _nr0 >= this->nr0 && _nr1 <= (this->nr0 + this->nr1) &&
			_nc0 >= this->nc0 && _nc1 <= (this->nc0 + this->nc1) )
		return true;
	else
		return false;
}


size_t h5offsets::nrows()
{
	return this->nr1 - this->nr0;
}


size_t h5offsets::ncols()
{
	return this->nc1 - this->nc0;
}

/*
size_t h5offsets::interval_length( const size_t _n0, const size_t _n1 )
{
	if ( this->is_within_bounds( _n0, _n1) == true )
		return _n1-_n0;
	else
		return -1;
}
*/


void debug_hdf5( void )
{
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	* Copyright by The HDF Group.                                               *
	* Copyright by the Board of Trustees of the University of Illinois.         *
	* All rights reserved.                                                      *
	*                                                                           *
	* This file is part of HDF5.  The full HDF5 copyright notice, including     *
	* terms governing use, modification, and redistribution, is contained in    *
	* the COPYING file, which can be found at the root of the source code       *
	* distribution tree, or in https://support.hdfgroup.org/ftp/HDF5/releases.  *
	* If you do not have access to either file, you may request a copy from     *
	* help@hdfgroup.org.                                                        *
	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	/*
	*  This example illustrates how to create a dataset that is a 4 x 6
	*  array.  It is used in the HDF5 Tutorial.
	*/

	//#include "hdf5.h"
	#define FILE "dset.h5"
	//int main() {
	hid_t       file_id, dataset_id, dataspace_id;  /* identifiers */
	hsize_t     dims[2];
	herr_t      status;

	/* Create a new file using default properties. */
	file_id = H5Fcreate(FILE, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

	/* Create the data space for the dataset. */
	dims[0] = 4;
	dims[1] = 6;
	dataspace_id = H5Screate_simple(2, dims, NULL);

	/* Create the dataset. */
	dataset_id = H5Dcreate2(file_id, "/dset", H5T_STD_I32BE, dataspace_id,
						  H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

	/* End access to the dataset and release resources used by it. */
	status = H5Dclose(dataset_id);

	/* Terminate access to the data space. */
	status = H5Sclose(dataspace_id);

	/* Close the file. */
	status = H5Fclose(file_id);
}


/*
void write_pos_hdf5( vector<vector<pos>*> & in, const string h5_io_fn )
{
//#ifndef UTILIZE_HDF5
	cout << "PARAPROBE was not compiled with HDF5 support" << endl;
	return;
//#else
	//##MK::add group names
	//MK::write HDF5 file showing the positions and typ of all ions in reconstructed space
	double tic, toc;
	tic = MPI_Wtime();

	//MK::why HDF5 and chunking? i) enable compression, ii) avoid allocating monolithic block of additional buffer for ppp and lll iii) I/O efficiency
	//take for instance MATLAB loading a file takes intermediately twice the memory, the same with Paraview
	//sure this work nicely for files in the order of 1GB or so but when facing 30GB and more, read will not work because 60GB++ may exceed main memory
	//which is completely unnecessary
	//instead read file successively in packages of several Megabytes

	//##MK::HDF5 chunking requires dataset size to be integer multiple of chunk size therefore round to nearest integer multiple

	size_t Atoms = 0;
	for(size_t b = 0; b< in.size(); ++b) {
		if ( in.at(b) != NULL )
			Atoms += in.at(b)->size();
	}
	if ( Atoms >= UINT32MX ) {
		stopping("Attempting to export more than 4.2 billion atoms is not implemented!"); return;
	}

	size_t BytesPerChunk = 10*1024*1024; //##MK::must be integer multiple of 4 !
	size_t AtomsPerChunk = BytesPerChunk / (4 * 4); //10MB chunk size, four float32 sized 4B each
	size_t NChunks = static_cast<size_t>(ceil(static_cast<double>(Atoms)/static_cast<double>(AtomsPerChunk)));

	cout << "HDF5 Atoms/BytesPerChunk/AtomsPerChunk/NChunks\t\t" << Atoms << ";" << BytesPerChunk << ";" << AtomsPerChunk << ";" << NChunks << endl;

	//allocate a write buffer on stack memory
	float* wbuf = NULL;
	try {
		wbuf = new float[4*AtomsPerChunk];          //##MK::() initializes values to zero
		for(size_t i = 0; i < 4*AtomsPerChunk; ++i) { wbuf[i] = F32MI; }
	}
	catch (bad_alloc &h5croak) {
		stopping("Allocation error for write buffer in write HDF5");
		return;
	}

	//write total number of Atoms in the HDF5 dataset given that the last chunk might contain additional dummy data
	unsigned int wdata[1] = { static_cast<unsigned int>(Atoms) };
	//initialize the HDF5 file, does file with same name exist already overwrite, if not create, in every case open it
	herr_t status;
	hid_t fileid = H5Fcreate( h5_io_fn.c_str() , H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );
	hsize_t sdims[1] = {1};
	int rank = 1;
	hid_t dspcid = H5Screate_simple(rank, sdims, NULL);
	hid_t dsetid = H5Dcreate2(fileid, "/NumberOfIons", H5T_STD_U32LE, dspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dwrite(dsetid, H5T_STD_U32LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, wdata );
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);

	cout << "HDF5 wrote NumberOfIons to file " << wdata[0] << endl;

	//##MK::add versioning within HDF5 file via group ID

	//start processing chunks into buffer successively and write to HDF5 file
	hsize_t dims[2] = { AtomsPerChunk, 4};
	hsize_t maxdims[2] = {H5S_UNLIMITED, H5S_UNLIMITED};
	hsize_t offset[2] = {0, 0};
	hsize_t dimsnew[2] = {0, 4};

	dspcid = H5Screate_simple(2, dims, maxdims);
	hid_t cparms = H5Pcreate(H5P_DATASET_CREATE);
	status = H5Pset_chunk( cparms, 2, dims);
	float fillval = 0.f;
	status = H5Pset_fill_value(cparms, H5T_IEEE_F32LE, &fillval );
	dsetid = H5Dcreate2(fileid, "/XYZMQ", H5T_IEEE_F32LE, dspcid, H5P_DEFAULT, cparms, H5P_DEFAULT);

	hid_t fspcid;

	//prepare HDF5 file for chunked data set
	size_t ChunkCurrPos = 0;
	size_t ChunkMaxiPos = AtomsPerChunk;
	for(size_t b = 0; b < in.size(); ++b) { //successive walk from buckets and collect result until chunk buffer is full then write, refresh buffer and proceed
		if( in.at(b) != NULL ) {
			vector<pos>* these = in.at(b);
			size_t BucketCurrPos = 0;
			size_t BucketMaxiPos = these->size(); //one last last element

			//cout << b << "\t\t\t[ " << BucketCurrPos << " ; " << BucketMaxiPos << " ) " << endl;
			while ( BucketCurrPos < BucketMaxiPos ) { //bucket can be much larger than chunk cache
				size_t ChunkRemaining = ChunkMaxiPos - ChunkCurrPos;
				size_t BucketRemaining = BucketMaxiPos - BucketCurrPos;
				size_t BucketStopPos = (BucketRemaining <= ChunkRemaining) ? BucketMaxiPos : (BucketCurrPos + ChunkRemaining);

				for(     ; BucketCurrPos < BucketStopPos; BucketCurrPos++ ) { //write content into cache
					wbuf[4*ChunkCurrPos+0] = these->at(BucketCurrPos).x;
					wbuf[4*ChunkCurrPos+1] = these->at(BucketCurrPos).y;
					wbuf[4*ChunkCurrPos+2] = these->at(BucketCurrPos).z;
					wbuf[4*ChunkCurrPos+3] = these->at(BucketCurrPos).mq;
					ChunkCurrPos++;
				}

				if ( ChunkCurrPos < ChunkMaxiPos ) { //chunk buffer not full yet
					//cout << "--->fill--->BucketCurrPos/MaxiPos/ChunkCurrPos/MaxiPos\t\t" << BucketCurrPos << "\t\t" << BucketMaxiPos << "\t\t" << ChunkCurrPos << "\t\t" << ChunkMaxiPos << endl;
					continue;
				}
				else { //chunk buffer full, empty write content to HDF5, ##MK::catch H5 errors
					//cout << "--->dump--->BucketCurrPos/MaxiPos/ChunkCurrPos/MaxiPos\t\t" << BucketCurrPos << "\t\t" << BucketMaxiPos << "\t\t" << ChunkCurrPos << "\t\t" << ChunkMaxiPos << endl;
					dimsnew[0] = dimsnew[0] + AtomsPerChunk;
					status = H5Dset_extent(dsetid, dimsnew);
					fspcid = H5Dget_space(dsetid);
					status = H5Sselect_hyperslab(fspcid, H5S_SELECT_SET, offset, NULL, dims, NULL);
					status = H5Dwrite(dsetid, H5T_IEEE_F32LE, dspcid, fspcid, H5P_DEFAULT, wbuf);
					offset[0] = offset[0] + AtomsPerChunk; //MK::second dimensions remains as is!

					cout << "Chunk intermediate " << ChunkCurrPos << " written to HDF5 file" << endl;

					//reset allocated chunk buffer
					for( size_t i = 0; i < 4*AtomsPerChunk; ++i ) { wbuf[i] = F32MI; }
					ChunkCurrPos = 0;

					//cout << "--->dump--->dimsnew[0]\t\t" << dimsnew[0] << endl;
				}

				//##MK::BucketCurrPos increases so somewhen we will get out of the while loop

			} //process all element from the bucket
		} //...if it contains elements
	} //do so for all buckets

	//cout << "Buckets done now ChunkCurrPos\t\t" << ChunkCurrPos << endl;

	//dont forget to write the last chunk as it might not have been filled completely even when all buffer were processed
	if ( ChunkCurrPos > 0 ) {
		dimsnew[0] = dimsnew[0] + AtomsPerChunk;
		status = H5Dset_extent(dsetid, dimsnew);
		fspcid = H5Dget_space(dsetid);
		status = H5Sselect_hyperslab(fspcid, H5S_SELECT_SET, offset, NULL, dims, NULL);
		status = H5Dwrite(dsetid, H5T_IEEE_F32LE, dspcid, fspcid, H5P_DEFAULT, wbuf);
		offset[0] = offset[0] + AtomsPerChunk;
	}

	//done writing HDF5 file, release resources
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Sclose(fspcid);
	status = H5Pclose(cparms);
	status = H5Fclose(fileid);

	cout << "Chunk last " << ChunkCurrPos << " written to HDF5 file" << endl;

	//deallocate chunk buffer in any caseas no longer needed
	delete [] wbuf; wbuf = NULL;

	string mess = "HDF5IO writing ion location in reconstruction space to " + h5_io_fn;
	reporting( mess );

	toc = MPI_Wtime();
	cout << "HDF5 Reconstruction ion locations written in " << (toc - tic) << " seconds" << endl;
//#endif
}
*/


h5Hdl::h5Hdl()
{
	status = 0;
	fileid = 0;
	ftypid = 0;
	groupid = 0;
	mtypid = 0;
	mspcid = 0;
	dsetid = 0;
	dspcid = 0;
	cparms = 0;
	fspcid = 0;
	attrid = 0;

	dims[0] = 0; 		dims[1] = 0;
	maxdims[0] = 0; 	maxdims[1] = 0;
	offset[0] = 0; 		offset[1] = 0;
	dimsnew[0] = 0; 	dimsnew[1] = 0;

	nrows = 0;
	ncols = 0;
	BytesPerChunk = 0;
	RowsPerChunk = 0;
	ColsPerChunk = 0;
	RowsColsPerChunk = 0;
	NChunks = 0;
	TotalWriteHere = 0;
	TotalWritePortion = 0;
	TotalWriteAllUsed = 0;
	TotalWriteAllChunk = 0;

	h5resultsfn = "";

	u32le_buf = NULL;
	f64le_buf = NULL;
}


h5Hdl::~h5Hdl()
{
	delete [] u32le_buf; u32le_buf = NULL;
	delete [] f64le_buf; f64le_buf = NULL;
}


void h5Hdl::reinitialize()
{
	status = 0;
	fileid = 0;
	ftypid = 0;
	groupid = 0;
	mtypid = 0;
	dsetid = 0;
	dspcid = 0;
	cparms = 0;
	fspcid = 0;
	attrid = 0;

	dims[0] = 0; 		dims[1] = 0;
	maxdims[0] = 0; 	maxdims[1] = 0;
	offset[0] = 0; 		offset[1] = 0;
	dimsnew[0] = 0; 	dimsnew[1] = 0;

	nrows = 0;
	ncols = 0;
	BytesPerChunk = 0;
	RowsPerChunk = 0;
	ColsPerChunk = 0;
	RowsColsPerChunk = 0;
	NChunks = 0;
	TotalWriteHere = 0;
	TotalWritePortion = 0;
	TotalWriteAllUsed = 0;
	TotalWriteAllChunk = 0;

	h5resultsfn = "";

	delete [] u32le_buf;
	delete [] f64le_buf;
}


int h5Hdl::create_file( const string h5fn )
{
	h5resultsfn = h5fn;
	fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


/*
int h5Hdl::create_paraprobe_apth5( const string h5fn )
{
	//generate a IFES APTTC conformant APTH5 HDF5 file
	//##MK::catch errors
	h5resultsfn = h5fn;
	fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );

	//domain specific HDF5 keywords and data fields
	groupid = H5Gcreate2(fileid, APTH5_ACQ_00, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "APTH5_ACQ_00 " << status << "\n";

	//metadata specifying the environment in which the tool was used
	groupid = H5Gcreate2(fileid, APTH5_ACQ_01, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "APTH5_ACQ_01 " << status << "\n";

	//metadata specifying details and pieces of information about the tool
	groupid = H5Gcreate2(fileid, APTH5_ACQ_02, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "APTH5_ACQ_02 " << status << "\n";

	//measured quantities during the course of this experiment with this specific tool for this specific sample
	groupid = H5Gcreate2(fileid, APTH5_ACQ_03, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << "APTH5_ACQ_03 " << status << "\n";

	status = H5Fclose(fileid);
cout << "Closing APTH5 file" << "\n";

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_paraprobe_results_file( const string h5fn, const int rank )
{
	//generate PARAPROBE reporting file for general results referring to the reconstruction and surface
	//H5F_ACC_TRUNC truncate content of all files in current working directory with same name as h5fn
	h5resultsfn = h5fn;

	if ( rank == MASTER ) {
		fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );

		//Ranging is always done, all types and ranges
		if ( Settings::RangingMode == E_RANGING_AUTOMATIC ) {
			groupid = H5Gcreate2(fileid, PARAPROBE_AUTORANGING, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
			groupid = H5Gcreate2(fileid, PARAPROBE_AUTORANGING_SETTINGS, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
			groupid = H5Gcreate2(fileid, PARAPROBE_AUTORANGING_PEAKS, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
			groupid = H5Gcreate2(fileid, PARAPROBE_AUTORANGING_PEAKS_CANDS, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
			groupid = H5Gcreate2(fileid, PARAPROBE_AUTORANGING_PEAKS_SCORES, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
		}

		//VolumeRecon,xyz,mq
		if ( Settings::IOReconstruction == true || Settings::IOIonTipSurfDists == true ) {
			groupid = H5Gcreate2(fileid, PARAPROBE_VOLRECON, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
		}

		//SurfaceRecon, alphaShape, distances
		if ( Settings::SurfaceMeshingAlgo != E_NOSURFACE ) {
			groupid = H5Gcreate2(fileid, PARAPROBE_SURFRECON, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
			groupid = H5Gcreate2(fileid, PARAPROBE_SURFRECON_ASHAPE, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
			groupid = H5Gcreate2(fileid, PARAPROBE_SURFRECON_ASHAPE_HULL, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
		}

		//descriptive statistics
		if ( Settings::SpatialDistributionTask != E_NOSPATSTAT ) {
			groupid = H5Gcreate2(fileid, PARAPROBE_DESCRSTATS, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);

			if ( Settings::SpatStatDoNPCorr == true ) {
				groupid = H5Gcreate2(fileid, PARAPROBE_DESCRSTATS_NCORR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
				status = H5Gclose(groupid);
			}
		}

		//close file
		status = H5Fclose(fileid);
	}
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_paraprobe_clust_file( const string h5fn, const int rank )
{
	//generate PARAPROBE reporting file for clustering results
	//H5F_ACC_TRUNC truncate content of all files in current working directory with same name as h5fn
	h5resultsfn = h5fn;

	if ( rank == MASTER ) {
		fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );

		//clustering
		groupid = H5Gcreate2(fileid, PARAPROBE_CLUST, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		status = H5Gclose(groupid);

		if ( Settings::ClusteringTask == E_MAXSEPARATION ) {
			groupid = H5Gcreate2(fileid, PARAPROBE_CLUST_MAXSEP, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
			groupid = H5Gcreate2(fileid, PARAPROBE_CLUST_MAXSEP_SZOUT, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
			groupid = H5Gcreate2(fileid, PARAPROBE_CLUST_MAXSEP_SZINN, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
			groupid = H5Gcreate2(fileid, PARAPROBE_CLUST_MAXSEP_XYZOUT, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
			groupid = H5Gcreate2(fileid, PARAPROBE_CLUST_MAXSEP_XYZINN, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
			groupid = H5Gcreate2(fileid, PARAPROBE_CLUST_MAXSEP_SZALL_CDF, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
			groupid = H5Gcreate2(fileid, PARAPROBE_CLUST_MAXSEP_SZINN_CDF, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
		}

		//close file
		status = H5Fclose(fileid);
	}
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_paraprobe_cryst_file( const string h5fn, const int rank )
{
	//generate PARAPROBE reporting file for crystallography results

	//all ranks set the name of a result file,
	//MK::but for a calls to the sequential H5 library only one may write to it at a time and only one
	//create the file as otherwise we have a race condition at file creation
	h5resultsfn = h5fn;

	if ( rank == MASTER ) {
		fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );

		//Crystallography, sampling point cloud, elev/azim pair cloud, three strongest signals
		//cloud of points in corresponding spaces, not necessarily regular nd grids!
		groupid = H5Gcreate2(fileid, PARAPROBE_CRYSTALLO, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		status = H5Gclose(groupid);

		if ( Settings::ExtractCrystallographicHow == E_ARAULLOPETERS || Settings::ExtractCrystallographicHow == E_BREENSDM ) {
			if ( Settings::IOCrystalloStorePeaks == true ) {
				groupid = H5Gcreate2(fileid, PARAPROBE_CRYSTALLO_THREESTRONGEST, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
				status = H5Gclose(groupid);
			}
			if ( Settings::IOCrystalloStoreAllPeaks == true ) {
				groupid = H5Gcreate2(fileid, PARAPROBE_CRYSTALLO_ALLPEAKS, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
				status = H5Gclose(groupid);
			}
			if ( Settings::IOCrystalloStoreHistoCnts == true ) {
				groupid = H5Gcreate2(fileid, PARAPROBE_CRYSTALLO_HISTCNTS, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
				status = H5Gclose(groupid);
			}
			if ( Settings::IOCrystalloStoreSpecificPeak == true ) {
				groupid = H5Gcreate2(fileid, PARAPROBE_CRYSTALLO_SPECIFICPEAK, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
				status = H5Gclose(groupid);
			}
		}
		else { //E_FRANCOIS_FOURIER
			groupid = H5Gcreate2(fileid, PARAPROBE_APTOIM_DEBUG_FT, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
			groupid = H5Gcreate2(fileid, PARAPROBE_APTOIM_DEBUG_FT_MAGNITUDE, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			status = H5Gclose(groupid);
		}

		//close file
		status = H5Fclose(fileid);
	}
	//##MK::
	return WRAPPED_HDF5_SUCCESS;
}


//int h5Hdl::create_paraprobe_voronoi_file( const string h5fn, const int rank )
//{
//	if ( Settings::VolumeTessellation != E_NOTESS ) {
//		//generate PARAPROBE default results reporting file
//		//H5F_ACC_TRUNC truncate content of all files in current working directory with same name as h5fn
//		h5resultsfn = h5fn;
//		if ( rank == MASTER ) {
//			fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );
//
//			//close file
//			status = H5Fclose(fileid);
//		}
//		return WRAPPED_HDF5_SUCCESS;
//	}
//	return WRAPPED_HDF5_SUCCESS;
//}


int h5Hdl::create_paraprobe_oimindexing_file( const string h5fn, const int rank )
{
	//generate PARAPROBE reporting file for 3D APT OIM indexing results
	h5resultsfn = h5fn;

	if ( rank == MASTER ) {
		fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );

		//##MK::debugging currently development of APT 3D OIM functionality
		groupid = H5Gcreate2(fileid, PARAPROBE_APTOIM_INDEXING, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		status = H5Gclose(groupid);
		groupid = H5Gcreate2(fileid, PARAPROBE_APTOIM_DEBUG, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		status = H5Gclose(groupid);
		groupid = H5Gcreate2(fileid, PARAPROBE_APTOIM_INDEXING_SOLUTION, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		status = H5Gclose(groupid);

		//close file
		status = H5Fclose(fileid);
	}
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_paraprobe_debug_file( const string h5fn, const int rank )
{
	//generate PARAPROBE reporting file for crystallography results
	h5resultsfn = h5fn;

	if ( rank == MASTER ) {
		fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );

		//##MK::debugging currently development of APT 3D OIM functionality
		groupid = H5Gcreate2(fileid, PARAPROBE_DEBUG, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		status = H5Gclose(groupid);
		groupid = H5Gcreate2(fileid, PARAPROBE_DEBUG_APTOIM, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		status = H5Gclose(groupid);

		//close file
		status = H5Fclose(fileid);
	}
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_tapsim_report_file( const string h5fn )
{
	//generate PARAPROBE reporting file for comparing TAPSim simulation results with input synthetic tips
	h5resultsfn = h5fn;
	fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );

	//##MK::add subgroups here

	//close file
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}
*/

int h5Hdl::query_contiguous_matrix_dims( const string _dn, h5offsets & offsetinfo )
{
	offsetinfo = h5offsets();

	fileid = H5Fopen(h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	//status = H5Eset_auto(NULL, NULL);
	//cout << "Checking existence of " << _dn.c_str() << " status " << status << "\n";
	status = H5Gget_objinfo (fileid, _dn.c_str(), 0, NULL);
cout << "Checking existence of " << _dn.c_str() << " status " << status << "\n";

	if ( status == 0 ) {
		//group exists, so attempt to read dimensions of existent dataset
		dsetid = H5Dopen2( fileid, _dn.c_str(), H5P_DEFAULT );
	//cout << "Opening2 " << _dn.c_str() << dsetid << "\n";
		dspcid = H5Dget_space(dsetid);
	//cout << "Get space " << dspcid << "\n";
		const int ndims = H5Sget_simple_extent_ndims(dspcid);

		if ( ndims == 2 ) {
			hsize_t dims[ndims];
			status = H5Sget_simple_extent_dims(dspcid, dims, NULL);
		cout << "Getting simple extent dims " << status << "\n";

			offsetinfo = h5offsets( 0, dims[0], 0, dims[1], dims[0], dims[1] );
		}
		status = H5Sclose(dspcid);
		status = H5Dclose(dsetid);
		status = H5Fclose(fileid);

		return WRAPPED_HDF5_SUCCESS;
	}
	//implicit else
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_DOESNOTEXIST;
}


int h5Hdl::create_group( const string grpnm )
{
	//##MK::check if file exists
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	if ( fileid < 0 ) {
		cerr << "Opening file " << h5resultsfn << " failed!" << "\n"; return WRAPPED_HDF5_FAILED;
	}
	groupid = H5Gcreate2(fileid, grpnm.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Creation of group " << grpnm << " failed!" << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Group handle closure failed " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Fclose(fileid);
	if ( status < 0 ) {
		cerr << "File handle closure failed " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}
	return WRAPPED_HDF5_SUCCESS;
}



int h5Hdl::create_group( const string h5fn, const string grpnm )
{
	//##MK::check if file exists
	fileid = H5Fopen(h5fn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	if ( fileid < 0 ) {
		cerr << "Opening file " << h5fn << " failed!" << "\n"; return WRAPPED_HDF5_FAILED;
	}
	groupid = H5Gcreate2(fileid, grpnm.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Creation of group " << grpnm << " failed!" << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


bool h5Hdl::probe_group_existence( const string grpnm )
{
	//##MK::check if file exists
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	if ( fileid < 0 ) {
		cerr << "Opening file " << h5resultsfn << " failed!" << "\n"; return WRAPPED_HDF5_FAILED;
	}
	//status = H5Eset_auto( NULL, NULL );
	status = H5Gget_objinfo( fileid, grpnm.c_str(), 0, NULL );
	if ( status == 0 ) {
		status = H5Fclose( fileid );
		return true;
	}
	//cerr << "Group " << grpnm << " does either not exist or other error occurred!" << "\n";
	return false;
}

/*
#define MAX_NAME	1024
bool h5Hdl::probe_datasets_in_group( const string grpnm, vector<string> & dsnms )
{
	//##MK::reads out which datasets or objects are in a group
	//##MK::using HDF5 query datasets in group C
	//##MK::currently reporting the names of the datasets in the group at the top level, no nesting, no sub-group analyzed
	//based on H5_info.c from http://support.hdfgroup.org

	//example: open a file, open the root, scan the whole file
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	if ( fileid < 0 ) {
		cerr << "Opening file " << h5resultsfn << " failed!" << "\n"; return false;
	}
	groupid = H5Gopen( fileid, grpnm.c_str() );
	if ( groupid < 0 ) {
		cerr << "Access to group " << grpnm << " failed!" << "\n"; return false;
	}
	//scan_group(grpid);
	//Process a group and all it's members, can be modeled to implement different actions and searches
	//hid_t grpid, typeid, dsid;

	//get name and attributes: other info, not shown here: number of links, object id
	ssize_t len = 0;
	char group_name[MAX_NAME];
	char memb_name[MAX_NAME];
	char colon = (char) 42; //ascii code of the colon ASCII character to mask name array entries empty
	//char terminator = (char) 0;
	for( size_t k = 0; k < MAX_NAME; k++ ) {
		group_name[k] = colon; //##MK::terminator?
	}
	//https://support.hdfgroup.org/HDF5/doc/RM/H5I/H5Iget_name.htm
	len = H5Iget_name( groupid, group_name, MAX_NAME );
	if ( len < 0 || len == 0 ) {
		cerr << "Len of group failed or dataset empty! " << len << "\n"; return false;
	}
	string mess = "";
	for ( size_t k = 0; k < MAX_NAME; k++) {
		if ( group_name[k] != colon ) {
			mess = mess + group_name[k];
		}
		else {
			break;
		}
	}
	cout << "Group Name: __" << mess << "__" << "\n";

	//process the attributes of the group, if any.
	//##MK:: scan_attrs(group);
	//############IS THIS NECESSARY?

	//Get all the members of the groups, one at a time
	hsize_t nobj;
	//status = H5Gget_num_objs( groupid, &nobj);
	H5G_info_t info;
	//https://support.hdfgroup.org/HDF5/doc1.8/RM/RM_H5G.html#Group-GetInfo
	status = H5Gget_info( groupid, &info );
	if ( status < 0 ) {
		cerr << "Getting number of objects failed!" << "\n"; return false;
	}
	if ( info.nlinks < 1 ) {
		cerr << "No datasets in the group!" << "\n"; return false;
	}
	for ( hsize_t i = 0; i < info.nlinks; i++ ) {
		//for each object in the group, get the name and what type of object it is
		cout << "Member: " << i << "\n";
		for( size_t k = 0; k < MAX_NAME; k++ ) { //##MK::MAX_NAME-1
			memb_name[k] = colon;
		}
		//memb_name[MAX_NAME-1] = terminator;

		//len = H5Gget_objname_by_idx( groupid, i, memb_name, static_cast<size_t>(MAX_NAME) );
		if ( len > 0 ) {
			cout << "Len " << len << "\n";

			//##MK::thats the only information we are currently hunting after...
			string memb = "";
			for( size_t k = 0; k < MAX_NAME; k++ ) {
				if ( memb_name[k] != colon ) {
					memb = memb + memb_name[k];
				}
				else { //stop parsing the member name
					break;
				}
			}
			cout << "Member: __" << memb << "__" << "\n";

			int	otype = H5Gget_objtype_by_idx( groupid, static_cast<size_t>(i) );
			if ( otype < 0 ) {
				cerr << "H5 library states member is of unknown type!" << "\n"; return false;
			}

			//process each object according to its type
			switch(otype) {
				//case H5G_LINK:
				//	printf(" SYM_LINK:\n");
				//	do_link(gid,memb_name);
				//	break;
				//case H5G_GROUP:
				//	printf(" GROUP:\n");
				//	grpid = H5Gopen(gid,memb_name);
				//	scan_group(grpid);
				//	H5Gclose(grpid);
				//	break;
				case H5G_DATASET:
					cout << "Dataset " << "\n";

					dsnms.push_back( memb );

					//
					//dsid = H5Dopen( groupgid,memb_name);
					//do_dset(dsid);
					//H5Dclose(dsid);
					break;
				//case H5G_TYPE:
				//	printf(" DATA TYPE:\n");
				//	typeid = H5Topen(gid,memb_name);
				//	do_dtype(typeid);
				//	H5Tclose(typeid);
				//	break;
				default:
					cout << "Skipping an ignored object within " << grpnm << "\n";
					break;
			}
		}
		else { //len < 0 || len == 0 )
			cerr << "Len of group member i " << i << " failed or dataset empty!" << "\n";
			continue;
		}
	}
	status = H5Fclose(fileid);
	return true;
}
*/


int h5Hdl::create_contiguous_matrix_u8le( h5iometa const & h5info )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	hsize_t dims[2] = { h5info.nr, h5info.nc };
	hsize_t maxdims[2] = { h5info.nr, h5info.nc };
	dspcid = H5Screate_simple( 2, dims, maxdims );
	dsetid = H5Dcreate2( fileid, h5info.dsetnm.c_str(), H5T_STD_U8LE, dspcid,
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_contiguous_matrix_u16le( h5iometa const & h5info )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	hsize_t dims[2] = { h5info.nr, h5info.nc };
	hsize_t maxdims[2] = { h5info.nr, h5info.nc };
	dspcid = H5Screate_simple( 2, dims, maxdims );
	dsetid = H5Dcreate2( fileid, h5info.dsetnm.c_str(), H5T_STD_U16LE, dspcid,
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_contiguous_matrix_u32le( h5iometa const & h5info )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	hsize_t dims[2] = { h5info.nr, h5info.nc };
	hsize_t maxdims[2] = { h5info.nr, h5info.nc };
	dspcid = H5Screate_simple( 2, dims, maxdims );
	dsetid = H5Dcreate2( fileid, h5info.dsetnm.c_str(), H5T_STD_U32LE, dspcid,
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_contiguous_matrix_i32le( h5iometa const & h5info )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	hsize_t dims[2] = { h5info.nr, h5info.nc };
	hsize_t maxdims[2] = { h5info.nr, h5info.nc };
	dspcid = H5Screate_simple( 2, dims, maxdims );
	dsetid = H5Dcreate2( fileid, h5info.dsetnm.c_str(), H5T_STD_I32LE, dspcid,
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_contiguous_matrix_i64le( h5iometa const & h5info )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	hsize_t dims[2] = { h5info.nr, h5info.nc };
	hsize_t maxdims[2] = { h5info.nr, h5info.nc };
	dspcid = H5Screate_simple( 2, dims, maxdims );
	dsetid = H5Dcreate2( fileid, h5info.dsetnm.c_str(), H5T_STD_I64LE, dspcid,
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_contiguous_matrix_u64le( h5iometa const & h5info )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	hsize_t dims[2] = { h5info.nr, h5info.nc };
	hsize_t maxdims[2] = { h5info.nr, h5info.nc };
	dspcid = H5Screate_simple( 2, dims, maxdims );
	dsetid = H5Dcreate2( fileid, h5info.dsetnm.c_str(), H5T_STD_U64LE, dspcid,
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_contiguous_matrix_i16le( h5iometa const & h5info )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	hsize_t dims[2] = { h5info.nr, h5info.nc };
	hsize_t maxdims[2] = { h5info.nr, h5info.nc };
	dspcid = H5Screate_simple( 2, dims, maxdims );
	dsetid = H5Dcreate2( fileid, h5info.dsetnm.c_str(), H5T_STD_I16LE, dspcid,
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_contiguous_matrix_f32le( h5iometa const & h5info )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	hsize_t dims[2] = { h5info.nr, h5info.nc };
	hsize_t maxdims[2] = { h5info.nr, h5info.nc };
	dspcid = H5Screate_simple( 2, dims, maxdims );
	dsetid = H5Dcreate2( fileid, h5info.dsetnm.c_str(), H5T_IEEE_F32LE, dspcid,
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_contiguous_matrix_f64le( h5iometa const & h5info )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	hsize_t dims[2] = { h5info.nr, h5info.nc };
	hsize_t maxdims[2] = { h5info.nr, h5info.nc };
	dspcid = H5Screate_simple( 2, dims, maxdims );
	dsetid = H5Dcreate2( fileid, h5info.dsetnm.c_str(), H5T_IEEE_F64LE, dspcid,
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_string_ascii( const string grpnm, const string val )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );

	//get C string character representation of the ASCII string val
	hsize_t sdims[1] = { 1}; //a single string no an array of strings!

	size_t SDIM = val.length()+1;
	char* cstr = new char[SDIM];
	strcpy(cstr, val.c_str());

	//Create file and memory datatypes. For this example we will save
	//the strings as FORTRAN strings, therefore they do not need space
	//for the null terminator in the file.
	ftypid = H5Tcopy(H5T_FORTRAN_S1);
	status = H5Tset_size(ftypid, SDIM-1); //-1 because Fortran string we do not store null terminator
	mtypid = H5Tcopy(H5T_C_S1);
	status = H5Tset_size(mtypid, SDIM);

	//Create dataspace. Setting maximum size to NULL sets the maximum size to be the current size
	dspcid = H5Screate_simple(1, sdims, NULL);

	dsetid = H5Dcreate(fileid, grpnm.c_str(), ftypid, dspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dwrite(dsetid, mtypid, H5S_ALL, H5S_ALL, H5P_DEFAULT, cstr);

	//close and release resources
	delete [] cstr; cstr = NULL;
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Tclose(ftypid);
	status = H5Tclose(mtypid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_settings_entry( const string prefix, pparm in )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );

	char u8[3][HDF5_STR_MAXLEN];
	for( int i = 0; i < 3; i++ ) {
		for( int j = 0; j < HDF5_STR_MAXLEN; j++ ) {
			u8[i][j] = '\0'; //'' not supported by gcc;
		}
	}
	for( size_t i = 0; i < in.value.length(); i++ ) { u8[0][i] = in.value.at(i); }
	for( size_t i = 0; i < in.unit.length(); i++ ) { u8[1][i] = in.unit.at(i); }
	for( size_t i = 0; i < in.info.length(); i++ ) { u8[2][i] = in.info.at(i); }

	hsize_t sdims[1] = { 3}; //an array of fixed-length ASCII strings!

	/*
	//get C string character representation of the ASCII string val
	size_t SDIM = in.value.length()+1;
	if ( (SDIM + 1) >= HDF5_STR_MAXLEN ) {
		cerr << "write_settings_entry for keyword " << in.keyword << " is longer than " << HDF5_STR_MAXLEN << "!" << "\n";
		return WRAPPED_HDF5_FAILED;
	}
	char* cstr = new char[HDF5_STR_MAXLEN];
	for( int i = 0; i < HDF5_STR_MAXLEN; i++ ) {
		cstr[i] = '\0';
	}
	strcpy(cstr, in.value.c_str());
	*/

	ftypid = H5Tcopy(H5T_FORTRAN_S1);
	status = H5Tset_size(ftypid, HDF5_STR_MAXLEN - 1);
	mtypid = H5Tcopy(H5T_C_S1);
	status = H5Tset_size(mtypid, HDF5_STR_MAXLEN);

	//Create dataspace. Setting maximum size to NULL sets the maximum size to be the current size
	dspcid = H5Screate_simple(1, sdims, NULL);
	string fwslash = "/";
	string dsnm = prefix + fwslash + in.keyword;
	dsetid = H5Dcreate(fileid, dsnm.c_str(), ftypid, dspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dwrite(dsetid, mtypid, H5S_ALL, H5S_ALL, H5P_DEFAULT, u8[0] );

	//close and release resources
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Tclose(ftypid);
	status = H5Tclose(mtypid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_scalar_u64le( const string h5fn, const string grpnm, const size_t val )
{
	fileid = H5Fopen(h5fn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
	int rank = 1;
	hsize_t sdims[1] = { 1};
	dspcid = H5Screate_simple( rank, sdims, NULL);
	dsetid = H5Dcreate2(fileid, grpnm.c_str(), H5T_STD_U64LE, dspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	size_t wdata[1] = { val };
	status = H5Dwrite(dsetid, H5T_STD_U64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, &wdata );
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::init_chunked_matrix_u32le( const string h5fn, const string dsetnm, const size_t nr, const size_t nc )
{
	//customizes h5Hdl properties for writing a matrix of U32LE nr x nc elements to an existent H5 file
	//we used a chunked data layout in the HDF5 file to allow for inplace compression
	//we caveat is that for arbitrary data the total number of elements nr*nc may not be integer multiple
	//of chunk size, hence we have to fill with buffer
	//##MK::in the future use new partial chunking feature of HDF5 1.10.2 to avoid section of trailing dummies on last chunk
	//we can still though use current concept for visualizing for instance Voronoi cells using XDMF
	//for this we need at least two implicit arrays, one encoding the explicit cell topology
	//another the cell vertex coordinates, however if nr*nc is not necessarily integer multiple of chunk size
	//this is ugly but works as within the XDMF file we can just tell the Dimension of the dataset
	//here we can use the trick that the dimension of the implicit 1d array can be defined shorter
	//than the actual size of the chunked data set

	//we use a several MB write buffer fill before writing the first chunk with dummy values
	//this is useful then fusing successively threadlocal data because threads know their local
	//write entry and exit positions on the global array thereby allowing to handle the complexity
	//of having threadlocal datasets potentially crossing chunk boundaries or generating partially filled chunks
	//which the next thread continues filling and writes to file
	nrows = nr; //real dataset size in number of elements on 1d implicit buffer of unsigned int is nr*nc
	ncols = nc;
	BytesPerChunk = static_cast<size_t>(1*1024*1024); //MK::use default chunk size 1MB, ##MK::performance study
	//##MK::value check needs to be integer multiple of sizeof(unsigned int)*ncols !
	RowsPerChunk = BytesPerChunk / (sizeof(unsigned int) * ncols);
	ColsPerChunk = ncols;
	RowsColsPerChunk = RowsPerChunk*ColsPerChunk;
	NChunks = static_cast<size_t>( ceil(static_cast<double>(nrows*ncols) / static_cast<double>(RowsColsPerChunk)) );

cout << "HDF5 RealDataRows/Cols_BytesPerChunk_Rows/Cols/Rows*ColsPerChunk_NChunksTotal\t\t" << nrows << ";" << ncols << ";" << BytesPerChunk << ";" << RowsPerChunk << ";" << ColsPerChunk << ";" << RowsColsPerChunk << ";" << NChunks << endl;

	//allocate a read/write buffer to pass data to the H5 library call functions
	if ( u32le_buf != NULL ) {
		delete [] u32le_buf;
		u32le_buf = NULL;
	}
	else {
		try {
			u32le_buf = new unsigned int[RowsPerChunk*ColsPerChunk];
		}
		catch (bad_alloc &h5croak) {
			cerr << "Allocation error for write buffer in init_chunked_matrix_u32le HDF5" << "\n";
			return WRAPPED_HDF5_ALLOCERR;
		}
	}

	//open an existent H5 file with read/write access
	fileid = H5Fopen( h5fn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);

	//initialize in this file a chunked data set in an existent group

	//start processing chunks into buffer successively and write to HDF5 file
	dims[0] = RowsPerChunk; 		dims[1] = ColsPerChunk;
	maxdims[0] = H5S_UNLIMITED;		maxdims[1] = H5S_UNLIMITED;
	offset[0] = 0;					offset[1] = 0;
	dimsnew[0] = 0;					dimsnew[1] = ColsPerChunk;

	dspcid = H5Screate_simple(2, dims, maxdims);
	cparms = H5Pcreate(H5P_DATASET_CREATE);
	status = H5Pset_chunk( cparms, 2, dims);

	unsigned int fillval = HDF5_U32LE_DUMMYVALUE;
	status = H5Pset_fill_value(cparms, H5T_STD_U32LE, &fillval );
	dsetid = H5Dcreate2(fileid, dsetnm.c_str(), H5T_STD_U32LE, dspcid, H5P_DEFAULT, cparms, H5P_DEFAULT);

	//fill read/write buffer with dummy values which the preceeding write steps can parse
	for(size_t i = 0; i < RowsColsPerChunk; ++i) {
		u32le_buf[i] = HDF5_U32LE_DUMMYVALUE;
	}
	//and use in combination with the position offsets to always complete first individual chunks,
	//thereafter write them to file and refresh the read/write buffer with dummy values to avoid
	//having to read the file content for any preceeding thread whose data portion interval falls arbitrarily
	//on a chunk boundary
	TotalWriteHere = 0;
	TotalWritePortion = RowsColsPerChunk;
	TotalWriteAllUsed = nrows*ncols;
	TotalWriteAllChunk = NChunks*RowsColsPerChunk; //so many implicitly coded unsigned int to write in total (including potential buffer of trailing dummies)

	//do not close any resource handlers or the file, in what follows we will add data portions per thread
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_chunked_matrix_u32le( const string h5fn, const string dsetnm, vector<unsigned int> const & buffer )
{
	//write a local data portion using opened resources use previous allocated read/write buffer
	//and evaluating Total* interval positioning values to identify whether previous chunk is complete or not
	/*
	cout << "start/end/dims[0]/buffer.size()\t\t" << start << ";" << end << ";" << dims[0] << ";" << buffer.size() << endl;
	//MK::[start, end) mark positions on the initialized current dataset were the buffer data should be written to
	//check that [start,end) is within initialized bounds
	if ( start >= dims[0] || end > dims[0] )
		return WRAPPED_HDF5_OUTOFBOUNDS;
	//check that [start,end) length is the same as the buffer
	if ( (end-start) != buffer.size() )
		return WRAPPED_HDF5_ARGINCONSISTENT;
	*/

	//challenge is that interval [start,end) is not necessarily aligned with the implicitly defined chunk boundaries
	//hence find first chunk which contains array index position start
	size_t LocalCompleted = 0; //how much of feed buffer already processed
	size_t LocalTotal = buffer.size(); //total size of the feed buffer

	size_t BufferWriteHere = 0; //where to start filling in h5 write buffer u32le_buf
	size_t BufferWriteNow = 0; //how many remaining fillable before u32le_buf is full and needs a flush

	do { //loop for as many chunks as fillable with feed buffer content
		//on the fly filling and writing back of completed chunk
		//prior to filling the very first value from feed buffer TotalWriteHere == 0
		//right after filling n-th chunk TotalWriteHere % RowsCols == 0 so buffer was full but already emptied
		//in both these cases the buffer is in a state scientifically completely unfilled
		//MK::so if we feed the results from the threadlocal buffers in the strict ascending order
		//of their threadIDs, i.e. begin with MASTER, the h5 write buffer was not yet flushed to file

		BufferWriteHere = TotalWriteHere % RowsColsPerChunk;
		BufferWriteNow = RowsColsPerChunk;
		//correct write interval end because potential feed buffer not large enough
		//correct write interval end additionally based on whether still sufficient place on buffer available
		if ( (BufferWriteHere+(LocalTotal-LocalCompleted)) < RowsColsPerChunk )
			BufferWriteNow = BufferWriteHere + LocalTotal-LocalCompleted;

cout << "1::BufferWriteHere;Now;LocalCompleted;TotalWriteHere\t\t" << BufferWriteHere << ";" << BufferWriteNow << ";" << LocalCompleted << ";" << TotalWriteHere << endl;
		for( size_t w = BufferWriteHere; w < BufferWriteNow; w++ ) {
			u32le_buf[w] = buffer.at(LocalCompleted);
			LocalCompleted++;
			TotalWriteHere++;
		}
cout << "2::BufferWriteHere;Now;LocalCompleted;TotalWriteHere\t\t" << BufferWriteHere << ";" << BufferWriteNow << ";" << LocalCompleted << ";" << TotalWriteHere << endl;

		//potentially write to H5 file
		if ( TotalWriteHere % RowsColsPerChunk == 0 ) {
cout << "3::Writing a full chunk" << endl;
			dimsnew[0] = dimsnew[0] + RowsPerChunk;
			//second dimension of dimsnew needs to remain as is!
			//the chunk is always written completely, regardless how many values physically relevant or not!
			status = H5Dset_extent(dsetid, dimsnew);
			fspcid = H5Dget_space(dsetid);
			status = H5Sselect_hyperslab(fspcid, H5S_SELECT_SET, offset, NULL, dims, NULL);
			status = H5Dwrite(dsetid, H5T_STD_U32LE, dspcid, fspcid, H5P_DEFAULT, u32le_buf);
			offset[0] = offset[0] + RowsPerChunk;
			//second dimension of offset needs to remain as is!

			//refresh buffer with dummies completely
			for ( size_t i = 0; i < RowsColsPerChunk; ++i )
				u32le_buf[i] = HDF5_U32LE_DUMMYVALUE;
		}

	} while ( LocalCompleted < LocalTotal );
	//processing of feed buffer done

cout << "--->Local done\t\t" << TotalWriteHere << "\t\t" << TotalWriteAllUsed << "\t\t" << TotalWriteAllChunk << endl;

	//dont forget to pass potentially remaining part of the h5 write buffer to file
	if ( TotalWriteHere >= TotalWriteAllUsed ) { //everything which was planned in buffer
		//write h5 buffer out last time if not already happened
		if ( TotalWriteHere % RowsColsPerChunk != 0 ) {
cout << "4::Writing last chunk" << endl;

			dimsnew[0] = dimsnew[0] + RowsPerChunk;
			//second dimension of dimsnew needs to remain as is!
			//the chunk is always written completely, regardless how many values physically relevant or not!
			status = H5Dset_extent(dsetid, dimsnew);
			fspcid = H5Dget_space(dsetid);
			status = H5Sselect_hyperslab(fspcid, H5S_SELECT_SET, offset, NULL, dims, NULL);
			status = H5Dwrite(dsetid, H5T_STD_U32LE, dspcid, fspcid, H5P_DEFAULT, u32le_buf);
			offset[0] = offset[0] + RowsPerChunk;
			//second dimension of offset needs to remain as is!
		}
	}

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::reset_chunked_matrix_u32le_aftercompletion()
{
cout << "Resetting u32le" << endl;

	if ( u32le_buf != NULL ) {
		delete [] u32le_buf;
		u32le_buf = NULL;
	}

	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Sclose(fspcid);
	status = H5Pclose(cparms);
	status = H5Fclose(fileid);

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_contiguous_matrix_u8le_atonce( const string h5fn, const string dsetnm,
				const size_t nr, const size_t nc, vector<unsigned char> const & buffer )
{
	//open existing file and place data in there
	fileid = H5Fopen( h5fn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	dims[0] = nr; 		dims[1] = nc;

    dspcid = H5Screate_simple( 2, dims, NULL );
    dsetid = H5Dcreate( fileid, dsetnm.c_str(), H5T_STD_U8LE, dspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
    status = H5Dwrite( dsetid, H5T_STD_U8LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, buffer.data() );
    status = H5Dclose (dsetid);
    status = H5Sclose (dspcid);
    status = H5Fclose (fileid);

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_contiguous_matrix_u32le_atonce( const string h5fn, const string dsetnm,
	const size_t nr, const size_t nc, vector<unsigned int> const & buffer )
{
	//open existing file and place data in there
	fileid = H5Fopen( h5fn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	dims[0] = nr; 		dims[1] = nc;

    dspcid = H5Screate_simple( 2, dims, NULL );
    dsetid = H5Dcreate( fileid, dsetnm.c_str(), H5T_STD_U32LE, dspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
    status = H5Dwrite( dsetid, H5T_STD_U32LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, buffer.data() );
    status = H5Dclose (dsetid);
    status = H5Sclose (dspcid);
    status = H5Fclose (fileid);

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_contiguous_matrix_u64le_atonce( const string h5fn, const string dsetnm,
	const size_t nr, const size_t nc, vector<size_t> const & buffer )
{
	//open existing file and place data in there
	fileid = H5Fopen( h5fn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	dims[0] = nr; 		dims[1] = nc;

    dspcid = H5Screate_simple( 2, dims, NULL );
    dsetid = H5Dcreate( fileid, dsetnm.c_str(), H5T_STD_U64LE, dspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
    status = H5Dwrite( dsetid, H5T_STD_U64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, buffer.data() );
    status = H5Dclose (dsetid);
    status = H5Sclose (dspcid);
    status = H5Fclose (fileid);

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_contiguous_matrix_f32le_atonce( const string h5fn, const string dsetnm,
	const size_t nr, const size_t nc, vector<float> const & buffer )
{
	//open existing file and place data in there
	fileid = H5Fopen( h5fn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	dims[0] = nr; 		dims[1] = nc;

    dspcid = H5Screate_simple( 2, dims, NULL );
    dsetid = H5Dcreate( fileid, dsetnm.c_str(), H5T_IEEE_F32LE, dspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
    status = H5Dwrite( dsetid, H5T_IEEE_F32LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, buffer.data() );
    status = H5Dclose (dsetid);
    status = H5Sclose (dspcid);
    status = H5Fclose (fileid);

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_contiguous_matrix_f64le_atonce( const string h5fn, const string dsetnm,
	const size_t nr, const size_t nc, vector<double> const & buffer )
{
	//open existing file and place data in there
	fileid = H5Fopen( h5fn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	dims[0] = nr; 		dims[1] = nc;

    dspcid = H5Screate_simple( 2, dims, NULL );
    dsetid = H5Dcreate( fileid, dsetnm.c_str(), H5T_IEEE_F64LE, dspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
    status = H5Dwrite( dsetid, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, buffer.data() );
    status = H5Dclose (dsetid);
    status = H5Sclose (dspcid);
    status = H5Fclose (fileid);

	return WRAPPED_HDF5_SUCCESS;
}



int h5Hdl::write_contiguous_matrix_u8le_hyperslab( h5iometa const & h5info,
		h5offsets const & offsetinfo, vector<unsigned char> const & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };


	//bounds check and consistency buffer has the same length than planed
	if ( buffer.size() == dimssub[0] * dimssub[1] ) {
		//define offset
		hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
		hsize_t count[2] = { dimssub[0], dimssub[1] };
		hsize_t stride[2] = { 1, 1};
		hsize_t block[2] = { 1, 1};
		fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
		dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

		mspcid = H5Screate_simple(2, dimssub, mxdims );
		dspcid = H5Dget_space( dsetid );
		status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
		status = H5Dwrite( dsetid, H5T_STD_U8LE, mspcid, dspcid, H5P_DEFAULT, buffer.data() );

	    status = H5Sclose(mspcid);
	    status = H5Sclose(dspcid);
	    status = H5Dclose(dsetid);
	    status = H5Fclose(fileid);
		return WRAPPED_HDF5_SUCCESS;
	}
	else {
		return WRAPPED_HDF5_INCORRECTDIMENSIONS;
	}
}


int h5Hdl::write_contiguous_matrix_u16le_hyperslab( h5iometa const & h5info,
		h5offsets const & offsetinfo, vector<unsigned short> const & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };


	//bounds check and consistency buffer has the same length than planed
	if ( buffer.size() == dimssub[0] * dimssub[1] ) {
		//define offset
		hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
		hsize_t count[2] = { dimssub[0], dimssub[1] };
		hsize_t stride[2] = { 1, 1};
		hsize_t block[2] = { 1, 1};
		fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
		dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

		mspcid = H5Screate_simple(2, dimssub, mxdims );
		dspcid = H5Dget_space( dsetid );
		status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
		status = H5Dwrite( dsetid, H5T_STD_U16LE, mspcid, dspcid, H5P_DEFAULT, buffer.data() );

	    status = H5Sclose(mspcid);
	    status = H5Sclose(dspcid);
	    status = H5Dclose(dsetid);
	    status = H5Fclose(fileid);
		return WRAPPED_HDF5_SUCCESS;
	}
	else {
		return WRAPPED_HDF5_INCORRECTDIMENSIONS;
	}
}


int h5Hdl::write_contiguous_matrix_u32le_hyperslab( h5iometa const & h5info,
		h5offsets const & offsetinfo, vector<unsigned int> const & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };


	//bounds check and consistency buffer has the same length than planed
	if ( buffer.size() == dimssub[0] * dimssub[1] ) {
		//define offset
		hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
		hsize_t count[2] = { dimssub[0], dimssub[1] };
		hsize_t stride[2] = { 1, 1};
		hsize_t block[2] = { 1, 1};
		fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
		dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

		mspcid = H5Screate_simple(2, dimssub, mxdims );
		dspcid = H5Dget_space( dsetid );
		status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
		status = H5Dwrite( dsetid, H5T_STD_U32LE, mspcid, dspcid, H5P_DEFAULT, buffer.data() );

	    status = H5Sclose(mspcid);
	    status = H5Sclose(dspcid);
	    status = H5Dclose(dsetid);
	    status = H5Fclose(fileid);
		return WRAPPED_HDF5_SUCCESS;
	}
	else {
		return WRAPPED_HDF5_INCORRECTDIMENSIONS;
	}
}


int h5Hdl::write_contiguous_matrix_i32le_hyperslab( h5iometa const & h5info,
		h5offsets const & offsetinfo, vector<int> const & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };


	//bounds check and consistency buffer has the same length than planed
	if ( buffer.size() == dimssub[0] * dimssub[1] ) {
		//define offset
		hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
		hsize_t count[2] = { dimssub[0], dimssub[1] };
		hsize_t stride[2] = { 1, 1};
		hsize_t block[2] = { 1, 1};
		fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
		dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

		mspcid = H5Screate_simple(2, dimssub, mxdims );
		dspcid = H5Dget_space( dsetid );
		status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
		status = H5Dwrite( dsetid, H5T_STD_I32LE, mspcid, dspcid, H5P_DEFAULT, buffer.data() );

	    status = H5Sclose(mspcid);
	    status = H5Sclose(dspcid);
	    status = H5Dclose(dsetid);
	    status = H5Fclose(fileid);
		return WRAPPED_HDF5_SUCCESS;
	}
	else {
		return WRAPPED_HDF5_INCORRECTDIMENSIONS;
	}
}


int h5Hdl::write_contiguous_matrix_i64le_hyperslab( h5iometa const & h5info,
			h5offsets const & offsetinfo, vector<long> const & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };


	//bounds check and consistency buffer has the same length than planed
	if ( buffer.size() == dimssub[0] * dimssub[1] ) {
		//define offset
		hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
		hsize_t count[2] = { dimssub[0], dimssub[1] };
		hsize_t stride[2] = { 1, 1};
		hsize_t block[2] = { 1, 1};
		fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
		dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

		mspcid = H5Screate_simple(2, dimssub, mxdims );
		dspcid = H5Dget_space( dsetid );
		status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
		status = H5Dwrite( dsetid, H5T_STD_I64LE, mspcid, dspcid, H5P_DEFAULT, buffer.data() );

	    status = H5Sclose(mspcid);
	    status = H5Sclose(dspcid);
	    status = H5Dclose(dsetid);
	    status = H5Fclose(fileid);
		return WRAPPED_HDF5_SUCCESS;
	}
	else {
		return WRAPPED_HDF5_INCORRECTDIMENSIONS;
	}
}


int h5Hdl::write_contiguous_matrix_u64le_hyperslab( h5iometa const & h5info,
			h5offsets const & offsetinfo, vector<size_t> const & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };


	//bounds check and consistency buffer has the same length than planed
	if ( buffer.size() == dimssub[0] * dimssub[1] ) {
		//define offset
		hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
		hsize_t count[2] = { dimssub[0], dimssub[1] };
		hsize_t stride[2] = { 1, 1};
		hsize_t block[2] = { 1, 1};
		fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
		dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

		mspcid = H5Screate_simple(2, dimssub, mxdims );
		dspcid = H5Dget_space( dsetid );
		status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
		status = H5Dwrite( dsetid, H5T_STD_U64LE, mspcid, dspcid, H5P_DEFAULT, buffer.data() );

	    status = H5Sclose(mspcid);
	    status = H5Sclose(dspcid);
	    status = H5Dclose(dsetid);
	    status = H5Fclose(fileid);
		return WRAPPED_HDF5_SUCCESS;
	}
	else {
		return WRAPPED_HDF5_INCORRECTDIMENSIONS;
	}
}


int h5Hdl::write_contiguous_matrix_i16le_hyperslab( h5iometa const & h5info,
						h5offsets const & offsetinfo, vector<short> const & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };


	//bounds check and consistency buffer has the same length than planed
	if ( buffer.size() == dimssub[0] * dimssub[1] ) {
		//define offset
		hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
		hsize_t count[2] = { dimssub[0], dimssub[1] };
		hsize_t stride[2] = { 1, 1};
		hsize_t block[2] = { 1, 1};
		fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
		dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

		mspcid = H5Screate_simple(2, dimssub, mxdims );
		dspcid = H5Dget_space( dsetid );
		status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
		status = H5Dwrite( dsetid, H5T_STD_I16LE, mspcid, dspcid, H5P_DEFAULT, buffer.data() );

	    status = H5Sclose(mspcid);
	    status = H5Sclose(dspcid);
	    status = H5Dclose(dsetid);
	    status = H5Fclose(fileid);
		return WRAPPED_HDF5_SUCCESS;
	}
	else {
		return WRAPPED_HDF5_INCORRECTDIMENSIONS;
	}
}


int h5Hdl::write_contiguous_matrix_f32le_hyperslab( h5iometa const & h5info,
						h5offsets const & offsetinfo, vector<float> const & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };


	//bounds check and consistency buffer has the same length than planed
	if ( buffer.size() == dimssub[0] * dimssub[1] ) {
		//define offset
		hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
		hsize_t count[2] = { dimssub[0], dimssub[1] };
		hsize_t stride[2] = { 1, 1};
		hsize_t block[2] = { 1, 1};
		fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
		dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

		mspcid = H5Screate_simple(2, dimssub, mxdims );
		dspcid = H5Dget_space( dsetid );
		status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
		status = H5Dwrite( dsetid, H5T_IEEE_F32LE, mspcid, dspcid, H5P_DEFAULT, buffer.data() );

	    status = H5Sclose(mspcid);
	    status = H5Sclose(dspcid);
	    status = H5Dclose(dsetid);
	    status = H5Fclose(fileid);
		return WRAPPED_HDF5_SUCCESS;
	}
	else {
		return WRAPPED_HDF5_INCORRECTDIMENSIONS;
	}
}


int h5Hdl::write_contiguous_matrix_f32le_hyperslab( h5iometa const & h5info,
						h5offsets const & offsetinfo, float* const buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };

	//bounds check and consistency buffer has the same length than planed
	//define offset
	hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
	hsize_t count[2] = { dimssub[0], dimssub[1] };
	hsize_t stride[2] = { 1, 1};
	hsize_t block[2] = { 1, 1};
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
	dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

	mspcid = H5Screate_simple(2, dimssub, mxdims );
	dspcid = H5Dget_space( dsetid );
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	status = H5Dwrite( dsetid, H5T_IEEE_F32LE, mspcid, dspcid, H5P_DEFAULT, buffer );

	status = H5Sclose(mspcid);
	status = H5Sclose(dspcid);
	status = H5Dclose(dsetid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_contiguous_matrix_f64le_hyperslab( h5iometa const & h5info, h5offsets const & offsetinfo,
		vector<double> const & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };


	//bounds check and consistency buffer has the same length than planed
	if ( buffer.size() == dimssub[0] * dimssub[1] ) {
		//define offset
		hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
		hsize_t count[2] = { dimssub[0], dimssub[1] };
		hsize_t stride[2] = { 1, 1};
		hsize_t block[2] = { 1, 1};
		fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
		dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

		mspcid = H5Screate_simple(2, dimssub, mxdims );
		dspcid = H5Dget_space( dsetid );
		status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
		status = H5Dwrite( dsetid, H5T_IEEE_F64LE, mspcid, dspcid, H5P_DEFAULT, buffer.data() );

	    status = H5Sclose(mspcid);
	    status = H5Sclose(dspcid);
	    status = H5Dclose(dsetid);
	    status = H5Fclose(fileid);
		return WRAPPED_HDF5_SUCCESS;
	}
	else {
		return WRAPPED_HDF5_INCORRECTDIMENSIONS;
	}
}


int h5Hdl::read_contiguous_matrix_u8le_hyperslab( h5iometa const & h5info, h5offsets const & offsetinfo,
		vector<unsigned char> & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };

	hsize_t nalloc = dimssub[0]*dimssub[1];
	unsigned char* rbuf = NULL;
	try {
		rbuf = new unsigned char[nalloc];
	}
	catch (bad_alloc &hdfcroak) {
		return WRAPPED_HDF5_RBUFALLOC_FAILED;
	}

	//sufficent space to writebounds check and consistency buffer has the same length than planed
	hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
	hsize_t count[2] = { dimssub[0], dimssub[1] };
	hsize_t stride[2] = { 1, 1};
	hsize_t block[2] = { 1, 1};
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
	dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

	mspcid = H5Screate_simple(2, dimssub, mxdims );
	dspcid = H5Dget_space( dsetid );
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	status = H5Dread( dsetid, H5T_STD_U8LE, mspcid, dspcid, H5P_DEFAULT, rbuf );
	if ( status < 0 ) {
		return WRAPPED_HDF5_READFAILED;
	}

	//##MK::pre-existent buffer! for OIM_INDEXING buffer = vector<float>( nalloc, 0.f );
	buffer = vector<unsigned char>( nalloc, 0x00 );
	for( size_t i = 0; i < nalloc; ++i ) {
		buffer[i] = rbuf[i];
	}

	delete [] rbuf; rbuf = NULL;

	status = H5Sclose(mspcid);
	status = H5Sclose(dspcid);
	status = H5Dclose(dsetid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::read_contiguous_matrix_u16le_hyperslab( h5iometa const & h5info, h5offsets const & offsetinfo,
		vector<unsigned short> & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };

	hsize_t nalloc = dimssub[0]*dimssub[1];
	unsigned short* rbuf = NULL;
	try {
		rbuf = new unsigned short[nalloc];
	}
	catch (bad_alloc &hdfcroak) {
		return WRAPPED_HDF5_RBUFALLOC_FAILED;
	}

	//sufficent space to writebounds check and consistency buffer has the same length than planed
	hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
	hsize_t count[2] = { dimssub[0], dimssub[1] };
	hsize_t stride[2] = { 1, 1};
	hsize_t block[2] = { 1, 1};
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
	dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

	mspcid = H5Screate_simple(2, dimssub, mxdims );
	dspcid = H5Dget_space( dsetid );
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	status = H5Dread( dsetid, H5T_STD_U16LE, mspcid, dspcid, H5P_DEFAULT, rbuf );
	if ( status < 0 ) {
		return WRAPPED_HDF5_READFAILED;
	}

	//##MK::pre-existent buffer! for OIM_INDEXING buffer = vector<float>( nalloc, 0.f );
	buffer = vector<unsigned short>( nalloc, 0 );
	for( size_t i = 0; i < nalloc; ++i ) {
		buffer[i] = rbuf[i];
	}

	delete [] rbuf; rbuf = NULL;

	status = H5Sclose(mspcid);
	status = H5Sclose(dspcid);
	status = H5Dclose(dsetid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::read_contiguous_matrix_i32le_hyperslab( h5iometa const & h5info, h5offsets const & offsetinfo,
		vector<int> & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };

	hsize_t nalloc = dimssub[0]*dimssub[1];
	int* rbuf = NULL;
	try {
		rbuf = new int[nalloc];
	}
	catch (bad_alloc &hdfcroak) {
		return WRAPPED_HDF5_RBUFALLOC_FAILED;
	}

	//sufficent space to writebounds check and consistency buffer has the same length than planed
	hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
	hsize_t count[2] = { dimssub[0], dimssub[1] };
	hsize_t stride[2] = { 1, 1};
	hsize_t block[2] = { 1, 1};
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
	dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

	mspcid = H5Screate_simple(2, dimssub, mxdims );
	dspcid = H5Dget_space( dsetid );
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	status = H5Dread( dsetid, H5T_STD_I32LE, mspcid, dspcid, H5P_DEFAULT, rbuf );
	if ( status < 0 ) {
		return WRAPPED_HDF5_READFAILED;
	}

	//##MK::pre-existent buffer! for OIM_INDEXING buffer = vector<float>( nalloc, 0.f );
	buffer = vector<int>( nalloc, 0 );
	for( size_t i = 0; i < nalloc; ++i ) {
		buffer[i] = rbuf[i];
	}

	delete [] rbuf; rbuf = NULL;

	status = H5Sclose(mspcid);
	status = H5Sclose(dspcid);
	status = H5Dclose(dsetid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::read_contiguous_matrix_f32le_hyperslab( h5iometa const & h5info, h5offsets const & offsetinfo,
		vector<float> & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };

	hsize_t nalloc = dimssub[0]*dimssub[1];
	float* rbuf = NULL;
	try {
		rbuf = new float[nalloc];
		//buffer = vector<float>( nalloc, 0.f );
	}
	catch (bad_alloc &hdfcroak) {
		return WRAPPED_HDF5_RBUFALLOC_FAILED;
	}

	//sufficent space to writebounds check and consistency buffer has the same length than planed
	hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
	hsize_t count[2] = { dimssub[0], dimssub[1] };
	hsize_t stride[2] = { 1, 1};
	hsize_t block[2] = { 1, 1};
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
	dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

	mspcid = H5Screate_simple(2, dimssub, mxdims );
	dspcid = H5Dget_space( dsetid );
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	status = H5Dread( dsetid, H5T_IEEE_F32LE, mspcid, dspcid, H5P_DEFAULT, rbuf ); //&(buffer.front()) ); //&buffer.at(0) );
	if ( status < 0 ) {
		return WRAPPED_HDF5_READFAILED;
	}

	buffer = vector<float>( nalloc, 0.f );
	for( size_t i = 0; i < nalloc; ++i ) {
		buffer[i] = rbuf[i];
	}

	delete [] rbuf; rbuf = NULL;

	status = H5Sclose(mspcid);
	status = H5Sclose(dspcid);
	status = H5Dclose(dsetid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::read_contiguous_matrix_u32le_hyperslab( h5iometa const & h5info, h5offsets const & offsetinfo,
		vector<unsigned int> & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };

	hsize_t nalloc = dimssub[0]*dimssub[1];
	unsigned int* rbuf = NULL;
	try {
		rbuf = new unsigned int[nalloc];
		//buffer = vector<float>( nalloc, 0.f );
	}
	catch (bad_alloc &hdfcroak) {
		return WRAPPED_HDF5_RBUFALLOC_FAILED;
	}

	//sufficent space to writebounds check and consistency buffer has the same length than planed
	hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
	hsize_t count[2] = { dimssub[0], dimssub[1] };
	hsize_t stride[2] = { 1, 1};
	hsize_t block[2] = { 1, 1};
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
	dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

	mspcid = H5Screate_simple(2, dimssub, mxdims );
	dspcid = H5Dget_space( dsetid );
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	status = H5Dread( dsetid, H5T_STD_U32LE, mspcid, dspcid, H5P_DEFAULT, rbuf ); //&(buffer.front()) ); //&buffer.at(0) );
	if ( status < 0 ) {
		return WRAPPED_HDF5_READFAILED;
	}

	buffer = vector<unsigned int>( nalloc, 0.f );
	for( size_t i = 0; i < nalloc; ++i ) {
		buffer[i] = rbuf[i];
	}

	delete [] rbuf; rbuf = NULL;

	status = H5Sclose(mspcid);
	status = H5Sclose(dspcid);
	status = H5Dclose(dsetid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::init_chunked_matrix_f64le( const string h5fn, const string dsetnm, const size_t nr, const size_t nc )
{
	//see additional comments for *_u32le version of these functions...
	nrows = nr; //real dataset size in number of elements on 1d implicit buffer of unsigned int is nr*nc
	ncols = nc;
	BytesPerChunk = static_cast<size_t>(ncols*1024*1024); //MK::use default chunk size 1MB, ##MK::performance study
	//##MK::value check needs to be integer multiple of sizeof(unsigned int)*ncols !
	RowsPerChunk = BytesPerChunk / (sizeof(double) * ncols);
	ColsPerChunk = ncols;
	RowsColsPerChunk = RowsPerChunk*ColsPerChunk;
	NChunks = static_cast<size_t>( ceil(static_cast<double>(nrows*ncols) / static_cast<double>(RowsColsPerChunk)) );

cout << "HDF5 RealDataRows/Cols_BytesPerChunk_Rows/Cols/Rows*ColsPerChunk_NChunksTotal\t\t" << nrows << ";" << ncols << ";" << BytesPerChunk << ";" << RowsPerChunk << ";" << ColsPerChunk << ";" << RowsColsPerChunk << ";" << NChunks << endl;

	//allocate a read/write buffer to pass data to the H5 library call functions
	if ( f64le_buf != NULL ) {
		delete [] f64le_buf;
		f64le_buf = NULL;
	}
	else {
		try {
			f64le_buf = new double[RowsPerChunk*ColsPerChunk];
		}
		catch (bad_alloc &h5croak) {
			cerr << "Allocation error for write buffer in init_chunked_matrix_f64le HDF5" << "\n";
			return WRAPPED_HDF5_ALLOCERR;
		}
	}

	//open an existent H5 file with read/write access
	fileid = H5Fopen( h5fn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);

	//initialize in this file a chunked data set in an existent group

	//start processing chunks into buffer successively and write to HDF5 file
	dims[0] = RowsPerChunk; 		dims[1] = ColsPerChunk;
	maxdims[0] = H5S_UNLIMITED;		maxdims[1] = H5S_UNLIMITED;
	offset[0] = 0;					offset[1] = 0;
	dimsnew[0] = 0;					dimsnew[1] = ColsPerChunk;

	dspcid = H5Screate_simple(2, dims, maxdims);
	cparms = H5Pcreate(H5P_DATASET_CREATE);
	status = H5Pset_chunk( cparms, 2, dims);

	double fillval = HDF5_F64LE_DUMMYVALUE;
	status = H5Pset_fill_value(cparms, H5T_IEEE_F64LE, &fillval );
	dsetid = H5Dcreate2(fileid, dsetnm.c_str(), H5T_IEEE_F64LE, dspcid, H5P_DEFAULT, cparms, H5P_DEFAULT);

	//fill read/write buffer with dummy values which the preceeding write steps can parse
	for(size_t i = 0; i < RowsColsPerChunk; ++i) {
		f64le_buf[i] = HDF5_F64LE_DUMMYVALUE;
	}
	//and use in combination with the position offsets to always complete first individual chunks,
	//thereafter write them to file and refresh the read/write buffer with dummy values to avoid
	//having to read the file content for any preceeding thread whose data portion interval falls arbitrarily
	//on a chunk boundary
	TotalWriteHere = 0;
	TotalWritePortion = RowsColsPerChunk;
	TotalWriteAllUsed = nrows*ncols;
	TotalWriteAllChunk = NChunks*RowsColsPerChunk; //so many implicitly coded unsigned int to write in total (including potential buffer of trailing dummies)

	//do not close any resource handlers or the file, in what follows we will add data portions per thread
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_chunked_matrix_f64le( const string h5fn, const string dsetnm, vector<double> const & buffer )
{
	//see additional comments for *_u32le version of this functions...
	size_t LocalCompleted = 0; //how much of feed buffer already processed
	size_t LocalTotal = buffer.size(); //total size of the feed buffer

	size_t BufferWriteHere = 0; //where to start filling in h5 write buffer u32le_buf
	size_t BufferWriteNow = 0; //how many remaining fillable before u32le_buf is full and needs a flush

	do { //loop for as many chunks as fillable with feed buffer content
		BufferWriteHere = TotalWriteHere % RowsColsPerChunk;
		BufferWriteNow = RowsColsPerChunk;

		if ( (BufferWriteHere+(LocalTotal-LocalCompleted)) < RowsColsPerChunk )
			BufferWriteNow = BufferWriteHere + LocalTotal-LocalCompleted;

		//fill buffer
cout << "1::BufferWriteHere;Now;LocalCompleted;TotalWriteHere\t\t" << BufferWriteHere << ";" << BufferWriteNow << ";" << LocalCompleted << ";" << TotalWriteHere << endl;
		for( size_t w = BufferWriteHere; w < BufferWriteNow; w++ ) {
			f64le_buf[w] = buffer.at(LocalCompleted);
			LocalCompleted++;
			TotalWriteHere++;
		}
cout << "2::BufferWriteHere;Now;LocalCompleted;TotalWriteHere\t\t" << BufferWriteHere << ";" << BufferWriteNow << ";" << LocalCompleted << ";" << TotalWriteHere << endl;

		//potentially flush buffer into H5 file
		if ( TotalWriteHere % RowsColsPerChunk == 0 ) {
			//##MK::write#####
cout << "3::Writing a full chunk" << endl;
			dimsnew[0] = dimsnew[0] + RowsPerChunk;
			//second dimension of dimsnew needs to remain as is!
			//the chunk is always written completely, regardless how many values physically relevant or not!
			status = H5Dset_extent(dsetid, dimsnew);
			fspcid = H5Dget_space(dsetid);
			status = H5Sselect_hyperslab(fspcid, H5S_SELECT_SET, offset, NULL, dims, NULL);
			status = H5Dwrite(dsetid, H5T_IEEE_F64LE, dspcid, fspcid, H5P_DEFAULT, f64le_buf);
			offset[0] = offset[0] + RowsPerChunk;
			//second dimension of offset needs to remain as is!

			//refresh buffer with dummies completely
			for ( size_t i = 0; i < RowsColsPerChunk; ++i )
				f64le_buf[i] = HDF5_F64LE_DUMMYVALUE;
		}

	} while ( LocalCompleted < LocalTotal );
	//processing of feed buffer done

cout << "--->Local done\t\t" << TotalWriteHere << "\t\t" << TotalWriteAllUsed << "\t\t" << TotalWriteAllChunk << endl;

	//dont forget to pass potentially remaining part of the h5 write buffer to file
	if ( TotalWriteHere >= TotalWriteAllUsed ) { //everything which was planned in buffer
		//write h5 buffer out last time if not already happened
		if ( TotalWriteHere % RowsColsPerChunk != 0 ) {
cout << "4::Writing last chunk" << endl;

			dimsnew[0] = dimsnew[0] + RowsPerChunk;
			//second dimension of dimsnew needs to remain as is!
			//the chunk is always written completely, regardless how many values physically relevant or not!
			status = H5Dset_extent(dsetid, dimsnew);
			fspcid = H5Dget_space(dsetid);
			status = H5Sselect_hyperslab(fspcid, H5S_SELECT_SET, offset, NULL, dims, NULL);
			status = H5Dwrite(dsetid, H5T_IEEE_F64LE, dspcid, fspcid, H5P_DEFAULT, f64le_buf);
			offset[0] = offset[0] + RowsPerChunk;
			//second dimension of offset needs to remain as is!
		}
	}

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_attribute_string( const string dsetnm, vector<pair<string,string>> const & attr )
{
	//add a number of string attributes to an already existent but closed dataset

	//##MK::avoid I/O overhead by opening the file here

	//https://bitbucket.hdfgroup.org/projects/HDFFV/repos/hdf5-examples/browse/1_10/C/H5T/h5ex_t_stringatt.c
	for( auto it = attr.begin(); it != attr.end(); it++ ) {
	//if ( attr.size() > 0 ) {
		//##MK::write also at(>0) entries
		//string key = attr.at(0).first; //for instance "SIUnit"
		//string val = attr.at(0).second; //for instance "nm"
		string key = it->first;
		string val = it->second;

		//write only nonempty attribute strings
		if ( key.length() > 0 && val.length() > 0 ) {
cout << "Writing attribute key/val\t\t" << key << "\t\t" << val << "\n";

			//C-style ASCII conformant
			size_t SDIM = val.length()+1;
			char* cstr = new char[SDIM]; //MK::no +1 because attr are without null/newline separator;
			strcpy(cstr, val.c_str());

			fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
			dsetid = H5Dopen2( fileid, dsetnm.c_str(), H5P_DEFAULT );
			dspcid = H5Dget_space( dsetid );

			//Create file and memory datatypes. For this example we will save
			//the strings as FORTRAN strings, therefore they do not need space
			//for the null terminator in the file
			ftypid = H5Tcopy(H5T_FORTRAN_S1);
			status = H5Tset_size(ftypid, SDIM-1);
cout << "Setting attribute size " << status << "\n";
		    mtypid = H5Tcopy(H5T_C_S1);
		    status = H5Tset_size(mtypid, SDIM); //MK::no null/newline terminator -1);
cout << "Setting attribute mtypid size " << status << "\n";
		    //create the attribute and write the string data to it
		    //##MK::here exemplified using the first token only of attr
		    attrid = H5Acreate(dsetid, key.c_str(), ftypid, dspcid, H5P_DEFAULT, H5P_DEFAULT);
		    status = H5Awrite (attrid, mtypid, cstr);
cout << "Writing attribute " << status << "\n";

			delete cstr; cstr = NULL;

			//release resources
			status = H5Aclose(attrid);
			status = H5Tclose(mtypid);
			status = H5Tclose(ftypid);
			status = H5Sclose(dspcid);
			status = H5Dclose(dsetid);
			status = H5Fclose(fileid);

			//##MK::get out here, debug return WRAPPED_HDF5_SUCCESS;
		}
		else {
cout << "Skipping empty attribute key/vals" << "\n";
		}
    }

	//##MK::avoid I/O overhead by closing the file here

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::reset_chunked_matrix_f64le_aftercompletion()
{
cout << "Resetting f64le" << endl;

	if ( f64le_buf != NULL ) {
		delete [] f64le_buf;
		f64le_buf = NULL;
	}

	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Sclose(fspcid);
	status = H5Pclose(cparms);
	status = H5Fclose(fileid);

	return WRAPPED_HDF5_SUCCESS;
}


/*
bool h5Hdl::read_reconstruction_from_apth5( const string fn, vector<p3dm1> & out )
{
	int status = 0;
	h5iometa ifo_recon = h5iometa();
	h5offsets offs_recon = h5offsets();

	if ( fn.substr(fn.length()-6) != ".apth5" ) {
		cerr << "The InputfileReconstruction " << fn << " has not the proper apth5 file extension!" << "\n";
		return false;
	}

	//probe existence of xyz coordinates array in the input file and get dimensions
	h5resultsfn = fn;
	if ( query_contiguous_matrix_dims( PARAPROBE_VOLRECON_XYZ, offs_recon ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the XYZ data in the InputfileReconstruction!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_VOLRECON_XYZ is a " << offs_recon.nrows() << " rows " << offs_recon.ncols() << " matrix" << "\n";
	}

	if ( offs_recon.nrows() < 1 || offs_recon.ncols() != PARAPROBE_VOLRECON_XYZ_NCMAX ) {
		cerr << "PARAPROBE_VOLRECON_XYZ is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo_recon = h5iometa( PARAPROBE_VOLRECON_XYZ, offs_recon.nrows(), offs_recon.ncols() );
	vector<float> f32;
	status = read_contiguous_matrix_f32le_hyperslab( ifo_recon, offs_recon, f32 );
cout << "PARAPROBE_VOLRECON_XYZ read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_VOLRECON_XYZ on Inputfile failed!" << "\n";
		return false;
	}

	try {
		out = vector<p3dm1>();
		out.reserve( offs_recon.nrows() ); //one p3dm1 object for every ion
//MK::here point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading PARAPROBE_VOLRECON_XYZ!" << "\n"; return false;
	}

	for( size_t i = 0; i < f32.size(); i += 3 ) {
		out.push_back( p3dm1( f32.at(i+0), f32.at(i+1), f32.at(i+2), 0 ) );
	}
	f32 = vector<float>();

	return true;
}
*/


bool h5Hdl::read_trianglehull_from_apth5( const string fn, vector<tri3d> & out )
{
	int status = 0;
	h5iometa ifo_hull = h5iometa();
	h5offsets offs_hull = h5offsets();

	h5resultsfn = fn;
	if ( query_contiguous_matrix_dims( PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ, offs_hull ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ in the InputfileTriangleHull" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ is a " << offs_hull.nrows() << " rows " << offs_hull.ncols() << " columns matrix" << "\n";
	}

	if ( offs_hull.nrows() < 1 || offs_hull.ncols() != PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ_NCMAX ) {
		cerr << "PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::check to block-wise read
	ifo_hull = h5iometa( PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ, offs_hull.nrows(), offs_hull.ncols() );
	vector<float> f32;
	status = read_contiguous_matrix_f32le_hyperslab( ifo_hull, offs_hull, f32 );
cout << "PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ on InputfileTriangleHull failed" << "\n";
		return false;
	}

	try {
		out = vector<tri3d>();
		out.reserve( offs_hull.nrows() );
//##MK::point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading triangle hull positions" << "\n";
		return false;
	}

	for( size_t i = 0; i < f32.size(); i += 9 ) {
		out.push_back( tri3d(
				f32.at(i+0), f32.at(i+1), f32.at(i+2),
				f32.at(i+3), f32.at(i+4), f32.at(i+5),
				f32.at(i+6), f32.at(i+7), f32.at(i+8) ) );
	}
	f32 = vector<float>();

	return true;
}


bool h5Hdl::read_voxelized_from_apth5( const string fn, vxlizer & out )
{
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	h5resultsfn = fn;
	//read vxlbox
	out.vxlgrid = sqb();
	if ( query_contiguous_matrix_dims( PARAPROBE_SURF_VXLBOX_META_VXL_NXYZ, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_SURF_VXLBOX_META_VXL_NXYZ!" << "\n"; return false;
	}
	else {
		cout << "PARAPROBE_SURF_VXLBOX_META_VXL_NXYZ is a " << offs.nrows() << " rows " << offs.ncols() << " columns matrix" << "\n";
	}
	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_SURF_VXLBOX_META_VXL_NXYZ_NCMAX ) {
		cerr << "PARAPROBE_SURF_VXLBOX_META_VXL_NXYZ is either empty and/or has unexpected size!" << "\n"; return false;
	}
	ifo = h5iometa( PARAPROBE_SURF_VXLBOX_META_VXL_NXYZ, offs.nrows(), offs.ncols() );
	vector<unsigned int> u32;
	status = read_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_VXLBOX_META_VXL_NXYZ read failed " << status << "\n"; return false;
	}
	cout << "PARAPROBE_SURF_VXLBOX_META_VXL_NXYZ read " << status << "\n";

	out.vxlgrid.nx = u32.at(0);
	out.vxlgrid.ny = u32.at(1);
	out.vxlgrid.nz = u32.at(2);
	out.vxlgrid.nxy = out.vxlgrid.nx * out.vxlgrid.ny;
	out.vxlgrid.nxyz = out.vxlgrid.nx * out.vxlgrid.ny * out.vxlgrid.nz;
	u32 = vector<unsigned int>();

	//bounds mi,mx
	if ( query_contiguous_matrix_dims( PARAPROBE_SURF_VXLBOX_META_VXL_MIMX, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_SURF_VXLBOX_META_VXL_MIMX!" << "\n"; return false;
	}
	else {
		cout << "PARAPROBE_SURF_VXLBOX_META_VXL_MIMX is a " << offs.nrows() << " rows " << offs.ncols() << " columns matrix" << "\n";
	}
	if ( offs.nrows() != 3 || offs.ncols() != PARAPROBE_SURF_VXLBOX_META_VXL_MIMX_NCMAX ) {
		cerr << "PARAPROBE_SURF_VXLBOX_META_VXL_MIMX is either empty and/or has unexpected size!" << "\n"; return false;
	}
	ifo = h5iometa( PARAPROBE_SURF_VXLBOX_META_VXL_MIMX, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_VXLBOX_META_VXL_MIMX read failed " << status << "\n"; return false;
	}
	cout << "PARAPROBE_SURF_VXLBOX_META_VXL_MIMX read " << status << "\n";

	out.vxlgrid.box = aabb3d( 	f32.at(0), f32.at(1),
								f32.at(2), f32.at(3),
								f32.at(4), f32.at(5) );
	out.vxlgrid.box.scale();
	f32 = vector<float>();

	//load width
	if ( query_contiguous_matrix_dims( PARAPROBE_SURF_VXLBOX_META_VXL_WIDTH, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_SURF_VXLBOX_META_VXL_WIDTH!" << "\n"; return false;
	}
	else {
		cout << "PARAPROBE_SURF_VXLBOX_META_VXL_WIDTH is a " << offs.nrows() << " rows " << offs.ncols() << " columns matrix" << "\n";
	}
	if ( offs.nrows() != 1 || offs.ncols() != PARAPROBE_SURF_VXLBOX_META_VXL_WIDTH_NCMAX ) {
		cerr << "PARAPROBE_SURF_VXLBOX_META_VXL_WIDTH is either empty and/or has unexpected size!" << "\n"; return false;
	}
	ifo = h5iometa( PARAPROBE_SURF_VXLBOX_META_VXL_WIDTH, offs.nrows(), offs.ncols() );
	status = read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_VXLBOX_META_VXL_WIDTH read failed " << status << "\n"; return false;
	}
	cout << "PARAPROBE_SURF_VXLBOX_META_VXL_WIDTH read " << status << "\n";

	out.vxlgrid.width = f32.at(0);
	f32 = vector<float>();

	//load states
	if ( query_contiguous_matrix_dims( PARAPROBE_SURF_VXLBOX_RES_VXL_STATE, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_SURF_VXLBOX_RES_VXL_STATE!" << "\n"; return false;
	}
	else {
		cout << "PARAPROBE_SURF_VXLBOX_RES_VXL_STATE is a " << offs.nrows() << " rows " << offs.ncols() << " columns matrix" << "\n";
	}

	if ( offs.nrows() != out.vxlgrid.nxyz || offs.nrows() < 1 || offs.ncols() != PARAPROBE_SURF_VXLBOX_RES_VXL_STATE_NCMAX ) {
		cerr << "PARAPROBE_SURF_VXLBOX_RES_VXL_STATE is either empty and/or has unexpected size!" << "\n"; return false;
	}
	//read the content and transfer to working array
	//##MK::check to block-wise read
	ifo = h5iometa( PARAPROBE_SURF_VXLBOX_RES_VXL_STATE, offs.nrows(), offs.ncols() );
	vector<unsigned char> uc8;
	status = read_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8 );
cout << "PARAPROBE_SURF_VXLBOX_RES_VXL_STATE read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_SURF_VXLBOX_RES_VXL_STATE read failed " << status << "\n"; return false;
	}

	if ( out.IsInside != NULL || out.IsSurface != NULL || out.IsVacuum != NULL ) {
		cerr << "out bitfields have already been allocated!" << "\n"; return false;
	}

	bool* img = NULL;
	try {
		img = new bool[out.vxlgrid.nxyz]; //allocate on heap and feedback
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during allocating out.IsInside state" << "\n"; return false;
	}

	for( size_t i = 0; i < uc8.size(); i += PARAPROBE_SURF_VXLBOX_RES_VXL_STATE_NCMAX ) {
		if ( uc8.at(i) == 0xFF ) { //IS_INSIDE
			img[i] = 0xFF;
		}
		else {
			img[i] = 0x00;
		}
	}
	uc8 = vector<unsigned char>();

	out.IsInside = img;
	//##MK::other fields not changed

	return true;
}


bool h5Hdl::read_xyz_from_apth5( const string fn, vector<p3d> & out )
{
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	if ( fn.substr(fn.length()-3) != ".h5" ) { //".apth5" ) {
		cerr << "The InputfileReconstruction " << fn << " has not the proper .h5 file extension!" << "\n";
		return false;
	}

	//probe existence of xyz coordinates array in the input file and get dimensions
	h5resultsfn = fn;
	if ( query_contiguous_matrix_dims( PARAPROBE_SYNTH_VOLRECON_RES_XYZ, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the XYZ data in the InputfileReconstruction!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_SYNTH_VOLRECON_RES_XYZ is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_SYNTH_VOLRECON_RES_XYZ_NCMAX ) {
		cerr << "PARAPROBE_SYNTH_VOLRECON_RES_XYZ is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_RES_XYZ, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_SYNTH_VOLRECON_RES_XYZ on Inputfile failed!" << "\n"; return false;
	}
cout << "PARAPROBE_SYNTH_VOLRECON_RES_XYZ read " << status << "\n";

	try {
		out = vector<p3d>();
		out.reserve( offs.nrows() ); //one p3dm1 object for every ion
//MK::here point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading PARAPROBE_SYNTH_VOLRECON_RES_XYZ!" << "\n"; return false;
	}

	for( size_t i = 0; i < f32.size(); i += 3 ) {
		out.push_back( p3d( f32.at(i+0), f32.at(i+1), f32.at(i+2) ) );
	}
	f32 = vector<float>();
	return true;
}


bool h5Hdl::read_detxy_from_apth5( const string fn, vector<p2dm1> & out )
{
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	if ( fn.substr(fn.length()-3) != ".h5" ) {
		cerr << "The Inputfile " << fn << " has not the proper .h5 file extension!" << "\n";
		return false;
	}

	//probe existence of detxy coordinates array in the input file and get dimensions
	h5resultsfn = fn;
	if ( query_contiguous_matrix_dims( PARAPROBE_TRANSCODE_DETSPACE_RES_XY, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the XY data in the Inputfile!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_TRANSCODE_DETSPACE_RES_XY is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_TRANSCODE_DETSPACE_RES_XY_NCMAX ) {
		cerr << "PARAPROBE_TRANSCODE_DETSPACE_RES_XY is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_TRANSCODE_DETSPACE_RES_XY, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_TRANSCODE_DETSPACE_RES_XY on Inputfile failed!" << "\n"; return false;
	}
cout << "PARAPROBE_TRANSCODE_DETSPACE_RES_XY read " << status << "\n";

	try {
		out = vector<p2dm1>();
		out.reserve( offs.nrows() ); //one p3dm1 object for every ion
//MK::here point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading PARAPROBE_TRANSCODE_DETSPACE_RES_XY!" << "\n"; return false;
	}

	unsigned int ionID = 0;
	for( size_t i = 0; i < f32.size(); i += 2 ) {
		out.push_back( p2dm1( f32.at(i+0), f32.at(i+1), ionID ) );
		ionID++;
	}
	f32 = vector<float>();
	return true;
}


bool h5Hdl::read_voltages_from_apth5( const string fn, vector<voltm1> & out )
{
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	if ( fn.substr(fn.length()-3) != ".h5" ) {
		cerr << "The Inputfile " << fn << " has not the proper .h5 file extension!" << "\n";
		return false;
	}

	//probe existence of Vdu data array and get dimensions
	h5resultsfn = fn;
	if ( query_contiguous_matrix_dims( PARAPROBE_TRANSCODE_VOLTCURVES_RES_VDC, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the VDC data in the Inputfile!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_TRANSCODE_VOLTCURVES_RES_VDC is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_TRANSCODE_VOLTCURVES_RES_VDC_NCMAX ) {
		cerr << "PARAPROBE_TRANSCODE_VOLTCURVES_RES_VDC is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_TRANSCODE_VOLTCURVES_RES_VDC, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_TRANSCODE_VOLTCURVES_RES_VDC on Inputfile failed!" << "\n"; return false;
	}
cout << "PARAPROBE_TRANSCODE_VOLTCURVES_RES_VDC read " << status << "\n";

	try {
		out = vector<voltm1>();
		out.reserve( offs.nrows() ); //one p3dm1 object for every ion
//MK::here point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading PARAPROBE_TRANSCODE_DETSPACE_RES_XY!" << "\n"; return false;
	}

	unsigned int ionID = 0;
	for( size_t i = 0; i < f32.size(); i++ ) {
		out.push_back( voltm1( f32.at(i+0), 0.f, i ) );
	}
	f32 = vector<float>();

	//load Vpu
	if ( query_contiguous_matrix_dims( PARAPROBE_TRANSCODE_VOLTCURVES_RES_VPU, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the VPU data in the Inputfile!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_TRANSCODE_VOLTCURVES_RES_VPU is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_TRANSCODE_VOLTCURVES_RES_VPU_NCMAX ) {
		cerr << "PARAPROBE_TRANSCODE_VOLTCURVES_RES_VPU is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	if ( offs.nrows() != out.size() ) {
		cerr << "VDU and VPU arrays have different size this is unexpected!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_TRANSCODE_VOLTCURVES_RES_VPU, offs.nrows(), offs.ncols() );
	status = read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_TRANSCODE_VOLTCURVES_RES_VPU on Inputfile failed!" << "\n"; return false;
	}
cout << "PARAPROBE_TRANSCODE_VOLTCURVES_RES_VPU read " << status << "\n";

	for( size_t i = 0; i < f32.size(); i++ ) {
		if ( out.at(i).m == static_cast<unsigned int>(i) ) {
			out.at(i).Vpu = f32.at(i);
		}
		else {
			cerr << "Interleaving of PARAPROBE_TRANSCODE_VOLTCURVES_RES_VPU with PARAPROBE_TRANSCODE_VOLTCURVES_RES_VDC failed for ion " << i << "\n";
			return false;
		}
	}
	f32 = vector<float>();
	return true;
}


bool h5Hdl::read_m2q_from_apth5( const string fn, vector<apt_real> & out )
{
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	if ( fn.substr(fn.length()-3) != ".h5" ) {
		cerr << "The InputfileReconstruction " << fn << " has not the proper .h5 file extension!" << "\n";
		return false;
	}

	//probe existence of xyz coordinates array in the input file and get dimensions
	h5resultsfn = fn;
	if ( query_contiguous_matrix_dims( PARAPROBE_SYNTH_VOLRECON_RES_MQ, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the MQ data in the InputfileReconstruction!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_VOLRECON_MQ is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_SYNTH_VOLRECON_RES_MQ_NCMAX ) {
		cerr << "PARAPROBE_SYNTH_VOLRECON_RES_MQ is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_RES_MQ, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
cout << "PARAPROBE_SYNTH_VOLRECON_RES_MQ read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SYNTH_VOLRECON_RES_MQ on Inputfile failed!" << "\n";
		return false;
	}

	try {
		out = vector<apt_real>();
		out.reserve( offs.nrows() ); //one p3dm1 object for every ion
//MK::here point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading PARAPROBE_SYNTH_VOLRECON_RES_MQ!" << "\n"; return false;
	}

	for( auto it = f32.begin(); it != f32.end(); it++ ) {
		out.push_back( *it );
	}
	f32 = vector<float>();
	return true;
}



bool h5Hdl::read_iontypes_from_apth5( const string fn, vector<rangedIontype> & out )
{
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	//temporary container before ion types get filtered to collate 24Mg+ and 25Mg+ to a single ion type
	vector<rangedIontype> tmp_out;

	if ( fn.substr(fn.length()-3) != ".h5" ) {
		cerr << "The InputfileReconstruction " << fn << " has not the proper .h5 file extension!" << "\n";
		return false;
	}

	//probe existence of iontype IDs array in the input file and get dimensions
	h5resultsfn = fn;
	if ( query_contiguous_matrix_dims( PARAPROBE_RANGER_META_TYPID_DICT_ID, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_RANGER_META_TYPID_DICT_ID in InputfileReconstruction!" << "\n"; return false;
	}
	else {
		cout << "PARAPROBE_RANGER_META_TYPID_DICT_ID is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX ) {
		cerr << "PARAPROBE_RANGER_META_TYPID_DICT_ID is either empty and/or has unexpected size!" << "\n"; return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_RANGER_META_TYPID_DICT_ID, offs.nrows(), offs.ncols() );
	vector<unsigned char> uc8;
	status = read_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8 );
cout << "PARAPROBE_RANGER_META_TYPID_DICT_ID read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_RANGER_META_TYPID_DICT_ID on InputfileReconstruction failed!" << "\n"; return false;
	}

	try {
		tmp_out = vector<rangedIontype>( uc8.size()/PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX, rangedIontype() ); //every row an ion
		//MK::here point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading PARAPROBE_RANGER_META_TYPID_DICT!" << "\n"; return false;
	}

	for( size_t r = 0; r < offs.nrows(); r++ ) {
		tmp_out.at(r).id = static_cast<unsigned int>((int) uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX+0));
	}
	uc8 = vector<unsigned char>();
	
	//read elemental composition of these iontypes
	if ( query_contiguous_matrix_dims( PARAPROBE_RANGER_META_TYPID_DICT_WHAT, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_RANGER_META_TYPID_DICT_WHAT in InputfileReconstruction!" << "\n"; return false;
	}
	else {
		cout << "PARAPROBE_RANGER_META_TYPID_DICT_WHAT is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX ) {
		cerr << "PARAPROBE_RANGER_META_TYPID_DICT_WHAT is either empty and/or has unexpected size!" << "\n"; return false;
	}
	if ( offs.nrows() != tmp_out.size() ) { //every rangedIontype
		cerr << "PARAPROBE_RANGER_META_TYPID_DICT_WHAT specifies unexpectedly a different number of rangedIontypes than _DICT_ID!" << "\n"; return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_RANGER_META_TYPID_DICT_WHAT, offs.nrows(), offs.ncols() );
	status = read_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8 );
cout << "PARAPROBE_RANGER_META_TYPID_DICT_WHAT read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_RANGER_META_TYPID_DICT_WHAT on InputfileReconstruction failed!" << "\n"; return false;
	}

	//last entry for the ion type tells us how many ranges are distinguished for this type
	for( size_t r = 0; r < offs.nrows(); r++ ) {
		tmp_out.at(r).strct = evapion3( uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX+0),
									uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX+1),
									uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX+2),
									uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX+3),
									uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX+4),
									uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX+5),
									uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX+6),
									uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX+7)  );
	}	
	uc8 = vector<unsigned char>();
	
	//read mass2charge ranges for those ions
	if ( query_contiguous_matrix_dims( PARAPROBE_RANGER_META_TYPID_DICT_IVAL, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_RANGER_META_TYPID_DICT_IVAL in InputfileReconstruction!" << "\n"; return false;
	}
	else {
		cout << "PARAPROBE_RANGER_META_TYPID_DICT_WHAT is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX ) {
		cerr << "PARAPROBE_RANGER_META_TYPID_DICT_IVAL is either empty and/or has unexpected size!" << "\n"; return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_RANGER_META_TYPID_DICT_IVAL, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
cout << "PARAPROBE_RANGER_META_TYPID_DICT_IVAL read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_RANGER_META_TYPID_DICT_IVAL on InputfileReconstruction failed!" << "\n"; return false;
	}
	//how ever we also need to know to which rangedIontypes these mass2charge interval bounds belong
	//read elemental composition of these iontypes
	if ( query_contiguous_matrix_dims( PARAPROBE_RANGER_META_TYPID_DICT_ASSN, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_RANGER_META_TYPID_DICT_ASSN in InputfileReconstruction!" << "\n"; return false;
	}
	else {
		cout << "PARAPROBE_RANGER_META_TYPID_DICT_ASSN is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}
	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX ) {
			cerr << "PARAPROBE_RANGER_META_TYPID_DICT_ASSN is either empty and/or has unexpected size!" << "\n"; return false;
	}
	if ( offs.nrows() != f32.size()/PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX ) { //in this case not every ival has an assign rangedIontypeID
		cerr << "PARAPROBE_RANGER_META_TYPID_DICT_ASSN specifies unexpectedly a different number of rangedIontypes than _DICT_IVAL!" << "\n"; return false;
	}
	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_RANGER_META_TYPID_DICT_ASSN, offs.nrows(), offs.ncols() );
	status = read_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8 );
cout << "PARAPROBE_RANGER_META_TYPID_DICT_ASSN read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_RANGER_META_TYPID_DICT_ASSN on InputfileReconstruction failed!" << "\n"; return false;
	}
	
	//parse for all atom types which ranges are associated with them by decoding DICT_ASSN ion type IDs
	for( size_t r = 0; r < offs.nrows(); r++ ) {
		//find to which ID this belongs
		unsigned int thisid = static_cast<unsigned int>((int) uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX+0));
		bool found = false;
		for( auto it = tmp_out.begin(); it != tmp_out.end(); it++ ) { //search appropriate type
			if ( it->id != thisid ) {
				continue;
			}
			else {
				found = true;
				it->rng.push_back( mqival( 	f32.at(r*PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX+0),
											f32.at(r*PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX+1) ) );
				break;
			}
		}
		if ( found == false ) {
			cerr << "Inconsistency when associating mqival value with rangedIontype!" << "\n"; return false;
		}
	}

	//assume now that every row contributes only one mqival, i.e every IDs and MQ2ID
	map<size_t, vector<mqival>> selector; //selector is the final table of ion/atom types with their individual collection of associated ranged mq intervals
cout << "Catalogue of tmp ion types from the HDF5" << "\n";
	for( size_t r = 0; r < tmp_out.size(); r++ ) {
		//size_t hashval = z1z2z3_molecular_ion( tmp_out.at(r).strct.Z1, tmp_out.at(r).strct.Z2, tmp_out.at(r).strct.Z3 );
		size_t hashval =  	static_cast<size_t>(tmp_out.at(r).strct.Z3)*static_cast<size_t>(FIRST_BYTE) +
							static_cast<size_t>( 0)*static_cast<size_t>(SECOND_BYTE) +
							static_cast<size_t>(tmp_out.at(r).strct.Z2)*static_cast<size_t>(THIRD_BYTE) +
							static_cast<size_t>( 0)*static_cast<size_t>(FOURTH_BYTE) +
							static_cast<size_t>(tmp_out.at(r).strct.Z1)*static_cast<size_t>(FIFTH_BYTE) +
							static_cast<size_t>( 0)*static_cast<size_t>(SIXTH_BYTE) +
							static_cast<size_t>( 0)*static_cast<size_t>(SEVENTH_BYTE) +
							static_cast<size_t>( 0); //the seventh byte is padding and we must not multiply by the EIGTH_BYTE as it would invalidate the value
cout << "Hashval " << hashval << "\n";
		map<size_t, vector<mqival>>::iterator thisone = selector.find( hashval );
		//UNKNOWN_IONTYPES are internal ion states which have been added to the rangeTable upon construction
		//and therefore must not be added again when reading it in the UNKNOWN_IONTYPE has hashval 0 so
		if ( hashval != 0 ) {
			if ( thisone != selector.end() ) { //add mqrange to an existent unique evapion3
				for( auto kt = tmp_out.at(r).rng.begin(); kt != tmp_out.at(r).rng.end(); kt++ ) {
					selector[hashval].push_back( mqival( kt->lo, kt->hi ) );
	cout << "Selector add to existent key " << hashval << "\t\t" << kt->lo << "\t\t" << kt->hi << "\n";
				}
				//selector[hashval].push_back( mqival( f32.at(r*PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX+0),
				//									 f32.at(r*PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX+1) ) );
	//cout << "Selector add to existent key " << hashval << "\t\t" << f32.at(r*PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX+0) << "\t\t" << f32.at(r*PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX+1) << "\n";
			}
			else { //new unique evapion3
				selector.insert( pair<size_t, vector<mqival>>( hashval, vector<mqival>() ) );
				for( auto kt = tmp_out.at(r).rng.begin(); kt != tmp_out.at(r).rng.end(); kt++ ) {
					selector[hashval].push_back( mqival( kt->lo, kt->hi ) );
	cout << "Selector init new key " << hashval << "\t\t" << kt->lo << "\t\t" << kt->hi << "\n";
				}
				//selector[hashval].push_back( mqival( f32.at(r*PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX+0),
				//									 f32.at(r*PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX+1) ) );
	//cout << "Selector init new key " << hashval << "\t\t" << f32.at(r*PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX+0) << "\t\t" << f32.at(r*PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX+1) << "\n";
			}
		}
		else {
	cout << "Selector skipping UNKNOWN_IONTYPE" << "\n";
		}
	}
cout << "Selector number of unique keys " << selector.size() << "\n";

	//finally assign these selector values to the out variable
cout << "Decoded the following ion types from the HDF5" << "\n";
	//out = vector<rangedIontype>( selector.size(), rangedIontype() );
	unsigned int id = UNKNOWN_IONTYPE;
	for( auto it = selector.begin(); it != selector.end(); it++ ) {
		//##MK::will imply relabeling of ions according to what vector does
		out.push_back( rangedIontype() );
		out.back().id = static_cast<unsigned int>(id+1); //##MK::catch size_t narrowing conversion exception

			evapion3 tmp = evapion3();
			size_t hsh = it->first;
			size_t val = 0;

			val = hsh / FIRST_BYTE;		hsh = hsh - (val*FIRST_BYTE);
		//cout << "Z3.) " << val << endl;
			tmp.Z3 = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

			val = hsh / SECOND_BYTE; 	hsh = hsh - (val*SECOND_BYTE);
		//cout << "N3.) " << val << endl;
			tmp.N3 = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

			val = hsh / THIRD_BYTE; 	hsh = hsh - (val*THIRD_BYTE);
		//cout << "Z2.) " << val << endl;
			tmp.Z2 = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

			val = hsh / FOURTH_BYTE; 	hsh = hsh - (val*FOURTH_BYTE);
		//cout << "N2.) " << val << endl;
			tmp.N2 = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

			val = hsh / FIFTH_BYTE; 	hsh = hsh - (val*FIFTH_BYTE);
		//cout << "Z1.) " << val << endl;
			tmp.Z1 = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

			val = hsh / SIXTH_BYTE; 	hsh = hsh - (val*SIXTH_BYTE);
		//cout << "N1.) " << val << endl;
			tmp.N1 = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

			val = hsh / SEVENTH_BYTE; 	hsh = hsh - (val*SEVENTH_BYTE);
		//cout << "Pa.) " << val << endl;

			//must not divide by EIGTH_BYTE because would be division by zero!
			val = hsh;
		//cout << "Ch.) " << val << endl;
			tmp.charge = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

cout << "Unhash " << (int) tmp.N1 << "\t" << (int) tmp.Z1 << "\t" << (int) tmp.N2 << "\t" << (int) tmp.Z2 << "\t" << (int) tmp.N3 << "\t" << (int) tmp.Z3 << "\t\t" << (int) tmp.charge << endl;
		out.back().strct = tmp; //molecular_ion_z1z2z3( it->first );
		for( auto jt = it->second.begin(); jt != it->second.end(); jt++ ) {
			out.back().rng.push_back( *jt );
			//##MK::BEGIN DEBUG
cout << "ID, Z1Z2Z3, mqmin, mqmax " << out.back().id << "\t\t" << (int) out.back().strct.Z1 << ";" << (int) out.back().strct.Z2 << ";" << (int) out.back().strct.Z3 << "\t\t" << jt->lo << ";" << jt->hi << "\n";
			//##MK::END DEBUG
		}
		id++;
	}

	uc8 = vector<unsigned char>();
	f32 = vector<float>();
	return true;	
}


bool h5Hdl::read_ityp_from_apth5( const string fn, vector<unsigned char> & out )
{
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	if ( fn.substr(fn.length()-3) != ".h5" ) {
		cerr << "The inputfile " << fn << " has not the proper .h5 file extension!" << "\n";
		return false;
	}

	//probe existence of Ion2distance value array in the input file and get dimensions
	h5resultsfn = fn;
	if ( query_contiguous_matrix_dims( PARAPROBE_RANGER_RES_TYPID, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_RANGER_RES_TYPID!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_RANGER_RES_TYPID is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_RANGER_RES_TYPID_NCMAX ) {
		cerr << "PARAPROBE_RANGER_RES_TYPID is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	vector<unsigned char> uc8;
	ifo = h5iometa( PARAPROBE_RANGER_RES_TYPID, offs.nrows(), offs.ncols() );
	status = read_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8 );
cout << "PARAPROBE_RANGER_RES_TYPID read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_RANGER_RES_TYPID failed!" << "\n";
		return false;
	}

	try {
		out = vector<unsigned char>();
		out.reserve( offs.nrows() );
//MK::here point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading PARAPROBE_RANGER_RES_TYPID!" << "\n"; return false;
	}

	for( auto it = uc8.begin(); it != uc8.end(); it++ ) {
		out.push_back( *it );
	}
	uc8 = vector<unsigned char>();
	return true;
}


bool h5Hdl::read_dist_from_apth5( const string fn, vector<apt_real> & out )
{
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	if ( fn.substr(fn.length()-3) != ".h5" ) {
		cerr << "The inputfile " << fn << " has not the proper .h5 file extension!" << "\n";
		return false;
	}

	//probe existence of Ion2distance value array in the input file and get dimensions
	h5resultsfn = fn;
	if ( query_contiguous_matrix_dims( PARAPROBE_SURF_DIST_RES_D, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_SURF_DIST_RES_D!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_SURF_DIST_RES_D is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_SURF_DIST_RES_D_NCMAX ) {
		cerr << "PARAPROBE_SURF_DIST_RES_D is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_SURF_DIST_RES_D, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
cout << "PARAPROBE_SURF_DIST_RES_D read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_SURF_DIST_RES_D failed!" << "\n";
		return false;
	}

	try {
		out = vector<apt_real>();
		out.reserve( offs.nrows() );
//MK::here point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading PARAPROBE_SURF_DIST_RES_D!" << "\n"; return false;
	}

	for( auto it = f32.begin(); it != f32.end(); it++ ) {
		out.push_back( *it );
	}
	f32 = vector<float>();
	return true;	
}

//#endif


/*
if ( ppp.size() != lll.size() ) {
	reporting("Point cloud data and label arrays have dissimilar size!");
	return;
}

//get total number of elements to write from buffers ppp and lll
size_t nn = 0;
for( size_t b = 0; b < ppp.size(); ++b ) {
	if ( ppp.at(b) != NULL && lll.at(b) != NULL ) { //point coordinate and label data exist
		if ( ppp.at(b)->size() == lll.at(b)->size() ) //dataset is consistent
			nn += ppp.at(b)->size();
		else {
			reporting("Point cloud data and label arrays have dissimilar size!"); return;
		}
	}
	else {
		reporting("Point cloud data and label arrays have dissimilar size!"); return;
	}
}
*/
