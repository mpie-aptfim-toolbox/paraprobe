//##MK::GPLV3


#ifndef __PARAPROBE_UTILS_SPACEBUCKETING_H__
#define __PARAPROBE_UTILS_SPACEBUCKETING_H__

#include "PARAPROBE_AABBTree.h"

//implements a self-threadlocal-memory-allocating spatial index structure
//for amortized constant time queries of Moore neighborhood about spherical ball
//the container knows how the buckets have been mapped to threadlocal memory
//thus upon querying the struct sequentially by the callee using the range_rball function
//the callee will just read the data from shared memory and potentially has to query
//addresses in buckets which reference pieces of threadlocal memory
//if instead the callee is running threadparallel the individual threads will query
//the data structure and can make use of reading more likely local pieces of memory
//which will be faster at higher thread count on ccNUMA systems specifically on
//systems where multi-level hierarchies on top of the usual L1,L2,L3 cache hierarchy have
//were built

//for development of E_OIM_POLYCRYSTAL it is useful to deliver additional data to the tip construction
//in this case define the following variable, it will allow to compile in additional accounting
//#define SPACEBUCKETING_ACTIVATE_ACCOUNTING

struct erase_log
{
	size_t ncleared;
	size_t nkept;
	erase_log() : ncleared(0L), nkept(0L) {}
	erase_log(const size_t _nc, const size_t _nk) :
		ncleared(_nc), nkept(_nk) {}
};


struct spacebucket
{
	sqb mdat;
	vector<vector<pos>*> buckets;
#ifdef SPACEBUCKETING_ACTIVATE_ACCOUNTING
	vector<vector<unsigned short>*> bucketsinfo;
#endif

	spacebucket();
	~spacebucket();

	void initcontainer( const aabb3d & container );
	void freecontainer( void );
	void add_atom( const pos p );
#ifdef SPACEBUCKETING_ACTIVATE_ACCOUNTING
	void add_atom_info( const pos p, const unsigned short ifo );
#endif

	void range_rball_noclear_nosort( const pos p, apt_xyz r, vector<pos> & candidates );
	erase_log erase_rball( const p3d p, apt_xyz r );

	//##MK::
	/*
	void write_occupancy_raw();
	*/
	size_t get_memory_consumption();
};

#endif
