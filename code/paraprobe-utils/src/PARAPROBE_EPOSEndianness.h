//##MK::CODE

#ifndef __PARAPROBE_UTILS_EPOSENDIANNESS_H__
#define __PARAPROBE_UTILS_EPOSENDIANNESS_H__

#include "PARAPROBE_Profiling.h"

//MK::smart struct encapsulates detector events and converts from bigendian-encoded IVAS EPOS binary files into C-compliant little endian
//MK::DO NOT CHANGE THE FLOATING POINT VARIABLE TYPE FROM FLOAT TO DOUBLE IN AN ATTEMPT TO ENFORCE CONSISTENCE WITH REAL!
//THEN THE ENDIANNESS TRANSFORMATION WILL VERY LIKELY NOT WORK! instead we accept that the APT delivers us single precision floats which we bring
//into little endian format and ones being stored in a struct epos we can do with them what we want, for instance
//keeping as floats when EMPLOY_SINGLEPRECISION or promote them implicitly to double when utilizing DOUBLEPRECISION


//native reading of pos files
struct pos_be_2_stl_le {
	//the four BigEndian floats of an IVAS POS file
	unsigned int f1; //x
	unsigned int f2; //y
	unsigned int f3; //z
	unsigned int f4; //mq
};


struct pos {
	float x;
	float y;
	float z;
	float mq;

	inline float my_bswap_uint_2_float( unsigned int ui )
	{
		float retVal = 0.f; //keep it deterministic
		char *pVal = (char*) &ui;
		char* pRetVal = (char*) &retVal;
		for (int i=0; i<4; ++i) {
			pRetVal[4-1-i] = pVal[i];
		}
		return retVal;
	}

	pos() : x(0.f),y(0.f),z(0.f), mq(0.f) {}

	pos(const struct pos_be_2_stl_le in)
	{ //upon storing the detector event within the PARAPROBE data structure we pass the bigendian values and change endianness on the fly
		x = my_bswap_uint_2_float(in.f1);
		y = my_bswap_uint_2_float(in.f2);
		z = my_bswap_uint_2_float(in.f3);
		mq = my_bswap_uint_2_float(in.f4);
	}

	pos(const float _x, const float _y, const float _z, const float _mq) :
		x(_x), y(_y), z(_z), mq(_mq) {}
};


//native reading of epos files
struct epos_be_2_stl_le {
	//the nine IEEE754 BigEndian floats of an IVAS EPOS file
	unsigned int f1; //x;
	unsigned int f2; //y;
	unsigned int f3; //z;

	unsigned int f4; //mq;
	unsigned int f5; //tof;
	unsigned int f6; //vdc;

	unsigned int f7; //vpu;
	unsigned int f8; //detx;
	unsigned int f9; //dety;

	//the two preceeding 32bit BigEndian ints
	int i1;
	int i2;
};


struct epos {
	float x;
	float y;
	float z;

	float mq;			//reconstructed mass-to-charge-state ratio (Da)e
	float tof;			//raw time of flight (ns)
	float vdc;			//standing voltage (V)

	float vpu;			//pulse voltage for laser zero (V)
	float detx;			//detector hit position x (mm)
	float dety;			//detector hit position y (mm)

	int i1;				//DeltaP number of pulses since the last detected ion (pulses) for multi-hit records, after the first record, this is zero
	int i2;				//NM Hit multiplicity (ions) for multi-hit records, after the first record, this is zero

	inline float my_bswap_uint_2_float( unsigned int ui ) //;
	//inline float epos::my_bswap_uint_2_float( unsigned int ui )
	{
		float retVal = 0.f; //keep it deterministic
		char *pVal = (char*) &ui;
		char* pRetVal = (char*) &retVal;
		for (int i=0; i<4; ++i) {
			pRetVal[4-1-i] = pVal[i];
		}
		return retVal;
	}

	inline int32_t my_bswap_int32( int32_t val )
	{
		val = ((val << 8) & 0xFF00FF00) | ((val >> 8) & 0xFF00FF );
		return (val << 16) | ((val >> 16) & 0xFFFF);
	}

	epos() : x(0.f),y(0.f),z(0.f),
				mq(0.f), tof(0.f), vdc(0.f),
					vpu(0.f), detx(0.f), dety(0.f),
						i1(0), i2(0) {}

	epos(const struct epos_be_2_stl_le in)
	{ //upon storing the detector event within the PARAPROBE data structure we pass the bigendian values and change endianness
		x = my_bswap_uint_2_float(in.f1);
		y = my_bswap_uint_2_float(in.f2);
		z = my_bswap_uint_2_float(in.f3);
		mq = my_bswap_uint_2_float(in.f4);

		tof = my_bswap_uint_2_float(in.f5);
		vdc = my_bswap_uint_2_float(in.f6);
		vpu = my_bswap_uint_2_float(in.f7);
		detx = my_bswap_uint_2_float(in.f8);
		dety = my_bswap_uint_2_float(in.f9);

		i1 = my_bswap_int32(in.i1);
		i2 = my_bswap_int32(in.i2);
	}
};

ostream& operator<<(ostream& in, epos const & val);

ostream& operator<<(ostream& in, pos const & val);

#endif
