//##MK::GPLV3


#ifndef __PARAPROBE_UTILS_CONFIG_SHARED_H__
#define __PARAPROBE_UTILS_CONFIG_SHARED_H__

//#include "PARAPROBE_Vxlizer.h"
#include "PARAPROBE_MPIDatatypes.h"

apt_real str2real( const string str );
string read_xml_attribute_string( xml_node<> const * in, const string keyword );
apt_real read_xml_attribute_float( xml_node<> const * in, const string keyword );
unsigned short int read_xml_attribute_uint16( xml_node<> const * in, const string keyword );
unsigned int read_xml_attribute_uint32( xml_node<> const * in, const string keyword );
int read_xml_attribute_int32( xml_node<> const * in, const string keyword );
bool read_xml_attribute_bool( xml_node<> const * in, const string keyword );
unsigned long read_xml_attribute_uint64( xml_node<> const * in, const string keyword );
string sizet2str( const size_t val );
string real2str( const apt_real val );
string uint2str( const unsigned int val );
string int2str( const int val );


struct elementinfo
{
	apt_real mass;		//Da
	unsigned char Z;	//number
	unsigned char pad1;
	unsigned char pad2;
	unsigned char pad3;
	elementinfo() : mass(0.0), Z(0x00), pad1(0x00), pad2(0x00), pad3(0x00) {}
	elementinfo( const apt_real _mass, const unsigned char _Z ) :
		mass(_mass), Z(_Z), pad1(0x00), pad2(0x00), pad3(0x00) {}
};

struct isotopeinfo
{
	apt_real mass;
	apt_real abundance;
	unsigned char N;			//number of total nucleons, not neutrons!
	unsigned char Z;			//number of protons
	unsigned char radioactive;
	unsigned char pad2;
	isotopeinfo() : mass(0.0), abundance(0.0), N(0x00), Z(0x00), radioactive(false), pad2(0x00) {}
	isotopeinfo( const apt_real _mass, const apt_real _abun,
			const unsigned char _N, const unsigned char _Z, const bool _radioactive ) :
		mass(_mass), abundance(_abun), N(_N), Z(_Z), radioactive(_radioactive), pad2(0x00) {}
};


class ConfigShared
{
public:

	static unsigned int SimID;
	static unsigned int RndDescrStatsPRNGSeed;
	static unsigned int RndDescrStatsPRNGDiscard;

	//static void readXML( string filename = "" );
	//static bool checkUserInput();
	static vector<size_t> MySpacegroups;
	static map<string, elementinfo> MyElementsSymbol;
	static map<unsigned char, elementinfo> MyElementsZ;
	//static map<unsigned char, map<unsigned char, isotopeinfo>> MyIsotopes;
	static vector<isotopeinfo> MyIsotopes;
};

#endif
