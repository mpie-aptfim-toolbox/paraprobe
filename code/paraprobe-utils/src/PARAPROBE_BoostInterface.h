//##MK::GPLV3

#ifndef __PARAPROBE_UTILS_BOOSTINTERFACE_H__
#define __PARAPROBE_UTILS_BOOSTINTERFACE_H__

#include "PARAPROBE_STLIncludes.h"
#include "PARAPROBE_Numerics.h"
#include "PARAPROBE_PhysicalConstants.h"
//#include "PARAPROBE_Crystallography.h"
#include "PARAPROBE_UnitConversions.h"
#include "PARAPROBE_Parallelization.h"


//#define UTILIZE_BOOST

#ifdef UTILIZE_BOOST
	//boost
	#include <boost/algorithm/string.hpp>
	#include <boost/math/special_functions/bessel.hpp>
#else
	//C++ processing of regular expression to avoid having to use boost for now
	#include <regex>
	using std::regex;
	using std::string;
	using std::sregex_token_iterator;
#endif


#endif
