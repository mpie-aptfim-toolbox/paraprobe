//##MK::GPLV3

#include "PARAPROBE_Datatypes.h"

ostream& operator<<(ostream& in, voltm1 const & val)
{
	in << "Vdu " << val.Vdc << " Vpu " << val.Vpu << " m " << val.m << "\n";
	return in;
}


void t3x3::add( const t3x3 & increase, const real_m33 weight )
{
	this->a11 += weight * increase.a11;
	this->a12 += weight * increase.a12;
	this->a13 += weight * increase.a13;

	this->a21 += weight * increase.a21;
	this->a22 += weight * increase.a22;
	this->a23 += weight * increase.a23;

	this->a31 += weight * increase.a31;
	this->a32 += weight * increase.a32;
	this->a33 += weight * increase.a33;
}

void t3x3::div( const real_m33 divisor )
{
	if (abs(divisor) > DOUBLE_EPSILON) {
		this->a11 /= divisor;
		this->a12 /= divisor;
		this->a13 /= divisor;

		this->a21 /= divisor;
		this->a22 /= divisor;
		this->a23 /= divisor;

		this->a31 /= divisor;
		this->a32 /= divisor;
		this->a33 /= divisor;
	}
}


real_m33 t3x3::det()
{
	real_m33 row1 = +1.f*this->a11 * ((this->a22*this->a33) - (this->a32*this->a23));
	real_m33 row2 = +1.f*this->a21 * ((this->a12*this->a33) - (this->a32*this->a13));
	real_m33 row3 = +1.f*this->a31 * ((this->a12*this->a23) - (this->a22*this->a13));
	return row1 - row2 + row3;
}


ostream& operator << (ostream& in, t3x3 const & val) {
	in << val.a11 << ";" << val.a12 << ";" << val.a13 << "\n";
	in << val.a21 << ";" << val.a22 << ";" << val.a23 << "\n";
	in << val.a31 << ";" << val.a32 << ";" << val.a33 << "\n";
	return in;
}


ostream& operator<<(ostream& in, p2d const & val)
{
	in << val.x << ";" << val.y << "\n";
	return in;
}


p3d p3d::active_rotation_relocate( t3x3 const & R )
{
	return p3d( 	R.a11 * this->x + R.a12 * this->y + R.a13 * this->z,
					R.a21 * this->x + R.a22 * this->y + R.a23 * this->z,
					R.a31 * this->x + R.a32 * this->y + R.a33 * this->z   );
}


apt_real dot( p3d const & a, p3d const & b )
{
	return a.x*b.x + a.y*b.y + a.z*b.z;
}


p3d cross( p3d const & a, p3d const & b )
{
	return p3d( a.y*b.z - a.z*b.y,
				a.z*b.x - a.x*b.z,
				a.x*b.y - a.y*b.x );
}


apt_real vnorm( p3d const & a )
{
	return sqrt(SQR(a.x)+SQR(a.y)+SQR(a.z));
}


ostream& operator<<(ostream& in, p3d const & val)
{
	in << val.x << ";" << val.y << ";" << val.z << "\n";
	return in;
}


ostream& operator<<(ostream& in, p3i const & val)
{
	in << val.x << ";" << val.y << ";" << val.z << "\n";
	return in;
}


ostream& operator<<(ostream& in, p6i const & val)
{
	in << val.xmi << ";" << val.xmx << "\t\t" << val.ymi << ";" << val.ymx << "\t\t" << val.zmi << ";" << val.zmx << "\n";
	return in;
}



ostream& operator<<(ostream& in, vxxl const & val)
{
	in << val.x << ";" << val.y << ";" << val.z << "\n";
	return in;
}


ostream& operator<<(ostream& in, p1dm1 const & val)
{
	in << val.pos << "--" << val.m << "\n";
	return in;
}


ostream& operator<<(ostream& in, p2dm1 const & val)
{
	in << val.x << ";" << val.y << ";" << "--" << val.m << "\n";
	return in;
}


ostream& operator<<(ostream& in, p3dm1 const & val)
{
	in << val.x << ";" << val.y << ";" << val.z << "--" << val.m << "\n";
	return in;
}


ostream& operator<<(ostream& in, p3dmq const & val)
{
	in << val.x << ";" << val.y << ";" << val.z << "--" << val.m2q << "\n";
	return in;
}


ostream& operator<<(ostream& in, p3dmqDet const & val)
{
	in << val.x << ";" << val.y << ";" << val.z << "--" << val.m2q << "--" << val.id << "\n";
	return in;
}


ostream& operator<<(ostream& in, p3difo const & val) {
	in << "dist/IonID/ityp_org/ityp_rnd\t\t" << val.dist << ";" << val.IonID << ";" << val.i_org << ";" << val.i_rnd << "\n";
	return in;
}


ostream& operator<<(ostream& in, p3dm2 const & val) {
	in << "ityp_org/ityp_rnd\t\t" << val.i_org << ";" << val.i_rnd << ";";
	in << val.x << ";" << val.y << ";" << val.z << "\n";
	return in;
}


ostream& operator<<(ostream& in, p3dm3 const & val) {
	in << "ID/sgn/Iontype/xyz\t\t" << val.id << ";" << val.sgn << ";" << val.iontype;
	in << val.x << ";" << val.y << ";" << val.z << "\n";
	return in;
}


void d3d::normalize_me()
{
	apt_real norm = 1.f / sqrt(SQR(this->u)+SQR(this->v)+SQR(this->w));
	this->u *= norm;
	this->v *= norm;
	this->w *= norm;
}


apt_real d3d::len()
{
	apt_real sqrlen = SQR(this->u) + SQR(this->v) + SQR(this->w);
	return ( sqrlen > EPSILON) ? sqrt(sqrlen) : 0.f;
}


ostream& operator<<(ostream& in, d3d const & val)
{
	in << val.u << ";" << val.v << ";" << val.w << "\n";
	return in;
}


inline apt_real v3d::len() const
{
	return (this->SQR_len > EPSILON) ? sqrt(this->SQR_len) : 0.f;
}

ostream& operator<<(ostream& in, v3d const & val)
{
	in << val.u << ";" << val.v << ";" << val.w << "----" << val.SQR_len << "\n";
	return in;
}



void aabb2d::scale()
{
	this->xsz = this->xmx - this->xmi;
	this->ysz = this->ymx - this->ymi;
}

void aabb2d::blowup( const apt_real f )
{
	this->xmi -= f;
	this->xmx += f;
	this->ymi -= f;
	this->ymx += f;
	this->scale();
}

apt_real aabb2d::diag()
{
	return sqrt(SQR(this->xmx-this->xmi)+SQR(this->ymx-this->ymi));
}


void aabb2d::possibly_enlarge_me( p2dm1 const & p )
{
	if ( p.x <= this->xmi )
		this->xmi = p.x;
	if ( p.x >= this->xmx )
		this->xmx = p.x;

	if ( p.y <= this->ymi )
		this->ymi = p.y;
	if ( p.y >= this->ymx )
		this->ymx = p.y;
}


ostream& operator<<(ostream& in, aabb2d const & val)
{
	in << val.xmi << ";" << val.xmx << "--" << val.ymi << ";" << val.ymx << "----" << val.xsz << ";" << val.ysz << "\n";
	return in;
}


void aabb3d::add_epsilon_guard()
{
	this->xmi -= AABBINCLUSION_EPSILON; //EPSILON;
	this->xmx += AABBINCLUSION_EPSILON; //EPSILON;
	this->ymi -= AABBINCLUSION_EPSILON; //EPSILON;
	this->ymx += AABBINCLUSION_EPSILON; //EPSILON;
	this->zmi -= AABBINCLUSION_EPSILON; //EPSILON;
	this->zmx += AABBINCLUSION_EPSILON; //EPSILON;
}


void aabb3d::scale()
{
	this->xsz = this->xmx - this->xmi;
	this->ysz = this->ymx - this->ymi;
	this->zsz = this->zmx - this->zmi;
}

void aabb3d::blowup( const apt_real f )
{
	this->xmi -= f;
	this->xmx += f;
	this->ymi -= f;
	this->ymx += f;
	this->zmi -= f;
	this->zmx += f;
	this->scale();
}


apt_real aabb3d::diag()
{
	return sqrt(SQR(this->xmx-this->xmi)+SQR(this->ymx-this->ymi)+SQR(this->zmx-this->zmi));
}


bool aabb3d::inside( const p3dm1 test )
{
	if ( test.x < this->xmi )	return false;
	if ( test.x > this->xmx )	return false;
	if ( test.y < this->ymi )	return false;
	if ( test.y > this->ymx )	return false;
	if ( test.z < this->zmi )	return false;
	if ( test.z > this->zmx )	return false;

	return true;
}


bool aabb3d::is_inside_box_xy( aabb3d const & reference, apt_real guard )
{
	if ( 	(reference.xmi - guard) > this->xmi &&
			(reference.xmx + guard) < this->xmx &&
			(reference.ymi - guard) > this->ymi &&
			(reference.ymx + guard) < this->ymx )
		return true;
	else
		return false;
	//&& (reference.zmi - guard) > this->zmi && (reference.zmx + guard) < this->zmx )
}


bool aabb3d::is_inside( p3d const & test ) {
	if ( test.x < this->xmi )
		return false;
	if ( test.x > this->xmx )
		return false;
	if ( test.y < this->ymi )
		return false;
	if ( test.y > this->ymx )
		return false;
	if ( test.z < this->zmi )
		return false;
	if ( test.z > this->zmx )
		return false;

	//not return so on box walls or inside
	return true;
}


p3d aabb3d::center()
{
	return p3d( 0.5*(this->xmi + this->xmx),
				0.5*(this->ymi + this->ymx),
				0.5*(this->zmi + this->zmx) );
}


apt_real aabb3d::circumscribed_sphere()
{
	p3d boxcenter = p3d( 	0.5*(this->xmi + this->xmx),
							0.5*(this->ymi + this->ymx),
							0.5*(this->zmi + this->zmx)  );
	apt_real RSQR = 0.f;
	apt_real DistSQR = 0.f;
	DistSQR = SQR(this->xmi-boxcenter.x) + SQR(this->ymi-boxcenter.y) + SQR(this->zmi-boxcenter.z);
	if ( DistSQR >= RSQR )
		RSQR = DistSQR;
	DistSQR = SQR(this->xmx-boxcenter.x) + SQR(this->ymi-boxcenter.y) + SQR(this->zmi-boxcenter.z);
	if ( DistSQR >= RSQR )
		RSQR = DistSQR;
	DistSQR = SQR(this->xmi-boxcenter.x) + SQR(this->ymx-boxcenter.y) + SQR(this->zmi-boxcenter.z);
	if ( DistSQR >= RSQR )
		RSQR = DistSQR;
	DistSQR = SQR(this->xmx-boxcenter.x) + SQR(this->ymx-boxcenter.y) + SQR(this->zmi-boxcenter.z);
	if ( DistSQR >= RSQR )
		RSQR = DistSQR;
	DistSQR = SQR(this->xmi-boxcenter.x) + SQR(this->ymi-boxcenter.y) + SQR(this->zmx-boxcenter.z);
	if ( DistSQR >= RSQR )
		RSQR = DistSQR;
	DistSQR = SQR(this->xmx-boxcenter.x) + SQR(this->ymi-boxcenter.y) + SQR(this->zmx-boxcenter.z);
	if ( DistSQR >= RSQR )
		RSQR = DistSQR;
	DistSQR = SQR(this->xmi-boxcenter.x) + SQR(this->ymx-boxcenter.y) + SQR(this->zmx-boxcenter.z);
	if ( DistSQR >= RSQR )
		RSQR = DistSQR;
	DistSQR = SQR(this->xmx-boxcenter.x) + SQR(this->ymx-boxcenter.y) + SQR(this->zmx-boxcenter.z);
	if ( DistSQR >= RSQR )
		RSQR = DistSQR;

	if ( RSQR > EPSILON )
		return sqrt(RSQR);
	else
		return F32MX;
}


apt_real aabb3d::volume()
{
	return 	(this->xmx - this->xmi) *
			(this->ymx - this->ymi) *
			(this->zmx - this->zmi);
}


p3i aabb3d::blockpartitioning( const size_t p_total, const size_t p_perblock_target )
{
	p3i out = p3i( 1, 1, 1);
	if ( this->xsz > EPSILON ) {
		apt_real yrel = this->ysz / this->xsz;
		apt_real zrel = this->zsz / this->xsz;
		apt_real ix = pow(
			(static_cast<apt_real>(p_total) / p_perblock_target) / (yrel * zrel),
			(1.f/3.f) );

		out.x = ( static_cast<int>(floor(ix)) > 0 ) ? static_cast<int>(floor(ix)) : 1;
		apt_real iy = yrel * ix;
		out.y = ( static_cast<int>(floor(iy)) > 0 ) ? static_cast<int>(floor(iy)) : 1;
		apt_real iz = zrel * ix;
		out.z = ( static_cast<int>(floor(iz)) > 0 ) ? static_cast<int>(floor(iz)) : 1;
	}
	return out;
}

/*
void aabb3d::possibly_enlarge_me( p3d const & p )
{
	if ( p.x <= this->xmi )
		this->xmi = p.x;
	if ( p.x >= this->xmx )
		this->xmx = p.x;

	if ( p.y <= this->ymi )
		this->ymi = p.y;
	if ( p.y >= this->ymx )
		this->ymx = p.y;

	if ( p.z <= this->zmi )
		this->zmi = p.z;
	if ( p.z >= this->zmx )
		this->zmx = p.z;
}
*/

void aabb3d::possibly_enlarge_me( const p3d p )
{
	if ( p.x <= this->xmi )
		this->xmi = p.x;
	if ( p.x >= this->xmx )
		this->xmx = p.x;

	if ( p.y <= this->ymi )
		this->ymi = p.y;
	if ( p.y >= this->ymx )
		this->ymx = p.y;

	if ( p.z <= this->zmi )
		this->zmi = p.z;
	if ( p.z >= this->zmx )
		this->zmx = p.z;
}


void aabb3d::possibly_enlarge_me( const p3d64 p )
{
	apt_real x = p.x;
	if ( x <= this->xmi )
		this->xmi = x;
	if ( x >= this->xmx )
		this->xmx = x;

	apt_real y = p.y;
	if ( y <= this->ymi )
		this->ymi = y;
	if ( y >= this->ymx )
		this->ymx = y;

	apt_real z = p.z;
	if ( z <= this->zmi )
		this->zmi = z;
	if ( z >= this->zmx )
		this->zmx = z;
}


void aabb3d::possibly_enlarge_me( aabb3d const & refbx )
{
	if ( refbx.xmi <= this->xmi )
		this->xmi = refbx.xmi;
	if ( refbx.xmx >= this->xmx )
		this->xmx = refbx.xmx;
	if ( refbx.ymi <= this->ymi )
		this->ymi = refbx.ymi;
	if ( refbx.ymx >= this->ymx )
		this->ymx = refbx.ymx;
	if ( refbx.zmi <= this->zmi )
		this->zmi = refbx.zmi;
	if ( refbx.zmx >= this->zmx )
		this->zmx = refbx.zmx;
}


ostream& operator<<(ostream& in, aabb3d const & val)
{
	in << "\n" << "\t\t" << val.xmi << ";" << val.xmx << "\n";
	in <<         "\t\t" << val.ymi << ";" << val.ymx << "\n";
	in <<         "\t\t" << val.zmi << ";" << val.zmx << "\n";
	in <<         "\t\t" << val.xsz << ";" << val.ysz << ";" << val.zsz << "\n";
	return in;
}


ostream& operator<<(ostream& in, cuboidgrid3d const & val)
{
	in << "Gridcells\t\t" << val.gridcells.xmi << ";" << val.gridcells.xmx << "\n";
	in << "         \t\t" << val.gridcells.ymi << ";" << val.gridcells.ymx << "\n";
	in << "         \t\t" << val.gridcells.zmi << ";" << val.gridcells.zmx << "\n";
	in << "Binwidth \t\t" << val.binwidths.x << ";" << val.binwidths.y << ";" << val.binwidths.z << "\n";
	in << "AABB3D   \t\t" << val.ve.xmi << ";" << val.ve.xmx << ";" << "\n";
	in << "         \t\t" << val.ve.ymi << ";" << val.ve.ymx << ";" << "\n";
	in << "         \t\t" << val.ve.zmi << ";" << val.ve.zmx << "\n";
	return in;
}


/*void cuboidgrid3d::init( const p3i64 extend, const p3d dims, aabb3d const & roi )
{
	this->gridcell = extend;
	this->binwidths = dims;
	this->nxy = extend.nx * extend.ny;
	this->nxyz = extend.nx * extend.ny * extend.nz;
	this->ve = roi;
}*/


p3d cuboidgrid3d::where( const int ix, const int iy, const int iz )
{
	p3d out = p3d( F32MX, F32MX, F32MX ); //failure point
	if ( 	ix >= this->gridcells.xmi &&
			ix <= this->gridcells.xmx &&
			iy >= this->gridcells.ymi &&
			iy <= this->gridcells.ymx &&
			iz >= this->gridcells.zmi &&
			iz <= this->gridcells.zmx ) {

		p3d out = this->ve.center();
		//spreading in steps of binwidths to the six directions axis aligned
		out.x = out.x + (static_cast<apt_real>(ix) * this->binwidths.x);
		out.y = out.y + (static_cast<apt_real>(iy) * this->binwidths.y);
		out.z = out.z + (static_cast<apt_real>(iz) * this->binwidths.z);
		return out;
	}
	return p3d( F32MX, F32MX, F32MX );
}


size_t cuboidgrid3d::ngridcells()
{
	int nx = this->gridcells.xmx - this->gridcells.xmi + 2; //##MK::+1?
	int ny = this->gridcells.ymx - this->gridcells.ymi + 2; //##MK::+1?
	int nz = this->gridcells.zmx - this->gridcells.zmi + 2; //##MK::+1?
	return static_cast<size_t>(nx) * static_cast<size_t>(ny) * static_cast<size_t>(nz);
}


/*size_t cuboidgrid3d::whichcell( const p3d p )
{
	//3d implicit x+y*nx+z*nx*ny
	//##MK::numerical robustness might require to add EPSILON environment
	apt_real ix = floor((p.x - this->ve.xmi) / (this->ve.xmx - this->ve.xmi) * static_cast<apt_real>(this->gridcell.nx));
	apt_real iy = floor((p.y - this->ve.ymi) / (this->ve.ymx - this->ve.ymi) * static_cast<apt_real>(this->gridcell.ny));
	apt_real iz = floor((p.z - this->ve.zmi) / (this->ve.zmx - this->ve.zmi) * static_cast<apt_real>(this->gridcell.nz));
	size_t res = static_cast<size_t>(ix) + static_cast<size_t>(iy) * this->gridcell.nx + static_cast<size_t>(iz) * this->nxy;
	return res;
}*/


bool cylinder::inside( const p3dm1 test )
{
	if ( test.x < this->aabb.xmi )	return false;
	if ( test.x > this->aabb.xmx )	return false;
	if ( test.y < this->aabb.ymi )	return false;
	if ( test.y > this->aabb.ymx )	return false;
	if ( test.z < this->aabb.zmi )	return false;
	if ( test.z > this->aabb.zmx )	return false;

	//inside spherical section
	if ( ( SQR(test.x - 0.5*(this->aabb.xmi+this->aabb.xmx)) + SQR(test.x - 0.5*(this->aabb.xmi+this->aabb.xmx)) ) > SQR(this->R) )
		return false;
	else
		return true;
}


ostream& operator<<(ostream& in, cylinder const & val)
{
	in << val.aabb.xmi << ";" << val.aabb.xmx << "--" << val.aabb.ymi << ";" << val.aabb.ymx << "--" << val.aabb.zmi << ";" << val.aabb.zmx << "----" << val.aabb.xsz << ";" << val.aabb.ysz << ";" << val.aabb.zsz << "\n";
	in << val.H << ";" << val.R << "\n";
	return in;
}


ostream& operator<<(ostream& in, jobreceipt const & val)
{
	in << "JobID/ProcessID/ThreadID/WallClock = " << val.jobid << ";" << val.rank << ";" << val.thread << "\t\t" << val.wallclock << "\n";
	return in;
}



ostream& operator<<(ostream& in, nbor const & val)
{
	in << "Distance/MarkValue = " << val.d << "\t\t" << val.m << "\n";
	return in;
}



p3d tri3d::barycenter()
{
	return p3d( (this->x1 + this->x2 + this->x3 )/3.f,
				(this->y1 + this->y2 + this->y3 )/3.f,
				(this->z1 + this->z2 + this->z3 )/3.f );
}


ostream& operator<<(ostream& in, tri3d const & val)
{
	in << val.x1 << ";" << val.y1 << ";" << val.z1 << "\n";
	in << val.x2 << ";" << val.y2 << ";" << val.z2 << "\n";
	in << val.x3 << ";" << val.y3 << ";" << val.z3 << "\n";
	return in;
}

ostream& operator<<(ostream& in, triref3d const & val)
{
	in << val.v1 << "\t\t" << val.v2 << "\t\t" << val.v3 << "\n";
	return in;
}


ostream& operator<<(ostream& in, tri3dui const & val)
{
	in << val.v1 << "\t\t" << val.v2 << "\t\t" << val.v3 << "\n";
	return in;
}


ostream& operator<<(ostream& in, quad3d const & val)
{
	in << val.v1 << "\t\t" << val.v2 << "\t\t" << val.v3 << "\t\t" << val.v4 << "\n";
	return in;
}


size_t sqb::where( const p3dm1 p )
{
	//3d implicit x+y*nx+z*nx*ny
	if ( 	p.x >= this->box.xmi &&
			p.x <= this->box.xmx &&
			p.y >= this->box.ymi &&
			p.y <= this->box.ymx &&
			p.z >= this->box.zmi &&
			p.z <= this->box.zmx ) {
		apt_real ix = floor((p.x - this->box.xmi) / (this->box.xmx - this->box.xmi) * static_cast<apt_real>(this->nx));
		apt_real iy = floor((p.y - this->box.ymi) / (this->box.ymx - this->box.ymi) * static_cast<apt_real>(this->ny));
		apt_real iz = floor((p.z - this->box.zmi) / (this->box.zmx - this->box.zmi) * static_cast<apt_real>(this->nz));
		size_t res = static_cast<size_t>(ix) + static_cast<size_t>(iy) * this->nx + static_cast<size_t>(iz) * this->nxy;
		return res;
	}
	else {
		return -1;
	}
}


p3d sqb::where( const size_t bxyz )
{
	//3d implicit x+y*nx+z*nx*ny
	size_t bz = bxyz / this->nxy;
	size_t rem = bxyz - bz * this->nxy;
	size_t by = rem / this->nx;
	size_t bx = rem - by*this->nx;
	if ( bx < this->nx && by < this->ny && bz < this->nz ) {
		apt_real px = this->box.xmi + this->width*(0.5 + static_cast<apt_real>(bx));
		apt_real py = this->box.ymi + this->width*(0.5 + static_cast<apt_real>(by));
		apt_real pz = this->box.ymi + this->width*(0.5 + static_cast<apt_real>(by));
		return p3d( px, py, pz );
	}

	return p3d(F32MX, F32MX, F32MX);
}


ostream& operator<<(ostream& in, t3x1 const & val)
{
	in << val.a11 << "," << val.a21 << "," << val.a31 << "\n";
	return in;
}


void plane3d::normaldistance_parameterization( t3x1 const & normal, p3d const & ponplane )
{
	real_m33 _magn = sqrt(SQR(normal.a11)+SQR(normal.a21)+SQR(normal.a31));
	real_m33 minusp = -1.f * ((normal.a11/_magn)*ponplane.x + (normal.a21/_magn)*ponplane.y + (normal.a31/_magn)*ponplane.z);
	cout << "magn/minusp = " << _magn << "\t\t" << minusp << "\n";
	this->n0 = normal.a11 / _magn;
	this->n1 = normal.a21 / _magn;
	this->n2 = normal.a31 / _magn;
	this->d = minusp;
	//Hesse normal form of plane3d with unit normal * point in space x + d == 0 if this holds point is on plane
	//set x = (0,0,0) worldcoordinate origin, see that d gives signed distance to origin
}


ostream& operator<<(ostream& in, plane3d const & val)
{
	in << "Plane normal " << val.n0 << "\t\t" << val.n1 << "\t\t" << val.n2 << "\n";
	in << "Plane sgn(d) " << val.d << "\n";
	return in;
}




ostream& operator<<(ostream& in, sqb const & val)
{
	in << "BinningNXYZ/width = " << val.nx << ";" << val.ny << ";" << val.nz << "\t\t" << val.nxy << ";" << val.nxyz << "\t\t" << val.width << "\n";
	in << "BinningBoundingBox = " << val.box << "\n";
	return in;
}


geomodel::geomodel(const apt_real _crb, const apt_real _crt, const apt_real _chcb,
		const apt_real _chct, const apt_real _UnitCellVolume, const size_t _N, const size_t _NperCell )
{
	this->crB = _crb;
	this->crT = _crt;
	this->chcapB = _chcb;
	this->chcapT = _chct;
	this->UnitCellVolume = _UnitCellVolume;
	this->N = _N;
	this->NperCell = _NperCell;

	/*
	apt_real c1 = this->crB;
	apt_real c2 = this->crT;
	apt_real c3 = this->chcapB;
	apt_real c4 = this->chcapT;
	apt_real aa = this->a; //in nanometer

	size_t Nuz = ATOMS_PER_UNITCELL_FCC;
	apt_real alpha = static_cast<apt_real>(N) * CUBE(aa);
	alpha *= 6.f / (static_cast<apt_real>(ATOMS_PER_UNITCELL_FCC) * MYPI);

	//volume factor total volume is top cap volume + conical frustum - bottom cap concavity
	apt_real beta = c4*SQR(c2)+CUBE(c4) + 2.f*(SQR(c1)+c1*c2+SQR(c2)) - (c3*SQR(c1)-CUBE(c3));
	apt_real HH = pow( (alpha / beta), (1.f/3.f) );
	*/

	apt_real c1 = this->crT;
	apt_real c2 = this->crB;
	apt_real c3 = this->chcapT;
	apt_real c4 = this->chcapB;
	//apt_real aa = this->a; //in nanometer

	apt_real alpha = (static_cast<apt_real>(N) / static_cast<apt_real>(NperCell)) * UnitCellVolume * 6.f / MYPI;
	//total volume is top cap volume + conical frustum - bottom cap concavity cap volume
	apt_real beta = c3*(3.f*SQR(c1)+SQR(c3)) + 2.f*(SQR(c2)+c2*c1+SQR(c1)) - c4*(3.f*SQR(c2)+SQR(c4));
	apt_real HH = pow( (alpha / beta), (1.f/3.f) );

	this->H = HH;
	this->rB = _crb * HH;
	this->rT = _crt * HH;
	this->hcapB = _chcb * HH;
	this->hcapT = _chct * HH;

	this->mybox.xmi = -1.f * this->rB;
	this->mybox.xmx = +1.f * this->rB;
	this->mybox.ymi = -1.f * this->rB;
	this->mybox.ymx = +1.f * this->rB;
	this->mybox.zmi = 0.f;
	this->mybox.zmx = this->H + this->hcapT;
	this->mybox.scale();

	cout << "alpha/beta = " << alpha << ";" << beta << "\n";
	cout << "H/rB/rT/hcB/hcT = " << this->H << ";" << this->rB << ";" << this->rT << ";" << this->hcapB << ";" << this->hcapT << "\n";
}


bool geomodel::is_inside( const p3d p)
{
	//MK::check if position p is inside tip volume
	apt_real rB = this->rB;		//radii of the conical frustum
	apt_real rT = this->rT;
	apt_real hB = this->hcapB;	//height of the bottom and top cap elements
	apt_real hT = this->hcapT;
	apt_real H = this->H;		//height of the conical frustum

	//MK::tip volume is axis-aligned with a right-handed cartesian coordinate system
	//the tip axis is parallel to z, the base of the convex hull about the tip is centered at
	//(0,0,0) such that the tip axis is the coordinate system z axis

	//check first if point is in z range above bottom cap but below top cap, most likely case
	apt_real rZ = rB - p.z/H * (rB-rT);
	apt_real sqrxy = SQR(p.x)+SQR(p.y);
	if ( p.z > hB && p.z < H ) { //frustum pillar part
		if ( sqrxy <= SQR(rZ) )
			return true;
		else
			return false;
	}
	else { //only two cases left: either in lower or upper half
		if ( p.z <= hB ) { //lower half
			if ( sqrxy > SQR(rZ) ) //outside conical frustum cross-section circle
				return false;
			else { //inside circle but might still be outside if inside the concave excluded section of the bottom cap
				if ( hB > EPSILON ) { //r = (a^2+h^2)/2h so catch degenerated case
					apt_real bottomradius = (SQR(rB)+SQR(hB)) / (2.f*hB);
					apt_real hhat = hB - p.z;
					apt_real SQRahat = 2.f*bottomradius*hhat - SQR(hhat);
					if ( sqrxy <= SQRahat )
						return false;
					else
						return true;
				}
				//height of the cap is negligible therefore the bottom of the frustum has negligible concavity in which case we are inside
				return true;
			}
		}
		else { //in upper half, only inside if in spherical cap atop of conical frustum
			if ( hT > EPSILON ) { //like with bottom case
				apt_real topradius = (SQR(rT)+SQR(hT)) / (2.f*hT);
				apt_real hhat = hT - (p.z - H);
				apt_real SQRahat = 2.f*topradius*hhat - SQR(hhat);
				if ( sqrxy > SQRahat )
					return false;
				else
					return true;
			}
			//height of cap is again negligible but this time p.z is above so we are outside
			return false;
		}
	}
}


/*
bool SortSpeciAscComposition( speci & first, speci & second )
{
	return first.c < second.c;
}


solutemodel::solutemodel()
{
	urng.seed( MT19937SEED );
	urng.discard( MT19937WARMUP );

	//##MK::debug building of composition, must be the same as in the range file
	//https://www.aircraftmaterials.com/data/aluminium/1200.html
	//http://arc.nucapt.northwestern.edu/refbase/files/Royset-2005.pdf

	//pure aluminiumdummy isotopes
	speci Al27 = speci( 1.000, 26.8, 1 );
	composition.push_back( Al27 );

	////speci Al27 = speci( 0.980, 26.8, 1 );
	////speci Fe56 = speci( 0.010, 55.93, 2 );
	////speci Si28 = speci( 0.008, 27.97, 3 );
	////speci Sc45 = speci( 0.002, 44.955, 4 );
	////composition.push_back( Al27 );
	////composition.push_back( Fe56 );
	////composition.push_back( Si28 );

	//sort and renormalize
	sort( composition.begin(), composition.end(), SortSpeciAscComposition );

	//as cdf is a copy we can modify now the concentration values to be a cdf
	apt_real renorm = 0.f;
	for(size_t i = 0; i < composition.size(); i++)
		renorm += composition.at(i).c;

	apt_real sum = 0.f;
	for(size_t i = 0; i < composition.size(); i++) {
		sum += composition.at(i).c;
		composition.at(i).c = sum / renorm;
//cout << "i/c " << i << "\t\t" << cdf.at(i).c << "\t\t" << cdf.at(i).typid << "\n";
	}
}

solutemodel::~solutemodel() {}


unsigned int solutemodel::get_random_speci_typid()
{
	//##MK::error checks

	//acceptance rejection sampling approach with composition CDF to pick a random ion's mass2charge
	//key idea sort compositions in ascending order based on composition compute CDF value

	//pick uniform random number on [0,1)
	uniform_real_distribution<apt_real> distribution(0.f,1.f);
	apt_real cdfval = distribution(urng);

	//use in the mentality of rejection sampling to get corresponding speci index
	for(auto it = composition.begin(); it != composition.end(); ++it) {
		if ( cdfval > it->c ) { //larger than bin end?
			continue;
		}
		return it->typid; //##MK::because for synthetic tips we do so far an in-place ranging;
	}

	//use speci index to get mass to charge
cout << "Unexpected control flow " << "\n";
	return UNKNOWNTYPE;
}


apt_real solutemodel::get_random_speci_mq()
{
	//##MK::error checks

	//acceptance rejection sampling approach with composition CDF to pick a random ion's mass2charge
	//key idea sort compositions in ascending order based on composition compute CDF value

	//pick uniform random number on [0,1)
	uniform_real_distribution<apt_real> distribution(0.f,1.f);
	apt_real cdfval = distribution(urng);

	//use in the mentality of rejection sampling to get corresponding speci index
	for(auto it = composition.begin(); it != composition.end(); ++it) {
		if ( cdfval > it->c ) { //larger than bin end?
			continue;
		}
		return it->m2q;
	}

	//use speci index to get mass to charge
cout << "Unexpected control flow " << "\n";
	return 0.f;
}
*/



unitcellaggr::unitcellaggr()
{
	this->parms = unitcellparms();
	this->a = 0.f;

	this->umin = 0;
	this->umax = 0;
	this->vmin = 0;
	this->vmax = 0;
	this->wmin = 0;
	this->wmax = 0;

	this->a1 = v3d();
	this->a2 = v3d();
	this->a3 = v3d();

	this->base.clear();
}


unitcellaggr::unitcellaggr( unitcellparms const & theseparms, aabb3d unitbox, const unsigned int model)
{
	parms.a = theseparms.a;
	parms.b = theseparms.b;
	parms.c = theseparms.c;
	parms.alpha = theseparms.alpha;
	parms.beta = theseparms.beta;
	parms.gamma = theseparms.gamma;
	a = parms.a; //##MK::obsolete

	//initialize cubic base vectors
	a1 = v3d( parms.a*1.f, 0.f, 0.f);
	a2 = v3d( 0.f, parms.a*1.f, 0.f);
	a3 = v3d( 0.f, 0.f, parms.a*1.f);

	if ( model == AL3SC ) {
		//initialize Al3Sc base atoms
		base.push_back( p3d(0.f, 0.f, 0.f) ); //Sc 8x 1/8 = 1
		base.push_back( p3d(0.5, 0.5, 0.f) ); //Al
		base.push_back( p3d(0.f, 0.5, 0.5) ); //Al
		base.push_back( p3d(0.5, 0.f, 0.5) ); //Al 6x 1/2 = 3 Al --> Al3Sc okay
	}
	else if ( model == AL3LI ) {
		//initialize Al3Sc base atoms
		base.push_back( p3d(0.f, 0.f, 0.f) ); //Li 8x 1/8 = 1
		base.push_back( p3d(0.5, 0.5, 0.f) ); //Al
		base.push_back( p3d(0.f, 0.5, 0.5) ); //Al
		base.push_back( p3d(0.5, 0.f, 0.5) ); //Al 6x 1/2 = 3 Al --> Al3Li okay
	}
	else if ( model == ALUMINIUM ) { //FCC
		//initialize fcc base atoms
		base.push_back( p3d(0.f, 0.f, 0.f) ); //Al 8x 1/8 = 1
		base.push_back( p3d(0.5, 0.5, 0.f) ); //Al
		base.push_back( p3d(0.f, 0.5, 0.5) ); //Al
		base.push_back( p3d(0.5, 0.f, 0.5) ); //Al 6x 1/2 = 3 Al --> 4 units per EZ okay
	}
	else { //== WOLFRAM
		base.push_back( p3d(0.f, 0.f, 0.f) ); //W 8x 1/8 = 1
		base.push_back( p3d(0.5, 0.5, 0.5) ); //W 1x 1/1 = 1 --> 2 unit per EZ okay
	}

	//unitbox gives min/max dimensions in nanometer that we have to fill construct on positive sectors of \mathcal{R}^3
	umin = static_cast<int>(floor(unitbox.xmi / parms.a));
	umax = static_cast<int>(ceil(unitbox.xmx / parms.a));
	vmin = static_cast<int>(floor(unitbox.ymi / parms.a));
	vmax = static_cast<int>(ceil(unitbox.ymx / parms.a));
	wmin = static_cast<int>(floor(unitbox.zmi / parms.a));
	wmax = static_cast<int>(ceil(unitbox.zmx / parms.a));

	//unitbox is axis-aligned to standard orientation 0.0, 0.0, 0.0 Bunge Euler fcc crystal lattice
}


unitcellaggr::unitcellaggr( const unitcellparms theseparms, aabb3d unitbox )
{
	//##MK::updated version of lattice generator uses
	parms.a = theseparms.a;
	parms.b = theseparms.b;
	parms.c = theseparms.c;
	parms.alpha = theseparms.alpha;
	parms.beta = theseparms.beta;
	parms.gamma = theseparms.gamma;
	a = theseparms.a;

	//initialize face centered orthorhombic base vectors, ##MK::no primitive vectors!
	a1 = v3d( theseparms.a*1.f, 0.f, 				0.f );
	a2 = v3d( 0.f, 				theseparms.b*1.f, 	0.f );
	a3 = v3d( 0.f, 				0.f, 				theseparms.c*1.f );

	//initialize orthorhombically distorted fcc cubic
	base.push_back( p3d(0.f, 0.f, 0.f) ); //Al 8x 1/8 = 1
	base.push_back( p3d(0.5, 0.5, 0.f) ); //Al
	base.push_back( p3d(0.f, 0.5, 0.5) ); //Al
	base.push_back( p3d(0.5, 0.f, 0.5) ); //Al 6x 1/2 = 3 Al --> 4 units per EZ okay

	//unitbox gives min/max dimensions in nanometer that we have to fill construct on positive sectors of \mathcal{R}^3
	//##MK::this defining of the bounding box works only for cubic, orthorhombic, and tetragonal
	umin = static_cast<int>(floor(unitbox.xmi / theseparms.a));
	umax = static_cast<int>(ceil(unitbox.xmx / theseparms.a));
	vmin = static_cast<int>(floor(unitbox.ymi / theseparms.b));
	vmax = static_cast<int>(ceil(unitbox.ymx / theseparms.b));
	wmin = static_cast<int>(floor(unitbox.zmi / theseparms.c));
	wmax = static_cast<int>(ceil(unitbox.zmx / theseparms.c));

	//unitbox is axis-aligned to standard orientation 0.0, 0.0, 0.0 Bunge Euler fcc crystal lattice
}


unitcellaggr::unitcellaggr( const unitcellparms theseparms, aabb3d unitbox, p3d rebase, const unsigned int model )
{
	//##MK::updated version of lattice generator uses
	parms.a = theseparms.a;
	parms.b = theseparms.b;
	parms.c = theseparms.c;
	parms.alpha = theseparms.alpha;
	parms.beta = theseparms.beta;
	parms.gamma = theseparms.gamma;
	a = theseparms.a;

	//initialize face centered orthorhombic base vectors, ##MK::no primitive vectors!
	a1 = v3d( theseparms.a*1.f, 0.f, 				0.f );
	a2 = v3d( 0.f, 				theseparms.b*1.f, 	0.f );
	a3 = v3d( 0.f, 				0.f, 				theseparms.c*1.f );

	//initialize orthorhombically distorted fcc cubic
	/*
	base.push_back( p3d(0.f, 0.f, 0.f) ); //Al 8x 1/8 = 1
	base.push_back( p3d(0.5, 0.5, 0.f) ); //Al
	base.push_back( p3d(0.f, 0.5, 0.5) ); //Al
	base.push_back( p3d(0.5, 0.f, 0.5) ); //Al 6x 1/2 = 3 Al --> 4 units per EZ okay
	*/
	if ( model == AL3SC ) {
		//initialize Al3Sc base atoms
		base.push_back( p3d(0.f, 0.f, 0.f) ); //Sc 8x 1/8 = 1
		base.push_back( p3d(0.5, 0.5, 0.f) ); //Al
		base.push_back( p3d(0.f, 0.5, 0.5) ); //Al
		base.push_back( p3d(0.5, 0.f, 0.5) ); //Al 6x 1/2 = 3 Al --> Al3Sc okay
	}
	else if ( model == ALUMINIUM ) { //FCC
		//initialize fcc base atoms
		base.push_back( p3d(0.f, 0.f, 0.f) ); //Al 8x 1/8 = 1
		base.push_back( p3d(0.5, 0.5, 0.f) ); //Al
		base.push_back( p3d(0.f, 0.5, 0.5) ); //Al
		base.push_back( p3d(0.5, 0.f, 0.5) ); //Al 6x 1/2 = 3 Al --> 4 units per EZ okay
	}
	else { //== WOLFRAM
		base.push_back( p3d(0.f, 0.f, 0.f) ); //W 8x 1/8 = 1
		base.push_back( p3d(0.5, 0.5, 0.5) ); //W 1x 1/1 = 1 --> 2 unit per EZ okay
	}

	//unitbox gives min/max dimensions in nanometer that we have to fill construct on positive sectors of \mathcal{R}^3
	//##MK::this defining of the bounding box works only for cubic, orthorhombic, and tetragonal
	umin = static_cast<int>(floor((unitbox.xmi - rebase.x) / theseparms.a));
	umax = static_cast<int>(ceil((unitbox.xmx - rebase.x) / theseparms.a));
	vmin = static_cast<int>(floor((unitbox.ymi - rebase.y) / theseparms.b));
	vmax = static_cast<int>(ceil((unitbox.ymx - rebase.y) / theseparms.b));
	wmin = static_cast<int>(floor((unitbox.zmi - rebase.z) / theseparms.c));
	wmax = static_cast<int>(ceil((unitbox.zmx - rebase.z) / theseparms.c));

	cout << "Rebasing lattice" << "\n";

	//unitbox is axis-aligned to standard orientation 0.0, 0.0, 0.0 Bunge Euler fcc crystal lattice
}


unitcellaggr::~unitcellaggr(){}


p3d unitcellaggr::get_atom_pos(const size_t b, const int u, const int v, const int w)
{
	apt_real uu = static_cast<apt_real>(u);
	apt_real vv = static_cast<apt_real>(v);
	apt_real ww = static_cast<apt_real>(w);

	//##MK::implicit origin at 0,0,0
	p3d res = p3d(
			(base[b].x + uu)*a1.u + (base[b].y + vv)*a2.u + (base[b].z + ww)*a3.u,
			(base[b].x + uu)*a1.v + (base[b].y + vv)*a2.v + (base[b].z + ww)*a3.v,
			(base[b].x + uu)*a1.w + (base[b].y + vv)*a2.w + (base[b].z + ww)*a3.w  );

	return res;
}

/*
void msphere::get_atoms( vector<pos> & out )
{
	//##MK::unrotated Al3Sc ideal unit cell centered at sphere center
	//https://materials.springer.com/isp/crystallographic/docs/sd_1922024
	speci Al27 = speci( 0.00, 26.8, 1 );
	speci Sc45 = speci( 0.00, 44.955, 4 );
	apt_real Al3ScLatticeConstant = 0.410; //nm

	apt_real OriginShift = 0.5 * Al3ScLatticeConstant; //MK::base unit cell center is defined at sphere center

	p3d c = center;
	apt_real r = radius;

//cout << "center/c.x/c.y/c.z/r\t\t" << c.x << ";" << c.y << ";" << c.z << "\t\t" << r << "\n";

	//aabb3d window = aabb3d( c.x-r, c.x+r, c.y-r, c.y+r, c.z-r, c.z+r );
	aabb3d window = aabb3d( -1.f*r, +1.f*r, -1.f*r, +1.f*r, -1.f*r, +1.f*r );

	unitcellaggr al3sclatt = unitcellaggr( Al3ScLatticeConstant, window, AL3SC );

//cout << "latt/a\t\t\t" << al3sclatt.a << "\n";
//cout << "latt/cx/umi/umx\t\t" << c.x << "\t\t" << al3sclatt.umin << "\t\t" << al3sclatt.umax << "\n";
//cout << "latt/cy/vmi/vmx\t\t" << c.y << "\t\t" << al3sclatt.vmin << "\t\t" << al3sclatt.vmax << "\n";
//cout << "latt/cz/wmi/wmx\t\t" << c.z << "\t\t" << al3sclatt.wmin << "\t\t" << al3sclatt.wmax << "\n";

	for(size_t b = 0; b < al3sclatt.base.size(); ++b) {
		//unsigned int mark = (b == 0) ? 4 : 1; //##MK::first defined is Scandium rest Al
		apt_real mass2charge = (b == 0) ? Sc45.m2q : Al27.m2q;

		for(int w = al3sclatt.wmin; w <= al3sclatt.wmax; ++w) {
			for(int v = al3sclatt.vmin; v <= al3sclatt.vmax; ++v) {
				for(int u = al3sclatt.umin; u <= al3sclatt.umax; ++u) {

					p3d ap = al3sclatt.get_atom_pos(b, u, v, w); //##MK::applying temporary shift of origin

					if ( (SQR(ap.x-OriginShift)+SQR(ap.y-OriginShift)+SQR(ap.z-OriginShift)) <= SQR(r) ) {
						out.push_back( pos(
								ap.x - OriginShift + c.x,
								ap.y - OriginShift + c.y,
								ap.z - OriginShift + c.z, mass2charge) );
					} //atom is inside defined sphere
				}
			}
		}
	} //next base
}


bool SortRadiiDesc( apt_real & first, apt_real & second )
{
	return first > second;
}

secondphasemodel::secondphasemodel(const geomodel & geom,
			const size_t N, const apt_real rm, const apt_real rvar )
{
	urng.seed( MT19937SEED );
	urng.discard( MT19937WARMUP );

	//##MK::rm and rvar are in nanometer however we require the distribution parameter mu and sigma
	vector<apt_real> radii;
	lognormal_distribution<apt_real> lognrm( rm, rvar ); //##MK::is in nanometer!
	for( size_t i = 0; i < N; ++i) {
		//fixed size radius value
		radii.push_back( rm );
		//distributed radius value
		//radii.push_back( lognrm(urng) );
	}

	//sorting by descending radius improves packing process speed
	sort( radii.begin(), radii.end(), SortRadiiDesc );

//##MK::DEBUG#############for( size_t i = 0; i < radii.size(); ++i)  { cout << radii.at(i) << "\n"; } cout << "\n" << "\n";

	uniform_real_distribution<apt_real> unifrnd(0.f,1.f);
	size_t itermax = 100000; //at most so many attempts to place a particle
	size_t iter = 0;
	size_t pid = 0;

	while( particles.size() < N && iter < itermax ) { //attempt placement
		//pick a place at random
		p3d rnd = p3d(
				geom.mybox.xmi + (unifrnd(urng) * geom.mybox.xsz),
				geom.mybox.ymi + (unifrnd(urng) * geom.mybox.ysz),
				geom.mybox.zmi + (unifrnd(urng) * geom.mybox.zsz) );

//cout << "particlesSize/N/iter/itermax\t\t" << particles.size() << ";" << N << ";" << iter << ";" << itermax << "\t\t" << rnd.x << ";" << rnd.y << ";" << rnd.z << "\n";

		//overlapping with existent spherical cluster ?
		//MK::precipitates can protrude outside mybox
		bool contact = false;
		//MK::test until intrusion or touching particle found if any
		for( auto pt = particles.begin(); pt != particles.end(); ++pt ) {
			//apt_real distnow = sqrt( SQR(rnd.x - pt->center.x) + SQR(rnd.y - pt->center.y) + SQR(rnd.z - pt->center.z) ); //##MK::sqrt can be avoided here work with SQR values instead
			//apt_real distcrt = radii.at(pid) + pt->radius;

			apt_real distnow = SQR(rnd.x - pt->center.x) + SQR(rnd.y - pt->center.y) + SQR(rnd.z - pt->center.z);
			apt_real distcrt = SQR(radii.at(pid) + pt->radius);

//cout << "\t\t\t" << distnow << "\t\t" << distcrt << "\n";
			if ( distnow <= distcrt ) { //most likely the more particles are placed the more overlap
				contact = true;
				break; //MK::we can break if we found at least one overlap
			}
		}

		//decision
		if ( contact == true ) { //later more and more likely
			iter++;
		}
		else { //place the particle
			particles.push_back( msphere( rnd, radii.at(pid) ));
//cout << "rnd/pid/iter\t\t" << rnd.x << ";" << rnd.y << ";" << rnd.z << "\t\t" << pid << "\t\t" << iter << "\n";
			pid++;
			iter = 0; //reset counter because next particle placed will again be allowed only maximum
		}
	} //fill 3d box geom.mybox with N randomly placed particles of mean size rm and variance rvar
}


void secondphasemodel::reportParticles( const unsigned int simid, const int rank )
{
	//##MK::suboptimal... one file per rank
	string fn = "PARAPROBE.SimID." + to_string(simid) + ".Rank." + to_string(rank) + ".SyntheticTipCluster.csv";

	ofstream csvlog;
	csvlog.open(fn.c_str(), ofstream::out | ofstream::trunc);
	if (csvlog.is_open() == true) {
		//header
		csvlog << "ClusterID;BarycenterX;BarycenterY;BarycenterZ;Radius\n";
		csvlog<< ";nm;nm;nm;nm\n";
		csvlog << "ClusterID;BarycenterX;BarycenterY;BarycenterZ;Radius\n";

		//report
		for (size_t cid = 0; cid < particles.size(); ++cid ) {
			csvlog << cid << ";" << particles.at(cid).center.x << ";" << particles.at(cid).center.y << ";" << particles.at(cid).center.z << ";" << particles.at(cid).radius << "\n";
		}

		csvlog.flush();
		csvlog.close();
	}
	else {
		cerr << "Unable to write synthetic tip placed particles" << "\n";
	}
}


void secondphasemodel::reportParticlesVTK( const unsigned int simid, const int rank )
{
	//MK::write VTK file showing barycenter of all synthesized cluster in reconstructed space
	//includes particles outside actual tip
	string vtk_io_fn = "PARAPROBE.SimID." + to_string(simid) + ".Rank." + to_string(rank) + ".SyntheticTipCluster.vtk";

	ofstream vtk;
	vtk.open(vtk_io_fn.c_str(), ofstream::out | ofstream::trunc);
	if (vtk.is_open() == true) {
		//header
		vtk << "# vtk DataFile Version 2.0\n";
		vtk << "PARAPROBE Synthetic tip cluster placed\n";
		vtk << "ASCII\n";
		vtk << "DATASET POLYDATA\n";
		vtk << "POINTS " << particles.size() << " double\n";
		for( auto it = particles.begin(); it != particles.end(); ++it ) {
			vtk << it->center.x << " " << it->center.y << " " << it->center.z << "\n";
		}
		vtk << "\n";
		vtk << "VERTICES " << particles.size() << " " << 2*particles.size() << "\n";
		for( size_t i = 0; i < particles.size(); ++i ) {
			vtk << 1 << " " << i << "\n";
		}
		vtk << "\n";
		vtk << "POINT_DATA " << particles.size() << "\n";
		vtk << "FIELD FieldData 1\n";
		vtk << "Radius 1 " << particles.size() << " float\n";
		for( auto it = particles.begin(); it != particles.end(); ++it ) {
			vtk << it->radius << "\n";
		}
		vtk.flush();
		vtk.close();
		cout << "VTK file of synthesized particles positions written to file" << "\n";
	}
	else {
		cout << "VTK file of synthesized particles positions was not written" << "\n";
	}
}
*/


ostream& operator<<(ostream& in, occupancy_geometry const & val)
{
	in << "Result of voxelization is as follows" << "\n";
	in << "TotalBins\t\t" << val.ntotal << "\n";
	in << "VacuumBins\t\t" << val.nvacuum << "\n";
	in << "SurfaceBins\t\t" << val.nsurface << "\n";
	in << "InsideBins\t\t" << val.ninside << "\n";
	in << "SurfaceIons\t\t" << val.nions_surface << "\n";
	in << "InsideIons\t\t" << val.nions_inside << "\n";
	in << "InsideVolume\t\t" << val.volume_inside << " (nm^3)\n";
	return in;
}



ostream& operator<<(ostream& in, occupancy_ions const & val)
{
	in << "Inside/Surface/TypeLabel\t\t" << val.nions_inside << "\t\t" << val.nions_surface << "\t\t" << val.iontype << "\n";
	return in;
}


ostream& operator<<(ostream& in, localmaximum const & val)
{
	//##MK::for FFT peaks
	in << "Peakposition = " << val.position << " (1/nm) strength = " << val.strength << " (a.u.)\n";
	return in;
}


ostream& operator<<(ostream& in, localvalpos const & val)
{
	//##MK::for FFT peaks
	in << "Elev/Azim ea = " << val.pos << " strength = " << val.strength << " (a.u.)\n";
	return in;
}


