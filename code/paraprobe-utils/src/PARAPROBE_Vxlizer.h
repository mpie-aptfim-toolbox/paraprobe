//##MK::GPLV3

#ifndef __PARAPROBE_UTILS_VXLIZER_H__
#define __PARAPROBE_UTILS_VXLIZER_H__

//here is the level where we loop in all tool-specific utils

#include "PARAPROBE_KDTree.h"

class vxlizer
{
public:
	vxlizer();
	~vxlizer();
	
	sqb vxlgrid;
	bool* IsVacuum;		//a bin with no ions laying in vacuum aka outside the tip
	bool* IsSurface;	//a bin with at least one ion in the Moore neighborhood of the vacuum
	bool* IsInside;		//a bin entirely enclosed with no Moore neighbor that is IsSurface == true
};

#endif
