//##MK::GPLV3


#ifndef __PARAPROBE_UTILS_VOLUMESAMPLER_H__
#define __PARAPROBE_UTILS_VOLUMESAMPLER_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_Decompositor.h"

struct mp3d
{
	apt_real x;						//center of the ROI
	apt_real y;
	apt_real z;
	apt_real R;						//radius of the spherical ROI
	unsigned int mpid;				//ID of the point, no implicit IDs!
	unsigned int nions;				//number of ions in the interior or on the boundary of the ROI
	mp3d() : x(0.f), y(0.f), z(0.f), R(0.f), mpid(UINT32MX), nions(0) {}
	mp3d( const apt_real _x, const apt_real _y, const apt_real _z, 
		const apt_real _r, const unsigned int _mpid, const unsigned int _nions ) :
		x(_x), y(_y), z(_z), R(_r), mpid(_mpid), nions(_nions) {}
};

ostream& operator << (std::ostream& in, mp3d const & val);


class volsampler
{
	//the class is a utility tool to implement different ways how to sample ROI in the tip volume
public:
	volsampler();
	~volsampler();
	
	void define_probe_volume( aabb3d const & wdw );					//which volume and region of the specimen is probed?
	void define_regular_grid( const apt_real dx, const apt_real dy, const apt_real dz, const apt_real R,
			sqb & bx, const bool* inside, vector<tri3d> const & trihull );
	void define_regular_grid( const apt_real dx, const apt_real dy, const apt_real dz, const apt_real R );
	void define_single_point( p3d const & p, const apt_real R );
	void define_random_cloud_fixed( const unsigned int nrois, const apt_real R,
			sqb & bx, const bool* inside, vector<tri3d> const & trihull );
	void define_random_cloud_fixed( const unsigned int nrois, const apt_real R );
	void define_locally_refined_grid( vector<p3d> const & barycenter, const apt_real R,
			const apt_real old_binwidth_i, const unsigned int new_subdiv_i,
			sqb & bx, const bool* inside, vector<tri3d> const & trihull );
	
	aabb3d window;					//the region of interest in which the material points are probed, typically the entire tip or a cuboidal section
	vector<mp3d> matpoints;			//the individual regions of interest where we want to mine for local crystallographic information
};

#endif
