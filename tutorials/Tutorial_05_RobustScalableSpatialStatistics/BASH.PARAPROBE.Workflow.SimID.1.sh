#!/bin/bash

echo "OpenMP NUM THREADS env option"
echo $OMP_NUM_THREADS
echo "OpenMP PLACES env option"
echo $OMP_PLACES
ulimit -s unlimited
echo "ulimit env option"
ulimit


###  paraprobe tool workflow SimID 1

mpiexec -n 1 ./paraprobe_spatstat 1 PARAPROBE.Spatstat.SimID.1.xml 1>PARAPROBE.Spatstat.SimID.1.STDOUT.txt 2>PARAPROBE.Spatstat.SimID.1.STDERR.txt

