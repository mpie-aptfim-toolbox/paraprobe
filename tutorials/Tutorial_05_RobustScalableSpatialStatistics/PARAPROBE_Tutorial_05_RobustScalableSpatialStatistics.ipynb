{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial 5: Compute spatial statistics and two-point statistics<br>(nearest neighbors, radial distribution, and spatial distribution maps)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Motivation and approach"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this tutorial we cover several of the key methods on APT datasets. Spatial statistics and two-point statistics! These enable us to characterize the distribution (and direction) of distances between an ensemble of points, here points representing positions of atoms of specific atom types. A number of tools have been developed for this task, both by commercial software companies and the scientific community. My motivation to develop yet another tool for these quantification was to give people a tool which supports flexible batch processing with strong scaling performance to handle even the largest APT datasets out there, to quantify uncertainties for above statistics with these methods, document the metadata using HDF5, and have all this in a completely open-source tool."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In fact, APT practitioners frequently reported me that spatial statistics for large ROIs and large dataset were slow, eventually crashed, took forever with many of the existent solutions. Especially, worrying to me was that when executing with commercial software it remain unclear what is getting computed in detail and how are sources of bias handled. Such bias can occur if for instance regions-of-interest (ROIs) close to the edge of the dataset are analyzed but it is not clearly accounted for whether these ROIs extend beyond the point cloud or not or whether other corrective measures are applied."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Admittedly, computing spatial statistics can be costly because we need to inspect some neighborhood of eventually every atom and build histograms of the distances. If we have multi-hundred million point clouds and different atom types to distinguish there is no reason to click GUIs here. This is where automated tools can serve us much more productively."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What will we learn?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* How to define batch queues for processing combinations of atom types and different statistics, reutilizing spatial information as best as possible.\n",
    "* How to handle the edge correction rigorously to assure that the ROIs remain in the point cloud interior.\n",
    "* How to automate the creation of figures for histograms and two-point statistics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## *paraprobe-spatstat* is a strong-scaling high-throughput tool for<br>quantifying statistics and two-point statistics for APT datasets"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We use multithreading and process parallelism which offers scalability to thousands of computing cores to be able to handle even the most demanding, i.e. billion atom datasets without any need for downsampling or making of compromises."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The workflow with *paraprobe-spatstat* is straightforward:\n",
    "1. **Load** collection of **datasets** in question. \n",
    "2. **Define combinations** of **atom types** and **potentially multiple spatial statistics**\n",
    "3. **Load the distances** of the atoms to the dataset edge to quantify and eliminate bias from edge effects.\n",
    "\n",
    "Currently, paraprobe implements spherical ROIs. In return, we get a collection of histograms, one-dimensional histograms for $k^{th}$ nearest neighbors and radial distribution functions and three-dimensional histograms, respectively for spatial distribution maps (SDMs)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Atoms on the edge of the dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Atoms at the edge of the dataset require particular care. In fact, a ROI which protrudes beyond the dataset edge contains fewer atoms on average, resulting in quantitative bias in the counting statistics. Therefore, *paraprobe-spatstat* distinguishes two categories of atoms, irregardless of their type. Atoms, with a threshold distance below $d_{srf}$ to the dataset edge versus atoms which are embedded deeper in the point cloud. ROIs are placed only at the locations of the deeper embedded atoms. Thereby, we can account for the atoms close to the edge of the dataset. These atoms do not get own ROIs assigned but can get counted by deeper embedded ROIs.\n",
    "For this edge correction it pays off that we use the atom-to-edge distances computed by *paraprobe-surfacer* as this works for point cloud with regions that can be convex or concave, so there is support for arbitrarily shaped point clouds."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Atom type label randomization"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course *paraprobe-spatstat* computes each distribution always for the original and randomized atom type labels."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 1: Detail what you want to do"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We continue working with the three example datasets. First, we use *paraprobe-parmsetup* to define which analyses we want to perform. Like in the previous tutorials, there is a specific tool in *paraprobe-parmsetup* which serves *paraprobe-spatstat*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define the location of the paraprobe/code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "basePath = '/home/markus/ParaprobeVideoTutorials/paraprobe'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load Python and *paraprobe-parmsetup*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "#load relevant Python3 (standard) packages\n",
    "import os\n",
    "import sys\n",
    "import glob\n",
    "from pathlib import Path\n",
    "import numpy as np\n",
    "#check for existence of specific non-standard packages we also need\n",
    "try:\n",
    "    import periodictable\n",
    "except ImportError as e:\n",
    "    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')\n",
    "try:\n",
    "    import h5py\n",
    "except ImportError as e:\n",
    "    raise ValueError('Install h5py Python package via e.g. pip install h5py !')\n",
    "\n",
    "#import the paraprobe-parmsetup tools we need\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/')\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/src/')\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/src/utils/')\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/src/metadata/')\n",
    "#from PARAPROBE_SlurmSingle import *\n",
    "#from PARAPROBE_BashBatch import *\n",
    "from PARAPROBE_BashSingle import *\n",
    "from PARAPROBE_XML import *\n",
    "from PARAPROBE_Surfacer import *\n",
    "from PARAPROBE_Spatstat import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define your computer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MyComputer = { \n",
    "    'Compiler': 'GNU', \n",
    "    'NumberOfComputingNodes': 1,\n",
    "    'NumberOfSocketsPerNode': 1,\n",
    "    'NumberOfProcessesPerSocket': 1 }\n",
    "#'Compiler' alternatively 'ITL' or 'GNU'\n",
    "ProcessesToUse = MyComputer['NumberOfComputingNodes'] * MyComputer['NumberOfSocketsPerNode'] * MyComputer['NumberOfProcessesPerSocket']\n",
    "print('Planning for parallel execution with ' + str(ProcessesToUse) + ' MPI processes')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Like I mentioned in previous tutorials, using more processes speeds up the analyses.<br>All processes and threads work always coorporatively on a dataset before processing the next dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Setting up XML configuration files for *paraprobe-spatstat* is similar to previous tools. We need to define which atom types we want to combine using *add_iontype_combi* commands. The first argument specifies a comma-separated list of desired target atom types. These are atom types at which we place ROIs. The second argument specifies a comma-separated list of desired neighbor atom types. These are atom types for which distances to the target (the ROI center) are computed. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Like for *paraprobe-ranger*, append a colon to all element names with a single character. Let's make an example: assume you have a dataset with aluminium, hydrogen, and gallium contaminants. If you specify *add_iontype_combi( 'Al', 'H:' )* paraprobe-spatsat places ROIs at every Al atom and checks for distances to hydrogen atoms in each ROI."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you specify *add_iontype_combi( 'H:,Al', 'Ga,H:' )* ROIs will be placed at all hydrogen and aluminium atoms and distances computed to gallium and hydrogen neighbors. If you compute in this case the $k^{th}$ nearest neighbor that neighbor can be either hydrogen or gallium, whichever has the $k^{th}$ shortest distance. So a typical script for instructing the following:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Al-Al, Sc-Sc, Al-Sc, Sc-Al pairs, original labels and randomized labels, for all these pairings compute:\n",
    "    * Radial distribution functions with ROIs of $5nm$ radius using $0.001nm$ binning\n",
    "    * $k^{th}$ nearest neighbors with $k = 1, 10, 100$ and the same radius for the ROIs and same binning\n",
    "    * Three-dimensional spatial distribution maps of the $k^{th}$ nearest neighbor with $k = 1, 10, 100$, same ROI radius but coarser binning $0.1nm$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Is shown here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds_srf = np.array( [['PARAPROBE.Transcoder.Results.SimID.1.h5','PARAPROBE.Surfacer.Results.SimID.1.h5'],\n",
    "                    ['PARAPROBE.Transcoder.Results.SimID.2.h5','PARAPROBE.Surfacer.Results.SimID.2.h5'],\n",
    "                    ['PARAPROBE.Synthetic.Results.SimID.3.h5','PARAPROBE.Surfacer.Results.SimID.3.h5']])\n",
    "\n",
    "SimulationID = 0\n",
    "#AnalysisJobsSlurm = {}\n",
    "AnalysisJobsBash = {}\n",
    "for dataset in ds_srf:\n",
    "    SimulationID += 1\n",
    "    print('Creating configuration for spatstat SimID.' + str(SimulationID))\n",
    "    WhatToDo = {}\n",
    "    \n",
    "    recon_fnm = dataset[0]\n",
    "    hull_fnm = dataset[1]\n",
    "    \n",
    "    #create paraprobe-parmsetup class specialization for spatstat tool\n",
    "    task = paraprobe_spatstat( SimulationID, recon_fnm, hull_fnm )\n",
    "    #both the synthetic and experimental datasets have Al and Sc atoms\n",
    "    #lets define which combinations of atom types we want to study\n",
    "    task.add_iontype_combi( 'Al', 'Al' )\n",
    "    task.add_iontype_combi( 'Sc', 'Sc' )\n",
    "    task.add_iontype_combi( 'Al', 'Sc' )\n",
    "    #lets add a consistency test \n",
    "    task.add_iontype_combi( 'Sc', 'Al' )\n",
    "    #you might tend to say the combinations are invertible, yes but the counts are different!\n",
    "    #say you have 1 in 10 atoms Sc in an Al matrix, then you place approx ten times less ROIs for the Sc-Al pairing than for Al-Sc compared to Al-Sc!\n",
    "    #okay, lets do some spatial statistics\n",
    "    #e.g. radial distribution function\n",
    "    #we start always at 0.0, linear interval spacing here in steps of 0.001, up to the right interval bound\n",
    "    #here set to 5nm\n",
    "    task.set_rdf( 0.001, 5.000 )\n",
    "    #e.g. kth nearest neighbors for several kth, here 1, 10, 100 \n",
    "    task.set_knn( 0.001, 5.000, np.array([1, 10, 100]) ) #add the kth combinations in the numpy  array\n",
    "    #yes a single line gives you all combinations, easy peasy\n",
    "    #e.g. why not also three-dimensional environment i.e. directional spatial statistics, i.e. 3D spatial distribution maps\n",
    "    #be careful here, paraprobe-spatstat currently gives you a voxelated/3D histogram this can take much memory,\n",
    "    #consult the documentation of the paraprobe-spatstat tool to get more information, again compute here\n",
    "    #e.g. k=1, k=10 interested in the 100th as well, now worries\n",
    "    task.set_sdm( 0.1, 5.0, np.array([1, 10, 100]) )\n",
    "    WhatToDo['1_Spatstat'] = task.run( ProcessesToUse )\n",
    "    del task\n",
    "    \n",
    "    myjobname = str('MK') + str(SimulationID)\n",
    "    #slm = paraprobe_slurm_single( myjobname[0:9], SimulationID, 'TALOS', ProcessesToUse, WhatToDo, Compiler )\n",
    "    #slm.write_slurm_script()\n",
    "    #but also write a bash script (to be submitted to a local workstation without super computer)\n",
    "    bsh = paraprobe_bash_single( myjobname[0:9], SimulationID, '', ProcessesToUse, WhatToDo, MyComputer['Compiler'] )\n",
    "    bsh.write_bash_script()\n",
    "    del WhatToDo\n",
    "    \n",
    "    #AnalysisJobsSlurm[N] = slm.get_slurm_script_filename()\n",
    "    AnalysisJobsBash[SimulationID] = bsh.get_bash_script_filename()\n",
    "\n",
    "print('All configuration files created')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Voila, *paraprobe-spatstat* XML configuration files and SH files. For a total of three datasets, four combinations of atom types, RDF, 1NN, 10NN, 100NN and 3D SDMs for k=1, 10, 100."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ls PARAPROBE.Spatstat.SimID.*.xml\n",
    "! ls BASH.PARAPROBE.Workflow.SimID.*.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 2: Execute *paraprobe-spatstat*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nothing new for us here: take the H5, XML, and SH files where you want to execute *paraprobe-spatstat*. Thereafter, execute *paraprobe-spatstat* either interactively via the console or by submitting to queue using scripts. In effect, *paraprobe-spatstat* will store the results in a HDF5 file."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 3: Inspect the results with *paraprobe-autoreporter*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So with above tool you can compute as many spatial statistics as you want. Historically, people parsed these out manually from spreadsheets and visualized these according to their convenience. If you have multiple combinations and results to manage, doing this manually becomes quickly very tedious and error-prone work. \n",
    "\n",
    "Therefore, I thought it would be useful to have a tool which post-processes the spatial statistics in such an automated way that we do not have to worry how to plot things. Let the computer do this. Instead, we can invest our time in discussing the results. Previous tutorials showed already that such automation strategy works with *paraprobe-autoreporter*.\n",
    "Working with this tool is for course not mandatory. You are always free to work with the two-dimensional data arrays in the H5 files directly. In many cases, I am convinced, though, *paraprobe-autoreporter* is the much smarter and more convenient solution, once you have familiarized yourself with how to set such analyses up.\n",
    "\n",
    "So let's do this together so see that it is simple."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load paraprobe-autoreporter"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#import the paraprobe-autoreporter tools we need\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/latex/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/metadata/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/plotting/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/utils/')\n",
    "from PARAPROBE_Autoreporter_Numerics import *\n",
    "from PARAPROBE_Autoreporter_CorpDesign import *\n",
    "from PARAPROBE_Autoreporter_Profiler import *\n",
    "from PARAPROBE_Autoreporter_LatexGenerator import *\n",
    "\n",
    "from PARAPROBE_Autoreporter_Surfacer import *\n",
    "from PARAPROBE_Autoreporter_Tessellator import *\n",
    "from PARAPROBE_Autoreporter_Spatstat import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In principle there is nothing really new now, we set up a report with a corresponding autoreporter_spatstat class. However, radial distribution functions are nasty in detail, as they require a volume normalization. Ask yourself by which volume you typically normalize. Setting $vol$ below to 1.0 does not normalize counts at all. Alternatively, we can use the results from the tessellation and normalize by the accumulated Voronoi volume, evaluated at a given erosion distance $d_{ero}$. This is the strategy we use here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "SimID = 0\n",
    "for dataset in ds_srf:\n",
    "    SimID += 1\n",
    "    \n",
    "SimID = 3\n",
    "#we need a Python string of the simulation ID\n",
    "SimulationID = str(SimID)\n",
    "print('Autoreporting for SimID.' + SimulationID)\n",
    "\n",
    "recon_fnm = dataset[0]\n",
    "hull_fnm = dataset[1]\n",
    "#assume here tessellations results coming from matching SimulationID\n",
    "tess_fnm = 'PARAPROBE.Tessellator.Results.SimID.' + str(SimulationID) + '.Stats.h5'\n",
    "\n",
    "res = {}\n",
    "prof = {}\n",
    "#create a Latex report\n",
    "reportfilename = recon_fnm + '.tex'\n",
    "caption = 'Results SimID.' + SimulationID\n",
    "author = r'Markus K\\\"uhbach'\n",
    "tex = autoreporter_latex( reportfilename, caption, author )\n",
    "\n",
    "#standardized format for result files from paraprobe-spatstat\n",
    "spst_fnm = 'PARAPROBE.Spatstat.Results.SimID.' + str(SimulationID) + '.h5'\n",
    "res['1_Spatstat'] = autoreporter_spatstat( recon_fnm, spst_fnm )\n",
    "prof['1_Spatstat'] = autoreporter_profiler( 'Spatstat', SimulationID ).report()\n",
    "#computing radial distribution functions need a normalization by atom number density specific for each dataset\n",
    "#so we need the volume of the dataset, remember, paraprobe-spatstat computed spatial statistics only for atoms at least Rthr nm embedded in the dataset\n",
    "#you can check the metadata like so\n",
    "#res['1_Spatstat'].config\n",
    "#metadata have value, unit, and description, currently not all have descriptions implemented\n",
    "#parse out the specific dsrf, here 'ROIRadiiRDFMax'\n",
    "Rthr = np.float32(res['1_Spatstat'].config['ROIRadiiKNNMax'][0])\n",
    "#where to get the dataset volume from, many use the accumulated volume of its tessellation, alternatively\n",
    "#and strictly speaking more accurate is to take the accumulated Voronoi cell volume for all atoms at least Rthr distant from the dataset edge\n",
    "res['2_Tessellator'] = autoreporter_tessellator( recon_fnm, hull_fnm, tess_fnm )\n",
    "vol = res['2_Tessellator'].get_sum_voronoi_volume( Rthr )\n",
    "#if not we need to tell the volume manually\n",
    "#vol = ##########ADD YOUR BEST SHOT HERE########\n",
    "#radial distribution functions\n",
    "\n",
    "#for which figures do we want to create plots, you can add multiple combinations [['Al','Al'], ['Sc', 'Al']] )\n",
    "res['1_Spatstat'].get_rdf( vol, tgnb = [['Al', 'Al']] )\n",
    "#kth nearest neighbor distributions tgnb is an array of combinations, autoreporter scans the\n",
    "#results file automatically to check which statistics exist and which not\n",
    "res['1_Spatstat'].get_knn( tgnb = [['Sc','Sc'], ['Al', 'Al']] ) #similarly multiple combinations possible\n",
    "#example how to plot all available knn results, autoreporter internally detects which results exists and which not\n",
    "#res['1_Spatstat'].get_knn()\n",
    "\n",
    "#spatial distribution maps, this is currently not yet implemented, motivate me to do it\n",
    "#res['1_Spatstat'].get_sdm()\n",
    "tex.add_section_spatstat( res['1_Spatstat'].report(), '' )\n",
    "#if you want you can add a section for the tessellation results here as well\n",
    "tex.add_section_profiling( prof, r'' )\n",
    "tex.write_report()\n",
    "del tex"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's see what we got. I hope, I saved you some lifetime filled with otherwise boring analyzing, let me know."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ls PARAPROBE.Spatstat.Results.SimID.*.png"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color=\"red\">##MK::TO DO As an example we inspect the nearest neighbor distributions from the synthetic datasets for the Sc-Sc atoms. The filenames are self-explaining.</font>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#from IPython.display import Image\n",
    "#Image(filename='PARAPROBE.Synthetic.Results.SimID.3.h5.VisScIons.png', width= 600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see the Sc atoms highlighted in green. The Sc atoms cluster. This meets our expectations because we inserted spherical ${Al}_3{Sc}$ precipitates into the synthetic dataset. Remember that we set $\\eta = 0.6$ for *PARAPROBE.synthetic.Results.SimID.3.h5*, i.e. we removed $40\\%$ of the atoms. Let's inspect exemplarily $k^{th}$ nearest neighbor distributions for Sc-Sc pairings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Image(filename='PARAPROBE.Spatstat.Results.SimID.3.h5.SpatialStatistics.KNN.Results.Task10.KNN.png', width = 600)\n",
    "##Image(filename='PARAPROBE.Spatstat.Results.SimID.3.h5.SpatialStatistics.KNN.Results.Task11.KNN.png', width = 400)\n",
    "#Image(filename='PARAPROBE.Spatstat.Results.SimID.3.h5.SpatialStatistics.KNN.Results.Task18.KNN.png', width = 400)\n",
    "##Image(filename='PARAPROBE.Spatstat.Results.SimID.3.h5.SpatialStatistics.KNN.Results.Task19.KNN.png', width = 400)\n",
    "#Image(filename='PARAPROBE.Spatstat.Results.SimID.3.h5.SpatialStatistics.KNN.Results.Task26.KNN.png', width = 400)\n",
    "##Image(filename='PARAPROBE.Spatstat.Results.SimID.3.h5.SpatialStatistics.KNN.Results.Task27.KNN.png', width = 400)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, we see the histogram and cumulative distribution for Sc-Sc pairs for the original atom type labels, i.e. non randomized labels. The corresponding image Task11.KNN.png is for the randomized labels. We make two key observations: \n",
    "\n",
    "* The histogram is quantized.\n",
    "* The counts are low. \n",
    "\n",
    "The quantization is expected because we assumed no spatial noise during the dataset synthesis. In effect, atoms are at the ideal locations. However, we removed atoms so some atoms have their neighbors at multiples of the lattice distance, resulting in peaks at larger distances with lower counts. These are neighbors along the $<100>$, $<110>$, $<111>$ directions or multiples thereof. \n",
    "\n",
    "The total counts are low, we because used ROIs with $R = 5nm$ radius.<br>Keep in mind that paraprobe-spatstat places ROIs only at ions at least $R$ from the edge of the dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Recalling the atom-to-edge distances for this dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Image(filename='PARAPROBE.Surfacer.Results.SimID.3.h5.CDF.Ion2EdgeDist.png', width=600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "reveals that most atoms are closer to the dataset than $5nm$. Hence, placing ROIs at these locations<br>would increase counts but introduce bias. In effect, we have now a tool for quantifying the statistical significance.\n",
    "\n",
    "If we choose smaller ROIs this would be sufficient for this application and increase substantially the counts <br>in the histogram. Still, though we can assure that the ROIs are completely embedded in the dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What we learned"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Combinations of atom types and different spatial and two-point statistics can be computed.\n",
    "* The procedures give rigorous control and removal of edge effects, using reproducible protocols.\n",
    "* The results can be post-processed conveniently without any manual shoveling of data around."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color=\"orange\">What is missing is a mechanism to restrict the analysis window to specific regions of the point cloud instead of working, like now, with the entire dataset. Support for this functionality will be added with the next update on the tool. This will enable the definition of intersections between arbitrarily-shaped polyhedral domains and the dataset.</font>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## This completes the tutorial. The next tutorial is on DBScan clustering."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Tutorial written by Markus Kühbach, last updated 13.11.2020*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Professional advice"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Don't let your intuition fool you --- even with parallelization, playing kd-trees, and many other implementation tricks, computing spatial statistics can be a costly numerical exercise. Think about it again, you visit maybe each atom check its neighborhood and do bookkeeping. In addition, and different to many other tools, we do this here for as many combinations as we are interested in. <font color=\"blue\">I would be happy to hear your experiences to improve.</font>\n",
    "\n",
    "Of course, one could add a simple sub-sampling filtering step, which would place ROIs only at each $n^{th}$ location where currently a ROI is placed. This would be one of the typical downsampling approaches which many of you apply at least in the extrapolatory stage of the analysis.\n",
    "\n",
    "To me, though, this reads always counter-initutive, as we aim to get the most substantiated statistics possible and now there is obviously a tool without a need for such downsampling procedures, even if billion atom datasets have to be mastered. I am curious about your opinion m.kuehbach at mpie.de"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
