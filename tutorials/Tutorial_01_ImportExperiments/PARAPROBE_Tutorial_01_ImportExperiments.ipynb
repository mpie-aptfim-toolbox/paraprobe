{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial 1: Import one or multiple datasets from experiment"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Motivation and approach"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We start with probably the most important application of paraprobe. How to apply the tool on datasets from real APT experiments? Paraprobe currently focuses on the delivery of high-throughput methods instead of offering the latest capabilities to all sorts of exploratory analyses."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Therefore, paraprobe works currently with already reconstructed datasets. We use externally created range files. This tutorial shows how we can take a collection of reconstructed APT datasets with respective ranging information and import these data into paraprobe. It does not matter with which tool of the APT community you have created your reconstructed dataset and ranged your mass-to-charge values because paraprobe has a converter tool.\n",
    "To substantiate that paraprobe serves the community in making APT tools more interoperable between different groups, there is a specific tutorial for how to translate e.g. HDF5 file specification from Peter Felfer's group at FAU Erlangen-Nürnberg and make them talk with paraprobe. <font color=\"blue\">Please contact me, if you are interested in getting access to further readers for APT data format specifications of the community.</font>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Such capabilities enable information exchange between proprietary APT software and/or APT community software tools. Open file formats are the key to handling such data exchange. In effect, paraprobe expects the following pieces of information for a dataset:\n",
    "1. Reconstructed x,y,z positions and corresponding calibrated mass-to-charge-state ratio values (m/q) per atom\n",
    "2. Ranging information, i.e. a collection of mapping conventions how to translate mass-to-charge-state ratios to atom types.\n",
    "\n",
    "\n",
    "<font color=\"orange\">Optionally, further pieces of information per atom, such as data on multiplicities, can be injected but are not mandatory here and an aspect to consider in the future. For applications were multiplicities are important users should build a reconstruction and work with this.</font> <font color=\"blue\">Please contact me in cases where this is not a viable option to help me get you on track such that also for your datasets you can profit from paraprobe.</font>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What will we learn?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The key workflow you will learn in this tutorial is to transcode different file formats from APT into a standard format.<br>This standard HDF5-based format is what paraprobe works with. There are two steps:\n",
    "1. **Transcode**, i.e. extract above x,y,z, m/q pieces of information, using *paraprobe-transcoder*.\n",
    "2. **Map**, i.e. range/assign each atom an ion/atom type label based on the pre-existent externally generated ranging information, using *paraprobe-ranger*.\n",
    "\n",
    "In addition, you will learn key ideas that help you with organizing your analyses using simulation IDs and give ideas how to use Python wizards for more complete and convenient documenting of your results and workflows. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## POS, EPOS, APT, RNG, and RRNG as starting points"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Historically motivated, above pieces of information are typically stored in different file formats. Many of them were defined by Cameca, now AMETEK. We discuss here only open file formats. Of course, especial for AMETEK instruments, proprietary formats like RHIT and HITS exist. However, they contain pieces of information to which there is at the moment no unrestricted legal access. Getting access to such pieces of information is an ongoing discussion in the APT community. We should not let us stop here because now with APSuite, AMETEK fortunately elivers the APT file format. The specification of this format is open. In effect, *paraprobe* parses open file formats for reconstructed x,y,z, m/q, and respective ranging data:\n",
    "* **POS**, x,y,z,m/q binary arrays, the historical working horse\n",
    "* **EPOS**, x,y,z,m/q, with additional voltage and multiplicity information\n",
    "* **APT**, not to be mistaken for ATO, APT is the new open exchange format introduced with AMETEK APSuite6/IVAS4\n",
    "* **RNG** and **RRNG**, simple plain ASCII tables how mass-to-charge maps to atom types\n",
    "\n",
    "In the future, it is planned to support also the HDF5-based, open file exchange format of the <a href=\"http://www.fieldemission.org/article.php?id=technical_committee\">IFES Atom Probe Technical Committee (IFES, APT TC)</a>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Workflow and wizards to set up paraprobe jobs, *paraprobe-parmsetup* "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each paraprobe tool is specialized on a task. The idea of paraprobe is that we can loop in as many and as versatile data analyses as possible. This motivates to think rather in terms how the dataset, i.e. the data flow through the tools - the only configuration we as users of paraprobe tools need to make.\n",
    "\n",
    "This gives us flexibility to connect to other community and handshake with commercial tools. Therefore, each paraprobe tool has an own set of settings. To simplify access and control over these settings, the creation of configuration files is used. To make it more convenient for the end users, I wrote a Python wizard tool *paraprobe-parmsetup*. The only purpose of this tool is to make the creating of configuration files more convenient. Upon executing the tool parses from its XML configuration file to identify which analysis settings are desired. This approach is beneficial as it gives the users full control and record of what has been computed and have get assistance with this boring step of documenting your workflow."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because *paraprobe-parmsetup* is pure Python, we can use the Jupyter notebook and create the configuration files directly in Jupyter notebooks."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Take-home message / principle workflow"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Every analysis using paraprobe has three steps:\n",
    "\n",
    "1. Make a decision what you want to do. This gets documented in the configuration file using Python, *paraprobe-parmsetup*.\n",
    "2. Run the tool with the dataset and configuration file on the workstation, computer cluster, or super computer, this will be parallelized with optimized code using respective paraprobe tools.\n",
    "3. Post-process the results. This will be performed in Python made more convenient and documented again with a wizard tool, *paraprobe-autoreporter*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Questions?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can take these Jupyter notebooks as templates and just modify them to run your own analyses.\n",
    "The more experienced you will become the more you will start taking out code section from these notebooks and fuse them together into new analysis workflows. I am happy to hear your feedback and give advice (m.kuehbach at mpie.de)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Concept of simulation ID"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The concept of the simulation ID in paraprobe is an easy but essential one. We want that our results are perfectly bookkept. Therefore, it is useful to give each analysis run a unique ID, consider it as the name, for each analysis, the simulation ID. <font color=\"red\">Be careful: when there are multiple files with the same simulation ID, paraprobe will overwrite data without warning! Therefore, always make your life simpler and organize your workflows using unique names, here simulation IDs.</font>\n",
    "In the future one can think about replacing the simulation IDs with hash values, thereby adding another piece to the puzzle how to make APT data analyzing better aligned with the FAIR principles."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## **paraprobe-transcoder** is the tool for importing data from experiments"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 1: Define your datasets using *paraprobe-parmsetup* Python tool"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the *tutorials* sub-directory there are two small POS files and a RRNG text file with ranging information. These datasets, kindly provided by Priyanshu Bajaj, serve us as examples to get some datasets from real experiments into paraprobe."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "print(os.getcwd())\n",
    "#define location of the experiments\n",
    "#cd H:/Paper/Paper18_APT3DOIM/bb_simulation/ParmsetupAutoreporter"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define the location of the paraprobe/code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "basePath = '/home/markus/ParaprobeVideoTutorials/paraprobe'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we need to load a few Python tools. <font color=\"orange\">If this fails you likely need to install a few Python packages. Normally, this is the **periodictable** Python package. H5py should be in the standard anaconda Python distribution.</font>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#load relevant Python3 (standard) packages\n",
    "import os\n",
    "import sys\n",
    "import glob\n",
    "from pathlib import Path\n",
    "import numpy as np\n",
    "#check for existence of specific non-standard packages we also need\n",
    "try:\n",
    "    import periodictable\n",
    "except ImportError as e:\n",
    "    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')\n",
    "try:\n",
    "    import h5py\n",
    "except ImportError as e:\n",
    "    raise ValueError('Install h5py Python package via e.g. pip install h5py !')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Second, we need to specify where we have the source code for the *paraprobe-parmsetup*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#import the paraprobe-parmsetup tools we need\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/')\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/src/')\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/src/utils/')\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/src/metadata/')\n",
    "#from PARAPROBE_SlurmSingle import *\n",
    "#from PARAPROBE_BashBatch import *\n",
    "from PARAPROBE_BashSingle import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Third, we load *paraprobe-parmsetup*. Specifically, those parts of *paraprobe-parmsetup* which are relevant for the tool in use, here *paraprobe-transcoder*. The names of the Python classes match the tools to make it more intuitive."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from PARAPROBE_Ranging2HDF5 import *\n",
    "from PARAPROBE_Transcoder import *\n",
    "from PARAPROBE_Ranger import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Define which compiler you want to use. We use only one MPI process because transcoding is not costly enough to warrant using parallel execution for now."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Let's tell the wizard what we need"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can equally place the script snippets that we walk through now, also in a Python script and execute it instead of using Jupyter notebook. Pure Python scripting with an IDE like spyder or Jupyter notebooks are just two strategies for creating the XML configuration files for PARAPROBE. Of course you can also generate them completely manually with a text editor. Whatever, is more convenient for you."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's walk through the necessary snippets of a *paraprobe-parmsetup* script, here using this Jupyter notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Specify your computer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can accept these sensible defaults."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MyComputer = { \n",
    "    'Compiler': 'GNU', \n",
    "    'NumberOfComputingNodes': 1,\n",
    "    'NumberOfSocketsPerNode': 1,\n",
    "    'NumberOfProcessesPerSocket': 1 }\n",
    "#'Compiler' alternatively 'ITL' or 'GNU'\n",
    "ProcessesToUse = MyComputer['NumberOfComputingNodes'] * MyComputer['NumberOfSocketsPerNode'] * MyComputer['NumberOfProcessesPerSocket']\n",
    "print('Planning for parallel execution with ' + str(ProcessesToUse) + ' MPI processes')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Specify your datasets"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The key benefit of scripting is that it does not matter whether you want to process one or thousands of datasets. To convince you, let's import a bunch of datasets, in our case the examples (ds) and corresponding ranging (rg) data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds_rg = np.array([['R76_27656-v01.pos','R76_29974-v03.rrng'],\n",
    "                  ['R76_28050-v01.pos','R76_29974-v03.rrng']])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Above, we have defined manually the two example datasets. We have dataset pairs: a dataset, here in POS format, and a ranging definition, here in RRNG format. Alternatively, we could parse the respective files and locations from a database and inject code snippets here. Hopefully, you see the benefit. You can combine any file with any ranging information and analyze as many datasets as you want in a controlled and reproducible way. The Jupyter notebook also gives you the lab report of the workflow."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Standarized ranging information"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are multiple ways how the APT community reports conventions how to map mass-to-charge-state ratios to atom types. RNG and RRNG are two easy ways of documenting this. Paraprobe uses a standardized format based on HDF5. So *paraprobe-parmsetup* has a utility tool to transcode from RNG and RRNG respectively to HDF5. So let's first transcode our RRNG files to HDF5."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#make a dictionary to hold the mapping between ds_rg filename pairs and simulation ID\n",
    "dsrg2simid = {}\n",
    "#loop over the datasets we want to process\n",
    "SimulationID = 0\n",
    "for dataset in ds_rg:\n",
    "    #give a unique ID, IDs can be [1,(2^32)-1]\n",
    "    SimulationID += 1\n",
    "    dsrg2simid[SimulationID] = dataset\n",
    "    print(dataset)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's do the transcoding of RNG/RRNG to HDF5. For this, *paraprobe-parmsetup* contains a RNG/RRNG to HDF5 reader.<br><font color=\"blue\">@Andrew London, I would be happy to discuss how to handshake this parser with yours.</font>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for dataset in ds_rg:\n",
    "    task = paraprobe_ranging2hdf5( dataset[1] )\n",
    "    task.write_h5_rangefile()\n",
    "    del task"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check your working directory, there should be now be a collection of H5 files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ls *.h5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Like all HDF5 files, the formatting of this file can be inspected using the open-source HDFView. More information to this is available here:\n",
    "https://www.hdfgroup.org/downloads/hdfview/\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Transcode x, y, z, m/q and map the ranging data using PARAPROBE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As it is the idea of PARAPROBE, you define what you want to do, here transcode from POS to PARAPROBE HDF5 and range each file. You get a collection of PARAPROBE input files for this and scripts for step 2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#lets make a dictionary of all the individual analysis/datasets we want to process\n",
    "#AnalysisJobsSlurm = {}\n",
    "AnalysisJobsBash = {}\n",
    "\n",
    "#loop over our datasets\n",
    "SimulationID = 0\n",
    "for dataset in ds_rg:\n",
    "    SimulationID += 1\n",
    "    \n",
    "    #make a dictionary to remember what is part of our workflow here\n",
    "    WhatToDo = {}\n",
    "    \n",
    "    task = paraprobe_transcoder( SimulationID, dataset[0] )\n",
    "    WhatToDo['0_Transcode'] = task.run( ProcessesToUse )\n",
    "    #will give me a reconstruction transcoded HDF5 format\n",
    "    recon_fnm = task.get_recon_filename()\n",
    "    del task\n",
    "    \n",
    "    range_fnm = dataset[1] + '.h5'\n",
    "    task = paraprobe_ranger( SimulationID, recon_fnm, range_fnm )\n",
    "    WhatToDo['1_Ranger'] = task.run( ProcessesToUse )\n",
    "    \n",
    "    #now fuse all these steps into a workflow\n",
    "    #for executing on a workstation or a super computer you may need a slurm script\n",
    "    myjobname = str('PB') + str(SimulationID)\n",
    "    #slm = paraprobe_slurm_single( myjobname[0:9], SimulationID, 'TALOS', ProcessesToUse, WhatToDo, Compiler )\n",
    "    #slm.write_slurm_script()\n",
    "    #for interactive use you either call the tools in sequence, here transcoder and ranger thereafter\n",
    "    #but much better is to use a bash script, paraprobe-parmsetup, here will create you a script\n",
    "    bsh = paraprobe_bash_single( myjobname[0:9], SimulationID, '', ProcessesToUse, WhatToDo, MyComputer['Compiler'] )\n",
    "    bsh.write_bash_script()\n",
    "    \n",
    "    #AnalysisJobsSlurm[SimulationID] = slm.get_slurm_script_filename()\n",
    "    AnalysisJobsBash[SimulationID] = bsh.get_bash_script_filename()\n",
    "\n",
    "#this last line here makes the submission to the cluster even simpler\n",
    "#it fuses all above slurm shell scripts into one to submit to cluster at once\n",
    "#sh = paraprobe_bash_batch( 'PriyBajajAMCaseStudy', AnalysisJobsSlurm, AnalysisJobsBash )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Voila, *paraprobe-parmsetup* generated us all necessary configuration files to perform our analysis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ls *.xml"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Also we have now bash scripts to simplify our life."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ls *.SimID.*.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 2: Execute *paraprobe-transcoder* and *paraprobe-ranger*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can now move the datasets, i.e. POS and range files to the computer where you have the paraprobe executables. Then you can either execute *paraprobe-transcoder* and *paraprobe-ranger* interactively or much easier, just execute above bash scripts. Each bash script does one analysis on one dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 3: Post-process results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For now there is nothing more to do. As mentioned, you can inspect the content of the HDF files using HDFView. Supplementary files created during the analyses, are text files. These give details how long the program executed and most importantly details in cases where problems occurred. Please use these *STDOUT* and *STDERR* files for any correspondence with me to make it easier to understand where eventual problems come from and improve the tools."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What have we learned?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* What are the main ideas behind the *parmsetup*, *transcoder*, and *ranger* tools.\n",
    "* How to get datasets from experiment in common formats into paraprobe.\n",
    "* How to apply existent ranging information and tell paraprobe to use it.\n",
    "* How to run analyses for multiple datasets using scripts.\n",
    "\n",
    "In effect, you have transformed your collection of datasets into a collection of as many HDF5 files, with x, y, z, mq, and atom type. The HDF5 files contain metadata which specify how you have mapped mass-to-charge-state ratios to atom types and which atom types you have distinguished. Unless ranged otherwise, an atom has always a default type (ityp = 0). You can now take your datasets and do further processing."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### This completes the tutorial. Next, we learn how to create synthetic datasets."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we will get to know another utility tool *paraprobe-autoreporter*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Tutorial written by Markus Kühbach, last updated 12.11.2020*"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
