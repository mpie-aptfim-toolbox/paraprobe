{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial 6: Analyze clustering with the DBScan clustering algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Motivation and approach"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this tutorial we will cover another key application for what APT is frequently used for: The quantification of the shape, number density, and composition of second-phase precipitates and clusters. Although facing inaccuracies due to trajectory aberrations, preferential evaporation of certain atom types, or simplifying assumptions within current reconstruction algorithms,  we can extra useful information from clustering analyses. \n",
    "\n",
    "Multiple clustering methods have been developed and applied to APT data. One of the most frequently used algorithms is the DBScan clustering algorithm. Typically, for APT it is implemented in a modified form, in the majority of case as the so-called maximum separation method. To my surprise, I found that despite clustering has been a frequent topic for APT data, the role of scientific computing or high-throughput studying in such studies has been underrated or not touched frequently. Given that many clustering algorithms have parameters, though, it is essential to quantify their influence and be able to do so productively, whether we work with one dataset or dataset collections. So I got interested in this topic of uncertainty quantification and eventually it ended up in a multithreaded tool: *paraprobe-dbscan*. Like with *paraprobe-spatstat* you can run high-throughput studies over the space of DBScan parameters for multiple combinations of atom types and multiple datasets. \n",
    "\n",
    "In what follows, I will use the terms cluster and precipitate synonymously."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What will we learn?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* How to define high-throughput parameter studies for the DBScan method\n",
    "* How to summarize the results of such studies."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## *paraprobe-dbscan* is a tool for high-throughput DBScanning on APT datasets"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Paraprobe implements a scientific computing approach with multithreading to speed up each clustering analysis. In addition, paraprobe implements process parallelism to solve multiple parameter realizations in parallel."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The workflow with *paraprobe-dbscan* is straightforward:\n",
    "1. **Load dataset** and corresponding **dataset edge** quantification to detect any precipitates which eventually touch the boundary,i.e. precipitates which have been truncated by the edge of the datasets.\n",
    "2. **Define** which **combinations of parameters** and **combinations of atom types** to investigate.\n",
    "\n",
    "The tool yields a collection of distributions of precipitate sizes. Specifically, for each parameter and atom type set we get two distributions, one distribution that accounts for all precipitates and one which accounts only for the precipitates that do not touch the edge of the dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Atoms on the edge of the dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As with *paraprobe-spatstat*, we use a threshold distance $d_{thr}$ to identify which clusters touch the edge of the dataset because they have at least one atom of the cluster laying within $d_{thr}$ to the edge and which clusters have not."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 1: Detail what you want to do"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We use the three example datasets. Performing business as usual, we first use *paraprobe-parmsetup* to create configuration files. Second, we execute *paraprobe-dbscan* jobs using these configuration files. Third, we inspect the results either manually or use *paraprobe-autoreporter* to help us with much of the bookkeeping and data wrangling."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define the location of the paraprobe/code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "basePath = '/home/markus/ParaprobeVideoTutorials/paraprobe'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load Python and *paraprobe-parmsetup*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#load relevant Python3 (standard) packages\n",
    "import os\n",
    "import sys\n",
    "import glob\n",
    "from pathlib import Path\n",
    "import numpy as np\n",
    "#check for existence of specific non-standard packages we also need\n",
    "try:\n",
    "    import periodictable\n",
    "except ImportError as e:\n",
    "    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')\n",
    "try:\n",
    "    import h5py\n",
    "except ImportError as e:\n",
    "    raise ValueError('Install h5py Python package via e.g. pip install h5py !')\n",
    "\n",
    "#import the paraprobe-parmsetup tools we need\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/')\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/src/')\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/src/utils/')\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/src/metadata/')\n",
    "#from PARAPROBE_SlurmSingle import *\n",
    "#from PARAPROBE_BashBatch import *\n",
    "from PARAPROBE_BashSingle import *\n",
    "from PARAPROBE_XML import *\n",
    "from PARAPROBE_Surfacer import *\n",
    "from PARAPROBE_DBScan import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Define which compiler and how many processes you want to use. Parameter configurations get distributed across processes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MyComputer = { \n",
    "    'Compiler': 'GNU', \n",
    "    'NumberOfComputingNodes': 1,\n",
    "    'NumberOfSocketsPerNode': 1,\n",
    "    'NumberOfProcessesPerSocket': 1 }\n",
    "#'Compiler' alternatively 'ITL' or 'GNU'\n",
    "ProcessesToUse = MyComputer['NumberOfComputingNodes'] * MyComputer['NumberOfSocketsPerNode'] * MyComputer['NumberOfProcessesPerSocket']\n",
    "print('Planning for parallel execution with ' + str(ProcessesToUse) + ' MPI processes')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Setting up XML configuration files for *paraprobe-spatstat* is similar to the steps learned in previous tools."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds_srf = np.array( [['PARAPROBE.Transcoder.Results.SimID.1.h5','PARAPROBE.Surfacer.Results.SimID.1.h5'],\n",
    "                    ['PARAPROBE.Transcoder.Results.SimID.2.h5','PARAPROBE.Surfacer.Results.SimID.2.h5'],\n",
    "                    ['PARAPROBE.Synthetic.Results.SimID.3.h5','PARAPROBE.Surfacer.Results.SimID.3.h5']])\n",
    "\n",
    "SimulationID = 0\n",
    "#AnalysisJobsSlurm = {}\n",
    "AnalysisJobsBash = {}\n",
    "for dataset in ds_srf:\n",
    "    SimulationID += 1\n",
    "    print('Creating configuration for dbscan SimID.' + str(SimulationID))\n",
    "    WhatToDo = {}\n",
    "    \n",
    "    recon_fnm = dataset[0]\n",
    "    hull_fnm = dataset[1]\n",
    "    \n",
    "    #create paraprobe-parmsetup class specialization for dbscan tool\n",
    "    task = paraprobe_dbscan( SimulationID, recon_fnm, hull_fnm )\n",
    "    #define clustering parameter to probe\n",
    "    #dmax equivalent to epsilon\n",
    "    eps_range = [0.2, 0.02, 5.00] #in nm min, incr, max\n",
    "    #minpts for general DBScan, in the special realization maximum separation method \n",
    "    #minpts=1 then minpts_range defines nmin_range, i.e. the number for which cluster is deemed significant\n",
    "    pts_range = [5, 10, 5] #in cnts, min, incr, max\n",
    "    #distance threshold for which clusters with atoms below this threshold are considered truncated clusters\n",
    "    dthr = 1.0 #in nm    \n",
    "    task.set_high_throughput_maximum_separation_method( eps_range, pts_range, dthr )\n",
    "    #what might possibly cluster, lets see..., we have Al and Sc\n",
    "    task.add_targets( 'Sc' )\n",
    "    #task.add_targets( 'Al' )\n",
    "    WhatToDo['1_DBScan'] = task.run( ProcessesToUse )\n",
    "    del task\n",
    "    \n",
    "    myjobname = str('MK') + str(SimulationID)\n",
    "    #slm = paraprobe_slurm_single( myjobname[0:9], SimulationID, 'TALOS', ProcessesToUse, WhatToDo, Compiler )\n",
    "    #slm.write_slurm_script()\n",
    "    #but also write a bash script (to be submitted to a local workstation without super computer)\n",
    "    bsh = paraprobe_bash_single( myjobname[0:9], SimulationID, '', ProcessesToUse, WhatToDo, MyComputer['Compiler'] )\n",
    "    bsh.write_bash_script()\n",
    "    del WhatToDo\n",
    "    \n",
    "    #AnalysisJobsSlurm[N] = slm.get_slurm_script_filename()\n",
    "    AnalysisJobsBash[SimulationID] = bsh.get_bash_script_filename()\n",
    "\n",
    "print('All configuration files created')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Done, let's check the XML configration and SH scripts for *paraprobe-dbscan*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ls PARAPROBE.DBScan.SimID.*.xml\n",
    "! ls BASH.PARAPROBE.Workflow.SimID.*.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 2: Execute *paraprobe-dbscan*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nothing new here, place the H5, XML, SH files where you want to execute *paraprobe-dbscan*. Thereafter, execute, either interactively on the console or by submitting to queue using scripts. In effect, *paraprobe-dbscan* will store the results in a HDF5 file."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 3: Inspect the results with *paraprobe-autoreporter*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The tutorial on spatial statistics motivated why we need a powerful tool to help us makes sense of these automated high-throughput jobs: Also for *paraprobe-dbscan* jobs we may end up with hundreds or eventually even thousands of analyses done for us in the background. Of course we could then check again the results manually in the HDF5 results file but this is time consuming. Other use cases are even more tedious. Let's take for instance a research who is interested in getting an idea how the parameterization of the DBScan/maximum separation method affects the predicted number density of clusters. For answering such question we face two difficulties: The first difficulty is that we need to pull results from multiple runs with different parameterization together. The second difficulty is that we need to define what is the volume of the dataset because otherwise computing number densities is impossible. Here, I will show how to use *paraprobe-autoreporter* to handle such a use case and create the figures for it,if you want the distribution of sizes and the number density, and get all this compiled into a PDF report with much computer assistance."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load paraprobe-autoreporter"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#import the paraprobe-autoreporter tools we need\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/latex/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/metadata/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/plotting/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/utils/')\n",
    "from PARAPROBE_Autoreporter_Numerics import *\n",
    "from PARAPROBE_Autoreporter_CorpDesign import *\n",
    "from PARAPROBE_Autoreporter_Profiler import *\n",
    "from PARAPROBE_Autoreporter_LatexGenerator import *\n",
    "\n",
    "from PARAPROBE_Autoreporter_Surfacer import *\n",
    "from PARAPROBE_Autoreporter_Tessellator import *\n",
    "from PARAPROBE_Autoreporter_DBScan import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nothing really new here. We set up a report with a corresponding autoreporter_dbscan class. Number densities again require us to define the volume of the dataset. Ask yourself by which volume you typically normalize. Setting $vol$ below to 1.0 does not normalize at all, so gives raw counts. Alternatively, we can use the results from the tessellation and normalize by the accumulated Voronoi volume to a certain threshold distance. Both strategies are exemplified below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "SimID = 0\n",
    "for dataset in ds_srf:\n",
    "    SimID += 1\n",
    "    \n",
    "SimID = 3\n",
    "#we need a Python string of the simulation ID\n",
    "SimulationID = str(SimID)\n",
    "print('Autoreporting for SimID.' + SimulationID)\n",
    "    \n",
    "recon_fnm = dataset[0]\n",
    "hull_fnm = dataset[1]\n",
    "#assume here tessellations results coming from matching SimulationID\n",
    "tess_fnm = 'PARAPROBE.Tessellator.Results.SimID.' + str(SimulationID) + '.Stats.h5'\n",
    "\n",
    "res = {}\n",
    "prof = {}\n",
    "#create a Latex report\n",
    "reportfilename = recon_fnm + '.tex'\n",
    "caption = 'Results SimID.' + SimulationID\n",
    "author = r'Markus K\\\"uhbach'\n",
    "tex = autoreporter_latex( reportfilename, caption, author )\n",
    "    \n",
    "#standardized format for result files from paraprobe-dbscan\n",
    "dbsc_fnm = 'PARAPROBE.DBScan.Results.SimID.' + str(SimulationID) + '.h5'\n",
    "res['1_DBScan'] = autoreporter_dbscan( recon_fnm, dbsc_fnm )\n",
    "prof['1_DBScan'] = autoreporter_profiler( 'DBScan', SimulationID ).report()\n",
    "    \n",
    "#EXAMPLE HOW TO GET DISTRIBUTION FOR A SINGLE CLUSTER SIZE\n",
    "#example for a simple distribution of cluster sizes\n",
    "#eps = dmax\n",
    "eps = 0.5\n",
    "#kth here Sc neighbors in eps\n",
    "kth = 1\n",
    "#here maximum separation method Nmin, number of ions to consider cluster a significant one\n",
    "minpts = 5\n",
    "res['1_DBScan'].get_cluster_size_distros( eps, kth, minpts)\n",
    "    \n",
    "#like it was the case for paraprobe-spatstat we might have quantities, like number densities,\n",
    "#for which we need the dataset volume, maybe we have forgotten which threshold we setted\n",
    "#let's ask the metadata dbsc_fnm stores the metadata, query the distance to identify precipitates to edge\n",
    "#you can check the metadata like so\n",
    "#metadata have value, unit, and description\n",
    "#parse out e.g. the threshold distance\n",
    "dthr = np.float32(res['1_DBScan'].config['DatasetEdgeThresholdDistance'][0])\n",
    "#where to get the dataset volume from? Many use the accumulated volume of its tessellation, alternatively,\n",
    "#and strictly speaking more accurate, is to take the accumulated Voronoi cell volume for all atoms \n",
    "#which are at least Rthr distant from the dataset edge\n",
    "res['2_Tessellator'] = autoreporter_tessellator( recon_fnm, hull_fnm, tess_fnm )\n",
    "vol = res['2_Tessellator'].get_sum_voronoi_volume( dthr )\n",
    "#let's normalize however not here for educational purposes because then we get the number of counts\n",
    "vol = 1e27 #to go from m^3 to nm^3\n",
    "    \n",
    "#dont forget that in the above example we instructed DBScan runs for a range of kth neighbors and Nmin values\n",
    "kth = 1\n",
    "Nmin = 5\n",
    "#the autoreporter_dbscan is adaptive it will check if the result exists\n",
    "res['1_DBScan'].get_number_density_vs_eps_minpts( kth, Nmin, vol )\n",
    "\n",
    "#add all the created figures for DBScan in the report\n",
    "tex.add_section_dbscan( res['1_DBScan'].report(), '' )\n",
    "tex.add_section_profiling( prof, r'' )\n",
    "tex.write_report()\n",
    "del tex"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's see what we obtained."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ls PARAPROBE.DBScan.Results.SimID.*.png"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's inspect for instance the ${Al}_3{Sc}$ precipitates of the synthetic dataset (SimID.3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Image\n",
    "Image(filename='PARAPROBE.Synthetic.Results.SimID.3.VisSc.png', width = 600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This figure was generated with Paraview using the XDMF supplementary files. Scandium ions are colored in green. So our ground truth here is a dataset with 10 precipitates. Given that PARAPROBE distributes the precipitates randomly in a bounding box, some precipitates are truncated. The lattice constant was $0.410nm$, so when set $\\epsilon = 0.5nm$, $k^{th} = 1$, and $N_{min} = 5$ we should be able to detect the Scandium clusters because $eps > 0.410nm$. The corresponding distribution of sizes "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Image(filename='PARAPROBE.DBScan.Results.SimID.3.h5.DBScan.Results.Task15.GSD.png', width = 600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "was fished out automatically from the sea of parameter runs in the PARAPROBE.DBScan.Results.SimID.3.h5 results file. We see two distributions. Given the very small number of particles both curves show evidence for finite size effects and have steps rather than be smooth curves. \n",
    "\n",
    "The orange curve shows the cumulated distribution as a function of number of (Sc) ions in the cluster. Orange shows for all precipitates. Remember that all precipitates were created with the same radius $2nm$. However, given that precipitates are truncated we see different sizes remaining. The blue curve shows the distribution if we ignore all truncated precipitates. As the rendering of the dataset above shows, only a single precipitate remains. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Sc count of this precipitate is the expected for a sphere $N_{Sc} \\approx \\frac{\\frac{4}{3}\\pi2^3}{0.410^3}\\cdot 1$. We should remember that for SimID.3 we set $\\eta = 0.6$. See also here the beauty of Jupyter notebooks, no need to pull a calculator any longer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.uint32(4/3*np.pi*2**3/(0.410**3)*0.6)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "that should be 291, i.e. close to exact number that we expected. However, given the small size of the precipitate it makes a difference how to place the lattice unit cell aggregate relative to the barycenter of the particle because the lattice constant is not orders of magnitude smaller than the precipitate."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, let's inspect how the number (density) of detected precipitates becomes smaller the larger we set $eps$, i.e. the stronger neighboring clusters might get fused."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Image(filename='PARAPROBE.DBScan.Results.SimID.3.h5.DBScan.Results.Sc.NumberDensityVsEps.png', width = 600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each data point in this plot is the result of a single DBScan run. Here, we plot number (not density because above we have for educational purpose divided by 1.0e27) over $\\epsilon$. Paraprobe-autoreporter detected automatically that for $\\epsilon$ below $0.4nm$ no cluster were detected, so no data points are drawn into the figure. For $0.5$ we recover the expected $10$ ${Al}_3{Sc}$ precipitates. For lower $eps$, subtilities are possible. For instance, when we remove atoms randomly, also in precipitates, we may create situations locally where isolated regions form. For larger $eps$ we successively fuse Sc atoms again into larger cluster, until at $eps > 2.0nm$ all precipitates are fused into a single cluster (percolation)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What we learned"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* We can perform high-throughput parameter studies for clustering methods, here exemplified for a DBScan-based algorithm.\n",
    "* The procedures give rigorous control and removal of edge effects, using reproducible protocols.\n",
    "* The results can be post-processed conveniently without any manual shoveling of data around."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The DBScan clustering algorithm is currently the only one implemented in *paraprobe-dbscan*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## This completes the DBScan tutorial."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Tutorial written by Markus Kühbach, last updated 09.12.2020*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## In effect, the tutorials should have given you an insight of what paraprobe is, how it can be used and serve you. I am already curious to show you the tutorials for atom probe crystallography but this will be the story for our next paper. Like always, feel free to ask for feedback. Happy atom probing and analyzing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
