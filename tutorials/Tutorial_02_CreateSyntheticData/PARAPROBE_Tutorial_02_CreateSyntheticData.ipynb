{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial 2: Create one or multiple synthetic datasets with ranging"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Motivation and approach"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this tutorial we show how you can create one or multiple synthetic APT datasets with respective ranging information using PARAPROBE. Such capabilities enable you to create specific reference datasets that can be used for testing new methods rigorously, to inspect the spatial arrangement of atoms for different crystal structures, and to set up high-throughput studies for such cases. Like it was the case in tutorial 1, we need two pieces of information:\n",
    "1. **Synthetic dataset**, i.e. a virtually created reconstructed APT dataset with x, y, z positions and corresponding mass-to-charge-state ratio values (m/q) per atom\n",
    "2. **Ranging** information, i.e. a collection of mapping conventions how to translate mass-to-charge-state ratios to atom types.\n",
    "\n",
    "In addition, we want to keep track of the microstructure in these synthetic datasets. PARAPROBE documents this via tool-specific metadata. These metadata are stored in HDF5 files. After creation of the dataset we are in the same situation as we are after having transcoded or reconstructed datasets from experiments. This is because "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What will we learn?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The key workflow you will learn in this tutorial is to create synthetic datasets:\n",
    "1. **Synthesize**, i.e. create synthetic datasets using *paraprobe-synthetic*.\n",
    "2. **Map**, i.e. range with pre-existent or synthetically created ranging information, using *paraprobe-ranger*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Synthetic datasets should represent a microstructure volume"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The atomic architecture of real materials is complex. Real materials contain defects like dislocations, stacking faults, interfaces, junctions between these defects, and gradients of the chemistry. These individual defects build a three-dimensional hierarchy with different length scales. APT specimens probe a nano- to micron-scale volume of such microstructures. There is to the best of our knowledge so far, however, no algorithm that is capable of reproducing a microstructure where all spatial n-point correlations between above defects can be rigorously controlled. A detailed discussion is made here <a href=\"https://publications.rwth-aachen.de/record/712180\">(M. Kühbach, 2017)</a>. Therefore, we need to make simplifications whenever creating synthetic datasets. Currently, PARAPROBE can create three different types of synthetic microstructure realizations:\n",
    "* Single-crystalline single-phase datasets\n",
    "* Polycrystalline single-phase datasets\n",
    "* Above datasets with an embedding of second-phase precipitates.\n",
    "\n",
    "Here, we show how to synthesize a collection of synthetic datasets. Specifically, we create single crystals with a volume fraction of second-phase precipitates."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Modeling inaccuracies (detector and spatial noise)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compared to the atomic architecture of the real specimen a reconstructed APT dataset contains inaccuracies. Atoms can be missing (limited detection efficiency) or be displaced relative to their position in the specimen (spatial inaccuracies). To the best of our knowledge, there is no generally working quantitative physical model for the noise within APT datasets. Therefore, we make two simplifying assumptions:\n",
    "* Missing atoms are modelled as randomly missing atoms.\n",
    "* Spatial inaccuracies are modelled by assuming an anisotropic Gaussian smearing with $\\sigma_x \\approx \\sigma_y \\leq \\sigma_z$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## *paraprobe-synthetic* is the tool for creating synthetic datasets"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 1: Detail what you want to do"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Like in the previous tutorial, we load Python and tool-specific modules of *paraprobe-parmsetup*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define the location of the paraprobe/code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "basePath = '/home/markus/ParaprobeVideoTutorials/paraprobe'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load Python and *paraprobe-parmsetup*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "#load relevant Python3 (standard) packages\n",
    "import os\n",
    "import sys\n",
    "import glob\n",
    "from pathlib import Path\n",
    "import numpy as np\n",
    "#check for existence of specific non-standard packages we also need\n",
    "try:\n",
    "    import periodictable\n",
    "except ImportError as e:\n",
    "    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')\n",
    "try:\n",
    "    import h5py\n",
    "except ImportError as e:\n",
    "    raise ValueError('Install h5py Python package via e.g. pip install h5py !')\n",
    "\n",
    "#import the paraprobe-parmsetup tools we need\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/')\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/src/')\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/src/utils/')\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/src/metadata/')\n",
    "#from PARAPROBE_SlurmSingle import *\n",
    "#from PARAPROBE_BashBatch import *\n",
    "from PARAPROBE_BashSingle import *\n",
    "from PARAPROBE_XML import *\n",
    "from PARAPROBE_Synthetic import *\n",
    "from PARAPROBE_Ranging2HDF5 import *\n",
    "from PARAPROBE_Ranger import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define your computer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Planning for parallel execution with 1 MPI processes\n"
     ]
    }
   ],
   "source": [
    "MyComputer = { \n",
    "    'Compiler': 'GNU', \n",
    "    'NumberOfComputingNodes': 1,\n",
    "    'NumberOfSocketsPerNode': 1,\n",
    "    'NumberOfProcessesPerSocket': 1 }\n",
    "#'Compiler' alternatively 'ITL' or 'GNU'\n",
    "ProcessesToUse = MyComputer['NumberOfComputingNodes'] * MyComputer['NumberOfSocketsPerNode'] * MyComputer['NumberOfProcessesPerSocket']\n",
    "print('Planning for parallel execution with ' + str(ProcessesToUse) + ' MPI processes')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define ranging data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our synthetic datasets should contain a number of different atom types. Many sophisticated models can be imagined with concentration gradients, precipitates, and different dataset shapes. Let's start with very basic datasets, here single crystals, single-phase without solute atoms. In this scenario, we can imagine that the dataset is cut out from a large aggregate of unit cells. So assume for now we have an Al lattice. Then, in an ideal world and using a hypothetically ideal instrument, the mass-to-charge-state ratio diagram would have a single peak. This holds when we assume <font color=\"0000ff\">$q=+1$ and $^{27}$Al isotopes with $m/q = 26.981$ Da.</font>\n",
    "We may be interested in adding a second atom type, e.g. Scandium <font color=\"00ff00\">$q=+1$ and $^{45}$Sc isotopes $m/q = 44.956$ Da</font>, the lovely other partner in ${Al}_3{Sc}$ precipitates."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's create a synthetic RRNG file for this and corresponding HDF5 range file using Jupyter notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['Range1', '26.97', '26.99', 'Vol:0.0000', 'Al:1', 'Color:0000FF']\n",
      "['Range2', '44.95', '45.00', 'Vol:0.0000', 'Sc:1', 'Color:00FF00']\n",
      "PARAPROBE.CreateSyntheticDatasets.rrng.h5\r\n"
     ]
    }
   ],
   "source": [
    "#SimulationID = 1\n",
    "rrng = r'[Ions]' + '\\n'\n",
    "rrng += r'Number=2' + '\\n'\n",
    "rrng += r'Ion1=Al' + '\\n'\n",
    "rrng += r'Ion2=Sc' + '\\n'\n",
    "rrng += r'[Ranges]' + '\\n'\n",
    "rrng += r'Number=2' + '\\n'\n",
    "rrng += r'Range1= 26.97 26.99 Vol:0.0000 Al:1 Color:0000FF' + '\\n'\n",
    "rrng += r'Range2= 44.95 45.00 Vol:0.0000 Sc:1 Color:00FF00' + '\\n'\n",
    "rrngfile = open('PARAPROBE.CreateSyntheticDatasets.rrng', 'w')\n",
    "rrngfile.write(rrng)\n",
    "rrngfile.close()\n",
    "#transcode this to HDF5\n",
    "task = paraprobe_ranging2hdf5( 'PARAPROBE.CreateSyntheticDatasets.rrng' )\n",
    "task.write_h5_rangefile()\n",
    "del task\n",
    "!ls *.h5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see, *paraprobe-parmsetup* created the H5 file for us."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define crystal structure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Instead of an ad hoc approach, paraprobe implements crystal structures using crystallographic space groups. In crystallography, a crystal structure is a combination of a lattice (definition of three base vectors spanning the smallest asymmetric unit to describe a three-dimensional periodic lattice) plus a motif (pattern of atoms). Currently, paraprobe implements several common space groups and also examples of more complicated structures. All these are created as specialization of the general tricline. With these examples, I am convinced that developers find a good starting point to implement whatever they are interested. Let us for now, though, use the space groups that I implemented exemplarily:\n",
    "* 229, body-centered cubic, e.g. ${W}$ \n",
    "* 225, face-centered cubic, e.g. ${Al}$\n",
    "* 194, ${P6}_3{/mmc}$, e.g. ${Mg}$\n",
    "* 221, ${L1}_2$, e.g. ${Cu}_3{Al}$\n",
    "* 141, ${I4}_1{/amd}$, e.g. Zircon\n",
    "* 062, ${Pnma}$, e.g. ${CaTiO}_3$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's give an example how to define a lattice in *paraprobe-synthetic* with space groups."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "matrixmotif = [motif('Al', 0.0, 0.0, 0.0, 1)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The entries specify the element symbol, the three fractional coordinates in the unit cell, and the assumed charge state, as this defines the mass-to-charge-state ratio. Two things are important:\n",
    "* <font color=\"red\">**For single character elements, like H, C, B etc. we need to add a colon, i.e. write not 'H' but write 'H:'!**</font>\n",
    "* <font color=\"red\">We build primitive cells by default.</font>\n",
    "\n",
    "More complex unit cells have multiple lattice sites, so we build these as an array of motifs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "al3scmotif = [motif('Sc', 0.0, 0.0, 0.0, 1), \n",
    "              motif('Al', 0.0, 0.5, 0.5, 1), \n",
    "              motif('Al', 0.5, 0.0, 0.5, 1), \n",
    "              motif('Al', 0.5, 0.5, 0.0, 1)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Evidently, the motif is an array of motif class objects. It is this motif array that we pass to *paraprobe-parmsetup*.<br> Next, we will learn how to use the motif arrays to complete the definition of crystal structures."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define which parameters we want to change and loop over"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Synthetic datasets are created in the shape of a conical frustrum with the possibility to place a hemi-spherical cap at the top and optionally carve another such out at the bottom. For simplicity we create cylindrical datasets. Let's take a radius-to-height ratio of $R/H = 0.5$ with approximately $0.2$ million atoms to begin with. Of course, you can create much larger datasets. Assume that we want to investigate the effect of randomly missing atoms on spatial statistics or the deterioration of crystal structure detection algorithms. With such assumption,  the only difference between the synthetic datasets should be the fraction $\\eta$ of remaining atoms. Using *paraprobe-synthetic*, the commands for creating such a collection of synthetic dataset reads as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Creating configuration for synthetic dataset SimID.1 with eta = 1.0\n",
      "Creating configuration for synthetic dataset SimID.2 with eta = 0.8\n",
      "Creating configuration for synthetic dataset SimID.3 with eta = 0.6\n",
      "All configuration files created\n"
     ]
    }
   ],
   "source": [
    "SimulationID = 0\n",
    "#AnalysisJobsSlurm = {}\n",
    "AnalysisJobsBash = {}\n",
    "\n",
    "#probe here three values for fraction of remaining atoms\n",
    "for eta in [1.0, 0.8, 0.6]:\n",
    "    SimulationID += 1\n",
    "    print('Creating configuration for synthetic dataset SimID.' + str(SimulationID) + ' with eta = ' + str(eta))\n",
    "    \n",
    "    WhatToDo = {}\n",
    "    task = paraprobe_synthetic( SimulationID )\n",
    "    #geometry\n",
    "    #0.2 million atoms\n",
    "    task.set_tip_geom_natoms( 0.2e6 ) \n",
    "    #cylindrical dataset R/H = 0.5\n",
    "    task.set_tip_geom_radius_bottom( 0.5 )\n",
    "    task.set_tip_geom_radius_top( 0.5 )\n",
    "    \n",
    "    #use space group to create arbitrary crystal structure\n",
    "    #by default we get single crystals\n",
    "    #crystal structure of the matrix, Al is fcc, so spacegroup 225\n",
    "    matrixmotif = [motif('Al', 0.0, 0.0, 0.0, 1)]\n",
    "    task.set_tip_matrix_lattice_spacegroup( 225, 0.404, 0.404, 0.404, matrixmotif )\n",
    "    #orientation of the matrix, Bunge-Euler angles\n",
    "    task.set_tip_matrix_ori( 0.0, 0.0, 0.0 )    \n",
    "    \n",
    "    #precipitates\n",
    "    #number of precipitates, here no particles right now\n",
    "    task.set_tip_particles_number( 10 )\n",
    "    #orientation of the precipitates, currently all the same orientation\n",
    "    task.set_tip_particles_ori( 8.0, 8.0, 8.0 )\n",
    "    #radius of the precipitates\n",
    "    task.set_tip_particles_radius( 2.0 )\n",
    "    #spread of size distribution\n",
    "    #task.set_tip_particles_sigma( 0.1 )\n",
    "    #set crystal structure of the precipitate, Al3Sc is L12, so spacegroup 221\n",
    "    precipitatemotif = [motif('Sc', 0.0, 0.0, 0.0, 1),\n",
    "                        motif('Al', 0.0, 0.5, 0.5, 1),\n",
    "                        motif('Al', 0.5, 0.0, 0.5, 1),\n",
    "                        motif('Al', 0.5, 0.5, 0.0, 1)]    \n",
    "    task.set_tip_particles_lattice_spacegroup( 221, 0.410, 0.410, 0.410, precipitatemotif )\n",
    "    \n",
    "    #noise models\n",
    "    #model missing atoms\n",
    "    task.set_tip_noise_deteff( eta ) \n",
    "    #do not model spatial noise, i.e. nail atoms at their ideal positions\n",
    "    task.set_tip_noise_gaussianx( 0.0 ) \n",
    "    task.set_tip_noise_gaussiany( 0.0 )\n",
    "    task.set_tip_noise_gaussianz( 0.0 )\n",
    "    \n",
    "    WhatToDo['1_Synthetic'] = task.run( ProcessesToUse)\n",
    "    del task\n",
    "    #after execution on the cluster, this will give us HDF5 results file with this nam\n",
    "    recon_fnm = 'PARAPROBE.Synthetic.Results.SimID.' + str(SimulationID) + '.h5'\n",
    "    \n",
    "    #paraprobe-synthetic only creates the geometry with the mass-to-charge-state ratios\n",
    "    #of course we want to map the ranges from above as well\n",
    "    range_fnm = 'PARAPROBE.CreateSyntheticDatasets.rrng.h5'    \n",
    "    task = paraprobe_ranger( SimulationID, recon_fnm, range_fnm )\n",
    "    WhatToDo['2_Ranger'] = task.run( ProcessesToUse )\n",
    "    del task\n",
    "    \n",
    "    #now fuse all these steps into a workflow, write a slurm script (to be submitted to workstation or super computer) \n",
    "    myjobname = str('MK') + str(SimulationID)\n",
    "    #slm = paraprobe_slurm_single( myjobname[0:9], SimulationID, 'TALOS', ProcessesToUse, WhatToDo, Compiler )\n",
    "    #slm.write_slurm_script()\n",
    "    #but also write a bash script (to be submitted to a local workstation without super computer)\n",
    "    bsh = paraprobe_bash_single( myjobname[0:9], SimulationID, '', ProcessesToUse, WhatToDo, MyComputer['Compiler'] )\n",
    "    bsh.write_bash_script()\n",
    "    del WhatToDo\n",
    "    \n",
    "    #remember that we want to submit this job\n",
    "    #AnalysisJobsSlurm[N] = slm.get_slurm_script_filename()\n",
    "    AnalysisJobsBash[SimulationID] = bsh.get_bash_script_filename()\n",
    "\n",
    "print('All configuration files created')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**For creating a single dataset writing such scripts might seem way too complicated compared to using a graphical user interface (GUI) but think twice.** Try this with multiple combinations or more combinations. You will quickly see how much more productive you can become. We inspect the resulting configuration XML files and SH shell scripts."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "PARAPROBE.Synthetic.SimID.1.xml  PARAPROBE.Synthetic.SimID.3.xml\n",
      "PARAPROBE.Synthetic.SimID.2.xml\n",
      "BASH.PARAPROBE.Workflow.SimID.1.sh  BASH.PARAPROBE.Workflow.SimID.3.sh\n",
      "BASH.PARAPROBE.Workflow.SimID.2.sh\n"
     ]
    }
   ],
   "source": [
    "! ls PARAPROBE.Synthetic.SimID.*.xml\n",
    "! ls BASH.PARAPROBE.Workflow.SimID.*.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We take the XML configuration files, the RRNG.H5 file, and the SH scripts to the computer where the *paraprobe-synthetic* and *paraprobe-ranger* and execute."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 2: Execute *paraprobe-synthetic* and *paraprobe-ranger*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is nothing to transcode here, the dataset is created as if it were a reconstruction, the formatting in the HDF5 file is the same a for a transcoded experimental dataset. This assures that synthetic and experimental datasets can from now on be processed in the same way."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 3: Inspect the results, here with *paraprobe-autoreporter*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Paraprobe synthetic creates now only the desired datasets but also include a large number of metadata which give details. In addition, supplementary *.xdmf* files are created. These can be used with Paraview or VisIt to inspect e.g. where the atoms or precipitates are located."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "ls: cannot access 'PARAPROBE.Synthetic.Results.SimID.*.h5': No such file or directory\n",
      "ls: cannot access '*.xdmf': No such file or directory\n"
     ]
    }
   ],
   "source": [
    "!ls PARAPROBE.Synthetic.Results.SimID.*.h5\n",
    "!ls *.xdmf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Typically, the results files for above workflow serve as input for more complicated workflows. Experienced users would just load the HDF5 files quickly into HDFView and check key metadata. Already at this stage, though, we would like to introduce a complementary way for inspecting above results. The Python wizard for post-processing results from paraprobe tools - *paraprobe-autoreporter*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The main idea of this tool is automation and make analyses rigorous and convenient. Automation is for instance useful when we create figures which are frequently asked for like mass-to-charge state ratios, compositions, spatial statistics, Saxey plot to name a few. Likely, every APTer has its own idea how such figure should look like. In my opinion this is personal figure make-up, though only because the information content and key message of the figure is almost exactly the same. Therefore, automated creation of figures, and making the automation methods open source, enables to wire these methods into other workflows without loosing much flexibility for individual make-up. That is the main idea behind *paraprobe-autoreporter*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What have we learned?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* What are the main ideas behind the *synthetic* tools.\n",
    "* How to create synthetic single and polycrystal with arbitrary crystal structure."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### This completes the tutorial. Now that you can load your experimental datasets or create synthetic datasets and range them, it is time to do some analyses. We will perform these in the next tutorials."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Tutorial written by Markus Kühbach, last updated 12.11.2020*"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
