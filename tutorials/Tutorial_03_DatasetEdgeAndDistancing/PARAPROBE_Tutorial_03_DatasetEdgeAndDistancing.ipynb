{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial 3: Analyze the edge of the dataset using *paraprobe-surfacer*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Motivation and approach"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Previous tutorials showed how to import reconstructed APT datasets from experiment or create synthetic datasets. I In this and the next tutorials, we will learn how to apply specific data analysis methods. This tutorial covers how to quantify the edge of the dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Why is quantifying the edge of the dataset important?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An APT dataset is a finite point cloud. So we can imagine that analyses close to the edge of the dataset will require particular care as otherwise we make our analysis more biased and more inaccurate. For instance, a region-of-interest (ROI) which protrudes out of the point cloud counts fewer atoms on average which skews counting statistics or add noise when doing Fourier transforms. \n",
    "\n",
    "Therefore, it is useful to define and quantify what could be a suitable hull to the point cloud and compute how close the atoms and ROIs are to this hull. A triangle hull, represented as a triangle mesh, is a good compromise when it comes to handling the dataset edge numerically and capture that the point cloud samples an object with eventually both convex and concave regions. Paraprobe computes a particular type of triangle hull, the so-called $\\alpha$-shapes. These are generalizations of convex hulls. <a href=\"https://arxiv.org/pdf/2004.05188.pdf\">Details are in the paper about paraprobe</a>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What will we learn?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. How to create edge of the dataset robustly and fast without any need for downsampling.\n",
    "2. How to compute analytically the distance of atoms within a certain distance to the dataset edge.\n",
    "3. How far can we safely define ROIs towards the dataset edge.\n",
    "4. How can we automate the post-processing of the results and let paraprobe create automatic LaTex reports."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The key workflow with *paraprobe-surfacer* has two steps:\n",
    "1. **Compute a triangle hull** $\\alpha$-shape.\n",
    "2. **Compute **analytically** the **distance** of **each atom** to the **triangle hull**\n",
    "3. We will use *paraprobe-autoreporter* to post-process the results automatically.\n",
    "4. We will learn how to create LaTex reports from our analysis.\n",
    "\n",
    "Eventually, I may split the tool into two. One for each step but for now these are two tasks executed in sequence inside *paraprobe-surfacer*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## *paraprobe-surfacer* is the tool to computing an edge of the reconstructed volume"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 1: Detail what you want to do"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this tutorial we build on the datasets from the previous tutorials --- to stay focused on the settings specific to *paraprobe-surfacer*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define the location of the paraprobe/code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "basePath = '/home/markus/ParaprobeVideoTutorials/paraprobe'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load Python and *paraprobe-parmsetup*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#load relevant Python3 (standard) packages\n",
    "import os\n",
    "import sys\n",
    "import glob\n",
    "from pathlib import Path\n",
    "import numpy as np\n",
    "#check for existence of specific non-standard packages we also need\n",
    "try:\n",
    "    import periodictable\n",
    "except ImportError as e:\n",
    "    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')\n",
    "try:\n",
    "    import h5py\n",
    "except ImportError as e:\n",
    "    raise ValueError('Install h5py Python package via e.g. pip install h5py !')\n",
    "\n",
    "#import the paraprobe-parmsetup tools we need\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/')\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/src/')\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/src/utils/')\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/src/metadata/')\n",
    "#from PARAPROBE_SlurmSingle import *\n",
    "#from PARAPROBE_BashBatch import *\n",
    "from PARAPROBE_BashSingle import *\n",
    "from PARAPROBE_XML import *\n",
    "from PARAPROBE_Surfacer import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define your computer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MyComputer = { \n",
    "    'Compiler': 'GNU', \n",
    "    'NumberOfComputingNodes': 1,\n",
    "    'NumberOfSocketsPerNode': 1,\n",
    "    'NumberOfProcessesPerSocket': 1 }\n",
    "#'Compiler' alternatively 'ITL' or 'GNU'\n",
    "ProcessesToUse = MyComputer['NumberOfComputingNodes'] * MyComputer['NumberOfSocketsPerNode'] * MyComputer['NumberOfProcessesPerSocket']\n",
    "print('Planning for parallel execution with ' + str(ProcessesToUse) + ' MPI processes')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's take the two experimental datasets and one synthetic dataset from the previous two tutorials. Yes, we can script the processing of as many and as different datasets as we like. We need no range files here any longer because the ranging has been stored in the HDF5 file.<br>\n",
    "This was the purpose and duty of *paraprobe-ranger*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#an array of dataset names\n",
    "ds = np.array( ['PARAPROBE.Transcoder.Results.SimID.1.h5',\n",
    "                'PARAPROBE.Transcoder.Results.SimID.2.h5',\n",
    "                'PARAPROBE.Synthetic.Results.SimID.3.h5'] )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "SimulationID = 0\n",
    "#AnalysisJobsSlurm = {}\n",
    "AnalysisJobsBash = {}\n",
    "for dataset in ds:\n",
    "    SimulationID += 1\n",
    "    print('Creating configuration for surfacer SimID.' + str(SimulationID))\n",
    "    \n",
    "    WhatToDo = {}\n",
    "    #filename of the dataset\n",
    "    recon_fnm = dataset\n",
    "    task = paraprobe_surfacer( SimulationID, recon_fnm )\n",
    "    #how larger should the ROIs be at most, so we need at least to know all atoms closer than rmax to the dataset edge\n",
    "    task.set_roi_rmax( 2.0 )\n",
    "    #skin computes really for the skin only, i.e. atoms farther away get rmax and for all others we compute analytically\n",
    "    #task.set_distancing_skin( 1.0 )\n",
    "    #above is faster than analyzing the distance for each atom, however if you want to get all you choose complete\n",
    "    task.set_distancing_complete()\n",
    "    WhatToDo['1_Surfacer'] = task.run( ProcessesToUse )\n",
    "    #paraprobe-surfacer creates a standardized name for the result, here is how you can read it\n",
    "    #hull_fnm = task.get_hull_filename()\n",
    "    del task\n",
    "    \n",
    "    #now fuse all these steps into a workflow, write a slurm script (to be submitted to workstation or super computer) \n",
    "    myjobname = str('MK') + str(SimulationID)\n",
    "    #slm = paraprobe_slurm_single( myjobname[0:9], SimulationID, 'TALOS', ProcessesToUse, WhatToDo, Compiler )\n",
    "    #slm.write_slurm_script()\n",
    "    #but also write a bash script (to be submitted to a local workstation without super computer)\n",
    "    bsh = paraprobe_bash_single( myjobname[0:9], SimulationID, '', ProcessesToUse, WhatToDo, MyComputer['Compiler'] )\n",
    "    bsh.write_bash_script()\n",
    "    del WhatToDo\n",
    "    \n",
    "    #remember that we want to submit this job\n",
    "    #AnalysisJobsSlurm[N] = slm.get_slurm_script_filename()\n",
    "    AnalysisJobsBash[SimulationID] = bsh.get_bash_script_filename()\n",
    "\n",
    "print('All configuration files created')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Voila, *paraprobe-surfacer* XML configuration files and SH files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ls PARAPROBE.Surfacer.SimID.*.xml\n",
    "! ls BASH.PARAPROBE.Workflow.SimID.*.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We take the XML configuration files, the H5 files, and the SH scripts to the computer where *paraprobe-surfacer* should be executed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 2: Execute *paraprobe-surfacer*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In effect, paraprobe-surfacer will do the number crunching for you, keep track of the metadata, and writes you a set of H5 files with the distances and triangle sets and XDMF files to visualize the triangle set and the distances. Also supplementary XDMF files are created, which are useful for inspecting the results in Paraview or VisIt."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 3: Inspect the results, here with *paraprobe-autoreporter*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I am a fan of distance distributions, maybe because of my grain boundary dynamics background. I would like to motivate you that knowing the distribution how far atoms are away from the dataset edge is valuable information. With this knowledge we can answer the following questions:\n",
    "* How large can I choose the radius of the ROI?\n",
    "* How many atoms will remain if I go for a certain ROI radius?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Not everybody might be familiar with how to compute such distance distributions. Even if you know exactly what to do, it is very likely that you will implement it again. This makes not much sense. In fact, how to colorize certain diagrams is a question of personal preference but what a cumulative distribution of distances of atoms to the dataset edge is clear once the dataset edge is well defined. Therefore, I thought it is a good idea to develop a tool that allows us to reutilize steps for creating figures. A tool which automatically creates such content from the meta- and the heavy data of the individual paraprobe tool runs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## paraprobe-autoreporter is a tool for this task"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It assists us with several tasks:\n",
    "* Create defined reproducible figures from H5 files and metadata automatically\n",
    "* Create content for an automatic report to help us focus on the science within the data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To get us started, I will show a simple example of *paraprobe-autoreporter* in action. In the following tutorials, we will go into more details."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load paraprobe-autoreporter"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#import the paraprobe-autoreporter tools we need\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/latex/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/metadata/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/plotting/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/utils/')\n",
    "#utility tools of paraprobe-autoreporter\n",
    "from PARAPROBE_Autoreporter_Numerics import *\n",
    "from PARAPROBE_Autoreporter_CorpDesign import *\n",
    "from PARAPROBE_Autoreporter_Profiler import *\n",
    "from PARAPROBE_Autoreporter_LatexGenerator import *\n",
    "#the autoreporter class specific for surfacer\n",
    "from PARAPROBE_Autoreporter_Surfacer import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Run *paraprobe-autoreporter* on *paraprobe-surfacer* results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pretty much in the same spirit as we scripted *paraprobe-parmsetup* command, we can now create an analysis with *paraprobe-autoreporter*.<br>Given that *paraprobe-autoreporter* is a Python tool, we can run it directly in this Jupyter notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#we can run paraprobe-autoreporter as a trivial loop to process as many datasets as we have computing power for\n",
    "SimID = 0\n",
    "for dataset in ds:\n",
    "    SimID += 1\n",
    "    \n",
    "#or we can run the analysis, here for educational purposes for a single analysis only\n",
    "SimID = 3\n",
    "#we need a Python string of the simulation ID\n",
    "SimulationID = str(SimID)\n",
    "print('Autoreporting for SimID.' + SimulationID)\n",
    "\n",
    "#standardized filename of reconstructed dataset (synthetic/experimental)\n",
    "recon_fnm = dataset\n",
    "#standardized filename of paraprobe-surfacer results\n",
    "hull_fnm = 'PARAPROBE.Surfacer.Results.SimID.' + SimulationID + '.h5'\n",
    "\n",
    "res = {}\n",
    "prof = {}\n",
    "#we want to create a Latex report, give same user-defined names and infos\n",
    "reportfilename = recon_fnm + '.tex'\n",
    "caption = 'Results SimID.' + SimulationID\n",
    "author = r'Markus K\\\"uhbach'\n",
    "\n",
    "#create a report object\n",
    "tex = autoreporter_latex( reportfilename, caption, author )\n",
    "#let paraprobe-autoreporter parse metadata and data from paraprobe-surfacer results\n",
    "res['1_Surfacer'] = autoreporter_surfacer( recon_fnm, hull_fnm )\n",
    "#interest how long the run took?\n",
    "prof['1_Surfacer'] = autoreporter_profiler( 'Surfacer', SimulationID ).report()\n",
    "#generate me the figure for the distance distribution\n",
    "res['1_Surfacer'].get_distances()\n",
    "#add a section in the automatic report which summarizes these results\n",
    "tex.add_section_surfacer( res['1_Surfacer'].report(), '' )\n",
    "#add also a section how long the analysis took\n",
    "tex.add_section_profiling( prof, r'' )\n",
    "\n",
    "#create the Latex document\n",
    "tex.write_report()\n",
    "del tex"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ls PARAPROBE.Surfacer.Results.SimID.*.png"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Evidently, autoreporter created figures."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's display for instance the one from the synthetic dataset (SimID.3) and inspect it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Image\n",
    "Image(filename='PARAPROBE.Surfacer.Results.SimID.3.h5.CDF.Ion2EdgeDist.png', width = 600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us understand what we have achieved here. All the pieces of information, from which dataset e.g. the results were taken, the $\\alpha$-value of the hull, and the bookkeeping was handled automatically."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### What do we see here?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A cumulative distribution of the distances of the ions/atoms to the edge of the dataset.<br>\n",
    "The $\\alpha$-shape representing this edge has an $\\alpha$ value of $0.257$ (1/nm)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### What does the curve tell us?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Like every cumulative distribution, how many ions are closer than a certain distance to the dataset edge.<br>\n",
    "We see that for this particular synthetic dataset and cylindrical shape $\\approx 50\\%$ of the atoms are closer than $1nm$ to the dataset edge.\n",
    "\n",
    "The dashed orange line is an example of an automatically placed marker which tells us how far we have computed distances. Remember we opted to compute complete distances. Hence, the marker is placed at the maximum distance found. If we would have, alternatively, computed the distances to the $\\alpha$-shape only for atoms in a skin to the $\\alpha$-shape, the marker will be placed at the respective skin distance. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Do we expect such steps in the curve, aka why is the curve not smooth?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember, that the dataset 3, is a synthetically generated one, specifically we started from a perfect single crystal. The only noise comes from missing atoms. Given that we have a lattice, i.e. a highly regular spacing of the atoms, we see steps in the curve. Given that we have missing atoms results in bumps in the $\\alpha$-shape. These bumps result in nudged edges of the curve."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Check the Latex report"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ls *.tex"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## This completes the tutorial. Next, we will learn how to compute tessellations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Tutorial written by Markus Kühbach, last updated 12.11.2020*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Advocating for open-source workflow tools"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I would like to address a few comments on the complexity of paraprobe compared to GUI-based analyses: \"Why so much code for computing a few distances only? I could get this with commercial software with a single button!\" Really? Even if such distances would be computed, is there access to the triangles, what does the source code do in detail, what do you do if you have multiple datasets to analyze? How do you document with the commercial software or ad hoc implementations what you have done in such detail as it does paraprobe, like shown here? What do you do if your commercial software is not capable of handling your large datasets, would you just downsample, drop it, or  deem such numerically costly data analyses unnecessary? I am convinced *paraprobe* is a useful addition to the software landscape in APT to help you with quantifying and eventually reconsidering your opinion. Let me know your thoughts.\n",
    "\n",
    "Other criticism towards such scripting of APT analyses reads as follows: \"But if I need to submit my job first to a cluster I have to wait.\" **Yes** waiting in cluster queues can take time, I have spent time in such queues as well. However, once you get your turn you can instruct all your analyses at once, without any having to worry about licenses. Try this with the commercial software. I feel, we should as researchers better ask us how we can get the same flexibility like above and such tools better integrated and even more flexible."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Professional advice on how many cores to use"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\"I have heard criticism that computing distances of points to geometric primitives (triangles, spheres) can be numerically costly. There are people who expect tools like IVAS and others to be faster on this or people who expect that parallelization can remove all these limitations.\" But think a little bit longer what it means to compute e.g. ROIs up to 10 nanometers. Make an experiment of thought for yourself. How close is each atom to the edge of the dataset? You have only a triangle mesh. So, you do not know *a priori* which of the triangle is the closest to each atom. So then you also do not know which of your ROIs protrude out of the dataset. So how do you quantify your bias? I would be happy to hear your suggestions. \n",
    "\n",
    "Paraprobe offers a solution with its incremental and tree-based computing of the distance. In addition, *paraprobe-surfacer* supports both multithreading and MPI process parallelism and a few other programming tricks. Employing more cores to solve the task is likely useful, especially for very large datasets (multi-hundred million). This is one of the unique capabilities of paraprobe. \n",
    "\n",
    "In effect, you can set **ProcessesToUse** to a larger number. However, keep in mind that the total number of cores used is always $N_{total} = N_{threads} \\cdot N_{processes}$. So make sure you have enough physical cores on your system. Otherwise, your run may eventually execute slower than faster because too many cores fight for resources and the few computational tasks. My advice is that for most workstations in APT, keeping ProcessesToUse = 1 suffices. If you have a two-socket solution choose 2. If you go on a computer cluster, like we did for TALOS, the FHI/MPIP/MPIE cluster in Garching, we choose of course ProcessesToUse = 80 when running the full system, so our equation becomes $N_{total} = 40 \\cdot 80$. In special cases it might be faster to use two processes per socket, in particular if you have many threads per core. Be creative and experiment here, the best settings are always case dependent! <font color=\"blue\">Me and other APTers would be happy to hear your feedback. Also feel free to drop me a message if there are errors popping up when using multiple processes. Be so kind to sent the STDOUT/STDERR log files with your run, this helps me tracing.</font>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
