#!/bin/bash

echo "OpenMP NUM THREADS env option"
echo $OMP_NUM_THREADS
echo "OpenMP PLACES env option"
echo $OMP_PLACES
ulimit -s unlimited
echo "ulimit env option"
ulimit


###  paraprobe tool workflow SimID 3

mpiexec -n 1 ./paraprobe_tessellator 3 PARAPROBE.Tessellator.SimID.3.xml 1>PARAPROBE.Tessellator.SimID.3.STDOUT.txt 2>PARAPROBE.Tessellator.SimID.3.STDERR.txt

