{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial 4: Tessellate datasets using *paraprobe-tessellator*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Motivation and approach"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The previous tutorial showed how we can quantify a triangle hull that can represent an edge of the dataset. Another key computational geometry method that is useful for APT datasets are tessellations. A tessellation of a dataset enables us to answer questions like how much volume of the dataset is closest to a particular atom or how we can identify topologically robust neighborhood relations between atoms."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What will we learn?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. How to tessellate the point cloud that a reconstructed APT dataset represents robustly using Voronoi tessellations.\n",
    "2. How to characterize the volume, geometry, and topology of each Voronoi cell, given that Voronoi cells are convex polyhedra, one polyhedron per atom/point, with each having distinct polygon facets and facet junctions.\n",
    "3. How to identify and treat separately the Voronoi cells at the edge of the dataset, whose shape is ill-defined."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## *paraprobe-tessellator* is the tool for computing such tessellations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compared to previously reported community tools for APT and specifically implementations of Voronoi tessellations in Matlab or Python, *paraprobe-tessellator* uses multithreading and process parallelism at the same time to speed up the computation. The tool is currently the only one that is capable of handling tessellations of very large, i.e. multi-hundred million ion APT datasets. The tools works equally for real or synthetic datasets."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 1: Detail what you want to do"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Like you learn in the previous examples, it is possible to analyze multiple dataset via trivial parallelism. For this tutorial we take the synthetic dataset that we created in tutorial 2. You should by now be already quite familiarized with the three-stepped workflow. Configure, execute, post-process. So let's use *paraprobe-parmsetup* to instruct an tessellation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define the location of the paraprobe/code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "basePath = '/home/markus/ParaprobeVideoTutorials/paraprobe'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load Python and *paraprobe-parmsetup*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#load relevant Python3 (standard) packages\n",
    "import os\n",
    "import sys\n",
    "import glob\n",
    "from pathlib import Path\n",
    "import numpy as np\n",
    "#check for existence of specific non-standard packages we also need\n",
    "try:\n",
    "    import periodictable\n",
    "except ImportError as e:\n",
    "    raise ValueError('Install periodictable Python package via e.g pip install periodictable !')\n",
    "try:\n",
    "    import h5py\n",
    "except ImportError as e:\n",
    "    raise ValueError('Install h5py Python package via e.g. pip install h5py !')\n",
    "\n",
    "#import the paraprobe-parmsetup tools we need\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/')\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/src/')\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/src/utils/')\n",
    "sys.path.append(basePath + '/code/paraprobe-parmsetup/src/metadata/')\n",
    "#from PARAPROBE_SlurmSingle import *\n",
    "#from PARAPROBE_BashBatch import *\n",
    "from PARAPROBE_BashSingle import *\n",
    "from PARAPROBE_XML import *\n",
    "from PARAPROBE_Tessellator import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define your computer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MyComputer = { \n",
    "    'Compiler': 'GNU', \n",
    "    'NumberOfComputingNodes': 1,\n",
    "    'NumberOfSocketsPerNode': 1,\n",
    "    'NumberOfProcessesPerSocket': 1 }\n",
    "#'Compiler' alternatively 'ITL' or 'GNU'\n",
    "ProcessesToUse = MyComputer['NumberOfComputingNodes'] * MyComputer['NumberOfSocketsPerNode'] * MyComputer['NumberOfProcessesPerSocket']\n",
    "print('Planning for parallel execution with ' + str(ProcessesToUse) + ' MPI processes')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The professional advice I gave to the end of tutorial 3 about combining processes and threads holds true also for *paraprobe-tessellator*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Atoms on the edge of the dataset are troublemakers ..., like in *paraprobe-surfacer*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Atoms at the edge of the dataset challenge again all straightforward attempts to compute tessellations. In fact, tessellations partition up the space between points according to a tessellation rule. However, for finite datasets there are no points beyond the edge. Equally worse, let's assume our atom point cloud is embedded in infinite space, then there are infinitely many points to each atom at the dataset edge."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So the key question that every analysis tool needs to answer is: where does a cell to a point/atom end?<br>\n",
    "This is ill-defined. In *paraprobe-tessellator* we solve this pragmatically by supplementing distance information, computed e.g. with *paraprobe-surfacer*. Analyzing geometrically the intersection between the triangle mesh of the dataset edge and a tessellation is tricky and numerically costly, if done rigorously, especially if the mesh is not water-tight which is not guaranteed if using $\\alpha$-shapes. Therefore, I opted for an intermediate solution which is to inject results from *paraprobe-surfacer*.<br>\n",
    "My solution so far is to inform the tessellator tool for which points close to the dataset edge the computation might be problematic. This gives a practical mechanism to identify these cells at the dataset edge and threat them separately.<br>\n",
    "One may argue that computing intersection between triangle meshes and Voronoi cells would be more rigorous. However, we should not forget that also the choice to use an $\\alpha$-shape or, essentially the same idea, a convex hull is also an assumption."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's configure for the three datasets from the previous tutorial. Again, we need no range file because the ranging is stored in the HDF5 file. To support distance data though we form pairs of datasets: Pairs of x,y,z,mq and atom type data, here injected via results from *paraprobe-transcoder* and *paraprobe-synthetic* and supplemented by matching *paraprobe-surfacer* results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds_srf = np.array( [['PARAPROBE.Transcoder.Results.SimID.1.h5','PARAPROBE.Surfacer.Results.SimID.1.h5'],\n",
    "                    ['PARAPROBE.Transcoder.Results.SimID.2.h5','PARAPROBE.Surfacer.Results.SimID.2.h5'],\n",
    "                    ['PARAPROBE.Synthetic.Results.SimID.3.h5','PARAPROBE.Surfacer.Results.SimID.3.h5']])\n",
    "\n",
    "SimulationID = 0\n",
    "AnalysisJobsSlurm = {}\n",
    "AnalysisJobsBash = {}\n",
    "for dataset in ds_srf:\n",
    "    SimulationID += 1\n",
    "    print('Creating configuration for tessellator SimID.' + str(SimulationID))\n",
    "    WhatToDo = {}\n",
    "    \n",
    "    recon_fnm = dataset[0]\n",
    "    hull_fnm = dataset[1]\n",
    "    \n",
    "    #build a tessellation of the dataset, that will be useful for the next step...\n",
    "    task = paraprobe_tessellator( SimulationID, recon_fnm, hull_fnm )\n",
    "    #on which cells so-and-so-close to the edge should be pay attention?\n",
    "    task.set_cell_erosion( 1.0 ) #nm\n",
    "    #no worries, below command computes tessellates for all atoms plus gives the ones at the edge additional pampering\n",
    "    WhatToDo['1_Tessellator'] = task.run( ProcessesToUse )\n",
    "    del task\n",
    "    \n",
    "    #now fuse all these steps into a workflow, write a slurm script (to be submitted to workstation or super computer) \n",
    "    myjobname = str('MK') + str(SimulationID)\n",
    "    #slm = paraprobe_slurm_single( myjobname[0:9], SimulationID, 'TALOS', ProcessesToUse, WhatToDo, Compiler )\n",
    "    #slm.write_slurm_script()\n",
    "    #but also write a bash script (to be submitted to a local workstation without super computer)\n",
    "    bsh = paraprobe_bash_single( myjobname[0:9], SimulationID, '', ProcessesToUse, WhatToDo, MyComputer['Compiler'] )\n",
    "    bsh.write_bash_script()\n",
    "    del WhatToDo\n",
    "    \n",
    "    #remember that we want to submit this job\n",
    "    #AnalysisJobsSlurm[N] = slm.get_slurm_script_filename()\n",
    "    AnalysisJobsBash[SimulationID] = bsh.get_bash_script_filename()\n",
    "\n",
    "print('All configuration files created')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Done, we have now the *paraprobe-tessellator* XML configuration files and SH files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ls PARAPROBE.Tessellator.SimID.*.xml\n",
    "! ls BASH.PARAPROBE.Workflow.SimID.*.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 2: Execute *paraprobe-tessellator*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Place the H5 files to which you refer in the script above to where you want to execute *paraprobe-tessellator* and place next to these also the XML and SH files. Then, execute, either interactively on the console or by submitting to queue using the created scripts. In effect, paraprobe-tessellator will do the number crunching and computational geometry for you plus keep track of the metadata."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 3: Inspect the results, here with *paraprobe-autoreporter*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As I told you I am a fan of distributions; *paraprobe-tessellator* gives us a few new one of these to explore.<br>\n",
    "The most important is the distribution of volume for the Voronoi cell to each atom. With this we can quantify\n",
    "* What is the distribution of atomic scale concentration values?\n",
    "* How much volume is closest to each atom?\n",
    "* What is the accumulated volume of Voronoi cells?\n",
    "* How much volume accumulates for atoms with a certain threshold distance to the dataset edge?\n",
    "* <font color=\"red\">##MK::TO DO: What is the topology of neighbors?</font>\n",
    "* <font color=\"red\">##MK::TO DO: Can we build blobs of atoms by building blobs of Voronoi cells?</font>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's use *paraprobe-autoreporter* to post-process the *paraprobe-tessellator* results automatically."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load paraprobe-autoreporter"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#import the paraprobe-autoreporter tools we need\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/latex/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/metadata/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/plotting/')\n",
    "sys.path.append(basePath + '/code/paraprobe-autoreporter/src/utils/')\n",
    "from PARAPROBE_Autoreporter_Numerics import *\n",
    "from PARAPROBE_Autoreporter_CorpDesign import *\n",
    "from PARAPROBE_Autoreporter_Profiler import *\n",
    "from PARAPROBE_Autoreporter_LatexGenerator import *\n",
    "\n",
    "from PARAPROBE_Autoreporter_Surfacer import *\n",
    "from PARAPROBE_Autoreporter_Tessellator import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Run paraprobe-autoreporter on paraprobe-tessellator/-surfacer results¶"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#as learned in the previous tutorials, you can build simple loops to process multiple datasets\n",
    "#SimID = 0\n",
    "#for dataset in ds_srf:\n",
    "#    SimID += 1\n",
    "    \n",
    "SimID = 3\n",
    "#we need a Python string of the simulation ID\n",
    "SimulationID = str(SimID)\n",
    "print('Autoreporting for SimID.' + SimulationID)\n",
    "\n",
    "recon_fnm = dataset[0]\n",
    "hull_fnm = dataset[1]\n",
    "\n",
    "res = {}\n",
    "prof = {}\n",
    "#create a Latex report\n",
    "reportfilename = recon_fnm + '.tex'\n",
    "caption = 'Results SimID.' + SimulationID\n",
    "author = r'Markus K\\\"uhbach'\n",
    "tex = autoreporter_latex( reportfilename, caption, author )\n",
    "\n",
    "#creating a reporter class specific for tessellator tool\n",
    "#standardized filename for tessellator results\n",
    "#mind the 'Stats' suffix, we want to separate lean values per atom for heavy data for cell shape and geometry\n",
    "tess_fnm = 'PARAPROBE.Tessellator.Results.SimID.' + str(SimulationID) + '.Stats.h5'    \n",
    "\n",
    "res['1_Tessellator'] = autoreporter_tessellator( recon_fnm, hull_fnm, tess_fnm )\n",
    "prof['1_Tessellator'] = autoreporter_profiler( 'Tessellator', SimulationID ).report()\n",
    "#cumulated distribution Voronoi cell volume\n",
    "res['1_Tessellator'].get_voronoi_volume_distro()\n",
    "#compute me the volume of the specimen for different settings when eroding ions from the dataset\n",
    "res['1_Tessellator'].get_sum_voronoi_volume_vs_distance()\n",
    "tex.add_section_tessellator( res['1_Tessellator'].report(), '' )\n",
    "#parsing profiling data from *.xls data if desired how long the analyses took\n",
    "prof['1_Tessellator'] = autoreporter_profiler( 'Tessellator', SimulationID ).report()\n",
    "\n",
    "#hand over all results and figures to the reporter class to write the corresponding latex code\n",
    "#we have this dictionary and handing over because we want that the report can contain multiple sections\n",
    "tex.add_section_tessellator( res['1_Tessellator'].report(), '' )\n",
    "tex.add_section_profiling( prof, r'' )\n",
    "tex.write_report()\n",
    "del tex"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "! ls PARAPROBE.Tessellator.Results.SimID.*.png"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Evidently, autoreporter created figures."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Inspect your working directory. There will be a collection of PNG figures and a TEX source file for the Latex report. <font color=\"red\"><br>Simply compile the *.tex files with your favorite Latex editor</font> or use parts of these *.tex file to compose your<br>\n",
    "supplementary material for your paper more reproducibly and efficiently."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inspect the results and figures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Distribution of Voronoi cell volume"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "from IPython.display import Image\n",
    "Image(filename='PARAPROBE.Tessellator.Results.SimID.3.Stats.h5.VoroTess.Results.VolCDF.png', width = 600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see the distribution of cell volume for our ideal single crystal lattice with randomly missing atoms.\n",
    "\n",
    "First of all we should understand what is $d_{ero}$. This is the threshold distance below which we said for which atoms closer to the edge of the dataset we do not want to compute the Voronoi cell because its geometry is ill-posed. By definition, *paraprobe-tessellator* sets the volume of such atoms to 0.0. Remember, that the distribution of distances on this dataset (tutorial 3) told us that about $\\approx 50\\%$ of the atoms are within $1nm$ to the edge."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Therefore, we get the plateau in curve at approximately $50\\%$. In an ideal lattice the volume per atom $V_{at}$ is defined by the volume of the unit cell. In our case, we have a face-centered cubic lattice with $V_{at} = \\frac{(0.404nm)^3}{4} \\approx 0.016 {nm}^3$. We have not squeezed atoms in the lattice, so $V_{at}$ is a strict lower bound. Like it is expected for a perfect crystal, the curve should show an increase which starts at $V_{at}$. Given that we have removed atoms randomly, there will be many atoms with more empty space around them on average. Correspondingly, there is a tail to larger cell volume. For every random sampling it is possible that atom thinning results in spatial clusters of missing atoms, which leaves for some atoms even fewer neighbors. The observed long tail to at most $0.08{nm}^3$ confirms this expectation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compare this with a dataset from experiment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "#Image(filename='PARAPROBE.Tessellator.Results.SimID.1.Stats.h5.VoroTess.Results.VolCDF.png', width = 600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Noteworthy enough, the specimen probed an Al-Sc-Si alloy. We see how spatial noise and missing atoms in concert render the distribution perfectly smooth beyond $d_{ero}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Volume of the dataset as a function of the atom distance to the dataset edge"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another useful results can be obtained when combining the atom-to-edge distance information with the accumulated volume of the Voronoi cells. This yields an alternative answer for the total volume of the dataset as a function of the distance of the atoms to the edge of the dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "Image(filename='PARAPROBE.Tessellator.Results.SimID.3.Stats.h5.VoroTess.Results.VolVsDist.png', width = 600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can understand such curve needs to look like. We expected that the maximum accumulated volume is limited by $d_{ero}$. For larger distances we can imagine that we have eroded atoms (Voronoi cells) from the edge of the dataset. Correspondingly, the volume of the dataset becomes smaller and smaller the more atoms we erode. The steps are again expected because we probe an ideal lattice. This dictates at which next erosion distance we are able to scoop off the \"next layer\" of atoms. With stronger positional noise these steps are smoothened out, until the curve becomes smooth for real datasets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Image(filename='PARAPROBE.Tessellator.Results.SimID.1.Stats.h5.VoroTess.Results.VolVsDist.png', width = 600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I think this is enough computational geometry with tessellations for now."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## This completes the tutorial. The next tutorial will cover spatial statistics including two-point statistics (or spatial distribution maps, as they are referred to in APT jargon)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Tutorial written by Markus Kühbach, last updated 13.11.2020*"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
