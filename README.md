# paraprobe-toolbox

This repository summarizes the work that was performed at the Max-Planck-Institut für Eisenforschung GmbH
(MPIE) on a software package called the paraprobe-toolbox. This is a collection of software for strong-
scaling distributed- and shared-memory CPU-parallelized tools for mining atom probe tomography data
as well as GPU-parallelized tools for atom probe crystallography. The main part of the work was
documented in the following two papers:
* https://doi.org/10.1038/s41524-020-00486-1
* https://doi.org/10.1107/S1600576721008578

This repository contains the relevant code for these publications. I consider the version
here v0.2. After a great and very instructive time at the MPIE, which was made possible
by many colleagues, especially Franz Roters, Baptiste Gault, Andrew Breen, and Dierk Raabe,
it was time to move on with my journey in the field of data-centric research at the interface
of (applied) condensed-matter physics and materials engineering.

To assure that the benefits of the paraprobe-toolbox get not lost and - most importantly - 
to remain able in the future to offer the software via an MPCDF/MPIE-independent repository,
I have decided to communicate the new developments of the software via a different repository:

https://gitlab.com/paraprobe/paraprobe-toolbox

Therefore, the specific code in this repository will no longer be maintained but the majority
of the functionalities have been migrated and developed further. A preprint with a summary
of the newer developments to functionalities of the paraprobe-toolbox is available here:

https://arxiv.org/abs/2205.13510

Thanks to the continued support and many atom probers, I am thankful that the project can
continue. With the integration into the research data management system NOMAD OASIS
of the FAIRmat project, I am convinced that we are heading for a bright and interesting future
with the paraprobe-toolbox to continue supporting scientists who wish to apply complementary
computational methods on their atom probe data which are off-the-beaten path and which show
a glimpse of what is possible with open-source community-driven software.

Please consider the code in the new repository the relevant one in the future.
As usual feel free to contact me with specific questions about the paraprobe-toolbox
and questions about research data management related to e.g. atom probe, electron microscopy,
and multi-scale materials modeling techniques:
* https://www.fair-di.eu/fairmat/about-fairmat/team-fairmat

***

**Purpose:**  
Strong-scaling distributed- and shared-memory CPU-parallelized tools for mining atom probe tomography data and GPU-parallelized tools for atom probe crystallography.

**Documentation:**  
A detailed documentation is available here:  
https://paraprobe-toolbox.readthedocs.io  

**Preprint to papers specific for these and related tools:**  
https://arxiv.org/abs/2004.05188
https://arxiv.org/abs/2009.00735
https://dx.doi.org/10.1088/1361-651X/ab7f8c

**Dependencies:**  
- Required: HDF5, MPI/OpenMP-capable compiler like GNU, CGAL, Voro++
- Recommended: Python
- Optionally, for more convenience: Matlab
- Optionally, for GPU processing: CUDA and OpenACC-capable compiler (either PGI or GNU with NVPTX), Intel compiler

**Execution:**  
Paraprobe tools parse their tasks and settings from configuration files.
You can either write command-line scripts to create these and execute the tools
or use the Python wizard tools paraprobe-parmsetup and paraprobe-autoreporter to 
get assistance with creating the input files and process the results.
Individual tools are executed after proper environment setting as MPI programs.

**Get data in and out:**  
The individual tools read from and write to HDF5 files.
Data from experiment, in the form of APT-specific file formats like
POS, EPOS, APT, RRNG, and RNG, can be transcoded with paraprobe-transcoder and paraprobe-parmsetup, respectively.

**How to cite:**  
The code in this repository is under active development. All utilization of code portions or ideas from it should be cited::  

    Markus Kühbach, Priyanshu Bajaj, Andrew Breen, Eric A. Jägle, and Baptiste Gault
    On Strong Scaling Open Source Tools for Mining Atom Probe Tomography Data
    Microscopy and Microanalysis, Vol 25, Supplement S2, 2019
    https://dx.doi.org/10.1017/S1431927619002228


**PARAPROBE** Strong-scaling tools for atom probe tomography  
https://gitlab.mpcdf.mpg.de/mpie-aptfim-toolbox/paraprobe  
