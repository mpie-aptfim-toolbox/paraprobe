#!/bin/bash
module purge

module load Compilers/Intel/Intel2018
module load MPI/Intel/IntelMPI2018
module load Libraries/IntelMKL2018
module list

export OMP_NUM_THREADS=36
export OMP_PLACES=cores
echo "OpenMP NUM THREADS env option"
echo $OMP_NUM_THREADS
echo "OpenMP PLACES env option"
echo $OMP_PLACES
ulimit -s unlimited
echo "ulimit env option"
ulimit
echo "Running on MAWS15"


#this is the toolchain
mpiexec -n 1 ./paraprobe_synthetic 0 PARAPROBE.Synthetic.SimID.0.xml 1>PARAPROBE.Synthetic.SimID.0.STDOUT.txt 2>PARAPROBE.Synthetic.SimID.0.STDERR.txt
mpiexec -n 1 ./paraprobe_ranger 0 PARAPROBE.Ranger.SimID.0.xml 1>PARAPROBE.Ranger.SimID.0.STDOUT.txt 2>PARAPROBE.Ranger.SimID.0.STDERR.txt
mpiexec -n 1 ./paraprobe_surfacer 0 PARAPROBE.Surfacer.SimID.0.xml 1>PARAPROBE.Surfacer.SimID.0.STDOUT.txt 2>PARAPROBE.Surfacer.SimID.0.STDERR.txt
mpiexec -n 1 ./paraprobe_spatstat 0 PARAPROBE.Spatstat.SimID.0.xml 1>PARAPROBE.Spatstat.SimID.0.STDOUT.txt 2>PARAPROBE.Spatstat.SimID.0.STDERR.txt
mpiexec -n 1 ./paraprobe_tessellator 0 PARAPROBE.Tessellator.SimID.0.xml 1>PARAPROBE.Tessellator.SimID.0.STDOUT.txt 2>PARAPROBE.Tessellator.SimID.0.STDERR.txt
