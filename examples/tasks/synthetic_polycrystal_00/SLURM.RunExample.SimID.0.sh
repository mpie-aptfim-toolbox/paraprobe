#!/bin/bash -l
#SBATCH -o ./PARAPROBE.RunExample.SimID.0.STDOUT.%j
#SBATCH -e ./PARAPROBE.RunExample.SimID.0.STDERR.%j
#SBATCH -D ./
#SBATCH -J paraprobe-example
#SBATCH --partition=p.talos
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=40
#SBATCH --mail-user=m.kuehbach@mpie.de
#SBATCH 95:55:00
#submits to p.talos make jobs currently by default to run exclusively on resources
module load cmake
module load intel
module load impi
module load mkl
module load boost
module list
if [ ! -z $SLURM_CPUS_PER_TASK ] ; then
	export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
else
	export OMP_NUM_THREADS=1
fi
export OMP_PLACES=cores
echo "OpenMP NUM THREADS env option"
echo $OMP_NUM_THREADS
echo "OpenMP PLACES env option"
echo $OMP_PLACES
ulimit -s unlimited
echo "ulimit env option"
ulimit
echo $SLURM_JOB_NODELIST
echo $SLURM_JOB_NUM_NODES
echo "job $SLURM_JOB_NAME with job id $SLURM_JOB_ID is running on $SLURM_JOB_NUM_NODES node(s): $SLURM_JOB_NODELIST"


#this is the toolchain
srun paraprobe_synthetic 0 PARAPROBE.Synthetic.SimID.0.xml 
srun paraprobe_ranger 0 PARAPROBE.Ranger.SimID.0.xml
srun paraprobe_surfacer 0 PARAPROBE.Surfacer.SimID.0.xml
srun paraprobe_spatstat 0 PARAPROBE.Spatstat.SimID.0.xml
srun paraprobe_araullo 0 PARAPROBE.Araullo.SimID.0.xml
srun paraprobe_fourier 0 PARAPROBE.Fourier.SimID.0.xml
srun paraproeb_indexer 0 PARAPROBE.Indexer.SimID.0.xml