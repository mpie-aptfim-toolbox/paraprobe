#!/bin/bash
module purge

module load Compilers/Intel/Intel2018
module load MPI/Intel/IntelMPI2018
module load Libraries/IntelMKL2018
module list

export OMP_NUM_THREADS=20
export OMP_PLACES=cores
echo "OpenMP NUM THREADS env option"
echo $OMP_NUM_THREADS
echo "OpenMP PLACES env option"
echo $OMP_PLACES
ulimit -s unlimited
echo "ulimit env option"
ulimit
echo "Running on MAWS30"


#this is the toolchain
mpiexec -n 1 ./paraprobe_synthetic 0 PARAPROBE.Synthetic.SimID.0.xml 1>PARAPROBE.Synthetic.SimID.0.STDOUT.txt 2>PARAPROBE.Synthetic.SimID.0.STDERR.txt
mpiexec -n 1 ./paraprobe_ranger 0 PARAPROBE.Ranger.SimID.0.xml 1>PARAPROBE.Ranger.SimID.0.STDOUT.txt 2>PARAPROBE.Ranger.SimID.0.STDERR.txt
mpiexec -n 1 ./paraprobe_surfacer 0 PARAPROBE.Surfacer.SimID.0.xml 1>PARAPROBE.Surfacer.SimID.0.STDOUT.txt 2>PARAPROBE.Surfacer.SimID.0.STDERR.txt
mpiexec -n 1 ./paraprobe_spatstat 0 PARAPROBE.Spatstat.SimID.0.xml 1>PARAPROBE.Spatstat.SimID.0.STDOUT.txt 2>PARAPROBE.Spatstat.SimID.0.STDERR.txt
mpiexec -n 1 ./paraprobe_araullo 0 PARAPROBE.Araullo.SimID.0.xml 1>PARAPROBE.Araullo.SimID.0.STDOUT.txt 2>PARAPROBE.Araullo.SimID.0.STDERR.txt
mpiexec -n 1 ./paraprobe_fourier 0 PARAPROBE.Fourier.SimID.0.xml 1>PARAPROBE.Fourier.SimID.0.STDOUT.txt 2>PARAPROBE.Fourier.SimID.0.STDERR.txt

#to index we need first a reference against which to compare
	mpiexec -n 1 ./paraprobe_synthetic 210001 PARAPROBE.Synthetic.SimID.210001.xml 1>PARAPROBE.Synthetic.SimID.210001.STDOUT.txt 2>PARAPROBE.Synthetic.SimID.210001.STDERR.txt
	mpiexec -n 1 ./paraprobe_ranger 210001 PARAPROBE.Ranger.SimID.210001.xml 1>PARAPROBE.Ranger.SimID.210001.STDOUT.txt 2>PARAPROBE.Ranger.SimID.210001.STDERR.txt
	mpiexec -n 1 ./paraprobe_surfacer 210001 PARAPROBE.Surfacer.SimID.210001.xml 1>PARAPROBE.Surfacer.SimID.210001.STDOUT.txt 2>PARAPROBE.Surfacer.SimID.210001.STDERR.txt
	mpiexec -n 1 ./paraprobe_araullo 210001 PARAPROBE.Araullo.SimID.210001.xml 1>PARAPROBE.Araullo.SimID.210001.STDOUT.txt 2>PARAPROBE.Araullo.SimID.210001.STDERR.txt
#now that we have the library entry we can index
mpiexec -n 1 ./paraprobe_indexer 0 PARAPROBE.Indexer.SimID.0.xml 1>PARAPROBE.Indexer.SimID.0.STDOUT.txt 2>PARAPROBE.Indexer.SimID.0.STDERR.txt
