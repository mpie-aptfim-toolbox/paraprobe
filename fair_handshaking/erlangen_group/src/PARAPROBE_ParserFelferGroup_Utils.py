# -*- coding: utf-8 -*-
"""
created 2020/10/06, Markus K\"uhbach, m.kuehbach@mpie.de
a parser to read and interpret data and metadata written by Peter Felfer's group Matlab APT tools 
for using these data and metadata in Python, i.e. to make interoperable with Paraprobe or other APT tools
"""

import numpy as np
import re
import periodictable as pse


def felfer_isotope_split( strng ):
    """
    in: strng: UTF-8 isotope name like 1H, 10N, but 
    out: A, Z, n uint8
    """
    #extract element symbol
    Z = 0
    A = 0    
    n = 0
    #strng = '98Mo2'
    #strng = '1H2'
    #strng = '97Mo'
    #parse out element name
    symb = re.sub('\d+', '', strng)
    for el in pse.elements:
        if not symb == el.symbol:
            continue
        else:
            Z = el.number
            break
    if Z == 0:
        raise ValueError(symb + ' is not a valid element symbol !')
    #extract leading A
    tmp = re.sub('^\d+','', strng) #the trailing part
    A = strng[0:len(strng)-len(tmp)]
    #extract trailing n, ##MK::seems to be currently the multiplier for e.g. H2
    tmp = re.sub('\d+$', '', strng) #the leading part
    n = strng[len(tmp):] #handle Peter's inconsistent handling of single isotopes because H2 becomes 1H2 but H is 1H and not 1H1 !
    if n == '':
        n = 1    
    return np.uint8([Z, A, n])


def felfer_molecularion_2_paraprobe_molecularion( strng ):
    """
    in: strng : UTF-8 name string with human readable description of ion
    out: 8x1 uint8 array encoding N1Z1,N2Z2,N3Z3,??,CHARGE
    """
    #currently only single element molecular ions support for this type of plot
    #strng = '98Mo 97Mo2 ++'
    #strng = '1H2 ++'
    #strng = '1H3 +'
    #print(strng)
    components = strng.split(' ')
    # ##MK::it seems that Peter is currently separating molecular ion components and charge by a single space
    nspaces = strng.count(' ')
    if nspaces < 1:
        raise ValueError('strng has unsupported format, is likely incorrectly formatted !')
    # ##MK::it seems also that Peter currently encodes the molecular ion charge by writing out the '+' characters
    nplusses = strng.count('+')
    if nplusses < 1:
        raise ValueError('non-positively charged ions dont fly to the detector, strng is likely incorrectly formatted !')
    # ##MK::it seems Peter encodes nothing else in the ion type key
    ncomp = len(components) - 1 #because last component of strng are the plusses
    if ncomp > 3:
        raise ValueError('Premature exit because such a complex molecular ion is currently not support by Paraprobe !')

    #return array 8x uint8
    nused = 0 #how many isotopes already combined
    ret = np.array([ np.uint8(0), np.uint8(0), np.uint8(0), np.uint8(0), 
                        np.uint8(0), np.uint8(0), np.uint8(0), np.uint8(0), ])
    
    # ##MK::it seems Peter has sorted the isotopes in decreasing A
    for i in np.arange(0,ncomp):
        # ##MK::assuming that there is currently only one isotope building the molecular ion
        Z_A_n = felfer_isotope_split( components[i] )
        #is there enough space to decode possible isotope pairs, triplets etc?
        if Z_A_n[2] > 3-i: #ncomp-i+1:
            raise ValueError('Premature exit because such a complex molecular ion is currently not support by Paraprobe !')
        for j in np.arange(0,Z_A_n[2]):
            ret[(i+j)*2+0] = Z_A_n[0]
            ret[(i+j)*2+1] = Z_A_n[1]
            
    #populate the rest, ##MK::forget about the charges here so leave the last two ret[6:7] = 0
    ret[7] = np.uint8(nplusses)
    return ret
