# -*- coding: utf-8 -*-
"""
created 2020/10/06, Markus K\"uhbach, m.kuehbach@mpie.de
HDF5 group name, dataset name, and attribute dictionaries for HDF5 files created by
members of Peter Felfer's APT group at FAU Erlangen
"""

import numpy as np

#HDF5 group keyword definitions
#definitions relevant for reconstruction
FELFER_RECON='/atomProbeTomography/reconstruction'
FELFER_RECON_ATOM='/atomProbeTomography/reconstruction/atom'
FELFER_RECON_ION='/atomProbeTomography/reconstruction/ion'
FELFER_RECON_ION_ID='/atomProbeTomography/reconstruction/ion/fieldEvaporationSequenceIndex'
FELFER_RECON_ION_MQ='/atomProbeTomography/reconstruction/ion/massToChargeState'
FELFER_RECON_ION_X='/atomProbeTomography/reconstruction/ion/x'
FELFER_RECON_ION_Y='/atomProbeTomography/reconstruction/ion/y'
FELFER_RECON_ION_Z='/atomProbeTomography/reconstruction/ion/z'

#definitions relevant for ranging
FELFER_RANGE_IVALS='/atomProbeTomography/massToChargeRange'

#definitions relevant for arbitrary metadata for the experiment
FELFER_EXP='/experiment'
FELFER_INSTR='/instrument'
FELFER_LAB='/laboratory'
FELFER_META='/metaData'
FELFER_PROJ='/project'
FELFER_SAMPLE='/sample/images'

#HDF5 dataset attribute keyword name definitions
#definitions relevant for massToCharge range
class h5attr():
    def __init__(self, dtype, val, *args, **kwargs):
        self.dtype = dtype
        self.val = val


FELFER_H5ATTR_MQIV = { 'begin': h5attr( np.float64, 0.0 ),
                       'color': h5attr( np.float64, 0.0 ),
                       'end': h5attr( np.float64, 0.0 ),
                       'ion': h5attr( np.str, '' ),
                       'name': h5attr( np.str, '' ) }

#definitions relevant for arbitrary metadata
##MK::h5attr_apt_exp and sub-groups according to similar lines

FELFER_H5ATTR_EXP = { 'beginDateTime': h5attr( np.str, '' ),
                      'dataEmbargoPeriodEndDate': h5attr( np.str, '' ),
                      'dataIsEncrypted': h5attr( np.bool, False ),
                      'endDateTime': h5attr( np.str, '' ),
                      'instrumentOperator': h5attr( np.str, '' ),
                      'latitude': h5attr( np.float64, 0.0 ),
                      'longitude': h5attr( np.float64, 0.0 ),
                      'notes': h5attr( np.str, '' ),
                      'status': h5attr( np.str, '' ),
                      'type': h5attr( np.str, '' ),
                      'uniqueIdentifier': h5attr( np.str, '' ) }
#                      'institution': h5attr( np.str, '' ),

FELFER_H5ATTR_INSTR = { 'acquisitionDevice': h5attr( np.str, '' ),
                        'family': h5attr( np.str, '' ),
                        'lastCalibrationDate': h5attr( np.str, '' ),
                        'manufacturer': h5attr( np.str, '' ),
                        'type': h5attr( np.str, '' )  }

FELFER_H5ATTR_LAB = { 'ambientTemperature': h5attr( np.float64, 0.0 ) }

FELFER_H5ATTR_META = { 'templateURL': '',
                       'templateVersion': '' }

FELFER_H5ATTR_PROJ = { 'confidentiality': h5attr( np.str, '' ),
                       'contactPerson': h5attr( np.str, '' ),
                       'dataSharingLicense': h5attr( np.str, '' ),
                       'endDate': h5attr( np.str, '' ),
                       'identifier': h5attr( np.str, '' ),
                       'startDate': h5attr( np.str, '' )  }
