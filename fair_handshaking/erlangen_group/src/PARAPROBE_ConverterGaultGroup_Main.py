# -*- coding: utf-8 -*-
"""
created 2020/10/06, Markus K\"uhbach, m.kuehbach@mpie.de
a converter to read parsed APT data into PARAPROBE format
"""

#definition: parsers implement a class that is able to read HDF5 data and metadata for a specific method and research group
#e.g. a parser for Peter Felfer's HDF5 results files created by their Matlab tools
#definition:converters implement a class which understands the source format (e.g. the parser class) use it convert into another format
#here a converter taking an instantiated Felfer group parser class and using it to convert into input for PARAPROBE

#Python packages and dependencies
import os
import sys
#sys.path.append('../src/')
import glob, subprocess
import pandas as pd
import numpy as np
import re
import h5py
import periodictable as pse

#parser-specific dependencies
from PARAPROBE_ParserFelferGroup_Defs import *
from PARAPROBE_ParserFelferGroup_Utils import *
from PARAPROBE_ParserFelferGroup_Main import *

#example how to parse results from a HDF5 file of Peter Felfer
#h5src = 'R56_02346-v05.h5'
#tip = apth5_felfergroup( h5src )
#tip.get_experiment()
#tip.get_instrument()
#tip.get_reconstruction()
#tip.get_ranging()

##MK::later use these
#from PARAPROBE_MetadataTranscoder import *
#from PARAPROBE_MetadataRanger import *
##MK::as a quick hack use

PARAPROBE_RECON='/VolumeRecon'
PARAPROBE_RECON_META='/VolumeRecon/Metadata'
PARAPROBE_RECON_RES='/VolumeRecon/Results'
PARAPROBE_RECON_RES_MQ='/VolumeRecon/Results/MQ'
##MK::no topology so far
PARAPROBE_RECON_RES_XYZ='/VolumeRecon/Results/XYZ'

PARAPROBE_RANGE='/Ranging'
PARAPROBE_RANGE_META='/Ranging/Metadata'
PARAPROBE_RANGE_META_ITYP='/Ranging/Metadata/Iontypes'
PARAPROBE_RANGE_META_ITYP_ID='/Ranging/Metadata/Iontypes/IDs'
PARAPROBE_RANGE_META_ITYP_MQ2ID='/Ranging/Metadata/Iontypes/MQ2IDs'
PARAPROBE_RANGE_META_ITYP_IVAL='/Ranging/Metadata/Iontypes/MQIVal'
PARAPROBE_RANGE_META_ITYP_WHAT='/Ranging/Metadata/Iontypes/What'


class apth5_gaultgroup_paraprobe():
    """
    class for converting classes with data and metadata of different research group to paraprobe, i.e. felfer2gault so to say
    """
    def __init__(self, *args, **kwargs):
        pass
    
    def felfer2gault(self, h5felfer):
        #h5felfer = tip
        # ##MK::check if x,y,z, and mq exist and are consistent in their length
        #if h5felfer.reconstruction['mq'].shape[0] == h5felfer.reconstruction['xyz'].shape[0]
        # ##MK::exemplary for the reconstruction
        h5fn = 'PARAPROBE.Converter.Reconstruction.' + h5felfer.h5src
        h5w = h5py.File( h5fn, 'w')
        #create top-level groups
        h5w.create_group(PARAPROBE_RECON_META)
        h5w.create_group(PARAPROBE_RECON_RES)
        #write datasets
        h5w.create_dataset(PARAPROBE_RECON_RES_MQ, data = np.float32(h5felfer.reconstruction['mq']) )
        ##MK::no topology and XDMF right now
        h5w.create_dataset(PARAPROBE_RECON_RES_XYZ, data = np.float32(h5felfer.reconstruction['xyz']) )
        h5w.close()
        print(h5fn + ' created successfully')
        
        h5fn = 'PARAPROBE.Converter.Ranging.' + h5felfer.h5src
        h5w = h5py.File( h5fn, 'w' )
        #create top-level groups
        h5w.create_group(PARAPROBE_RANGE_META_ITYP)
        #convert
        nitypes = len(h5felfer.ranging_felfer_iontype.keys())
        nivals = len(h5felfer.ranging_felfer_ival.keys())
        if nitypes == nivals:
            MQ2IDTbl = np.zeros( [nitypes, 1], np.uint8 )
            MQ2IValTbl = np.zeros( [nitypes, 2], np.float32 )
            WhatTmpTbl = np.zeros( [nitypes, 8], np.uint8 )
            i = 0
            for key in h5felfer.ranging_felfer_iontype:
                val = h5felfer.ranging_felfer_iontype[key]
                #convert into paraprobe specific representation
                MQ2IDTbl[i,0] = i
                MQ2IValTbl[i,0] = h5felfer.ranging_felfer_ival[key][0,0]
                MQ2IValTbl[i,1] = h5felfer.ranging_felfer_ival[key][1,0]                
                WhatTmpTbl[i,:] = felfer_molecularion_2_paraprobe_molecularion( val )
                i = i + 1
            UniqueTbl = np.unique(WhatTmpTbl, axis=0)
            if UniqueTbl.shape == WhatTmpTbl.shape:
                #write datasets
                u8 = np.linspace(0, WhatTmpTbl.shape[0]-1, WhatTmpTbl.shape[0], endpoint=True)
                h5w.create_dataset(PARAPROBE_RANGE_META_ITYP_ID, data = np.uint8(u8) )
                h5w.create_dataset(PARAPROBE_RANGE_META_ITYP_MQ2ID, data = np.uint8(MQ2IDTbl) )
                h5w.create_dataset(PARAPROBE_RANGE_META_ITYP_IVAL, data = np.float32(MQ2IValTbl) )
                h5w.create_dataset(PARAPROBE_RANGE_META_ITYP_WHAT, data = np.uint8(WhatTmpTbl) )
            else:
                raise ValueError('APT-HDF5 of Felfer contains duplicate molecular ion definitions !')
        else:
            raise ValueError('Not every ion has an associated mass-to-charge range !')
        h5w.close()
        print(h5fn + ' created successfully')
            
#example how to use tip to convert it to loop dataset into PARAPROBE
#h5out_recon = 'PARAPROBE.Converter.Reconstruction.R56-02346-v05.h5'
#h5out_range = 'PARAPROBE.Converter.Ranging.R56-02346-v05.h5'
#conv = apth5_gaultgroup_paraprobe()
#conv.felfer2gault( tip )
#this will write a HDF5 file for the reconstruction and ranging named according to h5out formatted correctly for PARAPROBE
