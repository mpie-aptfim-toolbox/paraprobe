# -*- coding: utf-8 -*-
"""
created 2020/10/06, Markus K\"uhbach, m.kuehbach@mpie.de
a parser to read and interpret data and metadata written by Peter Felfer's group Matlab APT tools 
for using these data and metadata in Python, i.e. to make interoperable with Paraprobe or other APT tools
"""

#definition: parsers implement a class that is able to read HDF5 data and metadata for a specific method and research group
#here exemplified for a parser for Peter Felfer's HDF5 results files created by their Matlab tools
#by contrast
#definition:converters implement a class which understands the source format (e.g. the parser class) use it convert into another format
#e.g. for hand-shaking between PARAPROBE and Peters tools

#Python packages and dependencies
import os
import sys
#sys.path.append('../src/')
import glob, subprocess
import pandas as pd
import numpy as np
import re
import h5py
import periodictable as pse

#parser-specific dependencies
from PARAPROBE_ParserFelferGroup_Defs import *
from PARAPROBE_ParserFelferGroup_Utils import *

class apth5_felfergroup():
    """
    class for parsing content of HDF5 files from Peter Felfer's Matlab tools
    """
    def __init__(self, h5fn, *args, **kwargs):
        self.h5src = h5fn
        self.reconstruction = { 'mq': [], 
                                'xyz': [],
                                'sid': [] }
        self.ranging_felfer_ival = None
        self.ranging_felfer_iontype = None
        #self.ranging_paraprobe_iontype = None
        self.experiment = FELFER_H5ATTR_EXP
        self.instrument = FELFER_H5ATTR_INSTR
        self.lab = FELFER_H5ATTR_LAB
        self.metadata = FELFER_H5ATTR_META
        self.project = FELFER_H5ATTR_PROJ
    
    def get_experiment(self):
        #h5fn = h5src
        h5fn = self.h5src
        hf = h5py.File( h5fn, 'r' )
        for keyword in FELFER_H5ATTR_EXP:
            #keyword = 'beginDateTime'
            #keyword = 'latitude'
            #keyword = 'dataIsEncrypted'
            #keyword = 'institution'
            #https://docs.djangoproject.com/en/3.1/topics/i18n/timezones/
            #b'Friedrich-Alexander Universit\xe4t Erlangen-N\xfcrnberg, Department of Materials Science, Institute for General Materials Properties'
            #is not correctly decodeable
            #print(keyword)
            attr_bytes_obj = hf[FELFER_EXP].attrs.get(keyword)
            #print(attr_bytes_obj)
            #type-dependent decoding of the values #.decode('UTF-8') )
            expected_dtype = FELFER_H5ATTR_EXP[keyword].dtype
            if expected_dtype == np.str:
                self.experiment[keyword].val = attr_bytes_obj.decode('UTF-8')
            if expected_dtype == np.float64:
                self.experiment[keyword].val = attr_bytes_obj
            if expected_dtype == np.bool:
                if attr_bytes_obj == b'true':
                    self.experiment[keyword].val = True
                else:
                    self.experiment[keyword].val = False
            #else:
            #    raise ValueError('Unable to decode dtype of the attribute !')
            print(self.experiment[keyword].val)
        print(FELFER_EXP + ' parsed successfully')
           
    def get_instrument(self):
        #h5fn = h5src
        h5fn = self.h5src
        hf = h5py.File( h5fn, 'r' )
        for keyword in FELFER_H5ATTR_INSTR:
            #keyword = 'acquisitionDevice'
            #print(keyword)
            attr_bytes_obj = hf[FELFER_INSTR].attrs.get(keyword)
            #print(attr_bytes_obj)
            #type-dependent decoding of the values #.decode('UTF-8') )
            expected_dtype = FELFER_H5ATTR_INSTR[keyword].dtype
            if expected_dtype == np.str:
                self.instrument[keyword].val = attr_bytes_obj.decode('UTF-8')
            if expected_dtype == np.float64:
                self.instrument[keyword].val = attr_bytes_obj
            if expected_dtype == np.bool:
                if attr_bytes_obj == b'true':
                    self.instrument[keyword].val = True
                else:
                    self.instrument[keyword].val = False
            #else:
            #    raise ValueError('Unable to decode dtype of the attribute !')
            print(self.instrument[keyword].val)
        print(FELFER_INSTR + ' parsed successfully')
            
    def get_reconstruction(self):
        #h5fn = h5src
        h5fn = self.h5src
        hf = h5py.File( h5fn, 'r' )
        try:
            ds = hf[FELFER_RECON_ION]
        except:
            print(FELFER_RECON_ION + ' does not exist in ' + h5fn + ' !')
        if not ds == None:
            #in paraprobe we by default compute all spatial statistics always for original and randomized labels
            #so we get pairs of spatial statistics per task and expect the same target ions and neighbor ions for each task
            #test first that dimensions of x,y,z, and mq match to avoid storing large temporary copies
            if FELFER_RECON_ION_X in ds:
                nx = hf[FELFER_RECON_ION_X].shape[1]
            if FELFER_RECON_ION_Y in ds:
                ny = hf[FELFER_RECON_ION_Y].shape[1]
            if FELFER_RECON_ION_Z in ds:
                nz = hf[FELFER_RECON_ION_Z].shape[1]
            if FELFER_RECON_ION_MQ in ds:
                nmq = hf[FELFER_RECON_ION_MQ].shape[1]
            if nx == ny and nx == nz:
                if nmq == nx:
                    self.reconstruction['xyz'] = np.zeros( [nx, 3], np.float64 )
                    self.reconstruction['xyz'][:,0] = np.transpose(hf[FELFER_RECON_ION_X][0,:]) #use C-style storage order when working with HDF5
                    self.reconstruction['xyz'][:,1] = np.transpose(hf[FELFER_RECON_ION_Y][0,:])
                    self.reconstruction['xyz'][:,2] = np.transpose(hf[FELFER_RECON_ION_Z][0,:])
                    self.reconstruction['mq'] = np.transpose(hf[FELFER_RECON_ION_MQ][0,:])
                else:
                    raise ValueError('Datasets mq is inconsistent in length compared to x,y,z !')
            else:
                raise ValueError('Datasets x, y, z are inconsistent in their length !')
        print(FELFER_RECON_ION + ' parsed successfully')
                
    def get_ranging(self):
        #h5fn = h5src
        h5fn = self.h5src
        hf = h5py.File( h5fn, 'r' )
        try:
            ivals = hf[FELFER_RANGE_IVALS]
        except:
            print(FELFER_RANGE_IVALS + ' does not exist in ' + h5fn + ' !')
        if not ivals == None:
            self.ranging_felfer_ival = {}
            self.ranging_felfer_iontype = {}
            #self.ranging_paraprobe_iontype = {}
            for key in ivals:
                #print(key)
                mqmin = hf[FELFER_RANGE_IVALS + '/' + key].attrs.get('begin')
                mqmax = hf[FELFER_RANGE_IVALS + '/' + key].attrs.get('end')                
                val = hf[FELFER_RANGE_IVALS + '/' + key].attrs.get('ion').decode('UTF-8')
                #print(val)
                #print( felfer_molecularion_2_paraprobe_molecularion( val ) )
                self.ranging_felfer_ival[key] = np.float64([mqmin, mqmax])
                self.ranging_felfer_iontype[key] = val
                #translate representation of molecular ions
                #self.ranging_paraprobe_iontype[key] = felfer_molecularion_2_paraprobe_molecularion( val )
        print(FELFER_RANGE_IVALS + ' parsed successfully')

            
#example how to use
#load a HDF5 file form Peter Felfer's group and transcode it to a file for PARAPROBE
#h5src = 'R56_02346-v05.h5'
#AA = apth5_felfergroup( h5src )
#AA.get_experiment()
#AA.get_instrument()
#AA.get_reconstruction()
#AA.get_ranging()

#get the shape
#AA.reconstruction['mq'].shape
#AA.reconstruction['xyz'].shape
