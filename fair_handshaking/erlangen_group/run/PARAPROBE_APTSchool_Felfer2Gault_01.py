# -*- coding: utf-8 -*-
"""
created 2020/10/06, Markus K\"uhbach, m.kuehbach@mpie.de
exemplifying how to convert from HDF5 of FAU Erlangen to HDF5 of PARAPROBE
"""

#definition: parsers implement a class that is able to read HDF5 data and metadata for a specific method and research group
#e.g. a parser for Peter Felfer's HDF5 results files created by their Matlab tools
#definition:converters implement a class which understands the source format (e.g. the parser class) use it convert into another format
#here a converter taking an instantiated Felfer group parser class and using it to convert into input for PARAPROBE

#Python packages and dependencies
import os
import sys
#sys.path.append('../src/')
import glob, subprocess
import pandas as pd
import numpy as np
import re
import h5py
import periodictable as pse

#parser-specific dependencies
from PARAPROBE_ParserFelferGroup_Defs import *
from PARAPROBE_ParserFelferGroup_Utils import *
from PARAPROBE_ParserFelferGroup_Main import *
from PARAPROBE_ConverterGaultGroup_Main import *

#parse results from a HDF5 file of Peter Felfer
h5src = 'R56_02346-v05.h5'
tip = apth5_felfergroup( h5src )
tip.get_experiment()
tip.get_instrument()
tip.get_reconstruction()
tip.get_ranging()

#convert it to loop dataset into PARAPROBE
conv = apth5_gaultgroup_paraprobe()
conv.felfer2gault( tip )
