This is the first version of a parser and converter Python tool to handshake data between
Peter Felfer's group from FAU Erlangen-N�rnberg and paraprobe.

The run folder has a *.h5 file, formatted like Peter Felfer did it at the time of the FAU APT school in October, 2020.
The *.py file in the run folder shows an example how to read values from such H5 files formatted by Peter
and how to convert from this into an input file that paraprobe can use.

Markus K�hbach, 2020/11/20