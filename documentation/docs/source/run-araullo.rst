Run **paraprobe-araullo**
^^^^^^^^^^^^^^^^^^^^^^^^^

**Firstly**, assure that all input file(s) exist in the local directory (HDF5 file). **Secondly**, assure that there is a properly configured XML paraprobe-araullo settings file. For using GPUs, the same settings as for paraprobe-fourier apply.

..  **Thirdly**, assure that all GPUs are visible at runtime::
..  export CUDA_VISIBLE_DEVICES=comma_separated_list_of_device_ids
..  Comma_separated_list_of_device_ids is typically 0 for a single and 0,1 for two GPUs.

..  **Fourth**, configure the shell environment that PGI and CUDA locations are found::
..  export PGI_CURR_CUDA_HOME=/mypath/to/cuda/10.0.130
..  export PGI=/mypath/to/pgi/19.9
..  **Fifth**, allow for multi-threading::

Enable multi-threading::

    export OMP_NUM_THREADS=20
    export OMP_NESTED=true
    ulimit -s unlimited

Otherwise, similar comments as for paraprobe-spatstat apply. If the workpackages are small, it is recommended to use a single thread per MPI process.

Execute the tool as follows::

    export MYRANKS_EMPLOYED=2
    export SIMID=1
    export XML_TXT=PARAPROBE.Araullo.xml
    export STDOUT_RUN_TXT = PARAPROBE.Araullo.SimID.${SIMID}.STDOUT.txt
    export STDERR_RUN_TXT = PARAPROBE.Araullo.SimID.${SIMID}.STDERR.txt

    mpiexec -n ${MYRANKS_EMPLOYED} ./paraprobe_araullo ${SIMID} ${XML_TXT} 1>${STDOUT_RUN_TXT} 2>${STDERR_RUN_TXT}

Mind the comments made for paraprobe-fourier, especially when using GPUs.
