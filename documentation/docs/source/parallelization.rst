Which parallelization methods are employed?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Individually, the **PARAPROBE tools realize** a **hierarchical and dynamic workload distributing** to make most efficient
use of the computing cores for the specific analysis problem at hand. Advanced Programming Interfaces (APIs) are used to implement these parallelization layers into the software:  

**Process data parallelism** via the Message Passing Interface (MPI_) API:  
The key idea behind process parallelism is the instantiation of clones of the program. Once a tool starts, its state gets cloned on processes which are typically executed on multiple computers. The clones communicate via digital messages. The implementation and handling of the messages is taken care of by the Message Passing Interface library (MPI_). At runtime, the tools invoke MPI library calls to communicate pieces of information between physically disjoint computers. MPI is a library, so it requires installation and linking.

**Shared memory thread data parallelism** via the Open Multi-Processing (OpenMP_) API:  
Per process, most tools partition the data into spatially disjoint chunks. Explicit strategies are applied to map these chunks in physical memory locations which are closest to the computing core that executes a work package. This reduces false sharing and performance degradation on resources with multiple ccNUMA layers. In contrast to MPI, OpenMP builds on preprocessor directives through which corresponding pragmas code get automatically translated into parallel code upon compilation. As such, OpenMP needs no installation but a compiler and potentially an MPI library which supports OpenMP commands.

**Accelerator utilization** via the Compute Unified Device Architecture (CUDA_) and OpenACC_:  
The atom probe crystallography tools of PARAPROBE make use of so-called accelerator or graphics cards (GPUs) to
speed up specific computing-intense parts. A good example for which this is useful are Fourier transform operations. Accelerator utilization encompasses **three methods**.
The first and key of which is **CUDA**. CUDA is a programming language and API, which assists the programmer in offloading specific code portions to one or multiple GPUs. Compiling code with CUDA needs two compilers: one which compiles the code sections for the GPU; and another one, the so-called back-end compiler, which compiles the CPU code sections. These program parts are fused into a single executable program upon linking.
The second method which PARAPROBE employs is **OpenACC**. Like OpenMP, it is an API for compiler directive programming with which the compiler gets instructed to perform automatic code parallelization of specific computing intensive parts of the program. Like OpenMP, OpenACC is a functionality which the compiler needs to support.
The third method is **CUFFT**, one of a set of **CUDA-powered third-party libraries**. CUFFT in PARAPROBE is used to compute fast Fourier transforms via instructing specific API calls. Upon code compilation, the library is linked to the code. At runtime the library functions work through the analysis task portions and offload them to the GPUs dynamically. For workstations with multiple GPUs, PARAPROBE uses a concept where each GPU gets instructed by one MPI process. A single thread instructs offloading to each GPU.

**Streaming instruction data parallelism** (SIMD_) is used:
In addition to above techniques, PARAPROBE employs implementation concepts which promote that the compiler vectorizes the code.
This so-called instruction level parallelism utilizes the fact that modern computing cores can in principle execute multiple commands in parallel in a single CPU cycle. Specific CPU commands, the so-called SIMD commands, are implemented through the compiler as a task during code optimization at compile time. As this generates computer-architecture- and CPU-configuration-specific code, PARAPROBE uses cmake_ **build configuration files** to simplify the compilation stage for the user and thereby automatize much of the details of the code compilation.

 .. _MPI: http://www.mcs.anl.gov/research/projects/mpi/
 .. _OpenMP: http://www.openmp.org/
 .. _CUDA: http://developer.nvidia.com/cuda-zone
 .. _OpenACC: http://www.openacc.org/
 .. _SIMD: http://en.wikipedia.org/wiki/SIMD
 .. _cmake: http://cmake.org/
