Run **paraprobe-reconstruct**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Firstly**, assure that all input file(s) exist in the local directory (APTH5 file and periodic table of elements XML file). **Secondly**, assure that there is a properly configured XML paraprobe-reconstruct settings file.
The same comments as detailed in paraprobe-synthetic apply.

**Finally**, paraprobe-reconstruct is executed::

    export SIMID=1
    export XML_TXT=PARAPROBE.Reconstruct.xml
    export STDOUT_RUN_TXT = PARAPROBE.Reconstruct.SimID.${SIMID}.STDOUT.txt
    export STDERR_RUN_TXT = PARAPROBE.Reconstruct.SimID.${SIMID}.STDERR.txt

    mpiexec -n 1 ./paraprobe_reconstruct ${SIMID} ${XML_TXT} 1>${STDOUT_RUN_TXT} 2>${STDERR_RUN_TXT}

PARAPROBE reconstruct is an MPI program.
