Configure **paraprobe-araullo**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PARAPROBE araullo executes atom probe crystallography methods in reconstruction space proposed by V. Araullo-Peters et al. The motivation is to identify regions in the reconstruction with substantial long-range periodicity of the point cloud. The method evaluates a collection of spherical regions of interest. For each ROI, the analysis has four steps. First, the ions inside the ROI are identified. Second, their positions are projected along the normals of a collection of one-dimensional projections on planes through the ROI origin. Next, possible periodicities of each 1d histograms of projected distances are identified via applying a one-dimensional fast Fourier transform per direction. Finally, the Fourier amplitude spectra is inspected for peaks at positions that match a priori expected interplanar spacing for a known crystal structure. PARAPROBE araullo is a parallel implementation of this method with several modifications. Probing for each direction the same frequency bins yields a spherical image. These images are passed to the PARAPROBE indexer tool to perform template matching against spherical images from well-defined synthetic specimens to index crystal structure and the most likely crystal orientation. PARAPROBE araullo reads in an APTH5 file with the ion positions and range labels. Furthermore, a predefined set of projection directions is required. The analysis is parameterized through a single XML settings file. The results are stored in a separate HDF5 file. The possible settings for a PARAPROBE araullo settings file are as follows:

| **VolumeSamplingMethod**
| Specifies how the ROI should be chosen.
| Set 0 for cuboidal ROI grid.
| Set 1 for placing a single ROI at a user-defined location specified by SamplingPosition.
| Set 2 for placing a single ROI in the center of the specimen bounding box.
| Set 3 for placing a collection of FixedNumberOfROIs spatially uncorrelatedly inside the volume.

|	**InputfilePeriodicTableOfElements**
Specifies like in paraprobe-ranger the periodic table of elements.

|	**InputfileReconstruction**
Specifies the ion positions and range labels.

|	**InputfileHullAndDistances**
Specifies precomputed ion to triangle hull distances to avoid bias for ROIs which lay too close to the dataset boundary.
Expects a HDF5 file of surfacer results with the ending *.h5

|	**ROIRadiusMax**
Specifies in nanometer the maximum radius of the spherical regions of interest to use. Currently the same ROI radius is used for all ROIs.


|	**SamplingGridBinWidthX**
|	**SamplingGridBinWidthY**
|	**SamplingGridBinWidthZ**
Specifies in nanometer the spacing of the cuboidal ROI grid. Significant only when VolumeSamplingMethod is 1.

|	**SamplingPosition**
Specifies in nanometer the position where to place a ROI in specimen reference coordinate system.
Only coordinates above the positive z halfspace are accepted. XYZ coordinates are separated using semicola like so 0.0;0.0;0.0.

|	**FixedNumberOfROIs**
For VolumeSampling 3 how many ROIs to place.

|	**RemoveROIsProtrudingOutside**
Use the information from the surfacer tool to avoid placing ROIs which are not fully contained.

|	**SDMBinExponent**
Needs to be a positive integer. Is interpreted as 2^SDMBinExponent to define the spatial resolution of the binning. Uses one padding bin on either side.

|	**SDMNormalize**
Set to 1 to normalize the intensity values of the spherical image for each ROI individually.

|	**WindowingMethod**
Set to 0 to use a rectangular binning. This offers a high frequency resolution but also high side lobes.

|	**KaiserAlpha**
Only when WindowingMethod is not 0, defines scaling constant in the Kaiser windowing model.

|	**LatticeIontypeCombinations**
Specifies for which particular iontypes at all the projections should be executed and at which frequency positions the resulting Fourier spectra are probed to compose a Fourier intensity image.
Every line is one image. Iontype naming convention from the PARAPROBE spatstat tool apply. Examples how to define what to probe use the following syntax::

    <entry targets="Al" realspace="0.404" realspacemin="0.18225" realspacemax="0.22275"/>

This means project, for each ROI, only the aluminium ions. From the resulting Fourier spectra pick the highest peak on the frequency bin interval defined by realspacemin and realspacemax.

Multiple lines are possible, each line instructs an own analysis, thereby allowing to distinguish crystal structure like Al3Sc. Namely, the Sc atoms of the Al3Sc form a simple cubic sub-lattice with spacing a while the Al atoms have their closest neighbors in 0.5a.

|	**IOStoreHistoCnts**
Set to 1 to export for every ROI and every direction the raw histogram counts. **Be careful, this will consume very much memory and thus should only be used for a few ROIs an debugging purposes. Consult the memory consumption estimates below.**

|	**IOStoreHistoFFTMagn**
Set to 1 to export for every ROI the magnitude of the fast Fourier transform for the raw histogram counts. **Be careful, also this will consume very much memory. Estimates are available below.**

|	**IOStoreSpecificPeaks**
Set to 1 to store the FFT amplitude spectra evaluated for specific frequency bins detailed by LatticeIontypeCombinations.

|	**IOStoreThreeStrongestPeaks**
Set to 1 to store the FFT signal magnitude for the three strongest bins for every ROI and direction. Can be used to measure the signal-to-noise level but is also primarily used for debugging purposes.

Memory consumption per ROI and option::

    Ndir = NumberOfDirections
    Nbin = NumberOfBins (2^SDMBinExponent)
    Nimg = NumberOfLatticeIontypeCombinations
    HistoCnts = Nimg * Ndir * Nbin * 4 B/ROI
    HistoFFTMagn = Nimg * Ndir * Nbin * 4 B/ROI
    ThreeStrongest = Nimg * Ndir * 6 * 4 B/ROI
    SpecificPeak = Nimg * Ndir * 1 * 4 B/ROI

Assuming typical values::

    Ndir = 40962
    Nbin = 1024
    Nimg = 2
    HistoCnts = 320 MB/ROI
    HistoFFTMagn = 320 MB/ROI
    ThreeStrongest = 1.9 MB/ROI
    SpecificPeak = 0.32 MB/ROI

|	**InputfileElevAzimGrid**
Specifies the name of the H5 file which stores the directions along which to project the ions per ROI.

..  |	**MaxMemoryPerNode**
..  Specifies how much 

|	**GPUsPerComputingNode**
Specifies how many GPUs have been configured for the run, because currently this is not parsed from the operating system or runtime environment. Possible values are 0, in which cases GPUs will not be used and instead CPUs compute everything. Setting n should be used, with n equals 1 or 2 respectively. **This setting must match with the number of MPI process to use per node!** In other words, either no MPI process uses a GPU or every process an own one. In the second case GPUs per computing node are distributed modulo-based. **Be aware that for a computing cluster the GPUs must be made visible!** This is controlled in SLURM for which one should consult the manual of the compute cluster.

|	**GPUWorkload**
Empirical integer which specifies how many transforms should be executed per GPU, specifically how many more than on CPUs. Currently, PARAPROBE araullo employs heterogeneous parallelism. OpenMP threads query the ions within the ROIs. Thereafter, GPUs and possibly CPUs machine off these transforms in parallel. As modern GPUs like the V100 offer floating point performance equivalent to between 50 and 100 CPU cores, we use this WorkloadFactor to give the GPUs more work than the CPUs. In this way we reduce waiting of the GPUs for the CPUs to finish their work per cycle.


.. ROICenterInsideOnly
.. ROIVolumeInsideOnly
.. MaxMemoryPerNode
.. **KaiserAlpha**
.. Positive floating point value.
.. Set to 1 to use Kaiser windowing with Kaiser alpha, this reduces frequency resolution but also dampens side lobes.
