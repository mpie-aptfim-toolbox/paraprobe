Compile **paraprobe-gui**
^^^^^^^^^^^^^^^^^^^^^^^^^

The gui tool is a browser-based tool to assist researchers in creating configuration files for PARAPROBE.
It uses a Python script surplus the PyVIZ, panel and Bokeh Python libraries to generate a collection of 
tabs, one for each tool. Navigate through these tabs to configure the XML files as desired.

The source code to the gui tool does not need to be compiled. Instead, what is necessary only is to execute 
it with a working anaconda/Python installation. The most powerful and useful solution is to use anaconda. 
A good recipe how to install anaconda is the manual from Lisa Tagliaferri ( here_ )

 .. _here: http://www.digitalocean.com/community/tutorials/how-to-install-anaconda-on-ubuntu-18-04-quickstart
 
Similar good manuals on how to install anaconda/Python for Windows and Mac are available online.
So the cooking recipe to get paraprobe-gui working is as follows:

1. Install anaconda/Python.

2. Make sure you have a browser install, I tested with Firefox.

3. Open the anaconda console, **not the Windows command line console**, to install the following libraries, if not already available::

	conda install param
	conda install panel
	conda install bokeh
	Navigate into the folder where you have the paraprobe tools
	bokeh serve --show PARAPROBE_BokehGUI.py

4. Ideally, this will open up your browser and show the tabs of the GUI::

	Navigate through the tabs and configure your run as desired.

5. The XML configuration files are stored in the folder from which the bokeh serve command was executed.

6. Use the files to execute PARAPROBE runs as detailed in the rest of this manual. 
