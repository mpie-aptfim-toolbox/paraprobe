How to compile the tools manually?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 3
   
   compile-gui
   compile-utils
   compile-transcoder
   compile-synthetic
   compile-ranger
   compile-surfacer

   compile-spatstat
   compile-autoreporter
   compile-fourier
   compile-araullo
   compile-indexer

..   compile-distancer