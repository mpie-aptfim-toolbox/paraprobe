Configure **paraprobe-gui**
^^^^^^^^^^^^^^^^^^^^^^^^^^^

PARAPROBE gui is an experimental utility tool to generate XML configuration files graphically using your webbrowser.

1. Check if all steps of the compile-gui manual section have been done.

2. Open an anaconda console, **not the Windows command line console**, to start the gui::

	Navigate into the folder where you have the paraprobe tools
	bokeh serve --show PARAPROBE_BokehGUI.py

3. Ideally, this will open up your browser and show the tabs of the GUI. Navigate through the tabs and configure your run as desired.

4. The XML configuration files are stored in the folder from which the bokeh serve command was executed.

5. Use the files to execute PARAPROBE runs as detailed in the rest of this manual. 

.. figure:: ../images/ScreenshotPARAPROBE_BokehGUI_01.png
   :scale: 50%
   :align: center

.. figure:: ../images/ScreenshotPARAPROBE_BokehPSE_01.png
   :scale: 50%
   :align: center

