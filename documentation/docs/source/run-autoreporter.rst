Run **paraprobe-autoreporter**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PARAPROBE autoreporter is executed from Jupyter_ notebooks or directly within the Spyder_ Integrated Development Environment (IDE).

 .. _Jupyter: http://jupyter.org/
 .. _Spyder: http://www.spyder-ide.org/
