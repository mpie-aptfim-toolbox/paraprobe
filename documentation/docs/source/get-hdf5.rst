Get **HDF5**
^^^^^^^^^^^^

The HDF5 library comes in two principle flavors. As a sequential and a parallel library. I have used both. The sequential library allows that only one process at a time is granted write access to the harddisk. In most cases this is what atom probers have in their labs, so the sequential library should do it also for other reasons detailed in the advanced sections. So, let's focus for now on how to configure HDF5 for sequential I/O. As HDF5 comes shipped with a good manual how to do so, I just stress the important modifications. Using HDF5 v1.10.2 suffices completely for all I/O activities within PARARPROBE tools.

1. Download the version here http://www.hdfgroup.org/downloads/hdf5/ and compile it from source. Check the manual against the following loose cooking recipe::

    sudo mkdir HDF5Local
    sudo cd HDF5Local
    sudo tar –xvf hdf5-1.10.2.tar.bz2
    cd hdf5-1.10.2

2. Check additional compile flags according to::

    http://support.hdfgroup.org/HDF5/release/chgcmkbuild.html

3. The following commands will build the dynamic libraries, test them thereafter, and do not encode the compression::

    cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE:STRING=Release -DBUILD_SHARED_LIBS:BOOL=OFF -DBUILD_TESTING:BOOL=ON -DHDF5_BUILD_TOOLS:BOOL=ON -DHDF5_BUILD_FORTRAN:BOOL=ON ../hdf5-1.10.2
    cmake -build . -config Release
    ctest . -C Release
    cpack -C Release CPackConfig.cmake
    ./HDF5-1.10.2-Linux.sh
    cd HDF5-1.10.2-Linux
    cd HDF_Group
    cd HDF5
    cd 1.10.2

This 1.10.2 folder is the one that has to be mentioned in the cmake settings file in the paraprobe/code folder to proceed with setting up PARAPROBE. I know that the compilation of HDF5 can be tricky. Feel free to ask for help_ !

 .. _help: http://bigmax.iwww.mpg.de/39151/bigmax-software-engineering-consultant
