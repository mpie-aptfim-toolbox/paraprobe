.. figure:: ../images/PARAPROBEFront_03.png
   :scale: 25%
   :align: center
   :target: http://gitlab.mpcdf.mpg.de/mpie-aptfim-toolbox/paraprobe

paraprobe-toolbox
^^^^^^^^^^^^^^^^^
**PARAPROBE** is a collection of **open source tools** for **efficient analyses** of **Atom Probe Tomography** (APT) and **Field Ion Microscopy** (FIM) **data**. The toolbox does not replace but complement existent APT/FIM software tools.

.. such as AMETEK's APSuite/IVAS, 3depict, Scito, or a number of community scripts out there.

PARAPROBE is a **collection of individual tools**, each is specialized for a specific task. Examples of such tasks are the loading of data from commercial software, the creating of synthetic datasets, the computing of dense alpha shapes to define the edge of a dataset, of full volume tessellations, the computing of spatial statistics, performing of clustering methods, and assessing datasets with methods from atom probe crystallography.

PARAPROBE is designed for **workstations** and **computer clusters**. The tools offer **strong scaling performance**, i.e. they **make the most of** multi-core **CPUs** and **GPUs**. 

.. The tools achieve their performance through algorithmic optimizations, multi-level parallelization including vectorization, shared and distributed memory data parallelism, and the utilization of **CPUs** and graphic cards (accelerators) **GPGPUs**.

The toolbox is developed by Markus_ Kühbach, a scientific computing Postdoc with the Max-Planck BiGmax_'s research network at the Max-Planck-Institut für Eisenforschung GmbH (MPIE_) in Düsseldorf.

**Feel free to utilize the tool**. In doing so, **feel free to suggest** me_ any improvements or analysis features that you think would be great to have or could improve PARAPROBE.

 .. _Markus: http://bigmax.iwww.mpg.de/39151/bigmax-software-engineering-consultant
 .. _MPIE: http://www.mpie.de
 .. _me: http://www.mpie.de/person/51206/2656491
 .. _here: http://gitlab.mpcdf.mpg.de/mpie-aptfim-toolbox/paraprobe


1. Getting started
^^^^^^^^^^^^^^^^^^
This documentation should serve as a guide through PARAPROBE. In case of questions, feel free to ask me_. In these sections you will learn to work with PARAPROBE as a user. For advanced users and software developers there is a developer section with more details.

.. toctree::
   :maxdepth: 1

   pros-and-cons
   faq

2. Download, configure, and compile PARAPROBE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
There is a detailed Jupyter notebook tutorial on how to prepare your computer and compile PARAPROBE tools. **I recommend to start with this tutorial.** You can find additional advice in the advanced topics sections.


3. Define and execute your analyses
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
PARAPROBE analyses have three steps: configuration, execution, post-processing. Each tool is instructed by an XML configuration file. These files instructs the tool to do something and are your record of what was planned. There are detailed Jupyter notebook tutorials in the tutorial section which guide you through the currently most important tools. The tutorials exemplify how setting up configuration files gets simpler with Python instead of editing XML files manually. The tutorials instruct how to run the tools. Thereafter, the tutorials exemplify how you can use the autoreporter Python tool for post-processing results obtained with PARAPROBE. You can find additional advice in the advanced topics section.


4. Tutorials
^^^^^^^^^^^^

.. toctree::
   :maxdepth: 3

The tutorials are in the form of Jupyter notebooks in the **paraprobe/tutorial** directory.
Tutorials should be executed from within paraprobe/tutorial::

    PARAPROBE_Tutorial_HowToBuild.ipynb
    PARAPROBE_Tutorial_ImportExperimentalDatasets.ipynb
    PARAPROBE_Tutorial_CreateSyntheticDatasets.ipynb
    PARAPROBE_Tutorial_Surfacer.ipynb
    PARAPROBE_Tutorial_Tessellator.ipynb
    PARAPROBE_Tutorial_Spatstat.ipynb
    PARAPROBE_Tutorial_DBScan.ipynb

..   example-synthetic-polycrystal.ipynb
..   example-ranger
..   example-surfacer
..   example-spatstat


5. Open source, how to cite
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 3

   license
   refs


6. How to contribute
^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 3

   developers

.. contact


7. Funding
^^^^^^^^^^^
Markus Kühbach gratefully acknowledges the support from the Deutsche Forschungsgemeinschaft (DFG_) through project BA 4253/2-1.
Markus Kühbach especially acknowledges the provisioning of computing resources by the Max_ Planck Gesellschaft, and the funding received through BiGmax_, 
the Max-Planck-Research Network on Big-Data-Driven Materials Science.

 .. _DFG: https://www.dfg.de/
 .. _Max: https://www.mpcdf.mpg.de/services/computing/linux/TALOS
 .. _BiGmax: https://bigmax.iwww.mpg.de/


8. Advanced topics
^^^^^^^^^^^^^^^^^^^

This section addresses mainly software developers who wish to compile individual tools manually.

.. toctree::
   :maxdepth: 1
   
   parallelization
   parallelization-table
   thirdpartydep
   dependencies-all
   compile-manually
   compile-all
   configure-details
   execute-details
   bestpracticesomp
   phdf5

