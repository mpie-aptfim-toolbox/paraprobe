Configure **paraprobe-ranger**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PARAPROBE ranger is the tool to inject external ranging information that have been performed using e.g. IVAS or APT community solutions. The tool can handle molecular ions which are composed of at most three disjoint elements. Isotopes and arbitrary charge states are supported. At most 255 different user-defined iontypes can be distinguished. If there is a necessity to change this please feel free to contact me. **PARAPROBE ranger takes an APTH5** file **and** for now an **RRNG range file** as input to translate the mass-to-charge values to integer IDs. The tool is controlled with a single XML settings file. Supplementary XML files are read to train the tools the periodic table of elements. This XML file requires no modifications. The possible settings for paraprobe-ranger are as follows:

|	**InputfilePSE**
Specify name of XML file containing the periodic table of the elements. The file can be renamed but needs to be in the working directory.

|	**InputfileComposition**
|	**InputfileContaminants**
Not support currently.

|	**InputfileReconstruction**
Specify the name of the real or synthetic dataset. Needs file ending *.apth5 the file will be modified in as much as ion labels are added.

|	**InputfileRangingData**
Specifies the external ranging information needs to be *.rrng and RRNG range file format conformant.

|	**RangingMode**
Currently only mode 0 is supported. It applies the external labels.

.. AMUBinningWidth
.. AMUBinningMax
.. SNIPIterations
.. SavitzkyGolayM
.. PeakDetectionWindowHalfwidth
.. PeakDetectionSignalToNoise
.. RangingFWHM
.. RangingFlankBumpTolerance