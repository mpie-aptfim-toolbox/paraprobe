Configure
^^^^^^^^^

.. toctree::
   :maxdepth: 3

   config-utils
   config-gui
   config-transcoder
   config-synthetic
   config-reconstruct
   config-ranger
   config-surfacer
   config-spatstat
   config-tessellator
   config-autoreporter
   config-fourier
   config-araullo
   config-indexer

..   config-distancer

