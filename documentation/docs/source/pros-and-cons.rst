PARAPROBE - Pros
^^^^^^^^^^^^^^^^

| **Open source software / no black box**
|	No usage restrictions.
|	No licences, no costs, regardless whether running on one or thousands of CPUs.
|	Full functional transparency and modifiability.
| **Reduced analysis bias**
|	Enabled by state of the art computational geometry.
| **Scalable performance especial for large datasets**
| 	Thanks to a parallel implementation with rigorous hierarchical spatial partitioning of the data.
| **High-throughput solution for APT data analyzing**
|   Scripting workflows and batch process these with 
|   Python/Jupyter notebooks as the front-end and 
|   PARAPROBE as the back-end it is possible to define 
|   reusable workflows.

PARAPROBE - Cons
^^^^^^^^^^^^^^^^

| **Script-based interface than classical APT software**
|	Visualization is deferred to state of the art third-party tools like Paraview, VisIt, or Blender.
|	Scripts are generated to assist the user in reducing the barrier to load the data into these third-party tools.

.. |	A browser-based GUI is in development to assist the user in configuring PARAPROBE runs.

| **Complexer configuration**
|	This is expected because compared to sequential Python or Matlab analysis scripts 
|	more complexity layers have to be managed.
