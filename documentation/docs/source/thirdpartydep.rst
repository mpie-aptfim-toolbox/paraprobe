Which third-party dependencies does each tool have?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

 .. csv-table::  Third-party dependencies
     :header: "paraprobe-", "C/C++", "Python", "HDF5", "CGAL", "Voro++", "TetGen"
	 
	 "parmsetup", " ", "x", " ", " ", " ", " "
	 "utils", "x", " ", "x", " ", " ", " "
	 "transcoder", "x", " ", "x", " ", " ", " "
	 "synthetic", "x", " ", "x", " ", "x", " "
	 "ranger", "x", " ", "x", " ", " ", " "
	 "surfacer", "x", " ", "x", "x", " "
	 "spatstat", "x", " ", "x", " ", " ", " "
	 "fourier", "x", " ", "x", " ", " ", " "
	 "araullo", "x", " ", "x", " ", " ", " "
	 "indexer", "x", " ", "x", " ", " ", " "
	 "autoreporter", " ", "x", "x", " ", " ", " "

..  "intersector", "x", " ", "x", " ", " ", " "
..  "distancer", "x", " ", "x", " ", " ", " "
..  "tessellator", "x", " ", "x", " ", "x", " "
