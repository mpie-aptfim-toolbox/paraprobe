PARAPROBE and related references
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**The method** and its key implementation::
 | M. Kühbach et al.
 | On Strong Scaling Open Source Tools for Mining Atom Probe Tomography Data
 | Microscopy and Microanalysis
 | Vol 25, Supplement S2, 2019
 | https://doi.org/10.1017/S1431927619002228

 | M. Kühbach and P. Bajaj and M. H. Celik and E. A. Jägle and B. Gault
 | On Strong Scaling and Open Source Tools for Analyzing Atom Probe Tomography Data
 | 2020
 | https://arxiv.org/abs/2004.05188
 
 | M. Kühbach et al.
 | On Open and Strong Scaling Tools for Atom Probe Crystallography: 
 | High-Throughput Methods for Indexing Crystal Structure and Orientation
 | in preparation for Journal of Applied Crystallography

**Solid atom probe tomography method background**
 | B. Gault and M. P. Moody and J. M. Cairney and S. P. Ringer
 | Atom Probe Microscopy
 | 1st edition, 2012, Springer New York
 | https://dx.doi.org/10.1007/978-1-4614-3436-8

 | W. Lefebvre and F. Vurpillot and X. Sauvage
 | Atom Probe Tomography: Put Theory Into Practice
 | 1st edition, 2016, Academic Press

 | M. K. Miller and R. G. Forbes
 | Atom-Probe Tomography The Local Electrode Atom Probe
 | 1st edition, 2014, Springer
 | https://dx.doi.org/10.1007/978-1-4899-7430-3


Third-party contributions
^^^^^^^^^^^^^^^^^^^^^^^^^
**Thread-parallelized DBSCAN --- HPDBSCAN**
 | M. Götz and C. Bodenstein and M. Riedel
 | HPDBSCAN: highly parallel DBSCAN
 | MLHPC15 Proceedings of the Workshop on Machine Learning in High-Performance Computing Environments
 | 2015
 | https://dx.doi.org/10.1145/2834892.2834894
 
**DBScan sequential and threaded**
 | M. Ester and H.-P. Kriegel and J. Sander and X. Xu
 | A Density-Based Algorith for Discovering Clusters in Large Spatial Databases with Noise
 | Proceedings of the 2nd International Conference on 
 | Knowledge Discovery and Data Mining (KDD-96), 1996
 | https://dx.doi.org/10.5555/3001460.3001507

**Maximum separation method**
 | J. M. Hyde and C. A. English
 | An analysis of the structure of irradiation induced Cu-enriched clusters in low and high Nickel welds
 | Proceedings of the MRS Fall Meeting 2000: Symposium R - Microstructural Processes in Irradiated Materials
 | 2000, Vol 650 R6.6, 6-12
 | https://dx.doi.org/10.1556/proc-650-r6.6

 | L. T. Stephenson and M. P. Moody and P. V. Liddicoat and S. P. Ringer
 | New techniques for the analysis of fine-scaled clustering phenomena within Atom Probe Tomography (APT) data
 | Microscopy & Microanalysis, 2007, Vol 13, 448-463
 | https://dx.doi.org/10.1017/s1431927607070900

 | E. A. Jägle and P.-P. Choi and D. Raabe
 | The Maximum Separation Cluster Analysis Algorithm for Atom-Probe Tomography: 
 | Parameter Determination and Accuracy
 | Microscopy & Microanalysis, 2014, Vol 20, 1662-1671
 | https://dx.doi.org/10.1017/S1431927614013294

**CGAL, the Computational Geometry Algorithms Library**
 | T. K. F. Da and S. Loriot and M. Yvinec
 | CGAL User and Reference Manual: 3D Alpha Shapes
 | 2017
 | https://doc.cgal.org/latest/Alpha_shapes_3/index.html#Chapter_3D_Alpha_Shapes

 | S. Hert and S. Schirra
 | CGAL User and Reference Manual: 3D Convex Hulls
 | 2017
 | https://doc.cgal.org/latest/Convex_hull_3/index.html#Chapter_3D_Convex_Hulls

**Alpha Shapes**
 | H. Edelsbrunner and E. P. Mücke
 | Three-Dimensional Alpha Shapes
 | ACM Transactions on Graphics
 | 1994, 13, 1, 43-72
 | https://dx.doi.org/10.1145/174462.156635

**Voro**
 | C. H. Rycroft
 | VORO++: A three-dimensional Voronoi cell library in C++
 | Chaos 
 | 2009, 19, 041111
 | https://dx.doi.org/10.1063/1.3215722
 | http://math.lbl.gov/voro++/

**Mining crystallographic signal** in reconstruction space
 | V. J. Araullo-Peters, A. Breen, A. V. Ceguerra, B. Gault, S. P. Ringer, J. M. Cairney
 | A new systematic framework for crystallographic analysis of atom probe data
 | Ultramicroscopy
 | 2015, 154, 7-14
 | https://dx.doi.org/10.1016/j.ultramic.2015.02.009

**AABBTree**
 | Lohedges
 | https://github.com/lohedges/aabbcc


Computational geometry-focused APT method development by other people
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
**Advanced Computational Geometry for APT Processing**
 | P. Felfer and A. Ceguerra and S. P. Ringer and J. M. Cairney
 | Applying computational geometry techniques for advanced feature analysis in atom probe data
 | Ultramicroscopy
 | 2013, 132, 100-106
 | https://dx.doi.org/10.1016/j.ultramic.2013.03.004

 | P. Felfer and J. Cairney
 | A computational geometry framework for the optimisation of atom probe reconstructions
 | Ultramicroscopy
 | 2016, 169, 62-68
 | https://dx.doi.org/10.1016/j.ultramic.2016.07.008

**Robust predicates**
 | Shewchuk, J. R.
 | *Adaptive Precision Floating-Point Arithmetic and Fast Robust Geometric Predicates*
 | Computational Geometry, 1997, 18, p305
 | https://dx.doi.org/10.1007/PL00009321


Recommended HPC background literature
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| J. L. Hennessy and D. A. Patterson
| Computer Architectures: A Quantitative Approach
| 5th edition, 2012, Morgan Kaufmann, Amsterdam

| T. Rauber and G. Rünger
| Parallel Programming for Multicore and Cluster Systems
| 2nd edition, 2013, Springer Heidelberg
| http://dx.doi.org/10.1007/978-3-642-37801-0

| J. Reinders and J. Jeffers
| High Performance Parallelism Pearls Volume One:
| Multicore and Many-Core Programming Approaches
| 1st edition, 2014, Morgan Kaufmann

| J. Jeffers and J. Reinders
| High Performance Parallelism Pearls Volume Two:
| Multicore and Many-Core Programming Approaches
| 1st edition, 2015, Morgan Kaufmann

| Prabhat and Q. Koziol
| High Performance Parallel I/O
| 1st edition, 2014, Chapman & Hall/CRC