Compile **paraprobe-indexer**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Firstly**, assure that all relevant third-party dependencies have been properly installed and configured. Especial to mention here for PARAPROBE indexer is the PGI compiler and the CUDA API. See the corresponding details in the compilation hints for PARARPROBE fourier.

**Secondly**, it is essential to set the environment variables such that the PGI compiler can find CUDA::

    export CUDACXX=/usr/local/cuda-10.1/bin/nvcc

**Thirdly**, one needs to understand that also PARAPROBE indexer makes use of the PARAPROBE utils object files. One is better off with recompiling these object files with the PGI compiler before including them while compiling PARAPROBE indexer like it applies for the PARAPROBE fourier and araullo tool. See the corresponding details in :refs:`compile-utils`.

**Fourthly**, the tools CMakeLists.txt configuration file should be checked to assure that HDF5 can be found.

**Finally**, the compilation of the PARAPROBE indexer is straightforward::

   export STDOUT_CMKE_TXT = PARAPROBE.Indexer.CMakeSettings.STDOUT.txt
   export STDERR_CMKE_TXT = PARAPROBE.Indexer.CMakeSettings.STDERR.txt
   export STDOUT_MAKE_TXT = PARAPROBE.Indexer.MakeSettings.STDOUT.txt
   export STDERR_MAKE_TXT = PARAPROBE.Indexer.MakeSettings.STDERR.txt

Use the PGI compiler::

    cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_CUDACXX=nvcc -DCMAKE_C_COMPILER=pgcc -DCMAKE_CXX_COMPILER=pgc++ .. 1>${STDOUT_CMKE_TXT} 2>${STDERR_CMKE_TXT}
    make 1>${STDOUT_MAKE_TXT} 2>${STDERR_MAKE_TXT}

It should also be possible to compile the code with the GCC compiler. The standard Linux toolchain GNU compiler, though, will not generate OpenACC code. 
For this a customized version of GCC NVTPX_ is required until the compiler developers have implemented these features into their compilers.

 .. _NVTPX: http://www.heise.de/developer/artikel/Accelerator-Offloading-mit-GCC-3317330.html?seite=all
