Configure **paraprobe-tessellator**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PARAPROBE tessellator is the tool to build Voronoi tessellations of the dataset. **PARAPROBE ranger takes an APTH5** file with the reconstructed
positions and an *.H5 from the paraprobe-surfacer tool for the distances
to detect and eventually remove Voronoi cells in close contact with the 
dataset boundary. Supplementary XML files are read to train the tools the periodic table of elements. This XML file requires no modifications. 
The possible settings for paraprobe-tessellator are as follows:

|	**InputfilePeriodicTableOfElements**
Specify name of XML file containing the periodic table of the elements. The file can be renamed but needs to be in the working directory.

|	**InputfileReconstruction**
Specifies an *.apth5 file with reconstructed x,y,z and ion type labels.

|	**InputfileDistances**
Specifies an *.H5 file from paraprobe-surfacer with one distance value for
each ion referred to in InputfileReconstruction.
Distances to be evaluated against CellErosionDistance.

|	**SpatialSplittingModel**
Currently only 0 implemented, split point cloud to thread aiming for
quasi equal number.

|	**CellErosionDistance**
Use as the threshold value based on which to decide if an ion is closer to the edge of the point cloud or not, in nanometer.

|	**IOCellVolume**
|	**IOCellNeighbors**
|	**IOCellShape**
|	**IOCellProfiling**
Deactivate (0) or activate (1) which output to generate.
CellShapes and neighbors currently not implemented.
