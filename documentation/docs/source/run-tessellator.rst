Run **paraprobe-tessellator**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Firstly**, assure that all input file(s) exist in the local directory (APTH5 file with reconstruction, H5 file with distances, and periodic table of elements XML file). **Secondly**, assure that there is a properly configured XML paraprobe-tesellator XML settings file.
The same comments as detailed in paraprobe-synthetic apply.

**Finally**, paraprobe-tessellator is executed::

    export SIMID=1
    export XML_TXT=PARAPROBE.Tessellator.xml
    export STDOUT_RUN_TXT = PARAPROBE.Tessellator.SimID.${SIMID}.STDOUT.txt
    export STDERR_RUN_TXT = PARAPROBE.Tessellator.SimID.${SIMID}.STDERR.txt

    mpiexec -n 1 ./paraprobe_tessellator ${SIMID} ${XML_TXT} 1>${STDOUT_RUN_TXT} 2>${STDERR_RUN_TXT}

PARAPROBE tessellator is an MPI program. Check the supplementary material
of our paper on the PARAPROBE tools in the reference section to assure
that you use enough MPI processes such that when planning for computing
complete tessellations of multi-hundred million ion specimens that there
is sufficient memory used.
