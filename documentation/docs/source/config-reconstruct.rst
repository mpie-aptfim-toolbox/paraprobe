Configure **paraprobe-reconstruct**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PARAPROBE reconstruct is the tool to build reconstructions from detector hit x, y and voltage curve sequence data.
**PARAPROBE ranger takes an APTH5** file as input. The tool is controlled with a single XML settings file. 
Supplementary XML files are read to train the tools the periodic table of elements. This XML file requires no modifications. 
The possible settings for paraprobe-reconstruct are as follows:

|	**InputfilePeriodicTableOfElements**
Specify name of XML file containing the periodic table of the elements. The file can be renamed but needs to be in the working directory.

|	**InputfileTranscoder**
Specifies the *.apth5 file with detector hit x, y and voltage pulse (standing and DC) detector event sequence data to build the reconstruction.

|	**WhichSpace**
In which reference space to operate? 0 detector or 1 reconstruction space.

|	**ReconstructionAlgorithm**
Currently only the global reconstruction approach by Gault 2011 Ultramicroscopy supported, see https://doi.org/10.1016/j.ultramic.2010.11.016

|	**FlightLengthMin**
|	**FlightLengthIncr**
|	**FlightLengthMax**
Flight length in cm instrument dependent.

|	**AtomicDensityMin**
|	**AtomicDensityIncr**
|	**AtomicDensityMax**
Number of atoms per nanometer to the power 3.

|	**EvaporationFieldMin**
|	**EvaporationFieldIncr**
|	**EvaporationFieldMax**

|	**DetectionEfficiencyMin**
|	**DetectionEfficiencyIncr**
|	**DetectionEfficiencyMax**
Detector efficiency on (0,1).

|	**FieldFactorMin**
|	**FieldFactorIncr**
|	**FieldFactorMax**
Dimensionless field factor kf.

|	**ImageCompressFactorMin**
|	**ImageCompressFactorIncr**
|	**ImageCompressFactorMax**
Dimensionless image compression factor.

|	**IonsPerHitMapMin**
|	**IonsPerHitMapIncr**
|	**IonsPerHitMapMax**
Number of ions to consider when building a detector hit map.

|	**DetectorGridBinWidthX**
|	**DetectorGridBinWidthY**
Binning of the detector hit map in cm.
