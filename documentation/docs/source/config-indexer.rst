Configure **paraprobe-indexer**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PARAPROBE indexer is a tool to index araullo results using template matching. The procedures are described in the a recent paper. Consult the reference section for details. The settings are as follows:

| **InputfileAraullo**
Specifies the results of the araullo tool run. Expects a HDF5 file with ending *.h5

| **InputfileRefImages**
| Specifies flexibly against which references the tool should index.
| Each line is one reference template with the following format::

      <entry refname="PARAPROBE.Araullo.Results.SimID.210001.h5" imagename="/AraulloPeters/Results/SpecificPeak/PH0/0" usetoindex="PH0" refori="8.0;8.0;8.0" targets="Al"/>

This example will specify the following. Index all ROIs against a synthetic specimen with an araullo run whose results are stored in *210001.h5. In this file use specifically the reference spherical images in the dataset imagename, i.e. *SpecificPeak/PH0/0. The synthetic specimen was oriented 8.0;8.0;8.0 and evaluated Al atoms.

| **InputfileTestOris**
Specifies a HDF5 file with ending *.h5 which contains the quaternions of the test orientations.

| **AnalysisMode**
Set to 3 to instruct indexing. All other settings are undocumented developer options and tests.

| **IndexSoManyHighestPeaks**
Specifies how many of the strongest peak per image are evaluated against the rotated reference images.

| **IOUpToKthClosest**
Instructs the reporting of the kth best solutions.

| **IOSolutionQuality**
Report the image intensity differences.

| **IODisoriMatrices**
Should stay 0 for all users. The option allows to compare the disorientation of a solution to a ground truth, if such is known for a single-crystalline specimen.

| **IOSymmReduction**
Should stay 0.

| **IOSymmRedDisoriAngle**
Should stay 0.0.

| **GPUsPerNode**
| **GPUWorkload**
Unused.
 
| **CachePerIndexingEpoch**
Specifies in gigabyte how much memory each MPI processes uses to load araullo results and index them before loading the next chunk. This is necessary to be able to handle results that dont fit in memory. In the future, the option will likely be replaced.

