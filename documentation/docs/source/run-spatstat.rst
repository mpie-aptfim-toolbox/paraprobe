Run **paraprobe-spatstat**
^^^^^^^^^^^^^^^^^^^^^^^^^^

**Firstly**, assure that all input file(s) exist in the local directory (HDF5 file). **Secondly**, assure that there is a properly configured XML paraprobe-spatstat settings file. **Thirdly**, configure the shell environment to allow for multi-threading::

    export OMP_NUM_THREADS=20
    export OMP_NESTED=true
    ulimit -s unlimited

Otherwise similar comments as for paraprobe-synthetic apply.

**Finally**, paraprobe-spatstat is executed::

    export MYRANKS_EMPLOYED=2
    export SIMID=1
    export XML_TXT=PARAPROBE.Spatstat.xml
    export STDOUT_RUN_TXT = PARAPROBE.Spatstat.SimID.${SIMID}.STDOUT.txt
    export STDERR_RUN_TXT = PARAPROBE.Spatstat.SimID.${SIMID}.STDERR.txt

    mpiexec -n ${MYRANKS_EMPLOYED} ./paraprobe_spatstat ${SIMID} ${XML_TXT} 1>${STDOUT_RUN_TXT} 2>${STDERR_RUN_TXT}

PARAPROBE spatstat is an MPI program. The comments as for running paraprobe-surfacer apply.