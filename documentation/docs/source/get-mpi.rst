Get **MPI**
^^^^^^^^^^^

The Message Passing Interface API has been implemented in different software packages. One for example is by the Intel Corporation (IMPI), two others are (MPICH), the reference open source implementation and OpenMPI (ompi). PARAPROBE demands support for at least MPI_THREAD_FUNNELED as the threading support level. A requirement which to the best of my knowledge all three implementations by now support.

I will now exemplify the installation of MPICH on a clean Ubuntu 18.04 LTS workstation.

1. Check if mpich has already been installed::

    mpiexec -version

2. If nothing shows up, one needs to install it::

    sudo apt-get update
    sudo apt-get mpich
    mpiexec –version

