Configure **paraprobe-fourier**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PARAPROBE fourier is the tool to perform a direct Fourier transform of local point clouds within a grid or collection of randomly chosen regions of interest in an atom probe reconstruction. The key aim is to characterize the amount of long-range periodic structure within the ion position point cloud. The tool is currently in development. So far it allows to define ROIs and compute for these the reflector intensities in 3D reciprocal space. For an ideal infinite crystal lattice one would expect a 3D grid of clearly separated intensity peaks in reciprocal (Fourier) space. This grid encodes the crystal structure and orientation. For real atom probe data, ions are missing and finite spatial resolution causes a smearing of the peaks. In addition, the ROIs defined in nanocrystalline APT specimens may reach into different crystals causing additional peaks even for a perfect lattice. Thus, the second-, third-, and higher-order peaks in Fourier space, after normalizing their intensities by the highest peak per ROI, can serve as a first measure for the amount of crystallographic information within the point cloud locally. This, or the entire reciprocal space grid intensities respectively, is what PARAPROBE fourier currently exports. The tool takes an APTH5 file with the ion positions and range labels. Furthermore, a supplementary XML periodic table of elements file, and a single XML settings file is expected. The results are stored in a separate HDF5 file. The possible settings for this paraprobe-fourier settings file are as follows:

| **VolumeSamplingMethod**
| Specifies how the ROI should be chosen.
| Set 0 for cuboidal ROI grid.
| Set 1 for placing a single ROI at a user-defined location specified by SamplingPosition.
| Set 2 for placing a single ROI in the center of the specimen bounding box.
| Set 3 for placing a collection of FixedNumberOfROIs spatially uncorrelatedly inside the volume.

|	**InputfilePSE**
Specifies like in paraprobe-ranger the periodic table of elements.

|	**InputfileReconstruction**
Specifies the ion positions. Like for the other tools expects a file with the ending *.apth5

|	**InputfileHullAndDistances**
Specifies precomputed ion to triangle hull distances to avoid bias for ROIs which lay too close to the dataset boundary. Expects a HDF5 file of surfacer results with the ending *.h5

|	**SamplingGridPointType**
Set to 1 when a specimen axis-centered cuboidal ROI grid should be used. This is comparable to instructing a 3D EBSD mapping of the specimen volume.

|	**ROIRadiusMax**
Specifies in nanometer the maximum radius of the spherical regions of interest to use.

|	**SamplingGridBinWidthX**
|	**SamplingGridBinWidthY**
|	**SamplingGridBinWidthZ**
Specifies in nanometer the spacing of the cuboidal ROI grid.

|	**SamplingPosition**
For VolumeSampling 1 position x;y;z formatting.

|	**FixedNumberOfROIs**
For VolumeSampling 3 how many ROIs to place.

|	**RemoveROIsProtrudingOutside**
Use the information from the surfacer tool to avoid placing ROIs which are not fully contained.

|	**HKLGridBoundsHMin**
|	**HKLGridBoundsHMMax**
|	**HKLGridBoundsHVoxelResolution**
Defines which reciprocal space positions to probe, cubic grid, linear sampling of k points.

|	**IOStoreFullGridPeaks**
Store Fourier amplitude for each ROI and point of the k-point mesh. **Be careful can become very memory costly!**, stores 32-bit floats.

|	**IOStoreSignificantPeaks**
Uses SignificantPeakIntensityThreshold to decide individually per ROI and by amplitude which k-points are assumed
significant. Only these are stored, typically cuts an order of magnitude stores floating point.

|	**IODBScanOnSignificantPeaks**
Executes a DBScan on the significant peaks, for every ROI to distill the amplitude information further and link
ADT/TEM methods. Development feature.

|	**DBScanSearchRadius**
In voxel, real value.

|	**DBScanMinPksForReflector**
Integer.

|	**GPUsPerComputingNode**
Specifies how many GPUs have been configured for the run, because currently this is not parsed from the operating system or runtime environment. Possible values are 0, in which cases GPUs will not be used and instead the CPUs compute everything. Setting n should be used, with n equals 1 or 2 respectively. **This setting needs to match with the number of MPI process used per node! In other words, either no MPI process uses a GPU or every process an own one. In the second case, GPUs per computing node are distributed modulo-based. **Be aware that for a computing cluster the GPUs must be made visible!** This is controlled in SLURM for which one should consult the manual of the compute cluster.

|	**GPUWorkload**
Empirical integer which specifies how many transforms should be executed per GPU, specifically how many more than on CPUs. Currently, PARAPROBE fourier employs heterogeneous parallelism. OpenMP threads query the ions within the ROIs. Thereafter, GPUs and possibly CPUs machine off these transforms in parallel. As modern GPUs like the V100 offer floating point performance equivalent to between 50 and 100 CPU cores, we use this WorkloadFactor to give the GPUs more work than the CPUs. In this way we reduce waiting of the GPUs for the CPUs to finish their work per cycle.