Run/Execute tools
^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 3

   run-utils
   run-gui
   run-transcoder
   run-synthetic
   run-reconstruct
   run-ranger
   run-surfacer

   run-spatstat
   run-tessellator
   run-autoreporter
   run-fourier
   run-araullo
   run-indexer


..   setenvironment
..   run-distancer