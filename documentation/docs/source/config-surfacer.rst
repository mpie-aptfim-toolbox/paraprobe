Configure **paraprobe-surfacer**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PARAPROBE surfacer is the tool which computes an alpha-shape triangle hull to the entire dataset. The CGAL library is used to compute the alpha shape and export the triangles as objects. They can be visualized using Paraview and the generated XDMF/HDF5 file combination. PARAPROBE surfacer uses a filtering algorithm which thins the point cloud. Thereby, most ions in the interior are not at all passed to CGAL. This enables surfacer to compute alpha-shapes even for datasets as large as two billion ions without any need for ad hoc sub-sampling or random thinning of the data like it was commonly done in classical APT studies. The resulting triangle hulls are not necessarily water-tight. Feel free to contact me in case there is interest in collaborating to patch these holes. The alpha-shape triangle hull is used to evaluate the distance of each ion to the closest triangle using an exact analytical procedure.

PARAPROBE surfacer takes an APTH5 file with the ion positions and returns a HDF5 file with the triangles, distance values, and additional details. The tool is controlled with a single XML settings file. The possible settings for paraprobe-surfacer are as follows:


|	**SurfacingMode**
Set to 1 to use CGAL and compute an alpha shape to the dataset.

|	**InputfileReconstruction**
The ion positions as an APTH5 file. Needs to have file ending *.apth5

|	**AlphaShapeAlphaValue**
Choose 0 to pick the alpha shape with the smallest alpha value to get a solid through the data points.  
Choose 1 to pick the alpha shape which CGAL considers to be the optimal value. Consult the CGAL library for further details.

|	**AdvIonPruneBinWidthMin**
|	**AdvIonPruneBinWidthIncr**
|	**AdvIonPruneBinWidthMax**
Specify in nanometer the width of the rectangular bins chosen during the initial filtering stage. Values of 0.5 nm are suited for most datasets. If value is larger unnecessarily many ions are passed to CGAL, thereby slowing down the alpha shape computation. Values smaller may result in gaps in the dataset or percolation of triangles through the point cloud such that a bi-layer hull is generated.

|	**DistancingMode**
Set to 0 if no distancing is desired. Typically results in bias for ions at the dataset boundary and is not recommended when reporting spatial statistics but is very fast. Set to 1 to compute for every ion analytically a distance value. Can be time consuming. However, this is where the multi-level parallelization of PARAPROBE assists. Set to 2 if spatial statistics and all other subsequent analyses should take into account only regions of interest with a maximal radius of DistancingRadiusMax from the boundary. In this case, PARAPROBE surfacer sets DistancingRadiusMax as the default distance for all ions which are farther away from the boundary than DistanceRadiusMax. The tool employs a pre-processing algorithm which evaluates whether an ion has triangle neighbors within DistanceRadiusMax or closer, if not it skips the ion. This makes 2 a faster option than 1.

|	**DistancingRadiusMax**
Only significant when DistancingMode is set to 2. Defines the threshold distance up to which ions are exactly distanced. For all other ions this threshold is returned.

|	**AdvDistanceBinWidthMin**
|	**AdvDistanceBinWidthIncr**
|	**AdvDistanceBinWidthMax**
For large datasets and analytical distancing in mode 1 one has to eventually test a large number of triangles for each ion as one does not know a priori which triangle is closest. Therefore, mode 1 uses a pre-processing strategy. First, the specimen is gridded using a coarse voxel grid. The size of which is controlled with the AdvDistanceBinWidth settings. Now for all these voxels we compute the center distance to the triangle hull exactly. Having now a coarse estimate of the size distribution this information is used to reduce triangle querying costs.

|	**RequeryingThreshold**
This is a constant that should be larger than 0 and smaller than 1, I recommend 0.7.
The value controls details in the fine distancing stage where it assists to reduce the number of triangle tests.
