What is the key idea behind PARAPROBE?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
**PARAPROBE** is a **collection of C/C++/CUDA source code** tools and **Python** as well as **Matlab scripts**. The source code needs to be compiled before it is ready to rock'n'roll. As we should not re-implement the wheel, PARAPROBE uses third-party software which has to be properly installed on the workstation or cluster before the tools can be compiled and used to their full potential. There is a Jupyter notebook in the tutorials to help users with this process.


Which operating system is supported?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
PARAPROBE targets **workstations** and **computing clusters**, i.e. **Linux-based** operation systems. It was extensively tested on
Ubuntu 18.04 LTS and SUSE Linux Enterprise Server 15 SP1. The compilation of most tools on a Windows system should in principle be possible technically. 
It has so far, though, not been tested. We are evaluating the possibility to build conda packages.


Which datasets are processable?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Those from real APT/FIM experiments and synthetic datasets. Currently datasets with individually at most 2^31 ions are supported. As of 2020, such large specimens have, to the best of my knowledge, not been measured experimentally. The tool has been tested with synthetic datasets containing up to two billion ions. Please contact me_ if you have larger datasets, I am eager to modify the code.

 .. _me: https://www.mpie.de/person/51206/2656491

 
Which type of computer do I need?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
**Memory** --- data mining APT datasets is processing of three-dimensional point data with annotations. Therefore, hardware minimum requirements depend primarily and necessarily on the total number of ions the user wishes to process. Internally, each ion occupies 16B memory. In addition, buffers are created which decouple the computations during multi-threading. This requires additional memory. Quantitative results are detailed in the initial PARAPROBE paper which is referred to in the reference section of this manual.

**CPU** --- virtually all modern workstation and cluster computing processors are capable of executing PARAPROBE. Their cost-benefit-ratio and speed of doing so may differ substantially though. Consequently, claiming minimum hardware requirements is pointless.

**GPU** --- support is offered for the atom probe crystallography tools, i.e. paraprobe-fourier, paraprobe-araullo, and paraprobe-indexer. The tools have a multithreaded version if there is no GPU available. The code has been tested on single GPU workstations with NVIDIA RTX2080 Ti as well as on cluster computer nodes with two NVIDIA V100 GPUs each, scaling for up to 160 GPUs.

Which additional software do I need?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
PARAPROBE depends on third-party open source software and open source Linux tools. These dependencies are detailed in the tables below. **PARAPROBE is not a monolithic software**. Instead, it comes as a collection of tools. Frequently used functionality and algorithms
are implemented in a set of utility object code, called utils, whose compilation is always required. The other PARAPROBE tools include these functions upon compilation as required.


What else is useful to know?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
**It is not required to compile all code** to be able to use PARAPROBE. 
This is illustrated in the following tables which specify the dependencies.

**Not every tool uses all levels of parallelism**. This is illustrated in the second table. For two reasons: First, it may not necessarily bring a substantial performance advantage and second, my time resources are finite. Therefore, I eventually have to make compromises on which tools and core computational parts to parallelize and focus first. If you are interested in spinning the wheel and contribute to extend PARAPROBE, feel very much invited to do so. I am happy to contribute within joint projects. Feel free to contact_ me. 


Third-party tools
^^^^^^^^^^^^^^^^^
The **Hierarchical Data Format (HDF5)** library is **required** for every tool. HDF5_ is an API and data format specification for binary container files. While this at first glance might appear more complex and difficult to use than the traditional ASCII-based text files frequently, there are several advantages to use HDF5 over text files or especially the closed containers file of many commercial software companies.

* The output of all analysis runs is bundled. This increases processing speed.
* HDF5 offers capabilities to store the metadata. 
* The format is self-descriptive with respect to which format and size the data arrays within the file have.
* Also HDF5 is transparent with respect to which data elements the file contains.
* HDF5 is optimized for performance on datasets typically much larger than used for even the largest APT datasets.
* HDF5 offers dataset slicing, data compression, and parallel I/O functionality, i.e. reduces re-implementing the wheel.
* Accessing the content of an HDF5 dataset is straightforward as reading in an Excel file as below examples show.
* HDF5 files are binary files but their content and even individual values can be viewed using the HDFView.

Using Matlab and its high-level HDF5 library interface, a simple two-dimensional array can be imported using a single line of code::

    MATRIX2D = h5read( 'PARAPROBE.Tool.Results.h5', 'Datasetname/MatrixOfInterest' )

Similarly simple it is to read the same matrix using Python::

    import h5py
    hf = h5py.File('PARAPROBE.Tool.Results.h5', 'r')
    MATRIX2D = np.array(list(hf['Datasetname/MatrixOfInterest'])).flatten()

See? Honestly, this is even simpler to use as writing the actual implementation in C/C++ in PARAPROBE... Anyway, let's proceed with third-party tools.

**Python** is a programming language that can be accessed through writing scripts. I recommend you to use anaconda with its Spyder integrated development environment (IDE) and Jupyter notebooks. It is useful for almost every researcher. For PARAPROBE tools, Python_ is used as a glue language to implement wizards for setting up XML configuration files (paraprobe-parmsetup tool) and wizards for post-processing the results of PARAPROBE runs (paraprobe-autoreporter tool).

**CGAL**, the Computational Geometry Algorithms Library (CGAL_) is used in the paraprobe-surfacer tool to perform key computational geometry tasks numerically robustly.

**Voro** (++), is a third-party C++ software (library_) used in the paraprobe-synthetic and paraprobe-tessellator tool for computing Voronoi or radical plane tessellations. It serves as an easier accessible alternative to the Quickhull library that has been traditionally used in some APT analyses. Its key advantage is that it allows for incremental building of the tessellations.

**TetGen** and **Overlap**, are third-party computational geometry libraries used in paraprobe-intersector, a tool for software developers, which enables tetrahedralizing Voronoi cells and compute intersection volumina between different geometrical primitives (tetraeder, spheres) in 3D robustly.

 .. _Python: http://anaconda.org/
 .. _HDF5: http://www.hdfgroup.org
 .. _CGAL: http://www.cgal.org
 .. _library: http://math.lbl.gov/voro++/download/


Compilers required
^^^^^^^^^^^^^^^^^^
Most cross-compatible is to use the open GNU compiler. Some combinations of tools and compilers are currently not possible. This is also detailed in the tables below. If you are interested in atom probe crystallography, i.e. the araullo, indexer, and fourier tools compile for now either with GNU (only CPU support currently) or the PGI compiler. 

**y** means yes, this compiler can be used. **?** means this compiler has not been tested on PARAPROBE. **n** means it is expected that this compiler will not work.

 .. csv-table:: How to compile or bring to the tool to life.
      :header: "paraprobe-", "Intel", "GNU", "PGI", "Python"
	 
	 "utils", "y", "y", "y", " "
	 "gui", " ", " ", " ", "y"
	 "parmsetup", " ", " ", " ", "y"
	 "transcoder", "y", "y", "y", " "
	 "synthetic", "y", "y", "y", " "
	 "reconstruct", "y", "y", "?", " "
	 "ranger", "y", "y", "y", " "
	 "surfacer", "y", "y", "n", " "
	 "spatstat", "y", "y", "?", " "
	 "tessellator", "y", "y", "?", " "
	 "autoreporter", " ", " ", " ", "y"
	 "araullo", "?", "y", "y", " "
	 "fourier", "?", "y", "y", " "	 
	 "indexer", "?", "y", "y", " "

..  "distancer", "y", "?", "?", " "


The compilers above mean the Intel_, the GNU, or the Portland Group (PGI_) compiler, respectively.  
  .. _Intel: http://software.intel.com/en-us/qualify-for-free-software
  .. _GNU: http://de.wikipedia.org/wiki/GNU_Compiler_Collection
  .. _PGI: http://www.pgroup.com/products/community.htm

So, which tools do I need for what?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. **utils** is always required.
2. **parmsetup** is not required but helps to set up XML configuration and SH shell scripts.
3. **transcode** brings from commercial software into PARAPROBE.
4. **synthetic** builds you rigorously defined test configurations, synthetic datasets, single- and polycrystalline specimens, with and without precipitates, with support for arbitrary crystal structures.
5. **surfacer** quantifies you the edge of the dataset.
6. **tessellator** is the tool to build Voronoi tessellations of the dataset.
7. **spatstat** is the tool for all spatial statistics questions.
8. **dbscan** is a tool for DBScan.
9. **autoreporter** is not required but helps to post-process PARAPROBE results into figures.
10. **araullo**, **indexer**, and **fourier** are your friends for cutting-edge atom probe crystallography, i.e. quantification how much structural information is retained and tools for eventually backing out orientation and phase locally.


..  5. **reconstruct** allows you to build reconstructions from detector hit and voltage curve data.


How to get my dataset into PARAPROBE?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Use commercial software and export a reconstructed dataset to POS, EPOS, or APT (new format with APSuite6/IVAS4). 
2. Use your favorite tool for ranging and export mapping of mass-to-charge-state-ratio interval to atom type via either RNG or RRNG.
3. Use the transcoder tool to go from POS/EPOS/APT to APTH5.
4. Apply PARAPROBE tools as desired on real or synthetic data.

There is a Jupyter notebook tutorial to this question in the tutorial section.

What to do with the PARAPROBE results?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

5. Extract results for further analyses from HDF5 using Python or Matlab. The autoreporter tool has many example scripts and class definitions to help you automating this. There are multiple tutorials in the tutorials section.
6. Visualize results using Paraview XDMF/HDF5. 

.. 7. Transcoding back from HDF5 to POS and EPOS is in development. Feel free to contact_ me on this.


How to visualize results?
^^^^^^^^^^^^^^^^^^^^^^^^^
Paraview_ and VisIt_ are state of the art scientific visualization tools which can run on multi-node GPU clusters. Thus, they can in principle achieve a much better performance than all hitherto proposed APT solutions to the best of my knowledge. However, many APT probers have their own philosophy and working strategy how to visualize APT data. This is why I opted to deliver XDMF supplementary results and leave the choice for now to the users.

  .. _Paraview: http://www.paraview.org/
  .. _VisIt: http://wci.llnl.gov/simulation/computer-codes/visit/

As much as APT experimentalist value the sophistication of their tomography lab equipment, they should also value the numerical costs that most computational tasks during APT data mining pose. Especial when working with multi-hundred million ions, i.e. large datasets. This touches the lower end of performance requirements of scientific visualization methods. Open source tools for this exist. It is just on us to use them productively and creatively!

.. figure:: ../images/VoroTessPartition02.png
   :scale: 20%
   :align: center

   
Are the results deterministic? What about randomness?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
PARAPROBE uses the Mersenne Twister algorithm. Random numbers are created using deterministic random number seeds, one for each thread. Therefore, paraprobe-synthetic runs are reproducible when executed with the same number of OpenMP threads.

 .. _contact: https://www.mpie.de/person/51206/2656491



  
