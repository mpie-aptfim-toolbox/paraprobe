Run **paraprobe-utils**
^^^^^^^^^^^^^^^^^^^^^^^

PARAPROBE utils alone can not be executed. It is just a compilation of object code that other tools include to reduce compilation time.
