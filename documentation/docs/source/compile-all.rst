How to compile? A cooking recipe!
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Prepare your system to have functional set of third-party tools.

2. Go to Gitlab_ and download the PARAPROBE source code.

  .. _Gitlab: http://gitlab.mpcdf.mpg.de/mpie-aptfim-toolbox/paraprobe.git
  
3. Unpack the repository in a location of your choice.

4. Edit PARAPROBE.ExternalLibraries.cmake to tell where HDF5 is. The following lines need to be modified to reflect where the paraprobe code folder is on your computer::

    set MYPROJECTPATH
    set MYHDFPATH 

5. By default PARAPROBE comes shipped with GPU support off, go to step 8 to optionally activate compilation for GPUs.

6. Edit the slurm.paraprobe.build.all.sh script and switch on which tools should be compiled::

    build_transcoder=true
    ...

7. Assure you are in paraprobe/code then compile all tools with a single command. Also it might be necessary to make the scripts write executable. If you are using environment modules, which I recommend, don't forget to load them::

    source slurm.paraprobe.build.all.sh
    chmod +x slurm.paraprobe.build.all.sh

8. Advanced knowledge: GPU support, activate it only when there is a GPU and using either araullo or fourier tools::

    cd paraprobe-araullo/src/
    vim PARAPROBE_AraulloHdlGPU.h
    i
    #define UTILIZE_GPUS
    wq!
    cd ../../
    cd paraprobe-fourier/src/
    vim PARAPROBE_DFTHdlGPU.h
    i
    #define UTILIZE_GPUS
    wq!
    cd ../../

9. Assure your back in paraprobe/code and execute step 7.
