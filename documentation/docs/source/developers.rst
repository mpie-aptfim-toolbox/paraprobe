How to contribute
^^^^^^^^^^^^^^^^^

**Did you know** that **you can contribute** to PARAPROBE? **Yes!** - given that the source code is open and given that there is a GitLab repository open for developers world-wide here_ contributing is straightforward! Contact me_ for details.

 .. _me: http://www.mpie.de/person/51206/2656491
 .. _here: http://gitlab.mpcdf.mpg.de/mpie-aptfim-toolbox/paraprobe
 

How to get assistance
^^^^^^^^^^^^^^^^^^^^^
Please feel free to contact me (using my MPIE_ or BiGmax_ contact details). Whether its because you are facing problems with using PARAPROBE or want to recommend useful new features to include PARAPROBE. In case of bug reports please send always a console prompt, i.e. the STDOUT and STDERR command line output of the simulation run and the XML input file. This will help me in diagnosing potential problems and improve the tools.

 .. _MPIE: https://www.mpie.de/person/51206/2656491
 .. _BiGmax: https://bigmax.iwww.mpg.de/39151/bigmax-software-engineering-consultant


