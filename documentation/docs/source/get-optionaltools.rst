Get **optional tools**
^^^^^^^^^^^^^^^^^^^^^^

I can recommend the following open source software which I am using on my APT workstation on a daily basis:

* Anaconda_

  .. _anaconda: http://www.anaconda.com/distribution/?gclid=EAIaIQobChMIs83qltzD5QIVEOR3Ch1KgAFnEAAYASAAEgKVV_D_BwE

* spyder_

  .. _spyder: http://www.spyder-ide.org/

* Paraview_

  .. _Paraview: http://www.paraview.org/
  
And less frequently, though because even more powerful but also complexer:

* VisIt_

  .. _VisIt: http://wci.llnl.gov/simulation/computer-codes/visit/

* Blender_

  .. _Blender: http://www.blender.org/

