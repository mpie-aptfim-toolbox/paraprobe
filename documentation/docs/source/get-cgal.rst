Get **GMP**
^^^^^^^^^^^
The Computational Geometry Algorithms Library (CGAL) is an open source project of computational geometry domain experts. The library is used in the surfacer tool 
for computing alpha shape triangle hulls to the dataset. CGAL in version 4.11.3 supports sufficiently all currently implemented functionality.

A key challenge in computational geometry is the handling of numerical degeneracies such as triangles where one of the vertices collapses on an edge and cases alike.
One of many tricks are played to handle these cases in professional tools, like CGAL. One trick is to use floating point formats with higher than 64-bit precision. This calls for installation of additional software packages before CGAL can be used. These two packages are GMP and MPFR. Consequently, I will detail first the installation of the utility libraries 
for an exemplar clean install of an Ubuntu 18.04 LTS workstation.

1. Download GMP, the GNU Multiple Precision Library here_

 .. _here: http://gmplib.org/

2. Extract the tar archive via::

    sudo tar –xvf gmp-6.1.2.tar.bz2
    cd gmp-6.1.2.tar.bz2
    sudo ./configure
    sudo make
    sudo make install
    sudo make check

3. If all checks were passed, GMP is all set.

Get **MPFR**
^^^^^^^^^^^^

MPFR, the GNU MPFR library is available via the default apt-get Ubuntu software stack::

    sudo apt-get update
    sudo apt-get install libmpfr-dev


Get and configure **CGAL**
^^^^^^^^^^^^^^^^^^^^^^^^^^

CGAL can be downloaded at http://www.cgal.org/ ::

    sudo tar –xvf CGAL-4.11.3.tar.xz
    cd CGAL-4.11.3

**We need exclusively the header-only part of the library!** This avoids any need to install additional requirements::

    sudo vim CMakeLists.txt
    i
    change the option(CGAL_HEADER_ONLY … OFF) from OFF to ON
    wq!

Now configure cmake only once in the top-level folder CGAL-4.11.3::

    sudo cmake .


.. sudo make
