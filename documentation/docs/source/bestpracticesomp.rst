Troubleshooting?!
^^^^^^^^^^^^^^^^^
If unrecoverable errors occur during the compilation process occur it is wise to attempt a clean of the build folder. For this go into the 
build folder and type::

    rm -rf *

**Be careful, this will delete all content from the build folder also possible non-PARAPROBE related content!**.


Compiler optimization
^^^^^^^^^^^^^^^^^^^^^
Compiler optimization within PARAPROBE is set be default to at least O2. If desired, adjust the level of compiler optimization via the OPTLEVEL variable in the CMakeLists.txt upper section. OPTLEVEL "-O0" means no optimization and should be utilized for debugging purposes only, while "-O3 -march=native" is the maximum and recommended level for production tasks. Improvements between the two extremes vary between a factor of 2 - 5 faster with maximum optimization compared to without.


Best practices when using multi-threading
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Multi-threading in PARAPROBE is realized through OpenMP API calls which distributes the workload dynamically
across multiple computing cores. Multi-threading is activated through setting the OpenMP number of threads environment variables::

    export OMP_NUM_THREADS=20
    export OMP_PLACES=cores

The first command takes a positive integer which specifies how many threads should be spawned per MPI process. The second command manages what is referred to as thread affinity, i.e. how the operating system executes the threads on the cores in relation to the cores specific access to memory.


Further details of OpenMP multi-threading
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Please note, setting above environment variables is required only once per active console session. 
Especial Intel processors offer a Hyper-Threading (HT_) as a functionality, which suggests that there are twice as many cores as should be used in practice.
The reason is that Hyper-Threading cores come always in pairs. Both cores of the pair share almost all performance critical memory infrastructure, which will cause that if a program attempts to always use both cores, the cores will fight for resources. For this reasoning, only as many threads as 
existent real physical core pairs should be instructed through the OMP_NUM_THREADS environment variable. If you have for instance a two-socket ten-(HT)-core system, you should instruct the usage of at most 2x10 instead of 2x20 threads.

 .. _HT: https://www.intel.com/content/www/us/en/architecture-and-technology/hyper-threading/hyper-threading-technology.html

 
Manage thread affinity
^^^^^^^^^^^^^^^^^^^^^^
Thread affinity deals with the question whether a thread, once it has been mapped on a CPU core, should always be executed on this core or may
be relocated to another core in an effort to improve global system performance. In fact, if not explicitly instructed otherwise, 
the operating system usually instructs such migration operations during program operation, in particular when multiple users or programs
work on the same system. Such strategy to improve global system performance, however, may not necessarily mean a performance improvement of the
your PARAPROBE tool job, how should the operating system know... Instead, frequent thread migration reduces PARAPROBE performance owing to re-initialization costs. Namely, once a thread is instructed to migrate, its already allocated cache content will in most cases not exist on the new core. Instead, costly refurnishing of cache content is required. For these reasons, overwriting the operating systems thread affinity policy can be worthwhile to prevent
the OS from migrating threads across the available cores. One can instruct this explicitly via setting for instance the KMP AFFINITY_ 
environment variables when using the Intel compiler or the GNU_ thread affinity policy or, use as I recommend the OMP_PLACES=cores automatism.

 .. _AFFINITY: https://software.intel.com/en-us/node/522691
 .. _GNU: https://gcc.gnu.org/onlinedocs/libgomp/GOMP_005fCPU_005fAFFINITY.html
