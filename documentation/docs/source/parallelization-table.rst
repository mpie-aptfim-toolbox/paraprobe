Which tool offers which parallelism?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

 .. csv-table:: Parallelization and resulting dependencies.
     :header: "paraprobe-", "MPI", "OpenMP", "CUDA", "OpenACC", "CUFFT"
	 
	 "parmsetup", " ", " ", " ", " ", " "
	 "utils", "x", "x", " ", " ", " "
	 "gui", " ", " ", " ", " ", " "
	 "transcoder", "x", " ", " ", " ", " "
	 "synthetic", "x", "x", " ", " ", " "
	 "reconstruct", "x", " ", " ", " ", " "
	 "ranger", "x", " ", " ", " ", " "
	 "surfacer", "x", "x", " ", " ", " "
	 "spatstat", "x", "x", " ", " ", " "
	 "tessellator", "x", "x", " ", " ", " "
	 "fourier", "x", "x", "x", "x", " "
	 "araullo", "x", "x", "x", "x", "x"
	 "indexer", "x", "x", "x", "x", " "
	 "autoreporter", " ", " ", " ", " ", " "

..  "intersector", "x", "x", " ", " ", " "
..  "distancer, "x", "x", " ", " ", " "


MPI_ is the Message Passing Interface.
OpenMP_ the Open Multi-Processing API.
CUDA_ the Compute Unified Device Architecture API.
OpenACC_ the Open Accelerator API.
CUFFT_ NVIDIAs CUDA-powered Fourier transform library.

  .. _MPI: http://en.wikipedia.org/wiki/Message_Passing_Interface
  .. _OpenMP: http://en.wikipedia.org/wiki/OpenMP
  .. _OpenACC: http://en.wikipedia.org/wiki/OpenACC
  .. _CUDA: http://developer.nvidia.com/cuda-zone
  .. _CUFFT: http://docs.nvidia.com/cuda/cufft/index.html