First things first
^^^^^^^^^^^^^^^^^^
**PARAPROBE** is a **collection of C/C++/CUDA source code** packages and **Python and Matlab scripts**. The source code needs to be compiled before it is ready to rock'n'roll. As we should not re-implement the wheel, PARAPROBE uses third-party software which has to be properly installed on the workstation or cluster before PARAPROBE can be used to its full potential.


Which operating system is supported?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
PARAPROBE targets **workstations and computing clusters**, i.e. **Linux-based** operation systems. It was extensively tested on
Ubuntu 18.04 LTS and SUSE Linux Enterprise Server 15 SP1. The compilation of most tools on a Windows system should in principle be possible technically. 
It has so far, though, not been tested.


How large datasets are supported?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Currently, single APT specimens (measurements) containing at most 2^31 ions. As of 2019, such large specimens have to the best of my knowledge not been measured experimentally. The tool has been tested with synthetic specimens containing up to two billion ions. 
Please contact me_ if you have larger datasets, I am eager to modify the code.

 .. _me: https://www.mpie.de/person/51206/2656491

 
What are the minimum hardware requirements?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
**Memory** --- data mining APT datasets is processing of three-dimensional point data with annotations. Therefore, hardware minimum requirements depend primarily and necessarily on the total number of ions the user wishes to process. Internally, each ion occupies 16B memory. In addition, buffers are created which decouple the computations during multi-threading. This requires additional memory. Quantitative results are detailed in the initial PARAPROBE paper which is referred to in the reference sub-section of this manual.

**CPU** --- virtually all modern workstation and cluster computing processors are capable of executing PARAPROBE. Their cost-benefit-ratio and speed of doing so may differ substantially though. Consequently, claiming minimum hardware requirements is pointless.

**GPU** --- PARAPROBE utilizes GPUs in the atom probe crystallography tools, i.e. paraprobe-fourier, paraprobe-araullo, and paraprobe-indexer. CPU-only fallbacks are implemented. The code has been tested on single GPU workstations with NVIDIA RTX2080 Ti as well as on server nodes with two NVIDIA V100 GPUs each.


Which prerequisites are necessary?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
PARAPROBE depends on third-party open source software and open source Linux tools. These dependencies are detailed in the tables below.


Key ideas to understand
^^^^^^^^^^^^^^^^^^^^^^^
**PARAPROBE is not a monolithic software**. Instead it comes as a collection of tools. Frequently used functionality and algorithms
are implemented in a set of utility object code, called paraprobe-utils, whose compilation is always required. The other PARAPROBE tools
include these functions upon compilation as required.

**It is not required to compile all code** to be able to use PARAPROBE. 
This is illustrated in the following tables which specify the dependencies.

**Not every tool uses all levels of parallelism**. This is illustrated in the second table. For two reasons: on the one hand because it may not necessarily bring a substantial performance advantage. On the other hand because my time resources are finite, which is why I eventually had to made compromises on which tools and core computational parts to parallelize and focus first. If you are interested in spinning the wheel and contribute to extend PARAPROBE, feel very much invited to do so, ideally for cooperations or 
at least contact_ me. 


Third-party tools
^^^^^^^^^^^^^^^^^
The **Hierarchical Data Format (HDF5)** library **is required** for every tool. HDF5_ is a binary container file format. While this at first glance might appear more complex and difficult to use than the traditional ASCII-based text files frequently used in APT, there are several advantages to use HDF5 over traditional text files or especially the closed containers file used in the atom probe tomography.

* The output of all analysis runs is bundled. Thus, the number of files is reduced which improves processing speed.
* HDF5 offers capabilities to store the metadata. 
* The format is self-descriptive with respect to which format and size the data arrays within the file have.
* Also HDF5 is transparent with respect to which data elements the file contains.
* HDF5 is optimized for performance on datasets typically much larger than used for even the largest APT specimens.
* HDF5 offers dataset slicing, data compression, and parallel I/O functionality, i.e. reduces re-implementing the wheel.
* Accessing a content of an HDF5 dataset is straightforward as reading in an Excel file as these examples show.
* HDF5 files are binary files but their content and even individual values can be viewed using the HDF5Viewer.

Using Matlab and its high-level HDF5 library interface, a simple two-dimensional array can be imported using a single line of code::

    MATRIX2D = h5read( 'PARAPROBE.Tool.Results.h5', 'Datasetname/MatrixOfInterest' )

Similarly simple it is, to read the same matrix using Python::

    import h5py
    hf = h5py.File('PARAPROBE.Tool.Results.h5', 'r')
    MATRIX2D = np.array(list(hf['Datasetname/MatrixOfInterest'])).flatten()

See? Honestly, this is even simpler to use as writing the actual implementation in C/C++ in PARAPROBE... Anyway, let's proceed with third-party tools.

**Python** is a programming language that can be accessed through writing scripts. I recommend anaconda, spyder and jupyter notebooks. For PARAPROBE tools, Python_ is used as a glue language to post-process the results via less compute intensive parts and especial to allow the users to write arbitrarily advanced automation and data post-processing scripts, which can again be in principle interface to APSuite/IVAS.

**CGAL**, the Computational Geometry Algorithms Library (CGAL_) is used in PARAPROBE to perform key computational geometry tasks numerically robustly.

**Voro** (++), is a third-party C++ software (library_) which computes Voronoi tessellations. It serves as an easier accessible alternative to the Quickhull library that has been traditionally used in some atom probe analyses. Its key advantage is that it allows for incremental building of the tessellations as a consequence of which much lower memory consumption during construction of the tesselation especial for large datasets is achieved.

**TetGen** and **Overlap**, are third-party computational geometry libraries that PARAPROBE uses to tetrahedralize Voronoi cells and compute the 3D intersection volume
of different geometrical primitives (tetraeder, spheres).

 .. _Python: http://anaconda.org/
 .. _HDF5: http://www.hdfgroup.org
 .. _CGAL: http://www.cgal.org
 .. _library: http://math.lbl.gov/voro++/download/


Compilers required
^^^^^^^^^^^^^^^^^^
**y** means yes, this compiler can be used. **?** means this compiler has not been tested on PARAPROBE. **n** means it is expected that this compiler will not work.

 .. csv-table:: How to compile or bring to the tool to life.
      :header: "paraprobe-", "Intel", "GCC", "PGI", "Python"
	 
	 "utils", "y", "?", "y", " "
	 "transcoder", "y", "?", "y", " "
	 "synthetic", "y", "?", "y", " "
	 "ranger", "y", "?", "y", " "
	 "surfacer", "y", "?", "n", " "
	 "spatstat", "y", "?", "?", " "
	 "autoreporter", " ", " ", " ", "y"
	 "fourier", "n", "?", "y", " "
	 "araullo", "n", "?", "y", " "
	 "indexer", "n", "?", "y", " "
..	 "distancer", "y", "?", "?", " "
..	 "tessellator", "y", "?", "?", " "

The compilers above mean the Intel_, the GNU, the Portland Group (PGI_) compiler.  
  .. _Intel: http://software.intel.com/en-us/qualify-for-free-software
  .. _GNU: http://de.wikipedia.org/wiki/GNU_Compiler_Collection
  .. _PGI: http://www.pgroup.com/products/community.htm

So, which tools do I need for what?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. utils is always required.
2. transcode brings your IVAS data into PARAPROBE.
3. synthetic builds you rigorously defined test configurations, single- and polycrystalline specimens, with and without precipitates.
4. surfacer and spatstat are you friend for all spatial statistics questions.
5. araullo + indexer and fourier are your friends for cutting-edge atom probe crystallography using GPUs.
4. autoreporter and parmsetup are tools for you who likes to automate analyses and create automatic lab reports.
  
So, what do I have to do in which order?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Prepare your system to have functional set of third-party tools.
2. Download PARAPROBE source code from Gitlab_

  .. _Gitlab: http://gitlab.mpcdf.mpg.de/mpie-aptfim-toolbox/paraprobe.git
  
3. Unpack the repository in a location of your choice.
4. Edit PARAPROBE.ExternalLibraries.cmake to tell where HDF5 is.
5. By default PARAPROBE comes shipped with GPU support off, there are OpenMP alternatives.
6. Edit the slurm.paraprobe.build.all.sh script and switch on which tools should be compiled::

    build_transcoder=true
	...

7. Assure you are in paraprobe/code then compile all tools with a single command::

    source slurm.paraprobe.build.all.sh
	
It might be necessary to make the scripts write executable::

    chmod +x slurm.paraprobe.build.all.sh
	
8. Advanced knowledge: GPU support, activate it only when there is a GPU and using either araullo or fourier tools.

	cd paraprobe-araullo/src/
	vim PARAPROBE_AraulloHdlGPU.h
    i
	#define UTILIZE_GPUS
	wq!
	cd ../../
	cd paraprobe-fourier/src/
	vim PARAPROBE_DFTHdlGPU.h
	i
	#define UTILIZE_GPUS
	wq!
	cd ../../

9. Assure your back in paraprobe/code and execute step 7.


How to get my IVAS data into PARAPROBE?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Use IVAS and export a reconstructed POS or EPOS. 
2. Use IVAS and perform a ranging, export a RRNG range file.
3. Use the transcoder tool to go from POS/EPOS to APTH5.
4. Apply PARAPROBE tools as desired on real or synthetic data.


What to do with the PARAPROBE results?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

5. Extract results for further analyses from HDF5 using Matlab or Python. Autoreporter has many example scripts to help you.
6. Visualize results using Paraview and XDMF/HDF5. 

.. 7. Transcoding back from HDF5 to POS and EPOS is in development. Feel free to contact_ me on this.

If interested in atom probe crystallography exclusively, i.e. paraprobe-fourier, paraprobe-araullo, or paraprobe-indexer, compile PARAPROBE with the PGI compiler. 
Otherwise, use the Intel compiler. Some combinations of tools and compilers are currently not possible. This is also detailed in the tables below.


Advanced knowledge: Visualizing APT results
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Paraview_ and VisIt_ are state of the art scientific visualization tools which run on multi-node GPU clusters. Thus, they can in principle achieve a much better scaling rendering and production performance than all hitherto proposed APT solutions to the best of my knowledge.

  .. _Paraview: http://www.paraview.org/
  .. _VisIt: http://wci.llnl.gov/simulation/computer-codes/visit/

As much as APT experimentalist value the sophistication of their tomography lab equipment, they should value the numerical costs that most computational tasks during APT data mining pose. Especial when working with multi-hundred million ion, i.e. large specimens: This touches the lower end of performance requirements of scientific visualization methods. Open source tools for this exist. It is just on us to use them productively and creatively!

.. figure:: ../images/VoroTessPartition02.png
   :scale: 20%
   :align: center

   
Advanced knowledge: Third-party dependencies
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

 .. csv-table::  Third-party dependencies
     :header: "paraprobe-", "C/C++", "Python", "HDF5", "CGAL", "Voro++", "TetGen"
	 
	 "parmsetup", " ", "x", " ", " ", " ", " "
	 "utils", "x", " ", "x", " ", " ", " "
	 "transcoder", "x", " ", "x", " ", " ", " "
	 "synthetic", "x", " ", "x", " ", "x", " "
	 "ranger", "x", " ", "x", " ", " ", " "
	 "surfacer", "x", " ", "x", "x", " "
	 "spatstat", "x", " ", "x", " ", " ", " "
	 "fourier", "x", " ", "x", " ", " ", " "
	 "araullo", "x", " ", "x", " ", " ", " "
	 "indexer", "x", " ", "x", " ", " ", " "
	 "autoreporter", " ", "x", "x", " ", " ", " "
..	 "intersector", "x", " ", "x", " ", " ", " "
..	 "distancer", "x", " ", "x", " ", " ", " "
..	 "tessellator", "x", " ", "x", " ", "x", " "


Advanced knowledge: Utilization of pseudo-random numbers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
PARAPROBE uses the Mersenne Twister algorithm. Random numbers are created using deterministic random number seeds, one for each thread. Therefore, paraprobe-synthetic runs are reproducible when executed with the same number of OpenMP threads.

 .. _contact: https://www.mpie.de/person/51206/2656491


Advanced knowledge: Parallelism offered
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

 .. csv-table:: Parallelization and resulting dependencies.
     :header: "paraprobe-", "MPI", "OpenMP", "CUDA", "OpenACC", "CUFFT"
	 
	 "parmsetup", " ", " ", " ", " ", " "
	 "utils", "x", "x", " ", " ", " "
	 "transcoder", "x", " ", " ", " ", " "
	 "synthetic", "x", "x", " ", " ", " "
	 "ranger", "x", " ", " ", " ", " "
	 "surfacer", "x", "x", " ", " ", " "
	 "spatstat", "x", "x", " ", " ", " "
	 "fourier", "x", "x", "x", "x", " "
	 "araullo", "x", "x", "x", "x", "x"
	 "indexer", "x", "x", "x", "x", " "
	 "autoreporter", " ", " ", " ", " ", " "
..	 "intersector", "x", "x", " ", " ", " "
..	 "distancer, "x", "x", " ", " ", " "
..	 "tessellator", "x", "x", " ", " ", " "

MPI_ is the Message Passing Interface.
OpenMP_ the Open Multi-Processing API.
CUDA_ the Compute Unified Device Architecture API.
OpenACC_ the Open Accelerator API.
CUFFT_ NVIDIAs CUDA-powered Fourier transform library.

  .. _MPI: http://en.wikipedia.org/wiki/Message_Passing_Interface
  .. _OpenMP: http://en.wikipedia.org/wiki/OpenMP
  .. _OpenACC: http://en.wikipedia.org/wiki/OpenACC
  .. _CUDA: http://developer.nvidia.com/cuda-zone
  .. _CUFFT: http://docs.nvidia.com/cuda/cufft/index.html
  
