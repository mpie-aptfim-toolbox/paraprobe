PARAPROBE browser-based GUI to define your analyses
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To assist you in setting up properly configured PARAPROBE XML settings file there is an optional Python/Bokeh-powered GUI. The concept behind this GUI is to set up workflows. A workflow is a specific sequential combination of the paraprobe tools which processes a dataset in a number of steps. Most of these individual steps are executed in parallel.
Analyses can be defined conveniently in the browser. The appropriate settings file are then created automatically.

Currently two workflows exist. One for processing spatial statistics and one for atom probe crystallography in reconstruction space.


