
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Configure paraprobe-surfacer &#8212; PARAPROBE 0.1 documentation</title>
    <link rel="stylesheet" href="_static/classic.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    
    <script id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script src="_static/language_data.js"></script>
    
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Configure paraprobe-spatstat" href="config-spatstat.html" />
    <link rel="prev" title="Configure paraprobe-ranger" href="config-ranger.html" /> 
  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="config-spatstat.html" title="Configure paraprobe-spatstat"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="config-ranger.html" title="Configure paraprobe-ranger"
             accesskey="P">previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">PARAPROBE 0.1 documentation</a> &#187;</li>
          <li class="nav-item nav-item-1"><a href="configure-details.html" accesskey="U">Configure</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Configure <strong>paraprobe-surfacer</strong></a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="configure-paraprobe-surfacer">
<h1>Configure <strong>paraprobe-surfacer</strong><a class="headerlink" href="#configure-paraprobe-surfacer" title="Permalink to this headline">¶</a></h1>
<p>PARAPROBE surfacer is the tool which computes an alpha-shape triangle hull to the entire dataset. The CGAL library is used to compute the alpha shape and export the triangles as objects. They can be visualized using Paraview and the generated XDMF/HDF5 file combination. PARAPROBE surfacer uses a filtering algorithm which thins the point cloud. Thereby, most ions in the interior are not at all passed to CGAL. This enables surfacer to compute alpha-shapes even for datasets as large as two billion ions without any need for ad hoc sub-sampling or random thinning of the data like it was commonly done in classical APT studies. The resulting triangle hulls are not necessarily water-tight. Feel free to contact me in case there is interest in collaborating to patch these holes. The alpha-shape triangle hull is used to evaluate the distance of each ion to the closest triangle using an exact analytical procedure.</p>
<p>PARAPROBE surfacer takes an APTH5 file with the ion positions and returns a HDF5 file with the triangles, distance values, and additional details. The tool is controlled with a single XML settings file. The possible settings for paraprobe-surfacer are as follows:</p>
<div class="line-block">
<div class="line"><strong>SurfacingMode</strong></div>
</div>
<p>Set to 1 to use CGAL and compute an alpha shape to the dataset.</p>
<div class="line-block">
<div class="line"><strong>InputfileReconstruction</strong></div>
</div>
<p>The ion positions as an APTH5 file. Needs to have file ending <a href="#id1"><span class="problematic" id="id2">*</span></a>.apth5</p>
<div class="line-block">
<div class="line"><strong>AlphaShapeAlphaValue</strong></div>
</div>
<p>Choose 0 to pick the alpha shape with the smallest alpha value to get a solid through the data points.
Choose 1 to pick the alpha shape which CGAL considers to be the optimal value. Consult the CGAL library for further details.</p>
<div class="line-block">
<div class="line"><strong>AdvIonPruneBinWidthMin</strong></div>
<div class="line"><strong>AdvIonPruneBinWidthIncr</strong></div>
<div class="line"><strong>AdvIonPruneBinWidthMax</strong></div>
</div>
<p>Specify in nanometer the width of the rectangular bins chosen during the initial filtering stage. Values of 0.5 nm are suited for most datasets. If value is larger unnecessarily many ions are passed to CGAL, thereby slowing down the alpha shape computation. Values smaller may result in gaps in the dataset or percolation of triangles through the point cloud such that a bi-layer hull is generated.</p>
<div class="line-block">
<div class="line"><strong>DistancingMode</strong></div>
</div>
<p>Set to 0 if no distancing is desired. Typically results in bias for ions at the dataset boundary and is not recommended when reporting spatial statistics but is very fast. Set to 1 to compute for every ion analytically a distance value. Can be time consuming. However, this is where the multi-level parallelization of PARAPROBE assists. Set to 2 if spatial statistics and all other subsequent analyses should take into account only regions of interest with a maximal radius of DistancingRadiusMax from the boundary. In this case, PARAPROBE surfacer sets DistancingRadiusMax as the default distance for all ions which are farther away from the boundary than DistanceRadiusMax. The tool employs a pre-processing algorithm which evaluates whether an ion has triangle neighbors within DistanceRadiusMax or closer, if not it skips the ion. This makes 2 a faster option than 1.</p>
<div class="line-block">
<div class="line"><strong>DistancingRadiusMax</strong></div>
</div>
<p>Only significant when DistancingMode is set to 2. Defines the threshold distance up to which ions are exactly distanced. For all other ions this threshold is returned.</p>
<div class="line-block">
<div class="line"><strong>AdvDistanceBinWidthMin</strong></div>
<div class="line"><strong>AdvDistanceBinWidthIncr</strong></div>
<div class="line"><strong>AdvDistanceBinWidthMax</strong></div>
</div>
<p>For large datasets and analytical distancing in mode 1 one has to eventually test a large number of triangles for each ion as one does not know a priori which triangle is closest. Therefore, mode 1 uses a pre-processing strategy. First, the specimen is gridded using a coarse voxel grid. The size of which is controlled with the AdvDistanceBinWidth settings. Now for all these voxels we compute the center distance to the triangle hull exactly. Having now a coarse estimate of the size distribution this information is used to reduce triangle querying costs.</p>
<div class="line-block">
<div class="line"><strong>RequeryingThreshold</strong></div>
</div>
<p>This is a constant that should be larger than 0 and smaller than 1, I recommend 0.7.
The value controls details in the fine distancing stage where it assists to reduce the number of triangle tests.</p>
</div>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <h4>Previous topic</h4>
  <p class="topless"><a href="config-ranger.html"
                        title="previous chapter">Configure <strong>paraprobe-ranger</strong></a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="config-spatstat.html"
                        title="next chapter">Configure <strong>paraprobe-spatstat</strong></a></p>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="_sources/config-surfacer.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="config-spatstat.html" title="Configure paraprobe-spatstat"
             >next</a> |</li>
        <li class="right" >
          <a href="config-ranger.html" title="Configure paraprobe-ranger"
             >previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">PARAPROBE 0.1 documentation</a> &#187;</li>
          <li class="nav-item nav-item-1"><a href="configure-details.html" >Configure</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Configure <strong>paraprobe-surfacer</strong></a></li> 
      </ul>
    </div>
    <div class="footer" role="contentinfo">
        &#169; Copyright 2018-2020, Markus Kuehbach.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 3.1.1.
    </div>
  </body>
</html>